# Khoja

## Versions

#### PHP

CakePhp: [Cookbook](https://book.cakephp.org/3.0/en/index.html) | [Github](https://github.com/cakephp/cakephp).

Localized plugin for CakePhp: [Github](https://github.com/cakephp/localized).

#### CSS

Bootstrap 3: [Official Website](http://getbootstrap.com/)

Bootstrap 3 Helpers for CakePHP: [Github](https://github.com/Holt59/cakephp3-bootstrap-helpers)

Inspinia 2.7 [Live Example](http://webapplayers.com/inspinia_admin-v2.7/index.html) | [Purchase Page](https://wrapbootstrap.com/theme/inspinia-responsive-admin-theme-WB0R5L90S)

Font-Awesome [Official website](http://fontawesome.io/icons/)

#### JS

iCheck 1.0.1 [Official website](http://icheck.fronteed.com/)

Jasny Bootstrap [Official website](http://www.jasny.net/bootstrap/)

## Configuration

Read and edit `config/app.php` and setup the `'Datasources'` section, the `'EmailTransport'` and `'Email'` parts. Also, the `'debuggingEmail'` is needed to catch all emails sent by the system when `debug` is set to true.

To view the web app, you can now either use your machine's webserver, or start
up the built-in webserver with:

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the home page.

## How To Compile The SCSS

1. Make sure you have npm. (https://www.npmjs.com/get-npm)

2. Install gulp globally if not already done.
	```bash
	npm install --global gulp-cli
	```

3. Install the development dependencies.
	```bash
	npm install
	```
	
4. Edit the gulpconfig.json if needed.
	- You might have to change the browserSync.proxy value to your project local server url.
	- You can set the browserSync.open to `local`, `external`, `ui`, `tunnel` and false.
		
5. Run our custom script to compile and watch for changes
	```bash
	npm run dev
	```

*** 
If browserSync doesn't work for you, it might be because the proxy is blocked by a software on your machine. In my case, it doesn't work when Malware Bytes Web Protection is turned on.
***

:shipit: