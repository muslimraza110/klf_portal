"use strict";

// config
import config from './gulpconfig.json';
const configSCSS = config.scss;
const configJS = config.js;

// gulp
import gulp from 'gulp';
import gulpSequence from 'gulp-sequence';

// file manipulation
import addsrc from 'gulp-add-src';
import rimraf from 'gulp-rimraf';
import rename from 'gulp-rename';
import finder from 'find-package-json';

// logging
import log from 'fancy-log';
import color from 'gulp-color';
import debug from 'gulp-debug';

// prevent pipe breaking, log and notify using gulp-plumber and node-notifier
import plumberNotifier from 'gulp-plumber-notifier';

// sass compiling
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import cleanCSS from 'gulp-clean-css';
import sourcemaps from 'gulp-sourcemaps';
import tildeImporter from 'node-sass-tilde-importer';

// js compiling
import babel from 'gulp-babel';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import jshint from 'gulp-jshint';

// browser refreshing
const browserSync = require('browser-sync').create();

/*
 * FUNCTIONS
 */

const getIncludes = (includes) => {
	
	let finalIncludes = [];
	const project = (finder('.')).next().value.name;
	
	for (var i in includes) {
		if (includes.hasOwnProperty(i)) {
			
			let path = includes[i];
			let obj = {};
			
			if (typeof path === "object") {
				
				obj = path;
				
			} else {
				
				obj = {
					path
				};
				
			}
			
			try {
				
				let module = (finder(path)).next().value.name;
				
				if (module !== project) {
					
					obj.module = module;
					
					if (!obj.subdir && obj.subdir !== false) {
						obj.subdir = module;
					}
					
				}
				
			} catch (err) {
				
				// not a node module
				
			}
			
			finalIncludes.push(obj);
			
		}
	}
	
	return finalIncludes;
	
};

/*
 * FONTS TASKS:
 *  - copy
 */

gulp.task('copy:fonts', (callback) => {
	
	if (!config.fonts || !config.fonts.includes || !config.fonts.includes.length) {
		return;
	}
	
	const includes = getIncludes(config.fonts.includes);
	
	for (let i in includes) {
		if (includes.hasOwnProperty(i)) {
			
			log.info('\'' + color('copy:fonts', 'CYAN') + '\' is copying ' + color(includes[i].path, 'BLUE'));
			
			gulp.src(includes[i].path)
				.pipe(plumberNotifier())
				.pipe(gulp.dest(config.fonts.output.path + includes[i].subdir));
			
		}
	}
	
	callback();
	
});

/*
 * SCSS TASKS:
 *  - clean
 *  - compile
 *  - build
 *  - watch (+ watchSeq)
 */

gulp.task('clean:scss', () => {
	
	let srcs = [
		configSCSS.output.path + configSCSS.output.name + '.css',
		configSCSS.output.path + configSCSS.output.name + '.map'
	];
	
	if (configSCSS.minify !== false) {
		
		srcs.push(configSCSS.output.path + configSCSS.output.name + '.min.css');
		srcs.push(configSCSS.output.path + configSCSS.output.name + '.min.map');
		
	}
	
	return gulp.src(srcs, {read: false})
		.pipe(plumberNotifier())
		.pipe(debug({
			title: '\'' + color('clean:scss', 'CYAN') + '\' is removing'
		}))
		.pipe(rimraf());
	
});

gulp.task('compile:scss', () => {
	
	log.info('\'' + color('compile:scss', 'CYAN') + '\' source is ' + color(configSCSS.input.path + configSCSS.input.name + '.scss', 'BLUE'));
	
	let task = gulp.src(configSCSS.input.path + configSCSS.input.name + '.scss')
		.pipe(plumberNotifier());
	
	if (configSCSS.maps !== false) {
		task = task.pipe(sourcemaps.init());
	}
	
	log.info('\'' + color('compile:scss', 'CYAN') + '\' is ' + color('compiling scss', 'YELLOW'));
	
	task = task
		.pipe(debug({
			title: '\'' + color('compile:scss', 'CYAN') + '\' compiled'
		}))
		.pipe(sass({
			outputStyle: 'expanded',
			importer: tildeImporter
		}))
		.pipe(rename({extname: '.css'}));
	
	if (configSCSS.autoprefixer === false) {
		
		log.info('\'' + color('compile:scss', 'CYAN') + '\' is ' + color('skipping autoprefixer', 'YELLOW'));
		
	} else {
		
		log.info('\'' + color('compile:scss', 'CYAN') + '\' is ' + color('using autoprefixer', 'YELLOW'));
		
		let autoprefixerBrowsers = [
			"last 1 major version",
			">= 1%"
		];
		
		if (configSCSS.autoprefixer === 'bootstrap4') {
			
			autoprefixerBrowsers = [
				"last 1 major version",
				">= 1%",
				"Chrome >= 45",
				"Firefox >= 38",
				"Edge >= 12",
				"Explorer >= 10",
				"iOS >= 9",
				"Safari >= 9",
				"Android >= 4.4",
				"Opera >= 30"
			];
			
		} else if (configSCSS.autoprefixer === 'bootstrap3') {
			
			autoprefixerBrowsers = [
				"last 1 major version",
				">= 1%",
				'Android 2.3',
				'Android >= 4',
				'Chrome >= 20',
				'Firefox >= 24',
				'Explorer >= 8',
				'iOS >= 6',
				'Opera >= 12',
				'Safari >= 6'
			];
			
		}
		
		task = task
			.pipe(debug({
				title: '\'' + color('compile:scss', 'CYAN') + '\' prefixed'
			}))
			.pipe(autoprefixer({
				browsers: autoprefixerBrowsers
			}));
		
	}
	
	if (configSCSS.minify === false) {
		
		log.info('\'' + color('compile:scss', 'CYAN') + '\' is ' + color('skipping the minifying', 'YELLOW'));
		
	} else {
		
		log.info('\'' + color('minify:scss', 'CYAN') + '\' is ' + color('minifying scss', 'YELLOW'));
		
		task = task
			.pipe(debug({
				title: '\'' + color('compile:scss', 'CYAN') + '\' minified'
			}))
			.pipe(cleanCSS({compatibility: '*'}))
			.pipe(rename({suffix: '.min'}))
		
	}
	
	if (configSCSS.maps === false) {
		log.info('\'' + color('compile:scss', 'CYAN') + '\' is ' + color('skipping sourcemaps', 'YELLOW'));
	} else {
		
		log.info('\'' + color('compile:scss', 'CYAN') + '\' is ' + color('writing sourcemaps', 'YELLOW'));
		
		task = task.pipe(sourcemaps.write('.', {
			sourceRoot: configSCSS.input.path
		}));
		
	}
	
	return task
		.pipe(gulp.dest(configSCSS.output.path))
		.pipe(debug({
			title: '\'' + color('compile:scss', 'CYAN') + '\' saved'
		}))
		.pipe(browserSync.stream({
			match: configSCSS.output.path + '**/*.css',
			once: true
		}));
	
});

gulp.task('build:scss', (callback) => {
	
	gulpSequence('clean:scss', 'compile:scss')(callback);
	
});

gulp.task('watch:scss', ['build:scss'], () => {
	
	return gulp.watch(configSCSS.input.path + configSCSS.input.name + '.scss', ['watchSeq:scss']);
	
});

gulp.task('watchSeq:scss', (callback) => {
	
	gulpSequence('build:scss', 'browserSync:reload')(callback);
	
});

/*
 * JS TASKS:
 *  - clean
 *  - compile
 *  - build
 *  - watch (+ watchSeq)
 */

gulp.task('clean:js', () => {
	
	let srcs = [
		configJS.output.path + configJS.output.name + '.js',
		configJS.output.path + configJS.output.name + '.map'
	];
	
	if (configJS.uglify !== false) {
		srcs.push(configJS.output.path + configJS.output.name + '.min.js');
		srcs.push(configJS.output.path + configJS.output.name + '.min.map');
	}
	
	return gulp.src(srcs, {read: false})
		.pipe(plumberNotifier())
		.pipe(debug({
			title: '\'' + color('clean:js', 'CYAN') + '\' is removing'
		}))
		.pipe(rimraf());
	
});

gulp.task('compile:js', () => {
	
	log.info('\'' + color('compile:js', 'CYAN') + '\' source is ' + color(configJS.input.path + configJS.input.name + '.js', 'BLUE'));
	
	let task = gulp.src(configJS.input.path + configJS.input.name + '.js')
		.pipe(plumberNotifier());
	
	if (configJS.maps !== false) {
		task = task.pipe(sourcemaps.init());
	}
	
	if (configJS.jshint === false) {
		
		log.info('\'' + color('compile:js', 'CYAN') + '\' is ' + color('leaving jshint OFF', 'YELLOW'));
		
	} else {
		
		log.info('\'' + color('compile:js', 'CYAN') + '\' is ' + color('turning jshint ON', 'YELLOW'));
		
		task = task
			.pipe(jshint())
			.pipe(jshint.reporter('default', {verbose: true}));
		
	}
	
	if (configJS.babel === false) {
		
		log.info('\'' + color('compile:js', 'CYAN') + '\' is ' + color('skipping babel', 'YELLOW'));
		
	} else {
		
		log.info('\'' + color('compile:js', 'CYAN') + '\' is ' + color('using babel', 'YELLOW'));
		
		task = task
			.pipe(babel())
			.pipe(rename(function (opt) {
				
				opt.basename = opt.basename.replace(/\.es6$/, '');
				return opt;
				
			}))
			.pipe(debug({
				title: '\'' + color('compile:js', 'CYAN') + '\' used babel on'
			}));
		
	}
	
	// adding js plugin files after compiling done on custom js files
	if (!configJS.includes || !configJS.includes.length) {
		
		log.info('\'' + color('compile:js', 'CYAN') + '\' is ' + color('not including any extra files', 'YELLOW'));
		
	} else {
		
		log.info('\'' + color('compile:js', 'CYAN') + '\' is ' + color('adding extra files', 'YELLOW'));
		
		task = task
			.pipe(addsrc.prepend(configJS.includes));
		
	}
	
	if (configJS.concat === false) {
		
		log.info('\'' + color('compile:js', 'CYAN') + '\' is ' + color('not concatenating the files into one', 'YELLOW'));
		
	} else {
		
		log.info('\'' + color('compile:js', 'CYAN') + '\' is ' + color('concatenating the files', 'YELLOW'));
		
		if (configJS.output.name === '*') {
			configJS.output.name = 'concat';
		}
		
		task = task
			.pipe(debug({
				title: '\'' + color('compile:js', 'CYAN') + '\' concatenated'
			}))
			.pipe(concat(configJS.output.name + '.js'));
		
	}
	
	if (configJS.uglify === false) {
		
		log.info('\'' + color('compile:js', 'CYAN') + '\' is ' + color('not uglifying the files', 'YELLOW'));
		
	} else {
		
		log.info('\'' + color('compile:js', 'CYAN') + '\' is ' + color('uglifying the files', 'YELLOW'));
		
		task = task
			.pipe(uglify())
			.pipe(rename({suffix: '.min'}))
			.pipe(debug({
				title: '\'' + color('compile:js', 'CYAN') + '\' is uglifying'
			}))
		
	}
	
	if (configJS.maps === false) {
		log.info('\'' + color('compile:js', 'CYAN') + '\' is ' + color('skipping sourcemaps', 'YELLOW'));
	} else {
		
		log.info('\'' + color('compile:js', 'CYAN') + '\' is ' + color('writing sourcemaps', 'YELLOW'));
		
		task = task.pipe(sourcemaps.write('.', {
			sourceRoot: configSCSS.input.path
		}));
		
	}
	
	return task
		.pipe(gulp.dest(configJS.output.path))
		.pipe(debug({
			title: '\'' + color('compile:js', 'CYAN') + '\' saved '
		}))
		.pipe(browserSync.stream({
			match: configJS.output.path + configJS.output.name + '.js',
			once: true
		}));
	
});

gulp.task('build:js', (callback) => {
	
	gulpSequence('clean:js', 'compile:js')(callback);
	
});

gulp.task('watch:js', ['build:js'], () => {
	
	return gulp.watch(configJS.input.path + configJS.input.name + '.js', ['watchSeq:js']);
	
});

gulp.task('watchSeq:js', (callback) => {
	
	gulpSequence('build:js', 'browserSync:reload')(callback);
	
});

/*
 * BROWSER SYNC TASKS:
 *  - start
 *  - reload
 */

gulp.task('browserSync:start', (callback) => {
	
	browserSync.init(config.browserSync);
	
	callback();
	
});

gulp.task('browserSync:reload', (callback) => {
	
	browserSync.reload();
	
	callback();
	
});

/*
 * GLOBAL TASKS:
 *  - build
 *  - watch
 *  - start
 *  - default
 */

gulp.task('build', ['copy:fonts', 'build:scss', 'build:js']);

gulp.task('watch', ['copy:fonts', 'watch:scss', 'watch:js']);

gulp.task('start', ['watch', 'browserSync:start']);

gulp.task('default', ['build']);