<?php
use Cake\Routing\Route\InflectedRoute;
use Cake\Routing\Router;

Router::defaultRouteClass(InflectedRoute::class);

Router::plugin('AssetManager', function ($routes) {
  $routes->fallbacks(InflectedRoute::class);
});