$(function(){

	var jcropApi;
	
	var $preview = $("<img>");
	$preview.attr("src", $("#jcrop").attr("src"));
	$("#jcrop_preview").append($preview);

	var ratio = null;
	
	var previewWidth = $("#jcrop_preview").width();
	var previewHeight = $("#jcrop_preview").height();

	var imageWidth = $("#jcrop").width();
	var imageHeight = $("#jcrop").height();

	var originalWidth = $("input[name='crop_image_width']").val();
	var originalHeight = $("input[name='crop_image_height']").val();

	var x = parseFloat(imageWidth) / parseFloat(originalWidth) * $("input[name='crop_x']").val();
	var y = parseFloat(imageHeight) / parseFloat(originalHeight) * $("input[name='crop_y']").val();

	var w = parseFloat(imageWidth) / parseFloat(originalWidth) * $("input[name='crop_width']").val();
	var h = parseFloat(imageHeight) / parseFloat(originalHeight) * $("input[name='crop_height']").val();

	if ($("input[name='crop_ratio']").length) {
		ratio = $("input[name='crop_ratio']").val();
	}

	$('#jcrop').Jcrop({
		onChange: showPreview,
		onSelect: showPreview,
  	aspectRatio: ratio
	}, function() {
		jcropApi = this;

		$("input[name='crop_display_width']").val(imageWidth);
		$("input[name='crop_display_height']").val(imageHeight);

		if (w + x > 0 && h + y > 0) {
			this.setSelect([x, y, w + x, h + y]);
		}		
		
	});

  function showPreview(coords)
  {
    if (parseInt(coords.w) > 0)
    {
			
			$("input[name='crop_x']").val(coords.x);
			$("input[name='crop_y']").val(coords.y);
			
			$("input[name='crop_width']").val(coords.w);
			$("input[name='crop_height']").val(coords.h);
			
      var rx = previewWidth / coords.w;
      var ry = previewHeight / coords.h;

      $preview.css({
				maxWidth: "none",
				display: "inline",
        width: Math.round(rx * imageWidth) + 'px',
        height: Math.round(ry * imageHeight) + 'px',
        marginLeft: '-' + Math.round(rx * coords.x) + 'px',
        marginTop: '-' + Math.round(ry * coords.y) + 'px'
      });
    }
  }

});