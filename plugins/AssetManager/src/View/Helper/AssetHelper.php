<?php

namespace AssetManager\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper\FormHelper;
use Cake\View\Helper\HtmlHelper;
use Cake\View\View;

class AssetHelper extends \Cake\View\Helper {

	public function scripts($action) {
		$files['upload'] = array(
			'AssetManager.jquery.ui.widget.js',
			'AssetManager.jquery.fileupload.js',
			'AssetManager.jquery.iframe-transport.js',
			'AssetManager.jquery.knob.js',
			'AssetManager.script.js',
		);
		$files['jcrop'] = array(
			'AssetManager.jquery.jcrop.min',
			'AssetManager.crop.js',
		);
		
		return $files[$action];
	}
	

	public function crop($asset, $options = array()) {

 		$formHelper = new FormHelper(new View());
		$htmlHelper = new HtmlHelper(new View());
		
		$displayWidth = $options['width'];
		$displayHeight = ($asset->height / $asset->width) * $options['width'];

		$html = $htmlHelper->image(
			$asset->url(['width' => $options['width'], 'crop' => 0, 'cache' => 0]),
			array('id' => 'jcrop', 'width' => $displayWidth, 'height' => $displayHeight
		));
		
		$html .= $formHelper->hidden('crop_x', ['value' => $asset->crop_x]);
		$html .= $formHelper->hidden('crop_y', ['value' => $asset->crop_y]);
		$html .= $formHelper->hidden('crop_width', ['value' => $asset->crop_width]);
		$html .= $formHelper->hidden('crop_height', ['value' => $asset->crop_height]);
		
		$html .= $formHelper->hidden('crop_image_width', ['value' => $asset->width]);
		$html .= $formHelper->hidden('crop_image_height', ['value' => $asset->height]);			
		
		$html .= $formHelper->hidden('crop_display_width');
		$html .= $formHelper->hidden('crop_display_height');

		$options = array_merge($asset->options, $options);

		if (! empty($options['ratio'])) {
			$html .= $formHelper->hidden('crop_ratio', array('default' => $options['ratio']));
		} else {
			$html .= $formHelper->hidden('crop_ratio', array('default' => 24.5/9 ));
		}
		
		return $html;
	}
	
	public function cropPreview($asset, $options = array()) {
		
		$options = array_merge($asset->options, $options);
		
		if (empty($options['ratio'])) {
			return '';
		}
		return '<div id="jcrop_preview" style="width:' . $options['width'] . 'px;height:' . round($options['width'] / $options['ratio']) . 'px;overflow:hidden;"></div>';
	}

	public function upload() {
		$formHelper = new FormHelper(new View());

		$html = $formHelper->create(null, array('id' => 'upload', 'type' => 'file'));
		$html .= '<div id="drop"><p>Drop Files Here or</p><a class="btn btn-primary">Browse</a>';
		$html .= $formHelper->file('resource.upload', array('multiple'));
		$html .= '</div>';
		$html .= '<ul></ul>';
		$html .= $formHelper->end();

		return $html;
	}
	
}

/*
App::uses('Helper', 'View');
App::uses('HtmlHelper', 'View/Helper');

class AssetHelper extends AppHelper {
	
	public $Asset;
	
	public function __construct($View, $settings = array()) {
		$this->Asset = ClassRegistry::init('AssetManager.Asset');
		parent::__construct($View, $settings);
	}
	
	public function data($model, $id) {
		$asset = $this->Asset->findByModelAndModelId($model, $id);
		
		if (! empty($asset))
			unset($asset['Asset']['asset']);
		
		return $asset;
	}
	
	protected function _getAssetId($asset) {
		if (is_array($asset) && !empty($asset['id']))
			return $asset['id'];
		elseif(is_numeric($asset))
			return $asset;
		else
			return 'NO ID';
	}
	
	protected function _getAsset($asset) {
		if (is_array($asset) && !empty($asset['id']))
			return $asset;
		elseif(is_numeric($asset)) {
			$a = $this->Asset->findById($asset);
			if(!empty($a['Asset']['id']))
				return $a;
		}
		
		return 'NO ASSET';
	}
	
	public function fileExists($id) {
		return ($this->Asset->fileExists($id)) ? true : false;
	}
	
	public function cacheExists($id, $resize) {
		return ($this->Asset->cacheExists($id, $resize)) ? true : false;
	}
	

	
	//----------------------------------------------------------------------------------------------------------------------LINK-----
	
	public function link($title, $id, $options = array(), $confirmMessage = false) {
		$htmlHelper = new HtmlHelper(new View());
		
		$route = array('plugin' => 'asset_manager', 'controller' => 'assets', 'action' => 'file', $id);
		
		if (! empty($this->request->prefix)) {
			$route[$this->request->prefix] = false;
		}
		
		return $htmlHelper->link($title, $route, $options, $confirmMessage);
	}
	
	//----------------------------------------------------------------------------------------------------------------------IMAGE-----

	public function image($asset, $resize = array(), $helperOptions = array()) {
		
		if (! isset($resize['cache']))
			$resize['cache'] = true;
		
		$htmlHelper = new HtmlHelper(new View());
		$id = $this->_getAssetId($asset);
		
		
		if ($resize['cache'] && $this->Asset->fileExists($id)) {
			
			if (! $this->Asset->cacheExists($id, $resize)) {
				$resize['file'] = $this->Asset->cachePath($id, $resize, true);
				$asset = $this->_getAsset($id);
				
				$this->Asset->image($asset['Asset'], $resize);
			}
			
			$url = $this->Asset->cachePath($id, $resize);
		}
		elseif ($this->Asset->fileExists($id)) {
			$route = $this->Asset->fileRoute($id);
			$route = array_merge($route, $resize);
			return $htmlHelper->image($route, $helperOptions);
		}
		elseif(isset($resize['noImage'])) {
			$url = $resize['noImage'];
		}
		else {
			return false;
		}

		return $htmlHelper->image($url, $helperOptions);
	}
	
//----------------------------------------------------------------------------------------------------------------------SCRIPT-----
	

	
	//----------------------------------------------------------------------------------------------------------------------UPLOAD-----
	
	public function upload() {
		$formHelper = new FormHelper(new View());
		
		$route = array('plugin' => 'asset_manager', 'controller' => 'assets', 'action' => 'upload');
		
		$prefixes = Router::prefixes();
    foreach($prefixes as $prefix)
      $route[$prefix] = false;		
		
		$html = $formHelper->create(null, array('id' => 'upload', 'type' => 'file'));
		$html .= '<div id="drop"><p>Drop Files Here or</p><a class="btn btn-primary">Browse</a>';
		$html .= $formHelper->file('upload', array('multiple'));
		$html .= '</div>';
		$html .= '<ul></ul>';
		$html .= $formHelper->end();
		
		return $html;
	}
	

	//----------------------------------------------------------------------------------------------------------------------CROP-----
	
	public function crop($asset, $options = array()) {

 		$formHelper = new FormHelper(new View());
		
		$asset = $this->_getAsset($asset);
		$asset = $asset['Asset'];
		
		$displayWidth = $options['width'];
		$displayHeight = ($asset['height'] / $asset['width']) * $options['width'];

		$html = $this->image(
			$asset['id'],
			array('width' => $options['width'], 'crop' => 0, 'cache' => 0),
			array('id' => 'jcrop', 'width' => $displayWidth, 'height' => $displayHeight
		));
		
		$html .= $formHelper->hidden('crop_id', array('default' => $asset['id']));
		$html .= $formHelper->hidden('crop_x', array('default' => $asset['crop_x']));
		$html .= $formHelper->hidden('crop_y', array('default' => $asset['crop_y']));
		$html .= $formHelper->hidden('crop_width', array('default' => $asset['crop_width']));
		$html .= $formHelper->hidden('crop_height', array('default' => $asset['crop_height']));
		
		$html .= $formHelper->hidden('crop_image_width', array('default' => $asset['width']));
		$html .= $formHelper->hidden('crop_image_height', array('default' => $asset['height']));			
		
		$html .= $formHelper->hidden('crop_display_width');
		$html .= $formHelper->hidden('crop_display_height');		

		if (! empty($options['ratio'])) {
			$ratio = $options['ratio'];
		}
		else {
			$Model = ClassRegistry::init($asset['model']);
			$ratio = $Model->assetRatio();
		}
		
		if (! empty($ratio)) {
			$html .= $formHelper->hidden('crop_ratio', array('default' => $ratio));
		}

		return $html;
	}
	
	public function crop_preview($asset, $options = array()) {
		$asset = $this->_getAsset($asset);
		$asset = $asset['Asset'];
		
		if (empty($options['ratio'])) {
			$Model = ClassRegistry::init($asset['model']);
			$options['ratio'] = $Model->assetRatio();
		}
		
		$html = '<div id="jcrop_preview" style="width:' . $options['width'] . 'px;height:' . round($options['width'] / $options['ratio']) . 'px;overflow:hidden;">';
		$html .= '</div>';
		
		return $html;
	}	
	
}
*/