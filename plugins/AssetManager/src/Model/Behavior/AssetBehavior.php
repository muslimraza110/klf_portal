<?php

namespace AssetManager\Model\Behavior;

use Cake\Utility\Inflector;
use Cake\ORM\TableRegistry;

class AssetBehavior extends \Cake\ORM\Behavior {
	
	protected $_defaultConfig = [
		'hasOne' => [],
		'hasMany' => []
	];
	
	public function initialize(array $config) {
		$parts = explode('\\', get_class($this->_table));
		$className = array_pop($parts);
		$this->config('className', $className);

		$config = $this->config();
		$associationTypes = ['hasOne', 'hasMany'];
		
		foreach($associationTypes as $associationType) {
			foreach($config[$associationType] as $key => $options) {
				$this->_table->$associationType($key, [
					'className' => 'AssetManager.Assets',
					'foreignKey' => "model_id",
					'conditions' => [
						"$key.model" => $className,
						"$key.association" => $key,
					],
					'dependent' => true
				]);
			}
		}
	}

	public function beforeSave(\Cake\Event\Event $event, $entity, $options) {
		
		$config = $this->config();
		
		foreach($config['hasOne'] as $key => $options) {
			$uKey = Inflector::underscore($key);

			if (! empty($entity->$uKey->upload)) {
				$upload = $entity->$uKey->upload;

				$entity->$uKey->model = $this->config('className');
				$entity->$uKey->model_id = $entity->id;
				$entity->$uKey->association = $key;
				$entity->$uKey->name = $upload['name'];
				$entity->$uKey->size = $upload['size'];
				$entity->$uKey->type = $upload['type'];
			}
		}
	}
	
	public function afterSave(\Cake\Event\Event $event, $entity, $options) {
		
    $Assets = TableRegistry::get('AssetManger.Assets');
		$config = $this->config();
		
		foreach($config['hasOne'] as $key => $options) {
			$uKey = Inflector::underscore($key);
			
			if (! empty($entity->$uKey->upload)) {
				$entity->$uKey->upload($options);
				$entity->$uKey->re_save_entity = true;
				//$Assets->save($entity->$uKey);
			}
			
			if (isset($entity->$uKey) && !empty($entity->$uKey) && is_object($entity->$uKey) && isset($options['type']) && $options['type'] == 'image') {
				if (is_null($entity->$uKey->crop_x) || is_null($entity->$uKey->crop_y) || is_null($entity->$uKey->crop_width) || is_null($entity->$uKey->crop_height)) {
					$entity->$uKey->crop_x = 0;
					$entity->$uKey->crop_y = 0;
					$entity->$uKey->crop_width = $entity->$uKey->width;
					$entity->$uKey->crop_height = $entity->$uKey->height;
					
					if (isset($options['ratio']) && is_numeric($options['ratio']) && $options['ratio'] !== 0) {
						$entity->$uKey->crop_height = round($entity->$uKey->width / $options['ratio']);
						
						if ($entity->$uKey->crop_height > $entity->$uKey->height) {
							$entity->$uKey->crop_height = $entity->$uKey->height;
							$entity->$uKey->crop_width = $entity->$uKey->height * $options['ratio'];
						}
					}
					
					$entity->$uKey->re_save_entity = true;
				}
			}
			
			if (isset($entity->$uKey->re_save_entity) && $entity->$uKey->re_save_entity === true) {
				$Assets->save($entity->$uKey);
				$entity->$uKey->clearCache();
			}
		}
	}
	
	public function options($association) {
		$config = $this->config();
		$associationTypes = ['hasOne', 'hasMany'];
		
		foreach($associationTypes as $associationType) {
			if (isset($config[$associationType][$association])) {
				return $config[$associationType][$association];
			}
		}
		return [];
	}

}
/*

	public function afterDelete(Model $Model) {
		$this->Asset->deleteAll(array(
			'Asset.model' => $Model->alias,
			'Asset.model_id' => $Model->id
		));
	}
	

	private function _allowedExtensions(Model $Model) {
		$fileType = $this->settings[$Model->alias]['fileType'];
		
		if (! empty($fileType)) {
			if ($fileType == 'image') {
				return array('jpg', 'jpeg', 'png');
			}
		}
		return array();
	}

}
*/
