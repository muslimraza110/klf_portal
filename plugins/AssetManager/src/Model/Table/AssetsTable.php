<?php
namespace AssetManager\Model\Table;

use Aws\S3\S3Client;

class AssetsTable extends \Cake\ORM\Table {

  public function initialize(array $config) {
    parent::initialize($config);

    $this->displayField('name');
    $this->addBehavior('Timestamp');

  }

  public function mimeTypes($type = 'all') {

    // Just basic MIME types
    $mimeTypes = [
      'webImage' => [
        'image/jpeg',
        'image/png',
        'image/gif'
      ],
      'pdf' => [
        'application/pdf'
      ],
      'excel' => [
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      ],
      'word' => [
        'application/msword',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
      ],
      'archive' => [
        'application/zip',
        'application/x-compressed-zip',
        'application/x-rar-compressed'
      ],
      'audio' => [
        'audio/wav',
        'audio/mpeg',
        'audio/vorbis',
        'audio/x-wav'
      ],
      'video' => [
        'video/avi',
        'video/mpeg',
        'video/msvideo',
        'video/x-msvideo'
      ],
      'text' => [
        'text/plain'
      ],
      'powerpoint' => [
        'application/vnd.ms-powerpoint',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation'
      ]
    ];

    if (strtolower($type) == 'all')
      return $mimeTypes;

    if (isset($mimeTypes[$type]))
      return $mimeTypes[$type];

    return [];

  }

	public function s3Client() {
		$credentials = \Cake\Core\Configure::read('AWS.S3.credentials');

		$s3Options = [
			'version' => '2006-03-01',
			'region' => 'us-east-1',
			'http'    => [
				'verify' => false
			]
		];

		if (! empty($credentials))
			$s3Options['credentials'] = $credentials;

		$s3Client = new S3Client($s3Options);

		return $s3Client;

	}

	public function s3Bucket() {

		return \Cake\Core\Configure::read('AWS.S3.bucket');

	}

	public function awsUploadFile($file) {

		$keyname = $file->id;
		$filepath = $file->upload['tmp_name'];

		$s3 = $this->s3Client();

		$result = $s3->putObject(array(
			'Bucket' => $this->s3Bucket(),
			'Key'    => $keyname,
			'SourceFile' => $filepath,
			'ContentType' => $file->upload['type'],
			'ACL'    => 'public-read'
		));

	}

}
