<?php
namespace AssetManager\Model\Entity;

use Aws\S3\Exception\S3Exception;
use Cake\Filesystem\Folder;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Helper\HtmlHelper;
use Cake\Filesystem\File;


class Asset extends \Cake\ORM\Entity {

	protected function _getExtension() {
		if (empty($this->name))
			return '';
		
		$parts = explode('.', strtolower($this->name));
		return array_pop($parts);
	}

	protected function _getRealPath() {
		if(empty($this->id))
			return;
		
		$path = ROOT . DS . 'assets';
		$folder = new Folder($path);
		
		if (empty($folder->path)) {
			$folder->create($path);
		}

		return sprintf('%s%s%s',
				$path,
				DS,
				$this->_getSafeFileName()
		);
	}

	protected function _getSafeFileName() {
		if(empty($this->id))
			return;
		
		$file = new File(
				sprintf('%s%sassets%s%s',
						ROOT,
						DS,
						DS,
						$this->name
				));

		return sprintf('%s-%d.%s',
				$file->safe($file->name(), $file->ext()),
				$this->id,
				$file->ext()
		);

	}
	
	protected function _getCacheDir() {
		return WWW_ROOT . 'img' . DS . 'cache' . DS;
	}
	
	protected function _getCachePath($resize = array(), $realPath = false) {
		if(empty($this->id))
			return;
		
		$options = '';
		
		if(isset($resize['width']))
			$options .= '_w' . $resize['width'];
		
		if(isset($resize['height']))
			$options .= '_h' . $resize['height'];
		
		if(isset($resize['crop']))
			$options .= '_c' . $resize['crop'];

		if (isset($resize['square']))
			$options .= '_sq'.$resize['square'];
		
		if($realPath === true) {
			$dir = $this->_getCacheDir();
			
			$folder = new Folder();
			$folder->create($dir);

			return $dir . $this->id . $options . '.jpg';
		}
		else {
			return Router::url('/', true) . 'img/cache/' . $this->id . $options . '.jpg';
		}
	}
	
	protected function _getRoute() {
		if(empty($this->id))
			return;

		if(!$this->fileExists())
			throw new NotFoundException('The asset does not exist.');
		
		$route = ['plugin' => 'AssetManager', 'prefix' => false, 'controller' => 'Assets', 'action' => 'image', $this->id];

		return $route;
	}
	
	protected function _getOptions() {
		if(empty($this->model))
			return;
		
		$Table = TableRegistry::get(str_replace('Table', '', $this->model));
		return $Table->options($this->association);
	}

	public function icon() {

		$mimeTypes = TableRegistry::get('AssetManager.Assets')->mimeTypes();

		$category = 'file';

		foreach ($mimeTypes as $mimeCategory => $mimeList)
			if (in_array($this->type, $mimeList)) {

				$category = $mimeCategory;
				break;

			}

		switch ($category) {

			case 'file':
				$fontAwesomeIcon = 'fa-file-o';
				break;

			case 'webImage':
				$fontAwesomeIcon = 'fa-picture-o';
				break;

			default:
				$fontAwesomeIcon = 'fa-file-'.$category.'-o';
				break;

		}

		return sprintf(
				'<i class="fa %s"></i>',
				$fontAwesomeIcon
		);

	}


	
	public function isImage() {

		if (in_array(
				$this->type,
				TableRegistry::get('AssetManager.Assets')->mimeTypes('webImage')
		))
			return true;

		return false;

	}

	public function path() {

		return $this->_getRealPath();

	}

	public function relativeUrl() {

		$this->_updateS3Object();

		return Router::url('assets'. DS . $this->_getSafeFileName(), true) ;

	}

	public function saveS3Object() {
		//gets single asset
		$Assets = TableRegistry::get('AssetManager.Assets');

		$s3 = $Assets->s3Client();
		try {

			$result = $s3->getObject([
				'Bucket'=> $Assets->s3Bucket(),
				'Key'=> $this->id,
				'SaveAs' => $this->_getRealPath()
			]);

			return $result;
		}
		catch (S3Exception $e) {
			return false;
		}

	}

	protected function _deleteAllVersions() {

		$path = ROOT.DS.'assets';

		$assetFolder = new Folder($path);

		$assets = $assetFolder->find('.*-'.$this->id.'\.\w{1,4}$');

		foreach ($assets as $asset) {
			@unlink($path.DS.$asset);
		}

	}

	protected function _updateS3Object() {

		if (
			!$this->fileExists()
			|| $this->modified->format('U') > filemtime($this->_getRealPath())
		) {

			// Delete (old) local versions of file
			$this->_deleteAllVersions();
			$this->clearCache();

			return $this->saveS3Object();

		}

		return true;

	}

	public function url($resize = array(), $fullbase = true) {

		$this->_updateS3Object();

		if (! isset($resize['cache']))
			$resize['cache'] = true;

		if ($this->isImage() && $resize['cache'] && $this->fileExists()) {

			if (! $this->cacheExists($resize)) {
				$resize['file'] = $this->_getCachePath($resize, true);
				$this->image($resize);
			}
			
			$route = $this->_getCachePath($resize);
		}
		elseif ($this->fileExists()) {
			$route = $this->_getRoute();
			$route = array_merge($route, $resize);
		}
		elseif(isset($resize['noImage'])) {
			$route = $resize['noImage'];
		}
		else
			return false;

		return Router::url($route, $fullbase);
	}
	
	public function fileExists() {
		$path = $this->_getRealPath();

		if (file_exists($path))
			return true;
		
		return false;
	}	
	
	public function cacheExists($resize = array()) {
		return file_exists($this->_getCachePath($resize, true));		
	}
	
	public function clearCache() {
		$dir = $this->_getCacheDir();
		foreach(glob($dir . $this->id . "_*") as $filename) {
			unlink($filename);
		}	
	}
	
	public function crop($data) {

		if(
			isset($data['crop_display_width']) && isset($data['crop_display_height']) &&
			isset($data['crop_x']) && isset($data['crop_y']) &&
			isset($data['crop_width']) && isset($data['crop_height'])
		) {
	
			$display_width = $data['crop_display_width'];
			$display_height = $data['crop_display_height'];
	
			$this->crop_x = round($this->width / $display_width * $data['crop_x']);
			$this->crop_y = round($this->height / $display_height * $data['crop_y']);
			
			$this->crop_width = round($this->width / $display_width * $data['crop_width']);
			$this->crop_height = round($this->height / $display_height * $data['crop_height']);		
			
			$Assets = TableRegistry::get('AssetManager.Assets');
			
			$Assets->save($this);
			$this->clearCache();
		}
	}

	public function upload($options) {

		/* NEED TO MOVE TO BEHAVIOUR beforeSave()
		if (empty($upload) || $upload['error'] == 4) {
			$this->validationErrors[] = 'No file was specified';
		}

		if ($upload['error'] != 0)
			$this->validationErrors[] = "An upload error has occurred";
		
		$extension = pathinfo($upload['name'], PATHINFO_EXTENSION);
		if (! empty($extensions)) {			
			if (! in_array(strtolower($extension), $extensions)) {
				$this->validationErrors[] = "An invalid file has been uploaded";
			}
		}

		if (! empty($this->validationErrors)) {
			return false;
		}
		*/
		
		ini_set('memory_limit', '-1');
		
		$this->clearCache();
		
		$uploadPath = $this->upload['tmp_name'];

		TableRegistry::get('AssetManager.Assets')->awsUploadFile($this);
		
		if ($this->isImage()) {
			list($width, $height) = getimagesize($uploadPath);

			$this->width = $width;
			$this->height = $height;
			
			/*
			if (! empty($options['maxSize'])) {
				$path = CACHE . String::uuid();
				$settings['maxSize']['file'] = $path;
				$this->image($upload, $settings['maxSize']);
				$upload['asset'] = file_get_contents($path);
			}
			*/

			if (! empty($options['ratio'])) {
				$this->crop_x = 0;
				$this->crop_y = 0;
				$this->crop_width = $this->width;
				$this->crop_height = round($this->width / $options['ratio']);
				
				if ($this->crop_height > $this->height) {
					$this->crop_height = $this->height;
					$this->crop_width = $this->height * $options['ratio'];
				}				
			}			
		}		
	}	
	

	public function image($options = array()) {

		$this->_updateS3Object();

		ini_set("memory_limit", "-1");
		
		if(isset($options['crop']) && ($options['crop'] !== true && $options['crop'] != 1))
			$crop = false;
		else
			$crop = true;

		$path = $this->_getRealPath();
		$current = [
			'img' => imagecreatefromstring(file_get_contents($path)),
			'type' => $this->type
		];
		
		if ((empty($this->crop_width) && empty($this->crop_height)) || $crop !== true) {
			$current['width'] = imagesx($current['img']);
			$current['height'] = imagesy($current['img']);
		}
		else {
			$current['width'] = $this->crop_width;
			$current['height'] = $this->crop_height;
		}

		$current['ratio'] = round($current['width'] / $current['height'], 2);

		// Set Resize Array		
		$resize = array();
		if (isset($options['width']))
			$resize['width'] = $options['width'];
		if (isset($options['height']))
			$resize['height'] = $options['height'];

		if (isset($options['square']))
			$resize['square'] = $options['square'];
		
		if (isset($options['fill'])) {
			$resize['fill'] = $options['fill'];
			$rgb = $this->__hex2rgb($resize['fill']);
		}
		else {
			$rgb = array('r' => 255, 'g' => 255, 'b' => 255);
		}

		/*
		if (isset($options['cropMode']) && $options['cropMode'] == 'fit')
			$resize['cropMode'] = $options['cropMode'];
		else
			$resize['cropMode'] = 'fill';
		*/
		
		// Set New Array
		
		$new = array();
		$new['img'] = null;
		$new['width'] = $current['width'];
		$new['height'] = $current['height'];
		$new['ratio'] = null;
		
		if ($crop === true && !(empty($this->crop_x) && empty($this->crop_y))) {
			$new['src_x'] = $this->crop_x;
			$new['src_y'] = $this->crop_y;
		}
		else {
			$new['src_x'] = 0;
			$new['src_y'] = 0;
		}

		if (isset($resize['square']))
			$new = $this->__resize_to_square($current, $resize, $new);
		else {

			if(isset($resize['height']) && isset($resize['width'])) {
				$new = $this->__resize_to_fit($current, $resize, $new);
			}
			elseif(isset($resize['width'])) {
				$new = $this->__resize_from_width($current, $resize, $new);
			}
			elseif(isset($resize['height'])) {
				$new = $this->__resize_from_height($current, $resize, $new);
			}

		}

		$new['img'] = imagecreatetruecolor($new['width'], $new['height']);

		switch($current['type']) {
			case 'image/jpg':		
			case 'image/jpeg':
			case 'image/pjpeg':
				$fill = imagecolorallocate($new['img'], $rgb['r'], $rgb['g'], $rgb['b']);
				imagefill($new['img'], 0, 0, $fill);
				break;
			
			case 'image/png':
			case 'image/gif':
			case 'image/bmp':
			case 'image/x-windows-bmp':
			case 'image/wbmp':
				imagesavealpha($new['img'], true);
				$transparent = imagecolorallocatealpha($new['img'], 0, 0, 0, 127);
				imagefill($new['img'], 0, 0, $transparent);
				break;
			
			default:
				return false;
		}
		
		imagecopyresampled($new['img'], $current['img'], 0, 0, $new['src_x'], $new['src_y'], $new['width'], $new['height'], $current['width'], $current['height']);
		imagedestroy($current['img']);

		if (isset($resize['square'])) {

			$size = empty($resize['height']) ? $resize['width'] : $resize['height'];

			$new['src_x'] = round(($new['width'] - $size) / 2, 0);
			$new['src_y'] = round(($new['height'] - $size) / 2, 0);

			$current['img'] = $new['img'];

			$new['img'] = imagecreatetruecolor($size, $size);

			imagecopy(
					$new['img'],
					$current['img'],
					0, 0,
					$new['src_x'], $new['src_y'],
					$size, $size
			);

			imagedestroy($current['img']);

		}
		
		if (isset($options['file']))
			$cache = $options['file'];
		else {
			$id = 0;
			
			if (isset($asset['id']))
				$id = $asset['id'];
				
			$cache = $this->_getCachePath($resize, true);
		}
		
		$img = null;
		
		if (empty($options['file']))
			ob_start();
		
		switch($current['type']) {
			case 'image/jpg':		
			case 'image/jpeg':
			case 'image/pjpeg':
				imagejpeg($new['img'], $cache, 100);
				
				if (! isset($options['file']))
					imagejpeg($new['img'], null, 100);
				break;
			
			case 'image/png':
				imagepng($new['img'], $cache, 0);
				
				if (! isset($options['file']))
					imagepng($new['img'], null, 0);
				break;
			
			case 'image/gif':
				imagegif($new['img'], $cache);

				if (! isset($options['file']))
					imagegif($new['img'], null);
				break;
			
			case 'image/bmp':
			case 'image/x-windows-bmp':
				imagewbmp($new['img'], null);
				break;
			
			case 'image/wbmp':
				imagewbmp($new['img'], null, $new['fill']);
				break;
			
			default:
				return false;
		}
		
		if (empty($options['file'])) {
			$img = ob_get_contents();
			ob_end_clean();			
		}
		
		imagedestroy($new['img']);
		return $img;
	}
	
	private function __resize_from_width($current, $resize, $new = array()) {
		$new['width'] = $resize['width'];
		$new['height'] = ($new['width'] * $current['height']) / $current['width'];
		
		return $new;
	}
	
	private function __resize_from_height($current, $resize, $new = array()) {
		$new['height'] = $resize['height'];
		$new['width'] = ($new['height'] * $current['width']) / $current['height'];
		
		return $new;
	}
	
	private function __resize_to_fit($current, $resize, $new = array()) {
		
		if (! isset($new['ratio']))
			$new['ratio'] = round($resize['width'] / $resize['height'], 2);

		$new['width'] = $current['width'];
		$new['height'] = $current['height'];
	
		if ($new['width'] > $resize['width']) {
			$new['height'] = $resize['width'] * $current['height'] / $current['width'];
			$new['width'] = $resize['width'];
		}

		if ($new['height'] > $resize['height']) {
			$new['width'] = $resize['height'] * $current['width'] / $current['height'];
			$new['height'] = $resize['height'];
		}

		return $new;
	}

	private function __resize_to_square($current, $resize, array $new = []) {

		$size = empty($resize['height']) ? $resize['width'] : $resize['height'];

		$aspectRatio = $current['width'] / $current['height'];

		if ($aspectRatio > 1) {
			// Landscape

			$new['width'] = round($size * $aspectRatio, 0);
			$new['height'] = $size;

		} else {
			// Portrait (or square)

			$new['width'] = $size;
			$new['height'] = round($size / $aspectRatio, 0);

		}

		return $new;

	}
		
	
	/*
	private function __resize_to_fill($current, $resize, $new = array()) {
		
		$current['ratio'] = round($current['width'] / $current['height'], 2);
		$new['ratio'] = round($resize['width'] / $resize['height'], 2);
		
		if ($current['ratio'] > $new['ratio']) {
			//full height, crop horizontally
			$new['height'] = $resize['height'];
			$new['width'] = $resize['height'] * $new['ratio'];
			$new['src_x'] = ($current['width'] - $new['width']) /2;
		
		} else {
			//full width, crop vertically
			$new['width'] = $resize['width'];
			$new['height'] = $resize['width'] / $new['ratio'];
			$new['src_y'] = ($current['height'] - $new['height']) /2;
		}

		return $new;
	}
	*/
	
	private function __hex2rgb($hex) {
		$hex = str_replace("#", "", $hex);
		
		if(strlen($hex) == 3) {
			$r = hexdec(substr($hex,0,1).substr($hex,0,1));
			$g = hexdec(substr($hex,1,1).substr($hex,1,1));
			$b = hexdec(substr($hex,2,1).substr($hex,2,1));
		} else {
			$r = hexdec(substr($hex,0,2));
			$g = hexdec(substr($hex,2,2));
			$b = hexdec(substr($hex,4,2));
		}
		$rgb = array('r' => $r, 'g' => $g, 'b' => $b);
		
		return $rgb;
	}
}
