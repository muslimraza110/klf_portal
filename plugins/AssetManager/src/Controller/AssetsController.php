<?php
namespace AssetManager\Controller;

class AssetsController extends \App\Controller\AppController {

	public function beforeFilter(\Cake\Event\Event $event) {
		parent::beforeFilter($event);
		
		$this->loadModel('AssetManager.Assets');
		$this->Auth->allow(['image', 'file']);
	}
		
	public function index() {
		$assets = $this->Asset->find('all', array('field' => 'id'));
		$this->set(compact('assets'));
	}
	
	public function upload() {
		$this->autoRender = false;
		
		$this->Asset->upload($this->data['Asset']['upload'], $allowed);
	}

	public function file($id) {
		$this->autoRender = false;
		$asset = $this->__getAsset($id);
		
		$this->response->type($asset['type']);
		$this->response->disableCache();		
		$this->response->download($asset['name']);
		$this->response->body($asset['asset']);
	}

	public function image($id) {
		$this->autoRender = false;

		$asset = $this->Assets->get($id);
		$img = $asset->image($this->request->query);

		$this->response->type($asset->type);
		$this->response->disableCache();
		$this->response->body($img);
	}
	
}