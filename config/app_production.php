<?php
return [
	/**
	 * Debug Level:
	 *
	 * Production Mode:
	 * false: No error messages, errors, or warnings shown.
	 *
	 * Development Mode:
	 * true: Errors and warnings shown.
	 */
	'debug' => false,
	'path' => '/',

	'AWS' => [
		'S3' => [
			'bucket' => 'khoja-assets-2',
			'credentials' => null,
		]
	],

	'App' => [
		'_environment' => 'production',
		'fullBaseUrl' => 'https://portal.khojaleadershipforum.org',
	],

	/**
	 * Email configuration.
	 *
	 * You can configure email transports and email delivery profiles here.
	 *
	 * By defining transports separately from delivery profiles you can easily
	 * re-use transport configuration across multiple profiles.
	 *
	 * You can specify multiple configurations for production, development and
	 * testing.
	 *
	 * ### Configuring transports
	 *
	 * Each transport needs a `className`. Valid options are as follows:
	 *
	 *  Mail   - Send using PHP mail function
	 *  Smtp   - Send using SMTP
	 *  Debug  - Do not send the email, just return the result
	 *
	 * You can add custom transports (or override existing transports) by adding the
	 * appropriate file to src/Network/Email.  Transports should be named
	 * 'YourTransport.php', where 'Your' is the name of the transport.
	 *
	 * ### Configuring delivery profiles
	 *
	 * Delivery profiles allow you to predefine various properties about email
	 * messages from your application and give the settings a name. This saves
	 * duplication across your application and makes maintenance and development
	 * easier. Each profile accepts a number of keys. See `Cake\Network\Email\Email`
	 * for more information.
	 */
	'EmailTransport' => [
		'default' => [
			'_secretId' => 'prod/khoja/ses',
			'className' => 'Smtp',
			// The following keys are used in SMTP transports
			'host' => '',
			'port' => 587,
			'timeout' => 30,
			'username' => '',
			'password' => '',
			'client' => null,
			'tls' => true,
		],
	],

	'Email' => [
		'default' => [
			'transport' => 'default',
			'from' => ['admin@khojaleadershipforum.org' => 'Khoja'],
		],
	],

	/**
	 * Connection information used by the ORM to connect
	 * to your application's datastores.
	 * Drivers include Mysql Postgres Sqlite Sqlserver
	 * See vendor\cakephp\cakephp\src\Database\Driver for complete list
	 */
	'Datasources' => [
		'default' => [
			'_secretId' => 'prod/khoja/rds',
			'className' => 'Cake\Database\Connection',
			'driver' => 'Cake\Database\Driver\Mysql',
			'persistent' => false,
			'host' => '',
			/**
			 * CakePHP will use the default DB port based on the driver selected
			 * MySQL on MAMP uses port 8889, MAMP users will want to uncomment
			 * the following line and set the port accordingly
			 */
			//'port' => 'nonstandard_port_number',
			'username' => '',
			'password' => '',
			'database' => 'khoja',
			'encoding' => 'utf8',
			'timezone' => 'UTC',
			'cacheMetadata' => true,

			/**
			 * Set identifier quoting to true if you are using reserved words or
			 * special characters in your table or column names. Enabling this
			 * setting will result in queries built using the Query Builder having
			 * identifiers quoted when creating SQL. It should be noted that this
			 * decreases performance because each query needs to be traversed and
			 * manipulated before being executed.
			 */
			'quoteIdentifiers' => false,

			/**
			 * During development, if using MySQL < 5.6, uncommenting the
			 * following line could boost the speed at which schema metadata is
			 * fetched from the database. It can also be set directly with the
			 * mysql configuration directive 'innodb_stats_on_metadata = 0'
			 * which is the recommended value in production environments
			 */
			//'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],
		],
	],

	/**
	 *
	 * Session configuration.
	 *
	 * Contains an array of settings to use for session configuration. The
	 * `defaults` key is used to define a default preset to use for sessions, any
	 * settings declared here will override the settings of the default config.
	 *
	 * ## Options
	 *
	 * - `cookie` - The name of the cookie to use. Defaults to 'CAKEPHP'.
	 * - `cookiePath` - The url path for which session cookie is set. Maps to the
	 *   `session.cookie_path` php.ini config. Defaults to base path of app.
	 * - `timeout` - The time in minutes the session should be valid for.
	 *    Pass 0 to disable checking timeout.
	 * - `defaults` - The default configuration set to use as a basis for your session.
	 *    There are four built-in options: php, cake, cache, database.
	 * - `handler` - Can be used to enable a custom session handler. Expects an
	 *    array with at least the `engine` key, being the name of the Session engine
	 *    class to use for managing the session. CakePHP bundles the `CacheSession`
	 *    and `DatabaseSession` engines.
	 * - `ini` - An associative array of additional ini values to set.
	 *
	 * The built-in `defaults` options are:
	 *
	 * - 'php' - Uses settings defined in your php.ini.
	 * - 'cake' - Saves session files in CakePHP's /tmp directory.
	 * - 'database' - Uses CakePHP's database sessions.
	 * - 'cache' - Use the Cache class to save sessions.
	 *
	 * To define a custom session handler, save it at src/Network/Session/<name>.php.
	 * Make sure the class implements PHP's `SessionHandlerInterface` and set
	 * Session.handler to <name>
	 *
	 * To use database sessions, load the SQL file located at config/Schema/sessions.sql
	 */
	'Session' => [
		'defaults' => 'php',
		'timeout ' => 3600 * 3, // 3 hours
//		'handler' => [
//			'engine' => 'DynamoDbSession',
//		],
	],

];