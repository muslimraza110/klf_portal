<?php
namespace Qalius;

use Aws\SecretsManager\SecretsManagerClient;
use Cake\Core\Configure;

class BootstrapHelper {

	protected $_iamRoleFile = ROOT.DS.'.iamrole';

	/**
	 * @return bool
	 */
	public function isEC2() {

		$hypervisorUuid = @file_get_contents('/sys/hypervisor/uuid');
		$sysVendor = @file_get_contents('/sys/class/dmi/id/sys_vendor');

		if (
			strpos(strtolower($hypervisorUuid), 'ec2') === 0
			|| strpos(strtolower($sysVendor), 'ec2') !== false
		) {
			return true;
		}

		return false;

	}

	/**
	 * @param array $config ['EC2Sandbox' => 'app_sandbox', 'EC2Production' => 'app_production']
	 * @param bool $merge
	 * @return bool
	 */
	public function loadConfigurationByRole(array $config, $merge = true) {

		if (file_exists($this->_iamRoleFile)) {
			$iamRole = file_get_contents($this->_iamRoleFile);
		} elseif (!empty($iamRole = @file_get_contents('http://169.254.169.254/latest/meta-data/iam/security-credentials/'))) {
			file_put_contents($this->_iamRoleFile, trim($iamRole));
		}

		if (!empty($iamRole) && !empty($config[$iamRole])) {
			return Configure::load($config[$iamRole], 'default', $merge);
		}

		return false;

	}

}

class SecretsManagerHelper {

	private $_sm = null;

	protected $_smConfigFile = CONFIG.'_app_sm.php';
	protected $_smConfigExpiration = 60 * 5; // 5 min

	/**
	 * @return bool
	 */
	public function hasConfiguration() {

		if (
			!file_exists($this->_smConfigFile)
			|| filemtime($this->_smConfigFile) < (time() - $this->_smConfigExpiration)
		) {
			return false;
		}

		return true;

	}

	/**
	 * @param array $config
	 * @return bool|int
	 */
	public function putConfiguration(array $config) {

		$varExport = var_export($config, true);

		$data = <<<PHP
<?php
return $varExport;
PHP;

		return file_put_contents($this->_smConfigFile, $data);

	}

	/**
	 * @return bool
	 */
	public function loadConfiguration() {

		if (!file_exists($this->_smConfigFile)) {
			return false;
		}

		return Configure::load('_app_sm', 'default', true);

	}

	/**
	 * @param array $args
	 * @return mixed|null
	 */
	public function getSecretValueStringParsed(array $args) {

		$secretsManager = $this->_factory();

		try {

			return json_decode(
				$secretsManager->getSecretValue($args)->get('SecretString')
			);

		} catch (\Exception $e) {
			return null;
		}

	}

	/**
	 * @param array $args
	 * @return SecretsManagerClient|null
	 */
	protected function _factory(array $args = []) {

		if (!empty($this->_sm)) {
			return $this->_sm;
		}

		$idDocument = @file_get_contents('http://169.254.169.254/latest/dynamic/instance-identity/document/');

		preg_match('~"region"\s*:\s*"([-a-zA-Z0-9]+)"~', $idDocument, $matches);

		if (!empty($matches[1])) {

			$args = array_merge([
				'version' => '2017-10-17',
				'endpoint' => 'https://secretsmanager.'.$matches[1].'.amazonaws.com',
				'region' => $matches[1]
			], $args);

		}

		return $this->_sm = new SecretsManagerClient($args);

	}

}