<?php
use Cake\Core\Plugin;
use Cake\Routing\Router;

const DEFAULT_HOMEROUTE = ['controller' => 'Users', 'action' => 'dashboard', 'prefix' => true, 'plugin' => false];
const PREFIXES = [
	'admin' => ['role' => 'A'],
	'founder' => ['role' => 'F'],
	'operating_board' => ['role' => 'O'],
	'user' => ['role' => 'C'],
	'copywriter' => ['role' => 'W'],
	'organizing_committee' => ['role' => 'R'],
	'_' => ['role' => false] //Fake prefix to redirect automatically to the right user prefix (for the CKEditor links)
];

Router::defaultRouteClass('InflectedRoute');

Router::scope('/', function ($routes) {
  $routes->extensions(['json']);
  
  $routes->connect('/', ['controller' => 'Users', 'action' => 'login']);
  $routes->connect('/register', ['controller' => 'Users', 'action' => 'register']);
  $routes->connect('/login', ['controller' => 'Users', 'action' => 'login']);
  $routes->connect('/logout', ['controller' => 'Users', 'action' => 'logout']);
  $routes->connect('/forgot-password', ['controller' => 'Users', 'action' => 'forgot_password_step_1']);
  $routes->connect('/reset-password/*', ['controller' => 'Users', 'action' => 'forgot_password_step_2']);
  $routes->connect('/terms-and-conditions', ['controller' => 'Pages', 'action' => 'terms_and_conditions']);

  $routes->connect('/chat_refresh', ['controller' => 'Chat', 'action' => 'chat_refresh']);

  // $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

  $routes->fallbacks();
});


foreach (PREFIXES as $prefix => $options) {
	
	Router::prefix($prefix, function($routes) use ($prefix, $options) {
		$routes->extensions(['json']);
		
		$homeRoute = (! empty($options['home_route'])) ? $options['home_route'] : DEFAULT_HOMEROUTE;
		
		if (! empty($homeRoute['prefix']) && $homeRoute['prefix'] === true) {
			$homeRoute['prefix'] = $prefix;
		}
		
		$routes->connect('/', $homeRoute);
		
		$routes->connect('/events', ['controller' => 'Events', 'action' => 'index']);
		
		$routes->connect('/forum', ['controller' => 'ForumPosts']);
		$routes->connect('/forum/:action/*', ['controller' => 'ForumPosts']);

		// $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);
		$routes->fallbacks();
	});
	
}

/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();