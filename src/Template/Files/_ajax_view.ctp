<table class="table table-bordered file-list">
	<thead>
		<?= $this->Html->tableHeaders([
			'Preview',
			'Name',
			'Size',
			'Date Created',
			['Actions' => ['class' => 'compact text-center']]
		]); ?>
	</thead>
	<tbody>
	<?php
		if (!$files->isEmpty()) {

			$rows = [];

			foreach ($files as $file) {

				$actions = $this->Form->postLink(
					$icons['delete'] . 'Delete',
					['controller' => 'Files', 'action' => 'delete', $file->id],
					[
						'escape' => false,
						'class' => 'btn btn-xs btn-danger',
						'style' => 'margin-left: 0;',
						'confirm' => sprintf('Do you really want to delete "%s"?', $file->resource->name)
					]
				);
				//'https://s3.us-east-1.amazonaws.com/'.$asset['bucket'].'/' . $asset['assets']->id

//				if ($file->resource->isImage())
//					$thumbnailIcon = $this->Html->image($file->resource->url(['width' => '100']));
//				else
//					$thumbnailIcon = $file->resource->icon();

				if ($file->resource->isImage())
					$thumbnailIcon = $this->Html->image('https://s3.us-east-1.amazonaws.com/'.$bucket.'/' . $file->resource->id, [
						'fullBase' => true,
						'width' => 100,
					]);
				else
					$thumbnailIcon = $file->resource->icon();

				$rows[] = [

					[$thumbnailIcon, ['class' => 'thumb']],

					$this->Html->link(
						$file->resource->name,
						['controller' => 'Files', 'action' => 'download', $file->id],
						['target' => '_blank']
					),

					$this->Number->toReadableSize($file->resource->size),

					$file->resource->created,

					[$actions, ['class' => 'compact']]
				];
			}

			echo $this->Html->tableCells($rows);
		}
	?>
	</tbody>
</table>
