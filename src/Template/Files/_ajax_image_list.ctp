<?php if (!$files->isEmpty()): ?>

	<?php foreach ($files as $file): ?>

		<div class="image-list-item" data-asset-id="<?= $file->resource->id; ?>">
			<span></span>
			<?= $this->Html->image('https://s3.us-east-1.amazonaws.com/'.$bucket.'/' . $file->resource->id, [
				'fullBase' => true,
				'width' => 100,
			]); ?>
		</div>

	<?php endforeach; ?>

<?php else: ?>

	No images found.

<?php endif; ?>
