<?= $this->element('ckeditor'); ?>

<?= $this->Html->scriptBlock("
	
	$('#dialog form').hide();
	$('#reply-box').on('click', function(){
		$(this).hide();
		$('#dialog-box form').fadeIn();
	});

	$('#reply-button').on('click', function(e){
		e.preventDefault();

		$('html, body').animate({
			scrollTop: $('#dialog-box').offset().top
		}, 2000);
	});
	
	$('.quote-post').on('click', function (e) {
		
		var content = $(e.target)
			.parents('.media').first()
			.find('.media-body .content')
			.html();
		
		content = '<div class=\"quote\">' + content + '</div><br>';
		
		$('#reply-box').trigger('click');
		
		CKEDITOR.instances.content.insertHtml(content);
		
	});
	
	$('.submit-cancel').on('click', function (e) {
		
		e.preventDefault();
		
		CKEDITOR.instances.content.setData('');
		
		$('#dialog-box form').hide();
		$('#reply-box').fadeIn();
		
	});
	
	function changeSubscriptions(self, changeFrequency) {
	
		var
			_frequency = $('.forum-frequency')
			_buttonText = $(self).find('.text')
			_modal = $('#modal-subscribe');
		
		if (_buttonText.text().toLowerCase() == 'subscribe' || changeFrequency) {
			
			_modal.modal('show');
			
			_modal.find('.submit').off().on('click', function (e) {
				
				$.post(
					'{$this->Url->build(['action' => 'ajax_toggle_subscription'])}.json',
					{
						id: $(self).data('id'),
						frequency: $('#modal-form-subscribe').find('input[name=\"frequency\"]:checked').val()
					},
					function (response) {
						
						_buttonText.text('Unsubscribe');
						_modal.modal('hide');
						_frequency.text('Frequency: ' + response.frequency);
						_frequency.show();
				
					}
				);
				
			});
			
		} else if (_buttonText.text().toLowerCase() == 'unsubscribe') {
			
			$.post(
				'{$this->Url->build(['action' => 'ajax_toggle_subscription'])}.json',
				{
					id: $(self).data('id')
				},
				function (response) {
				
					_buttonText.text('Subscribe');
					_frequency.hide();
					
				}
			);
			
		}
		
	}
	
	if ($('.forum-subscribe').find('.text').text().toLowerCase() == 'subscribe') {
	
		$('.forum-frequency').hide();
	
	} else {
	
		$('.forum-frequency').show();
	
	}
	
	$('.forum-frequency').off().on('click', function (e) {
		
		e.preventDefault();

		changeSubscriptions($('.forum-subscribe'), true);
		
	});

	$('.forum-subscribe').off().on('click', function (e) {
		
		e.preventDefault();

		changeSubscriptions(this);
		
	});

	
 ", ['block' => true]); ?>
	<div class="row">
		<div class="col-xs-24 col-sm-12">
			<?= $this->element('pagination'); ?>
		</div>
		<div class="col-xs-24 col-sm-12">
			<div class="text-right">
				<?php

				echo $this->Html->link(
					'Frequency: ' . $forumPost->subscriptionType($authUser),
					'#',
					[
						'escape' => false,
						'class' => 'btn btn-info forum-frequency',
						'data-id' => $forumPost->id
					]
				);

				echo $this->Html->link(
					sprintf(
						'%s<span class="text">%s</span>',
						$icons['rss'],
						$forumPost->isSubscribed($authUser) ? 'Unsubscribe' : 'Subscribe'
					),
					'#',
					[
						'escape' => false,
						'class' => 'btn btn-info forum-subscribe',
						'data-id' => $forumPost->id,
						'style' => 'margin-left: 10px;'
					]
				);

				if (
					$authUser->role != 'R'
					&& !($forumPost->visibility == 'A' && $authUser->role == 'C')
				) {
					/*
					echo $this->Html->link($icons['plus'] . 'New', ['action' => 'add'], [
						'class' => 'btn btn-info m-r-xs',
						'escape' => false
					]); */

					echo $this->Html->link($icons['mail-reply'] . 'Reply', '#', [
						'class' => 'btn btn-info',
						'escape' => false,
						'id' => 'reply-button',
						'style' => 'margin-left: 10px;'
					]);
				}
				?>
			</div>
		</div>
	</div>

	<div class="forum-post-info">
		<h3><?= $title; ?></h3>
	</div>
	
	<div class="ibox-content blue-bg forum-container">

		<?php

		if (!$paginated) {
			echo $this->element('forums/forum-view', ['data' => $forumPost]);
		}

		 ?>

		<?php

		if (
			!in_array($forumPost->visibility, ['A', 'S'])
			|| ($forumPost->visibility == 'A' && $authUser->role == 'A')
			|| ($forumPost->visibility == 'S' && in_array($authUser->role, ['A', 'O']))
		) {

			if (!empty($forumPost->children)) {

				foreach ($forumPost->children as $forumComment) {

					echo $this->element('forums/forum-view', ['data' => $forumComment, 'parentForumPost']);

					// if (!empty($forumComment->user)) {
					// 	if ($authUser->role == 'A') {
					// 		echo $this->element('forums/forum-view', ['data' => $forumComment]);
					// 	} else if ($forumComment->user->id == $authUser->id) {
					// 		echo $this->element('forums/forum-view', ['data' => $forumComment]);
					// 	} else if ($forumPost->visibility == 'P') {
					// 		echo $this->element('forums/forum-view', ['data' => $forumComment]);
					// 	}
					// }

				}

			} elseif (isset($forumPosts)) {

				foreach ($forumPosts as $forumComment) {

					echo $this->element('forums/forum-view', ['data' => $forumComment, 'parentForumPost']);

				}

			}

		}
		
		?>
		
	</div>

	<?php if (!($forumPost->visibility == 'A' && $authUser->role == 'C')): ?>

		<div id="dialog" class="ibox-content">
			<div class="row">

				<div class="col-xs-6 col-sm-2">
					<?= $this->Html->image($authUser->profile_thumb, ['class' => 'profile-thumb', 'style' => 'max-width: 100%']); ?>
				</div>

				<div class="col-xs-18 col-sm-22">

					<div id="dialog-box">
						<?php
						echo $this->Form->create(null);
						echo $this->Form->input('parent_id', ['type' => 'hidden', 'value' => $forumPost->id]);
						echo $this->Form->input('user_id', ['type' => 'hidden', 'value' => $authUser->id]);
						echo $this->Form->input('content', ['label' => false, 'type' => 'textarea', 'class' => 'ckeditor']);
						echo $this->Form->submit('Submit Reply', ['class' => 'btn btn-primary pull-right', 'style' => 'margin-left: 10px;']);
						echo $this->Form->button('Cancel', ['class' => 'btn btn-primary pull-right submit-cancel']);
						echo $this->Form->end();
						?>

						<div id="reply-box">
							<?= $icons['commenting']; ?>
							Reply to this topic...
						</div>
					</div>

				</div>

			</div>
		</div>

	<?php else : ?>
		<br>
		<div class="alert alert-info">
			Only Admins can post in this forum.
		</div>

	<?php endif ?>

<?= $this->element('pagination'); ?>

<div class="modal inmodal fade" id="modal-subscribe">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<?= $this->Form->create(null, ['id' => 'modal-form-subscribe']) ?>

			<div class="ibox">
				<div class="ibox-title">
					<h3>Subscription</h3>
				</div>

				<div class="ibox-content no-borders">

					<?= $this->Form->input('frequency', [
						'type' => 'radio',
						'options' => $subscriptionFrequencies,
						'default' => 'I'
					]) ?>

				</div>
			</div>

			<?= $this->Form->end() ?>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary submit">Subscribe</button>
			</div>

		</div>
	</div>
</div>