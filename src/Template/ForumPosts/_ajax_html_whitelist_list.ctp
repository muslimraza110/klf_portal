<?php if (!empty($forumPost->forum_post_whitelists)): ?>

	<ul class="list-group user-list-group">

		<?php foreach ($forumPost->forum_post_whitelists as $whitelist): ?>

			<?php
			if ($whitelist->model == 'GroupsTable') {
				$controller = 'Groups';
				$options = ['class' => 'img-circle', 'style' => 'border-radius: 0px;'];
			} else {
				$controller = 'Users';
				$options = ['class' => 'img-circle'];
			}
			?>

			<li class="list-group-item">
				<?= $this->Html->link(
					sprintf(
						'%s %s',
						$this->Html->image($whitelist->association->profile_thumb, $options),
						$whitelist->association->name
					),
					['controller' => $controller, 'action' => 'view', $whitelist->association->id],
					['escape' => false]
				) ?>

				<?= $this->Html->link(
					'Remove',
					'javascript:;',
					[
						'class' => 'btn btn-sm btn-danger pull-right',
						'onclick' => 'removeWhitelist(this)',
						'data-id' => $whitelist->id
					]
				) ?>
			</li>

		<?php endforeach; ?>

	</ul>

<?php endif; ?>