<?php if (!empty($groupedForumPosts)): ?>
	<?php foreach ($groupedForumPosts as $groupName => $forumPosts): ?>

		<?php foreach ($forumPosts as $forumPost): ?>

			<?php if (! empty($forumPost->user)): ?>
				<div class="vote-item">
					<div class="row">

						<div class="col-sm-18">
							
							<div class="vote-icon pull-left">
								<?= $icons['coffee']; ?>
							</div>

							<div style="padding-left: 50px;">
								<?php
									$name = $forumPost->topic;
	
									if (empty($forumPost->topic)) {
										$name = html_entity_decode(strip_tags($forumPost->content), ENT_QUOTES);
									}
	
									if (!empty($forumPost->pinned))
										$name = sprintf('%s %s', $icons['map-pin'], $name);
	
									echo $this->Html->link(
										$name,
										['controller' => 'ForumPosts', 'action' => 'view', $forumPost->id],
										['escape' => false, 'class' => 'vote-title']
									);
								?>
	
								<div class="vote-info m-b-sm">
									<?php
										$authorUrl = '#';
	
										if (!empty($forumPost->user->contact)) {
											$authorUrl = ['controller' => 'Users', 'action' => 'view', $forumPost->user->id];
										}
										
										echo sprintf('By %s <span class="post-time">%s</span>',
											$this->Html->link(
												$icons['user'] . ' ' . $forumPost->user->name,
												$authorUrl,
												['escape' => false, 'class' => 'post-author']
											),
											$forumPost->created->i18nFormat('EEEE LLLL d, yyyy @ h:mm a')
										);
										
									?>
								</div>
								
							</div>
						</div>
						<div class="col-sm-6">
							<div><i class="fa fa-comments-o"></i> Comments (<?= $forumPost->count_comments; ?>)</div>

							<?php if (in_array($authUser->role, ['A', 'O']) || $forumPost->isModerator($authUser)): ?>

								<div><?= $icons['warning']; ?> Pending Comments (<?= $forumPost->pending_posts->count(); ?>)</div>

							<?php endif; ?>

							<div><i class="fa fa-thumbs-up"></i> Likes (<?= $forumPost->count_likes; ?>)</div>
							<div><i class="fa fa-thumbs-down"></i> Dislikes (<?= $forumPost->count_dislikes ?>)</div>
							<div><i class="fa fa-clock-o"></i> Last Comment: <?= $forumPost->latest_comment_time; ?></div>
							<div><i class="fa fa-group"></i> Participating Users: (<?= $forumPost->count_users; ?>)</div>
							<?php
								if (in_array($authUser->role, ['A', 'O'])) {
									echo $this->Form->postLink(
										$icons['delete'] . 'Delete',
										['controller' => 'ForumPosts', 'action' => 'delete', $forumPost->id],
										[
											'escape' => false,
											'class' => 'btn btn-danger m-t-sm btn-xs',
											'confirm' => 'Do you really want to move the forum post to the trash?'
										]
									);

									echo $this->Html->link($icons['edit'].'Edit',
										['action' => 'edit', $forumPost->id],
										['class' => 'btn btn-info  m-t-sm m-l-sm btn-xs', 'escape' => false]
									);

								}
							?>
						</div>

					</div>
				</div>
			<?php endif; ?>

		<?php endforeach; ?>

	<?php endforeach; ?>
<?php endif; ?>

<?= $this->element('pagination'); ?>


