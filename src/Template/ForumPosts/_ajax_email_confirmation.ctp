<div id="confirmation-modal" class="modal fade"  role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" style="color:white">Sharing <?= $model;?></h4>
			</div>
			<div class="modal-body" id="lightbox-body">
				<h3 > You are going to send a notification about the <?= $model;?> to:</h3></br>
				<?php foreach ($receiversNames as $receiversName): ?>
					<?= '<span class="label label-default" style="line-height:2;font-size:14px;">'.$receiversName.'</span>'; ?>
				<?php endforeach; ?>
			</div>

			<div class="modal-footer">
				<div class="col-xs-12">
					<?= $this->Form->input('password', [
						'label' => false,
						'Placeholder' => 'enter your passsword',
						'required' => true,
						'type' => 'password',
					]); ?>
				</div>
				<?= $this->Html->link('Send',
					'javascript:;',
					[
						'type' => 'Submit',
						'onclick' => 'submitForm(true)',
						'escape' => false,
						'class' => 'btn btn-icon btn-info ',
					]
				); ?>
			</div>

		</div>
	</div>
</div>