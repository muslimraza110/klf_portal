<?php if (!empty($forumPost->moderators)): ?>

	<ul class="list-group user-list-group">

		<?php foreach ($forumPost->moderators as $moderator): ?>

			<li class="list-group-item">
				<?= $this->Html->link(
					sprintf(
						'%s %s',
						$this->Html->image($moderator->profile_thumb, ['class' => 'img-circle']),
						$moderator->name
					),
					['controller' => 'Users', 'action' => 'view', $moderator->id],
					['escape' => false]
				); ?>

				<?= $this->Html->link(
					'Remove',
					'javascript:;',
					[
						'class' => 'btn btn-sm btn-danger pull-right',
						'onclick' => 'removeUser(this)',
						'data-id' => $moderator->id
					]
				); ?>
			</li>

		<?php endforeach; ?>

	</ul>

<?php endif; ?>