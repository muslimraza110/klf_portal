<?php

$scriptBlock = <<<JS




	var submitForm =  function (password) {

		var contactList = [];
		$('form input[name^="lists"]:checked').map(function() {
			contactList.push($(this).val());
		});

		var emailReceivers = [];
		$('form input[name^="EmailLists"]:checked').map(function() {
			emailReceivers.push($(this).val());
		});

		if ((contactList.length != 0 || emailReceivers.length != 0) && !password) {
			$.post(
				'{$this->Url->build(['action' => 'ajax_email_confirmation'])}',
				{
					contactList: contactList,
					emailReceivers: emailReceivers,
					model : 'Post'
				},
				function (response) {
					$('#email-confirmation').html(response);
					$('#confirmation-modal').modal('show');
				}
			);
		} else {
			$('form').submit();
		}
	}


	$('.group-checkboxes input').on('ifChecked', function () {
		$('.everyone-checkbox').iCheck('uncheck');
	});

	$('.everyone-checkbox').on('ifChecked', function () {

		$('.group-checkboxes input').iCheck('uncheck');


	});

	$('.role').on('ifChecked', function () {

		var target = '.' + $(this).attr('name')
		$(target).iCheck('check');

	});


	$('.role').on('ifUnchecked', function () {

		var target = '.' + $(this).attr('name')
		$(target).iCheck('uncheck');

	});

	$('#savecontactlist').on('ifChanged', function () {

		if ($(this).iCheck('update')[0].checked) {
			$('#emaillists-title').attr('disabled', false);
		} else {
			$('#emaillists-title').attr('disabled', true);
			$('#emaillists-title').val('');
		}

	});

	$('div.checkboxes  .checkbox').addClass('inline m-r-md');



JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>
<div class="ibox col-xs-24">
	<div class="ibox-title blue-bg">
		<h3><?= $data->topic; ?></h3>
	</div>
	<div class="ibox-content">
		<div class="media<?= !empty($data->pending) ? ' media-pending' : ''; ?>" id="forum-post-<?= $data->id ?>">
			<div class="row">
				<div class="col-sm-6 col-md-4 text-center">

					<?php
						$profileLink = '#';

						if (!empty($data->user))
							$profileLink = ['controller' => 'Users', 'action' => 'view', $data->user->id];

						echo $this->Html->link(
							$this->Html->image($data->user->profile_thumb, ['class' => 'profile-thumb']) . '<br /><h4>' . $data->user->name . '</h4>',
							$profileLink,
							['escape' => false, 'class' => 'avatar']
						);
					?>

					<br />

					<div class="author-info">
						<strong>Posts:</strong> <?= $data->userTotalPosts($data->user_id); ?><br>
						<strong>Joined:</strong> <?= $data->user->created->i18nFormat('LLLL d, yyyy'); ?><br>
					</div>
				</div>

				<div class=" col-sm-18 col-md-19">
					<div class="media-body">
						<p><small class="text-muted">Posted: <?= $data->created->i18nFormat('EEEE LLLL d, yyyy @ h:mm a'); ?></small></p>
						<div class="content">
							<p class="quote-header">
								<strong><?= $data->user->name; ?></strong> wrote on <?= $data->created->i18nFormat('EEEE LLLL d, yyyy @ h:mm a'); ?>:
							</p>
							<?= $data->content; ?>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<?= $this->Form->create(null); ?>
<div id="email-confirmation">
</div>
<div class="ibox">
	<div class="ibox-title ibox-icons">
		<span><?= $icons['envelope']; ?></span>
		<h3>E-Blast</h3>
	</div>
	<div class="ibox-content" style="min-height: 300px; ">
		<?php if (!empty($contactList)): ?>
			<?= $this->element('add_email_list'); ?>
		<?php endif; ?>
		<?php if (!empty($emailLists)): ?>
			<?php foreach ($emailLists as $emailList): ?>
				<?= $this->element('email_lists', ['emailList' => $emailList]); ?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
	<div class="submit-big">
		<?= $this->Form->input('Send',
		 [
			 'type' => 'button',
			 'label' => false,
			 'class' => 'btn btn-primary',
			 'onclick' => 'submitForm()',
		 ]); ?>
	</div>

	<?= $this->Form->end(); ?>
</div>
