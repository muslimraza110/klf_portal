<?= $this->element('ckeditor'); ?>

<?= $this->Form->create($subforum); ?>

<div class="row">

	<div class="col-md-16">

		<div class="ibox">
			<div class="ibox-title ibox-icons">
				<span><?= $icons['folder'] ?></span>
				<h3>Subtopic Details</h3>
			</div>

			<div class="ibox-content checkboxes">

				<?php
				echo $this->Form->input('title');
				echo $this->Form->input('description', ['class' => 'ckeditor']);
				?>
			</div>
		</div>

	</div>

</div>

<div class="submit-big">
	<?= $this->Form->submit('Publish', ['class' => 'btn btn-primary']); ?>
</div>

<?= $this->Form->end(); ?>
