<?php if ($scope != 'admin' || $authUser->role == 'A'): ?>

	<div style="overflow: hidden">
		<?= $this->Html->link(
			'Add Subtopic',
			['controller' => 'ForumPosts', 'action' => 'add_subtopic', $scope],
			['escape' => false, 'class' => 'btn pull-right btn-primary']
		);
		?>
	</div>

<?php endif; ?>

<?php if (!empty($subtopics)): ?>
	<?php foreach ($subtopics as $subtopic): ?>
		
		<div class="row vote-item m-b-md">
			<div class="col-sm-24">
				
				<div class="vote-icon pull-left">
					<?=
					$this->Html->link(
						$icons['folder'] . '    ' . $subtopic->title,
						['controller' => 'ForumPosts', 'action' => 'view_subtopic', $subtopic->id],
						['escape' => false, 'class' => 'vote-title']
					);
					?>
				</div>
				
				<div class="pull-right" style="margin: 12px 0 10px;">
					<?php
					if (in_array($authUser->role, ['A'])) {
						echo $this->Form->postLink(
							$icons['delete'] . 'Delete',
							['controller' => 'ForumPosts', 'action' => 'delete_subtopic', $subtopic->id, $scope],
							[
								'escape' => false,
								'class' => 'btn btn-danger m-t-sm btn-xs',
								'confirm' => 'Do you really want to move ' . $subtopic->title . ' subtopic to the trash?'
							]
						);
					}
					?>
				</div>
			
			</div>
		</div>
	
	<?php endforeach; ?>
<?php endif; ?>

<?php if (!empty($groupedForumPosts)): ?>
	<?php foreach ($groupedForumPosts as $groupName => $forumPosts): ?>
		
		<h3><?= !in_array($scope, ['admin', 'public']) ? $groupName : ''; ?></h3>
		
		<?php foreach ($forumPosts as $forumPost): ?>
			
			<?php if (!empty($forumPost->user)): ?>
				<div class="vote-item">
					<div class="row">
						
						<div class="col-sm-18">
							
							<div class="vote-icon pull-left">
								<?= $icons['coffee']; ?>
							</div>
							
							<div style="padding-left: 50px;">
								<?php
								$name = $forumPost->topic;
								
								if (empty($forumPost->topic)) {
									$name = html_entity_decode(strip_tags($forumPost->content), ENT_QUOTES);
								}
								
								if (!empty($forumPost->pinned)) {
									$name = sprintf('%s %s', $icons['map-pin'], $name);
								}
								
								echo $this->Html->link(
									$name,
									['controller' => 'ForumPosts', 'action' => 'view', $forumPost->id],
									['escape' => false, 'class' => 'vote-title']
								);
								?>
								
								<div class="vote-info m-b-sm">
									<?php
									$authorUrl = '#';
									
									if (!empty($forumPost->user->contact)) {
										$authorUrl = ['controller' => 'Users', 'action' => 'view', $forumPost->user->id];
									}
									
									echo sprintf('By %s <span class="post-time">%s</span>',
										$this->Html->link(
											$icons['user'] . ' ' . $forumPost->user->name,
											$authorUrl,
											['escape' => false, 'class' => 'post-author']
										),
										$forumPost->created->i18nFormat('EEEE LLLL d, yyyy @ h:mm a')
									);
									
									?>
								</div>
								
							</div>
							
						</div>
						
						<div class="col-sm-6">
							
							<div>
								<i class="fa fa-comments-o"></i> Comments (<?= $forumPost->count_comments; ?>)
							</div>
							
							<?php if (in_array($authUser->role, ['A', 'O']) || $forumPost->isModerator($authUser)): ?>
								
								<div><?= $icons['warning']; ?> Pending Comments (<?= $forumPost->pending_posts->count(); ?>)</div>
							
							<?php endif; ?>
							
							<div>
								<i class="fa fa-thumbs-up"></i> Likes (<?= $forumPost->count_likes; ?>)
							</div>
							
							<div>
								<i class="fa fa-thumbs-down"></i> Dislikes (<?= $forumPost->count_dislikes ?>)
							</div>
							
							<div>
								<i class="fa fa-clock-o"></i> Last Comment: <?= $forumPost->latest_comment_time; ?>
							</div>
							
							<?php
							if (in_array($authUser->role, ['A', 'O'])) {
								echo $this->Form->postLink(
									$icons['delete'] . 'Delete',
									['controller' => 'ForumPosts', 'action' => 'delete', $forumPost->id],
									[
										'escape' => false,
										'class' => 'btn btn-danger m-t-sm btn-xs',
										'confirm' => 'Do you really want to move the forum post to the trash?'
									]
								);
							}
							?>
							</div>
					
					</div>
				</div>
			<?php endif; ?>
		
		<?php endforeach; ?>
	<?php endforeach; ?>

<?php endif; ?>
<?= $this->element('pagination'); ?>
