<?php

$scriptBlock = <<<JS

var moderatorList = function () {
	
	$.post(
		'{$this->Url->build(['action' => 'ajax_html_moderator_list'])}',
		{
			id: '{$forumPost->id}'
		},
		function (response) {
			
			$('#moderator-list').html(response);
			
		}
	);
	

}

var whitelistList = function () {
	
	$.post(
		'{$this->Url->build(['action' => 'ajax_html_whitelist_list'])}',
		{
			id: '{$forumPost->id}'
		},
		function (response) {
			
			$('#whitelist-list').html(response);
			
		}
	);
	
};

var submitForm =  function (password) {

	var contactList = [];
	$('form input[name^="lists"]:checked').map(function() {
		contactList.push($(this).val());
	});

	var emailReceivers = [];
	$('form input[name^="EmailLists"]:checked').map(function() {
		emailReceivers.push($(this).val());
	});

	if ((contactList.length != 0 || emailReceivers.length != 0) && !password) {
		$.post(
			'{$this->Url->build(['action' => 'ajax_email_confirmation'])}',
			{
				contactList: contactList,
				emailReceivers: emailReceivers,
				model : 'Post'
			},
			function (response) {
				$('#email-confirmation').html(response);
				$('#confirmation-modal').modal('show');
			}
		);
	} else {
		$('form').submit();
	}

}


var addUser = function (self) {
	
	$.post(
		'{$this->Url->build(['action' => 'ajax_add_moderator'])}.json',
		{
			id: '{$forumPost->id}',
			user_id: $(self).data('id')
		},
		function () {
			
			$(self).hide();
			
			moderatorList();
			
		}
	);
	
};

var removeUser = function (self) {
	
	$.post(
		'{$this->Url->build(['action' => 'ajax_remove_moderator'])}.json',
		{
			id: '{$forumPost->id}',
			user_id: $(self).data('id')
		},
		function () {
			
			$(self).hide();
			
			moderatorList();
			
		}
	);
	
};

var addWhitelist = function (self) {
	
	$.post(
		'{$this->Url->build(['action' => 'ajax_add_whitelist'])}.json',
		{
			id: '{$forumPost->id}',
			model_id: $(self).data('id'),
			model: $(self).data('model')
		},
		function () {
			
			$(self).hide();
			
			whitelistList();
			
		}
	);
	
};

var removeWhitelist = function (self) {
	
	$.post(
		'{$this->Url->build(['action' => 'ajax_remove_whitelist'])}.json',
		{
			id: $(self).data('id'),
		},
		function () {
			
			$(self).hide();
			
			whitelistList();
			
		}
	);
	
};

$(function() {
	
	var addModeratorTimeout;
	
	$('#add-moderator').keypress(function () {
		
		window.clearTimeout(addModeratorTimeout);
		
		var self = this;
		
		addModeratorTimeout = window.setTimeout(function () {
			
			$.post(
				'{$this->Url->build(['controller' => 'Users', 'action' => 'ajax_html_user_list'])}',
				{
					id: '{$forumPost->id}',
					term: $(self).val(),
					model: 'ModeratedForumPosts'
				},
				function (response) {
					
					$('#add-moderator-response').html(response);
					
				}
			);
			
		}, 500);
		
	});
	
	moderatorList();
	
	var addWhitelistTimeout;
	
	$('#add-whitelist-user, #add-whitelist-group').keypress(function () {
		
		window.clearTimeout(addWhitelistTimeout);
		
		var
			self = this,
			url = '{$this->Url->build(['controller' => 'Users', 'action' => 'ajax_html_user_list'])}';
		
		if ($(this).attr('id') == 'add-whitelist-group') {
			url = '{$this->Url->build(['controller' => 'Users', 'action' => 'ajax_html_group_list'])}';
		}
		
		addWhitelistTimeout = window.setTimeout(function () {
			
			$.post(
				url,
				{
					id: '{$forumPost->id}',
					term: $(self).val(),
					model: 'ForumPostWhitelists'
				},
				function (response) {
					
					$('#add-whitelist-response').html(response);
					
				}
			);
			
		}, 500);
		
	});
	
	whitelistList();
	
	$('.group-checkboxes input').on('ifChecked', function () {
		
		$('.everyone-checkbox').iCheck('uncheck');
		
	});
	
	$('.everyone-checkbox').on('ifChecked', function () {
		
		$('.group-checkboxes input').iCheck('uncheck');
		

	});
	
	$('#visibility-w').on('ifToggled', function () {
		
		var _restrictions = $('#ibox-restrictions');
		
		if ($(this).is(':checked')) {
			_restrictions.show();
		} else {
			_restrictions.hide();
		}
		
	});
	
});

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>

<?= $this->element('ckeditor'); ?>

<?= $this->Form->create($forumPost); ?>

<div id="email-confirmation">

</div>
<div class="row">

	<div class="<?= empty($forumPost->parent_id) ? 'col-md-16' : 'col-md-24'; ?>">

		<div class="ibox">
			<div class="ibox-title ibox-icons">
				<span><?= $icons['commenting'] ?></span>
				<h3>Topic Details</h3>
			</div>

			<div class="ibox-content checkboxes">

				<?php if ($authUser->role == 'A'): ?>
					<?php if (empty($forumPost->parent_id)): ?>

						<?= $this->Form->input('pinned', ['label' => 'Pinned Post']); ?>

					<?php endif; ?>

					<?= $this->Form->input('sod', ['label' => 'Show on Dashboard']); ?>
				<?php endif; ?>
				<?php
				echo $this->Form->input('topic');
				echo $this->Form->input('content', ['class' => 'ckeditor']);
				?>
			</div>
		</div>

	</div>

	<?php if (empty($forumPost->parent_id) && $authUser->role == 'A'): ?>

		<div class="col-md-8">

			<hr>
			<?= $this->Form->input('eblast', [
				'type' => 'checkbox',
				'label' => [
					'text' => '<b>Send E-Blast</b>',
					'escape' => false
				]
			]) ?>
			<hr>

			<div class="ibox">
				<div class="ibox-title ibox-icons">
					<span><?= $icons['user']; ?></span>
					<h3>Moderators</h3>
				</div>

				<div class="ibox-content">

					<?= $this->Form->input('moderated', ['label' => 'Post Approve Posts']); ?>

					<hr>

					<?php if ($forumPost->isNew()): ?>

						<div class="alert alert-info">
							You will be able to add moderators after saving the post.
						</div>

					<?php else: ?>

						<?= $this->Form->input(
							'add_moderator',
							[
								'placeholder' => 'Search for Contact Name'
							]
						); ?>

						<div id="add-moderator-response"></div>

						<div id="moderator-list"></div>

					<?php endif; ?>

				</div>
			</div>

			<?php if ($authUser->role == 'A'): ?>
			<div class="ibox">
				<div class="ibox-title ibox-icons">
					<span><?= $icons['eye']; ?></span>
					<h3>Visibility</h3>
				</div>
				<div class="ibox-content" style="max-height: 300px; ">
					<div class="visibility">
							<?= $this->Form->input('visibility', [
								'label' => false,
								'type' => 'radio',
								'hiddenField' => false,
								'default' => 'P',
								'options' => $groups,
							]); ?>
					</div>
				</div>
			</div>

			<div class="ibox" id="ibox-restrictions"<?= $forumPost->visibility != 'W' ? 'style="display: none"' : '' ?>>

				<div class="ibox-title ibox-icons">
					<span><?= $icons['check-circle-o'] ?></span>
					<h3>Restrictions</h3>
				</div>

				<div class="ibox-content">

					<?php if ($forumPost->isNew()): ?>

						<div class="alert alert-info">
							You will be able to edit restrictions after saving the post.
						</div>

					<?php else: ?>

						<label>Add to Whitelist</label>

						<div class="row">

							<div class="col-md-12">
								<?= $this->Form->input(
									'add_whitelist_user',
									[
										'placeholder' => 'Search for Contact Name',
										'label' => false
									]
								) ?>
							</div>

							<div class="col-md-12">
								<?= $this->Form->input(
									'add_whitelist_group',
									[
										'placeholder' => 'Search for Group Name',
										'label' => false
									]
								) ?>
							</div>

						</div>

						<div id="add-whitelist-response"></div>

						<div id="whitelist-list"></div>

					<?php endif; ?>

				</div>

			</div>

		<?php elseif(empty($subforum)): ?>
			<div class="ibox">
				<div class="ibox-title ibox-icons">
					<span><?= $icons['group']; ?></span>
					<h3><?= $scope ?></h3>
				</div>

				<div class="ibox-content" style="max-height: 300px; overflow-y: scroll;">

					<?php /*$this->Form->input('_groups[_ids]', [
						'type' => 'checkbox',
						'name' => $scope.'s[_ids]',
						// 'value' => $scope == 'public' ? 1 : 0,
						'label' => 'Everyone',
						'default' => $scope == 'public' ? 1 : 0,
						'class' => 'everyone-checkbox'
					]);*/ ?>
						<div class="group-checkboxes">
						<?php if (!empty($groups)): ?>
							<?php foreach ($groups as $name => $group): ?>

							<h5><?= $name; ?></h5>
							<?php $model = $name == 'Businesses' || $name == 'Charities' ? 'groups' : 'projects'; ?>
							<div >
								<?= $this->Form->input($model.'._ids', [
									'label' => false,
									'hiddenField' => false,
									'multiple' => 'checkbox',
									'value' => $id,
									'options' => $group
								]); ?>
							</div>


						<?php endforeach; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="ibox">
				<div class="ibox-title ibox-icons">
					<span><?= $icons['eye']; ?></span>
					<h3>Visibility</h3>
				</div>
				<div class="ibox-content" style="max-height: 300px; ">
					<div class="visibility">
							<?= $this->Form->input('post_visibility', [
								'label' => false,
								'type' => 'radio',
								'hiddenField' => false,
								'default' => 'P',
								'options' => $postVisibility,
							]); ?>
					</div>
				</div>
			</div>
			<?php endif; ?>

		</div>

			<?php endif; ?>

</div>

<div class="submit-big">
	<?= $this->Form->input('Publish',
	 [
		 'type' => 'button',
		 'label' => false,
		 'class' => 'btn btn-primary',
		 'onclick' => 'submitForm()',
		 'style' => 'margin-left: 15px;'
	 ]); ?>

	<?php

	if ($forumPost->isNew()) {
		$action = ['action' => 'view_subtopic', $forumPost->subforum_id];
	} else {
		$action = ['action' => 'view', !empty($forumPost->parent_id) ? $forumPost->parent_id : $forumPost->id];
	}

	?>

	<?= $this->Html->link('Cancel', $action, [
		'label' => false,
		'class' => 'btn btn-primary'
	]) ?>
</div>

<?= $this->Form->end(); ?>
