<?php

$this->Html->css('plugins/chosen/chosen.min', ['block' => true]);

$this->Html->script('plugins/chosen/chosen.jquery.min', ['block' => true]);

?>

<?= $this->element('ckeditor') ?>

<?= $this->Html->scriptBlock(
	<<<JS

	var memberList = function () {

		$.post(
			'{$this->Url->build(['action' => 'ajax_html_member_list'])}',
			{
				id: '{$group->id}'
			},
			function (response) {

				$('#member-list').html(response);

			}
		);

	};

	var addUser = function (self) {

		$.post(
			'{$this->Url->build(['action' => 'ajax_add_member'])}.json',
			{
				id: '{$group->id}',
				user_id: $(self).data('id')
			},
			function () {

				$(self).hide();

				memberList();

			}
		);

	};

	var groupAccess = function (self) {

		$.post(
			'{$this->Url->build(['action' => 'ajax_group_access'])}.json',
			{
				group_request_id: $(self).data('id'),
				type: $(self).data('type')
			},
			function (response) {

				if (response.status == 'ok')
					$('#group-access-buttons-' + $(self).data('id')).hide();

			}
		);

	};

	var groupAdmin = function (self) {

		$.post(
			'{$this->Url->build(['action' => 'ajax_group_admin'])}.json',
			{
				groups_users_id: $(self).data('id'),
				admin: $(self).prop('checked') | 0
			}
		);

	};
	
	var userAlias = function (self) {

		$.post(
			'{$this->Url->build(['action' => 'ajax_user_alias'])}.json',
			{
				groups_users_id: $(self).data('id'),
				alias: $(self).val()
			}
		);

	};

	$(function() {

		$('#category-id').change(function () {

			var _checkboxHidden = $('#checkbox-hidden');

			if ($(this).val() == 3)
				_checkboxHidden.hide();
			else
				_checkboxHidden.show();

		});

		$('#tags-ids').chosen({

			placeholder_text_multiple: 'Enter Tags',
      create_option: true,
			single_backstroke_delete:	true

		});



		var addMemberTimeout;

		$('#add-member').keypress(function () {

			window.clearTimeout(addMemberTimeout);

			var self = this;

			addMemberTimeout = window.setTimeout(function () {

				$.post(
					'{$this->Url->build(['controller' => 'Users', 'action' => 'ajax_html_user_list'])}',
					{
						id: '{$group->id}',
						term: $(self).val(),
						model: 'Groups'
					},
					function (response) {

						$('#add-member-response').html(response);

					}
				);

			}, 500);

		});

		memberList();

	});

JS
	, ['block' => true]); ?>

<?= $this->Form->create($group); ?>

<?php if (!empty($group->id)): ?>

	<div class="profile-banner" style="background-image: url(<?= $group->banner_img_url ?>">

		<div class="profile-img-wrapper">
			<?= $this->Html->image($group->profile_img_url.'?'.microtime(true)); ?>
			<?= $this->Html->link(
				'Change Profile Image',
				['action' => 'upload_image', $group->id, 'profile_img'],
				['escape' => false, 'class' => 'btn btn-info']
			); ?>
		</div>

		<?= $this->Html->link(
			'Change Banner Image',
			['action' => 'upload_image', $group->id, 'banner_img'],
			['escape' => false, 'class' => 'btn btn-info pull-right']
		); ?>

		<h1><?= $group->name ?></h1>
		<?= (!empty($group->description)) ? '<p>' . $group->description . '</p>' : '' ?>

	</div>

<?php endif; ?>

<div class="row">
	<div class="col-sm-16">


		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['edit'] ?></span>
				<h3>Main Information</h3>
			</div>
			<div class="ibox-content" style="overflow: visible;">

				<div class="row">

					<div class="col-md-9">
						<?= $this->Form->input('category_id'); ?>
					</div>

					<div class="col-md-3" id="checkbox-hidden"<?= $group->category_id == 3 ? ' style="display: none;"' : ''; ?>>
						<label>&nbsp;</label>
						<?= $this->Form->input('hidden', ['label' => 'Visible to Group Only']); ?>
					</div>

					<?php if (in_array($authUser->role, ['A'])): ?>

						<div class="col-md-4">
							<?= $this->Form->label('pending', 'Status'); ?>
							<?= $this->Form->input('pending'); ?>
						</div>

						<div class="col-md-4">
							<label>&nbsp;</label>
							<?= $this->Form->input('donations', [
								'label' => 'Accept Donations'
							]); ?>
						</div>
						<div class="col-md-4">
							<label>&nbsp;</label>
							<?= $this->Form->input('passive'); ?>
						</div>

					<?php endif; ?>

				</div>

				<?= $this->Form->input('name') ?>

				<?= $this->Form->input('description', [
					'type' => 'textarea',
					'rows' => 3,
					'help' => 'Maximum 250 characters'
				]) ?>
				<label>Main Email<span style="color:red">*</span></label>
				<?= $this->Form->input('email', ['label' => false, 'required']) ?>
				<label>Phone Number<span style="color:red">*</span></label>
				<?= $this->Form->input('phone', ['label' => false, 'required']) ?>
				<?= $this->Form->input('website', ['prepend' => 'http://']) ?>
				<?= $this->Form->input('tags._ids'); ?>

			</div>
		</div>

	</div>

	<div class="col-sm-8">

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['users'] ?></span>
				<h3>Members</h3>
			</div>
			<div class="ibox-content" style="max-height: 300px; overflow-y: scroll;">

				<?php if ($group->isNew()): ?>

					<div class="alert alert-info">
						You will be able to add members after saving the group.
					</div>

				<?php else: ?>

					<?php if (in_array($authUser->role, ['A', 'O']) || $group->isAdmin($authUser)): ?>

						<?= $this->Form->input(
							'add_member',
							[
								'placeholder' => 'Search for Contact Name'
							]
						); ?>

						<div id="add-member-response"></div>

					<?php endif; ?>

					<div id="member-list"></div>

				<?php endif; ?>

			</div>
		</div>

		<?php if (in_array($authUser->role, ['A']) || $group->isAdmin($authUser)): ?>

			<div class="ibox">
				<div class="ibox-title">
					<span><?= $icons['user-plus']; ?></span>
					<h3>Pending Access Requests</h3>
				</div>

				<div class="ibox-content" style="max-height: 300px; overflow-y: scroll;">

					<ul class="list-group user-list-group">

						<?php if (!empty($group->group_requests)): ?>

							<?php foreach ($group->group_requests as $request): ?>

								<li class="list-group-item">
									<?= $this->Html->link(
										sprintf(
											'%s %s',
											$this->Html->image($request->user->profile_thumb, ['class' => 'img-circle']),
											$request->user->name
										),
										['controller' => 'Users', 'action' => 'view', $request->user->id],
										['escape' => false]
									); ?>

									<div id="group-access-buttons-<?= $request->id; ?>" class="pull-right">

										<?= $this->Html->link(
											'Accept',
											'javascript:;',
											[
												'class' => 'btn btn-sm btn-primary',
												'onclick' => 'groupAccess(this)',
												'data-id' => $request->id,
												'data-type' => 'A'
											]
										); ?>

										<?= $this->Html->link(
											'Reject',
											'javascript:;',
											[
												'class' => 'btn btn-sm btn-danger',
												'onclick' => 'groupAccess(this)',
												'data-id' => $request->id,
												'data-type' => 'R'
											]
										); ?>

									</div>
								</li>

							<?php endforeach; ?>

						<?php else: ?>

							<li class="list-group-item disabled">
								No requests found.
							</li>

						<?php endif; ?>

					</ul>

				</div>
			</div>

		<?php endif; ?>

	</div>

</div>

<div class="row">
	<div class="col-md-12">

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['map-marker'] ?></span>
				<h3>Address</h3>
			</div>
			<div class="ibox-content">

				<?= $this->Form->input('address'); ?>
				<?= $this->Form->input('address2', ['label' => false]); ?>

				<div class="row">
					<div class="col-sm-6">
						<?= $this->Form->input('city'); ?>
					</div>
					<div class="col-sm-6">
						<?= $this->Form->input('postal_code'); ?>
					</div>
					<div class="col-sm-6">
						<?= $this->Form->input('region', ['label' => 'Province/State']); ?>
					</div>
					<div class="col-sm-6">
						<?= $this->Form->input('country'); ?>
					</div>
				</div>

			</div>
		</div>

	</div>
	<div class="col-md-12">

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['align-left'] ?></span>
				<h3>About Us</h3>
			</div>
			<div class="ibox-content">
				<?= $this->Form->input('bio', ['class' => 'ckeditor']); ?>
			</div>
		</div>

	</div>

</div>


<div class="submit-big">
	<?= $this->Form->submit('Save', ['bootstrap-type' => 'primary']); ?>
</div>

<?= $this->Form->end() ?>
