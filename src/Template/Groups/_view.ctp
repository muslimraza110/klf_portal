<?php
echo $this->Html->css(
	['plugins/jasny/jasny-bootstrap.min',
	'plugins/jsTree/style.min'
], ['block' => true]);
echo $this->Html->script([
	'plugins/jasny/jasny-bootstrap.min',
	'plugins/jasny/jasny-bootstrap.min',
	'plugins/jsTree/jstree.min'
], ['block' => true]);
?>

<?= $this->Html->scriptBlock(
	<<<JS

	var groupAccess = function (self) {

		$.post(
			'{$this->Url->build(['action' => 'ajax_group_access'])}.json',
			{
				group_request_id: $(self).data('id'),
				type: $(self).data('type')
			},
			function (response) {

				if (response.status == 'ok')
					$('#group-access-buttons-' + $(self).data('id')).hide();

			}
		);

	};

	$(function() {

		$('#request-access').click(function () {

			var self = this;

			$.post(
				'{$this->Url->build(['controller' => 'Users', 'action' => 'ajax_request_group_access'])}.json',
				{
					group_id: '{$group->id}'
				},
				function () {

					$(self)
						.append('&nbsp;{$icons['check']}')
						.addClass('disabled');

				}
			);

		});

		$('#cancel-access-request').click(function () {

			var self = this;

			$.post(
				'{$this->Url->build(['controller' => 'Users', 'action' => 'ajax_cancel_group_access_request'])}.json',
				{
					group_id: '{$group->id}'
				},
				function () {

					$(self)
						.append('&nbsp;{$icons['check']}')
						.addClass('disabled');

				}
			);

		});

		$('.remove-member').click(function () {

			var self = this;

			$.post(
				'{$this->Url->build([ 'action' => 'ajax_leave_group'])}.json',
				{
					group_id: '{$group->id}',
					id: $(self).data('id')
				},
				function () {

					$(self).parent().parent()
						.hide();

				}
			);

		});

		$('#leave-group').click(function () {

			var self = this;

			$.post(
				'{$this->Url->build([ 'action' => 'ajax_leave_group'])}.json',
				{
					group_id: '{$group->id}'
				},
				function () {

					$(self)
						.hide();

				}
			);

		});

		$('.equalize1').equalizeHeights();

		$(window).resize(function() {
			$('.equalize1').equalizeHeights();
		});

		$('#projects-tree').bind('select_node.jstree', function (e, data) {

			var href = data.node.a_attr.href;

			if (href == '#') {
				return '';
			}
			
			document.location.href = href;

		});

		$('#projects-tree').jstree({
			'core' : {
				'check_callback' : true
				},
				'plugins' : [ 'types', 'dnd' ],
				'types' : {
					'default' : {
						'icon' : 'fa fa-folder'
				},
				'html' : {
					'icon' : 'fa fa-file-code-o'
				},
				'svg' : {
					'icon' : 'fa fa-file-picture-o'
				},
				'css' : {
					'icon' : 'fa fa-file-code-o'
				},
				'img' : {
					'icon' : 'fa fa-file-image-o'
				},
				'js' : {
					'icon' : 'fa fa-file-text-o'
				}
			}
		});

	});

JS
	, ['block' => true]); ?>

<?php if (false): ?>

	<?php if ($authUser->role == 'O'): ?>

		<?= $this->element('vote', ['entity' => $group, 'vote' => $vote, 'model' => 'Groups']); ?>

	<?php endif; ?>

	<?php if (!empty($votes) && !$votes->isEmpty()): ?>

		<?= $this->element('vote_results', ['entity' => $group, 'votes' => $votes, 'model' => 'Groups']); ?>

	<?php endif; ?>

<?php endif; ?>

<div class="profile-banner" style="background-image: url(<?= $group->banner_img_url ?>">

	<div class="profile-img-wrapper">
		<?= $this->Html->image($group->profile_img_url); ?>
	</div>

	<?php
		if (in_array($authUser->role, ['A']) || $group->isAdmin($authUser)) {

			$actions = $this->Html->link($icons['edit'],
				['action' => 'edit', $group->id],
				['escape' => false, 'title' => 'Edit Group', 'data-toggle' => 'tooltip', 'class' => 'btn btn-info btn-icon']
			);

			echo '<div class="pull-right">' . $actions . '</div>';
		}
	?>

	<h1><?= $group->name ?></h1>
	<?= (!empty($group->description)) ? '<p>' . $group->description . '</p>' : '' ?>

</div>

<div class="row">
	<div class="col-md-16 col-sm-24">

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['users'] ?></span>
				<h3>Members</h3>

				<div class="ibox-tools">

					<?php if ($requestGroupAccess): ?>

						<?= $this->Html->link(
							'Request Group Access',
							'javascript:;',
							['class' => 'btn btn-sm btn-info', 'id' => 'request-access']
						); ?>

					<?php endif; ?>

					<?php if ($group->getRequestStatus($authUser) == 'P'): ?>

						<?= $this->Html->link(
							'Cancel Group Access Request',
							'javascript:;',
							['class' => 'btn btn-sm btn-danger', 'id' => 'cancel-access-request']
						); ?>

					<?php endif; ?>

					<?php if (!empty($memberIds) && in_array($authUser->id, $memberIds)): ?>
						<?= $this->Html->link(
							'Leave Group',
							'javascript:;',
							['class' => 'btn btn-sm btn-danger', 'id' => 'leave-group']
						); ?>
					<?php endif; ?>

					<?= $this->Html->link($icons['refresh'].' Refresh', ['action' => 'view', $group->id], [
						'escape' => false,
						'class' => 'btn btn-info btn-sm'
					]); ?>

				</div>

			</div>
			<div class="ibox-content equalize1" style="height: 120px; overflow-x: auto; padding-bottom: 40px;">
				<?php if (!empty($group->users)): ?>
					<?php foreach ($group->users as $user):	?>
						<?php $alias = '';
						if (!empty($user->_joinData->user_alias)) {
							$alias = ' - ' . $user->_joinData->user_alias;
						} ?>
						<div class="image-badge-container">
							<?php if ($user->_joinData->admin) : ?>
								<span class="image-badge" style="width:100%; text-align:right;"><?= $icons['star'] ?></span>
							<?php endif; ?>
							<?php if ($authUser->role == 'A' || $group->isAdmin($authUser)): ?>
								<span class="image-badge">
									<?= $this->Html->link(
										$icons['minus'],
										'javascript:;',
										[
											'escape' => false,
											'class' => 'btn btn-danger btn-icon remove-member',
											'style' => ['height:26px;width:26px;padding:3px;'],
											'data-id' => $user->id,
											'title' => 'Remove Member'
										]
									); ?>
								</span>
							<?php endif; ?>
							<?= $this->Html->link(
								$this->Html->image($user->profile_thumb),
								['controller' => 'Users', 'action' => 'view', $user->id],
								['escape' => false, 'title' => $user->name . $alias, 'data-toggle' => 'tooltip-right', 'class' => 'profile-thumb']
							); ?>

						</div>

					<?php endforeach; ?>
					<div class="ibox-content" style="border-top: none;">
						<div class="table-responsive">
										<table class="table table-condensed table-striped">
											<thead>
											<?php
												$headers = [
													'Name',
													'Group Title'
												];
												echo $this->Html->tableHeaders($headers);
											?>
											</thead>
											<tbody>
											<?php
												foreach ($group->users as $user) {
													$rows = [];
													$rows[] = [
														$user->name,
														$user->_joinData->user_alias,
													];
													echo $this->Html->tableCells($rows);
												}
											?>
											</tbody>
										</table>
									</div>
						</div>
				<?php else: ?>
					<p class="text-muted">No members yet...</p>
				<?php endif; ?>

			</div>
		</div>
	</div>
	<div class="col-md-8 col-sm-24">
		<div class="ibox">

			<div class="ibox-title">
				<span><?= $icons['cog'] ?></span>
				<h3>Related Groups</h3>
			</div>

			<div class="tab-content ibox-content equalize1" style="height: 150px; overflow-x: auto; padding-bottom: 40px;">
				<div class="panel-body">
					<?php if (!empty($relatedGroups = $group->getRelatedGroups($authUser))): ?>

						<?php foreach ($relatedGroups as $g):	?>

							<?= $this->Html->link(
								$this->Html->image($g->profile_thumb.'?'.microtime(true), ['class' => 'm-xs']),
								['controller' => 'Groups', 'action' => 'view', $g->id],
								['escape' => false, 'title' => $g->name, 'data-toggle' => 'tooltip']
							); ?>

						<?php endforeach; ?>

					<?php else: ?>
						<p class="text-muted">No related groups yet...</p>
					<?php endif; ?>

				</div>
			</div>

		</div>
	</div>

</div>


<div class="row">
	<div class="col-sm-10 col-md-8">

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['folder'] ?></span>
				<h3>Group Projects</h3>
				<?php if ($authUser->role == 'A' || in_array($authUser->id, $memberIds)): ?>
					<div class="ibox-tools">
						<?= $this->Html->link(
							$icons['plus'],
							['controller' => 'Projects', 'action' => 'add', $group->id],
							['escape' => false, 'class' => 'btn btn-info btn-icon']
						); ?>
					</div>
				<?php endif; ?>
			</div>

			<div class="ibox-content">
				<div id="projects-tree">

					<?php

					$list = [];

					foreach ($projects as $project) {

						$members = [];
						foreach ($project->users as $user) {

							$members[] = $this->Html->link($user->name,['controller' => 'Users', 'action' => 'view', $user->id]);

						}

						$projectLink =  $this->Html->link($project->name,['controller' => 'Projects', 'action' => 'view', $project->id]);

						$list[$projectLink] = $members;

					}

					echo $this->Html->nestedList($list);

					?>

				</div>

			</div>
		</div>

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['info'] ?></span>
				<h3>Information</h3>
			</div>

			<div class="ibox-content">

				<?php if (!empty($group->email)): ?>
					<h4>Email</h4>
					<p><?= $this->Html->link($group->email, 'mailto:' . $group->email, ['target' => '_blank']); ?></p>
				<?php endif; ?>

				<?php if (!empty($group->phone)): ?>
					<h4>Phone:</h4>
					<p><?= $group->phone; ?></p>
				<?php endif; ?>

				<?php if (!empty($group->multi_line_address)): ?>
					<h4>Address:</h4>
					<address><?= $group->multi_line_address; ?></address>
				<?php endif; ?>

				<?php if (!empty($group->website)): ?>
					<h4>Website:</h4>
					<p><?= $this->Html->link($group->website, 'http://'.$group->website, ['target' => '_blank']); ?></p>
				<?php endif; ?>

				<?php if (!empty($group->tags)): ?>

					<h4>Tags</h4>

					<?php

					$tags = [];

					if (!empty($group->tags))
						foreach ($group->tags as $tag)
							$tags[] = $tag->name;

					echo implode(', ', $tags);

					?>

				<?php endif; ?>

			</div>
		</div>

		<?php if (in_array($authUser->role, ['A']) || $group->isAdmin($authUser)): ?>

			<div class="ibox">
				<div class="ibox-title">
					<span><?= $icons['user-plus']; ?></span>
					<h3>Pending Access Requests</h3>
				</div>

				<div class="ibox-content" style="max-height: 300px; overflow-y: scroll;">

					<ul class="list-group user-list-group">

						<?php if (!empty($group->group_requests)): ?>

							<?php foreach ($group->group_requests as $request): ?>

								<li class="list-group-item">
									<?= $this->Html->link(
										sprintf(
											'%s %s',
											$this->Html->image($request->user->profile_thumb, ['class' => 'img-circle']),
											$request->user->name
										),
										['controller' => 'Users', 'action' => 'view', $request->user->id],
										['escape' => false]
									); ?>

									<div id="group-access-buttons-<?= $request->id; ?>" class="pull-right">

										<?= $this->Html->link(
											'Accept',
											'javascript:;',
											[
												'class' => 'btn btn-sm btn-primary',
												'onclick' => 'groupAccess(this)',
												'data-id' => $request->id,
												'data-type' => 'A'
											]
										); ?>

										<?= $this->Html->link(
											'Reject',
											'javascript:;',
											[
												'class' => 'btn btn-sm btn-danger',
												'onclick' => 'groupAccess(this)',
												'data-id' => $request->id,
												'data-type' => 'R'
											]
										); ?>

									</div>
								</li>

							<?php endforeach; ?>

						<?php else: ?>

							<li class="list-group-item disabled">
								No requests found.
							</li>

						<?php endif; ?>

					</ul>

				</div>
			</div>

		<?php endif; ?>

	</div>

	<div class="col-sm-14 col-md-16">

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['align-left'] ?></span>
				<h3>About Us</h3>
			</div>
			<div class="ibox-content">
				<?= nl2br($group->bio) ?>
			</div>
		</div>

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['calendar']; ?></span>
				<h3>Group Events</h3>
				<div class="ibox-tools">
					<?= $this->Html->link('Calendar',
					['controller' => 'Events', 'action' => 'index'],
					['class' => 'btn btn-info']); ?>
				</div>
			</div>
			<div class="ibox-content">
				<table class="table">
					<thead>
						<?php
							$headers = [
								'Project',
								'Event',
								'Creator',
								'Event Date'
							];
							echo $this->Html->tableHeaders($headers);
						?>
					</thead>
					<tbody>
					<?php
						$eventExist = false;
						if (!empty($events)) {
							foreach ($events as $event) {
								$row = [
									!empty($event->project_id) ? $event->project->name : '',
									$this->Html->link(
										$event->name,
										['controller' => 'Events', 'action' => 'index', $event->id]
									),
									$this->Html->link(
										!empty($event->project->users[0]->name) ? $event->project->users[0]->name : $event->user->name,
										['controller' => 'Users', 'action' => 'view', !empty($event->project->users[0]->id) ? $event->project->users[0]->id : $event->user->id ]
									),
									$event->date_range,
								];
								echo $this->Html->tableCells($row);
							}
						} else {
							echo $this->Html->tableCells([[
								['No events found.', ['colspan' =>count($headers)]]
							]]);
						}
					?>
					</tbody>
				</table>
			</div>
		</div>

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['commenting-o']; ?></span>
				<h3>Group Forum Posts</h3>
				<?php $category = $group->category_id == 2 ? 'business' : 'charity' ?>
				<?php if (in_array($authUser->id, $memberIds)
					// || $authUser->role == 'A'
					|| $group->isAdmin($authUser)):
				?>
					<div class="ibox-tools">
						<?= $this->Html->link($icons['plus'],
						['controller' => 'ForumPosts', 'action' => 'add', $category, $group->id],
						['class' => 'btn btn-icon btn-info', 'escape' => false]); ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="ibox-content">
				<table class="table rowlink"  data-link="row">
					<thead>
						<?php
							$headers = [
								'Title',
								'Creator',
								'Created'
							];
							echo $this->Html->tableHeaders($headers);
						?>
					</thead>
					<tbody>
					<?php
						if (!empty($forumPosts)) {
							foreach ($forumPosts as $forumPost) {
								if (in_array($authUser->id, $memberIds)
									// || $authUser->role == 'A'
									|| $group->isAdmin($authUser)
									|| $forumPost->post_visibility != 'R') {
									$row = [
										$this->Html->link('<strong>'.$forumPost->topic.'<strong>',
											['controller' => 'ForumPosts', 'action' => 'view', $forumPost->id],
											['escape' => false]),
										$forumPost->user->name,
										$forumPost->created,
									];

									echo 	$this->Html->tableCells($row);
								}
							}
						} else {
								echo $this->Html->tableCells([[
									['No events found.', ['colspan' => 3]]
								]]);
						}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
