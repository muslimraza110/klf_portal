<?= $this->Html->scriptBlock(
<<<JS
	$(function() {
		
		$('.contact-box').equalizeHeights();
		
		$(window).resize(function() {
			$('.contact-box').equalizeHeights();
		});
		
		$('#pending').on('ifToggled', function () {
			
			$('#show-pending').submit();
			
		});
		
	});
JS
, ['block' => true]); ?>

<div class="row hidden-print">

	<div class="col-md-20">
		<?= $this->Form->create(null, ['type' => 'get']) ?>

		<?= $this->Form->input('q', [
			'label' => false,
			'placeholder' => 'Search for a name or location',
			'append' => $this->Form->button($icons['search'], ['bootstrap-type' => 'info', 'class' => 'btn-icon']),
		]) ?>

		<?= $this->Form->input('type', ['type' => 'hidden']); ?>

		<?= $this->Form->end() ?>
	</div>

	<div class="col-md-4">

		<?php if (in_array($authUser->role, ['A', 'O'])): ?>

			<?= $this->Form->create(null, ['type' => 'get', 'id' => 'show-pending']); ?>

			<?= $this->Form->input('pending', [
				'type' => 'checkbox',
				'label' => 'Show Pending Only',
				'hiddenField' => false
			]); ?>

			<?= $this->Form->end(); ?>

		<?php endif; ?>

	</div>

</div>

<div class="row hidden-print">
	<div class="col-xs-24 m-b-lg">
		
		<div id="alphabetic-search">
			<?php
			foreach (range('A', 'Z') as $letter) {
				if (isset($this->request->data['letter']) && $this->request->data['letter'] == $letter)
					echo $this->Html->link($letter, ['action' => 'index'], ['class' => 'active']);
				else
					echo $this->Html->link($letter, ['action' => 'index', '?' => ['letter' => $letter, 'type' => $this->request->data('type')]]);
			}
			?>
		</div>
		
	</div>
</div>

<div class="blue-bg padding-lg" style="margin-left: -30px; margin-right: -30px;">
	
	<div class="row">
		
		<?php if(!$groups->isEmpty()): foreach ($groups as $group): ?>
			
			<div class="col-sm-12 col-lg-8">
				<div class="contact-box group-box equal-height">
					
					<div class="actions">
						<?php
							$actions = '';

							if (in_array($authUser->role, ['A']) || $group->isAdmin($authUser)) {

								$actions = $this->Html->link($icons['edit'],
									['action' => 'edit', $group->id],
									['escape' => false, 'title' => 'Edit', 'data-toggle' => 'tooltip', 'class' => 'btn btn-info btn-xs btn-transparent btn-icon']
								);

								if (in_array($authUser->role, ['A']))
									$actions .= $this->Form->postLink($icons['delete'], ['action' => 'delete', $group->id], [
										'escape' => false,
										'title' => 'Delete',
										'confirm' => 'Are you sure to delete the group "' . $group->name . '"?',
										'class' => 'btn btn-danger btn-xs btn-transparent btn-icon'
									]);
								
							}
							
							echo $actions;
						?>
					</div>
					
					<div class="row">
						<div class="col-md-8 text-center">
							<?= $this->Html->link(
								$this->Html->image($group->profile_thumb),
								['action' => 'view', $group->id],
								['escape' => false]
							); ?>
						</div>
						<div class="col-md-16">
							
							<h3 class="contact-name">
								<?= $this->Html->link($group->name, ['action' => 'view', $group->id]); ?>
							</h3>

							<?php if (!empty($requestStatus = $group->getRequestStatus($authUser))): ?>

								<p>
									<?= $icons['info-circle']; ?> Membership: <strong><?= $groupRequestStatuses[$requestStatus]; ?></strong>
								</p>

							<?php endif; ?>

							<!-- <?php if (!empty($group->pending)): ?>

								<p>
									<span class="label label-warning">Pending</span>
								</p>

							<?php endif; ?> -->
							<p><?= $group->description ?></p>
							
						</div>
					</div>
					
					<?= $this->Html->link(
						'View Group ' . $icons['caret-right'],
						['action' => 'view', $group->id],
						['escape' => false, 'class' => 'view-profile m-t-sm']
					); ?>
				
				</div>
			</div>
		
		<?php endforeach; endif; ?>
	</div>
</div>

<div class="m-t-lg">
	<?= $this->element('pagination', ['noPadding' => true]); ?>
</div>