<?php if (!empty($group->users)): ?>

	<ul class="list-group user-list-group">

		<?php foreach ($group->users as $user): ?>

			<li class="list-group-item">
				<?= $this->Html->link(
					sprintf(
						'%s %s',
						$this->Html->image($user->profile_thumb, ['class' => 'img-circle']),
						$user->name
					),
					['controller' => 'Users', 'action' => 'view', $user->id],
					['escape' => false]
				); ?>

				<div class="pull-right checkbox-group-admin">

					<?= $this->Form->input('admin_'.$user->id, [
						'type' => 'checkbox',
						'label' => 'Admin',
						'default' => $user->_joinData->admin,
						'data-id' => $user->_joinData->id,
						'onclick' => 'groupAdmin(this)'
					]); ?>

				</div>

			</li>

		<?php endforeach; ?>

	</ul>

<?php endif; ?>