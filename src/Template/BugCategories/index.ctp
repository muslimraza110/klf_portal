<div class="ibox float-e-margins">
	<div class="ibox-title">
		<h5>Bug Categories</h5>
	</div>

	<div class="ibox-content">
		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
				<?php

				echo $this->Html->tableHeaders([
					'Category',
					'Description',
					''
				]);

				?>
				</thead>
				<tbody>
				<?php
				if (!$bugCategories->isEmpty()) {
					foreach ($bugCategories as $category) {

						$actions = $this->Html->link(
							'Edit',
							['action' => 'edit', $category->id],
							['class' => 'btn btn-sm btn-primary', 'escape' => false, 'title' => 'edit']
						);
						$actions .= $this->Html->link(
							'Delete',
							['action' => 'delete', $category->id],
							['class' => 'btn btn-sm btn-danger', 'escape' => false, 'confirm' => 'Are you sure you want to delete '.' '.$category->name.'?']
						);

						$rows[] = [
							$category->name,
							$category->description,
							[$actions, ['class' => 'compact']]
						];
					}
				}

				else {
					$rows[] = [
						['No categories found.', ['colspan' => 3]]
					];
				}
				echo $this->Html->tableCells($rows);
				?>
				</tbody>
			</table>
		</div>
	</div>
</div>