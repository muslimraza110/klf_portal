<div class="ibox">
	<div class="ibox-title">
		<h5><?= $title ?></h5>
	</div>
	<div class="ibox-content">

		<?php echo $this->Form->create($bugCategories); ?>
		<div class="row">
			<div class="col-xs-12">
				<?php echo $this->Form->input('name'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-24">
				<?php echo $this->Form->input('description'); ?>
			</div>
		</div>

		<?php echo $this->Form->submit('Save', ['class' => 'btn btn-primary']); ?>

		<?php echo $this->Form->end(); ?>
	</div>
</div>