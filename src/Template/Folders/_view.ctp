<?php

echo $this->Html->script($this->Asset->scripts('upload'), ['block' => true]);
echo $this->Html->css('AssetManager.style');

$scriptBlock = <<<JS

function ajaxFileListLoad() {

	var _ajaxFileList = $('#ajax-file-list');
	
	if (_ajaxFileList.hasClass('working'))
		return;
	
	_ajaxFileList.addClass('working');
	
	$.get(
			'{$this->Url->build(['controller' => 'Files', 'action' => 'ajax_view', $folder->id])}',
			function(data) {
				_ajaxFileList
						.html(data)
						.removeClass('working');
			}
	);
	
};

$(function() {
	
	// Hook into AssetManager upload in plugins/AssetManager/webroot/js/script.js
	$('#upload').bind('fileuploaddone', function(e, data) {
		ajaxFileListLoad();
	});
	
	ajaxFileListLoad();
	
});

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>


<div class="row">
	<div class="col-md-16 col-xs-24">

		<div id="ajax-file-list"></div>

	</div>
	<div class="col-md-8 col-xs-24" style="margin-top: -12px;">

		<?= $this->Asset->upload(); ?>

	</div>
</div>

