<?= $this->Form->create($folder); ?>

<div class="row">
	<div class="col-xs-24">

		<div class="ibox">

			<div class="ibox-content">

				<?= $this->Form->input('parent_id', ['label' => 'Parent Folder', 'empty' => '-- Top Level --']); ?>

				<?= $this->Form->input('name'); ?>

			</div>

		</div>

	</div>
</div>

<div class="submit-big">
	<?= $this->Form->submit('Save', ['class' => 'btn btn-primary']); ?>
</div>

<?= $this->Form->end(); ?>