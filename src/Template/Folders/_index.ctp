<?php
	$this->Html->script([
		'nestedSortable/jquery.mjs.nestedSortable',
	], ['block' => true]);

	$this->Html->scriptBlock("
    //Add the name field to the list array
    function recursive_list(list) {
      for(var i=0 in list){
        var name = $('#item_'+ list[i].id).children().children('.label-name').html();
        var newItem = $('#item_'+ list[i].id).children().children().hasClass('new-item');
        var deleteItem = $('#item_'+ list[i].id).hasClass('delete');

        list[i].isNew = false;
        list[i].delete = false;

        if(deleteItem)
          list[i].delete = true;

        if(newItem)
          list[i].isNew = true;

        list[i].name = name;

        //has children
        if('children' in list[i])
          recursive_list(list[i].children);

      }
    }
		
    $(function() {
		
      $('.sortable').nestedSortable({
        placeholder: 'sortable-placeholder',
        handle: '.fa-bars',
        items: 'li',
        toleranceElement: '> div',
        isTree: true,
        stop: function(event, ui) {
          var list = $('ol.sortable').nestedSortable('toHierarchy', {startDepthCount: 0});
          //console.log(list);
          //Modify list by adding extra data to it
          recursive_list(list);
          //console.log(list);
          $.ajax({
            method: 'POST',
            url: '". $this->Url->build(['action' => 'ajax_save']) ."',
            data: {list: list}
          });
        }
      });
			
    });
  ", ['block' => true]);
?>

<div class="like-table-bordered">
	
	<div class="like-table-header">Folders</div>
	
	<ol class="sortable mjs-nestedSortable-branch">
		<?= $this->element('recursive_folders', ['folders' => $folders, 'user' => $user]); ?>
	</ol>
	
</div>