<?= $this->element('pagination', ['noPadding' => true]); ?>

<br>
<div class="table-responsive">
	<table class="table table-bordered">
		<thead>
		<?php

		$headers = [
			$this->Paginator->sort('Users.first_name', 'Name'),
			$this->Paginator->sort('ForumPosts.topic', 'Post Title'),
			$this->Paginator->sort('created', 'Subscribed')
		];

		echo $this->Html->tableHeaders($headers);

		?>
		</thead>
		<tbody>
		<?php

		if (!$forumPostSubscriptions->isEmpty()) {

			foreach ($forumPostSubscriptions as $subscription) {

				$row = [
					$this->Html->link(
						$subscription->user->name,
						['controller' => 'Users', 'action' => 'view', $subscription->user_id]
					),
					$this->Html->link(
						$subscription->forum_post->topic,
						['controller' => 'ForumPosts', 'action' => 'view', $subscription->forum_post_id]
					),
					$subscription->created
				];

				echo $this->Html->tableCells($row);

			}

		} else {
			echo $this->Html->tableCells([[
				['No subscribers found.', ['colspan' => count($headers)]]
			]]);
		}

		?>
		</tbody>
	</table>
</div>

<?= $this->element('pagination', ['noPadding' => true]); ?>