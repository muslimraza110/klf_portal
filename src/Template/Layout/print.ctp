<?= $this->element('Template/head'); ?>

<div class="row">
  <div class="col-xs-24">

    <div class="wrapper wrapper-content animated fadeInRight">

      <?= $this->Flash->render() ?>
      <?= $this->fetch('content') ?>

    </div>

  </div>
</div>

<?= $this->element('Template/foot'); ?>
