<?= $this->element('Template/head'); ?>

<div class="middle-box" style="max-width: 300px;">
	
	<div class="ibox">
		<div class="ibox-content">
			
			<?= $this->Flash->render() ?>
			<?= $this->fetch('content') ?>
			
		</div>
	</div>
	
</div>

<?= $this->element('Template/foot'); ?>