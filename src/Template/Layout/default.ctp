<?= $this->element('Template/head'); ?>

<div id="wrapper" ng-controller="ChatController as chatCtrl">

	<div id="page-wrapper" class="white-bg">
		
		<?= $this->element('Template/topnav'); ?>
		<?= $this->element('Template/right_sidebar'); ?>
    <?= $this->element('banner'); ?>
    
    <div class="wrapper wrapper-content">
			
			<?= $this->Flash->render() ?>
			<?= $this->fetch('content') ?>
			
    </div>
    
    <?= $this->element('Template/copyright'); ?>
  </div>
</div>

<?= $this->element('Template/foot'); ?>
