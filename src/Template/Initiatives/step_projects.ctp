<?php

$projectsArrayJSON = json_encode($projectsArray);
$projectsSessionJSON = json_encode($sessionInitiative->projects);

$this->Html->scriptBlock(<<<JS

var addProject = function (self) {
	
	var _projects = $('#projects'),
	nextKey = _projects.data('next-key');
	
	var _selected = $('#project-list').val();
	var projectsArray = $projectsArrayJSON;
	
	if (_selected) {
		
		var _text = $('#project-list :selected').text();
		var charitiesNameId = '#charities-names-' + nextKey;
		var charitiesPercentagesId = '#charities-percentages-' + nextKey;
		
		_projects.append(
			$('#project-prototype').html()
				.replace(/projects.name/g, _text)
				.replace(/projects.id/g, _selected)
				.replace(/000/g, nextKey)
		);
		
		$.each(projectsArray[_selected]['groups'], function (index, group) {
			_projects.find(charitiesNameId).append('<li>' + group.name + '</li>');
			_projects.find(charitiesPercentagesId).append('<li>' + group._joinData.percent + ' %</li>');
		});
		
		_projects.data('next-key', nextKey + 1);
		
		$("#project-list option[value='" + _selected + "']").remove();

	}
	
};

var removeProject = function (self, id, name) {

	if (window.confirm('Are you sure you want to remove this project?')) {
		$(self).parents('.project').first().remove();
		$('#project-list').append('<option value="' + id + '">' + name + '</option>');
	}

};

$(function() {
	
	var projectsSession = $projectsSessionJSON;
	
	$.each(projectsSession, function (index, value) {
		$("#project-list option[value='" + value.id + "']").remove();
	});
	
});

JS
, ['block' => true]);

?>

<div id="project-prototype" style="display: none;">

		<div class="project m-b-md">
			<div class="ibox-content">
				<div class="row">

					<div class="col-xs-10 col-xs-offset-2">

						<?= $this->Html->link('projects.name',
							['controller' => 'projects', 'action' => 'view', 'projects.id']
						); ?>

					</div>

					<div class="col-xs-4">
						<div class="input-group">
							<?= $this->Form->input('projects.000._joinData.percentage', [
								'type' => 'text',
								'label' => false,
								'placeholder' => 0
							]) ?>
							<span class="input-group-addon">%</span>
						</div>
					</div>

					<?= $this->Form->form('projects.000.id', ['type' => 'hidden', 'value' => 'projects.id']); ?>

					<div class="col-xs-6">

						<?= $this->Html->link('Remove', 'javascript:;', [
							'class' => 'btn btn-danger pull-left',
							'onclick' => "removeProject(this, 'projects.id', 'projects.name')"
						]) ?>

					</div>

				</div>

				<div class="row">
					<div class="col-xs-10 col-xs-offset-2">
						<ul id="charities-names-000"></ul>
					</div>
					<div class="col-xs-4">
						<ul id="charities-percentages-000" style="list-style-type:none;padding-left:0"></ul>
					</div>
				</div>
			</div>

		</div>

</div>



<?= $this->Form->create($sessionInitiative, ['novalidate']); ?>
<div class="row">
	<div class="col-md-16 col-md-offset-4">

		<div class="ibox ibox-shadow">
			<div class="ibox-title">

				<?= $this->StepProgress->render([
					['name' => 'Initiative'],
					['name' => 'Projects', 'active' => true],
					['name' => 'Donations'],
					['name' => 'Fundraisings'],
					['name' => 'Summary']
				]); ?>

			</div>
		</div>

		<div class="ibox">
			<div class="ibox-title">
				<h5>Projects</h5>
			</div>

			<div class="ibox-content">

				<div class="row">
					<div class="col-md-16">
						<?= $this->Form->input('project_list', [
							'options' => $projects,
							'empty' => '---',
							'label' => false,
						]); ?>
					</div>

					<div class="col-md-8">
						<?= $this->Html->link('Add Project', 'javascript:;', [
							'class' => 'btn btn-primary pull-left',
							'onclick' => 'addProject(this)'
						]) ?>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-24">

						<div id="projects" data-next-key="<?= count($sessionInitiative->projects); ?>">

							<?php if (isset($sessionInitiative->projects)): ?>
								<?php foreach ($sessionInitiative->projects as $key => $project): ?>

									<div class="project m-b-md">
										<div class="ibox-content">
											<div class="row">

												<div class="col-xs-10 col-xs-offset-2">

													<?= $this->Html->link($project->name,
														['controller' => 'projects', 'action' => 'view', $project->id]
													); ?>

												</div>

												<div class="col-xs-4">

													<?= $this->Form->input('projects.' . $key . '._joinData.percentage', [
														'type' => 'text',
														'label' => false,
														'placeholder' => 0,
														'append' => '%'
													]) ?>

												</div>

												<?= $this->Form->input('projects.' . $key . '.id', ['hidden' => true]) ?>

												<div class="col-xs-6">

													<?= $this->Html->link('Remove', 'javascript:;', [
														'class' => 'btn btn-danger pull-left',
														'onclick' => "removeProject(this, '" . $project->id . "', '" . $project->name . "')"
													]) ?>

												</div>

											</div>
											<div class="row">
												<div class="col-xs-10 col-xs-offset-2">
													<ul id="charities-names-<?= $key ?>">

														<?php foreach ($projectsArray[$project->id]->groups as $group) : ?>

															<li><?= $group->name ?></li>

														<?php endforeach; ?>

													</ul>
												</div>
												<div class="col-xs-4">
													<ul id="charities-percentages-<?= $key ?>" style="list-style-type:none;padding-left:0">
														<?php foreach ($projectsArray[$project->id]->groups as $group) : ?>

															<li><?= $group['_joinData']->percent ?> %</li>

														<?php endforeach; ?>
													</ul>
												</div>
											</div>
										</div>
									</div>

								<?php endforeach ?>
							<?php endif ?>

						</div>

					</div>
				</div>

			</div>

		</div>

	</div>
</div>

<div class="row">

	<div class="col-md-14 col-md-offset-5">
		<div class="submit-big">
			<div class="row">
				<div class="col-xs-20">
					<?= $this->Form->submit('previous', ['class' => 'btn btn-primary', 'name' => 'submit']); ?>
				</div>
				<div class="col-xs-4">
					<?= $this->Form->submit('next', ['class' => 'btn btn-primary', 'name' => 'submit']); ?>
				</div>
			</div>
		</div>
	</div>

</div>
<?= $this->Form->end(); ?>