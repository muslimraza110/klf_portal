<?= $this->Form->create($sessionInitiative, ['novalidate']); ?>
<div class="row">
	<div class="col-md-16 col-md-offset-4">

		<div class="ibox ibox-shadow">
			<div class="ibox-title">

				<?= $this->StepProgress->render([
					['name' => 'Initiative', 'active' => true],
					['name' => 'Projects'],
					['name' => 'Donations'],
					['name' => 'Fundraisings'],
					['name' => 'Summary']
				]); ?>

			</div>
		</div>

		<div class="ibox">
			<div class="ibox-title">
				<h5>Initiative</h5>
			</div>

			<div class="ibox-content">

				<div class="row">
					<div class="col-md-16">
						<?= $this->Form->input('name', [
							'label' => false,
							'placeholder' => 'Name'
						]) ?>
					</div>

					<div class="col-md-8">
						<?= $this->Form->input('show_on_dashboard', [
							'type' => 'checkbox',
						]) ?>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-24">

						<?= $this->Form->input('description', [
							'label' => false,
							'placeholder' => 'Description'
						]); ?>

					</div>
				</div>

			</div>

		</div>


	</div>
</div>

<div class="row">

	<div class="col-md-14 col-md-offset-5">
		<div class="submit-big">
			<div class="row">
				<div class="col-xs-20">
				</div>
				<div class="col-xs-4">
					<?= $this->Form->submit('next', ['class' => 'btn btn-primary', 'name' => 'submit']); ?>
				</div>
			</div>
		</div>
	</div>

</div>
<?= $this->Form->end(); ?>