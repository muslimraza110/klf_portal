<div class="row">
	<div class="col-md-16 col-md-offset-4">
		<div class="ibox">
			<div class="ibox-title">
				<h4>E-Blasts</h4>
			</div>
			<?php if (!empty($initiativeEmails)): ?>
				<div class="ibox-content" id="email-list">
					<?php foreach ($initiativeEmails as $initiativeEmail): ?>
						<?php
							$emailObject = unserialize($initiativeEmail->content);
						?>
						<div class="ibox">
							<div class="ibox-title">
								<h4 style="text-transform: none; font-weight: bolder;"><?= $emailObject['_subject'] ?></h4>
								Sent: <?= $initiativeEmail->created->format('M d, Y h:i A') ?>
								<div class="ibox-tools">

									<?php
									if (in_array($authUser->role, ['A','O'])) {
										echo $this->Html->link('UNPIN',
											['action' => 'pinEmail', 'controller' => 'Initiatives', $initiativeEmail->initiative_id, '?' => ['pin' => false, 'date_group' => $initiativeEmail->created->toUnixString()]],
											['escape' => false, 'class' => 'btn btn-sm btn-primary m-r-xs', 'confirm' => 'Are you sure you would like to unpin this email?']);
									}
									 ?>
									<?= $this->Html->link(
										$icons['chevron-down'],
										'javascript:;',
										[
											'escape' => false,
											'href' => '#email-' . $initiativeEmail->id,
											'class' => 'btn btn-icon btn-info collapse-link',
										]
									); ?>
								</div>

							</div>
							<div id="email-<?= $initiativeEmail->id; ?>" class="ibox-content" style="padding-left:4%; display:none;">
								<?= $emailObject['viewVars']['htmlContent'] ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
