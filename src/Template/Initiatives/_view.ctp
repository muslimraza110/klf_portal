<?php

$pledgersJSON = json_encode($initiative->pledgers);

if ($initiative->hasMatchFundraising()) {
	$currencyTargets = json_encode([4,5]);
	$dateTargets = json_encode([6]);
} else {
	$currencyTargets = json_encode([3]);
	$dateTargets = json_encode([4]);
}

$scriptBlock = <<<JS


	$(function() {

			// ################ SORTING CODE ####################
			
		jQuery.extend( jQuery.fn.dataTableExt.oSort, {
			"currency-pre": function ( a ) {
				a = (a==="-") ? 0 : a.replace( /[^\d\-\.]/g, "" );
				return parseFloat( a );
			},

			"currency-asc": function ( a, b ) {
				return a - b;
			},

			"currency-desc": function ( a, b ) {
				return b - a;
			}
		} );

		$(document).ready( function () {
			//$.fn.dataTable.moment(); //'M/DD/GG, H:mm A'
			//$.fn.dataTable.moment('M/DD/YY, h:mm A'); 
			$('#pledges-table').DataTable({
				columnDefs: [
				 {
				 	type: 'currency', targets: $currencyTargets
				 },
				 {
				 	type: 'date', targets: $dateTargets
				 }
			 ],
				"info": false,
				"ordering": true,
				"searching": false,
				"paging": false,
				"order": [],
				"scrollX": true
			});
		});
		
	});
JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);
?>


<div class="ibox">

	<div class="ibox-title">
		<h3><?= $initiative->name; ?></h3>
		<?php if ($authUser->role == 'A'): ?>
			<div class="ibox-tools pull-right">

				<?= $this->Html->link(
					$icons['envelope'] . ' Communications',
					['action' => 'communication', $initiative->id],
					['class' => 'btn btn-primary btn-icon', 'escape' => false]
				);?>

				<?= $this->Html->link(
					$icons['envelope'] . ' Send Update',
					['action' => 'view_eblast', $initiative->id, '?' => ['update' => true]],
					['class' => 'btn btn-primary btn-icon', 'escape' => false]
				);?>

				<?= $this->Html->link(
					$icons['envelope'] . ' E-Blast',
					['action' => 'view_eblast', $initiative->id],
					['class' => 'btn btn-primary btn-icon', 'escape' => false]
				);?>

			</div>
		<?php endif;?>	
	</div>

	<div class="ibox-content">

		<div class="row">

			<div class="col-xs-24 text-center">

				<h3>

					Total Initiative:

					<strong>
						<?= $this->Number->format($initiative->currentPledgeAmount, ['places' => 0, 'before' => 'USD ']) ?>
						of
						<?= $this->Number->format($initiative->goalPledgeAmount, ['places' => 0, 'before' => 'USD ']) ?>
					</strong>

				</h3>

				<?php

				$targetPercentage = ($initiative->currentPledgeAmount / $initiative->goalPledgeAmount) * 100;

				$amount = <<<HTML
					<div class="progress" style="margin-bottom: 0; border: 1px solid #aaa;">
					<div style="width: $targetPercentage%" class="progress-bar progress-bar-success"></div>
					</div>
HTML;

				echo $amount;

				?>

			</div>
		</div>

		</br>

		<div class="row">

			<div class="col-xs-24">

				<h4>Fundraisings</h4>

				<table class="table table-bordered">

					<thead>
					<?= $this->Html->tableHeaders([
						'Overview',
						'Type',
						'Amount'
					]); ?>
					</thead>

					<tbody>
					<?php
					if (!empty($initiative->fundraisings)) {

						$rows = [];

						foreach ($initiative->fundraisings as $fundraising) {

							$rows[] = [
								$this->Html->link(
									$fundraising->name,
									'#fundraising-' . $fundraising->id,
									['escape' => false]
								),
								$fundraising->typeLabel(),
								$this->Number->format($fundraising->pledgedAmount, ['places' => 0, 'before' => 'USD ']) . ' / ' .$this->Number->format($fundraising->totalPledgedAmount, ['places' => 0, 'before' => 'USD '])

							];

						}

						echo $this->Html->tableCells($rows);

					}
					?>

					</tbody>

				</table>

				<h4>Projects</h4>

				<table class="table table-bordered">

					<thead>
					<?= $this->Html->tableHeaders([
						'Name',
						'Percentage of Initiative',
						'Amount',
						'Charities'
					]); ?>
					</thead>

					<tbody>
					<?php
					if (!empty($initiative->projects)) {

						$rows = [];

						foreach ($initiative->projects as $project) {

							$name = $this->Html->link(
									$project->name,
									['action' => 'view', 'controller' => 'Projects', $project->id],
									['escape' => false]
								);

							$projectPercentage = $project['_joinData']['percentage'];
							$projectAmount = $initiative->goalPledgeAmount * ($projectPercentage / 100);

							$charities = [];

							foreach ($project->groups as $group) {

								$charityPercentage = $group['_joinData']['percent'];
								$charities[] = $group->name . ' - '
									. $charityPercentage . '% - '
									. $this->Number->format($projectAmount * ($charityPercentage / 100), ['places' => 0, 'before' => 'USD ']);

							}

							$rows[] = [
								[$name, ['style' => 'vertical-align:top']],
								[$projectPercentage . '%', ['style' => 'vertical-align:top']],
								[$this->Number->format($projectAmount, ['places' => 0, 'before' => 'USD ']), ['style' => 'vertical-align:top']],
								[$this->Html->nestedList($charities, ['class' => 'list-unstyled']), ['style' => 'vertical-align:top']]
							];

						}

						echo $this->Html->tableCells($rows);

					}
					?>

					</tbody>

				</table>

				<?php
				foreach ($initiative->fundraisings as $fundraising) {
					echo $this->element('fundraising_pledge', ['fundraising' => $fundraising, 'isInitiative' => true]);
				}
				?>

			</div>
		</div>

		<div class="ibox">
			<div class="ibox-title">
				<h3>Pledgers</h3>
			</div>
			<div class="ibox-content">

				<table class="table table-bordered" id="pledges-table" style="width: 100%">
					<thead>
					<?php

					if (in_array($authUser->role, ['A', 'O'])) {

						$headers = [
							'Name',
							'matching' => 'Matching',
							'Fundraising',
							'Charity',
							'Pledge Amount',
							'match' => 'Match Amount'
						];

					} else {

						$headers = [
							'Name',
							'matching' => 'Matching',
							'Fundraising',
							'Charity',
							'Amount',
							'match' => 'Match Amount'
						];

					}

					if (!$initiative->hasMatchFundraising()) {
						unset($headers['match']);
						unset($headers['matching']);
					}

					if (in_array($authUser->role, ['A', 'O'])) {
						$headers['date'] = 'Date';
						$headers[] = '';
					} else {
						$headers['date'] = ['Date' => ['hidden']];
					}

					echo $this->Html->tableHeaders($headers);

					?>
					</thead>

					<tbody>
					<?php

					$pledgers = $initiative->pledgers;
					$pledgesGrandTotal = 0;

					if (!empty($pledgers)) {

						foreach ($pledgers as $pledge) {

							$actions = [
								'edit' => $this->Html->link('EDIT',
									['action' => 'editPledge', 'controller' => 'Initiatives', $pledge->id],
									['class' => 'btn btn-xs btn-primary']),
								'delete' => $this->Html->link('DELETE',
									['action' => 'deletePledge', 'controller' => 'Initiatives', $pledge->id],
									['class' => 'btn btn-xs btn-danger', 'style' => 'margin-left: 5px']),
							];

							$name = $this->Html->link(
								$pledge->user->last_name . ', ' . $pledge->user->first_name,
								['controller' => 'Users', 'action' => 'view', $pledge->user->id],
								['escape' => false]
							);

							if ($pledge->anonymous) {
								$name .= ' <span style="color:red">*</span>';
							}

							$amount = $this->Number->format($pledge->amount, ['places' => 0, 'before' => 'USD ']);

							$matchAmount = '--';

							if ($initiative->hasMatchFundraising() && $pledge->fundraising->type == 'M') {
								$matchAmount = $this->Number->format($pledge->amount, ['places' => 0, 'before' => 'USD ']);
							}

							if (in_array($authUser->role, ['A', 'O'])) {

								$matcherNames = [];

								if (!empty($pledge->matchers())) {

									foreach ($pledge->matchers() as $matcher) {

										if ($matcher->anonymous) {
											$matcherNames[] = $matcher->user->fullName . '<span style="color:red">*</span>';
										} else {
											$matcherNames[] = $matcher->user->fullName;
										}

									}

								}

								$row = [
									$name,
									'matching' => implode(', ', $matcherNames),
									$pledge->fundraising->name,
									!empty($pledge->group) ? $pledge->group->name : '--',
									$amount . ($pledge->hide_amount ? '*' : ''),
									'match' => $matchAmount,
									$pledge->created,
									[$actions, ['class' => 'actions text-right']]
								];

							} else {

								$matcherNames = [];

								if (!empty($pledge->matchers())) {

									foreach ($pledge->matchers() as $matcher) {

										if ($matcher->anonymous) {
											$matcherNames[] = h('<Anonymous>');
										} else {
											$matcherNames[] = $matcher->user->fullName;
										}

									}

								}

								$row = [
									(empty($pledge->anonymous) ? $name : ($authUser->id == $pledge->user_id ? $name . ' ' : ' ') . h('(Anonymous)')),
									'matching' => implode(', ', $matcherNames),
									$pledge->fundraising->name,
									!empty($pledge->group) ? $pledge->group->name : '--',
									empty($pledge->hide_amount) ? $amount : h('<Anonymous>'),
									'match' => $matchAmount,
									[$pledge->created, ['hidden']]
								];

							}

							if (!$initiative->hasMatchFundraising()) {
								unset($row['match']);
								unset($row['matching']);
							}

							$pledgesGrandTotal += $pledge->amount;

							echo $this->Html->tableCells($row);

						}

					} else {

						echo $this->Html->tableCells([[
							['No pledgers found.', ['colspan' => count($headers)]]
						]]);

					}

					?>
					</tbody>
					<tfoot>
					<?php

					if (in_array($authUser->role, ['A', 'O'])) {
						if ($initiative->hasMatchFundraising()) {
							$row = [
								'',
								'',
								'',
								['<strong>Grand Total:<strong>', ['class' => 'text-right']],
								'<strong>USD ' . number_format($pledgesGrandTotal) . '</strong>',
								'',
								'',
								''
							];
						} else {
							$row = [
								'',
								'',
								['<strong>Grand Total:<strong>', ['class' => 'text-right']],
								'<strong>USD ' . number_format($pledgesGrandTotal) . '</strong>',
								'',
								''
							];
						}
					} else {
						if ($initiative->hasMatchFundraising()) {
							$row = [
								'',
								'',
								'',
								['<strong>Grand Total:<strong>', ['class' => 'text-right']],
								'<strong>USD ' . number_format($pledgesGrandTotal) . '</strong>',
								''
							];
						} else {
							$row = [
								'',
								'',
								['<strong>Grand Total:<strong>', ['class' => 'text-right']],
								'<strong>USD ' . number_format($pledgesGrandTotal) . '</strong>',
							];
						}
					}

					echo $this->Html->tableCells($row);

					?>
					</tfoot>
				</table>

			</div>
		</div>
	</div>

</div>

