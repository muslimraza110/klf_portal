<div class="ibox">
	<div class="ibox-title">
		<h3><?= $title ?></h3>
	</div>

	<div class="ibox-content">

		<?= $this->Form->create($fundraisingPledge); ?>

		<div class="row">

			<div class="col-sm-8">
				<label>Fundraising:</label>
				<?= $fundraisingPledge->fundraising->name ?>
			</div>

			<div class="col-sm-8">
				<label>Pledger:</label>
				<?= $this->Html->link($fundraisingPledge->user->name, [
					'action' => 'view', 'controller' => 'Users', $fundraisingPledge->user_id
				]) ?>
			</div>

			<div class="col-sm-8">
				<label>Date:</label>
				<?= $fundraisingPledge->created ?>
			</div>

		</div>

		<hr>

		<div class="row">

			<div class="col-xs-12 col-sm-6">
				<?= $this->Form->input('group_id'); ?>
			</div>

			<?php if ($fundraisingPledge->fundraising->type != 'B'): ?>

				<div class="col-xs-12 col-sm-6">
					<?= $this->Form->input('amount', [
						'prepend' => '$'
					]); ?>
				</div>

			<?php else: ?>
			<div class="col-xs-12 col-sm-6">
				<?php $range = range(0, $fundraisingPledge->fundraising->max_blocks); unset($range[0]); ?>
				<?= $this->Form->input('block_amount', [
					'label' => 'Block Amount',
					'value' => ($fundraisingPledge->fundraising->blocks /  ($fundraisingPledge->fundraising->amount / $fundraisingPledge->fundraising->getUserAmountPledged($authUser->id))),
					'options' => $range
				]); ?>
			</div>
			<?php endif; ?>

			<div class="col-xs-12 col-sm-6">
				<label>&nbsp;</label>
				<?= $this->Form->input('anonymous'); ?>
			</div>

			<div class="col-xs-12 col-sm-6">
				<label>&nbsp;</label>
				<?= $this->Form->input('hide_amount'); ?>
			</div>

		</div>

		<?= $this->Form->submit('SUBMIT', ['bootstrap-type' => 'primary', 'class' => 'btn-lg pull-right']) ?>

		<?= $this->Form->end() ?>

	</div>

</div>