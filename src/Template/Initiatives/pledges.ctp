<div class="ibox">
	<div class="ibox-title">
		<h3>Pledges</h3>
	</div>

	<div class="ibox-content">

		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
				<?php

				$headers = [
					'Name',
					'Matching',
					'Fundraising',
					'Charity',
					'Amount',
					'Match Amount',
					'Date',
					''
				];

				echo $this->Html->tableHeaders($headers);

				?>
				</thead>
				<tbody>
				<?php

				if (!$fundraisingPledges->isEmpty()) {

					foreach ($fundraisingPledges as $pledge) {

						$actions = [
							'edit' => $this->Html->link('EDIT',
								['action' => 'editPledge', 'controller' => 'Initiatives', $pledge->id],
								['class' => 'btn btn-sm btn-primary']),
							'delete' => $this->Html->link('DELETE',
								['action' => 'deletePledge', 'controller' => 'Initiatives', $pledge->id],
								['class' => 'btn btn-sm btn-danger', 'style' => 'margin-left: 5px', 'confirm' => 'Are you sure you want to delete this pledge?']),
						];

						$name = $this->Html->link(
							$pledge->user->name,
							['controller' => 'Users', 'action' => 'view', $pledge->user->id],
							['escape' => false]
						);

						if ($pledge->anonymous) {
							$name .= ' <span style="color:red">*</span>';
						}

						$matchAmount = '--';

						if ($pledge->fundraising->type == 'M') {
							$matchAmount = $this->Number->format($pledge->amount, ['places' => 0, 'before' => 'USD ']);
						}

						$matcherNames = [];

						if (!empty($pledge->matchers())) {

							foreach ($pledge->matchers() as $matcher) {

								if ($matcher->anonymous) {
									$matcherNames[] = $matcher->user->fullName . '<span style="color:red">*</span>';
								} else {
									$matcherNames[] = $matcher->user->fullName;
								}

							}

						}

						$rows[] = [
							$name,
							implode(', ', $matcherNames),
							$pledge->fundraising->name,
							!empty($pledge->group) ? $pledge->group->name : '--',
							$this->Number->format($pledge->amount, ['places' => 0, 'before' => 'USD ']),
							'match' => $matchAmount,
							$pledge->created,
							[$actions, ['class' => 'actions text-right']]
						];

					}

				} else {
					$rows[] = [['No Pledges Found.', ['colspan' => count($headers)]]];
				}

				echo $this->Html->tableCells($rows);

				?>
				</tbody>
			</table>
		</div>

	</div>
</div>