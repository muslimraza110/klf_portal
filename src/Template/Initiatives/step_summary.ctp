<?= $this->Form->create($sessionInitiative, ['novalidate']); ?>
<div class="row">
	<div class="col-md-16 col-md-offset-4">

		<div class="ibox ibox-shadow">
			<div class="ibox-title">

				<?= $this->StepProgress->render([
					['name' => 'Initiative'],
					['name' => 'Projects'],
					['name' => 'Donations'],
					['name' => 'Fundraisings'],
					['name' => 'Summary', 'active' => true]
				]); ?>

			</div>
		</div>


		<div class="ibox">
			<div class="ibox-title">
				<h5>Summary</h5>
			</div>

			<div class="ibox-content">

				<hr>
				<div class="text-center">
					<?= $this->Form->input('eblast', [
						'type' => 'checkbox',
						'label' => [
							'text' => '<b>Send E-Blast</b>',
							'escape' => false
						]
					]) ?>
				</div>
				<hr>

				<div class="row">
					<div class="text-center">
						<strong>Total Target:</strong>
					</br>
						<?= $this->Number->format($sessionInitiative->goalPledgeAmount, ['places' => 0, 'before' => 'USD ']) ?>
					</div>

				</div>

				<?php

				$summary = [];

				$summary['Name'] = 'Name: ' . $sessionInitiative->name;
				$summary['Description'] = 'Description: ' . $sessionInitiative->description;

				$summary['Projects'] = empty($sessionInitiative->projects) ? 'Projects: ' . 'None' : [];
				foreach ($sessionInitiative->projects as $key => $project) {
					$summary['Projects'][] = $project->name;
				}

				$summary['Charities'] = empty($sessionInitiative->groups) ? 'Groups: ' . 'None' : [];
				foreach ($sessionInitiative->groups as $key => $group) {
					$summary['Charities'][] = $group->name;
				}

				$summary['Fundraisings'] = empty($sessionInitiative->fundraisings) ? 'Projects: ' . 'None' : [];
				foreach ($sessionInitiative->fundraisings as $key => $fundraising) {
					$summary['Fundraisings'][] = $fundraising->name;
				}

				echo $this->Html->nestedList($summary);

				?>

			</div>
		</div>

	</div>

</div>


<div class="row">

	<div class="col-md-14 col-md-offset-5">
		<div class="submit-big">
			<div class="row">
				<div class="col-xs-20">
					<?= $this->Form->submit('previous', ['class' => 'btn btn-primary', 'name' => 'submit']); ?>
				</div>
				<div class="col-xs-4">
					<?= $this->Form->submit('submit', ['class' => 'btn btn-primary', 'name' => 'submit']); ?>
				</div>
			</div>
		</div>
	</div>

</div>
<?= $this->Form->end(); ?>