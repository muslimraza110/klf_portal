<?php

$donationsSessionJSON = json_encode($sessionInitiative->groups);

$this->Html->scriptBlock(<<<JS

var addGroup = function (self) {
	
	var _groups = $('#groups'),
	nextKey = _groups.data('next-key');
	
	var _selected = $('#group-list').val();
	
	if (_selected) {
		
		var _text = $('#group-list :selected').text();
		
		_groups.append(
			$('#group-prototype').html()
				.replace(/groups.name/g, _text)
				.replace(/groups.id/g, _selected)
				.replace(/000/g, nextKey)
		);
		
		_groups.data('next-key', nextKey + 1);
		
		$("#group-list option[value='" + _selected + "']").remove();
		
	}
	
};

var removeGroup = function (self, id, name) {

	if (window.confirm('Are you sure you want to remove this charity?')) {
		$(self).parents('.group').first().remove();
		$('#group-list').append('<option value="' + id + '">' + name + '</option>');
	}

};

$(function() {
	
	var donationsSession = $donationsSessionJSON;
	
	$.each(donationsSession, function (index, value) {
		$("#group-list option[value='" + value.id + "']").remove();
	});
	
});

JS
	, ['block' => true]);

?>

	<div id="group-prototype" style="display: none;">

		<div class="group m-b-md">
			<div class="ibox-content">
				<div class="row">

					<div class="col-xs-10 col-xs-offset-2">

						<?= $this->Html->link('groups.name',
							['controller' => 'groups', 'action' => 'view', 'groups.id']
						); ?>

					</div>

					<?= $this->Form->form('groups.000.id', ['type' => 'hidden', 'value' => 'groups.id']); ?>

					<div class="col-xs-6">

						<?= $this->Html->link('Remove', 'javascript:;', [
							'class' => 'btn btn-danger pull-left',
							'onclick' => "removeGroup(this, 'groups.id', 'groups.name')"
						]) ?>

					</div>

				</div>

			</div>

		</div>

	</div>



<?= $this->Form->create($sessionInitiative, ['novalidate']); ?>
	<div class="row">
		<div class="col-md-16 col-md-offset-4">

			<div class="ibox ibox-shadow">
				<div class="ibox-title">

					<?= $this->StepProgress->render([
						['name' => 'Initiative'],
						['name' => 'Projects'],
						['name' => 'Donations', 'active' => true],
						['name' => 'Fundraisings'],
						['name' => 'Summary']
					]); ?>

				</div>
			</div>

			<div class="ibox">
				<div class="ibox-title">
					<h5>Add Charities</h5>
				</div>

				<div class="ibox-content">

					<div class="row">
						<div class="col-md-16">
							<?= $this->Form->input('group_list', [
								'options' => $groups,
								'empty' => '---',
								'label' => false,
							]); ?>
						</div>

						<div class="col-md-8">
							<?= $this->Html->link('Add Charity', 'javascript:;', [
								'class' => 'btn btn-primary pull-left',
								'onclick' => 'addGroup(this)'
							]) ?>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-24">

							<div id="groups" data-next-key="<?= count($sessionInitiative->groups); ?>">

								<?php if (isset($sessionInitiative->groups)): ?>
									<?php foreach ($sessionInitiative->groups as $key => $group): ?>

										<div class="group m-b-md">
											<div class="ibox-content">
												<div class="row">

													<div class="col-xs-10 col-xs-offset-2">

														<?= $this->Html->link($group->name,
															['controller' => 'groups', 'action' => 'view', $group->id]
														); ?>

													</div>

													<?= $this->Form->input('groups.' . $key . '.id', ['hidden' => true]) ?>

													<div class="col-xs-6">

														<?= $this->Html->link('Remove', 'javascript:;', [
															'class' => 'btn btn-danger pull-left',
															'onclick' => "removeGroup(this, '" . $group->id . "', '" . $group->name . "')"
														]) ?>

													</div>

												</div>
											</div>
										</div>

									<?php endforeach ?>
								<?php endif ?>

							</div>

						</div>
					</div>

				</div>

			</div>

		</div>
	</div>

	<div class="row">

		<div class="col-md-14 col-md-offset-5">
			<div class="submit-big">
				<div class="row">
					<div class="col-xs-20">
						<?= $this->Form->submit('previous', ['class' => 'btn btn-primary', 'name' => 'submit']); ?>
					</div>
					<div class="col-xs-4">
						<?= $this->Form->submit('next', ['class' => 'btn btn-primary', 'name' => 'submit']); ?>
					</div>
				</div>
			</div>
		</div>

	</div>
<?= $this->Form->end(); ?>