<?php

$scriptBlock = <<<JS

$(function () {
	
	$('#status').change(function () {
		$('#status-form').submit();
	});
	
});

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>

<?= $this->Form->create(null, ['type' => 'get', 'id' => 'status-form']) ?>

<div class="row">
	<div class="col-xs-3 pull-right">
		<?= $this->Form->input('status', [
			'label' => false,
			'options' => [
				'A' => 'Active',
				'R' => 'Archived'
			]
		]) ?>
	</div>
</div>

<?= $this->Form->end() ?>

<?= $this->element('pagination', ['noPadding' => true]); ?>

<br>

<table class="table table-bordered">
	<thead>
	<?php

	$headers = [
		$this->Paginator->sort('name'),
		$this->Paginator->sort('Creators.first_name', 'Creator'),
		$this->Paginator->sort('amount', 'Target'),
		$this->Paginator->sort('created'),
		''
	];

	echo $this->Html->tableHeaders($headers);

	?>
	</thead>
	<tbody>
	<?php

	if (!$initiatives->isEmpty())
		foreach ($initiatives as $initiative) {

			$actions = [];

			$actions[] = $this->Html->link(
				'View',
				['action' => 'view', $initiative->id],
				['class' => 'btn btn-sm btn-primary']
			);

			if (in_array($authUser->role, ['A', 'O'])) {

				if ($initiative->status != 'R') {

					$actions[] = $this->Html->link(
						'Edit',
						['action' => 'edit', $initiative->id],
						['class' => 'btn btn-sm btn-primary']
					);

					$actions[] = $this->Form->postLink(
						'Archive',
						['action' => 'archive', $initiative->id],
						[
							'class' => 'btn btn-sm btn-info',
							'confirm' => 'Are you sure you want to archive this initiative?'
						]
					);

				} else {
					$actions[] = $this->Form->postLink(
						'Mark as Active',
						['action' => 'reactivate', $initiative->id],
						[
							'class' => 'btn btn-sm btn-info',
							'confirm' => 'Are you sure you want to mark this initiative as ACTIVE?'
						]
					);
				}

				$actions[] = $this->Form->postLink(
					'Delete',
					['action' => 'delete', $initiative->id],
					[
						'class' => 'btn btn-sm btn-danger',
						'confirm' => 'Are you sure you want to delete this initiative?'
					]
				);

			}

			if ($initiative->goalPledgeAmount == 0) {
				$targetPercent = 0;
			} else {
				$targetPercent = round(($initiative->currentPledgeAmount * 100) / $initiative->goalPledgeAmount, 2);
			}

			if ($targetPercent > 100) {
				$targetPercent = 100;
			}

			$amount = <<<HTML
<strong>
	{$this->Number->format($initiative->currentPledgeAmount, ['places' => 0, 'before' => 'USD '])} / {$this->Number->format($initiative->goalPledgeAmount, ['places' => 0, 'before' => 'USD '])} 
</strong>
<div class="progress" style="margin-bottom: 0; border: 1px solid #aaa;">
	<div style="width: $targetPercent%" class="progress-bar progress-bar-success"></div>
</div>
HTML;

			$row = [
				$this->Html->link(
					$initiative->name,
					['action' => 'view', $initiative->id],
					['escape' => false]
				),
				$this->Html->link(
					$initiative->user->name,
					['controller' => 'Users', 'action' => 'view', $initiative->user->id],
					['escape' => false]
				),
				[$amount, ['class' => 'text-center']],
				$initiative->created,
				[implode('', $actions), ['class' => 'compact']]
			];

			echo $this->Html->tableCells($row);

		}
	else
		echo $this->Html->tableCells([[
			['No initiatives found.', ['colspan' => count($headers)]]
		]]);

	?>
	</tbody>
</table>

<?= $this->element('pagination', ['noPadding' => true]); ?>