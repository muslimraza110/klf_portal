<?php

$fundraisingsSessionJSON = json_encode($sessionInitiative->fundraisings);

$this->Html->scriptBlock(<<<JS

var addFundraising = function (self) {
	
	var _fundraisings = $('#fundraisings'),
	nextKey = _fundraisings.data('next-key');
	
	var _selected = $('#fundraising-list').val();
	
	if (_selected) {
		
		var _text = $('#fundraising-list :selected').text();
		
		_fundraisings.append(
			$('#fundraising-prototype').html()
				.replace(/fundraisings.name/g, _text)
				.replace(/fundraisings.id/g, _selected)
				.replace(/000/g, nextKey)
		);
		_fundraisings.data('next-key', nextKey + 1);
	}
	
	$("#fundraising-list option[value='" + _selected + "']").remove();
	
};

var removeFundraising = function (self, id, name) {

	if (window.confirm('Are you sure you want to remove this fundraising?')) {
		$(self).parents('.fundraising').first().remove();
		$('#fundraising-list').append('<option value="' + id + '">' + name + '</option>');
	}

};

$(function () {
	
	$('.component input, .fundraising input').iCheck('destroy');
	
	var fundraisingsSession = $fundraisingsSessionJSON;
	
	$.each(fundraisingsSession, function (index, value) {
		$("#fundraising-list option[value='" + value.id + "']").remove();
	});
	
});

JS
	, ['block' => true]);

?>

<div id="fundraising-prototype" style="display: none;">

	<div class="fundraising m-b-md">
		<div class="ibox-content">
			<div class="row">

				<div class="col-xs-12 col-xs-offset-4">
					<div class="row">
					<?= $this->Html->link('fundraisings.name',
						['controller' => 'fundraisings', 'action' => 'view', 'fundraisings.id']
					); ?>
					</div>
					<div class="row">
						<?php

						foreach ($sessionInitiative->projects as $key => $project) {
							echo $this->Form->input('fundraisings.000.projects.' . $key . '.' . $project->id, [
								'type' => 'checkbox hidden',
								'label' => $project->name,
								'checked' => 'checked'
							]);
						}

						?>

					</div>

				</div>

				<?= $this->Form->form('fundraisings.000.id', ['type' => 'hidden', 'value' => 'fundraisings.id']); ?>

				<div class="col-xs-6">

					<?= $this->Html->link('Remove', 'javascript:;', [
						'class' => 'btn btn-danger pull-left',
						'onclick' => "removeFundraising(this, 'fundraisings.id', 'fundraisings.name')"
					]) ?>

				</div>

			</div>
		</div>
	</div>

</div>

<?= $this->Form->create($sessionInitiative, ['novalidate']); ?>
<div class="row">
	<div class="col-md-16 col-md-offset-4">

		<div class="ibox ibox-shadow">
			<div class="ibox-title">

				<?= $this->StepProgress->render([
					['name' => 'Initiative'],
					['name' => 'Projects'],
					['name' => 'Donations'],
					['name' => 'Fundraisings', 'active' => true],
					['name' => 'Summary']
				]); ?>

			</div>
		</div>


		<div class="ibox">
			<div class="ibox-title">
				<h5>Fundraisings</h5>
			</div>

			<div class="ibox-content">

				<div class="row">
					<div class="col-md-16">
						<?= $this->Form->input('fundraising_list', [
							'options' => $fundraisingList,
							'empty' => '---',
							'label' => false,
						]); ?>
					</div>

					<div class="col-md-8">
						<?= $this->Html->link('Add Fundraising', 'javascript:;', [
							'class' => 'btn btn-primary pull-left',
							'onclick' => 'addFundraising(this)'
						]) ?>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-24">

						<div id="fundraisings" data-next-key="<?= count($sessionInitiative->fundraisings); ?>">

							<?php if (isset($sessionInitiative->fundraisings)): ?>
								<?php foreach ($sessionInitiative->fundraisings as $key1 => $fundraising): ?>

									<div class="fundraising m-b-md">
										<div class="ibox-content">
											<div class="row">

												<div class="col-xs-12 col-xs-offset-4">

													<div class="row">
														<?= $this->Html->link($fundraising->name . ' - ' . $fundraising->typeLabel() . ' - $' . number_format($fundraising->amount),
															['controller' => 'Fundraisings', 'action' => 'view', $fundraising->id]
														); ?>

														<div class="row">

															<?php

															foreach ($sessionInitiative->projects as $key2 => $project) {

																if (isset($fundraising->projects)) {
																	$projectIds = array_map(function ($project) {return $project->id; }, $fundraising->projects);
																} else {
																	$projectIds = [];
																}

																echo $this->Form->input('fundraisings.'. $key1 .'.projects.' . $key2 . '.' . $project->id, [
																	'type' => 'checkbox hidden',
																	'label' => $project->name,
																	//'checked' => in_array($project->id, $projectIds),
																	'checked' => 'checked'
																]);

															}

															?>

														</div>
													</div>
												</div>

												<?= $this->Form->input('fundraisings.' . $key1 . '.id', ['hidden' => true]) ?>

												<div class="col-xs-6">

													<?= $this->Html->link('Remove', 'javascript:;', [
														'class' => 'btn btn-danger pull-left',
														'onclick' => "removeFundraising(this, '" . $fundraising->id . "', '" . $fundraising->name . "')"
													]) ?>

												</div>

											</div>
										</div>
									</div>

								<?php endforeach ?>
							<?php endif ?>

						</div>

					</div>
				</div>

			</div>

		</div>

	</div>
</div>



<div class="row">

	<div class="col-md-14 col-md-offset-5">
		<div class="submit-big">
			<div class="row">
				<div class="col-xs-20">
					<?= $this->Form->submit('previous', ['class' => 'btn btn-primary', 'name' => 'submit']); ?>
				</div>
				<div class="col-xs-4">
					<?= $this->Form->submit('next', ['class' => 'btn btn-primary', 'name' => 'submit']); ?>
				</div>
			</div>
		</div>
	</div>

</div>
<?= $this->Form->end(); ?>