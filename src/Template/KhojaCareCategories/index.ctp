<?php
if ($authUser->role == 'A') {
$scriptBlock = <<<JS

$(function() {

	$('#khojaCareCategories-list').sortable({
		update: function(event, ui) {
			var khojaCareCategoriesIds = new Array();
			$('#khojaCareCategories-list .ibox').each(function() {
				khojaCareCategoriesIds.push($(this).attr("id"));
			});

			$.post(
				'{$this->Url->build(['action' => 'ajax_reorder_khojaCareCategory_list'])}.json',
				{
					positions: khojaCareCategoriesIds,

				},
				function () {

					console.log('updated');
				}
			);
		}

	});

	$('#khojaCareCategories-list img').css({'width':'100%', 'height':'100%'});


});

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);
}
?>
<div class="col-md-14 col-md-offset-4">
	<div class="ibox">
		<div class="ibox-title">
			<h5><?= $icons['bars']; ?> Khoja Care Categories</h5>
		</div>
		<?php if (!empty($khojaCareCategories)): ?>
		<div class="ibox-content" id="khojaCareCategories-list">
			<?php foreach ($khojaCareCategories as $khojaCareCategory): ?>
				<div id="<?= $khojaCareCategory->id; ?>" class="ibox">
					<?php $inactive = $khojaCareCategory->status == 'D' ? "disabled" : null; ?>
					<div class="ibox-title" <?= $inactive; ?>>
							<h2 style="text-transform: none;"><?= $khojaCareCategory->name  ?></h2>
						<div class="ibox-tools">
							<?php if ($authUser->role == 'A'): ?>
								<?= $this->Html->link(
									$icons['edit'],
									['action' => 'edit', $khojaCareCategory->id],
									['escape' => false, 'class' => 'btn btn-icon btn-warning']
								); ?>
								<?= $this->Form->postLink(
									$icons['delete'],['action' => 'delete', $khojaCareCategory->id],
									[
										'escape' => false,
										'class' => 'btn btn-icon btn-danger',
										'title' => 'Reactivate',
										'confirm' => 'Are you sure to delete the Khoja Care Category "' . $khojaCareCategory->name . '"?',
									]
								); ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>
	</div>
</div>
