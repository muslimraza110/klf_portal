<?= $this->Form->create($khojaCareCategory); ?>

<div class="row">
	<div class="col-md-12">
		<div class="ibox">
			<div class="ibox-title ibox-icons">
				<span><?= $icons['bars']; ?></span>
				<h3>Khoja Care Category</h3>
			</div>
			<div class="ibox-content">
				<?= $this->Form->input('name'); ?>
			</div>
		</div>
	</div>
</div>

<div class="submit-big">
	<?= $this->Form->submit('Save', ['class' => 'btn btn-primary']); ?>
</div>

<?= $this->Form->end(); ?>
