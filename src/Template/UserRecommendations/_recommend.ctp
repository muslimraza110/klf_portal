<?php

$scriptBlock = <<<JS

var taggedUsers = [$authUser->id];

var addUser = function (self) {
	
	$(self).hide();
	
	var id = $(self).data('id');
	
	var element = $(self).parents('.list-group-item').first().clone()
		.append('<a href="javascript:;" class="btn btn-sm btn-danger pull-right" onclick="removeUser(this)" data-id="' + id + '">Remove</a>')
		.html();
	
	taggedUsers.push(id);
	
	$('#tagged-users').val(taggedUsers.join(','));
	
	$('#user-list ul').append('<li class="list-group-item">' + element + '</li>');
	
};

var removeUser = function (self) {
	
	taggedUsers.splice($.inArray($(self).data('id'), taggedUsers), 1);
	
	$('#tagged-users').val(taggedUsers.join(','));
	
	$(self).parents('.list-group-item').first().remove();
	
};

$(function () {
	
	var addUserTimeout;
	
	$('#add-user').keypress(function () {
		
		window.clearTimeout(addUserTimeout);
		
		var self = this;
		
		addUserTimeout = window.setTimeout(function () {
			
			$.post(
				'{$this->Url->build(['controller' => 'Users', 'action' => 'ajax_html_user_list'])}',
				{
					id: 0,
					term: $(self).val(),
          taggedUsers: taggedUsers,
					model: 'TaggedUserRecommendations'
				},
				function (response) {
					
					$('#add-user-response').html(response);
					
				}
			);
			
		}, 500);
		
	});
	
});

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>

<?= $this->Form->create($userRecommendation); ?>

<div class="row">

	<div class="col-md-12 col-md-offset-3">

		<div class="ibox">
			<div class="ibox-title">
				<h5>Recommend</h5>
			</div>

			<div class="ibox-content">

				<?= $this->Form->input('email'); ?>

				<div class="row">

					<div class="col-md-12">

						<?= $this->Form->input('first_name'); ?>

					</div>

					<div class="col-md-12">

						<?= $this->Form->input('last_name'); ?>

					</div>

				</div>

				<?= $this->Form->input('user_recommendation_notes.0.note'); ?>

			</div>
		</div>

	</div>

	<div class="col-md-6">

		<div class="ibox">
			<div class="ibox-title">
				<h5>Tagged Contacts</h5>
			</div>

			<div class="ibox-content">

				<?= $this->Form->input(
					'add_user',
					[
						'placeholder' => 'Search for Contact Name',
						'label' => 'Add Contact'
					]
				); ?>

				<div id="add-user-response"></div>

				<div id="user-list">

					<ul class="list-group user-list-group"></ul>

				</div>

				<ul class="list-group user-list-group">
					<li class="list-group-item">
						<?= $this->Html->link(
							sprintf(
								'%s %s',
								$this->Html->image($authUser->profile_thumb.'?'.microtime(true), ['class' => 'img-circle']),
								$authUser->name
							),
							['controller' => 'Users', 'action' => 'view', $authUser->id],
							['escape' => false]
						); ?>
					</li>
				</ul>
				<?= $this->Form->input('tagged_users', ['type' => 'hidden']); ?>

			</div>
		</div>

	</div>

</div>

<div class="row">

	<div class="col-md-18 col-md-offset-3">

		<div class="submit-big">
			<?= $this->Form->submit('Submit', ['bootstrap-type' => 'primary']); ?>
		</div>

	</div>

</div>

<?= $this->Form->end(); ?>