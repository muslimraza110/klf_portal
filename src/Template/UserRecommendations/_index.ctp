<div class="ibox">

	<div class="ibox-title">
		<h3>Pending Recommendations</h3>
	</div>

	<div class="ibox-content">
		<table class="table table-bordered">
			<thead>

			<?php

			$headers = [
				'Name',
				'email',
				'Recommended by',
				'Voted',
				'Total Votes',
				'created',
				''
			];

			echo $this->Html->tableHeaders($headers);

			?>
			</thead>
			<tbody>
			<?php

			if (!$noAdminDecisionRecommendations->isEmpty())
				foreach ($noAdminDecisionRecommendations as $recommendation) {

					$actions = [];

					$actions[] = $this->Html->link(
						'View',
						['action' => 'view', $recommendation->id],
						['class' => 'btn btn-sm btn-primary']
					);

					$name = $this->Html->link($recommendation->name, ['action' => 'view', $recommendation->id]);

					if (!empty($recommendation->user))
						$name .= sprintf(
							' (Registered as: %s)',
							$this->Html->link(
								$recommendation->user->name,
								['controller' => 'Users', 'action' => 'view', $recommendation->user->id]
							)
						);

					$row = [
						$name,
						$recommendation->email,
						$this->Html->link($recommendation->recommender->name, ['controller' => 'Users', 'action' => 'view', $recommendation->recommender->id]),
						[$recommendation->hasVoted($authUser) ? $icons['check'] : '', ['class' => 'text-center']],
						[count($recommendation->votes), ['class' => 'text-center']],
						$recommendation->created,
						[implode('', $actions), ['class' => 'compact']]
					];

					echo $this->Html->tableCells($row);

				}
			else
				echo $this->Html->tableCells([[
					['No recommendations found.', ['colspan' => count($headers)]]
				]]);

			?>
			</tbody>
		</table>
	</div>
</div>
<div class="ibox">

	<div class="ibox-title">
		<h3>Recommendations</h3>

		<div class="ibox-tools">
			<a href="javascript:;" class="btn btn-primary collapse-link">
				<?= $icons['chevron-down'] ?>Toggle
			</a>
		</div>

	</div>

	<div class="ibox-content" style="display: none;">

		<?= $this->element('pagination', ['noPadding' => true]); ?>

		<br>

		<table class="table table-bordered">
			<thead>

			<?php

			$headers = [
				$this->Paginator->sort('first_name', 'Name'),
				$this->Paginator->sort('email'),
				$this->Paginator->sort('Recommenders.first_name', 'Recommended by'),
				'Voted',
				'Total Votes',
				$this->Paginator->sort('created'),
				'',
				'Status',
				'Admin Decision'
			];

			echo $this->Html->tableHeaders($headers);

			?>
			</thead>
			<tbody>
			<?php

			if (!$userRecommendations->isEmpty())
				foreach ($userRecommendations as $recommendation) {

					$actions = [];

					$actions[] = $this->Html->link(
						'View',
						['action' => 'view', $recommendation->id],
						['class' => 'btn btn-sm btn-primary']
					);

					$name = $this->Html->link($recommendation->name, ['action' => 'view', $recommendation->id]);

					if (!empty($recommendation->user))
						$name .= sprintf(
							' (Registered as: %s)',
							$this->Html->link(
								$recommendation->user->name,
								['controller' => 'Users', 'action' => 'view', $recommendation->user->id]
							)
						);

						switch ($recommendation->vote_decision) {
							case 'Y':
									$status = 'Accepted';
								break;
							case 'N':
									$status = 'Rejected';
								break;
							default:
									$status = 'Pending';
								break;
						}
						$row = [
							$name,
							$recommendation->email,
							$this->Html->link($recommendation->recommender->name, ['controller' => 'Users', 'action' => 'view', $recommendation->recommender->id]),
							[$recommendation->hasVoted($authUser) ? $icons['check'] : '', ['class' => 'text-center']],
							[count($recommendation->votes), ['class' => 'text-center']],
							$recommendation->created,
							[implode('', $actions), ['class' => 'compact']],
							$status,
							[$status == 'Accepted' ?  $icons['check'] : '', ['class' => 'text-center']],
						];
					echo $this->Html->tableCells($row);

				}
			else
				echo $this->Html->tableCells([[
					['No recommendations found.', ['colspan' => count($headers)]]
				]]);

			?>
			</tbody>
		</table>

		<?= $this->element('pagination', ['noPadding' => true]); ?>
	</div>
</div>
