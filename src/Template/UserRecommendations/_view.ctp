<?php

$scriptBlock = <<<JS

var userList = function () {
	
	$.post(
		'{$this->Url->build(['action' => 'ajax_html_user_list'])}',
		{
			id: '{$userRecommendation->id}'
		},
		function (response) {
			
			$('#user-list').html(response);
			
		}
	);
	
};

var addUser = function (self) {
	
	$.post(
		'{$this->Url->build(['action' => 'ajax_add_user'])}.json',
		{
			id: '{$userRecommendation->id}',
			user_id: $(self).data('id')
		},
		function () {
			
			$(self).hide();
			
			userList();
			
		}
	);
	
};

var removeUser = function (self) {
	
	$.post(
		'{$this->Url->build(['action' => 'ajax_remove_user'])}.json',
		{
			id: '{$userRecommendation->id}',
			user_id: $(self).data('id')
		},
		function () {
			
			$(self).hide();
			
			userList();
			
		}
	);
	
};

$(function () {
	
	var addUserTimeout;
	
	$('#add-user').keypress(function () {
		
		window.clearTimeout(addUserTimeout);
		
		var self = this;
		
		addUserTimeout = window.setTimeout(function () {
			
			$.post(
				'{$this->Url->build(['controller' => 'Users', 'action' => 'ajax_html_user_list'])}',
				{
					id: '{$userRecommendation->id}',
					term: $(self).val(),
					model: 'TaggedUserRecommendations'
				},
				function (response) {
					
					$('#add-user-response').html(response);
					
				}
			);
			
		}, 500);
		
	});
	
	userList();
	
});

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>

<?php if (in_array($authUser->role, ['A', 'O'])): ?>

	<?= $this->element('vote', [
		'entity' => $userRecommendation,
		'vote' => $vote,
		'model' => 'UserRecommendations',
		'changeVote' => $authUser->role == 'O'
	]); ?>

<?php endif; ?>

<?php if (!empty($votes) && !$votes->isEmpty()): ?>

	<?= $this->element('vote_results', ['entity' => $userRecommendation, 'votes' => $votes, 'model' => 'UserRecommendations']); ?>

<?php endif; ?>

<div class="row">

	<div class="col-md-16">

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['info']; ?></span>
				<h3>Information</h3>
			</div>

			<div class="ibox-content" style="overflow: visible;">

				<div class="row">

					<div class="col-md-8 text-center">

						<small>Recommended by</small><br>

						<?= $this->Html->link(
							$this->Html->image($userRecommendation->recommender->profile_img_url.'?'.microtime(true), ['height' => '120px']),
							['controller' => 'Users', 'action' => 'view', $userRecommendation->recommender->id],
							['escape' => false, 'class' => 'profile-thumb m-t-sm', 'title' => $userRecommendation->recommender->name, 'data-toggle' => 'tooltip']
						); ?>

					</div>

					<div class="col-md-16">

						<h4><?= $userRecommendation->name; ?></h4>

						<strong>Email:</strong> <?= $this->Html->link($userRecommendation->email, 'mailto:'.$userRecommendation->email); ?>

						<hr>
            <?php if($authUser->role != 'C'): ?>
							<strong>Vote Decision:</strong> <?= $userRecommendation->vote_decision_label; ?>
						<?php endif; ?>
					</div>

				</div>

			</div>
		</div>

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['commenting-o']; ?></span>
				<h3>Notes</h3>
			</div>

			<div class="ibox-content">

				<div class="feed-activity-list">

					<?php foreach ($userRecommendation->user_recommendation_notes as $note): ?>

						<div class="feed-element">
							<?= $this->Html->link(
								$this->Html->image($note->user->profile_thumb.'?'.microtime(true), ['class' => 'img-circle']),
								['controller' => 'Users', 'action' => 'view', $note->user->id],
								['escape' => false, 'class' => 'pull-left']
							); ?>
							<div class="media-body">
								<strong>

									<?= $this->Html->link($note->user->name, ['controller' => 'Users', 'action' => 'view', $note->user->id]); ?>

									<?php if (!empty($note->private)): ?>
										(Private Note)
									<?php endif; ?>

								</strong>
								<br>
								<small class="text-muted"><?= $note->created; ?></small>
								<p>
									<?= nl2br($note->note); ?>
								</p>
							</div>
						</div>

					<?php endforeach; ?>

					<?= $this->Form->create(null, ['class' => 'm-t-md']); ?>

					<?= $this->Form->input('note', [
						'type' => 'textarea',
						'label' =>'Add Note',
						'templates' => ['formGroup' => '{{label}}<p class="help-block"><i>All notes visible to all tagged contacts unless marked as private.</i></p>{{input}}']

					]); ?>

					<?= $this->Form->input('private', ['type' => 'checkbox', 'label' => 'Private*']); ?>

					<p class="help-block">
						* Only visible by Admins, Founders and Governing Board of Trustees
					</p>

					<div class="text-right">

						<?= $this->Form->submit('Submit', ['class' => 'btn-sm']); ?>

					</div>

					<?= $this->Form->end(); ?>

				</div>

			</div>
		</div>

	</div>

	<div class="col-md-8">

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['users']; ?></span>
				<h3>Tagged Contacts</h3>
			</div>

			<div class="ibox-content" style="overflow-y: scroll; height: 400px;">

				<?= $this->Form->input(
					'add_user',
					[
						'placeholder' => 'Search for Contact Name',
						'label' => 'Add Contact'
					]
				); ?>
				<p class="help-block">
					Users who know this person being recommended
				</p>
				<div id="add-user-response"></div>

				<div id="user-list"></div>

			</div>
		</div>

	</div>

</div>