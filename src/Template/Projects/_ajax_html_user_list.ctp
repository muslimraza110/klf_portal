<?php if (!empty($project->users)): ?>

	<ul class="list-group user-list-group">

		<?php

		$isGroupAdmin = $project->isAdminAnyGroups($authUser);

		foreach ($project->users as $user): ?>

			<li class="list-group-item">
				<?= $this->Html->link(
					sprintf(
						'%s %s',
						$this->Html->image($user->profile_thumb.'?'.microtime(true), ['class' => 'img-circle']),
						$user->name
					),
					['controller' => 'Users', 'action' => 'view', $user->id],
					['escape' => false]
				); ?>

				<?php if (
					$project->status != 'R'
					&& ($authUser->role == 'A' || $isGroupAdmin)
				): ?>

					<?= $this->Html->link(
						'Remove',
						'javascript:;',
						[
							'class' => 'btn btn-sm btn-danger pull-right',
							'onclick' => 'removeUser(this)',
							'data-id' => $user->id
						]
					); ?>

				<?php endif; ?>
			</li>

		<?php endforeach; ?>

	</ul>

<?php endif; ?>