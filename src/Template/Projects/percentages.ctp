<?php

$scriptBlock = <<<JS

$(function () {

	$('#submit-percentages').click(function (e) {

		e.preventDefault();
		
		var percentage = 0;
		
		$('.charity-percentages').each(function () {
			percentage += parseFloat($(this).val());
		});
		
		if (percentage != 100) {
			alert('Percentages must be equal to 100.');
		} else {
			$('form').submit();
		}
		
	});

});

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>

<?= $this->Form->create($project);?>
<div class="row">
	<div class="col-md-12 col-md-offset-6">
		<div class="ibox">
			<div class="ibox-title">
				<h5>Charities</h5>
			</div>
			<div class="ibox-content">

				<table class="table table-condensed">
					<thead>
					<?php

					$headers = [
						'Name',
						'Percentage'
					];

					echo $this->Html->tableHeaders($headers);

					?>
					</thead>
					<tbody>
					<?php

					$rows = [];

					foreach ($project->groups as $index => $group) {

						$rows[] = [
							$group->name,
							$this->Form->hidden('groups.' . $index . '.id', [
								'value' => $group->id
							]).
							$this->Form->input('groups.' . $index . '._joinData.percent', [
								'class' => 'charity-percentages',
								'append' => '%',
								'type' => 'number',
								'step' => 'any',
								'min' => '0',
								'max' => '100',
								'label' => false,
								'templates' => [
									'inputContainer' => '<div class="form-group m-b-xs m-t-xs">{{content}}</div>'
								]
							])
						];

					}

					echo $this->Html->tableCells($rows);

					?>
					</tbody>
				</table>
				<div class="submit-big">
					<?= $this->Form->submit('Save', ['bootstrap-type' => 'primary', 'id' => 'submit-percentages']); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end(); ?>