<?php if (!empty($groups)): ?>

	<ul class="list-group user-list-group">

		<?php


		foreach ($groups as $group): ?>

			<li class="list-group-item">
				<?= $this->Html->link(
					sprintf(
						'%s %s',
						$this->Html->image($group->profile_tiny.'?'.microtime(true)),
						$group->name
					),
					['controller' => 'Groups', 'action' => 'view', $group->id],
					['escape' => false]
				); ?>
				<?php if (empty($remove)): ?>
					<?= $this->Html->link(
						'Add',
						'javascript:;',
						[
							'class' => 'btn btn-sm btn-primary pull-right',
							'onclick' => 'addGroup(this)',
							'data-id' => $group->id
						]
					); ?>
				<?php else: ?>
					<?= $this->Html->link(
						'Remove',
						'javascript:;',
						[
							'class' => 'btn btn-sm btn-danger pull-right',
							'onclick' => 'removeGroup(this)',
							'data-id' => $group->id
						]
					); ?>

				<?php endif; ?>
			</li>

		<?php endforeach; ?>

	</ul>

<?php endif; ?>
