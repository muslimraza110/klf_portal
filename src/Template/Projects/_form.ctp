<?php

$this->Html->script(['plugins/datapicker/bootstrap-datepicker'], ['block' => true]);

$this->Html->css('plugins/datapicker/datepicker3', ['block' => true]);

$tagsListJSON = json_encode($tagsList);

$scriptBlock = <<<JS

$(function () {

	$('.datepicker').datepicker({
		autoclose: true,
		format: 'm/d/yy',
		todayBtn: 'linked',
		todayHighlight: true
	});

	var addGroupTimeout;

	$('#add-group').keypress(function () {

		window.clearTimeout(addGroupTimeout);

		var self = this;

		addGroupTimeout = window.setTimeout(function () {

			$.post(
				'{$this->Url->build(['action' => 'ajax_html_group_list'])}',
				{
					term: $(self).val(),
					id: '{$project->id}'
				},
				function (response) {

					$('#add-groups-response').html(response);

				}
			);

		}, 500);

	});

	groupList();


	$('div.well.checkboxes .checkbox').addClass('inline m-r-lg');
	
	$.each($tagsListJSON, function (index, tags) {
		
		var selectInput = $("select[name='tags[" + index + "][id]'");
		var optionSelected;
	
		selectInput.on('focus', function () {
			optionSelected = selectInput.find(":selected");
			optionSelected.html(tags[optionSelected.val()]['text']);
		}).on('blur', function () {
			optionSelected = selectInput.find(":selected");
			optionSelected.html($.trim(optionSelected.text()));
		}).trigger('blur');
		
	});

});

var groupList = function () {

	$.post(
		'{$this->Url->build(['action' => 'ajax_html_group_list'])}',
		{
			id: '{$project->id}',
		},
		function (response) {

			$('#group-list').html(response);

		}
	);

};

var addGroup = function (self) {

	$.post(
		'{$this->Url->build(['action' => 'ajax_add'])}.json',
		{
			id: '{$project->id}',
			group_id: $(self).data('id'),
			model: 'Groups'
		},
		function () {

			$(self).parent().hide();

			groupList();

		}
	);

};

var removeGroup = function (self) {

	$.post(
		'{$this->Url->build(['action' => 'ajax_remove'])}.json',
		{
			id: '{$project->id}',
			group_id: $(self).data('id'),
			model: 'Groups'
		},
		function () {

			$(self).hide();

			groupList();

		}
	);

};

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>

<?= $this->Form->create($project); ?>

<div class="row">

	<div class="col-lg-12 col-lg-offset-6">

		<div class="ibox">
			<div class="ibox-title">
				<h5>Project</h5>
			</div>

			<div class="ibox-content">

				<?php if (
					!$project->isNew()
					&& ($authUser->role == 'A' || $authUser->id == $project->creator_id || $project->isAdminAnyGroups($authUser))
				): ?>
					<div class="well checkboxes">
						<?php if ($authUser->role == 'A'): ?>
							<?= $this->Form->input('approval_stamp', ['label' => 'Khoja Cares Stamp of Approval']); ?>
							<!-- <div class="col-md-16 pull-right"> -->

							<!-- </div> -->
						<?php endif; ?>
						<!-- <div class="col-md-8"> -->
						<?= $this->Form->input('read_only', [
							'label' => 'Read-Only',
							'type' => 'checkbox',
							'checked' => $project->status == 'R' ? true : false,
						]); ?>
						<?php if ($authUser->role == 'A'): ?>
							<?= $this->Form->input('khoja_care_category_id', [
								'label' => 'Khoja Care Category',
								'empty' => '--Empty--',
								'options' => $khojaCareCategories,
							]);
							?>
						<?php endif; ?>
					<!-- </div> -->
				</div>
				<?php endif; ?>
				<?php if ($project->isNew()): ?>

					<div class="alert alert-info">
						You will be able to add Business and Charities after saving the project.
					</div>

				<?php else: ?>
				<div class='row'  style="max-height: 300px; overflow-y: scroll;">

					<div class="col-md-24">

						<?= $this->Form->input(
							'add_group',
							[
								'label' => 'Business and Charities',
								'placeholder' => 'Search for Business and Charities Name'
							]
						); ?>
						<div id="add-groups-response"></div>
					</div>
					<div class="col-md-24">

						<div id="group-list"></div>
					</div>
				</div>
			<?php endif; ?>
				<?= $this->Form->input('name'); ?>

				<?= $this->Form->input('website', ['prepend' => 'http://']); ?>

				<?= $this->Form->input('description'); ?>

				<div class="row">

					<div class="col-md-12">
						<?= $this->Form->input('start_date', ['type' => 'text', 'class' => 'datepicker', 'prepend' => $icons['calendar']]); ?>
					</div>

					<div class="col-md-12">
						<?= $this->Form->input('est_completion_date', ['type' => 'text', 'class' => 'datepicker', 'label' => 'Estimated Completion Date', 'prepend' => $icons['calendar']]); ?>
					</div>

				</div>

				<div class="row">

					<div class="col-sm-12">
						<?= $this->Form->input('city'); ?>
					</div>

					<div class="col-sm-12">
						<?= $this->Form->input('country', ['default' => 'Canada']); ?>
					</div>

				</div>

				<div class="row">
					<?php foreach($tagsList as $tagId => $list) : ?>
						<div class="col-sm-8">
							<!-- <label><?= current($list)['text'] ?></label> -->
							<?= $this->Form->select('tags.' . $tagId . '.id', $list, [
								'empty' => '--',
								'required' => $project->tagRequired($tagId)
							]) ?>
						</div>
					<?php endforeach; ?>
				</div>

			</div>
		</div>

		<div class="submit-big">
			<?= $this->Form->button('Save', ['bootstrap-type' => 'primary', 'style' => 'margin-left: 15px']); ?>
			<?php if (!$project->isNew()) {
				echo $this->Html->link('Percentages',
					['controller' => 'Projects', 'action' => 'percentages', $project->id],
					['class' => 'btn btn-info', 'target' => '_blank']);
			} ?>
		</div>

	</div>

</div>

<?= $this->Form->end(); ?>