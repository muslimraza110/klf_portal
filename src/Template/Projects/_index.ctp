<?= $this->element('pagination', ['noPadding' => true]); ?>

<br>

<table class="table table-bordered">
	<thead>
	<?php

	$headers = [
		$this->Paginator->sort('name'),
		$this->Paginator->sort('Creators.first_name', 'Creator'),
		$this->Paginator->sort('Groups.name', 'Charity'),
		$this->Paginator->sort('start_date'),
		$this->Paginator->sort('city'),
		$this->Paginator->sort('created'),
		'Tags',
		''
	];

	echo $this->Html->tableHeaders($headers);

	?>
	</thead>
	<tbody>
	<?php

	if (!$projects->isEmpty())
		foreach ($projects as $project) {

			$actions = [];

			$actions[] = $this->Html->link(
				'View',
				['action' => 'view', $project->id],
				['class' => 'btn btn-sm btn-primary']
			);

			if (
				$authUser->role == 'A'
				|| $project->creator_id == $authUser->id
				||
				$project->isAdminAnyGroups($authUser)
			) {

				$actions[] = $this->Html->link(
					'Edit',
					['action' => 'edit', $project->id],
					['class' => 'btn btn-sm btn-primary']
				);

			}

			if (in_array($authUser->role, ['A'])) {

				$actions[] = $this->Form->postLink(
					'Delete',
					['action' => 'delete', $project->id],
					[
						'class' => 'btn btn-sm btn-danger',
						'confirm' => 'Are you sure you want to delete this project?'
					]
				);

			}

			$creator = '-';

			if (!empty($project->creator)) {

				$creator = $this->Html->link(
					$project->creator->name,
					['controller' => 'Users', 'action' => 'view', $project->creator->id]
				);

			}

			$row = [
				$this->Html->link(
					$project->name,
					['action' => 'view', $project->id],
					['escape' => false]
				).(!empty($project->approval_stamp) ? ' <span class="label label-default">Approved</span>' : ''),
				$creator,
				$project->projectCharities(),
				$project->start_date,
				sprintf('%s, %s', $project->city, $project->country),
				$project->created,
				implode(', ', $project->tagsList()),
				[implode('', $actions), ['class' => 'compact']]
			];

			echo $this->Html->tableCells($row);

		} else {
			echo $this->Html->tableCells([[
				['No projects found.', ['colspan' => count($headers)]]
			]]);
		}
	?>
	</tbody>
</table>

<?= $this->element('pagination', ['noPadding' => true]); ?>
