<?php
echo $this->Html->css('plugins/jasny/jasny-bootstrap.min');
echo $this->Html->script([
	'plugins/jasny/jasny-bootstrap.min',
	'plugins/jasny/jasny-bootstrap.min'
], ['block' => true]);

$scriptBlock = <<<JS

var userList = function () {

	$.post(
		'{$this->Url->build(['action' => 'ajax_html_user_list'])}',
		{
			id: '{$project->id}'
		},
		function (response) {

			$('#user-list').html(response);

		}
	);

};

var addUser = function (self) {

	$.post(
		'{$this->Url->build(['action' => 'ajax_add'])}.json',
		{
			id: '{$project->id}',
			user_id: $(self).data('id'),
			model: 'Users'
		},
		function () {

			$(self).hide();

			userList();

		}
	);

};

var projectAccess = function (self) {

	$.post(
		'{$this->Url->build(['action' => 'ajax_project_access'])}.json',
		{
			project_request_id: $(self).data('id'),
			type: $(self).data('type')
		},
		function (response) {

			if (response.status == 'ok')
				$('#project-access-buttons-' + $(self).data('id')).hide();

		}
	);

};

var removeUser = function (self) {

	$.post(
		'{$this->Url->build(['action' => 'ajax_remove'])}.json',
		{
			id: '{$project->id}',
			user_id: $(self).data('id'),
			model: 'Users'
		},
		function () {

			$(self).hide();

			userList();

		}
	);

};

var removeNote = function (self) {

	$.post(
		'{$this->Url->build(['action' => 'ajax_remove_note'])}.json',
		{
			id: $(self).data('id')
		},
		function () {
			$(self).parent().parent().hide();
		}
	);

};

$(function () {

	var addUserTimeout;

	$('#add-user').keypress(function () {

		window.clearTimeout(addUserTimeout);

		var self = this;

		addUserTimeout = window.setTimeout(function () {

			$.post(
				'{$this->Url->build(['controller' => 'Users', 'action' => 'ajax_html_user_list'])}',
				{
					id: '{$project->id}',
					term: $(self).val(),
					model: 'Projects'
				},
				function (response) {

					$('#add-user-response').html(response);

				}
			);

		}, 500);

	});

	userList();

	$('#request-access').click(function () {

		var self = this;

		$.post(
			'{$this->Url->build(['controller' => 'Users', 'action' => 'ajax_request_project_access'])}.json',
			{
				project_id: '{$project->id}'
			},
			function () {

				$(self)
					.append('&nbsp;{$icons['check']}')
					.addClass('disabled');

			}
		);

	});

	$('#cancel-access-request').click(function () {

		var self = this;

		$.post(
			'{$this->Url->build(['controller' => 'Users', 'action' => 'ajax_cancel_project_access_request'])}.json',
			{
				project_id: '{$project->id}'
			},
			function () {

				$(self)
					.append('&nbsp;{$icons['check']}')
					.addClass('disabled');

			}
		);

	});

});

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>

<div class="row">

	<div class="col-md-16">

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['tasks']; ?></span>
				<h3>Project</h3>

				<?php if (
					$authUser->role == 'A'
					|| $project->creator_id == $authUser->id
					|| $project->isAdminAnyGroups($authUser)
				): ?>

					<div class="ibox-tools">

						<?= $this->Html->link(
							$icons['edit'],
							['action' => 'edit', $project->id],
							['escape' => false, 'class' => 'btn btn-icon btn-info']
						); ?>

					</div>

				<?php endif; ?>

			</div>

			<div class="ibox-content" style="overflow: visible;">

				<div class="row">
					<div class="col-md-16">

						<?php if (!empty($project->fundraising_target) && !empty($project->approval_stamp)): ?>

							<div class="pull-right text-right m-l-md">
								<small>Fundraising Target</small>
								<h3><?= $this->Number->currency($project->fundraising_target); ?></h3>
							</div>

						<?php endif; ?>

						<h4><?= $project->name; ?></h4>

						<?php if ($project->approval_stamp): ?>
							<span class="label label-default">Approved</span>
						<?php endif; ?>

						<h6>
							<i class="fa fa-calendar fa-fw"></i> <?= $project->start_date; ?><?php if (!empty($project->est_completion_date)): ?> &ndash; <?= $project->est_completion_date; ?><?php endif; ?>
						</h6>

						<h6>
							<i class="fa fa-map-marker fa-fw"></i> <?php printf('%s, %s, %s', $project->city, $project->region, $project->country); ?>
						</h6>

						<p>
							<?= nl2br($project->description); ?>
						</p>

						<p><?= $this->Html->link($project->website, 'http://'.$project->website, ['target' => '_blank']); ?></p>

					</div>

				</div>

			</div>
		</div>

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['calendar']; ?></span>
				<h3>Project Events</h3>

				<div class="ibox-tools">
					<?= $this->Html->link('Calendar',
					['controller' => 'Events', 'action' => 'index'],
					['class' => 'btn btn-info']); ?>
				</div>

			</div>
			<div class="ibox-content">

				<table class="table">
					<thead>
						<?php
							$headers = [
								'Event',
								'Creator',
								'Event Date'
							];
							echo $this->Html->tableHeaders($headers);
						?>
					</thead>
					<tbody>
					<?php
						$rows = [];
						if (!empty($project->events)) {
							foreach ($project->events as $event) {
								$rows[] = [
									$this->Html->link(
										$event->name,
										['controller' => 'Events', 'action' => 'index', $event->id]
									),
									$this->Html->link(
										$event->user->name,
										['controller' => 'Users', 'action' => 'view', $event->user->id]
									),
									$event->date_range
								];

							}
						} else {
							echo $this->Html->tableCells([[
								['No events found.', ['colspan' => 3]]
							]]);
						}
						echo $this->Html->tableCells($rows);
					?>
					</tbody>
				</table>
			</div>
		</div>

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['commenting-o']; ?></span>
				<h3>Project Forum Posts</h3>
				<?php if (in_array($authUser->id, $memberIds)
					// || $authUser->role == 'A'
					|| $project->isAdminAnyGroups($authUser)):
				?>
					<div class="ibox-tools">
						<?= $this->Html->link($icons['plus'],
						['controller' => 'ForumPosts', 'action' => 'add', 'project', $project->id],
						['escape' => false, 'class' => 'btn btn-icon btn-info']); ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="ibox-content">
				<table class="table rowlink"  data-link="row">
					<thead>
						<?php
							$headers = [
								'Title',
								'Creator',
								'Created'
							];
							echo $this->Html->tableHeaders($headers);
						?>
					</thead>
					<tbody>
					<?php
						if (!empty($project->forum_posts)) {
							foreach ($project->forum_posts as $projectForumPost) {
								if (in_array($authUser->id, $memberIds)
										// || $authUser->role == 'A'
										|| $project->isAdminAnyGroups($authUser)
										|| $projectForumPost->post_visibility != 'R') {
									$row = [
										$this->Html->link('<strong>'.$projectForumPost->topic.'<strong>',
											['controller' => 'ForumPosts', 'action' => 'view', $projectForumPost->id],
											['escape' => false]),
										$projectForumPost->user->name,
										$projectForumPost->created,
									];

									echo 	$this->Html->tableCells($row);

								}

							}
						} else {
								echo $this->Html->tableCells([[
									['No events found.', ['colspan' => 3]]
								]]);
						}
					?>
					</tbody>
				</table>
			</div>
		</div>

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['commenting-o']; ?></span>
				<h3>Notes</h3>
			</div>

			<div class="ibox-content">

				<div class="feed-activity-list">

					<?php foreach ($project->project_notes as $note): ?>

						<?php if ($note->status == 'A'): ?>
							<div class="feed-element">
								<?php if (
									$project->status != 'R'
									&& ($authUser->role == 'A' || $authUser->id == $note->user->id || $project->isAdminAnyGroups($authUser))
								): ?>
									<div class="ibox-tools">
										<?= $this->Html->link('Delete',
										'javascript:;',
											[
												'class' => 'btn btn-sm btn-danger pull-right',
												'onclick' => 'removeNote(this)',
												'data-id' => $note->id
											]);
										?>
									</div>
								<?php endif; ?>
								<?= $this->Html->link(
									$this->Html->image($note->user->profile_thumb.'?'.microtime(true), ['class' => 'img-circle']),
									['controller' => 'Users', 'action' => 'view', $note->user->id],
									['escape' => false, 'class' => 'pull-left']
								); ?>
								<div class="media-body">
									<strong>

										<?= $this->Html->link($note->user->name, ['controller' => 'Users', 'action' => 'view', $note->user->id]); ?>

									</strong>
									<br>
									<small class="text-muted"><?= $note->created; ?></small>
									<p>
										<?= nl2br($note->note); ?>
									</p>
								</div>
							</div>
						<?php endif; ?>
					<?php endforeach; ?>

					<?php if ($project->status != 'R'): ?>

						<?= $this->Form->create(null, ['class' => 'm-t-md']); ?>

						<?= $this->Form->input('note', ['type' => 'textarea', 'label' => 'Add Note']); ?>

						<div class="text-right">

							<?= $this->Form->submit('Submit', ['class' => 'btn-sm']); ?>

						</div>

						<?= $this->Form->end(); ?>

					<?php endif; ?>

				</div>

			</div>
		</div>

	</div>

	<div class="col-md-8 col-sm-24">
		<div class="ibox">

			<div class="ibox-title">
				<span><?= $icons['group'] ?></span>
				<h3>Related Groups</h3>
			</div>

			<div class="tab-content ibox-content equalize1" style="height: 220px; overflow-x: auto; padding-bottom: 40px;">
				<div class="panel-body">
					<?php if (!empty( $groups = $project->groups)): ?>

						<?php foreach ($groups as $group):	?>

							<?= $this->Html->link(
								$this->Html->image($group->profile_thumb . '?' . microtime(true), ['class' => 'm-xs']),
								['controller' => 'Groups', 'action' => 'view', $group->id],
								['escape' => false, 'title' => $group->name, 'data-toggle' => 'tooltip']
							); ?>

						<?php endforeach; ?>

					<?php else: ?>
						<p class="text-muted">No related groups yet...</p>
					<?php endif; ?>

				</div>
			</div>

		</div>
	</div>

	<div class="col-md-8">

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['users']; ?></span>
				<h3>Members</h3>

				<?php if ($project->status != 'R'): ?>

					<div class="ibox-tools">

						<?php if ($requestProjectAccess): ?>

							<?= $this->Html->link(
								'Request Project Access',
								'javascript:;',
								['class' => 'btn btn-sm btn-info', 'id' => 'request-access']
							); ?>

						<?php endif; ?>

						<?php if ($project->getRequestStatus($authUser) == 'P'): ?>

							<?= $this->Html->link(
								'Cancel Project Access Request',
								'javascript:;',
								['class' => 'btn btn-sm btn-danger', 'id' => 'cancel-access-request']
							); ?>

						<?php endif; ?>

					</div>

				<?php endif; ?>

			</div>

			<div class="ibox-content" style="overflow-y: scroll; height: 400px;">

				<?php if (
					$project->status != 'R'
					&& ($authUser->role == 'A'
					|| $project->isAdminAnyGroups($authUser)
					)
				): ?>

					<?= $this->Form->input(
						'add_user',
						[
							'placeholder' => 'Search for Contact Name',
							'label' => 'Add Member'
						]
					); ?>

					<div id="add-user-response"></div>

				<?php endif; ?>

				<div id="user-list"></div>

			</div>
		</div>

		<?php if ($project->status != 'R' && ($authUser->role == 'A'
		|| $project->isAdminAnyGroups($authUser)
		)): ?>

			<div class="ibox">
				<div class="ibox-title">
					<span><?= $icons['user-plus']; ?></span>
					<h3>Pending Access Requests</h3>
				</div>

				<div class="ibox-content" style="max-height: 300px; overflow-y: scroll;">

					<ul class="list-group user-list-group">

						<?php if (!empty($project->project_requests)): ?>

							<?php foreach ($project->project_requests as $request): ?>

								<li class="list-group-item">
									<?= $this->Html->link(
										sprintf(
											'%s %s',
											$this->Html->image($request->user->profile_thumb.'?'.microtime(true), ['class' => 'img-circle']),
											$request->user->name
										),
										['controller' => 'Users', 'action' => 'view', $request->user->id],
										['escape' => false]
									); ?>

									<div id="project-access-buttons-<?= $request->id; ?>" class="pull-right">

										<?= $this->Html->link(
											'Accept',
											'javascript:;',
											[
												'class' => 'btn btn-sm btn-primary',
												'onclick' => 'projectAccess(this)',
												'data-id' => $request->id,
												'data-type' => 'A'
											]
										); ?>

										<?= $this->Html->link(
											'Reject',
											'javascript:;',
											[
												'class' => 'btn btn-sm btn-danger',
												'onclick' => 'projectAccess(this)',
												'data-id' => $request->id,
												'data-type' => 'R'
											]
										); ?>

									</div>
								</li>

							<?php endforeach; ?>

						<?php else: ?>

							<li class="list-group-item disabled">
								No requests found.
							</li>

						<?php endif; ?>

					</ul>

				</div>
			</div>

		<?php endif; ?>

		<div class="ibox">

			<div class="ibox-title">
				<span><?= $icons['tags']; ?></span>
				<h3>Tags</h3>
			</div>

			<div class="ibox-content">

				<?php foreach ($project->tags as $tag): ?>
					<span class="label label-primary m-r-xs">
						<?= $tag->treeLabel(); ?>
					</span>
				<?php endforeach; ?>

			</div>

		</div>

	</div>

</div>
