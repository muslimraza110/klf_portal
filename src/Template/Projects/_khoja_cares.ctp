<?php if (!$khojaCareCategories->isEmpty()): ?>
	
	<?php foreach ($khojaCareCategories as $khojaCareCategory): ?>

		<?php if (!empty($projects = $khojaCareCategory->projects)): ?>

			<h2><?= $khojaCareCategory->name;?></h2>
			<table class="table table-bordered">
				<thead>
				<?php
				$width = ['style' => 'width:17%;'];
				$headers = [
					['Name' => $width],
					['Creator' => $width],
					['Charity' => $width],
					['Start Date'=> $width],
					'City',
					'Created',
					['' => ['class' => 'compact']],
				];

				echo $this->Html->tableHeaders($headers);

				?>
				</thead>
				<tbody>
				<?php

					foreach ($projects as $project) {

						$actions = [];

						$actions[] = $this->Html->link(
							'View',
							['action' => 'view', $project->id],
							['class' => 'btn btn-sm btn-primary']
						);

						// if (in_array($authUser->role, ['A', 'F']) || $project->isMember($authUser)) {
						if (in_array($authUser->role, ['A'])  || $project->isAdminAnyGroups($authUser)) {

							$actions[] = $this->Html->link(
								'Edit',
								['action' => 'edit', $project->id],
								['class' => 'btn btn-sm btn-primary']
							);

							$actions[] = $this->Form->postLink(
								'Inactivate',
								['action' => 'delete', $project->id],
								[
									'class' => 'btn btn-sm btn-danger',
									'confirm' => 'Are you sure you want to delete this project?'
								]
							);

						}

						$creator = '-';

						if (!empty($project->creator)) {

							$creator = $this->Html->link(
								$project->creator->name,
								['controller' => 'Users', 'action' => 'view', $project->creator->id]
							);

						}

						$row = [
							$this->Html->link(
								$project->name,
								['action' => 'view', $project->id],
								['escape' => false]
							).(!empty($project->approval_stamp) ? ' <span class="label label-default">Approved</span>' : ''),
							$creator,
							$project->projectCharities(),
							$project->start_date,
							sprintf('%s, %s', $project->city, $project->country),
							$project->created,
							[implode('', $actions), ['class' => 'compact']]
						];

						echo $this->Html->tableCells($row);

					}
				?>
				</tbody>
			</table>

	<?php endif; ?>

	<?php endforeach; ?>
	
<?php endif; ?>

<!-- <?= $this->element('pagination', ['noPadding' => true]); ?> -->
