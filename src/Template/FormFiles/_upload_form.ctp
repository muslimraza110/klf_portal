<?= $this->element('form_steps', [
  'next' => 'SUBMIT',
  'steps' => [
    ['controller' => 'Forms', 'action' => 'edit', $form->id]
  ]
]); ?>

<fieldset>
	<legend><?= $form->name; ?> ( Upload a PDF)</legend>
	<?php
		echo $this->Form->create($formFile, ['type' => 'file']);
		echo $this->Form->input("pdf_form.upload", ['label' => false, 'type' => 'file']);
		echo $this->Form->submit('Upload');
		echo $this->Form->end();
	?>
</fieldset>