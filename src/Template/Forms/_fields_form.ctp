<?php
  $this->Html->script([
		'ckeditor/ckeditor',
		'ng-assets/factories/form-factory',
	], ['block' => true]);

  $this->Html->scriptBlock('
    KhojaApp.controller("FormController", ["$scope", "FormFactory", function($scope, FormFactory) {

      $scope.formEntity = ' . safe_json_encode($form) . ';
      $scope.requestData = ' . safe_json_encode($this->request->data) . ';
      $scope.form = mergeObj($scope.formEntity, $scope.requestData);

      if($scope.form.form_fields == undefined) {
        $scope.form.form_fields = [];
      }

      $scope.validationErrors = ' . safe_json_encode($validationErrors) . ';

      $scope.fieldTypes = ' . safe_json_encode($fieldTypes) . ';
      $scope.typesWithOptions = ["dropdown", "checkboxes", "radio"];
			$scope.typesNotFormFields = ["text_block"];

      $scope.sortableOptions = {
        axis: "y",
        handle: "h4 .fa",
      };

      $scope.checkType = function(type1, type2) {
        if(type1 == type2)
          return true;
        else
          return false;
      }

      $scope.addField = function(event, type) {

        var newFieldObj = {
          id: "",
          form_id: "' . $form->id . '",
          order_num: $scope.form.form_fields.length,
          name: "",
          type: type,
          options: "",
          option_array: []
        };

        if($scope.typesWithOptions.indexOf(type) !== -1) {
          newFieldObj.option_array.push("");
        }

        $(event.currentTarget).effect("transfer", {to: "#field-placeholder"}, 1000, function() {
          $scope.$apply(function() {
            $scope.form.form_fields.push(newFieldObj);
          });
        });

      };

      $scope.deleteField = function(index) {
        var r = confirm("Are you sure you want to delete this form field?");

        if (r) {
          if (! isEmpty($scope.form.form_fields[index].id)) {

            FormFactory.deleteFormField("' . $this->request->prefix . '", $scope.form.form_fields[index].id).then(function() {
              $scope.form.form_fields.splice(index, 1);
            });
          }
          else {
            $scope.form.form_fields.splice(index, 1);
          }
        }
      };

      $scope.addOption = function(index) {
        $scope.form.form_fields[index].option_array.push("");
      };

      $scope.removeOption = function(index, parentIndex) {
        if(index) //cannot remove first option
          $scope.form.form_fields[parentIndex].option_array.splice(index, 1);
      };

    }]);
  ', ['block' => true]);
?>


<?= $this->Form->create($form, ['ng-controller' => "FormController"]); ?>

<?= $this->element('form_steps', [
  'next' => 'SUBMIT',
  'steps' => [
    ['action' => 'edit', $form->id]
  ]
]); ?>

<div class="row">
  <div class="col-xs-24 col-sm-8 col-lg-5">
		
    <div class="panel panel-primary panel-form">
      <div class="panel-heading">Add Form Field</div>

      <div class="list-group">
        <a href="#" class="list-group-item" ng-repeat="(type, label) in fieldTypes" ng-click="addField($event, type)" ng-if="typesNotFormFields.indexOf(type) == -1">
          <?= $icons['plus-circle'] ?>{{label}}
        </a>
      </div>
    </div>
		
		<div class="panel panel-primary panel-form">
      <div class="panel-heading">Add Form Element</div>

      <div class="list-group">
        <a href="#" class="list-group-item" ng-repeat="(type, label) in fieldTypes" ng-click="addField($event, type)" ng-if="typesNotFormFields.indexOf(type) != -1">
          <?= $icons['plus-circle'] ?>{{label}}
        </a>
      </div>
    </div>

  </div>
  <div class="col-xs-24 col-sm-16 col-lg-19">

    <legend><?= $form->name ?></legend>

    <div ui-sortable="sortableOptions" ng-model="form.form_fields">

      <fieldset ng-repeat="field in form.form_fields" class="form-field-fieldset form-field-{{field.type}}">

        <h4>
          <?= $icons['arrows'] ?>{{fieldTypes[field.type]}}
          <a href="#" ng-click="deleteField($index)" class="btn btn-danger btn-xs pull-right">Remove</a>
        </h4>


        <?= $this->Form->input('form_fields.{{$index}}.id', [
          'type' => 'hidden',
          'ng-model' => 'field.id', //somehow does not work
          'value' => "{{field.id}}" //need value set explicitly
        ]); ?>

        <?= $this->Form->input('form_fields.{{$index}}.order_num', [
          'type' => 'hidden',
          'ng-model' => 'field.order_num', //somehow does not work
          'value' => '{{$index+1}}' //need value set explicitly
        ]); ?>

        <?= $this->Form->input('form_fields.{{$index}}.type', [
          'type' => 'hidden',
          'ng-model' => 'field.type', //somehow does not work
          'value' => '{{field.type}}' //need value set explicitly
        ]); ?>
				
				<div ng-if="typesNotFormFields.indexOf(field.type) == -1">
	
					<?= $this->Form->input('form_fields.{{$index}}.name', [
						'ng-model' => 'field.name',
						'label' => false,
						'type' => 'text',
						'templates' => [
							'inputContainer' => '<div class="form-group form-group-spaced-container {{type}}{{required}}" ng-class="{\'has-error\' : validationErrors.form_fields[$index].name}">{{content}}</div>',
							'input' => '
								<label for="form_fields.{{$index}}.name">
									<span ng-if="!checkType(field.type, \'checkbox\')">Q{{$index+1}}:</span>
									<div ng-if="checkType(field.type, \'checkbox\')">
										<input type="checkbox" icheck="icheck" name="example-checkboxes-{{$index}}">
									</div>
								</label>
								<div class="form-group-spaced">
									<input type="{{type}}" name="{{name}}" class="form-control{{attrs.class}}" {{attrs}} />
									<span class="help-block error-message" ng-if="validationErrors.form_fields[$index].name">{{validationErrors.form_fields[$index].name}}</span>
								</div>
							',
						]
					]); ?>
					
					<div class="form-group">
						<label>Required</label>
						<switch name="form_fields[{{$index}}][required]" ng-model="field.required"></switch>
						<?= $this->Form->input('form_fields.{{$index}}.required', [
							'type' => 'hidden',
							'ng-model' => 'field.required', //somehow does not work
							'value' => '{{field.required}}' //need value set explicitly
						]); ?>
					</div>
	
					<div ng-if="typesWithOptions.indexOf(field.type) != -1" class="options">
	
						<div ng-repeat="option in field.option_array track by $index">
							<?= $this->Form->input('form_fields.{{$parent.$index}}.option_array.{{$index}}', [
								'ng-model' => 'field.option_array[$index]',
								'label' => false,
								'templates' => [
									'inputContainer' => '<div class="form-group form-group-spaced-container {{type}}{{required}}" ng-class="{\'has-error\' : validationErrors.form_fields[$parent.$index][\'option_array.\' + $index]}">{{content}}</div>',
									'input' => '
										<label for="form_fields.{{$parent.$index}}.option_array.{{$index}}">
											<div ng-if="checkType(field.type, \'checkboxes\')">
												<input type="checkbox" icheck="icheck" name="example-checkboxes-{{$parent.$parent.$index}}">
											</div>
											<div ng-if="checkType(field.type, \'radio\')">
												<input type="radio" icheck="icheck" name="example-radio-{{$parent.$parent.$index}}">
											</div>
											<span ng-if="checkType(field.type, \'dropdown\')">O{{$index+1}}:</span>
										</label>
										<div class="form-group-spaced">
											<input type="{{type}}" name="{{name}}" class="form-control{{attrs.class}}" {{attrs}} />
											<span class="help-block error-message" ng-if="validationErrors.form_fields[$parent.$index][\'option_array.\' + $index]">Cannot be empty</span>
										</div>
										<a href="#" ng-click="removeOption($index, $parent.$index)" class="remove-option" ng-class="{ \'disabled\': !$index }">' . $icons['times-circle'] . '</a>
									',
								]
							]); ?>
						</div>
	
						<a href="#" class="btn btn-xs btn-success" ng-click="addOption($index)">Add Option</a>
					</div>
					
				</div>
				
				
				<div ng-if="typesNotFormFields.indexOf(field.type) != -1">
					
					<?= $this->Form->input('form_fields.{{$index}}.name', [
						'ng-model' => 'field.name',
						'label' => 'Content',
						'type' => 'textarea',
						'ckeditor' => "{customConfig: '" . $appConfig['path'] . "js/ckeditor/config-minimal-form-css.js'}"
					]); ?>
					
					
				</div>


      </fieldset>

    </div>

    <div id="field-placeholder">Please use the menu on the right to add fields to your form.</div>

  </div>
</div>



<div class="submit-big">
	<?= $this->Form->submit('Save', ['class' => 'btn btn-info']); ?>
</div>

<?= $this->Form->end(); ?>
