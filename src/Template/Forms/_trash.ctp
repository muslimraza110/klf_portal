<table class="table table-bordered table-with-status">
  <thead>
    
    <tr class="status-heading">
      <th colspan="6"></th>
      <th colspan="3" class="status-heading-cell">Status</th>
      <th colspan="2"></th>
    </tr>
    
    <?= $this->Html->tableHeaders([
        $this->Paginator->sort('name'),
        $this->Paginator->sort('start_date', 'Start'),
        $this->Paginator->sort('end_date', 'End'),
        $this->Paginator->sort('topic', 'Topic'),
        'Audience',
        $this->Paginator->sort('Users.first_name', 'Created by'),
        ['Pending' => ['class' => 'compact']],
        ['Current' => ['class' => 'compact']],
        ['Completed' => ['class' => 'compact']],
        ['Response' => ['class' => 'compact text-center']],
        ['Actions' => ['class' => 'compact text-center']]
      ]);
    ?>
  </thead>

  <tbody>
    <?php 
      if (! $forms->isEmpty()) {
        $rows = [];
        
        foreach ($forms as $form) {
          
          $actions = $this->Form->postLink(
            $icons['reply'] . 'Restore',
            ['controller' => 'Forms', 'action' => 'delete', $form->id, true],
            ['escape' => false, 'class' => 'btn btn-info']
          );
          
          $actions .= $this->Form->postLink(
            $icons['delete'] . 'Delete',
            ['controller' => 'Forms', 'action' => 'trash', $form->id],
            [
              'escape' => false,
              'class' => 'btn btn-danger',
              'confirm' => 'Do you want to permanently delete "' . $form->name . '" ?'
            ]
          );

          $rows[] = [
            $this->Html->link($form->name, ['controller' => 'Forms', 'action' => 'view', $form->id]),
            
            $form->start_date,
            $form->end_date,
            $form->topic,
            $form->getAudienceList(),
            $form->user->name,
            
            [
              ($form->isPending()) ? '<i class="dot text-warning"></i>' : '',
              ['class' => 'status']
            ],
            
            [
              ($form->isCurrent()) ? '<i class="dot text-success"></i>' : '',
              ['class' => 'status']
            ],
            
            [
              ($form->isCompleted()) ? '<i class="dot text-danger"></i>' : '',
              ['class' => 'status']
            ],
            
            [$form->response(), ['class' => 'compact text-center']],
            
            [$actions, ['class' => 'compact']]
          ];
        }

        echo $this->Html->tableCells($rows);
      }
    ?>
  </tbody>
</table>

<?= $this->element('pagination', ['noPadding' => true]); ?>