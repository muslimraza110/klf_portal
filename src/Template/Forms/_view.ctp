<?php

	if ($form->isPending()) {
		echo '<div class="alert alert-warning" style="overflow: hidden;">
			<i class="fa fa-exclamation-circle fa-3x fa-pull-left" aria-hidden="true"></i>
			This is only a preview.<br />This page will only be available after ';
		echo (!empty($form->start_date)) ? $form->start_date : 'the start date is set';
		echo '.</div>';
	}
	
	if ($form->isPost()) {
		echo $this->element('views/form_post', ['form' => $form, 'isMain' => true]);
	}
	else {
		echo $this->element('views/form', ['form' => $form, 'isMain' => true]);
	}

	if (isset($relatedPosts) && !empty($relatedPosts)) {
		foreach($relatedPosts as $relatedPost) {
			
			switch($relatedPost['model']) {
				
				case 'Posts':
					echo $this->element('views/post', ['post' => $relatedPost['data']]);
					break;
				
				case 'Forms':
					echo $this->element('views/form', ['form' => $relatedPost['data']]);
					break;
				
			}
			
		}
	}
	
?>