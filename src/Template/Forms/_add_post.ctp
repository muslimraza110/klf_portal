<?= $this->element('form_steps', [
  //'next' => 'SUBMIT',
  'steps' => [
    ['action' => 'edit', $form->id]
  ]
]); ?>

<?= $this->element('ckeditor'); ?>

<fieldset>
	<legend><?= $form->name ?> (Create a Post)</legend>
	
	<?php
		echo $this->Form->create($form);
		echo $this->Form->input('form_post.content', ['class' => 'ckeditor']);
	?>

	<div class="submit-big">
		<?= $this->Form->submit('Continue', ['class' => 'btn btn-info']); ?>
	</div>

	<?= $this->Form->end(); ?>
</fieldset>