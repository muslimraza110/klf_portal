<?= $this->element('form_steps', [
  //'next' => 'SUBMIT',
  'steps' => [
    ['action' => 'edit', $form->id]
  ]
]); ?>

<fieldset>
	<legend><?= $form->name ?> (Upload a PDF)</legend>
	<?php
		echo $this->Form->create($form, ['type' => 'file']);

		$uploadFormPlaceholder = 'Upload a Form...';

		if ((isset($form->form_upload) && !empty($form->form_upload->name))) {
			$uploadFormPlaceholder = $form->form_upload->name;
		}

		echo $this->Form->input("form_upload.upload", ['label' => false, 'type' => 'file', 'placeholder' => $uploadFormPlaceholder]);
	?>

	<div class="submit-big">
		<?= $this->Form->submit('Continue', ['class' => 'btn btn-info']); ?>
	</div>

	<?= $this->Form->end(); ?>
</fieldset>