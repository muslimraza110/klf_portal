<?php
	echo $this->Html->scriptBlock('
		$("table").stupidtable();
	', ['block' => true]);

	$this->Html->scriptBlock('
		KhojaApp.controller("FormStatsController", function($scope) {
			
			$scope.stats = ' . safe_json_encode($stats) . ';
			
			$scope.responseChart = {
				labels: ["Answered", "Did not answer", "Not in targeted audience"],
				data: [parseInt($scope.stats["In Audience"]), parseInt($scope.stats["Did Not Answer"]), parseInt($scope.stats["Not In Audience"])],
				colours: ["#79CA57", "#DCDCDC", "#f0ad4e"]
			};
		});
  ', ['block' => true]);
?>

<div class="row" ng-controller="FormStatsController">
	<div class="col-xs-24 col-md-12 col-lg-8">
		
		<table class="table table-bordered table-with-status">
			<thead>
				<tr class="status-heading">
					<th class="status-heading-cell" colspan="3" style="padding: 8px !important; text-align: left;">
						People Who Answered
						<span class="pull-right">
							<span class="label label-primary">
								<?php
									$countInAudience = isset($userForms['In Audience']) ? count($userForms['In Audience']) : 0;
									$countNotInAudience = isset($userForms['Not In Audience']) ? count($userForms['Not In Audience']) : 0;
									
									echo $countInAudience + $countNotInAudience;
								?> entries
							</span>
						</span>
					</th>
				</tr>
				<?= $this->Html->tableHeaders([
					['<a>Role</a>' => ['data-sort' => 'string-ins']],
					['<a>Name</a>' => ['data-sort' => 'string-ins']],
					['<a>Date</a>' => ['data-sort' => 'int']],
				]); ?>
			</thead>
			<tbody>
				<?php
					foreach ($userForms as $type => $userFormArray) {
						foreach($userFormArray as $userForm) {
							$username = ($userForm->user->status == 'D') ? '<s>' . $userForm->user->name . '</s>' : $userForm->user->name;
							$row = [
								$userForm->user->role_label,
								$this->Html->link($username, ['controller' => 'Forms', 'action' => 'user_answers', $userForm->id], ['escape' => false]),
								[$userForm->created, ['data-sort-value' => $userForm->created->toUnixString()]],
							];
							
							if ($type == 'In Audience')
								echo $this->Html->tableCells([$row]);
							else
								echo $this->Html->tableCells([$row], ['class' => 'text-muted'], ['class' => 'text-muted']);
						}
					}
				?>
			</tbody>
		</table>
		
		<table id="unanswered-table" class="table table-bordered">
			<thead>
				<?= $this->Html->tableHeaders([
					'People Who Didn\'t Answer<span class="pull-right"><span class="label label-primary">' . $expectedUsers->count() . ' users</span> '.
					$this->Html->link(
						'Remind Users',
						['action' => 'remind_unanswered', $form->id],
						['class' => 'btn btn-primary btn-xs']
					)
					.'</span>'
				]); ?>
			</thead>
			<tbody>
				<tr>
					<td style="padding: 0 !important;">
						<div ng-scrollbars ng-scrollbars-config="{setHeight: 250}">
							<ul class="like-table-rows">
								<?php
									foreach ($expectedUsers as $expectedUser) {
										echo '<li>' . $expectedUser->name . '</li>';
									}
								?>
							</ul>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		
	</div>
	<div class="col-xs-24 col-md-12 col-lg-16">
		
		<div class="widget form-box blue-bg">
			<div class="text-center article-title" style="margin: 0;">
				<span class="text-muted">Post # F<?= $form->id ?></span>
				<h2><?= $form->name; ?></h2>
			</div>
	</div>
		
		<div class="row" style="margin-top: -10px;">
			
			<div class="col-xs-24 col-md-16">
				<div class="row">
					
					<div class="col-xs-12">
						<div class="widget widget-md navy-bg p-lg text-center">
							<h3>Published Date</h3>
							<p><?= $form->published; ?></p>
						</div>
					</div>
					
					<div class="col-xs-12">
						<div class="widget widget-md navy-bg p-lg text-center">
							<h3 style="font-size: 40px; margin-bottom: 10px;"><?= $form->total_questions; ?></h3>
							<p>questions in the form</p>
						</div>
					</div>
					
					<div class="col-xs-12">
						<div class="widget widget-md navy-bg p-lg text-center">
							
							<h3>Additional Info</h3>
							
							<ul class="response-list text-left" style="padding-left: 15px;">
								<li>
									Currently contains up to <?= ($stats['Max Repeat Per User'] > 1) ? $stats['Max Repeat Per User'] . ' entries' : $stats['Max Repeat Per User'] . ' entry' ?> per user
								</li>
								<li>
									<?= ($stats['Users Deleted'] > 1) ? $stats['Users Deleted'] . ' users have' : $stats['Users Deleted'] . ' user has' ?> been deleted since the publication of this form.
								</li>
							</ul>
							
						</div>
					</div>
					
				</div>
			</div>
			<div class="col-xs-24 col-md-8">
				
				<div class="widget widget-lg navy-bg p-lg text-center">
					
					<canvas id="response-pie" class="chart chart-pie" chart-data="responseChart.data" chart-labels="responseChart.labels" chart-colours="responseChart.colours"></canvas>
					
					<ul class="response-list list-unstyled m-t-md text-left">
						<li>
							<span class="number" style="background: #79CA57;"><?= $stats['In Audience'] ?></span>
							<span class="text">users answered</span>
						</li>
						<li>
							<span class="number" style="background: #DCDCDC;"><?= $stats['Did Not Answer'] ?></span>
							<span class="text">users did not answer</span>
						</li>
						<li>
							<span class="number" style="background: #f0ad4e;"><?= $stats['Not In Audience'] ?></span>
							<span class="text">users answered but were not part of the targeted audience</span>
						</li>
					</ul>
					
					<ul class="response-list list-unstyled m-t-md text-left">
						<li>
							<span class="number"><?= $stats['Expected Answers'] ?></span>
							<span class="text">users were expected to answer as part of the targeted audience</span>
						</li>
					</ul>
					
				</div>
			</div>
			
			
			
			
		</div>
		
	</div>
</div>

