<?php
	$this->Html->scriptBlock('
		KhojaApp.controller("ViewAnswersController", function($scope) {
			
			$scope.formFields = ' . safe_json_encode($form->form_fields) . ';
			$scope.charts = {};
			
			angular.forEach($scope.formFields, function(value, key) {
				if ($scope.formFields.hasOwnProperty(key) && !isEmpty(value.count_per_answer)) {
					
					$scope.charts[key] = {
						chartData: [],
						chartLabels: [],
					};
					
					var count_per_answer = JSON.parse(value.count_per_answer);
					
					$scope.charts[key].chartLabels = Object.keys(count_per_answer);
					
					for (var i in count_per_answer) {
						$scope.charts[key].chartData.push(count_per_answer[i]);
					}
					
				}
			});
			
		});
  ', ['block' => true]);
?>

<div class="row">
	<div class="col-xs-24 col-md-12 col-md-push-12 col-lg-16 col-lg-push-8">

		<div class="form-box" id="form-<?= $form->id ?>" ng-controller="ViewAnswersController">

			<?= $this->Html->link('Return to stats', ['action' => 'stats', $form->id], [
				'class' => 'btn btn-info'
			]); ?>
			
			<span class="text-primary pull-right">Answered By: <?= ($user->status == 'D') ? '<s>' . $user->name . '</s>' : $user->name; ?><br />on <?= $form->auth_user_data->created ?></span><br />
			<div class="text-center article-title">
				<span class="text-muted"><?= (!empty($form->published)) ? $form->published : 'Unpublished' ?></span>
				<br /><span class="text-muted">Post # F<?= $form->id ?></span>
				<h2><?= $form->name; ?></h2>
			</div>

		  <?php
				echo $this->Form->create(null, [
					'id' => 'Forms-' . $form->id,
					'class' => 'is-disabled',
					'url' => ['controller' => 'Forms', 'action' => 'save_answers', $form->id, '?' => ['return' => urlencode($this->Url->build())]]
				]);

		    if(!empty($form->form_fields)) {

		      foreach ($form->form_fields as $key => $field) {

						$element =  $this->element('views/form_field', ['formId' => $form->id, 'key' => $key, 'field' => $field, 'isDisabled' => true]);

						if (! empty($field->option_array)) {
							
							echo '<div class="row" style="margin-bottom: 15px;">';
								echo '<div class="col-xs-24 col-md-16 col-lg-18">';
									echo $element;
									
									if (isset($this->request->data['forms'][$field->form_id]['user_form_values'][$key]['invalid_answer'])) {
										echo '<p class="text-danger" style="font-size: 14px;">' .
											'<strong>Answer given by user is not in available options anymore: </strong><br />' .
											$this->request->data['forms'][$field->form_id]['user_form_values'][$key]['invalid_answer'] .
										'</p>';
									}
									
								echo '</div>';
								echo '<div class="col-xs-24 col-md-8 col-lg-6 text-center" style="padding: 0">';
									echo '<label>' . $field->name . '</label>';
									echo '<canvas id="chart-' . $field->id . '" class="chart chart-pie" chart-data="charts['.$key.'].chartData" chart-labels="charts['.$key.'].chartLabels" chart-options="{ legend: { display: true } }"></canvas>';
								echo '</div>';
							echo '</div>';
							
						}
						else {
							echo $element;
						}

		      }

		    }
		    else {
		      echo '<div class="alert alert-default">This form does not have any fields...</div>';
		    }

				echo $this->Form->end();
		  ?>

		</div>

	</div>
	<div class="col-xs-24 col-md-12 col-md-pull-12 col-lg-8 col-lg-pull-16">
		
		<table class="table table-bordered table-with-status">
			<thead>
				<tr class="status-heading">
					<th class="status-heading-cell" colspan="3" style="padding: 8px !important; text-align: left;">
						People Who Answered
						<span class="pull-right">
							<span class="label label-primary">
								<?php
									$countInAudience = isset($userForms['In Audience']) ? count($userForms['In Audience']) : 0;
									$countNotInAudience = isset($userForms['Not In Audience']) ? count($userForms['Not In Audience']) : 0;
									
									echo $countInAudience + $countNotInAudience;
								?> entries
							</span>
						</span>
					</th>
				</tr>
				<?= $this->Html->tableHeaders([
					['<a>Role</a>' => ['data-sort' => 'string-ins']],
					['<a>Name</a>' => ['data-sort' => 'string-ins']],
					['<a>Date</a>' => ['data-sort' => 'int']],
				]); ?>
			</thead>
			<tbody>
				<?php
					foreach ($userForms as $type => $userFormArray) {
						foreach($userFormArray as $userForm) {
							$username = ($userForm->user->status == 'D') ? '<s>' . $userForm->user->name . '</s>' : $userForm->user->name;
							$row = [
								$userForm->user->role_label,
								$this->Html->link($username, ['controller' => 'Forms', 'action' => 'user_answers', $userForm->id], ['escape' => false]),
								[$userForm->created, ['data-sort-value' => $userForm->created->toUnixString()]],
							];
							
							if ($type == 'In Audience')
								echo $this->Html->tableCells([$row]);
							else
								echo $this->Html->tableCells([$row], ['class' => 'text-muted'], ['class' => 'text-muted']);
						}
					}
				?>
			</tbody>
		</table>
		
		<table id="unanswered-table" class="table table-bordered">
			<thead>
				<?= $this->Html->tableHeaders([
					'People Who Didn\'t Answered<span class="label label-primary pull-right">' . $expectedUsers->count() . ' users</span>'
				]); ?>
			</thead>
			<tbody>
				<tr>
					<td style="padding: 0 !important;">
						<div ng-scrollbars ng-scrollbars-config="{setHeight: 250}">
							<ul class="like-table-rows">
								<?php
									foreach ($expectedUsers as $expectedUser) {
										echo '<li>' . $expectedUser->name . '</li>';
									}
								?>
							</ul>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		
	</div>
</div>