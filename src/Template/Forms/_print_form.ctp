<div class="form-box white-bg m-t-lg">
	
	<div class="text-center article-title">
		<br /><span class="text-muted">Post # F<?= $form->id ?></span>
		<h2><?= $form->name; ?></h2>
	</div>	

	<?php 
		if (!empty($form->form_fields)) {
			foreach ($form->form_fields as $key => $field) {
				echo $this->element('views/form_field', [
					'formId' => $form->id, 
					'key' => $key, 
					'field' => $field, 
					'isDisabled' => 'disabled',
					'displayDropdown' => false
				]);
			}
		}
	?>

</div>