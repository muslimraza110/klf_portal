<table class="table table-bordered table-with-status">
  <thead>

    <?php if ($authUser->role == 'A'): ?>
      <tr class="status-heading">
        <th colspan="7"></th>
        <th colspan="3" class="status-heading-cell">Status</th>
        <th colspan="2"></th>
      </tr>
    <?php endif; ?>

    <?php
      $headers = [
        $this->Paginator->sort('Forms.name', 'Name'),
        $this->Paginator->sort('Forms.topic', 'Topic'),
        'Type',
        $this->Paginator->sort('Forms.start_date', 'Start of Publication'),
        $this->Paginator->sort('Forms.end_date', 'End'),
      ];

      if ($authUser->role == 'A') {

        $headers[] = $this->Paginator->sort('Users.first_name', 'Created by');

        $headers[] = ['Pending' => ['class' => 'compact']];
        $headers[] = ['Current' => ['class' => 'compact']];
        $headers[] = ['Completed' => ['class' => 'compact']];

        $headers[] = ['Response' => ['class' => 'compact text-center']];

        $headers[] = ['Actions' => ['class' => 'compact text-center']];
      }

      echo $this->Html->tableHeaders($headers);
    ?>
  </thead>

  <tbody>
    <?php
      if (! $forms->isEmpty()) {
        foreach ($forms as $form) {

          $start = (!empty($form->start_date)) ? $form->start_date : 'Unpublished';

          if($authUser->role == 'A' && $form->isPending() && empty($form->start_date)) {
            $start = $this->Form->postLink('Publish Now', ['action' => 'publish', $form->id], ['class' => 'btn btn-info btn-xs']);
          }

          $end = $form->end_date;

          if ($authUser->role != 'A' && empty($form->end_date))
            $end = '&mdash;';

          if ($authUser->role == 'A') {

            if (!$form->isPdf()) {
            	
              $subactions = '<li>' . $this->Html->link(
                $icons['list-ol'] . 'View Answers',
                ['controller' => 'Forms', 'action' => 'stats', $form->id],
                ['escape' => false]
              ) . '</li>';

              $subactions .= '<li>' . $this->Html->link(
                $icons['print'] . 'Print',
                ['controller' => 'Forms', 'action' => 'print_form', $form->id],
                ['escape' => false, 'target' => '_blank']
              ) . '</li>';

              $subactions .= '<li>' . $this->Html->link(
                $icons['copy'] . 'Duplicate',
                ['controller' => 'Forms', 'action' => 'duplicate', $form->id],
                ['escape' => false]
              ) . '</li>';
							
							$actions = '<div class="btn-group">' .
								$this->Html->link('More<span class="caret" style="margin-left: 8px;"></span>', '#', [
									'escape' => false,
									'class' => 'btn btn-primary dropdown-toggle',
									'data-toggle' => "dropdown",
									'aria-haspopup' => "true",
									'aria-expanded' => "false"
								]) .
								'<ul class="dropdown-menu">' .
									$subactions .
								'</ul>
							</div>';
							
            }
            else {
              $subactions = '';
							$actions = '<a href="#" class="btn btn-primary disabled">More<span class="caret" style="margin-left: 8px;"></span></a>';
            }

            
            $actions .= $this->Html->link(
              $icons['edit'] . 'Edit',
              ['controller' => 'Forms', 'action' => 'edit', $form->id],
              ['escape' => false, 'class' => 'btn btn-primary']
            );

            $actions .= $this->Form->postLink(
              $icons['delete'] . 'Delete',
              ['controller' => 'Forms', 'action' => 'delete', $form->id],
              [
                'escape' => false,
                'class' => 'btn btn-danger',
                'confirm' => 'Do you really want to delete "' . $form->name . '"?'
              ]
            );
          }

          $row = [
            $this->Html->link($form->name, $form->post_link, ['target' => '_blank']),
            $form->topic,
            $form->category,
            $start,
            $end,
          ];

          if ($authUser->role == 'A') {
          	
            $row[] = (isset($form->user->name)) ? $form->user->name : 'Unknown';

            $row[] = [
              ($form->isPending()) ? '<i class="dot text-warning"></i>' : '',
              ['class' => 'status']
            ];

            $row[] = [
              ($form->isCurrent()) ? '<i class="dot text-success"></i>' : '',
              ['class' => 'status']
            ];

            $row[] = [
              ($form->isCompleted()) ? '<i class="dot text-danger"></i>' : '',
              ['class' => 'status']
            ];

            $row[] = [$form->response, ['class' => 'compact text-center']];

            $row[] = [$actions, ['class' => 'compact']];
          }

          echo $this->Html->tableCells([$row]);
        }
      }
    ?>
  </tbody>
</table>

<?php if ($authUser->role == 'A'): ?>
  <div class="row">
    <div class="col-xs-24 col-md-18">
      <?= $this->element('pagination', ['noPadding' => true]); ?>
    </div>
    <div class="col-xs-24 col-md-6">
      <div class="pull-right">

        <?= $this->Html->link($icons['trash'] . 'View Trash', ['controller' => 'Forms', 'action' => 'trash'], [
          'escape' => false,
          'class' => 'btn btn-danger btn-sm'
        ]); ?>

      </div>
    </div>
  </div>
<?php else: ?>
  <?= $this->element('pagination', ['noPadding' => true]); ?>
<?php endif; ?>
