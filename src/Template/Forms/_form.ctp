<?php
	
	echo $this->Html->script([
	  'plugins/datapicker/bootstrap-datepicker',
	  $this->Asset->scripts('upload')
	], ['block' => true]);

	echo $this->Html->css([
		'plugins/datapicker/datepicker3',
		'AssetManager.style'
	]);

	echo $this->Html->scriptBlock("

		var startDate = new Date(),
				fromEndDate = new Date();

		$('#start-date').datepicker({
			//startDate: startDate,
			autoclose: true,
			format: 'DD MM dd, yyyy',
			todayBtn: 'linked',
			todayHighlight: true,
			forceParse: true,
		}).
		on('changeDate', function(selected){
			startDate = new Date(selected.date.valueOf());
			startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
			$('#end-date').datepicker('setStartDate', startDate);
		});

		$('#end-date').datepicker({
			startDate: startDate,
			autoclose: true,
			format: 'DD MM dd, yyyy',
			todayHighlight: true,
		})
		.on('changeDate', function(selected){
			fromEndDate = new Date(selected.date.valueOf());
	    fromEndDate.setDate(fromEndDate.getDate(new Date(selected.date.valueOf())));
	    $('#start-date').datepicker('setEndDate', fromEndDate);
		});
		
		$(function() {
		
			$('label[for=\"audience-a\"]').append(' <small class=\"text-warning\">(Only influences the stats; Admins can always view all forms)</small>');
			
			$('#topic').autocomplete({
				appendTo: '#topic-container',
				source: " . safe_json_encode($topics) . "
			});
			
			$('fieldset').equalizeHeights();
			
			$(window).resize(function() {
				$('fieldset').equalizeHeights();
			});

		});

", ['block' => true]);


echo $this->Html->scriptBlock('

	KhojaApp.controller("FormController", ["$scope", function($scope) {

		$scope.formEntity = ' . safe_json_encode($form) . ';
		$scope.requestData = ' . safe_json_encode($this->request->data) . ';
		$scope.form = mergeObj($scope.formEntity, $scope.requestData);

		if (Boolean(' . $form->isPdf() . ') && ! Boolean(' . $form->isNew() . ')) {
			$scope.form.upload_disabled = true;
		}
		else {
			$scope.form.upload_disabled = false;
		}

		$scope.form.post_disabled = false;
		
		if ( $scope.form.send_email == "on" || $scope.form.email_user_id != undefined )
			$scope.form.send_email = true;
		else
			$scope.form.send_email = false;

		if ( $scope.form.upload_form == "on" || Boolean(' . $form->isPdf() . ') )
			$scope.form.upload_form = true;
		else
			$scope.form.upload_form = false;

		if ( $scope.form.add_post == "on" || Boolean(' . $form->isPost() . ') )
			$scope.form.add_post = true;
		else
			$scope.form.add_post = false;

		if ( Boolean(' . $form->isPdf() . ') || Boolean(' . $form->isPost() . ') || Boolean(' . $form->hasFields() . ') ) {
			$scope.form.upload_disabled = true;
			$scope.form.post_disabled = true;
		}

		/*
		$scope.$watch("form.upload_form", function (newVal) {
		  if (newVal != false)
		 	  $scope.form.post_disabled = true;
		  else
		 		$scope.form.post_disabled = false;
		});

		$scope.$watch("form.add_post", function (newVal) {
		  if (newVal != false)
		 	  $scope.form.upload_disabled = true;
		  else
		 	  $scope.form.upload_disabled = false;
		});*/

		$scope.ifUpload = function(newVal) {
			if (! $scope.form.upload_disabled) {
				if (newVal != false)
			 	  $scope.form.post_disabled = true;
			  else
			 		$scope.form.post_disabled = false;
			 }
		};

		$scope.ifPost = function(newVal) {
			if (! $scope.form.post_disabled) {
				if (newVal != false)
			 	  $scope.form.upload_disabled = true;
			  else
			 	  $scope.form.upload_disabled = false;
			}
		};


	}]);

', ['block' => true]);
?>



<?= $this->Form->create($form, ['ng-controller' => 'FormController', 'novalidate']); ?>

<?= $this->element('form_steps', ['next' => 'SUBMIT']); ?>

<div class="ibox">
	<div class="ibox-content">
		
		<div class="row">
			<div class="col-xs-24 col-md-20 col-md-offset-2 col-lg-16 col-lg-offset-4">
				
				<?= $this->Form->input('name'); ?>
				
				<?= $this->Form->input('topic', [
					'type' => 'text',
					'help' => 'e.g. Event, Cleaning, Resources, ...',
					'templates' => [
						'input' => '<div id="topic-container" style="position: relative;"><input type="{{type}}" name="{{name}}" class="form-control{{attrs.class}}" {{attrs}} /></div>',
					]
				]); ?>
				
				<br />

				<div class="row">
					<div class="col-xs-24 col-sm-12">
						
						<fieldset>
							<legend>Publication Dates</legend>
							<?= $this->Form->input('start_date', ['id' => 'start-date', 'type' => 'text']); ?>
							<?= $this->Form->input('end_date', ['id' => 'end-date', 'type' => 'text']); ?>
						</fieldset>
						
					</div>

					<div class="col-xs-24 col-sm-12">
						<fieldset>
							<legend>Send Email to Admin</legend>

							<div class="form-group switch-container">
								<?= $this->Form->input('send_email', ['type' => 'hidden', 'value' => '{{form.send_email}}']); ?>
								<switch id="send-email" name="send_email" ng-model="form.send_email"></switch>
								<label for="send-email">Send Email to Admin User?</label>
							</div>
		
							<div ng-if="form.send_email">
								<?=
								  $this->Form->input('email_user_id', [
						        'type' => 'select',
						        'label' => 'Select a User',
						        'options' => $users,
						        'empty' => '---'
						      ]);
								?>
							</div>

						</fieldset>
					</div>

				</div>

				<br />

				<div class="row">
					<div class="col-xs-24">
						<div class="form-group switch-container">
							<?= $this->Form->input('upload_form', ['type' => 'hidden', 'value' => '{{form.upload_form}}']); ?>
							<switch id="upload-form" name="upload_form" ng-model="form.upload_form"
								disabled='form.upload_disabled' ng-change="ifUpload(form.upload_form)">
							</switch>
							<label for="upload-form">Do you want to upload a pdf form?</label>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-24">
						<div class="form-group switch-container">
							<?= $this->Form->input('add_post', ['type' => 'hidden', 'value' => '{{form.add_post}}']); ?>
							<switch id="create-form" name="add_post" ng-model="form.add_post"
								disabled='form.post_disabled' ng-change="ifPost(form.add_post)" >
							</switch>
							<label for="create-form">Do you want to create a post?</label>
						</div>
					</div>
				</div>
				
			</div>
		</div>

		
	</div>
</div>

<div class="submit-big">
	<?= $this->Form->submit('Continue', ['class' => 'btn btn-info']); ?>
</div>
<?= $this->Form->end(); ?>

