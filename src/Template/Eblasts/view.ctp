<?php

$scriptblock = <<<JS

$(function () {
	
	$('input').iCheck('check');
	
});

JS;


echo $this->Html->scriptBlock($scriptblock, ['block' => true]);


?>

<div class="row">

	<div class="col-xs-16">
		<div class="ibox">
			<div class="ibox-title">
				<h3>E-Mail Message</h3>
			</div>
			<div class="ibox-content">
				<?php

				$emailObject = unserialize($email->email_object);

				if (isset($emailObject['viewVars']['htmlContent'])) {
					echo html_entity_decode($emailObject['viewVars']['htmlContent']);
				}

				?>
			</div>
		</div>
	</div>

	<div class="col-md-8">

		<div class="ibox">
			<div class="ibox-title ibox-icons">
				<span><?= $icons['user'] ?></span>
				<h3>Recipients</h3>
			</div>
			<div class="ibox-content">
				<div class="ibox-content" style="padding: 1px 0px;">
					<div class="ibox" style="margin:2px;">
						<div class="ibox-title">
							<h2>Sent To List</h2>
							<div class="ibox-tools">
								<?= $this->Html->link(
									$icons['chevron-down'],
									'javascript:;',
									[
										'escape' => false,
										'class' => 'btn btn-icon btn-info  collapse-link',
									]
								); ?>
							</div>
						</div>
						<div class="ibox-content"  style="display:block;" id="contact-list-ibox">
							<?php foreach ($recipientList['Sent To'] as $role => $users): ?>
								<?php $roleHash = sha1($role); ?>
								<div class="ibox" style="margin:2px;">
									<div class="ibox-title"  style="height:0px;">
										<div class="pull-left" style="margin:10px;">
											<label><?= $role ?></label>
										</div>
										<div class="ibox-tools">
											<?= $this->Html->link(
												$icons['chevron-down'],
												'javascript:;',
												[
													'escape' => false,
													'class' => 'btn btn-icon btn-info  collapse-link',
												]
											); ?>
										</div>
									</div>

									<?php

									$style = '';

									if (count($users) > 20) {
										$style = 'overflow: scroll;';
									}

									?>

									<div class="ibox-content <?= $roleHash ?>" style="margin:2px; max-height: 625px; <?= $style ?>">
										<?= $this->Form->input('EmailLists.email_receivers.'.$roleHash.'._ids', [
											'label' => false,
											'multiple' => 'checkbox',
											'hiddenField' => false,
											'options' => $users,
											'checked' => true,
											'class' => 'receivers-list'
										]); ?>
									</div>
								</div>
							<?php endforeach; ?>

						</div>


					</div>


				</div>



			</div>
		</div>

	</div>

</div>