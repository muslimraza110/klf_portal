<?php

$scriptBlock = <<<JS
$(function () {
	
	$('.search-filter').on('keyup change', function () {
		
		var term = $(this).val().toLowerCase();
		
		$(this).parents('.ibox').first().find('.ibox-content .checkbox label').each(function () {
			
			var _target = $(this).parents('.checkbox').first();
			
			if ($(this).text().toLowerCase().indexOf(term) > -1) {
				_target.show();
			} else {
				_target.hide();
			}
			
		});
		
	});
	
	$('.role').on('ifToggled', function () {
		
		var _target = $('.' + $(this).attr('name'));
		
		if ($(this).is(':checked')) {
			_target.iCheck('check');
		} else {
			_target.iCheck('uncheck');
		}
		
	});
	
	$('#savecontactlist').on('ifToggled', function () {
		
		var _target = $('#emaillists-title');
		
		if ($(this).is(':checked')) {
			_target.prop('disabled', false);
		} else {
			_target.prop('disabled', true).val('');
		}
		
	});
	
	$('div.checkboxes .checkbox').addClass('inline m-r-md');
	
});
JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>

<?= $this->element('ckeditor') ?>

<?= $this->Form->create() ?>

<div class="row">

	<div class="col-md-16">

		<div class="ibox">
			<div class="ibox-title ibox-icons">
				<span><?= $icons['email'] ?></span>
				<h3>E-Blast</h3>
			</div>

			<div class="ibox-content">

				<?= $this->Form->input('subject', [
					'required' => true
				]) ?>

				<?= $this->Form->input('content', [
					'type' => 'textarea',
					'required' => true,
					'class' => 'ckeditor'
				]) ?>

				<?php

				if (isset($session['origin']['name']) && $session['origin']['name'] == 'initiative') {

					 echo $this->Form->input('pin', [
						'type' => 'checkbox',
						'label' => 'Pin Email'
					]);

				}
				?>

				<?php if (!empty($session['button_link'])): ?>
					<?= $this->Form->label('Button Link') ?>

					<p>
						<?= $this->Html->link($session['button_link']); ?>
					</p>

					<?= $this->Form->input('button_label', [
						'required' => true
					]) ?>
				<?php endif; ?>
			</div>
		</div>

		<div class="submit-big">
			<?= $this->Form->input('Send', [
				'type' => 'submit',
				'label' => false,
				'class' => 'btn btn-primary',
				'style' => 'margin-left: 15px;'
			]) ?>

			<?= $this->Html->link('Cancel', $session['return_url'], [
				'label' => false,
				'class' => 'btn btn-primary'
			]) ?>
		</div>

	</div>

	<div class="col-md-8">

		<div class="ibox">
			<div class="ibox-title ibox-icons">
				<span><?= $icons['user'] ?></span>
				<h3>Recipients</h3>
			</div>
			<div class="ibox-content">
				<?= $this->element('eblast_recipients', ['customList' => $customList]); ?>

				<?php if (!empty($contactLists)): ?>
					<?php foreach ($contactLists as $contactList): ?>
						<?= $this->element('email_lists', ['emailList' => $contactList]) ?>
					<?php endforeach ?>
				<?php endif ?>
			</div>
		</div>

	</div>

</div>

<?= $this->Form->end() ?>