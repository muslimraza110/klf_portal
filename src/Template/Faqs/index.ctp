<div class="row">
	<div class="col-md-14 col-md-offset-4">
		<div class="ibox">
			<div class="ibox-title">
				<h5>FAQ</h5>
				<div class="ibox-tools">
					<?php if ($authUser->role == 'A'): ?>
						<?= $this->Html->link(
							$icons['edit'] . ' Edit FAQ List',
							['action' => 'index', true],
							['escape' => false, 'class' => 'btn btn-icon btn-primary']
						); ?>
					<?php endif; ?>
				</div>
			</div>
			<?php if (!empty($faqs)): ?>
			<div class="ibox-content" id="faqs-list">
				<?php foreach ($faqs as $faq): ?>
					<div class="ibox">
						<div class="ibox-title">
							<h2 style="text-transform: none; font-weight: bolder;"><?= $faq->title; ?></h2>
							<?= $this->Html->link(
								$icons['chevron-down'],
								'javascript:;',
								[
									'escape' => false,
									'href' => '#faq' . $faq->id,
									'class' => 'btn btn-icon btn-info  collapse-link pull-right',
								]
							); ?>
						</div>
						<div id="faq<?= $faq->id; ?>" class="ibox-content" style="padding-left:4%; display:none;">
							<?= $faq->description ?>
							<?php foreach ($faq->children as $child): ?>
							<div  class="ibox">
								<div class="ibox-title">
										<h2 style="text-transform: none; font-weight: normal;"><?= $child->title; ?></h2>
									<?= $this->Html->link(
										$icons['chevron-down'],
										'javascript:;',
										[
											'escape' => false,
											'href' => '#faq' . $child->id,
											'class' => 'btn btn-icon btn-info  collapse-link pull-right',
										]
									);  ?>
								</div>
								<div  id="faq<?= $child->id; ?>"  class="ibox-content" >
									<?=  $child->description; ?>
								</div>
							</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>
