<?php
$this->Html->css('plugins/chosen/chosen.min', ['block' => true]);

$this->Html->script('plugins/chosen/chosen.jquery.min', ['block' => true]);

$this->Html->scriptBlock(
	<<<JS

	$(function() {

		$('#faqs-ids option').prop('selected', true);

		$('#faqs-ids').chosen({
			placeholder_text_multiple: 'Enter Faqs Category',
			create_option: true,
			single_backstroke_delete: true
		});

	});

JS
	, ['block' => true]);
?>

<?= $this->Form->create($faq); ?>

<div class="row">

	<div class="col-md-12 col-md-offset-6">

		<div class="ibox">

			<div class="ibox-content" style="height: 300px;">

				<div class="row">

					<div class="col-xs-16">
						<?= $this->Form->input('title', ['label' => 'Top Category']); ?>
					</div>

					<div class="col-xs-8">

						<label>&nbsp;</label>

						<?php

						$disabled = true;

						if ($faq->isNew() || $faq->root_faq->id == $faq->id) {
							$disabled = false;
						}

						?>

					</div>

				</div>

				<?= $this->Form->input('faqs._ids', ['label' => 'FAQ Sub-Categories']); ?>

			</div>

		</div>

		<div class="submit-big">
			<?= $this->Form->submit('Save', ['class' => 'btn btn-primary']); ?>
		</div>

	</div>

</div>

<?= $this->Form->end(); ?>
