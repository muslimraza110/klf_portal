<?php
$scriptBlock = <<<JS

	$(function() {

		$(".dd").nestable();

		$('.saveList').on('click', function(e) {
			e.preventDefault();

			$.post('{$this->Url->build(['controller' => 'Faqs', 'action' => 'ajax_reorder_faq_list'])}.json', {
				serialized_faqs : $('.dd').nestable('serialize')
			}).done(function (order) {
				 location.reload();
			});

		});
	});

JS;
$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>
<?= $this->element('ckeditor'); ?>
<div class="text-right">
	<?= $this->Html->link('Save Sorting', '#', ['class' => 'btn btn-primary saveList']) ?>
</div>

<hr>

<div class="dd">

	<?= $this->element('recursive_faqs', [
		'faqs' => $faqs,
		'level' => 0
	]); ?>

</div>
