<div class="col-md-14 col-md-offset-4">
	<div class="ibox">
		<div class="ibox-title">
			<h5>FAQ</h5>
			<div class="ibox-tools">
				<?php if ($authUser->role == 'A'): ?>
					<?= $this->Html->link(
						$icons['edit'] . ' Edit FAQ List',
						['action' => 'index', true],
						['escape' => false, 'class' => 'btn btn-icon btn-primary']
					); ?>
				<?php endif; ?>
			</div>
		</div>
		<?php if (!empty($faqs)): ?>
		<div class="ibox-content" id="faqs-list">
			<?php foreach ($faqs as $faq): ?>
				<div class="ibox">
					<div class="ibox-title">
						<h2 style="text-transform: none;"><?= $faq->title; ?></h2>
					</div>
					<div class="ibox-content">
						<?php foreach ($faq->children as $child): ?>
						<?php $chevron = $this->Html->link(
							$icons['chevron-down'],
							'javascript:;',
							[
								'escape' => false,
								'href' => '#' . $child->id,
								'class' => 'btn btn-icon btn-info  collapse-link pull-right',
							]
						); ?>
						<div id="<?= $child->id; ?>" class="ibox">
							<div class="ibox-title">
								<?php if($child->isParent()): ?>
									<?= $this->Html->link(
										'<h2 style="text-transform: none;">'.$child->title.'</h2>',
										['action' => 'view', $child->id],
										[
											'escape' => false,
										]
									);?>
								<?php else: ?>
									<h2 style="text-transform: none;"><?= $child->title; ?></h2>
								<?php endif; ?>
								<?= !empty($child->description) ? $chevron : null;  ?>
							</div>
							<div  class="ibox-content" style="display:none;">
								<?=  $child->description; ?>
							</div>
						</div>
						<?php endforeach; ?>
					</div>

				</div>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>
	</div>
</div>
