<?= $this->element('ckeditor'); ?>

<?= $this->Form->create($faq); ?>

<div class="row">

	<div class="ibox">
		<div class="ibox-title ibox-icons">
			<span><?= $icons['question']; ?></span>
			<h3>FAQ</h3>
		</div>

		<div class="ibox-content">
			<?= $this->Form->input('title'); ?>
			<?= $this->Form->input('description', ['class' => 'ckeditor']); ?>
		</div>
	</div>

</div>

<div class="submit-big">
	<?= $this->Form->submit('Save', ['class' => 'btn btn-primary']); ?>
</div>

<?= $this->Form->end(); ?>
