<div class="row">
	<div class="col-md-16 col-md-offset-4">

		<div class="ibox">

			<div class="ibox-title">
				<h5>Settings</h5>
			</div>

			<div class="ibox-content">
				<div class="table-responsive">
					<table class="table table-condensed">
						<thead>
						<?php

						$headers = [
							'Name',
							'Value',
							'Pinned News',
							'Content',
							''
						];

						echo $this->Html->tableHeaders($headers);

						?>
						</thead>
						<tbody>
						<?php

						$rows = [];

						foreach ($settings as $setting) {

							$actions = [
								'edit' => $this->Html->link('Edit',
									['action' => 'edit', $setting->id],
									['class' => 'btn btn-sm btn-primary pull-right']
								)
							];

							$news = '--';

							if (!empty($pinnedPost) && $setting->id == 'FIXED_NEWS_ITEM') {
								$news = $this->Html->link($pinnedPost->name,
									['action' => 'edit', 'controller' => 'Posts', $pinnedPost->id]);
								$actions['edit'] = '<span class="tool-tip" data-toggle="tooltip" data-placement="top" title="Tooltip on top">' .
									$this->Form->button('Edit', ['disabled', 'class' => 'btn-sm pull-right']) .
									'</span>';
							}

							$rows[] = [
								$setting->name,
								$setting->value ? '<span style="color:green">On</span>' : '<span style="color:red">Off</span>',
								$news,
								$this->Text->truncate(strip_tags($setting->content), 50, [
									'ellipsis' => '...',
									'exact' => false
								]),
								implode(' ', $actions)
							];

						}

						echo $this->Html->tableCells($rows);

						?>
						</tbody>
					</table>
				</div>
			</div>

		</div>

	</div>
</div>