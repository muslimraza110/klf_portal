<?php

$scriptBlock = <<<JS

$(function () {
	
	$('#status').change(function () {
		$('#status-form').submit();
	});
	
});

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>
<?= $this->element('ckeditor'); ?>
<?= $this->Form->create($setting); ?>
<div class="row">
	<div class="col-md-16 col-md-offset-4">

		<div class="ibox">

			<div class="ibox-title">
				<h5><?= $title ?></h5>
			</div>

			<div class="ibox-content">

				<div class="row">
					<div class="col-xs-12">
						<label>Name</label>
						<p><?= $setting->name ?></p>
					</div>
					<?php switch ($setting->id):
						case 'FIXED_NEWS_ITEM': ?>
							<div class="col-xs-12">
								<?= $this->Form->input('cron_timer', [
									'append' => 'Minutes'
								]); ?>
							</div>
							<?php break; ?>
						<?php endswitch; ?>
				</div>
				<div class="row">
					<?php switch ($setting->id):
						case 'FIXED_NEWS_ITEM': ?>
							<div class="col-xs-12">
								<label>On / Off</label>
								<?= $this->Form->input('value'); ?>
							</div>
							<?php break; ?>
						<?php case 'TERMS_AND_CONDITIONS': ?>
							<div class="col-xs-24">
								<?= $this->Form->input('content', [
									'class' => 'ckeditor'
								]); ?>
							</div>
							<?php break; ?>
						<?php endswitch; ?>
				</div>
				<div class="submit-big">
					<?= $this->Form->submit('Submit', ['bootstrap-type' => 'primary']); ?>
				</div>

			</div>

		</div>

	</div>
</div>

<?= $this->Form->end(); ?>