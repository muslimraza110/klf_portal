<?php if(!empty($results)): ?>
  <table class="table" id="search-table">
    <thead>
      <?= $this->Html->tableHeaders([
        ['Type' => ['class' => 'actions']],
        'Name',
        'Related Information'
      ]); ?>
    </thead>
    <tbody>
      <?php
        foreach($results as $result) {
          switch($result->dataType) {
            case 'Users':
              echo $this->element('search/users', ['user' => $result]);
              break;
						case 'Groups':
							echo $this->element('search/groups', ['group' => $result]);
							break;
            case 'ForumPosts':
              echo $this->element('search/forum_posts', ['forumPost' => $result]);
              break;
            case 'Posts':
              echo $this->element('search/posts', ['post' => $result]);
              break;
            case 'Forms':
              echo $this->element('search/forms', ['form' => $result]);
              break;
						case 'Projects':
							echo $this->element('search/projects', ['project' => $result]);
							break;
						case 'Events':
							echo $this->element('search/events', ['event' => $result]);
							break;
          }
        }
      ?>
    </tbody>
  </table>
<?php endif; ?>
