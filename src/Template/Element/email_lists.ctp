<div class="ibox-content" style="padding: 1px 0px;">
	<div class="ibox" style="margin:2px;">
		<div class="ibox-title">
			<?php if (!empty($emailList)): ?>
				<div class="pull-left" style="margin-top:8px;">
					<?= $this->Form->input( 'lists.'.$emailList['id'], [
						'label' => false,
						'type' => 'checkbox',
						'value' => $emailList['id'],
						'hiddenField' => false,
					]); ?>
				</div>
			<?php endif; ?>
			<h2><?= $emailList['title']; ?></h2>
			<div class="ibox-tools">
				<?= $this->Html->link(
					$icons['chevron-down'],
					'javascript:;',
					[
						'escape' => false,
						'class' => 'btn btn-icon btn-info  collapse-link',
					]
				); ?>
			</div>
		</div>
			<div class="ibox-content"  style="display:none;" >
				<?php foreach ($emailList['receivers'] as $role => $users): ?>
					<div class="ibox" style="margin:2px;">
						<div class="ibox-title"  style="height:0px;">
							<h6><?= $role;  ?></h6>
							<div class="ibox-tools">
								<?= $this->Html->link(
									$icons['chevron-down'],
									'javascript:;',
									[
										'escape' => false,
										'class' => 'btn btn-icon btn-info  collapse-link',
									]
								); ?>
							</div>
						</div>
					<div class="ibox-content <?= $role; ?>" style="margin:2px;">
							<?php foreach ($users as $user): ?>
								<p><?= $user['name'];  ?></p>
							<?php endforeach; ?>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
	</div>
</div>