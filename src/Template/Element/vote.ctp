<?php

$scriptBlock = <<<JS

$(function () {
	
	$('.vote-button').click(function () {
		
		$.post(
			'{$this->Url->build(['controller' => 'UserVotes', 'action' => 'ajax_vote'])}.json',
			{
				id: {$entity->id},
				vote_id: $(this).data('id'),
				model: '$model',
				vote: $(this).data('vote')
			},
			function (response) {
				
				if (response.status == 'ok') {
					
					$('.vote-buttons').hide();
					$('.vote-response').show();
					
				}
				
			}
		);
		
	});
	
});

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>

<div class="well">

	<div class="row">

		<div class="col-md-12 col-md-offset-6 text-center">

			<h3 class="no-margins">Vote</h3>
			<br>

			<?php if ($model == 'Groups'): ?>

				<p>Should this group be accepted?</p>

			<?php else: ?>

				<p>Should this contact be accepted as a new member?</p>

			<?php endif; ?>

			<br>

			<?php if (empty($vote)): ?>

				<div class="vote-buttons">

					<?= $this->Html->link(
						$icons['thumbs-up'].'Yes',
						'javascript:;',
						['escape' => false, 'class' => 'btn btn-success vote-button', 'data-vote' => 'Y', 'data-id' => 0]
					); ?>

					<?= $this->Html->link(
						$icons['thumbs-down'].'No',
						'javascript:;',
						['escape' => false, 'class' => 'btn btn-danger vote-button', 'data-vote' => 'N', 'data-id' => 0]
					); ?>

					<?= $this->Html->link(
						$icons['times'].'Abstain',
						'javascript:;',
						['escape' => false, 'class' => 'btn btn-primary vote-button', 'data-vote' => 'A', 'data-id' => 0]
					); ?>

				</div>

				<div class="vote-response" style="display: none;">

					<p><strong>Thank you for your vote.</strong></p>

				</div>

			<?php else: ?>

				<?php

				$voteTypes = [
					'A' => 'Abstain',
					'N' => 'No',
					'Y' => 'Yes'
				];

				?>

				<p>You voted <strong><?= $voteTypes[$vote->vote]; ?></strong> on <strong><?= $vote->created; ?></strong>.</p>

				<?php if (!empty($changeVote)): ?>

					<h5 class="m-t-md">Change Vote</h5>

					<div class="vote-buttons">

						<?= $this->Html->link(
							$icons['thumbs-up'].'Yes',
							'javascript:;',
							['escape' => false, 'class' => 'btn btn-success vote-button', 'data-vote' => 'Y', 'data-id' => $vote->id]
						); ?>

						<?= $this->Html->link(
							$icons['thumbs-down'].'No',
							'javascript:;',
							['escape' => false, 'class' => 'btn btn-danger vote-button', 'data-vote' => 'N', 'data-id' => $vote->id]
						); ?>

						<?= $this->Html->link(
							$icons['times'].'Abstain',
							'javascript:;',
							['escape' => false, 'class' => 'btn btn-primary vote-button', 'data-vote' => 'A', 'data-id' => $vote->id]
						); ?>

					</div>

					<div class="vote-response" style="display: none;">

						<p><strong>Thank you for your vote.</strong></p>

					</div>

				<?php endif; ?>

			<?php endif; ?>

		</div>

	</div>

</div>