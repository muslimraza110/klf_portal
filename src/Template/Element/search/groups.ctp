<tr>
  <td class="icon">
    <?= $icons['groups'] ?>
  </td>
  <td>
    <h3>
      <?= $this->Html->link(
        $this->element('search/highlighting', ['s' => $group->name]),
        ['controller' => 'Groups', 'action' => 'view', $group->id],
        ['escape' => false, 'target' => '_blank']
      ); ?>
    </h3>
  </td>
  <td>
		
  </td>
</tr>



