<?php 
  if (!empty($terms)) {
    $excerpt = implode($terms, ' ');

    echo $this->element('search/highlighting', 
      ['s' => $this->Text->excerpt($content, $excerpt, 150, '...')]
    );   
  } else 

    echo $this->element('search/highlighting', 
      ['s' => $this->Text->truncate(strip_tags($content), 150, [
        'ellipsis' => '...',
        'exact' => false
      ])]
    ); 
?>
