<tr>
  <td class="icon">
    <?php 
      if ($post->post_type_id == 3) 
        echo $icons['graduation-cap'];
      else 
        echo $icons['list'];
    ?>
  </td>
  <td>
    <h3>
      <?php 
        $postLink = ['controller' => 'Posts', 'action' => 'view', $post->id];

        if ($post->isPdf()) {
          $postLink = ['controller' => 'Posts', 'action' => 'download', $post->id];
        }

        echo $this->Html->link(
          $this->element('search/highlighting', ['s' => html_entity_decode($post->name)]),
          $postLink,
          ['escape' => false]
        ); 
      ?>
    </h3>
  </td>

  <td>
    <?= $this->element('search/get_excerpt', ['content' => $post->content]); ?>
  </td>

</tr>