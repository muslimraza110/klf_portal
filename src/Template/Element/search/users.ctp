<tr>
	<td class="icon">
		<?= $icons['user'] ?>
	</td>
	<td>
		<h3>
			<?= $this->Html->link(
				$this->element('search/highlighting', ['s' => $user->name]),
				['controller' => 'Users', 'action' => 'view', $user->id],
				['escape' => false, 'target' => '_blank']
			); ?>
		</h3>
	</td>
	<td>
		<table>
			
			<tr>
				<th>Title: </th>
				<td>
					<?php
					if (!empty($user->user_detail->title))
						echo $this->element('search/highlighting', ['s' => $user->user_detail->title]);
					?>
				</td>
			</tr>
			
			<tr>
				<th>Primary Email: </th>
				<td>
					<?= $this->element('search/highlighting', ['s' => $user->email]); ?>
				</td>
			</tr>
			
			<tr>
				<th>Secondary Email:</th>
				<td>
					<?php
						if (!empty($user->user_detail->email2))
							$this->element('search/highlighting', ['s' => $user->user_detail->email2]);
					?>
				</td>
			</tr>
			
			<tr>
				<th>Phone:</th>
				<td>
					<ul class="list-unstyled list-inline">
						<?php
						if (!empty($user->user_detail->phone))
							echo '<li>' . $user->user_detail->phone . ' (Primary Phone)</li>';
						
						if (!empty($user->user_detail->phone2))
							echo '<li>' . $user->user_detail->phone2 . ' (Secondary Phone)</li>';
						?>
					</ul>
				</td>
			</tr>
		
		</table>
	</td>
</tr>



