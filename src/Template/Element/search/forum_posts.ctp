<tr>
  <td class="icon">
    <?= $icons['coffee'] ?>
  </td>
  <td>
    <h3>
      <?php 

        if (!empty($forumPost->parent_forum_post)) {
          if (empty($forumPost->parent_forum_post->topic)) 
            $topic = $this->element('search/highlighting', ['s' => $forumPost->parent_forum_post->content]);
          else 
            $topic = $this->element('search/highlighting', ['s' => $forumPost->parent_forum_post->topic]);
        }
        else {
          if (empty($forumPost->topic)) 
            $topic = $this->element('search/highlighting', ['s' => $forumPost->content]);
          else 
            $topic = $this->element('search/highlighting', ['s' => $forumPost->topic]);
        }
        
        if (is_null($forumPost->parent_id))
          $redirectId = $forumPost->id;
        else 
          $redirectId = $forumPost->parent_id;

        echo $this->Html->link(
          $topic,
          ['controller' => 'ForumPosts', 'action' => 'view', $redirectId],
          ['escape' => false]
        ); 
      ?>
    </h3>
  </td>
  <td>
    <?= $this->element('search/get_excerpt', ['content' => $forumPost->content]); ?>
  </td>
</tr>