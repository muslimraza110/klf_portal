<tr>
	<td class="icon">
		<?= $icons['calendar'] ?>
	</td>
	<td>
		<h3>
			<?= $this->Html->link(
				$this->element('search/highlighting', ['s' => $event->name]),
				['javascript:;'],
				['escape' => false, 'target' => '_blank']
			) ?>
		</h3>
	</td>
	<td>
		<?= $icons['calendar'] ?> <?= $event->start_date ?>
		<?php if (!empty($event->city) || !empty($event->country)): ?>
			&nbsp;&nbsp;&nbsp;<?= $icons['map-marker'] ?> <?= $event->city.' '.$event->country ?>
		<?php endif; ?>
	</td>
</tr>