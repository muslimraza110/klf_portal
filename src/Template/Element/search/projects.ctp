<tr>
	<td class="icon">
		<?= $icons['projects'] ?>
	</td>
	<td>
		<h3>
			<?= $this->Html->link(
				$this->element('search/highlighting', ['s' => $project->name]),
				['controller' => 'Projects', 'action' => 'view', $project->id],
				['escape' => false, 'target' => '_blank']
			) ?>
		</h3>
	</td>
	<td>
	</td>
</tr>