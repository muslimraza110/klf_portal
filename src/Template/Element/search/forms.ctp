<tr>
  <td class="icon">
    <?= $icons['graduation-cap']; ?>
  </td>
  <td>
    <h3>
      <?php 
        $formLink = ['controller' => 'Forms', 'action' => 'view', $form->id];

        if ($form->isPdf()) {
          $formLink = ['controller' => 'Forms', 'action' => 'download', $form->id];
        }

        echo $this->Html->link(
          $this->element('search/highlighting', ['s' => $form->name]),
          $formLink,
          ['escape' => false]
        ); 
      ?>
    </h3>
  </td>

  <td>
    <?= $this->element('search/get_excerpt', ['content' => $form->content]); ?>
  </td>

</tr>