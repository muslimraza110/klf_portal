<?php

$scriptBlock = <<<JS

$(function () {
	
	$('.vote-result-button').click(function () {
		
		$.post(
			'{$this->Url->build(['controller' => 'UserVotes', 'action' => 'ajax_final_decision'])}.json',
			{
				id: {$entity->id},
				model: '$model',
				vote: $(this).data('vote')
			},
			function (response) {
				
				if (response.status == 'ok') {
					
					$('.vote-result-buttons').hide();
					$('.vote-result-response').show();
					
				}
				
			}
		);
		
	});
	
});

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>

<div class="well">

	<div class="row">

		<div class="col-md-12 col-md-offset-6 text-center">

			<h3 class="no-margins">Vote Results</h3>

			<table class="table" style="width: auto; margin: 15px auto 0 auto;">

				<?php

				$voteTypes = [
					'A' => 'Abstain',
					'N' => 'No',
					'Y' => 'Yes'
				];

				?>

				<?php foreach ($votes as $vote): ?>

					<tr>
						<td class="text-left"><?= $vote->voter->name; ?></td>
						<td><?= $vote->created; ?></td>
						<td class="text-center"><strong><?= $voteTypes[$vote->vote]; ?></strong></td>
					</tr>

				<?php endforeach; ?>

			</table>

			<p class="m-t-md">
				<strong>Decision</strong> <?= $entity->vote_decision_label; ?>
			</p>

			<?php if (!in_array($model, ['Groups'])): ?>

				<h3>Final Decision</h3>

				<?php if (empty($entity->vote_admin_decision)): ?>

					<div class="vote-result-buttons">

						<p>
							<?= $this->Html->link(
								$icons['thumbs-up'].'Accept',
								'javascript:;',
								[
									'escape' => false,
									'class' => 'btn btn-success vote-result-button',
									'data-vote' => 'Y'
								]
							); ?>
							<?= $this->Html->link(
								$icons['thumbs-down'].'Reject',
								'javascript:;',
								[
									'escape' => false,
									'class' => 'btn btn-danger vote-result-button',
									'data-vote' => 'N'
								]
							); ?>
						</p>

					</div>

					<div class="vote-result-response" style="display: none;">

						<p><strong>Final decision has been recorded.</strong></p>

					</div>

				<?php else: ?>

					<p><strong>Final decision has been made on <?= $entity->vote_decision_date; ?>.</strong></p>

				<?php endif; ?>

			<?php endif; ?>

		</div>

	</div>

</div>