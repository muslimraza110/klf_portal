<?php foreach ($customList as $listTitle => $list) {
	echo $this->element('add_email_list', ['contactList' => $list, 'listTitle' => $listTitle]);
}
?>
<div class="pull-left" style="margin:10px;">
	<?= $this->Form->input('saveContactList', [
		'label' => 'Save Contact List',
		'type' => 'checkbox',
		'hiddenField' => false,
	]); ?>
</div>
<div class="pull-left" style="margin:10px;">
	<?= $this->Form->input('EmailLists.title', [
		'label' => false,
		'placeholder' => 'Contact List Name',
		'disabled' => true
	]); ?>
</div>
