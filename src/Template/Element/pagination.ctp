<div class="pagination-wrapper<?= (isset($noPadding)) ? ' no-padding' : '' ?>">
  <div class="counter">
    <?= $this->Paginator->counter('Page {{page}} of {{pages}}, showing {{start}}-{{end}} out of {{count}} total'); ?>
  </div>

  <?= $this->Paginator->numbers(['modulus' => 15, 'size' => 'small']);?>
</div>