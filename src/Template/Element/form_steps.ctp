<div class="form-steps">
  
  <?php if (isset($previous)): ?>
    <?= $this->Html->link($icons['angle-left'], $previous, ['class' => 'arrow-left', 'escape' => false]); ?>
  <?php else: ?>
    <span class="arrow-left disabled"><?= $icons['angle-left'] ?></span>
  <?php endif; ?>
  
  
  <ul>
    
    <?php if(isset($steps)): ?>
      
      <?php
        $count = 1;
        foreach($steps as $key => $step) {
          echo '<li>' . $this->Html->link('Step ' . $count, $step) . '</li>';
          $count++;
        }
      ?>
      
      <li><span>Step <?= $count ?></span></li>
      
    <?php else: ?>
      <li><span>Step 1</span></li>
    <?php endif; ?>
    
    
  </ul>
  
  <?php
    if (isset($next) && $next === 'SUBMIT'){
      echo $this->Form->submit('Continue', ['bootstrap-type' => 'info', 'class' => 'btn-sm btn-continue']);
    }
    elseif (isset($next)){
      echo $this->Html->link('Continue', $next, ['class' => 'btn btn-info btn-continue']);
    }
  ?>
  
</div>
