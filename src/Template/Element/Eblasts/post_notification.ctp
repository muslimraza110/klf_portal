<p>Dear {receiver_name},</p>

<p>
	<strong><?= $authUser->name ?></strong> sent you a post that you may find it interesting.
</p>

<?php if (!empty($forumPost->topic)): ?>
	<p>
		<strong><?= $forumPost->topic ?></strong>
	</p>
<?php endif ?>

<?= $forumPost->content ?>