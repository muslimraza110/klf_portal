<?php

switch ($status) {

	case 'O':
		echo '<span class="label label-warning">On Hold</span>';
		break;

	case 'P':
		echo '<span class="label label-info">Pending</span>';
		break;

	case 'R':
		echo '<span class="label label-default">Archived</span>';
		break;

	case 'D':
		echo '<span class="label label-danger">Deleted</span>';
		break;

	default:
		echo '<span class="label label-primary">Active</span>';
		break;

}