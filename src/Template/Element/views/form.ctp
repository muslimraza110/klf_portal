<?= $this->element('views/form_angular', ['form' => $form]); ?>

<?php
	if ($form->type == 'F') {
		$this->Html->scriptBlock('
			
			$("#reset-form-fields-link").click(function(e) {
				e.preventDefault();
				
				$("#Forms-' . $form->id . '").find("input, textarea, select").each(function() {
					var fieldName = $(this).attr("name");
					
					if (
						fieldName != undefined &&
						(
							fieldName.match(/forms\[' . $form->id . '\]\[user_form_values\]\[\d+\]\[value\]/) ||
							fieldName.match(/forms\[' . $form->id . '\]\[user_form_values\]\[\d+\]\[_ids\]\[\]/)
						)
					) {
						
						if ( $(this).attr("type") != undefined ) {
							
							switch($(this).attr("type")) {
								
								case "text":
									$(this).val("");
									break;
								
								case "checkbox":
								case "radio":
									$(this).iCheck("uncheck");
									break;
								
							}
						}
						else {
							//textarea or select
							$(this).val("");
							$(this).prop("defaultSelected");
						}
					}
					
				});
			});
			
		', ['block' => true]);
	}
?>

<div class="form-box" id="form-<?= $form->id ?>">

	<div class="text-center article-title">
		<span class="text-muted"><?= (!empty($form->published)) ? $form->published : 'Unpublished' ?></span>
		<br /><span class="text-muted">Post # F<?= $form->id ?></span>
		<h2><?= $form->name; ?></h2>
	</div>
	
	<?php
		if (!isset($isMain) || !$isMain) {
			
			if ($form->isPending()) {
				if ($authUser->role == 'A') {
					echo '<div class="alert alert-warning" style="overflow: hidden;">
						<i class="fa fa-exclamation-circle fa-3x fa-pull-left" aria-hidden="true"></i>
						This is only a preview.<br />This form will only be available after ';
					echo (!empty($form->start_date)) ? $form->start_date : 'the start date is set';
					echo '.</div>';
				}
				else {
					echo '<div class="alert alert-warning">This form is not available yet.</div>';
				}
			}
			
		}
	?>
	
	<?php
		
		if (empty($form->form_fields)) {
			echo '<div class="alert alert-default">This form does not have any fields...</div>';
		}
		elseif ($form->isCurrent() || $authUser->role == 'A') {
			
			echo $this->Flash->render('form-' . $form->id);
			
			if ($form->disabled && !empty($form->auth_user_data) && !isset($form->customErrors)) {
				echo '<div class="alert alert-success">You have answered this form on ' . $form->auth_user_data->created . '. Thank you for your feedback.</div>';
			}
			elseif (!empty($form->auth_user_data) && !isset($form->customErrors)) {
				echo '<div class="alert alert-success">You have answered this form on ' . $form->auth_user_data->created . '.';
				echo '<a href="#" id="reset-form-fields-link" style="font-size: inherit;">Click here if you want to empty the form for a new submission.</a></div>';
			}
			
			$class = '';
			if($form->disabled) {
				$class = 'is-disabled';
			}
			
			echo $this->Form->create(null, [
				'id' => 'Forms-' . $form->id,
				'class' => $class,
				'ng-controller' => 'Form' . $form->id . 'Controller',
				'url' => ['controller' => 'Forms', 'action' => 'save_answers', $form->id, '?' => ['return' => urlencode($this->Url->build(null, true))]]
			]);
			
			foreach ($form->form_fields as $key => $field) {
				echo $this->element('views/form_field', ['formId' => $form->id, 'key' => $key, 'field' => $field, 'isDisabled' => $form->disabled]);
				
				if (isset($this->request->data['forms'][$field->form_id]['user_form_values'][$key]['invalid_answer'])) {
					
					echo '<p class="text-danger" style="font-size: 14px; padding-bottom: 20px; margin-top: -10px;">' .
						'<strong>Answer given is not in available options anymore: </strong><br />' .
						$this->request->data['forms'][$field->form_id]['user_form_values'][$key]['invalid_answer'] .
					'</p>';
				}
			}
			
			echo '<br />';
			
			$disabledOption = [];
			if($form->disabled)
				$disabledOption = ['class' => 'disabled', 'disabled' => 'disabled'];
			
			echo $this->Form->submit('Submit', $disabledOption + ['bootstrap-type' => 'primary']);
			echo $this->Form->end();
		}
	?>
	
</div>