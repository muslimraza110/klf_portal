<div class="form-box" id="form-<?= $form->id ?>">

	<div class="text-center article-title">
		<span class="text-muted"><?= (!empty($form->published)) ? $form->published : 'Unpublished' ?></span>
		<br /><span class="text-muted">Post # F<?= $form->id ?></span>
		<h2><?= $form->name; ?></h2>
	</div>

	<?php
		if (!isset($isMain) || !$isMain) {
			
			if ($form->isPending()) {
				if ($authUser->role == 'A') {
					echo '<div class="alert alert-warning" style="overflow: hidden;">
						<i class="fa fa-exclamation-circle fa-3x fa-pull-left" aria-hidden="true"></i>
						This is only a preview.<br />This form will only be available after ';
					echo (!empty($form->start_date)) ? $form->start_date : 'the start date is set';
					echo '.</div>';
				}
				else {
					echo '<div class="alert alert-warning">This form is not available yet.</div>';
				}
			}
		}
	?>
	
	<?php
		if ($form->isCurrent() || $authUser->role == 'A') {
			echo $form->form_post->content;
		}
	?>

</div>