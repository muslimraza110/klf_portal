<?php
  if (!isset($isMain))
    $isMain = false;

  switch($post->post_theme_id) {
		
    default:
      echo $this->element('views/post_default', ['post' => $post, 'isMain' => $isMain]);
      break;
    
  }
?>