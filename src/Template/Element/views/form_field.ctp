<?php
  if ($isDisabled) {
    $disabledOption = ['disabled' => 'disabled'];
  }
  else {
    $disabledOption = [];
  }

  if (! isset($displayDropdown)) {
    $displayDropdown = true;
  }

  $fieldName = 'forms.' . $formId . '.user_form_values.' . $key . '.value';

  echo $this->Form->input('forms.' . $formId . '.user_form_values.' . $key . '.form_field_id', [
    'type' => 'hidden',
    'value' => $field->id
  ]);

  switch($field->type) {

    case 'text':

      echo $this->Form->input($fieldName, $disabledOption + [
        'type' => 'text',
        'label' => $field->name,
        'ng-errors' => "user_form_values[$key].value",
        'required' => $field->required
      ]);

      break;

		
    case 'email':

      echo $this->Form->input($fieldName, $disabledOption + [
        'type' => 'email',
        'label' => $field->name,
        'ng-errors' => "user_form_values[$key].value",
        'required' => $field->required
      ]);

      break;

		
    case 'telephone':

      echo $this->Form->input($fieldName, $disabledOption + [
        'type' => 'text',
        'label' => $field->name,
        'ng-errors' => "user_form_values[$key].value",
        'required' => $field->required
      ]);

      break;

		
    case 'textarea':

      echo $this->Form->input($fieldName, $disabledOption + [
        'type' => 'textarea',
        'label' => $field->name,
        'ng-errors' => "user_form_values[$key].value",
        'required' => $field->required
      ]);

      break;

		
    case 'checkbox':
      echo '<div class="form-group single-checkbox">';
      echo $this->Form->input($fieldName, $disabledOption + [
        'type' => 'checkbox',
        'label' => $field->name,
        'value' => 'Yes',
        'hiddenField' => 'No',
        'required' => $field->required
      ]);
      echo '<div ng-errors="user_form_values[' . $key . '].value"></div>';
      echo '</div>';

      break;

		
    case 'select':
    case 'dropdown':
      if ($displayDropdown) {
        echo $this->Form->input($fieldName, $disabledOption + [
          'type' => 'select',
          'label' => $field->name,
          'options' => $field->option_array_with_keys,
          'empty' => '---',
          'ng-errors' => "user_form_values[$key].value",
          'required' => $field->required
        ]);
      }
      else {

        $this->Form->templates([
          'radioContainer' => '{{h_radioContainer_start}}<div class="form-group {{required}}">{{content}}' .
            '<p ng-errors="user_form_values[' . $key . '].value"></p>' .
            '</div>{{h_radioContainer_end}}',
        ]);

        echo $this->Form->input($fieldName, $disabledOption + [
          'type' => 'radio',
          'label' => $field->name,
          'options' => $field->option_array_with_keys,
          'required' => $field->required
        ]);
      }
			
      break;

		
    case 'radio':
      $this->Form->templates([
        'radioContainer' => '{{h_radioContainer_start}}<div class="form-group {{required}}">{{content}}' .
          '<p ng-errors="user_form_values[' . $key . '].value"></p>' .
          '</div>{{h_radioContainer_end}}',
      ]);

      echo $this->Form->input($fieldName, $disabledOption + [
        'type' => 'radio',
        'label' => $field->name,
        'options' => $field->option_array_with_keys,
        'required' => $field->required
      ]);

      break;

		
    case 'checkboxes':
      $this->Form->templates([
        'inputContainer' => '<div class="form-group {{type}}{{required}}">{{content}}' .
          '<div ng-errors="user_form_values[' . $key . '].value"></div>' .
          '</div>'
      ]);

      echo $this->Form->input('forms.' . $formId . '.user_form_values.' . $key . '._ids', $disabledOption + [
        'multiple' => 'checkbox',
        'label' => $field->name,
        'options' => $field->option_array_with_keys,
        'required' => $field->required
      ]);

      break;
		
		
		case 'text_block':
			echo '<div style="margin-bottom: 20px;">' . $field->name . '</div>';
			break;

  }
