<?php
  $errors = (isset($form->customErrors)) ? safe_json_encode($form->customErrors) : 0;
  echo $this->Html->scriptBlock('
	
		KhojaApp.controller("Form' . $form->id . 'Controller", ["$scope", function($scope) {
			
			$scope.errors = ' . $errors . ';
			
		}]);
	
		KhojaApp.directive("ngErrors", ["$compile", "$timeout", function($compile, $timeout) {
			return {
				restrict: "A",
				link: function(scope,element,attrs){
	
					if (!isEmpty(attrs.ngErrors)) {
						var data = attrs.ngErrors;
						
						if(scope.errors != undefined) {
							for(var i in scope.errors.user_form_values) {
		
								if (angular.equals("user_form_values[" + i + "].value", data)) {
									//Create a placeholder for error(s)
									var template = "<span ng-repeat=\"(type, error) in errors." + data + "\" class=\"help-block error-message\">{{error}}</span>";
		
									//Compile the template and append it to the parent of the form-control element
									for(var i in scope.errors) {
										console.log(scope.errors[i]);
									}
									element.parent().addClass("has-error");
									element.parent().append($compile(template)(scope));
								}
							}
						}
					}
				}
			}
		}]);
		
  ', ['block' => true]);
