<?php
	$class = 'post-box';
	
	switch ($post->post_theme_id) {
		case 2:
			$class .= ' blue-bg';
			break;
	}
?>

<div class="<?= $class ?>">
	<?php
			if ($post->isCurrent() || $authUser->role == 'A') {
				if (!empty($post->featured_img)) {
					echo $this->Html->image($post->getFeaturedImage().'?'.microtime(true), ['class' => 'm-b-lg', 'style' => 'width:100%;']);
				}
			}	

	 ?>
	<div class="text-center article-title">
		<span class="text-muted"><?= (!empty($post->published)) ? $post->published : 'Unpublished' ?></span>
		<br /><span class="text-muted">Post # P<?= $post->id ?></span>
		<h2><?= $post->name; ?></h2>
	</div>
	
	<?php
		if ((!isset($isMain) || !$isMain)) {
			
			if ($post->isPending()) {
				if ($authUser->role == 'A') {
					echo '<div class="alert alert-warning" style="overflow: hidden;">
						<i class="fa fa-exclamation-circle fa-3x fa-pull-left" aria-hidden="true"></i>
						This is only a preview.<br />This post will only be available after ';
					echo (!empty($post->published)) ? $post->published : 'the start date is set';
					echo '.</div>';
				}
				else {
					echo '<div class="alert alert-warning">This post is not available yet.</div>';
				}
			}
			
		}
	?>
	
	<?php
		if ($post->isCurrent() || $authUser->role == 'A') {
			echo $post->content;
		}
	?>
	



</div>
<div class="col-lg-20 col-lg-offset-2 news-margin ">
<?php
	if (!empty($previous)) {
		echo $this->Html->link($icons['arrow-left'].' '.$previous->name, ['action' => 'view', $previous->id], [
			'escape' => false,
			'class' => 'col-xs-12  text-left',
			'style' => 'padding-left:0px;'
		]);
	}
	if (!empty($next)) {
		echo $this->Html->link($next->name.' '.$icons['arrow-right'], ['action' => 'view', $next->id], [
			'class' => 'pull-right text-right',
			'escape' => false,
		]);
	}
?>

</div>
