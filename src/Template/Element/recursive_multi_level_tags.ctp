<ol class="dd-list">
<?php

foreach ($tags as $tag) :

	$actions = [
		'edit' => $this->Html->link(
			$icons['edit'] . 'Edit',
			['action' => 'multi_level_tag_edit', $tag->id],
			['escape' => false, 'class' => 'btn btn-primary btn-xs']
		),
		'delete' => $this->Form->postLink(
			$icons['delete'] . 'Delete',
			['action' => 'multi_level_tag_delete', $tag->id],
			['escape' => false, 'class' => 'btn btn-danger btn-xs', 'confirm' => 'Are you sure you want to delete the tag "'.$tag->name .'"?']
		)
	];

?>

	<li class="dd-item dd3-item" data-id="<?= $tag->id ?>">

		<div class="dd-handle dd3-handle" style="height:auto">&nbsp;</div>
		<div class="dd3-content">
			<span><?= $tag->name ?></span>
			<span class="pull-right"><?= implode(' ', $actions) ?></span>
		</div>

		<?php

		if (!empty($tag->children)) {
			echo $this->element('recursive_multi_level_tags', ['tags' => $tag->children, 'level' => $level + 1]);
		}

		?>

	</li>

<?php endforeach; ?>

</ol>
