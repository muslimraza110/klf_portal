<?php

echo $this->Html->script('ckeditor/ckeditor', ['block' => true]);

$options = '';

if (isset($customConfig)) {
	$options = "customConfig: '/js/ckeditor/" . $customConfig . ".js'";
} else {
	$options = "customConfig: '/js/ckeditor/config.js'";
}

echo $this->Html->scriptBlock(
<<<JS

	var PREFIX = '{$authUser->prefix}';
	var resizeTimer;
	
	$(function() {
		
		$('.ckeditor').load(function () {
		
			CKEDITOR.replace('.ckeditor', {
				$options
			});
			
		});
		
		$(window).on('resize', function (e) {
		
			clearTimeout(resizeTimer);
			
			resizeTimer = setTimeout(function () {
		
				var triggerIfTitleIs = 'Collapse Toolbar';
				var h = '500px';
				
				if ($(window).width() >= 768) {
					triggerIfTitleIs = 'Expand Toolbar';
					h = '300px';
				}
				
				CKEDITOR.instances.content.resize('100%', h);
				
				$('.cke_toolbox_collapser').each(function () {
					
					if ($(this).attr('title') == triggerIfTitleIs) {
						CKEDITOR.tools.callFunction(101);
						
						var selector = '#' + $(this).attr('id').substr(0, 5) + '_contents';
						$(selector).height(h);
					}
					
				});
				
			}, 250);

		});
		
	});
	
JS
, ['block' => true]);
