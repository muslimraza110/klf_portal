<ol class="dd-list">
<?php

foreach ($faqs as $faq) :


	$actions = [
		'edit' => $this->Html->link(
			$icons['edit'] . 'Edit',
			['action' => 'edit', $faq->id],
			['escape' => false, 'class' => 'btn btn-warning btn-xs']
		),
		'delete' => $this->Form->postLink(
			$icons['delete'] . 'Delete',
			['action' => 'delete', $faq->id],
			['escape' => false, 'class' => 'btn btn-danger btn-xs', 'confirm' => 'Are you sure you want to delete the faq "'.$faq->title .'"?']
		),
		'add_faq' => $this->Html->link(
			$icons['plus'] . 'Add FAQ' ,
			['action' => 'add', $faq->id],
			['escape' => false, 'class' => 'btn btn-primary btn-xs']
		)
	];


?>

	<li class="dd-item dd3-item" data-id="<?= $faq->id ?>">

		<div class="dd-handle dd3-handle" style="height:auto">&nbsp;</div>
		<div class="dd3-content">
			<span><?= $faq->title ?></span>
			<span class="pull-right"><?= implode(' ', $actions) ?></span>
		</div>

		<?php

		if (!empty($faq->children)) {
			echo $this->element('recursive_faqs', ['faqs' => $faq->children, 'level' => $level + 1]);
		}

		?>

	</li>

<?php endforeach; ?>

</ol>
