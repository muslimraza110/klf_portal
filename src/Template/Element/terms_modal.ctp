<?php

$js = <<<EOD

	$('#terms-modal').modal({
		keyboard: false,
		show: true,
		backdrop: 'static'
	});
	
	$('#terms').on('ifChecked', function (e) {
		$('#accept').attr('disabled', false);
		$('#accept').show();
	});

	$('#terms').on('ifUnchecked', function (e) {
		$('#accept').attr('disabled', true);
		$('#accept').hide();
	});

EOD;

echo $this->Html->scriptBlock($js, ['block' => true]);


?>

<?= $this->Form->create($authUser, ['id' => 'terms-modal-form']); ?>

<div class="modal fade" id="terms-modal" tabindex="-1" role="dialog" aria-labelledby="terms-modal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h5 class="modal-title" id="exampleModalLabel">Terms and Conditions</h5>
			</div>
			<div class="modal-body">
				<?= $termsSetting->content ?>
				<?= $this->Form->input('terms', [
					'label' => 'I Accept all the Terms and Conditions'
				]); ?>
			</div>
			<div class="modal-footer">

				<?php

				echo $this->Html->link(
					'Decline',
					['action' => 'acceptTerms', 'controller' => 'Users'],
					[
						'class' => 'btn btn-sm m-l-sm',
						'style' => 'background-color: #c2c2c2; border-color: #b5b5b5; color: white',
					]
				);

				echo $this->Html->link(
					'Accept',
					['action' => 'acceptTerms', 'controller' => 'Users', true],
					[
						'class' => 'btn btn-sm m-l-sm',
						'style' => 'background-color: #344D89; border-color: #2d4377; color: white; display: none',
						'disabled',
						'id' => 'accept'
					]
				);

				?>

			</div>
		</div>
	</div>
</div>
<?php $this->Form->end(); ?>