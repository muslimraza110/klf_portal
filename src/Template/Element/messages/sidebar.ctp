<?=
  $this->Html->scriptBlock("

    /*
    $('.folder-list li').each(function(){
      $(this).removeClass('active');
      var currentType = $(this).find('a').text().replace(/\d+/g, '').toLowerCase().trim();

      if (currentType == type) {
        $(this).addClass('active');
      }
    });
    */

  ", ['block' => true]);
?>

<div class="ibox-content mailbox-content m-b-lg">
  <div class="file-manager">
    
    <div class="space-25"></div>

    <?= $this->Html->link('COMPOSE', 
    	['action' => 'add'], 
    	['class' => 'btn btn-block btn-icon compose-mail font-bold']); 
    ?>
    
    <div class="space-25"></div>

    <ul class="folder-list list-unstyled m-b-md">
      <li>
      	<?= 
          $this->Html->link($icons['envelope'] . '&nbsp;Inbox' . '<span class="label label-danger pull-right">12</span>', 
            ['action' => 'index'], 
            ['escape' => false]
          ); 
        ?>
      </li>

      <li>
        <?= 
          $this->Html->link($icons['archive'] . '&nbsp;Archive' . '<span class="label label-danger pull-right">12</span>', 
            ['action' => 'index', 'archive'], 
            ['escape' => false]
          ); 
        ?>
      </li>

      <li>
        <?= 
          $this->Html->link($icons['exclamation-circle'] . '&nbsp;Important', 
            ['action' => 'index', 'important'], 
            ['escape' => false]
          ); 
        ?>
      </li>

      <li>
        <?= 
          $this->Html->link($icons['send-o'] . '&nbsp;Sent', 
            ['action' => 'index', 'sent'], 
            ['escape' => false]
          ); 
        ?>
      </li>

      <li>
        <?= 
          $this->Html->link($icons['inbox'] . '&nbsp;Drafts', 
            ['action' => 'index', 'draft'], 
            ['escape' => false]
          ); 
        ?>
      </li>

      <li>
        <?= 
          $this->Html->link($icons['trash'] . '&nbsp;Trash', 
            ['action' => 'index', 'trash'], 
            ['escape' => false]
          ); 
        ?>
      </li>


    </ul>
  </div>
</div>
