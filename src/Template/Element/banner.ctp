<?php

switch ($this->request->controller) {
	
	case 'Categories':
		$titleIcon = $icons['folder'];
		break;
		
	case 'ForumPosts':
		$titleIcon = $icons['commenting-o'];
		
		switch ($this->request->action) {
			
			case 'view_subtopic':
				
				if ($subforum->model != 'admin' || $authUser->role == 'A') {
					$bannerActions = [$icons['add'] => ['action' => 'add_subtopic_post', $subforum->id]];
				}
				
				break;
		}
		
		break;
		
	case 'Forms':
		$titleIcon = $icons['bar-chart'];
		break;
		
	case 'Posts':
		$titleIcon = $icons['file'];
		break;
		
	case 'Users':
		$titleIcon = $icons['users'];
		break;
		
	case 'Groups':
		$titleIcon = $icons['groups'];
		break;
		
	case 'Faqs':
		$titleIcon = $icons['question-circle'];
		break;
	
	case 'Pages':
		
		switch ($this->request->action) {
			case 'mission':
				$titleIcon = $icons['bullseye'];
				break;
		}
		
		break;
		
}

if (!isset($bannerActions)) {
	
	switch ($this->request->action) {
		
		case "index":
			
			switch ($this->request->controller) {
				
				case 'ForumPosts':
				case 'Settings':
					
					$noAdd = true;
					
					 //if ($scope == 'admin') {
					 //	if ($authUser['role'] == 'A') {
					 //		$bannerActions = [$icons['add'] => ['action' => 'add', 'admin']];
					 //	}
					 //}/* else {
					 //	$bannerActions = [$icons['add'] => ['action' => 'add', $scope]];
					 //}*/
			
					break;
				
				case 'Posts':
					
					if (empty($noAdd)) {
						$bannerActions = [$icons['add'] => ['action' => 'add', $categoryId]];
					}
					
					break;
				
				case 'Faqs':
					
					if ($authUser['role'] == 'A') {
						$bannerActions = [$icons['add'] => ['action' => 'add']];
					}
					
					break;
					
				case 'Initiatives':
					
					if (in_array($authUser->role, ['A'])) {
						$bannerActions = [$icons['add'] => ['action' => 'add']];
					}
					
					break;

				case 'Eblasts':

					if (in_array($authUser->role, ['A'])) {
						$bannerActions = [$icons['envelope'] => ['action' => 'eblast']];
					}

				break;
					
				default:
					
					$bannerActions = [$icons['add'] => ['action' => 'add']];
					
					if (!empty($type)) {
						$bannerActions = [$icons['add'] => ['action' => 'add', '?' => ['type' => $type]]];
					}
					
					break;
			}
			
			break;
		
		case 'profile':
			$bannerActions = [$icons['edit'] => ['action' => 'edit_profile']];
			break;
		
		case 'edit_profile':
			$bannerActions = [$icons['view'] => ['action' => 'profile']];
			break;
		
		case "view":
			
			switch ($this->request->controller) {
				
				case 'Users':
				case 'Groups':
					$hideTitle = true;
					break;
					
				case 'Projects':
					$groupName = true;
					$bannerActions = [$icons['back'] => ['action' => 'index']];
					break;
					
				default:
					$bannerActions = [$icons['back'] => ['action' => 'index']];
					break;
			}
			
			break;
		
		case "trash":
			
			$bannerActions = $this->Form->postLink(
				$icons['trash'],
				['controller' => $this->request->controller, 'action' => 'trash'],
				[
				'escape' => false,
					'class' => 'btn btn-info btn-icon pull-right',
					'title' => 'Empty&nbsp;Trash',
					'confirm' => sprintf(
						'Do you really want to empty the trash? All the deleted %s will be permanently lost.',
						strtolower($this->request->controller)
					)
			]);
			
			break;
		
		case 'multi_level_tag':
			$bannerActions = [$icons['add'] => ['action' => 'multi_level_tag_add']];
			break;
		
	}
	
}

if (!empty($bannerActions) && is_array($bannerActions)) {
	
	$a = '';
	
	foreach ($bannerActions as $actionTitle => $actionRoute) {
		
		$a .= $this->Html->link(
			$actionTitle,
			$actionRoute,
			['escape' => false, 'class' => 'btn btn-info btn-icon']
		);
		
	}
	
	$bannerActions = $a;
	
}

?>

<?php if (empty($hideTitle)): ?>

	<div class="page-heading">
		
		<h2><?= (!empty($titleIcon)) ? $titleIcon : '' ?><?= h($title) ?></h2>
		
		<?php if (!empty($groupName)): ?>
		
			<div class="inline text-center" style="width:50%">
				<h3 style="font-size: 30px;"><?= $project->projectCharities(); ?></h3>
			</div>
		
		<?php elseif ($this->request->action == 'khoja_cares'): ?>
		
			<div class="inline text-center" style="width:50%">
				
				<?= $this->Html->link(
					$this->Html->image('khoja-cares-logo.png', [
						'alt' => 'Khoja Cares',
						'id' => 'second-logo',
						'style' => 'max-width: 300px'
					]),
					['controller' => 'Projects', 'action' => 'khoja_cares'],
					['escape' => false]
				); ?>
				
			</div>
		
		<?php endif; ?>
		
		<?php if (!empty($bannerActions)): ?>
			<div class="pull-right"><?= $bannerActions ?></div>
		<?php endif; ?>
		
		<ol class="breadcrumb m-t-sm">
			
			<li><?= $this->Html->link($icons['home'], $authUser['home_route'], ['escape' => false]) ?></li>
			
			<?php if (!empty($breadcrumbs)): foreach ($breadcrumbs as $breadcrumb): if (!empty($breadcrumb['route'])): ?>
				<li><?= $this->Html->link($breadcrumb['name'], $breadcrumb['route']); ?></li>
			<?php endif; endforeach; endif; ?>
			
			<li class="active"><?= h($title) ?></li>
			
		</ol>
	
	</div>

<?php endif; ?>