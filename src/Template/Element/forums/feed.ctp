<?php foreach ($forumPosts as $forumPost): ?>
	<?php if (! empty($forumPost->user)): ?>
	
		<?php
			$profileLink = '#';
			if (!empty($forumPost->user->contact))
				$profileLink = ['controller' => 'Contacts', 'action' => 'profile', $forumPost->user->contact->id];
		?>
		
	  <div class="social-feed-box" id="social-feed-box-<?= $forumPost->id; ?>">

	    <div class="social-avatar">
		    <?php
		    	echo $this->Html->link($this->Html->image($forumPost->user->profile_thumb, ['class' => 'avatar img-circle']), $profileLink, [
						'escape' => false,
						'class' => 'pull-left'
					]);
				?>

		    <div class="media-body">
		      <?= $this->Html->link($forumPost->user->name, $profileLink, ['class' => 'profile-link']); ?>
		      <small class="text-muted"><?= $forumPost->created->i18nFormat('EEEE LLLL d, yyyy @ h:mm a'); ?></small>
		    </div>
			</div>

			<div class="social-body">
				<p><?= $forumPost->topic; ?></p>
		    <p><?= strip_tags($forumPost->content, '<ul><ol><li><strong><a>'); ?></p>
		  </div>

		  <div class="btn-group">
		  	<?php if ($authUser->role != 'R'): ?>
			    <button forum-post-id="<?= $forumPost->id; ?>" class="btn btn-info btn-xs comment-btn">
			    	Comment
			    </button>

			    <?= $this->element('forums/luv-it-button', ['data' => $forumPost]); ?>
			  <?php endif; ?>
		  </div>

		  <small><?= empty($forumPost->latest_comment_time) ? 'No comments yet.' : $forumPost->latest_comment_time; ?></small>

		  <ul class="list-inline post-summary">
		  	<li>
		  		<i class="fa fa-comment-o"></i>
		  		<span class="total-comments"><?= $forumPost->count_comments; ?></span>
		  		</li>
		  	<li>
		  		<i class="fa fa-thumbs-up"></i>
		  		<span class="total-likes"><?= $forumPost->count_likes; ?></span>
		  	</li>
				<li>
					<i class="fa fa-thumbs-down"></i>
					<span class="total-dislikes"><?= $forumPost->count_dislikes ?></span>
				</li>
		  </ul>

		  <div class="forum-comments"></div>

		  <div class="social-footer">
			  <?php if (! $forumPost->posts_children->isEmpty()): ?>
			  
			    <div class="well">
				    	<ul class="list-unstyled" style="padding-left: 0;">
								<?php if (
									!in_array($forumPost->visibility, ['A', 'S'])
									|| ($forumPost->visibility == 'A' && $authUser->role == 'A')
									|| ($forumPost->visibility == 'S' && in_array($authUser->role, ['A', 'O']))
								): ?>
				    			<?php foreach ($forumPost->posts_children as $forumComment): ?>

										<li class="social-comment">

											<?php
												$commentUserProfile = '#';

												if (!empty($forumComment->user->contact)) {
													$commentUserProfile = ['controller' => 'Contacts', 'action' => 'profile', $forumComment->user->contact->id];
												}

												echo $this->Html->link($this->Html->image($forumComment->user->profile_thumb, ['class' => 'avatar img-circle']),
													$commentUserProfile,
													['escape' => false, 'class' => 'pull-left']
												);
											?>

											<div class="media-body">

												<?= $this->Html->link($forumComment->user->name, $commentUserProfile); ?>

												<div><?= strip_tags($forumComment->content); ?></div>

												<div class="btn-group">
													<?php
														if ($authUser->role != 'R') {
															echo$this->element('forums/luv-it-button', ['data' => $forumComment]);
														}
													?>
													<span class="total-likes-span">
													<?= $icons['thumbs-up']?> <span class="total-likes"><?= $forumComment->count_likes; ?></span>
													<?= $icons['thumbs-down'] ?> <span class="total-dislikes"><?= $forumComment->count_dislikes ?></span>
													</span>
												</div>

											</div>

										</li>

									<?php endforeach; ?>
								<?php endif ?>
							</ul>
			    	<?php
			    		if ($forumPost->count_comments > 2) {
			    			echo $this->Html->link('View more comments', '#', ['class' => 'view-more-comments m-r-lg']);
			    			echo $this->Html->link('View less comments', '#', ['class' => 'view-less-comments']);
			    		}
			    	?>
			    </div>

			  <?php endif; ?>
		  </div>
			
	  </div>
	<?php endif; ?>
<?php endforeach; ?>