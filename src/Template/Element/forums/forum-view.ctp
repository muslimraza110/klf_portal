<?= $this->Html->scriptBlock("
	
	$('.grouped-buttons').off('click', '.like-btn').on('click', '.like-btn',function(){
		var button = $(this);
		var url = '" . $this->Url->build(['controller' => 'ForumLikes', 'action' => 'ajax_like', false]) . "';

		forumLikes(button, url, $('#luv-btn-group-' + button.attr('forum-post-id')));
	});
	
	$('.grouped-buttons').off('click', '.dislike-btn').on('click', '.dislike-btn', function () {
		var button = $(this);
		var url = '".$this->Url->build(['controller' => 'ForumLikes', 'action' => 'ajax_like', true])."';

		forumLikes(button, url, $('#luv-btn-group-' + button.attr('forum-post-id')));
	});
	
", ['block' => true]); ?>

<?php if (!empty($data->user)): ?>
	<div class="media<?= !empty($data->pending) ? ' media-pending' : ''; ?>" id="forum-post-<?= $data->id ?>">
		<div class="row">
			<div class="col-sm-6 col-md-4 text-center">
				
				<?php
				$profileLink = '#';
				
				if (!empty($data->user)) {
					$profileLink = ['controller' => 'Users', 'action' => 'view', $data->user->id];
				}
				
				echo $this->Html->link(
					$this->Html->image($data->user->profile_thumb, ['class' => 'profile-thumb']) . '<br /><h4>' . $data->user->name . '</h4>',
					$profileLink,
					['escape' => false, 'class' => 'avatar']
				);
				?>
				
				<br />
				
				<div class="author-info">
					<strong>Posts:</strong> <?= $data->userTotalPosts($data->user_id); ?>
					<br>
					<strong>Last Login:</strong> <?= !empty($data->user->last_login) ? $data->user->last_login->i18nFormat('LLLL d, yyyy') : '--'; ?>
					<br>
				</div>
			</div>
			<div class=" col-sm-18 col-md-19">

				<div class="media-body">
					<p>
						<small class="text-muted">Posted: <?= $data->created->i18nFormat('EEEE LLLL d, yyyy @ h:mm a'); ?></small>
					</p>
					<div class="content">
						<p class="quote-header">
							<strong><?= $data->user->name; ?></strong> wrote on <?= $data->created->i18nFormat('EEEE LLLL d, yyyy @ h:mm a'); ?>:
						</p>
						<?= $data->content; ?>
					</div>
				</div>

			</div>

		</div>

		<div class="pull-right grouped-buttons" style="bottom: 0">

			<?php if ($data->pending): ?>

				<?= $this->Html->link($icons['thumbs-up'] . 'Approve',
					['action' => 'approve', $data->id],
					['class' => 'btn btn-info btn-xs', 'escape' => false]
				); ?>

			<?php endif; ?>

			<?php if ($authUser->id == $data->user_id || in_array($authUser->role, ['A', 'O']) || in_array($authUser->id, $moderatorIds)): ?>

				<?php if ($authUser->role == 'A'): ?>
					<?= $this->Html->link($icons['envelope'] . ' E-Blast', ['action' => 'eblast', $data->id], [
						'class' => 'btn btn-info btn-xs', 'escape' => false,
					]); ?>

					<?php if (!empty($data->eblast)): ?>
						<div style="float: left;"><?= $icons['share'] ?>
							<small>Sent
								<strong><?= $data->eblast ?></strong>
							</small>
							|&nbsp;
						</div>
					<?php endif ?>
				<?php endif; ?>
			
				<?= $this->Html->link($icons['edit'] . 'Edit',
					['action' => 'edit', $data->id],
					['class' => 'btn btn-info btn-xs', 'escape' => false]
				); ?>

				<?= $this->Form->postLink(
					$icons['delete'] . 'Delete',
					['controller' => 'ForumPosts', 'action' => 'delete', $data->id],
					[
						'escape' => false,
						'class' => 'btn btn-info btn-xs',
						'confirm' => 'Do you really want to move the forum post to the trash?'
					]
				); ?>

			<?php endif; ?>

			<?php if (!($parentForumPost->visibility == 'A' && $authUser->role == 'C')) : ?>

				<?= $this->Html->link($icons['quote-right'] . 'Quote',
					'#dialog',
					['class' => 'btn btn-info btn-xs quote-post', 'escape' => false]
				); ?>

			<?php endif; ?>

			<div id="luv-btn-group-<?= $data->id ?>" style="padding-top: 10px">

				<?= $this->element('forums/luv-it-button', ['data' => $data]); ?>

				<?= $this->Html->link(
					sprintf(
						'%s <span class="total-likes">%s</span> | %s <span class="total-dislikes">%s</span>',
						$icons['thumbs-up'],
						$data->count_likes,
						$icons['thumbs-down'],
						$data->count_dislikes
					),
					['controller' => 'ForumLikes', 'action' => 'view', $data->id],
					['escape' => false, 'class' => 'total-likes-span']
				); ?>

			</div>

		</div>

	</div>
<?php endif; ?>