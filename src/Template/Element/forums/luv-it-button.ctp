<?php
	if ($data->likedBefore($authUser->id))
		$text = 'You Liked This';
	else
		$text = 'Like';

if ($data->likedBefore($authUser->id, true)) {
	$text2 = 'You Disliked This';
} else {
	$text2 = 'Dislike';
}
?>

<span class="text-nowrap">
	
	<button forum-post-id="<?= $data->id; ?>" user-id="<?= $authUser->id; ?>" class="btn btn-info btn-xs like-btn">
		<?= $text; ?>
	</button>
	
	<button forum-post-id="<?= $data->id ?>" user-id="<?= $authUser->id ?>" class="btn btn-info btn-xs dislike-btn">
		<?= $text2 ?>
	</button>

</span>