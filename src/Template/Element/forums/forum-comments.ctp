<?php
echo $this->Form->create(null, ['id' => 'form-' . $postId]);
echo $this->Form->input('parent_id', ['type' => 'hidden', 'value' => $postId]);
echo $this->Form->input('content', [
	'label' => false,
	'placeholder' => 'Write a comment...',
	'id' => false,
	'required' => 'required'
]);
echo $this->Form->end();