<?= $this->element('forums/luv-it-button') ?>

<?= $this->Html->link(
	sprintf(
		'%s <span class="total-likes">%s</span> | %s <span class="total-dislikes">%s</span>',
		$icons['thumbs-up'],
		$data->count_likes,
		$icons['thumbs-down'],
		$data->count_dislikes
	),
	['controller' => 'ForumLikes', 'action' => 'view', $data->id],
	['escape' => false]
); ?>