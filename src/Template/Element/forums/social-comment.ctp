<div class="social-comment">
	<?= $this->Html->link($this->Html->image($forumComment->user->profile_thumb, ['class' => 'avatar']), $commentUserProfile, [
		'escape' => false,
		'class' => 'pull-left'
	]); ?>
	
	<div class="media-body">
		<?= $this->Html->link($forumComment->user->name, $commentUserProfile); ?>
		<?= $forumComment->content; ?>
		
		<p>
			<a class="small" href="#">
				<i class="fa fa-thumbs-up"></i> 26 Like this!
			</a> -
			<small class="text-muted">12.06.2014</small>
		</p>
		
		<ul class="list-inline">
			<li><?= $this->Html->link('Like', '#'); ?></li>
			<li><?= $this->Html->link('Reply', '#'); ?></li>
		</ul>
	
	</div>
</div>