<?php foreach ($folders as $folder): ?>
	<?php if (!$folder->is_protected || $user->role == 'A'): ?>
	<li class="list-item">
		<span>
			<?= $icons['folder']; ?>
			<?= $this->Html->link($folder->name, 'javascript:;', ['class' => 'list-item-name', 'onclick' => 'loadImageList(this, '.$folder->id.')']); ?>
		</span>
		<div class="folder-images" id="folder-image-list-<?= $folder->id; ?>"></div>

		<?php
			if (!empty($folder->children)) {
				printf(
					'<ol class="folder-image-list">%s</ol>',
					$this->element('recursive_upload_folders', ['folders' => $folder->children])
				);
			}
		?>

	</li>
	<?php endif; ?>
<?php endforeach; ?>
