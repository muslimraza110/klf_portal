<?php

$this->Html->scriptBlock(<<<JS

var confirmPledge = function () {
	
	return window.confirm('Are you sure you want to submit this pledge?');
	
};

$(function () {

	$('#match').on('click', function () {
		
		$('#amount').val($(this).data('match')).focus();
		
	});

});

JS
	, ['block' => true]);

?>

<div class="ibox" id="fundraising-<?= $fundraising->id ?>">
	<div class="ibox-title">
		<h3><?= $fundraising->name ?></h3>
		<div class="ibox-tools">

			<?php if ($authUser->role == 'A') {
				echo $this->Html->link('EDIT PLEDGES',
					['controller' => 'Initiatives', 'action' => 'pledges', $fundraising->id],
					['class' => 'btn btn-md btn-primary']);
			} ?>
		</div>
	</div>
	<div class="ibox-content">

			<h3>
			<?php
			if (isset($fundraising->projects)) {
				foreach($fundraising->projects as $project) {
					echo '&bull; ' . $project->name . '</br>';
				}
			}
			?>
			</h3>


		<h3 class="text-center">

			<?php if ($fundraising->type == 'M') : ?>

				<p style="font-size: 1em">Match amount pledged by:</p>
		</h3>
				<table class="table table-striped">
					<thead>
					<?php

						$headers = [
							'Matcher',
							'match' => 'Match Full Amount?',
							'Amount'
						];

						if ($authUser->role != 'A') {
							unset($headers['match']);
						}

						echo $this->Html->tableHeaders($headers);

					?>
					</thead>
					<tbody>

					<?php

					$rows = [];

					foreach ($fundraising->fundraising_users as $user) {

						$name = $user->user->fullName;
						$amount = $this->Number->format($user->amount, ['places' => 0, 'before' => 'USD ']);

						if ($authUser->role == 'A') {

							if ($user->anonymous) {
								$name .= '<span style="color:red">*</span>';
							}

							if ($user->hide_amount) {
								$amount .= '<span style="color:red">*</span>';
							}

						} else {

							if ($user->anonymous) {
								$name = h('<Anonymous>');
							}

							if ($user->hide_amount) {
								$amount = h('<Hidden>');
							}

						}

						$row = [
							$name,
							'match' => $user->match_full_amount ? 'Yes' : 'No',
							$amount
						];

						if ($authUser->role != 'A') {
							unset($row['match']);
						}

						$rows[] = $row;

					}

					echo $this->Html->tableCells($rows);

					?>

					</tbody>
				</table>
		<h3 class="text-center">

			<?php endif; ?>

			<?= $this->Number->format($fundraising->pledgedAmount, ['places' => 0, 'before' => 'USD ']) ?>

			of
			<strong>
				<?= $this->Number->format($fundraising->totalPledgedAmount, ['places' => 0, 'before' => 'USD ']); ?>
			</strong>


		</h3>

		<?php

		$targetPercent = round(($fundraising->pledgedAmount * 100) / $fundraising->totalPledgedAmount, 2);

		if ($targetPercent > 100) {
			$targetPercent = 100;
		}

		?>

		<div class="progress" style="border: 1px solid #aaa;">
			<div style="width: <?= $targetPercent; ?>%" class="progress-bar progress-bar-success"></div>
		</div>

		<?php if (!empty($fundraising->getUserAmountPledged($authUser->id))): ?>

			<div class="row">
				<div class="col-md-12 col-md-offset-6 text-center">
					<div class="alert alert-info">
						You have pledged a total of <strong><?= $this->Number->format($fundraising->getUserAmountPledged($authUser->id), ['places' => 0, 'before' => 'USD ']); ?></strong> to this fundraising.
					</div>
				</div>
			</div>

		<?php endif; ?>

		<?php if ($fundraising->status != 'R'): ?>

			<?= $this->Form->create(null, ['url' => ['controller' => 'fundraisings', 'action' => 'pledge', $fundraising->id]]); ?>

			<?php if ($fundraising->type == 'B'): ?>

				<div class="text-center">

					<p>
						<strong><?= round($fundraising->pledgedAmount / ($fundraising->amount / $fundraising->blocks)); ?> blocks</strong> have been pledged.<br>
						<strong><?= $fundraising->blocks - round($fundraising->pledgedAmount / ($fundraising->amount / $fundraising->blocks)); ?> blocks</strong> needed to reach fundraising target. <br>
						Maximum blocks you can pledge is <strong><?= $fundraising->max_blocks ?></strong>.
					</p>

					<?php if ($targetPercent == 100): ?>

						<h5>Fundraising target has been reached. No more pledges possible.</h5>

					<?php elseif (!empty($fundraising->getUserAmountPledged($authUser->id))): ?>
						<h5>You have already pledged <?= ($fundraising->blocks / ($fundraising->amount / $fundraising->getUserAmountPledged($authUser->id))) ?> block(s).</h5>

					<?php else: ?>

						<?php if (isset($isInitiative) && $initiative->status != 'R'): ?>

							<div class="row">
								<div class="col-xs-12 col-sm-12 col-xs-offset-6 col-sm-offset-6">
									<?= $this->Form->input('group_id',[
										'label' => false,
										'empty' => '-- Charity --',
										'required' => true
									]); ?>
								</div>
								<div class="col-xs-12 col-sm-12 col-xs-offset-6 col-sm-offset-6">
									<?php $range = range(0, $fundraising->max_blocks); unset($range[0]);?>
									<?= $this->Form->input('pledge_blocks',[
										'label' => false,
										'empty' => '-- Blocks --',
										'required' => true,
										'options' => $range
									]); ?>
								</div>
							</div>

						<?php endif; ?>

						<?php if (!isset($isInitiative) || (isset($isInitiative) && $initiative->status != 'R')): ?>

							<h5>
								Pledge 1 Block (= <?= $this->Number->format($fundraising->amount / $fundraising->blocks, ['places' => 0, 'before' => 'USD ']); ?>)
							</h5>

							<div class="row">
								<div class="col-xs-4 col-xs-offset-8">
									<?php if ($fundraising->allow_anonymous) {
											echo $this->Form->input('anonymous', ['type' => 'checkbox']);
										} ?>
								</div>
								<div class="col-xs-4">
									<?php if ($fundraising->allow_hidden_amount) {
										echo $this->Form->input('hide_amount', ['type' => 'checkbox']);
									} ?>
								</div>
							</div>

							<br>

							<?= $this->Form->submit('Pledge', ['bootstrap-type' => 'primary', 'class' => 'btn-lg', 'onclick' => 'return confirmPledge()']); ?>

						<?php endif; ?>

					<?php endif; ?>

				</div>

			<?php endif; ?>

			<?php if (in_array($fundraising->type, ['P', 'M'])): ?>

				<?php if (isset($isInitiative) && $initiative->status != 'R'): ?>

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-xs-offset-6 col-sm-offset-6">
							<?= $this->Form->input('group_id', [
								'label' => false,
								'empty' => '-- Charity --',
								'required' => true
							]); ?>
						</div>
					</div>

				<?php endif; ?>

				<div class="row">

					<div class="col-md-12 col-md-offset-6 text-center">

						<?php if (!isset($isInitiative) || (isset($isInitiative) && $initiative->status != 'R')): ?>
							<h5>How much do you want to pledge?</h5>
							<br>
							<?= $this->Form->input('amount', [
								'type' => 'number',
								'step' => '0.01',
								'min' => '0.01',
								'class' => 'input-lg',
								'prepend' => 'USD',
								'label' => false,
								'required' => true
							]); ?>

							<div class="row">


								<div class="col-md-12">
									<?php
									if ($fundraising->allow_anonymous) {
										echo $this->Form->input('anonymous', ['type' => 'checkbox']);
									}
									?>
								</div>

								<div class="col-md-12">
									<?php
									if ($fundraising->allow_hidden_amount) {
										echo $this->Form->input('hide_amount', ['type' => 'checkbox']);
									}
									?>
								</div>

							</div>
							<br>

							<?= $this->Form->submit('Pledge', ['bootstrap-type' => 'primary', 'class' => 'btn-lg', 'onclick' => 'return confirmPledge()']); ?>

						<?php endif; ?>

					</div>

				</div>

			<?php endif; ?>

			<?= $this->Form->end(); ?>

		<?php endif; ?>

	</div>
</div>