<div class="ibox">
	<div class="ibox-title">
		<h3>Files</h3>
	</div>
	<div class="ibox-content">
		<div class="row">
			<div class="col-xs-24 col-md-10">
				<?= $this->Form->input('upload', [
					'type'=>'file',
					'label' => false,
					'templates' => [
						'inputContainer' => '<div class="form-group">{{content}}</div>'
					]
				]); ?>
			</div>
			<div class="col-xs-24 col-md-6">
				<?= $this->Form->submit('Upload File', [
					'class' => 'btn-primary',
					'name' => 'upload-file',
					'id' => 'upload-file'
				]); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-24">

				<?php if (!$assets->isEmpty()) :?>
					<div class="table-responsive">
						<table class="table table-condensed table-striped">
							<thead>
							<?php

							$headers = [
								'File',
								'Name',
								''
							];

							echo $this->Html->tableHeaders($headers);

							?>
							</thead>
							<tbody>
							<?php

							$extensions = [
								'image/gif',
								'image/jpeg',
								'image/png',
							];

							$rows = [];

							foreach ($assets as $key => $asset) {

								$actions = [
									'view' =>$this->Html->link(
										$icons['view'],
										['action' => 'view', 'controller' => 'Assets', $asset->id],
										['escape' => false, 'target' => '_blank']
									),
									'download' => $this->Html->link(
										$icons['download'],
										['action' => 'download', 'controller' => 'Assets', $asset->id],
										['escape' => false, 'target' => '_blank']
									),
									'delete' => $this->Html->link(
										$icons['delete'],
										['action' => 'delete', 'controller' => 'Assets', $asset->id, '?' => ['return_url' => $this->request->here()]],
										['escape' => false,'title' => 'Delete', 'confirm' => 'Are you sure you want to delete '.$asset->name.'?', 'title' => 'Delete']
									)
								];

								if (!in_array($asset->type, $extensions) && $asset->type != 'application/pdf')
									unset($actions['view']);

									$rows[] = [
										$this->Html->link(
											(in_array($asset->type, $extensions) ?
												$this->Html->image('https://s3.us-east-1.amazonaws.com/'.$bucket.'/' . $asset->id, [
													'fullBase' => true,
													'width' => '50px',
													'height' => '50px'
												]) :
												$this->Html->image('file-icon.png', [
													'fullBase' => true,
													'width' => '50px',
													'height' => '50px'
												])
											),
											['action' => 'view', 'controller' => 'Assets', $asset->id],
											['escape' => false, 'target' => '_blank']
										),
										$asset->name,
										[implode(' ', $actions),['class' => 'actions']]
									];

							}

							echo $this->Html->tableCells($rows);

							?>
							</tbody>
						</table>
					</div>
				<?php endif; ?>

			</div>
		</div>
	</div>
</div>
