<div class="ibox-content" style="padding: 1px 0px;">
	<div class="ibox" style="margin:2px;">
		<div class="ibox-title">
			<h2><?= isset($listTitle) ? $listTitle : 'User List' ?></h2>
			<div class="ibox-tools">
				<?= $this->Html->link(
					$icons['chevron-down'],
					'javascript:;',
					[
						'escape' => false,
						'class' => 'btn btn-icon btn-info  collapse-link',
					]
				); ?>
			</div>
		</div>
			<div class="ibox-content"  style="display:none;" id="contact-list-ibox">
				<?php foreach ($contactList as $role => $users): ?>
					<?php $roleHash = sha1($role); ?>
					<div class="ibox" style="margin:2px;">
						<div class="ibox-title"  style="height:0px;">
							<div class="pull-left" style="margin:10px;">
								<?= $this->Form->input($roleHash, [
									'label' => [
										'text' => $role,
										'escape' => false
									],
									'type' => 'checkbox',
									'hiddenField' => false,
									'class' => 'role'
								]); ?>
							</div>
							<div class="ibox-tools">
								<?= $this->Html->link(
									$icons['chevron-down'],
									'javascript:;',
									[
										'escape' => false,
										'class' => 'btn btn-icon btn-info  collapse-link',
									]
								); ?>
							</div>
						</div>

						<?php

						$style = '';

						if (count($users) > 20) {
							$style = 'overflow: scroll;';
						}

						?>

					<div class="ibox-content <?= $roleHash ?>" style="margin:2px; max-height: 625px; <?= $style ?>">
						<?= $this->Form->input($roleHash.'-search-filter', [
							'label' => false,
							'placeholder' => 'Search Name...',
							'class' => 'input-sm search-filter'
						]); ?>
						<?= $this->Form->input('EmailLists.email_receivers.'.$roleHash.'._ids', [
							'label' => false,
							'multiple' => 'checkbox',
							'hiddenField' => false,
							'options' => $users
						]); ?>
					</div>
				</div>
				<?php endforeach; ?>

			</div>


	</div>


</div>


