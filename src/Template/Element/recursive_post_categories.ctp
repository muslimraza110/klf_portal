<?php

	if (! isset($level))
		$level = 0;

	foreach ($items as $item) {
		
		$name = str_repeat('<i class="child-spacer"></i>', $level) . $this->Html->link($item->name, ['controller' => 'Posts', 'action' => 'index', $item->id]);
		$count = $item->postCount() . ' posts';
		
		if ($level == 0) {
			$name = '<span class="semibold">' . $name . '</span>';
			$count = '<span class="semibold">' . $count . '</span>';
		}
		
    $actions = $this->Html->link(
			$icons['edit'] . 'Edit',
			['controller' => 'PostCategories', 'action' => 'edit', $item->id],
			['escape' => false, 'class' => 'btn btn-primary']
		);
		
		if ($item->canDelete()) {
			$actions .= $this->Html->link(
				$icons['delete'] . 'Delete',
				['controller' => 'PostCategories', 'action' => 'delete', $item->id],
				['escape' => false, 'class' => 'btn btn-danger', 'confirm' => 'Are you sure you want to permanently delete this category?']
			);
		}
		else {
			$actions .= '<button disabled="disabled" class="btn btn-danger disabled">' . $icons['delete'] . 'Delete</button>';
		}
    
    if ($level == 0 && in_array($item->id, [1,3])) {
      $newBtn = $this->Html->link(
        $icons['add'] . 'New Subcategory',
        ['controller' => 'PostCategories', 'action' => 'add', $item->id],
        ['escape' => false, 'class' => 'btn btn-primary']
      );
    }
		else {
			$newBtn = '<button disabled="disabled" class="btn btn-primary disabled">' . $icons['add'] . 'New Subcategory</button>';
		}
		
		$row = [
			
			$name,
			
			$count,
			
			[
				$newBtn,
				['class' => 'compact']
			],
			
			[
				$actions,
				['class' => 'compact']
			]
		];
		
		echo $this->Html->tableCells([$row]);
		
		// RECURSIVE CALL
		echo $this->element('recursive_post_categories', ['items' => $item['children'], 'level' => $level + 1]);
	}