<?php
$js = <<<JS

var elems = document.querySelectorAll("#notification-table .js-switch");

for (var i = 0; i < elems.length; i++) {
	var switchery = new Switchery(elems[i], { size: "small",  color: "#79CA57" });
	
	elems[i].onchange = function() {
		var notificationButton = "";
		var currentId = $(this).attr("id");
		var id = currentId.split("-").pop();
		
		if ($(this).hasClass("activate")) {
			
			notificationButton = document.querySelector("#send-email-" + id);
			
			if (! this.checked && notificationButton.checked)
				notificationButton.click();
		}
		else {
			notificationButton = document.querySelector("#activate-" + id);
			
			if ( this.checked && ! notificationButton.checked) {
				notificationButton.click();
			}
		}
		
	}
}

JS;

echo $this->Html->scriptBlock($js, ['block' => true]);
?>

<div class="ibox">
  <div class="ibox-title">
    <h3>Notifications</h3>
  </div>
  <div class="ibox-content">
		
    <table id="notification-table" class="table table-bordered">
      <thead>
        <?=
          $this->Html->tableHeaders([
            'Publication Type',
            'Notify Me',
            'Notify Me By Email'
          ]);
        ?>
      </thead>
      <tbody>
        <?php
          $rows = [];

          foreach ($postCategories as $key => $category) {
         
            if ($category->id === 1) {

              $defaultActivate = 0;
              $defaultEmail = 0;
              $disabled = '';
              
              if (isset($contact) && !empty($contact->user)) {
                $defaultActivate = $contact->user->checkNotification($category->id, 'Posts', true);
                $defaultEmail = $contact->user->checkNotification($category->id, 'Posts', false);
              }
              else {
                $defaultActivate = $user->checkNotification($category->id, 'Posts', true);
                $defaultEmail = $user->checkNotification($category->id, 'Posts', false);
              }

              $rows[] = [
                $category->name,
                $this->Form->input("notification_settings.$category->id.activate", [
                  'type' => 'checkbox',
                  'class' => 'js-switch activate',
                  'id' => "activate-" . $key,
                  'label' => false,
                  'default' => $defaultActivate,
                  'disabled' => $disabled
                ]),

                $this->Form->input("notification_settings.$category->id.send_email", [
                  'type' => 'checkbox',
                  'class' => 'js-switch send-email',
                  'id' => "send-email-" . $key,
                  'label' => false,
                  'default' => $defaultEmail,
                  'disabled' => $disabled
                ])
              ];
            }
          }

          echo $this->Html->tableCells($rows);
        ?>
      </tbody>
    </table>
		
  </div>
</div>