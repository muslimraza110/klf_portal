<?php
$dataSlide = '';
$first = true;
$images = '';
$extensions = [
	'image/gif',
	'image/jpeg',
	'image/png',
];
foreach ($assets as $key => $asset) {
	$active = ($first ? 'active' : '');
	$dataSlide .= '<li data-target="#lightbox" data-slide-to="'.$key.'" class="'.$active.'"></li>';



	$images .=
		'<div class="item ' . $active . '"><center>' .
		(in_array($asset->type, $extensions) ?
			$this->Html->image('/'.$this->request->params['prefix'].'/assets/view/' . $asset->id, [
				'fullBase' => true,
				'width' => '100%'
			]) :
			$this->Html->image('file-icon.png', [
				'fullBase' => true,
				'height' => '250px'
			])
		) .
		'</center></div>';

	$first = false;
}

?>

<div class="container">

	<div class="modal fade and carousel slide" id="lightbox" style="position:fixed;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body" >
					<ol class="carousel-indicators">
						<?= $dataSlide ?>
					</ol>
					<div class="carousel-inner">
						<?= $images ?>
					</div><!-- /.carousel-inner -->
					<a class="left carousel-control" href="#lightbox" role="button" data-slide="prev">
						<span style="top:50%;position:absolute"><?= $icons['angle-double-left'] ?></span>
					</a>
					<a class="right carousel-control" href="#lightbox" role="button" data-slide="next">
						<span style="top:50%;position:absolute"><?= $icons['angle-double-right'] ?></span>
					</a>
				</div><!-- /.modal-body -->
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

</div><!-- /.container -->