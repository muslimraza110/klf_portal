<?php foreach ($folders as $folder): ?>

	<?php if (!$folder->is_protected || $user->role == 'A'): ?>
	<li class="list-item" data-categoryId="<?= $folder->id; ?>" id="item_<?= $folder->id; ?>">
		<div class="menuDiv">
			
			<?= $icons['bars']; ?><?= $this->Html->link($folder->name, ['action' => 'view', $folder->id]); ?>
			
			<div class="pull-right">
				
				<?= $this->Html->link(
					$icons['view'] . 'View',
					['action' => 'view', $folder->id],
					['class' => 'btn btn-xs btn-primary', 'escape' => false]
				); ?>

				<?= $this->Html->link(
					$icons['edit'] . 'Edit',
					['action' => 'edit', $folder->id],
					['escape' => false, 'class' => 'btn btn-xs btn-primary']
				); ?>

				<?= $this->Form->postLink(
						$icons['delete'] . 'Delete',
						['action' => 'delete', $folder->id],
						[
							'confirm' => sprintf('Are you sure you want to delete "%s"?', $folder->name),
							'escape' => false,
							'class' => 'btn btn-xs btn-danger'
						]
					);
				?>
				
			</div>
		</div>

		<?php
			if (!empty($folder->children)) {
				printf(
					'<ol class="mjs-nestedSortable-branch">%s</ol>',
					$this->element('recursive_folders', ['folders' => $folder->children])
				);
			}
		?>
		
	</li>
	<?php endif; ?>

<?php endforeach; ?>