<div class="row">
	<div class="col-xs-24">
		
		<div class="modal inmodal fade" id=<?= $formId; ?> tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					
					<?= $this->Form->create($contact, ['id' => 'form-' . $formId]); ?>

						<div class="ibox">
							<div class="ibox-title">
								<h3>Personal Information</h3>
							</div>

							<div class="ibox-content no-borders">
								<div class="row">
									<div class="col-xs-24">

										<p class="alert alert-danger text-center" id="responseError">
											An error occured... Please try again later.
										</p>

										<?php
											echo $this->Form->input('id', ['type' => 'hidden', 'value' => $contact->id]);
											
											foreach ($fields as $key => $field) {
												$options = [];

												if (isset($field['label']))
													$options = $field;
			
												if (isset($field['label']))
													echo $this->Form->input($key, $options);
												else
													echo $this->Form->input($field, $options);
											}

										?>
									</div>
								</div>
							</div>
						</div>
						
					<?= $this->Form->end(); ?>

					<div class="row">
						<div class="col-xs-24">
							<div class="ibox-content no-borders">
								<div class="sk-spinner sk-spinner-wave">
									<div class="sk-rect1"></div>
									<div class="sk-rect2"></div>
									<div class="sk-rect3"></div>
									<div class="sk-rect4"></div>
									<div class="sk-rect5"></div>
								</div>

								<p class="alert alert-success text-center" id="responseSuccess">
									The contact information has been saved. Please close this modal to review the change.
								</p>

							</div>
						</div>
					</div>
					
					<div class="modal-footer">
					  <button id="submit-form" type="button" class="btn btn-primary">Save changes</button>
		        <button id="cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		        <button class="btn btn-warning closeBtn" data-dismiss="modal">Close</button>
		      </div>

				</div>
			</div>
		</div>

	</div>
</div>


