<div class="rsABlock sampleBlock">
  <div class="icon"> <?= $icons['exclamation-triangle'] ?></div>
  <div class="headline">
    <h3><?= $slide->name; ?></h3>
    <p>
      <?= $this->Text->truncate(strip_tags($slide->content), 250, [
        'ellipsis' => '...',
        'exact' => false
      ]); ?>
    </p>
  </div>
</div>

<?php
  if ($authUser->role == 'A') {
    echo $this->Html->link( $icons['edit'] . ' Edit',
      ['controller' => 'Posts', 'action' => 'edit', $slide->id],
      ['class' => 'edit-link btn-icon', 'escape' => false]
    );
	}

 	echo $this->Html->link('', ['controller' => 'Posts', 'action' => 'view', $slide->id], ['class' => 'rsLink', 'target' => '_blank']);
?>
