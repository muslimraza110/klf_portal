    <?php

      //To stop chat from running (easier debugging)
			$isDebugTurnedOn = ($appConfig['debug']) ? 1 : 0;
			echo $this->Html->scriptBlock("
				var DEBUG = Boolean(" . $isDebugTurnedOn . ");
				var PATH = '" . $appConfig['path'] . "';
			");

      echo $this->Html->scriptBlock("
        window.paceOptions = {
          elements: false,
          eventLag: false,
          ajax: {
            ignoreURLs: [
              /chat_refresh/
            ]
          }
        };
      ");

      echo $this->Html->scriptBlock("
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-77731167-1', 'auto');
        ga('send', 'pageview');
      ");

      echo $this->Html->script([
        'jquery-2.1.1',
				'jquery.mousewheel.min',
        'bootstrap.min',
				
        //'plugins/metisMenu/jquery.metisMenu',
        //'plugins/slimscroll/jquery.slimscroll.min',
        'plugins/pace/pace.min',
        'plugins/iCheck/icheck.min',
        'plugins/switchery/switchery',
				'plugins/chartJs/Chart.min',
				'plugins/nestable/jquery.nestable',
				'plugins/dataTables/jquery.dataTables.min',
				'plugins/dataTables/dataTables.bootstrap.min',
				'plugins/dataTables/moment',
				'plugins/dataTables/datetime-moment',
				
				'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min', //instead of slimscroll
        'spin.min',
        'jquery-ui-1.10.4.custom.min',

        'inspinia.js?v=1',
        'custom.js?v=1',
        'plugins/fullcalendar/moment.min',
				'stupidtable.min',
				
        'ng-assets/vendor/angular', //give better debugging thant the .min version
        'ng-assets/vendor/angular-resource.min',
        'ng-assets/plugins/angular-css-js-injector',

        'ng-assets/plugins/angular-moment.min',
        'ng-assets/plugins/ui-bootstrap-tpls-1.3.2.min',
        'ng-assets/plugins/angular-spinner.min',
        //'ng-assets/plugins/angular-slimscroll',
				'ng-assets/plugins/ng-resize.min',
				'ng-assets/plugins/scrollbars-modified-by-lyz',
        'ng-assets/plugins/dnd-lists.min',
        'ng-assets/plugins/ui-sortable',
        'ng-assets/plugins/calendar',
        'ng-assets/plugins/angular-ckeditor.min',
        'ng-assets/plugins/angular-ui-switch.min',
        'ng-assets/plugins/angular-chart.min',
				
				'ng-assets/plugins/clickoutside.directive',
				
        'ng-assets/app',

        'ng-assets/factories/chat-factory',
        'ng-assets/controllers/chat-controller',
        'ng-assets/directives/chat/chat-directives',

        'ng-assets/resources/post',
        'ng-assets/controllers/posts-index-controller',
      ]);

      echo $this->Html->scriptBlock("
        $('.i-checks, input[type=\"checkbox\"]:not(input[icheck]):not(\".js-switch\"), input[type=\"radio\"]:not(input[icheck])').iCheck({
          checkboxClass: 'icheckbox_square-green',
          radioClass: 'iradio_square-green',
          increaseArea: '20%' // optional
        });

        $('[title]').tooltip({
          placement: 'bottom'
        });

        $('#global-search-form #dropdown-search-menu li > a').on('click', function(e){
          e.preventDefault();
          var scopeValue = $(this).attr('data-value');
          var scopeText = $(this).text();

          $('#global-search-form #search-field').html( '<i class=\"fa fa-bars\"></i><small>' + scopeText + '</small>');
          $('#global-search-form #scope').val(scopeValue);
        });

        var searchScopes = JSON.parse('" . json_encode($searchScopes) . "');

        $(window).on('load', function(){
          var scopeValue = $('#global-search-form #scope').val();
          var scopeText = '';

          for ( key in searchScopes ) {
            if (key === scopeValue)
              scopeText = searchScopes[key];
          }

          if (scopeValue != null && scopeValue.length != 0) {
            $('#global-search-form #search-field').html( '<i class=\"fa fa-bars\"></i><small>' + scopeText + '</small>');
          }
        });

      ", ['block' => true]);

      echo $this->fetch('script');
    ?>
  </body>
</html>
