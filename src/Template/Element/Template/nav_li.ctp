<?php

  if(!isset($level))
    $level = 1;

  if(isset($item['divider'])){
    echo '<li class="divider"></li>';
  }
  elseif($itemKey == 'chat-status') {
    
    echo '<li id="nav-' . $itemKey . '">' .
      $this->Html->link('<span class="text-danger">' . $icons['times-circle'] . ' Offline</span>', '#', [
        'ng-bind-html' => 'chatCtrl.data.authUserData.status.label | html',
        'ng-click' => 'chatCtrl.pickStatus()',
        'class' => '{{chatCtrl.data.authUserData.status.css}}',
        'escape' => false
      ]) .
    '</li>';
    
  }
  elseif($itemKey != 'hasActiveItem') {
    
    if(isset($main_nav_style) && $main_nav_style) {
      $name = $item['icon'] . '<span class="visible-xs-inline">' . $item['name'] . '</span>';
    }
    else {
      $name = (isset($item['icon'])) ? $item['icon'] . '<span class="nav-label">' . $item['name'] . '</span>' : $item['name'];
    }

    $liClasses = [];

    if(isset($item['special']) && $item['special'])
      $liClasses[] = 'special_link';

    if(isset($item['active']) && $item['active'])
      $liClasses[] = 'active';

    $linkOptions = ['escape' => false];
    
    if(isset($main_nav_style) && $main_nav_style) {
      $linkOptions['title'] = $item['name'];
      
      if(isset($item['children']) && !empty($item['children'])) {
        $linkOptions['title'] .= ' &nbsp; &nbsp;';
      }
    }

    if(isset($item['linkOptions']) && is_array($item['linkOptions'])) {
      $linkOptions = array_merge($linkOptions, $item['linkOptions']);
    }

    if(isset($item['children']) && !empty($item['children'])) {
      $name .= $icons['caret-down'];
      $item['route'] = '#';
      
      $liClasses[] = 'has-dropdown';
      
      $moreLinkOptions = [
        'class' => 'dropdown-toggle right-sidebar-close',
        'data-toggle' => 'dropdown',
      ];
      $linkOptions = array_merge($linkOptions, $moreLinkOptions);
    }
    
    echo '<li class="' . implode(' ', $liClasses) . '" id="nav-' . $itemKey . '">';

    echo $this->Html->link($name, $item['route'], $linkOptions);

    if(isset($item['children']) && !empty($item['children'])) {

      echo '<ul class="dropdown-menu">';

      $level++;
      foreach($item['children'] as $subItemKey => $subItem) {
        echo $this->element('Template/nav_li', ['itemKey' => $subItemKey, 'item' => $subItem, 'level' => $level]);
      }

      echo '</ul>';
    }

    echo '</li>';
  }
?>
