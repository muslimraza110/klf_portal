<!DOCTYPE html>
<html>
	<head>
		<?= $this->Html->charset() ?>
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>Khoja: <?= trim(strip_tags($title)) ?></title>
		
		<?= $this->Html->meta('favicon.ico', '/favicon.ico', ['type' => 'icon']); ?>
		<?= $this->fetch('meta') ?>
	
		<?= $this->Html->css([
			'plugins/dataTables/dataTables.bootstrap.min.css',
			'style.css?v=1',
			'plugins/iCheck/custom',
			'../js/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min'
		]) ?>
		
		<?= $this->fetch('css') ?>
		
	</head>
  <body ng-app="KhojaApp" ng-cloak>