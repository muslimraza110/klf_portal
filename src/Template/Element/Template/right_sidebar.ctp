<?= $this->Html->scriptBlock("
    $('.right-sidebar-toggle').click(function(){
     
      if ($('#notifications').length > 0) {
        $('#notifications li').on('click', function(e){
          e.preventDefault();
          
          var modelId = $('a', $(this)).attr('model_id');
          var model = $('a', $(this)).attr('model');
          var redirectLink = $('a', $(this)).attr('href');
          
          $.ajax({
            type: 'POST',
            url: '" . $this->Url->build(['controller' => 'NotificationExcludes', 'action' => 'ajax_add']) . "',
            data: {
              model_id: modelId,
              model: model
            },
          })
          .success(function(data){
            if (data['error'] == undefined || data['error'] == null || data['error'].length == 0) {
              window.location.href = redirectLink;
            }
          });
        });
      }
    });
  ", ['block' => true]);
?>

<?= $this->Html->scriptBlock('
  KhojaApp.controller("SidebarController", ["$scope", "$timeout", "$window", function($scope, $timeout, $window) {
    
    $scope.setChatHeight = function() {
      var chatSidebarHeight = $("#right-sidebar").height() - $("#right-sidebar-hiding-heading").height() - $("#right-sidebar .nav-tabs").height() - $("#chat-sidebar-title").height() - $("#chat-list-errors").height();
      
      if (!isNaN(chatSidebarHeight) && chatSidebarHeight > 100)
        $("#chat-list-users-online").height(chatSidebarHeight);
      else
        $("#chat-list-users-online").height(100);
    };
    
    $scope.chatSidebarConfig = {
      callbacks:{
        onUpdate:function() {
          $timeout(function() {
            $scope.setChatHeight();
          });
        }
      }
    };
    
    $scope.setNotificationsHeight = function() {
      var notificationsSidebarHeight = $("#right-sidebar").height() - $("#right-sidebar-hiding-heading").height() - $("#right-sidebar .nav-tabs").height() - $("#notifications-sidebar-title").height();
      
      if (!isNaN(notificationsSidebarHeight) && notificationsSidebarHeight > 100)
        $("#notifications").height(notificationsSidebarHeight);
      else
        $("#notifications").height(100);
    };
    
    $scope.notificationsSidebarConfig = {
      callbacks:{
        onUpdate:function() {
          $timeout(function() {
            $scope.setNotificationsHeight();
          });
        }
      }
    };
    
  }]);
  
  /*
    var chatSidebarHeight = $("#right-sidebar").height() - $("#chat-sidebar-title").height() - $("#chat-list-errors").height();
    $("#chat-list-users-online").height(chatSidebarHeight);
    console.log("chatSidebarHeight: ", chatSidebarHeight);
    
    
  */
  
  ', ['block' => true]);
?>


<div id="right-sidebar" ng-controller="SidebarController">
  <div class="sidebar-container">
    
    <div id="right-sidebar-hiding-heading">
      
      <button type="button" class="close right-sidebar-toggle" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <?= $icons['comments'] ?>
      
    </div>
    
    <uib-tabset active="rightSidebarActiveTab" ng-init="rightSidebarActiveTab = 0">

			<?php if (false): // @TODO ?>

      <uib-tab index="0" heading="Chat">
        
        <div id="chat-sidebar-title" class="sidebar-title">
          <h3><?= $icons['comments-o'] ?>Chat</h3>
          <small ng-if="!chatCtrl.data.errors.length">{{chatCtrl.data.usersOnline.length | number}} users are online at the moment.</small>
        </div>
        
        <ul id="chat-list-errors" class="list-errors" ng-if="chatCtrl.data.errors.length">
          <li ng-repeat="message in chatCtrl.data.errors">{{message}}</li>
        </ul>
        
        <ul id="chat-list-users-online" style="height: 100px;" class="list-users-online" ng-if="chatCtrl.data.usersOnline.length" ng-scrollbars ng-scrollbars-config="chatSidebarConfig">
          
          <li class="sidebar-message" ng-if="chatCtrl.data.usersOnline.length > 1" ng-click="chatCtrl.openMultiUserSessionModal()">
            <div class="pull-left text-center">
              <?= $this->Html->image('multi-user-chat.jpg', ['class' => 'img-circle', 'width' => '38']); ?>
            </div>
            
            <div class="media-body" style="font-weight: bold;">
              Start a session with multiple users
              <span spinner-key="chat-user-spinner-multi" us-spinner="{color: '#061F5C', scale: 0.2, left: '85%'}"></span>
            </div>
          </li>
          
          <li class="sidebar-message" ng-repeat="user in chatCtrl.data.usersOnline | orderBy: 'name' as filtered_users_online track by $index" ng-click="chatCtrl.openChatBox(user, $index)">
            
            <div class="pull-left text-center">
              <img ng-src="{{user.profile_thumb}}" class="img-circle" width="38" />
            </div>
            
            <div class="media-body">
              {{user.name}}
              <br>
              <small class="text-muted">{{user.role_label}}</small>
              <span spinner-key="chat-user-spinner-{{$index}}" us-spinner="{color: '#061F5C', scale: 0.2, left: '85%'}"></span>
            </div>
            
          </li>
          
        </ul>
      </uib-tab>

			<?php endif; ?>
      
      <uib-tab index="0" heading="Notifications">
        
        <div id="notifications-sidebar-title">
          <div class="sidebar-title">
            <h3><?= $icons['exclamation-circle'] ?>Notifications</h3>
          
            <?php if (empty($notifications)): ?>
              <small>You have 0 new notifications.</small>
            <?php endif; ?>
          </div>
        </div>

        <?php if (! empty($notifications)): ?>
          <div id="notifications" style="height: 100px;" ng-scrollbars ng-scrollbars-config="notificationsSidebarConfig" ng-resize="setNotificationsHeight()">
            <ul class="list-notifications">
              <?php foreach ($notifications as $data): ?>
                <?php foreach ($data as $notification): ?>
                  <li class="sidebar-message">
                    <?php
                      $name = '';

                      if ($notification->dataType == 'Posts') {
                        $name = $notification->name;
                      }
                      else {
                        if (empty($notification->topic))
                          $name = html_entity_decode(strip_tags($notification->content), ENT_QUOTES);
                        else
                          $name = $notification->topic;
                      }

                      echo '<h5>' . $this->Html->link($name,
                        $notification->post_link,
                        ['model_id' => $notification->id, 'model' => $notification->dataType, 'escape' => false]
                      ) . '</h5>';
                    ?>
                    
                    <?php if(!empty(strip_tags($notification->content))): ?>
                      <p><small>
                        <?= $this->Text->truncate(strip_tags($notification->content), 100, [
                          'ellipsis' => '...',
                          'exact' => false,
                          'escape' => false
                        ]); ?>
                      </small></p>
                    <?php endif; ?>
                    
                  </li>
                <?php endforeach; ?>
              <?php endforeach; ?>
            </ul>
          </div>
        <?php endif; ?>
      </uib-tab>
      
    </uib-tabset>
  </div>
</div>

<div id="chat-boxes-container">
  <div id="chat-boxes">
    
    <chat-box
      ng-repeat="chatBox in chatCtrl.data.chatBoxes | orderBy : 'session.auth_user_join_data.open'"
      data="chatBox"
    ></chat-box>
    
  </div>
</div>