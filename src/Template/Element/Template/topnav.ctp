<div class="row">
	
	<nav id="top-nav" class="navbar">
		<ul class="nav navbar-top-links navbar-right">
			<li class="hidden-xs">
				<?= $this->Html->link($icons['user'] . 'Edit My Profile',
					['controller' => 'Users', 'action' => 'edit_profile', $authUser->id],
					['escape' => false]); ?>
			</li>
			<?php if (!empty($nav['user'])): ?>
				<li class="dropdown">
				
					<a id="user-profile" data-toggle="dropdown" class="dropdown-toggle">
						
						<?= $this->Html->image($authUser->profile_thumb.'?'.microtime(), ['class' => 'img-circle']); ?>
						
						<span class="m-t-xs m-r-sm">
							<strong class="font-bold"><?= $authUser->name; ?></strong>
							<span ng-bind-html="chatCtrl.data.authUserData.status.icon | html" class="status {{chatCtrl.data.authUserData.status.css}}">
								<span class="text-danger"><?= $icons['times-circle'] ?></span>
							</span>
						</span>
						
						<span class="text-muted"><i class="fa fa-angle-down"></i></span>
						
					</a>
				
					<ul class="dropdown-menu">
						<?php

							foreach($nav['user'] as $itemKey => $item) {
								echo $this->element('Template/nav_li', array('itemKey' => $itemKey, 'item' => $item));
							}

						?>
					</ul>
				
				</li>
			<?php endif; ?>
			
			<?php if ($authUser->role != 'W'): ?>

				<li>
					<?= $this->Html->link($icons['comments'], '#', [
						'escape' => false,
						'title' => __('Communications'),
						'class' => 'right-sidebar-toggle',
						'ng-click' => '$event.preventDefault();'
					]); ?>
				</li>

			<?php endif; ?>

		</ul>
	</nav>
	
	<nav id="banner" class="navbar">
		
		<div class="container-fluid">
			<div class="not-a-row">
				
				<div class="col-xs-24 col-sm-12">
					
					<div class="navbar-header">
						<?php
						
						if (
							!empty($this->request->params['controller'])
							&& in_array($this->request->params['controller'], [
								'Carts',
								'Orders',
								'Products',
								'ProductCategories',
								'ProductItems',
								'ProductSales',
								'Promotions',
								'StoreMetrics',
								'FeaturedProducts'
							])
						) {
							
							echo $this->Html->link(
								$this->Html->image('logo-store.png', ['alt' => 'Shop', 'id' => 'nav-logo']),
								['controller' => 'ProductCategories', 'action' => 'index'],
								['escape' => false]
							);
						
						} else {

							echo $this->Html->link(
								$this->Html->image('logo.png', ['alt' => 'Khoja', 'id' => 'nav-logo']),
								['controller' => 'Users', 'action' => 'dashboard'],
								['escape' => false]
							);
							
						}
						
						?>
				</div>
				
			</div>
			
			<div class="col-xs-24 col-sm-offset-4 col-sm-8">
				
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu-collapse" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<?php
				
				if ($authUser->role != 'W') {
					
					echo $this->Form->create(null, [
						'type' => 'get',
						'url' => ['controller' => 'Searches', 'action' => 'index'],
						'id' => 'global-search-form'
					]);
					
					echo $this->Form->input('scope', ['type' => 'hidden']);
					
					$scopeDropdown = '<div class="dropdown">';
					
					$scopeDropdown .= $this->Html->link($icons['bars'] . '<small>All</small>', '#', [
						'id' => 'search-field',
						'escape' => false,
						'data-toggle' => 'dropdown'
					]);
					
					$scopeDropdown .= '<ul id="dropdown-search-menu" class="dropdown-menu" aria-labelledby="search-field">';
					
					foreach ($searchScopes as $key => $scope) {
						
						$scopeDropdown .= sprintf('<li><a href="#" data-value="%s">%s</a></li>', $key, $scope);
						
					}
					
					$scopeDropdown .= '</ul>';
					$scopeDropdown .= '</div>';
					
					echo $this->Form->input('q', [
						'label' => false,
						'placeholder' => 'Search...',
						'append' => $this->Form->button($icons['search'], ['bootstrap-type' => 'info', 'class' => 'btn-icon']),
						'prepend' => $scopeDropdown,
						'escape' => false
					]);
					
					echo $this->Form->end();
					
				}
				
				?>
				
			</div>
			
		</div>
	</nav>
	
</div>

<nav class="navbar" id="main-menu">
	
	<div class="collapse navbar-collapse" id="main-menu-collapse">
		<ul class="nav navbar-nav">
			<?php
				foreach($nav['left'] as $itemKey => $item) {
					echo $this->element('Template/nav_li', array('itemKey' => $itemKey, 'item' => $item, 'main_nav_style' => true));
				}
			?>
		</ul>
	
		<ul class="nav navbar-right navbar-nav">
			<?php
				if(isset($nav['right'])){
					foreach($nav['right'] as $itemKey => $item) {
						echo $this->element('Template/nav_li', array('itemKey' => $itemKey, 'item' => $item, 'main_nav_style' => true));
					}
				}

			?>
		</ul>
	</div>
	
</nav>