<?php

  echo $this->Html->script([
    'ckeditor/ckeditor',
    'plugins/fullcalendar/fullcalendar.min',
    'ng-assets/controllers/calendar-controller.js?v=2',
    'ng-assets/directives/calendar/calendar-directives',
    'ng-assets/resources/calendar',
    'ng-assets/directives/bootstrapUIDatepickerFix'
  ], ['block' => true]);

  echo $this->Html->scriptBlock("
    $(window).load(function() {
      
      $('html, body').animate({
    		scrollTop: $('#calendar').offset().top
   		}, 1000);
     
    });

  ", ['block' => true]);

  
?>

<calendar user-role="<?= $authUser->role ?>" last-date="<?= $lastDate ?>" date-diff="<?= $dateDiff ?>"></calendar>

<div class="row m-t-lg">
	<div class="ibox">
		<div class="ibox-title">
			<?php $link = $futureEvents->toArray(); ?>
			<h2><?= empty(array_shift($link)[0]->link) ? 'All Future Events' : 'Event Details'; ?></h2>
			<div class="ibox-tools pull-right">
				<?= $this->Html->link(
					$icons['chevron-down'],
					'javascript:;',
					[
						'escape' => false,
						'class' => 'btn btn-icon btn-info  collapse-link',
					]
				); ?>
			</div>
		</div>
		<div class="ibox-content fc-calendar fc fc-unthemed fc-ltr ng-not-empty ng-scope" style="display:block;"   >
			<div class="fc-view-container" style="">
				<div class="fc-view fc-list-view fc-widget-content" style="">
					<div class="fc-scroller">
						<table class="fc-list-table">
							<?php foreach ($futureEvents as $startDate => $futureEvent): ?>
							<tr class="fc-list-heading" data-date="2018-06-07">
								<td class="fc-widget-header" colspan="3" >
									<span class="fc-list-heading-main"><?= $startDate; ?></span>
								</td>
							</tr>
							<?php foreach ($futureEvent as $event): ?>
								<?php $groupName = !empty($event->group->name) ? $event->group->name . ', ' : null; ?>
								<?php $title = $groupName . $event->user->name . ', ' . $event->name . '<strong>' . $event->description . '</strong>'; ?>

							<tr class="fc-list-item default" title="<?= $title; ?>" id="event-<?= $event->id; ?>">
								<td class="fc-list-item-time fc-widget-content"><?= $event->date_range. '<strong> - </strong>' .($event->all_day ? 'all-day' : '') ; ?></td>
								<td class="fc-list-item-marker fc-widget-content">
									<span class="fc-event-dot"></span>
								</td>
								<td class="fc-list-item-title fc-widget-content">
									<p><?= $title; ?></p>
								</td>
							</tr>
							<?php endforeach; ?>
						<?php endforeach; ?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
