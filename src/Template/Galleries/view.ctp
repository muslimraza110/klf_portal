<?php

$this->Html->css(
		[
			'../js/plugins/justified-gallery/jquery.justifiedgallery.min',
			'../js/plugins/ilightbox/css/ilightbox.css'
		],
		['block' => true]
);

$this->Html->script(
		[
			'plugins/justified-gallery/jquery.justifiedgallery.min',
			'plugins/ilightbox/jquery.requestAnimationFrame.js',
			'plugins/ilightbox/jquery.mousewheel.js',
			'plugins/ilightbox/ilightbox.packed.js'
		],
		['block' => true]
);

$scriptBlock = <<<JS

$('#justified-gallery').justifiedGallery({
	rowHeight: 200,
	margins: 5,
	cssAnimation: true
});

$('.ilightbox-gallery-{$folder->id}').iLightBox({
	controls: {
		skin: 'metro-black',
		thumbnail: false
	}
});

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>

<div id="justified-gallery">

	<?php foreach ($folder->files as $file): ?>

		<?php

		if ($file->resource->type == 'video/mp4')
			printf(
					'<a href="%1$s" class="ilightbox-gallery-%2$d" data-type="video" data-options="videoType: \'video/mp4\', html5video: { h264: \'%1$s\' }">%3$s</a>',
					$file->resource->relativeUrl(),
					$folder->id,
					$this->Html->image('gallery-video-play.png')
			);
		else
			echo $this->Html->link(
					$this->Html->image($file->resource->url(['height' => '200'])),
					$file->resource->url(),
					[
						'escape' => false,
						'class' => 'ilightbox-gallery-'.$folder->id,
						'data-type' => 'image'
					]
			);

		?>

	<?php endforeach; ?>

</div>