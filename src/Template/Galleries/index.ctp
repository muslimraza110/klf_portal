<ul class="galleries">

	<?php foreach ($folders as $folder): ?>

		<li>
			<div>

				<?php

				if (!empty($folder->files))
					$image = $folder->files[0]->resource->url(['height' => '800', 'square' => true]);
				else
					$image = 'http://placehold.it/800x800';

				?>

				<a href="<?= $this->Url->build(['action' => 'view', $folder->id]); ?>">

					<?= $this->Html->image($image, ['class' => 'img-responsive']); ?>

					<h4><?= $folder->name; ?></h4>

				</a>

			</div>
		</li>

	<?php endforeach; ?>

</ul>