<div class="container">
  <div class="row">
    <div class="form-box white-bg m-t-lg col-xs-24">
      <div class="text-center m-b-lg">
        <?= $this->Html->image('logo.png'); ?>
      </div>

      <h3>FRANCHISEE USER AGREEMENT</h3>
      <p>
        Thank you for logging into the MOLLY MAID Mirror.  This is a legal agreement between MOLLY MAID Canada and the party that uses information provided by MOLLY MAID Canada, each of whom accepts the terms of this agreement for herself, himself or itself.
      </p>
      <p>
        Acceptance of these terms will constitute a legally binding agreement by and between MOLLY MAID Canada and you the franchisee (or applicable party).  According to the terms herein, use of the MOLLY MAID Mirror also signifies your agreement to be legally bound by these terms and conditions.  As described below, using some features also operates as your consent to the transmission of certain standard information for Support Office purposes. 
      </p>
      <p>
        The MOLLY MAID Mirror is protected by intellectual property laws. You are granted certain limited rights to download and use the content found in the Mirror.  You acknowledge and agree not to use the MOLLY MAID Mirror in a manner that violates any corporate standards, applicable laws, regulations or this agreement.
      </p>

      <h3>CONDITIONS</h3>
      <ol>
        <li>
          <p><strong>TERM. </strong>The term of this agreement does not expire, as long as franchisee remains as an operating Franchise Business Owner.
          </p>
        </li>  
        <li>
          <p><strong>FEEDBACK. </strong>If you give feedback about the Mirror or information within this Mirror, without charge or conditions, the right to use and share your feedback in any way and for any purpose, including future modifications of the website, other products or services, advertising or marketing materials. These rights survive this agreement.
          </p>
        </li>
        <li>
          <p>
            <strong>SCOPE OF LICENSE. </strong>This site is licensed, not sold. This agreement only gives you some rights to use the website.  MOLLY MAID Canada reserves all other rights.  Unless applicable law gives you more rights despite this limitation, you may use the website only as expressly permitted in this agreement
          </p>
        </li>
        <li>
          <p>
            <strong>CONSENT TO TRANSMISSION OF DATA. </strong>MOLLY MAID Canada may collect anonymous usage data of the application in a form that does not personally identify you. MOLLY MAID Canada uses this data in aggregate to optimize products and improve services for the overall business. System administrators can disable usage data with a registry key.
          </p>
        </li>
        <li>
          <p><strong>SUPPORT SERVICES. </strong>Franchisor and franchisee are bound by legal obligation. In reference to the MOLLY MAID Mirror, Support Office will provide support services for it.
          </p>
        </li>
        <li>
          <p><strong>ENTIRE AGREEMENT. </strong>This agreement, and the terms for supplements, updates, services and support services that you use, are the entire agreement for the website and support services.
          </p>
        </li>
        <li>
          <p><strong>TERMINATION. </strong>Access to the MOLLY MAID Mirror will terminate automatically if you fail to comply with the limitations described above. On termination, you must destroy all copies of MOLLY MAID related information.
          </p>
        </li>
      </ol>

      <div class="text-right m-t-lg">
        <?php 
          echo $this->Form->create(null);
          echo $this->Form->input('terms', [
            'label' => 'Agree to Terms and Conditions',
            'required' => 'required',
            'type' => 'checkbox'
          ]);
          echo $this->Form->submit('Submit');
        ?>
      </div>

    </div>
  </div>
</div>