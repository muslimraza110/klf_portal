<div class="col-md-12 col-md-offset-6">
	<div class="ibox">
		<div class="ibox-title">
			<h5>Vision, Mission, and Core Values</h5>
			<?php if ($authUser->role == 'A'): ?>
				<div class="ibox-tools">
					<?= $this->Html->link(
						$icons['edit'].' Edit',
						['action' => 'edit_mission'],
						['escape' => false, 'class' => 'btn btn-icon btn-info']
					); ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="ibox-content">
			<?php if (!empty($mission)): ?>
				<?= $mission->content; ?>
			<?php endif; ?>
		</div>
	</div>
</div>