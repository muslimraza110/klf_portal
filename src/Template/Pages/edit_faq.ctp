<?= $this->element('ckeditor'); ?>
<?= $this->Form->create($faq); ?>
	<div class="row">
		<div class="col-md-12 col-md-offset-6">
			<div class="ibox">
				<div class="ibox-title">
					<h5>FAQ Page</h5>
				</div>
				<div class="ibox-content">
					<?= $this->Form->input('content', ['class' => 'ckeditor']); ?>
				</div>
			</div>
			<div class="submit-big">
				<?= $this->Form->submit('Save', ['bootstrap-type' => 'primary']); ?>
			</div>
		</div>
	</div>
<?= $this->Form->end(); ?>