<?php
	echo $this->Html->css('plugins/jasny/jasny-bootstrap.min');
	echo $this->Html->script([
    'plugins/jasny/jasny-bootstrap.min',
    'plugins/jasny/jasny-bootstrap.min',
  ], ['block' => true]);
	echo $this->Html->scriptBlock("
		
		$('.btn-action').click(function(e) {
			e.preventDefault();
			
			var action = $(this).attr('data-action');
			$('#form-action').val(action);
			
			$('#action-form').submit();
		});

		$('.important-tag').on('click', function(e){
			e.preventDefault();

			var messageUserId = $(this).attr('data-action');
			var selectTag = $(this);

			$.ajax({
		    type: 'POST',
		    url: '" . $this->Url->build(['controller' => 'Messages', 'action' => 'ajax_important_tag']) . "',
		    data: {
		    	id: messageUserId
		    }
		  })
		  .success(function(data){

		  	if (data['success']) {
		  		if (data['success'] == 'active') {
		  			selectTag.addClass('active');
		  		} else {
		  			selectTag.removeClass('active');
		  		}
		  	}

		  });

		});
		
	", ['block' => true]);
	
?>

<div class="row">

  <div class="col-sm-6">
    <?= $this->element('messages/sidebar'); ?>
  </div>

  <div class="col-sm-18 animated fadeInRight">
    <div class="mail-box-header">
    	<h2><?= ucwords($type); ?></h2>

    	<div class="mail-tools m-t-md">
				<div class="btn-groups pull-left">

					<?php
						if ($type != 'trash') {
							echo $this->Html->link($icons['refresh'], [], [
								'class' => 'btn btn-white',
								'escape' => false,
								'title' => 'Refresh'
							]);

							echo $this->Html->link($icons['view'], [], [
								'class' => 'btn btn-white btn-action',
								'title' => "Mark As Read",
								'escape' => false,
								'data-action' => 'read',
							]);

							echo $this->Html->link($icons['eye-slash'], [], [
								'class' => 'btn btn-white btn-action',
								'title' => "Mark As Unread",
								'escape' => false,
								'data-action' => 'unread'
							]);

							if ($type == 'archive') {
								echo $this->Html->link($icons['envelope'], [], [
									'class' => 'btn btn-white btn-action',
									'title' => "Move back to Inbox",
									'escape' => false,
									'data-action' => 'not archive'
								]);
							} else {
								echo $this->Html->link($icons['archive'], [], [
									'class' => 'btn btn-white btn-action',
									'title' => "Archive",
									'escape' => false,
									'data-action' => 'archive'
								]);
							}

							echo $this->Html->link($icons['trash'], [], [
								'class' => 'btn btn-white btn-action',
								'title' => "Move to Trash",
								'escape' => false,
								'data-action' => 'trash'
							]);

						} else {
							echo $this->Html->link($icons['history'], [], [
								'class' => 'btn btn-white btn-action',
								'title' => "Restore",
								'escape' => false,
								'data-action' => 'not trash'
							]);

							echo $this->Html->link($icons['trash'], [], [
								'class' => 'btn btn-white btn-action',
								'title' => "Delete Permernantly ",
								'escape' => false,
								'data-action' => 'delete'
							]);
						}

					?>

				</div>

				<ul class="list-unstyled list-inline pull-right list-paginator">
					<?php
						echo $this->Paginator->prev($icons['angle-left'], ['escape' => false, 'title' => 'Previous']);
						echo $this->Paginator->next($icons['angle-right'], ['escape' => false, 'title' => 'Next']);
					?>
        </ul>

      </div>
    </div><!-- mail box header -->

    <div class="mail-box">
    	<?= $this->Form->create(null, ['id' => 'action-form']); ?>

    		<table class="table table-hover table-mail">
    			<?php if(! $messages->isEmpty()): ?>
    				<tbody>
    					<?php
    						foreach ($messages as $key => $message_user) {
    							$message = $message_user->message;
    							$sender = $message->sender;
    							$sentTime = '';
    							$contacts = '';
    							$clickAction = '';
    							$activeClass = '';

    							switch($type) {
										case 'sent':
										case 'draft':
											$trClass = 'rowlink read';

											$recipients = $message->recipientList('T');
											
											if(empty($recipients))
												$recipients = '<span class="text-muted">To: ---</span>';
											else
												$recipients = '<span class="text-primary">To:</span> ' . $recipients;
									
											$cc_recipients = $message->recipientList('C');
											
											if(!empty($cc_recipients))
												$recipients .= '<br /><span class="text-primary">CC:</span> ' . $cc_recipients;

											if($type == 'draft')
												$clickAction = 'edit';
											else
												$clickAction = 'view_sent';
												
											$contacts = $this->Html->link($recipients,
												['action' => $clickAction, $message->id],
												['escape' => false]
											);
					
										break;

										case 'archive':
										case 'inbox':
										default:
											$trClass = 'rowlink unread';

											if ($message_user->message_read)
												$trClass = 'rowlink read';

  										if ($type == 'archive')
												$clickAction = 'view_archive';
											else
												$clickAction = 'view';

											$contacts = $this->Html->link($this->Html->image('profile.jpg', ['class' => 'img-circle']) .
  											' ' . $sender->user->full_name,
												['action' => $clickAction, $message->id],
												['escape' => false]
											);
											
									}

    							if ($type == 'draft')
    								$sentTime = $message->created->nice();
    							else
    								$sentTime = $message->sent->nice();

    							if ($message_user->important == 1)
    								$activeClass = 'active';

    							$row = [
  									[
  										$this->Form->checkbox('messages.' . $key . '.check', ['class' => 'i-checks']) .
											$this->Form->hidden('messages.' . $key . '.id', ['value' => $message->id]),
											['class' => 'check-mail rowlink-skip']
										],
										[
											'<p data-action=' . $message_user->id . '
												class="btn btn-lg important-tag '. $activeClass .'" >'
												. $icons['exclamation-circle'] .
											'</p>',
		    							['class' => 'check-mail rowlink-skip']
										],
  									[
  										$contacts,
											['class' => 'mail-contact']
  									] ,
  									[
											$this->Html->link($message->subject, ['action' => 'view', $message->id]),
											['class' => 'mail-subject']
										],
										[
											$sentTime,
											['class' => 'text-right mail-date']
										]
  								];
								
									echo $this->Html->tableCells(
										[$row],
										['class' => $trClass, 'data-link' => "row"],
										['class' => $trClass, 'data-link' => "row"]
									);
    						}
  
							?>
    				</tbody>
    			<?php endif; ?>

    		</table>

    		<?= $this->Form->input('Form.action', ['type' => 'hidden', 'value' => '']); ?>

    	<?= $this->Form->end(); ?>

			<div class="mail-box-footer">
				<?= $this->element('pagination'); ?>
			</div>
    </div><!-- mail box -->

  </div>

</div>
