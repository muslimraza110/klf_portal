<div class="row">

  <div class="col-sm-6">
    <?= $this->element('messages/sidebar'); ?>
  </div>

  <div class="col-sm-18 animated fadeInRight">

  	<div class="mail-box-header">
  		<?php if ($this->request->action != 'view_sent'): ?>
	  		<div class="mail-tools m-t-md">
					<div class="btn-groups pull-left">
						<?php
							echo $this->Html->link($icons['reply'],
								['action' => 'reply', $message->id],
								[
									'escape' => false,
									'class' => 'btn btn-white',
									'title' => 'Reply'
								]
							);

							echo $this->Html->link($icons['reply-all'],
								['action' => 'reply', $message->id, 'all'],
								[
									'escape' => false,
									'class' => 'btn btn-white',
									'title' => 'Reply All'
								]
							);

							echo $this->Html->link($icons['eye-slash'],
								['action' => 'mark_as_not_read', $message->id],
								[
								'class' => 'btn btn-white',
								'title' => "Mark As Unread",
								'escape' => false,
								'data-action' => 'unread'
								]
							);

							echo $this->Html->link($icons['exclamation-circle'],
								['action' => 'mark_as_important', $message->id],
								[
									'class' => 'btn btn-white',
									'title' => "Mark As Important",
									'escape' => false,
									'data-action' => 'important'
								]
							);

							if ($this->request->action == 'view_archive') {
								echo $this->Html->link($icons['envelope'],
									['action' => 'remove_from_archive', $message->id],
									[
										'escape' => false,
										'class' => 'btn btn-white',
										'title' => 'Move to Inbox'
									]
								);
							} else {
								echo $this->Html->link($icons['archive'],
									['action' => 'move_to_archive', $message->id],
									[
										'class' => 'btn btn-white',
										'title' => "Archive",
										'escape' => false,
										'data-action' => 'archive'
									]
								);
							}
						?>
					</div>
				</div>
			<?php endif; ?>

			<h3>
        <span class="font-noraml">Subject: </span><?= $message->subject; ?>
      </h3>

      <h5>
        <span class="pull-right font-noraml"><?= $message->sent->nice(); ?></span>
        <span class="font-noraml">From: </span><?= $message->sender->user->name; ?>
			</h5>

			<h5>
				<span class="font-noraml">To: </span>
				<?= $message->recipientList('T'); ?>
      </h5>

      <?php if (!empty($message->recipientList('C'))): ?>
      	<h5>
	      	<span class="font-noraml">CC: </span>
					<?= $message->recipientList('C'); ?>
				</h5>
      <?php endif; ?>
  	</div>

  	<div class="mail-box">
      <div class="mail-body" style="min-height: 200px;">
        <?= $message->content; ?>
      </div>

    </div>

  </div>
 </div>