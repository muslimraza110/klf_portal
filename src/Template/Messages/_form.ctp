<?php 
  echo $this->Html->css([
    '/js/tagit/jquery.tagit'
  ], ['block' => true]);

  echo $this->Html->script([
	  'tagit/tag-it.min',
  ], ['block' => true]);
  
  echo $this->Html->scriptBlock('
  	$(function() {
			
	  	var tagitOptions = {
				allowSpaces: true,
				
				beforeTagAdded: function(event, ui) {
					var tagPieces = ui.tagLabel.split("::");
					
					if(tagPieces.length < 2) {
						$(".ui-autocomplete-input").val("");
						return false;
					}
					
					var userId = tagPieces.shift();
					if(isNaN(userId)) {
						$(".ui-autocomplete-input").val("");
						return false;
					}
				},
				afterTagAdded: function(event, ui) {
					var tagPieces = ui.tagLabel.split("::");
					var messageUserIdLabel = "";

					if (tagPieces.length == 3) 
						messageUserIdLabel = "<span style=\"display:none\">::" + tagPieces[2] + "</span>";

					//var userId = tagPieces.shift();
					//var userName = tagPieces.join("::");
					var userId = tagPieces[0];
					var userName = tagPieces[1];
					
					var editedTag = "<span style=\"display:none\">" + userId + "::</span>" + userName 
						+ messageUserIdLabel;

					ui.tag.find(".tagit-label").html(editedTag);
				},
				
				autocomplete: {
					source: "' . $this->Url->build(['controller' => 'Messages', 'action' => 'ajax_autocomplete_recipients']) . '",
					messages: {
						noResults: "",
						results: function(e) {
							
						}
					},
					minLength: 1,
					delay: 500,
					autoFocus: true,
					
					create: function () {
						$(this).data("ui-autocomplete")._renderItem = function( ul, item ) {
							return $("<li>")
								.append("<a data-user-id=" + item.id + ">" + 
									item.first_name + " " + item.last_name + " ( " + item.email + " )" + 
								"</a>" )
								.appendTo( ul );
				    };
          },
					
		      select: function( event, ui ) {
		 				var label = ui.item.id + "::" + ui.item.first_name + " " + ui.item.last_name;
						$(this).parents(".tagit").prev(".tagit-hidden-field").tagit("createTag", label);
		        return false;
		      },
				
				},
			};

			function tagRecipients(recipientsClass, recipientsId)
			{
				tagitOptions.autocomplete.appendTo = recipientsClass;
				$(recipientsId).tagit(tagitOptions);
				$(".ui-autocomplete-input", recipientsClass).focus();
			}

			tagRecipients(".autocomplete-recipients", "#recipients");
			tagRecipients(".autocomplete-cc-recipients", "#cc-recipients");

		});
  ', ['block' => true]);
	

  echo $this->element('ckeditor');
?>

<div class="row">
  <div class="col-sm-6">
    <?= $this->element('messages/sidebar'); ?>
  </div>
  
  <div class="col-sm-18 animated fadeInRight">
  	<div class="mail-box">
      
      <?= $this->Form->create($message); ?>
				
	      <div class="mail-body">

	        <?= $this->Form->input('recipients', [
	        	'templates' => [
            	'inputContainer' => '<div class="form-group autocomplete-recipients required {{type}}{{required}}">{{content}}</div>',
            ]
        	]); ?>

        	<?= $this->Form->input('cc_recipients', [
	        	'templates' => [
            	'inputContainer' => '<div class="form-group autocomplete-cc-recipients {{type}}">{{content}}</div>',
            ]
        	]); ?>

					<?= $this->Form->input('subject', ['autocomplete' => 'off']); ?>
	      </div>

	      <div class="mail-text h-200">
					<?= $this->Form->input('content', ['label' => false, 'class' => 'ckeditor']); ?>
	        <div class="clearfix"></div>
	      </div>

	      <div class="mail-body text-right">
					<?= $this->Form->input('id', ['type' => 'hidden']);	?>

					<div class="btn-groups">
						<?php 
							echo $this->Form->button('Save Draft', [
								'class' => 'btn btn-warning',
								'name' => 'draft-btn',
								'style' => 'margin-right: 5px;' 
							]);
							echo $this->Form->button('Send Message', [
								'class' => 'btn btn-success',
								'name' => 'send-btn'
							]);
						?>
					</div>
	      </div>

      <?= $this->Form->end() ?>
      <div class="clearfix"></div>

    </div>
  </div>
</div>