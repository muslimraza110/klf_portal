<div class="row">

	<div class="col-md-12">

		<h2>Likes</h2>

		<ul class="list-group user-list-group">

			<?php foreach ($forumPost->forum_likes as $like): ?>

				<li class="list-group-item">
					<?= $this->Html->link(
						sprintf(
							'%s %s',
							$this->Html->image($like->user->profile_thumb.'?'.microtime(), ['class' => 'img-circle']),
							$like->user->name
						),
						['controller' => 'Users', 'action' => 'view', $like->user->id],
						['escape' => false]
					) ?>
				</li>

			<?php endforeach ?>

		</ul>

	</div>

	<div class="col-md-12">

		<h2>Dislikes</h2>

		<ul class="list-group user-list-group">

			<?php foreach ($forumPost->forum_dislikes as $dislike): ?>

				<li class="list-group-item">
					<?= $this->Html->link(
						sprintf(
							'%s %s',
							$this->Html->image($dislike->user->profile_thumb.'?'.microtime(), ['class' => 'img-circle']),
							$dislike->user->name
						),
						['controller' => 'Users', 'action' => 'view', $dislike->user->id],
						['escape' => false]
					) ?>
				</li>

			<?php endforeach ?>

		</ul>

	</div>

</div>