
<?php if (!empty($fundraisingPledges)): ?>
<div class="row">
	<div class="col-md-offset-3 col-md-18">
		<?= $this->element('pagination'); ?>
		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['money'] ?></span>
				<h3>My Pledges</h3>
			</div>
			<div class="ibox-content">
				<div class="table-responsive">
					<table class="table table-condensed table-striped">
						<thead>
						<?php
							$headers = [
								'Project Name',
								$this->Paginator->sort('amount', 'Amount'),
								$this->Paginator->sort('Fundraisings.fundraising_status', 'Status'),
								['' => ['class' => 'compact']]
							];
							echo $this->Html->tableHeaders($headers);
						?>
						</thead>
						<tbody>
						<?php
							$rows = [];
							$actions = [];
							foreach ($fundraisingPledges as $fundraisingPledge) {

									$fundraising = $fundraisingPledge->fundraising;
									$disabled = !empty($fundraisingPledge->amount_pending) || $fundraising->blocks_pending !== null ? true : false;

									if ($fundraisingPledge->admin_response == 'D') {
										$actions = [
											'Request Denied.'
										];
										$actionsClass = 'text-center badge-danger actions';
									} elseif ($fundraisingPledge->admin_response == 'A') {
										$actions = [
											'Request Approved.'
										];
										$actionsClass = 'text-center badge-primary actions';
									} else {
										$actions = [
											$this->Html->link(
											'Request Change',
											['controller' => 'Fundraisings', 'action' => 'edit_pledge', $fundraisingPledge->id],
											[
												'escape' => false,
												'target' => '_blank',
												'class' => 'btn btn-sm btn-info',
												'disabled' => $disabled
											])
										];
										$actionsClass = 'actions';
									}

									$rows = [];

									$rows[] = [
										$fundraising->name,
										$this->Number->currency($fundraisingPledge->amount),
										$fundraising->fundraisingStatus(),
										[implode(' ', $actions),['class' => $actionsClass]]
									];

									echo $this->Html->tableCells($rows);

								}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<?= $this->element('pagination'); ?>
	</div>
</div>
<?php endif; ?>
