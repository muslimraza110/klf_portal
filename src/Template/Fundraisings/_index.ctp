<?php

$scriptBlock = <<<JS

$(function () {
	
	$('#status').change(function () {
		$('#status-form').submit();
	});
	
});

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>

<?= $this->Form->create(null, ['type' => 'get', 'id' => 'status-form']) ?>
<div class="row">
	<div class="col-xs-3 pull-right">
		<?= $this->Form->input('status', [
			'label' => false,
			'options' => [
				'A' => 'Active',
				'R' => 'Archived'
			]
		]) ?>
	</div>
</div>
<?= $this->Form->end() ?>

<?= $this->element('pagination', ['noPadding' => true]); ?>

<br>

<table class="table table-bordered">
	<thead>
	<?php

	$headers = [
		$this->Paginator->sort('name'),
		$this->Paginator->sort('Creators.first_name', 'Creator'),
		$this->Paginator->sort('amount', 'Target'),
		$this->Paginator->sort('type'),
		$this->Paginator->sort('Initiatives.name', 'Initiative'),
		$this->Paginator->sort('created'),
		''
	];

	echo $this->Html->tableHeaders($headers);

	?>
	</thead>
	<tbody>
	<?php

	if (!$fundraisings->isEmpty())
		foreach ($fundraisings as $fundraising) {

			$actions = [];

			$actions[] = $this->Html->link(
				'View',
				['action' => 'view', $fundraising->id],
				['class' => 'btn btn-sm btn-primary']
			);

			if (in_array($authUser->role, ['A', 'O'])) {

				if ($fundraising->status != 'R') {

					$actions[] = $this->Html->link(
						'Edit',
						['action' => 'edit', $fundraising->id],
						['class' => 'btn btn-sm btn-primary']
					);

					$actions[] = $this->Form->postLink(
						'Archive',
						['action' => 'archive', $fundraising->id],
						[
							'class' => 'btn btn-sm btn-info',
							'confirm' => 'Please confirm that by archiving this Fundraising, you will also be archiving a dependent Initiative: "' . (isset($fundraising->initiative->name) ? $fundraising->initiative->name : '--') . '"'
						]
					);

				}

				$actions[] = $this->Form->postLink(
					'Delete',
					['action' => 'delete', $fundraising->id],
					[
						'class' => 'btn btn-sm btn-danger',
						'confirm' => 'Are you sure you want to delete this fundraising?'
					]
				);

			}

			$targetPercent = $fundraising->amount == 0 ? 0 : round(($fundraising->pledgedAmount * 100) / $fundraising->amount, 2);

			if ($targetPercent > 100) {
				$targetPercent = 100;
			}

			$amount = <<<HTML
<strong>{$this->Number->format($fundraising->amount, ['places' => 0, 'before' => 'USD '])}</strong>
<div class="progress" style="margin-bottom: 0; border: 1px solid #aaa;">
	<div style="width: $targetPercent%" class="progress-bar progress-bar-success"></div>
</div>
HTML;

			$row = [
				$this->Html->link(
					$fundraising->name,
					['action' => 'view', $fundraising->id],
					['escape' => false]
				),
				$this->Html->link(
					$fundraising->user->name,
					['controller' => 'Users', 'action' => 'view', $fundraising->user->id],
					['escape' => false]
				),
				[$amount, ['class' => 'text-center']],
				$fundraising->typeLabel(),
				(!empty($fundraising->initiative) ? $fundraising->initiative->name : '--'),
				$fundraising->created,
				[implode('', $actions), ['class' => 'compact']]
			];

			echo $this->Html->tableCells($row);

		}
	else
		echo $this->Html->tableCells([[
			['No fundraising found.', ['colspan' => count($headers)]]
		]]);

	?>
	</tbody>

	<thead>
		<?= $this->Html->tableHeaders($headers); ?>
	</thead>

</table>

<?= $this->element('pagination', ['noPadding' => true]); ?>