<?php

$this->Html->script(['plugins/jquery-mask/jquery.mask.min'], ['block' => true]);

$this->Html->scriptBlock(<<<JS

var addUser = function (self) {
	
	var _users = $('#users'),
		nextKey = _users.data('next-key');
	
	var _matchFullAmount = $('.match-full-amount');
	
	if ($('#type').val() == 'M') {
		_matchFullAmount.show();
		_matchFullAmount.attr('disabled', false);
	} else {
		_matchFullAmount.attr('disabled', true);
		_matchFullAmount.hide();
	}

	_users.append(
		$('#user-prototype').html().replace(/111/g, nextKey)
	);
	
	$('#anonymous-template-'  + nextKey + ' input').iCheck({
		checkboxClass: 'icheckbox_square-green',
	});
	
	$('#hidden-amount-template-'  + nextKey + ' input').iCheck({
		checkboxClass: 'icheckbox_square-green',
	});

	_users.data('next-key', nextKey + 1);
	
	
};

var removeUser = function (self) {
	
	if (window.confirm('Are you sure you want to remove this user?')) {
		
		$(self).parents('.user').first().remove();
		
	}
	
};

$(function () {
	
	$('.component input, .user input').iCheck('destroy');
	
	$('.hide-amount').iCheck({
		checkboxClass: 'icheckbox_square-green',
	});
	
	$('.anonymous').iCheck({
		checkboxClass: 'icheckbox_square-green',
	});

	$('#type').on('change', function () {
			
			var _inputBlocks = $('#input-blocks');
			var _inputMaxBlocks = $('#input-max-blocks');
			var _userLists = $('.user-list');
			var _targetAmount = $('.target-amount');
			var _matchFullAmount = $('.match-full-amount');
			
			if ($(this).val() == 'B') {
				_inputBlocks.show();
				_inputMaxBlocks.show();
				_userLists.hide();
			} else {
				_inputBlocks.hide();
				_inputMaxBlocks.hide();
				_userLists.show();
			}
			
			if ($(this).val() == 'M') {
				_targetAmount.attr('disabled', true);
				_matchFullAmount.show();
				_matchFullAmount.attr('disabled', false);
			} else {
				_targetAmount.attr('disabled', false);
				_matchFullAmount.hide();
				_matchFullAmount.attr('disabled', true);
			}
			
		})
		.trigger('change');
	
	$('#amount, #blocks').on('input', function () {
		
		var amount = parseFloat($('#amount').cleanVal()),
			blocks = parseInt($('#blocks').val());
		
		if (amount > 0 && blocks > 0) {
			var blockSize = '(1 Block = USD ' + (Math.round((amount / blocks) * 100) / 100) + ')';
		} else {
			var blockSize = '';
		}
		
		$('#size-blocks').text(blockSize);
		
	});
	
	
	$('#users').on('focus', '.user-name', function () {
			
		$(this).autocomplete({
			source: '{$this->Url->build(['controller' => 'Users', 'action' => 'ajax_autocomplete_user_list'])}.json',
			select: function (event, ui) {
				
				var _eventTarget = $(event.target);
				
				_eventTarget.val(ui.item.label);
				_eventTarget.parents('.user-autocomplete').find('.user-id').first().val(ui.item.value);
				
				return false;
				
			}
		})
		.data('ui-autocomplete')._renderItem = function (ul, item) {
			
			return $('<li class="user-item">')
				.data('ui-autocomplete-item', item)
				.append('<a><img src="' + item.profile_thumb + '" class="img-circle pull-left">' + item.label + '</a>')
				.appendTo(ul);
			
		};
		
	});
	
	
	$('#blocks').trigger('input');
	
});

JS
	, ['block' => true]);

?>

<div id="user-prototype" style="display: none;">

	<div class="user m-b-md">

		<div class="row">

			<div class="col-md-4 text-center">
				<label>&nbsp;</label><br>
				<?= $this->Html->link('Remove', 'javascript:;', [
					'class' => 'btn btn-danger',
					'onclick' => 'removeUser(this)'
				]) ?>
			</div>

			<div class="col-md-6 user-autocomplete">

				<?= $this->Form->input('fundraising_users.111.user_name', [
					'placeholder' => 'Search for User Name',
					'class' => 'user-name',
					'templates' => [
						'input' => '<div class="ui-front" style="position: relative;"><input type="{{type}}" name="{{name}}" class="form-control{{attrs.class}}" {{attrs}} /></div>'
					]
				]) ?>

				<?= $this->Form->input('fundraising_users.111.user_id', ['type' => 'hidden', 'class' => 'user-id']); ?>

			</div>

			<div class="col-md-8">
				<?= $this->Form->input('fundraising_users.111.amount', [
					'type' => 'text',
					'prepend' => 'USD',
					'maxlength' => 15,
					'data-mask' => '#,##0',
					'data-mask-reverse' => 'true',
					'class' => 'user-amount'
				]) ?>
			</div>

			<div class="col-md-6 match-full-amount">
				<?= $this->Form->input('fundraising_users.111.match_full_amount', [
					'required' => true,
					'empty' => '--',
					'type' => 'select',
					'options' => [
						'No',
						'Yes'
					],
					'class' => 'match-full-amount',
					'title' => 'Will the person match the entire amount if the target isn\'t reached?'
				]) ?>
			</div>

		</div>

		<div class="row">

			<div id="anonymous-template-111" class="col-md-12 col-md-offset-4 anonymous-template">

				<?= $this->Form->input('fundraising_users.111.anonymous', [
					'type' => 'checkbox'
				]) ?>

			</div>

			<div id="hidden-amount-template-111" class="col-md-8 hide-amount-template">

				<?= $this->Form->input('fundraising_users.111.hide_amount', [
					'type' => 'checkbox'
				]) ?>

			</div>

		</div>

	</div>

</div>

<?= $this->Form->create($fundraising); ?>
<div class="row">

	<div class="col-md-20 col-md-offset-2">

		<div class="ibox">
			<div class="ibox-title">
				<h5>Fundraising</h5>
			</div>

			<div class="ibox-content">

				<div class="row">
					<div class="col-md-24">
						<?= $this->Form->input('name') ?>
					</div>
				</div>

				<div class="row">

					<div class="col-md-12">
						<?= $this->Form->input('type'); ?>
					</div>

					<div class="col-md-6">
						<div id="input-blocks" style="display: <?= $fundraising->type == 'B' ? 'block' : 'none'; ?>;">
							<?= $this->Form->input('blocks'); ?>
							<strong id="size-blocks"></strong>
						</div>
					</div>

					<div class="col-md-6">
						<div id="input-max-blocks" style="display: <?= $fundraising->type == 'B' ? 'block' : 'none'; ?>;">
							<?= $this->Form->input('max_blocks', [
								'type' => 'number'
							]); ?>
							<strong id="size-blocks"></strong>
						</div>
					</div>

				</div>

				<div class="row">
					<div class="col-md-24">
					<?= $this->Form->input('amount', [
						'type' => 'text',
						'label' => 'Target',
						'prepend' => 'USD',
						'maxlength' => 15,
						'data-mask' => '#,##0',
						'data-mask-reverse' => 'true',
						'class' => 'target-amount'
					]); ?>
					</div>
				</div>

			</div>
		</div>

		<div class="row">
			<div class="col-xs-16">
				<div class="ibox">
					<div class="ibox-title">
						<h5>Matchers</h5>
					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-xs-24 text-right">
								<?= $this->Html->link('Add', 'javascript:;', [
									'class' => 'btn btn-primary',
									'onclick' => 'addUser(this)'
								]) ?>
								<hr>
							</div>
							<div class="col-xs-24">
								<div id="users" data-next-key="<?= count($fundraising->fundraising_users); ?>">

									<?php if (isset($fundraising->fundraising_users)): ?>
										<?php foreach ($fundraising->fundraising_users as $key => $user): ?>

											<div class="users" data-key=$key>

												<div class="user m-b-md">

													<div class="row">

														<div class="col-md-4 text-center">
															<label>&nbsp;</label><br>
															<?= $this->Html->link('Remove', 'javascript:;', [
																'class' => 'btn btn-danger',
																'onclick' => 'removeUser(this)'
															]) ?>
														</div>

														<div class="col-md-6 user-autocomplete">

															<?= $this->Form->input('fundraising_users.' . $key . '.user.name', [
																'placeholder' => 'Search for User Name',
																'class' => 'user-name',
																'templates' => [
																	'input' => '<div class="ui-front" style="position: relative;"><input type="{{type}}" name="{{name}}" class="form-control{{attrs.class}}" {{attrs}} /></div>'
																]
															]) ?>

															<?= $this->Form->input('fundraising_users.'.$key.'.id', ['hidden' => true]) ?>

														</div>

														<div class="col-md-8">
															<?= $this->Form->input('fundraising_users.' . $key . '.amount', [
																'type' => 'text',
																'prepend' => 'USD',
																'maxlength' => 15,
																'data-mask' => '#,##0',
																'data-mask-reverse' => 'true',
																'class' => 'user-amount'
															]) ?>
														</div>

														<div class="col-md-6 match-full-amount">
															<?= $this->Form->input('fundraising_users.' . $key . '.match_full_amount', [
																'required' => true,
																'empty' => '--',
																'type' => 'select',
																'options' => [
																	'No',
																	'Yes'
																],
																'class' => 'match-full-amount',
																'title' => 'Will the person match the entire amount if the target isn\'t reached?'
															]) ?>
														</div>

													</div>

													<div class="row">

														<div class="col-md-12 col-md-offset-4 anonymous">

															<?= $this->Form->input('fundraising_users.' . $key . '.anonymous', [
																'type' => 'checkbox'
															]) ?>

														</div>

														<div class="col-md-8 hide-amount">

															<?= $this->Form->input('fundraising_users.' . $key . '.hide_amount', [
																'type' => 'checkbox'
															]) ?>

														</div>

													</div>

												</div>

											</div>

										<?php endforeach ?>
									<?php endif ?>

								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
			<div class="col-xs-8">
				<div class="ibox">
					<div class="ibox-title">
						<h5>Pledgers</h5>
					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-md-24">
								<?= $this->Form->input('allow_anonymous', ['type' => 'checkbox']) ?>
							</div>

							<div class="col-md-24">
								<?= $this->Form->input('allow_hidden_amount', [
									'type' => 'checkbox',
								]) ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="row">

	<div class="col-md-14 col-md-offset-5">
		<div class="submit-big">
			<?= $this->Form->submit('save', ['class' => 'btn btn-primary']); ?>
		</div>
	</div>

</div>