<?php
	$this->Html->script(['plugins/jquery-mask/jquery.mask.min'], ['block' => true]);
?>
<?= $this->Form->create($fundraisingPledge); ?>
<div class="row">
	<div class="col-md-offset-3 col-md-18">
		<div class="ibox">
			<div class="ibox-title ibox-icons">
				<span><?= $icons['bars']; ?></span>
				<h3>Edit Pledge</h3>
			</div>
			<div class="ibox-content">
				<h2><?= $fundraisingPledge->fundraising->name; ?></h2>
				<?php if (in_array($fundraisingPledge->fundraising->type, ['P', 'M'])): ?>
				<?= $this->Form->input('amount_pending', [
					'label' => 'New Amount',
					'prepend' => 'USD',
					'maxlength' => 15,
					'value' => $fundraisingPledge->amount,
				]); ?>
			<?php else: ?>
				<?php $range = range(0, $fundraisingPledge->fundraising->max_blocks); unset($range[0]); $range[-1] = 0; asort($range); ?>
				<?= $this->Form->input('amount_pending', [
					'label' => 'Block Amount',
					'value' => ($fundraisingPledge->fundraising->blocks /  ($fundraisingPledge->fundraising->amount / $fundraisingPledge->fundraising->getUserAmountPledged($authUser->id))),
					'options' => $range
				]); ?>
			<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<div class="submit-big">
	<?= $this->Form->submit('Save', ['class' => 'btn btn-primary']); ?>
</div>

<?= $this->Form->end(); ?>
