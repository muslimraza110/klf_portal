<?php
$this->Html->scriptBlock(<<<JS

	$('th:contains("-$"), td:contains("-$"), th:contains("($"):contains(")"), td:contains("($"):contains(")"), td:contains("-"):contains("%")').css('color', 'red');

	var adminResponse = function (self) {

		$.post(
			'{$this->Url->build(['action' => 'ajax_admin_response'])}.json',
			{
				id : $(self).data('id'),
				response : $(self).data('response')
			},
			function (response) {

				if (response.status == 'ok')
					$(self).parent().parent().hide();

			}
		);

	};
	
	$(function() {
		
		$('#v').change(function() {
			$('form').submit();
		});
		
	});


JS
	, ['block' => true]);

?>

<span class="pull-right">
	<?= $this->Form->create(null, ['type' => 'get']); ?>

	<?= $this->Form->input('v', [
		'empty' => 'Pending',
		'options' => [
			'A' => 'Approved',
			'D' => 'Denied'
		],
		'label' => false
	]); ?>

	<?= $this->Form->end(); ?>
</span>

<div class="ibox">
	<div class="ibox-title">
		<span><?= $icons['eye'] ?></span>
		<h3>Pledge Requests</h3>
	</div>
	<div class="ibox-content">
		<div class="table-responsive">
			<table class="table table-condensed table-striped">
				<thead>
				<?php
					$headers = [
						'Project Names',
						$this->Paginator->sort('Users.first_name', 'User'),
						$this->Paginator->sort('amount', 'Amount From'),
						$this->Paginator->sort('amount_pending', 'Amount To'),
						'Difference',
						$this->Paginator->sort('Fundraisings.type', 'Type'),
						$this->Paginator->sort('Fundraisings.fundraising_status', 'Status'),
						$this->Paginator->sort('created')
					];
					echo $this->Html->tableHeaders($headers);
				?>
				</thead>
				<tbody>
				<?php
					if (!$fundraisingPledges->isEmpty()) {
						$actions = [];

						foreach ($fundraisingPledges as $fundraisingPledge) {

							$fundraising = $fundraisingPledge->fundraising;
							$rows = [];

							$amountFrom = $fundraising->type == 'M' ? $this->Number->currency($fundraisingPledge->amount).' / '.$this->Number->currency($fundraising->amount) : $this->Number->currency($fundraisingPledge->amount);
							$difference = $this->Number->currency($fundraisingPledge->amount_to - $fundraisingPledge->amount);
							$amountTo = $fundraisingPledge->amount_to;

							if ($fundraisingPledge->admin_response == 'D') {
								$actions = [
									'Request Denied.'
								];
								$actionsClass = 'text-center badge-danger actions';
							} elseif ($fundraisingPledge->admin_response == 'A'){
								$actions = [
									'Request Approved.'
								];
								$actionsClass = 'text-center badge-primary actions';

								$amountTo = $fundraisingPledge->amount;
								$amountFrom = $fundraising->type == 'M' ? $this->Number->currency($fundraisingPledge->from_amount).' / '.$this->Number->currency($fundraising->from_amount) : $this->Number->currency($fundraisingPledge->from_amount);
								$difference = $this->Number->currency($amountTo - $fundraisingPledge->from_amount);

							} else {
								$actions = [
									$this->Html->link(
										'Approve',
										'javascript:;',
										[
											'class' => 'btn btn-sm btn-primary',
											'onclick' => 'adminResponse(this)',
											'data-id' => $fundraisingPledge->id,
											'data-response' => 'A'
										]),
									$this->Html->link(
										'Deny',
										'javascript:;',
										[
											'class' => 'btn btn-sm btn-danger',
											'onclick' => 'adminResponse(this)',
											'data-id' => $fundraisingPledge->id,
											'data-response' => 'D'
										]),
								];
								$actionsClass = 'actions';
							}

							$projects = '';

							foreach ($fundraising->initiative->projects as $project) {
								$projects .= $this->Html->link($project->name, [
										'action' => 'view', 'controller' => 'Projects', $project->id
									]) . '<br>';
							}

							$rows[] = [
								$projects,
								$fundraisingPledge->user->name,
								$amountFrom,
								$this->Number->currency($amountTo),
								$difference,
								$fundraising->typeLabel(),
								$fundraising->fundraisingStatus(),
								$fundraisingPledge->created->format('M d, Y'),
								[implode(' ', $actions),['class' => $actionsClass . ' text-right',]]
							];

							echo $this->Html->tableCells($rows);
						}
					} else {
						echo $this->Html->tableCells([[
							['No pledge request found.', ['colspan' => count($headers)]]
						]]);
					}
				?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="text-right">
<?= $this->element('pagination', ['noPadding' => true]); ?>
</div>
