<?php

$this->Html->scriptBlock(<<<JS

var confirmPledge = function () {
	
	return window.confirm('Are you sure you want to submit this pledge?');
	
};

$(function () {

	$('#match').on('click', function () {
		
		$('#amount').val($(this).data('match')).focus();
		
	});
	
	$('th').click(function (e) {
			sortTable($(this).data('header'));
		});
		
		function sortTable(n) {
			
			var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
			table = document.getElementById("pledges-table");
			switching = true;
			dir = "asc"; 

			while (switching) {

				switching = false;
				rows = table.getElementsByTagName("TR");

				for (i = 1; i < (rows.length - 1); i++) {

					shouldSwitch = false;

					x = $(rows[i].getElementsByTagName("TD")[n]).text();
					y = $(rows[i + 1].getElementsByTagName("TD")[n]).text();

					if (dir == "asc") {
						
						if (x.toLowerCase().indexOf('usd ') != -1) {
							if (Number(x.replace(/[^0-9]/g, '')) > Number(y.replace(/[^0-9]/g, ''))) {
								shouldSwitch = true;
								break;
							}
						} else {
							if (x.toLowerCase() > y.toLowerCase()) {
								shouldSwitch= true;
								break;
							}
						}
						
					} else if (dir == "desc") {
						
						if (x.toLowerCase().indexOf('usd ') != -1) {
							if (Number(x.replace(/[^0-9]/g, '')) < Number(y.replace(/[^0-9]/g, ''))) {
								shouldSwitch = true;
								break;
							}
						} else {
							if (x.toLowerCase() < y.toLowerCase()) {
								shouldSwitch = true;
								break;
							}
						}
						
					}
				}
				if (shouldSwitch) {

					rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
					switching = true;

					switchcount ++;   
					
				} else {

					if (switchcount == 0 && dir == "asc") {
						dir = "desc";
						switching = true;
					}
					
				}
				
			}
			
		}

});

JS
	, ['block' => true]);

?>

<div class="row">

	<div class="col-md-14 col-md-offset-5">

		<?= $this->element('fundraising_pledge', ['fundraising' => $fundraising]); ?>


		<?php if (in_array($fundraising->type, ['P', 'M']) || in_array($authUser->role, ['A', 'O'])): ?>

			<div class="ibox">
				<div class="ibox-title">
					<h3>Pledgers</h3>
				</div>
				<div class="ibox-content">

					<table class="table table-bordered" id="pledges-table">
						<thead>
						<?php

						if (in_array($authUser->role, ['A', 'O'])) {

							$headers = [
								['Name' => ['data-header' => 0, 'style' => 'cursor: pointer']],
								['Amount' => ['data-header' => 1, 'style' => 'cursor: pointer']],
								['Date' => ['data-header' => 2, 'style' => 'cursor: pointer']]
							];

						} else {

							$headers = [
								'Name',
								'Amount'
							];

						}

						echo $this->Html->tableHeaders($headers);

						?>
						</thead>
						<tbody>
						<?php

						if (!empty($fundraising->fundraising_pledges)) {

							foreach ($fundraising->fundraising_pledges as $pledge) {

								$name = $this->Html->link(
									$pledge->user->name,
									['controller' => 'Users', 'action' => 'view', $pledge->user->id],
									['escape' => false]
								);

								$amount = $this->Number->format($pledge->amount, ['places' => 0, 'before' => 'USD ']);

								if (in_array($authUser->role, ['A', 'O'])) {

									$row = [
										$name,
										$amount,
										$pledge->created
									];

								} else {

									$row = [
										empty($pledge->anonymous) ? $name : h('<Anonymous>'),
										empty($pledge->hide_amount) ? $amount : h('<Anonymous>')
									];

								}

								echo $this->Html->tableCells($row);

							}

						} else {
							echo $this->Html->tableCells([[
								['No pledgers found.', ['colspan' => count($headers)]]
							]]);
						}

						?>
						</tbody>
					</table>

				</div>
			</div>

		<?php endif; ?>

	</div>

</div>