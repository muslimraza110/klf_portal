<?php

$this->Html->script(['plugins/jquery-mask/jquery.mask.min'], ['block' => true]);

$this->Html->scriptBlock(<<<JS

var updateTotalPercent = function () {
	
	var totalPercent = 0;
	
	$('#components .component-percentage').each(function () {
		totalPercent += parseInt($(this).val());
	});
	
	$('#total-percent').text(totalPercent);
	$('#total-percent-delta').text(100 - totalPercent);
	
};

var updateComponentPercent = function (_component) {
	
	var componentAmount = 0,
		componentPercent = 0,
		target = parseFloat($('#amount').cleanVal());
		
	$(_component).find('.user-amount').each(function () {
		componentAmount += parseFloat($(this).cleanVal()) || 0;
	});
	
	if (target > 0) {
		componentPercent = Math.round((componentAmount / target) * 100);
	}
	
	$(_component).find('.component-percentage').val(componentPercent || 0);
	
	updateTotalPercent();
	
};

var addComponent = function () {
	
	var _components = $('#components'),
		nextKey = _components.data('next-key');
	
	_components.append(
		$('#component-prototype').html().replace(/000/g, nextKey)
	);
	
	_components.data('next-key', nextKey + 1);
	
};

var removeComponent = function (self) {
	
	if (window.confirm('Are you sure you want to remove this component?')) {
		
		$(self).parents('.component').first().remove();
		
		updateTotalPercent();
		
	}
	
};

var addUser = function (self) {
	
	var _component = $(self).parents('.component').first(),
		componentKey = _component.data('key'),
		_users = _component.find('.users').first(),
		nextKey = _users.data('next-key');
	
	_users.append(
		$('#user-prototype').html().replace(/000/g, componentKey).replace(/111/g, nextKey)
	);
	
	_users.data('next-key', nextKey + 1);
	
};

var removeUser = function (self) {
	
	if (window.confirm('Are you sure you want to remove this user?')) {
		
		$(self).parents('.user').first().remove();
		
	}
	
};

var submitForm =  function (password) {

	var contactList = [];
	$('form input[name^="lists"]:checked').map(function() {
		contactList.push($(this).val());
	});

	var emailReceivers = [];
	$('form input[name^="EmailLists"]:checked').map(function() {
		emailReceivers.push($(this).val());
	});

	if ((contactList.length != 0 || emailReceivers.length != 0) && !password) {
		$.post(
			'{$this->Url->build(['controller' => 'ForumPosts', 'action' => 'ajax_email_confirmation'])}',
			{
				contactList: contactList,
				emailReceivers: emailReceivers,
				model : 'Fundraising'
			},
			function (response) {
				$('#email-confirmation').html(response);
				$('#confirmation-modal').modal('show');
			}
		);
	} else {
		$('form').submit();
	}

}

$(function () {
	
	$('.component input, .user input').iCheck('destroy');
	
	$('#type')
		.on('change', function () {
			
			var _inputBlocks = $('#input-blocks');
			var _userLists = $('.user-list');
			
			if ($(this).val() == 'B') {
				_inputBlocks.show();
				_userLists.hide();
			} else {
				_inputBlocks.hide();
				_userLists.show();
			}
			
		})
		.trigger('change');
	
	$('#amount, #blocks').on('input', function () {
		
		var amount = parseFloat($('#amount').cleanVal()),
			blocks = parseInt($('#blocks').val());
		
		if (amount > 0 && blocks > 0) {
			var blockSize = '(1 Block = USD ' + (Math.round((amount / blocks) * 100) / 100) + ')';
		} else {
			var blockSize = '';
		}
		
		$('#size-blocks').text(blockSize);
		
	});
	
	$('#components')
		.delegate('select', 'change', function () {
			
			var _inputName = $(this).parents('.component').first().find('.component-name');
			
			if (_inputName.val().length < 1) {
				_inputName.val($(this).find('option:selected').text());
			}
			
		})
		.on('focus', '.user-name', function () {
			
			$(this).autocomplete({
				source: '{$this->Url->build(['controller' => 'Users', 'action' => 'ajax_autocomplete_user_list'])}.json',
				select: function (event, ui) {
					
					var _eventTarget = $(event.target);
					
					_eventTarget.val(ui.item.label);
					_eventTarget.parents('.user-autocomplete').find('.user-id').first().val(ui.item.value);
					
					return false;
					
				}
			})
			.data('ui-autocomplete')._renderItem = function (ul, item) {
				
				return $('<li class="user-item">')
					.data('ui-autocomplete-item', item)
					.append('<a><img src="' + item.profile_thumb + '" class="img-circle pull-left">' + item.label + '</a>')
					.appendTo(ul);
				
			};
			
		})
		.on('focus', '.user-amount', function () {
			
			$(this).mask('#,##0', {reverse: true});
			
		})
		.on('blur', '.user-amount', function () {
			
			updateComponentPercent($(this).parents('.component').first());
			
		});
	
	updateTotalPercent();
	
	$('#blocks').trigger('input');
	
	if ($( "#email-broadcast" ).is(':checked')) {
		$('#email').show();
	} else {
		$('#email').hide();
	}

	$('#email-broadcast').on('ifChecked', function() {
		$('#email').show();
	});

	$('#email-broadcast').on('ifUnchecked', function() {
		$('#email').hide();
	});

	$('.role').on('ifChecked', function () {

		var target = '.' + $(this).attr('name')
		$(target).iCheck('check');

	});


	$('.role').on('ifUnchecked', function () {

		var target = '.' + $(this).attr('name')
		$(target).iCheck('uncheck');

	});

	$('#savecontactlist').on('ifChanged', function () {

		if ($(this).iCheck('update')[0].checked) {
			$('#emaillists-title').attr('disabled', false);
		} else {
			$('#emaillists-title').attr('disabled', true);
			$('#emaillists-title').val('');
		}

	});

});

JS
	, ['block' => true]);

?>

<?= $this->element('ckeditor'); ?>
<div id="component-prototype" style="display: none;">

	<div class="component" data-key="000">

		<div class="row">

			<div class="col-md-4 text-center">
				<label>&nbsp;</label><br>
				<?= $this->Html->link('Remove', 'javascript:;', [
					'class' => 'btn btn-danger',
					'onclick' => 'removeComponent(this)'
				]); ?>
			</div>

			<div class="col-md-14">
				<?= $this->Form->input('fundraising_components.000.name', [
					'required' => true,
					'class' => 'component-name'
				]); ?>
			</div>

			<div class="col-md-6">
				<?= $this->Form->input('fundraising_components.000.percentage', [
					'class' => 'component-percentage',
					'append' => '%',
					'required' => true,
					'oninput' => 'updateTotalPercent()',
					'value' => 0
				]); ?>
			</div>

		</div>

		<div class="row">

			<div class="col-md-20 col-md-offset-4">

				<?= $this->Form->input('fundraising_components.000.project_id', [
					'required' => true,
					'empty' => '-'
				]); ?>

			</div>

		</div>

		<div class="row m-t-md user-list">

			<div class="col-md-20 col-md-offset-4">

				<?= $this->Html->link('Add', 'javascript:;', [
					'class' => 'btn btn-primary pull-right',
					'onclick' => 'addUser(this)'
				]) ?>

				<h3 class="m-b-lg">Users</h3>

				<div class="users" data-next-key="1">

					<div class="user m-b-md">

						<div class="row">

							<div class="col-md-4">
							</div>

							<div class="col-md-12 user-autocomplete">

								<?= $this->Form->input('fundraising_components.000.fundraising_component_users.0.user_name', [
									'placeholder' => 'Search for User Name',
									'class' => 'user-name',
									'templates' => [
										'input' => '<div class="ui-front" style="position: relative;"><input type="{{type}}" name="{{name}}" class="form-control{{attrs.class}}" {{attrs}} /></div>'
									]
								]) ?>

								<?= $this->Form->input('fundraising_components.000.fundraising_component_users.0.user_id', ['type' => 'hidden', 'class' => 'user-id']); ?>

							</div>

							<div class="col-md-8">
								<?= $this->Form->input('fundraising_components.000.fundraising_component_users.0.amount', [
									'type' => 'text',
									'prepend' => 'USD',
									'maxlength' => 15,
									'data-mask' => '#,##0',
									'data-mask-reverse' => 'true',
									'class' => 'user-amount'
								]) ?>
							</div>

						</div>

						<div class="row">

							<div class="col-md-12 col-md-offset-4">

								<?= $this->Form->input('fundraising_components.000.fundraising_component_users.0.anonymous', [
									'type' => 'checkbox'
								]) ?>

							</div>

							<div class="col-md-8">

								<?= $this->Form->input('fundraising_components.000.fundraising_component_users.0.hide_amount', [
									'type' => 'checkbox'
								]) ?>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<hr>

	</div>

</div>

<div id="user-prototype" style="display: none;">

	<div class="user m-b-md">

		<div class="row">

			<div class="col-md-4 text-center">
				<label>&nbsp;</label><br>
				<?= $this->Html->link('Remove', 'javascript:;', [
					'class' => 'btn btn-danger',
					'onclick' => 'removeUser(this)'
				]) ?>
			</div>

			<div class="col-md-12 user-autocomplete">

				<?= $this->Form->input('fundraising_components.000.fundraising_component_users.111.user_name', [
					'placeholder' => 'Search for User Name',
					'class' => 'user-name',
					'templates' => [
						'input' => '<div class="ui-front" style="position: relative;"><input type="{{type}}" name="{{name}}" class="form-control{{attrs.class}}" {{attrs}} /></div>'
					]
				]) ?>

				<?= $this->Form->input('fundraising_components.000.fundraising_component_users.111.user_id', ['type' => 'hidden', 'class' => 'user-id']); ?>

			</div>

			<div class="col-md-8">
				<?= $this->Form->input('fundraising_components.000.fundraising_component_users.111.amount', [
					'type' => 'text',
					'prepend' => 'USD',
					'maxlength' => 15,
					'data-mask' => '#,##0',
					'data-mask-reverse' => 'true',
					'class' => 'user-amount'
				]) ?>
			</div>

		</div>

		<div class="row">

			<div class="col-md-12 col-md-offset-4">

				<?= $this->Form->input('fundraising_components.000.fundraising_component_users.111.anonymous', [
					'type' => 'checkbox'
				]) ?>

			</div>

			<div class="col-md-8">

				<?= $this->Form->input('fundraising_components.000.fundraising_component_users.111.hide_amount', [
					'type' => 'checkbox'
				]) ?>

			</div>

		</div>

	</div>

</div>

<?= $this->Form->create($fundraising); ?>
<div id="email-confirmation">

</div>
<div class="row">

	<div class="col-md-16 col-md-offset-4">

		<div class="ibox">
			<div class="ibox-title">
				<h5>Fundraising</h5>
			</div>

			<div class="ibox-content">

				<div class="row">

					<div class="col-md-12">
						<?= $this->Form->input('name') ?>
					</div>
					<div class="col-md-12 m-t-lg">
						<div class="col-md-12">
							<?= $this->Form->input('broadcast', ['label' => 'Show on user Dashboard', 'class' => 'inline']) ?>
						</div>
						<div class="col-md-12">
							<?= $this->Form->input('email_broadcast', ['label' => 'Broadcast by Email', 'class' => 'inline']) ?>
						</div>
					</div>
				</div>

				<?= $this->Form->input('description'); ?>

				<div id="email">
					<div class="row">
						<div class="col-md-16">
							<?= $this->Form->input('email_subject'); ?>
							<?= $this->Form->input('email_body', [
								'class' => 'ckeditor',
								'value' => '<p><span style="font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;">Thank you for reviewing this opportunity to donate and make a difference to&nbsp;</span><strong style="font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;">'.$fundraising->fundraisingProjects(', ').'</strong><span style="font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;">. We believe that this cause is extremely worthy and request for you to donate generously.</span></p>'
							]); ?>
						</div>
						<div class="col-md-8">
							<div class="ibox">
								<div class="ibox-title ibox-icons">
									<span><?= $icons['envelope']; ?></span>
									<h3>E-Blast</h3>
								</div>
								<div class="ibox-content" style="min-height: 300px; ">
									<?php if (!empty($contactList)): ?>
										<?= $this->element('add_email_list'); ?>
									<?php endif; ?>
									<?php if (!empty($emailLists)): ?>
										<?php foreach ($emailLists as $emailList): ?>
											<?= $this->element('email_lists', ['emailList' => $emailList]); ?>
										<?php endforeach; ?>
									<?php endif; ?>
								</div>
							</div>
						</div>

					</div>
				</div>
				<?= $this->Form->input('amount', [
					'type' => 'text',
					'label' => 'Target',
					'prepend' => 'USD',
					'maxlength' => 15,
					'data-mask' => '#,##0',
					'data-mask-reverse' => 'true'
				]); ?>

				<div class="row">

					<div class="col-md-12">
						<?= $this->Form->input('type'); ?>
					</div>

					<div class="col-md-12">
						<div id="input-blocks" style="display: <?= $fundraising->type == 'B' ? 'block' : 'none'; ?>;">
							<?= $this->Form->input('blocks'); ?>
							<strong id="size-blocks"></strong>
						</div>
					</div>

				</div>

				<hr>

				<?= $this->Html->link('Add', 'javascript:;', [
					'class' => 'btn btn-primary pull-right',
					'onclick' => 'addComponent()'
				]); ?>

				<h3 class="m-b-lg">Components</h3>

				<div id="components" data-next-key="<?= count($fundraising->fundraising_components); ?>">

					<?php foreach ($fundraising->fundraising_components as $key => $component): ?>

						<div class="component" data-key="<?= $key ?>">

							<div class="row">

								<?= $this->Form->input('fundraising_components.'.$key.'.id', ['hidden' => true]); ?>

								<div class="col-md-4 text-center" style="visibility: <?= $key < 1 && $fundraising->isNew() ? 'hidden' : 'visible'; ?>;">
									<label>&nbsp;</label><br>
									<?= $this->Html->link('Remove', 'javascript:;', [
										'class' => 'btn btn-danger',
										'onclick' => 'removeComponent(this)'
									]); ?>
								</div>

								<div class="col-md-14">
									<?= $this->Form->input('fundraising_components.'.$key.'.name', [
										'required' => true,
										'class' => 'component-name'
									]); ?>
								</div>

								<div class="col-md-6">
									<?= $this->Form->input('fundraising_components.'.$key.'.percentage', [
										'class' => 'component-percentage',
										'append' => '%',
										'required' => true,
										'oninput' => 'updateTotalPercent()'
									]); ?>
								</div>

							</div>

							<div class="row">

								<div class="col-md-20 col-md-offset-4">

									<?= $this->Form->input('fundraising_components.'.$key.'.project_id', [
										'required' => true,
										'empty' => '-'
									]); ?>

								</div>

							</div>

							<div class="row m-t-md user-list">

								<div class="col-md-20 col-md-offset-4">

									<?= $this->Html->link('Add', 'javascript:;', [
										'class' => 'btn btn-primary pull-right',
										'onclick' => 'addUser(this)'
									]) ?>

									<h3 class="m-b-lg">Users</h3>

									<div class="users" data-next-key="<?= count($component->fundraising_component_users) ?>">

										<?php foreach ($component->fundraising_component_users as $userKey => $user): ?>

											<div class="user m-b-md">

												<div class="row">

													<?= $this->Form->input('fundraising_components.'.$key.'.fundraising_component_users.'.$userKey.'.id', ['hidden' => true]) ?>

													<div class="col-md-4 text-center" style="visibility: <?= $userKey < 1 && $fundraising->isNew() ? 'hidden' : 'visible' ?>;">
														<label>&nbsp;</label><br>
														<?= $this->Html->link('Remove', 'javascript:;', [
															'class' => 'btn btn-danger',
															'onclick' => 'removeUser(this)'
														]) ?>
													</div>

													<div class="col-md-12 user-autocomplete">

														<?= $this->Form->input('fundraising_components.'.$key.'.fundraising_component_users.'.$userKey.'.user_name', [
															'placeholder' => 'Search for User Name',
															'value' => !empty($user->user) ? $user->user->name : '',
															'class' => 'user-name',
															'templates' => [
																'input' => '<div class="ui-front" style="position: relative;"><input type="{{type}}" name="{{name}}" class="form-control{{attrs.class}}" {{attrs}} /></div>'
															]
														]) ?>

														<?= $this->Form->input('fundraising_components.'.$key.'.fundraising_component_users.'.$userKey.'.user_id', ['type' => 'hidden', 'class' => 'user-id']); ?>

													</div>

													<div class="col-md-8">
														<?= $this->Form->input('fundraising_components.'.$key.'.fundraising_component_users.'.$userKey.'.amount', [
															'type' => 'text',
															'prepend' => 'USD',
															'maxlength' => 15,
															'data-mask' => '#,##0',
															'data-mask-reverse' => 'true',
															'class' => 'user-amount'
														]) ?>
													</div>

												</div>

												<div class="row">

													<div class="col-md-12 col-md-offset-4">

														<?= $this->Form->input('fundraising_components.'.$key.'.fundraising_component_users.'.$userKey.'.anonymous', [
															'type' => 'checkbox'
														]) ?>

													</div>

													<div class="col-md-8">

														<?= $this->Form->input('fundraising_components.'.$key.'.fundraising_component_users.'.$userKey.'.hide_amount', [
															'type' => 'checkbox'
														]) ?>

													</div>

												</div>

											</div>

										<?php endforeach; ?>

									</div>

								</div>

							</div>

							<hr>

						</div>

					<?php endforeach; ?>

				</div>

				<div class="row">

					<div class="col-md-6 col-md-offset-18">
						<strong>Total: <span id="total-percent">100</span>%</strong> (&#916; <span id="total-percent-delta">0</span>%)
					</div>

				</div>

			</div>
		</div>

	</div>

</div>

<div class="row">

	<div class="col-md-14 col-md-offset-5">
		<div class="submit-big">
			<?= $this->Form->input('save & send',
			 [
				 'type' => 'button',
				 'label' => false,
				 'class' => 'btn btn-primary',
				 'onclick' => 'submitForm()',
			 ]); ?>
		</div>
	</div>

</div>

<?= $this->Form->end(); ?>