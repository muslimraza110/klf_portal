<?php
$fontFamily = "font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;";
$defaultFont = $fontFamily . " font-size: 14px; line-height: 20px; color: #676a6c;";
?>

<p style="<?= $defaultFont; ?>">
	<strong>Dear <?= $receiver['name'] ?></strong>,
</p>

<p>
	Your pledge request change has been updated and marked as <strong><?= ($pledgeRequest->admin_response == 'A' ? 'APPROVED' : 'DENIED') ?></strong>.
</p>

<p>Regards,</p>

<p>Khoja Portal Admin</p>

<?= $this->Html->link(
	'Link to the Initiative',
	['controller' => 'Initiatives', 'action' => 'view', $pledgeRequest['fundraising']->initiative_id, '_full' => true],
	['style' => $defaultFont . 'padding: 8px 24px; border-radius: 30px; background: #344D89; color: #FFF; text-decoration: none; font-weight: bold;']
) ?>
