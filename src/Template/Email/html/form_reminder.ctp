<?php
$fontFamily = "font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;";
$defaultFont = $fontFamily . " font-size: 14px; line-height: 20px; color: #676a6c;";
?>

<p style="<?= $defaultFont; ?>">
	Dear <?= $user->name; ?>,
</p>

<p style="<?= $defaultFont; ?>">
	This is a reminder to fill out the form:
</p>

<?= $this->Html->link(
	$form->name,
	['controller' => 'Forms', 'action' => 'view', $form->id, 'prefix' => false, '_full' => true],
	['style' => $defaultFont . 'padding: 8px 24px; border-radius: 30px; background:  #344D89; color: #FFF; text-decoration: none; font-weight: bold;']
); ?>