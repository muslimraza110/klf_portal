<?php
$fontFamily = "font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;";
$defaultFont = $fontFamily . " font-size: 14px; line-height: 20px; color: #676a6c;";
?>

<p style="<?= $defaultFont; ?>">
	Dear <?= $user->name; ?>,
</p>

<?php if ($user->vote_decision == 'Y'): ?>

	<p style="<?= $defaultFont; ?>">
		Your registration for the Khoja portal has been accepted.
	</p>

	<?= $this->Html->link(
		'Log in to the Khoja Portal',
		['controller' => 'Users', 'action' => 'login', 'prefix' => false, '_full' => true],
		['style' => $defaultFont . 'padding: 8px 24px; border-radius: 30px; background: #344D89; color: #FFF; text-decoration: none; font-weight: bold;']
	) ?>

<?php else: ?>

	<p style="<?= $defaultFont; ?>">
		Your registration for the Khoja portal has been rejected.
	</p>

<?php endif; ?>