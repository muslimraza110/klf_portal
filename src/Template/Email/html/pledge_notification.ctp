<?php
$fontFamily = "font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;";
$defaultFont = $fontFamily . " font-size: 14px; line-height: 20px; color: #676a6c;";
?>

<p style="<?= $defaultFont; ?>">
	<strong>Dear <?= $receiver['name'] ?></strong>,
</p>

<p>
	Thank you for your pledge(s) to the <strong><?= $pledgeInfo['fundraising']->name ?></strong> for <strong><?= $this->Number->format($pledgeInfo['pledge']->amount, ['places' => 0, 'before' => 'USD ']) ?></strong>.<br>
	The projects <strong><?= $pledgeInfo['projects'] ?></strong>, will greatly benefit from your donation.
</p>

<p>You have chosen to send your funds for this pledge to <strong><?= $pledgeInfo['group']->name ?></strong>.</p>

<p>Please get in-touch with <strong><?= $pledgeInfo['group']->name ?></strong> directly to make your contribution and please reference <strong><?= $pledgeInfo['fundraising']->initiative->name ?></strong>.</p>

<p>
	<strong><?= $pledgeInfo['group']->name ?></strong><br>
	Phone number: <?= !empty($pledgeInfo['group']->phone) ? $pledgeInfo['group']->phone : '-' ?><br>
	Email address: <?= !empty($pledgeInfo['group']->email) ? sprintf('<a href="mailto:%1$s">%1$s</a>', $pledgeInfo['group']->email) : '-' ?>
</p>

<p>Regards,</p>

<p>Khoja Portal Admin</p>
<br>

<?= $this->Html->link(
	'Link to the Initiative',
	['controller' => 'Initiatives', 'action' => 'view', $pledgeInfo['fundraising']->initiative->id, '_full' => true],
	['style' => $defaultFont . 'padding: 8px 24px; border-radius: 30px; background: #344D89; color: #FFF; text-decoration: none; font-weight: bold;']
) ?>
