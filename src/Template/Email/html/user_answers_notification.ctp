<?php
$fontFamily = "font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;";
$defaultFont = $fontFamily . " font-size: 14px; line-height: 20px; color: #676a6c;";
?>

<p style="<?= $defaultFont; ?>">
	Dear <?= $user->name; ?>,
</p>

<p style="<?= $defaultFont; ?> padding-bottom: 20px;">
	Form "<?= $entity->form->name; ?>" has been submitted by <?= $entity->user->name; ?>.
</p>

<?= $this->Html->link(
	__('View User Answers'),
	['controller' => 'Forms', 'action' => 'user_answers', $entity->id, 'prefix' => 'admin' , '_full' => true],
	['style' => $defaultFont . 'padding: 8px 24px; border-radius: 30px; background: #344D89; color: #FFF; text-decoration: none; font-weight: bold;']
); ?>