<?php
$orderDetailsBorderStyle = 'border-bottom: 1px solid #9f9f9f;';

$fontFamily = 'font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;';
$defaultFont = $fontFamily.' font-size: 14px; line-height: 20px; color: #676a6c;';
?>

<h2>New Order #<?= $order->number; ?></h2>

<p><strong>Delivery Details</strong></p>

<table border="0" cellpadding="3" cellspacing="0">

	<tr valign="top">
		<th style="text-align: left; width: 40%;">Name</th>
		<td style="width: 60%;">
			<?= $order->first_name; ?> <?= $order->last_name; ?>
		</td>
	</tr>

	<tr valign="top">
		<th style="text-align: left;">Email Address</th>
		<td>
			<?= $order->email; ?>
		</td>
	</tr>

	<tr valign="top">
		<th style="text-align: left;">Phone Number</th>
		<td>
			<?= $order->phone; ?>
		</td>
	</tr>

	<tr valign="top">
		<th style="text-align: left;">Address</th>
		<td>
			<?= $order->order_address->address; ?><br>
			<?= $order->order_address->city; ?>, <?= $order->order_address->province; ?> <?= $order->order_address->postal_code; ?>
		</td>
	</tr>

	<tr valign="top">
		<th style="text-align: left;">Comments</th>
		<td>
			<?= !empty($order->comments) ? nl2br($order->comments) : '-'; ?>
		</td>
	</tr>

</table>

<p><strong>Order Details</strong></p>

<table border="0" cellpadding="3" cellspacing="0">

	<tr>
		<th style="text-align: left; width: 5%;">#</th>
		<th style="text-align: left; width: 50%;">Name</th>
		<th style="text-align: right; width: 20%;">Item Price</th>
		<th style="text-align: center; width: 5%;">Qty</th>
		<th style="text-align: right; width: 20%;">Total</th>
	</tr>

	<?php foreach ($order->order_items as $key => $item): ?>

		<tr valign="top">
			<td style="<?= $orderDetailsBorderStyle; ?>">
				<?= $key + 1; ?>
			</td>
			<td style="<?= $orderDetailsBorderStyle; ?>">
				<strong><?= $item->name; ?></strong>
				<?php if (!empty($item->item_name)): ?>
					<br>&nbsp;&nbsp;&nbsp;&bull; <?= $item->item_name; ?>
				<?php endif; ?>
			</td>
			<td style="text-align: right; <?= $orderDetailsBorderStyle; ?>">
				<?= $this->Number->currency($item->price_active); ?>
			</td>
			<td style="text-align: center; <?= $orderDetailsBorderStyle; ?>">
				<?= $item->quantity; ?>
			</td>
			<td style="text-align: right; <?= $orderDetailsBorderStyle; ?>">
				<?= $this->Number->currency($item->quantity * $item->price_active); ?>
			</td>
		</tr>

	<?php endforeach; ?>

	<tr valign="top">
		<th style="text-align: right;" colspan="4">
			Subtotal
		</th>
		<td style="text-align: right;">
			<?= $this->Number->currency($order->subtotal); ?>
		</td>
	</tr>

	<?php if (!empty($order->discount_percent)): ?>

		<tr valign="top">
			<th style="text-align: right;" colspan="4">
				Promotional Code (<?= $order->discount_percent; ?>%)
			</th>
			<td style="text-align: right;">
				- <?= $this->Number->currency(round($order->subtotal * ($order->discount_percent / 100), 2)); ?>
			</td>
		</tr>

	<?php endif; ?>

	<tr valign="top">
		<th style="text-align: right;" colspan="4">
			Shipping*
		</th>
		<td style="text-align: right;">
			<?= $this->Number->currency($order->shipping); ?>
		</td>
	</tr>

	<tr valign="top">
		<th style="text-align: right; <?= $orderDetailsBorderStyle; ?>" colspan="4">
			Taxes
		</th>
		<td style="text-align: right; <?= $orderDetailsBorderStyle; ?>">
			<?= $this->Number->currency($order->taxes); ?>
		</td>
	</tr>

	<tr valign="top">
		<th style="text-align: right;" colspan="4">
			Grand Total
		</th>
		<td style="text-align: right;">
			<strong><?= $this->Number->currency($order->base_total + $order->taxes); ?></strong>
		</td>
	</tr>

</table>

<p><small>* Standard shipping via Canada Post (3-5 business days)</small></p>