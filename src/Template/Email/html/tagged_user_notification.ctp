<?php
$fontFamily = "font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;";
$defaultFont = $fontFamily . " font-size: 14px; line-height: 20px; color: #676a6c;";
?>

<p style="<?= $defaultFont; ?>">
	Dear <?= $user->name; ?>,
</p>

<?php if ($userRecommendation->vote_decision == 'Y'): ?>

	<p style="<?= $defaultFont; ?>">
		<strong><?= $userRecommendation->name; ?></strong> has been accepted to the Khoja portal.
	</p>

<?php else: ?>

	<p style="<?= $defaultFont; ?>">
		Thank you for recommending <strong><?= $userRecommendation->name; ?></strong>, unfortunately at this time the recommendation has not been approved.
		<br>
		Sincerely,
		<br>
		Khoja Leadership Forum Admin
	</p>

<?php endif; ?>