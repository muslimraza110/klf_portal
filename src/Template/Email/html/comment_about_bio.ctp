<?php
$fontFamily = "font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;";
$defaultFont = $fontFamily . " font-size: 14px; line-height: 20px; color: #676a6c;";
?>

<p style="<?= $defaultFont; ?>">
	Dear <?= $copywriter->name; ?>,
</p>

<p style="<?= $defaultFont; ?>">
	You have a comment from <strong><?= $user->name; ?></strong> about his bio.
</p>

<p style="<?= $defaultFont; ?>">
	<strong><?= $comment; ?></strong>
</p>

<?= $this->Html->link(
	'Login',
	['controller' => 'Users', 'action' => 'login', 'prefix' => false, '_full' => true],
	['style' => $defaultFont . 'padding: 8px 24px; border-radius: 30px; background: #344D89; color: #FFF; text-decoration: none; font-weight: bold;']
); ?>