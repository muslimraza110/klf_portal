<?php

$fontFamily = "font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;";
$defaultFont = $fontFamily . " font-size: 14px; line-height: 20px; color: #676a6c;";
?>

<p style="<?= $defaultFont; ?>">
	<strong>Dear <?= $receiver->name; ?></strong>,
</p>

<?= $fundraising->email_body; ?>


<?= $this->Html->link(
	'Link to the Fundraising',
	['controller' => 'Fundraisings', 'action' => 'view', 'prefix' => $receiver->prefix , $fundraising->id ],
	['style' => $defaultFont . 'padding: 8px 24px; border-radius: 30px; background: #344D89; color: #FFF; text-decoration: none; font-weight: bold;']
); ?>
