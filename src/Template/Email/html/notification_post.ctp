<?php
$fontFamily = "font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;";
$defaultFont = $fontFamily . " font-size: 14px; line-height: 20px; color: #676a6c;";
?>

<p style="<?= $fontFamily; ?>color: #9a9d9f; font-size: 12px; line-height: 16px;">
	<?= $entity->created; ?>
	<br />Post # P<?= $entity->id; ?>
</p>

<p style="<?= $defaultFont; ?>">
	Dear <?= $user->name; ?>,
</p>

<p style="<?= $defaultFont; ?>">
	<?= $this->Html->link(
		$entity->name,
		['controller' => 'Posts', 'action' => 'view', $entity->id, '_full' => true],
		['style' => $fontFamily . 'font-size: 18px; line-height: 24px; color: #334fac; text-decoration:none;']
	); ?>
</p>

<?php if (!$entity->isPdf()): ?>

	<div style="<?= $defaultFont; ?>">
		<?= $entity->content; ?>
	</div>

	<br />

	<?php if (!empty($entity->related_posts) && $entity->related_posts != '[]'): ?>
		<p style="<?= $defaultFont; ?> font-weight: bold; background: #EDF0F1; padding: 20px;">This post has additional content only available on the<br />Khoja Portal.</p>
		<br />
	<?php endif; ?>

<?php endif; ?>

<?= $this->Html->link(
	__('View Post on Khoja Portal'),
	['controller' => 'Posts', 'action' => 'view', $entity->id, '_full' => true],
	['style' => $defaultFont . 'padding: 8px 24px; border-radius: 30px; background: #344D89; color: #FFF; text-decoration: none; font-weight: bold;']
); ?>

<p style="<?= $fontFamily; ?>color: #9a9d9f; font-size: 11px; line-height: 16px;">* You will need to login first to view this link.</p>