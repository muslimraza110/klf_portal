<?php
$fontFamily = "font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;";
$defaultFont = $fontFamily . " font-size: 14px; line-height: 20px; color: #676a6c;";
?>

<p style="<?= $defaultFont; ?>">
	Dear <?= $user->name; ?>,
</p>

<p style="<?= $defaultFont; ?>">
	<strong><?= $userRecommendation->name; ?></strong> has been invited to the Khoja Portal. Please sign-in to provide your recommendation.</p>

<?= $this->Html->link(
	'View Recommendation',
	['controller' => 'UserRecommendations', 'action' => 'view', $userRecommendation->id, 'prefix' => 'user', '_full' => true],
	['style' => $defaultFont . 'padding: 8px 24px; border-radius: 30px; background: #344D89; color: #FFF; text-decoration: none; font-weight: bold;']
); ?>