<?php
$fontFamily = "font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;";
$defaultFont = $fontFamily . " font-size: 14px; line-height: 20px; color: #676a6c;";
?>

<p style="<?= $defaultFont ?> padding-bottom: 20px;">
	<?= __('Welcome to Khoja! Please click the following link to set your password:'); ?>
</p>

<?= $this->Html->link(
	__('Set My Password'),
	['controller' => 'Users', 'action' => 'set_initial_password', $user->reset_key, 'prefix' => false, '_full' => true],
	['style' => $defaultFont . 'padding: 8px 24px; border-radius: 30px; background: #344D89; color: #FFF; text-decoration: none; font-weight: bold;']
); ?>