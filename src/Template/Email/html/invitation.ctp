<?php
$fontFamily = "font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;";
$defaultFont = $fontFamily . " font-size: 14px; line-height: 20px; color: #676a6c;";
?>

<?php if (!empty($userInvitation->name)): ?>
	<p style="<?= $defaultFont; ?>">
		Dear <?= $userInvitation->name; ?>,
	</p>
<?php endif; ?>

<p style="<?= $defaultFont; ?>">
	You have been invited by <strong><?= implode(' ', array_filter([$user['first_name'], $user['middle_name'], $user['last_name']])); ?></strong> to join the Khoja portal.
</p>

<?= $this->Html->link(
	'Accept Invitation',
	['controller' => 'Users', 'action' => 'register', $userInvitation->hash, 'prefix' => false, '_full' => true],
	['style' => $defaultFont . 'padding: 8px 24px; border-radius: 30px; background: #344D89; color: #FFF; text-decoration: none; font-weight: bold;']
); ?>