<?php
$fontFamily = "font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;";
$defaultFont = $fontFamily . " font-size: 14px; line-height: 20px; color: #676a6c;";
?>

<?= $htmlContent ?>

<?php if (!empty($buttonLink)): ?>
<?= $this->Html->link(
	$buttonLabel,
	$buttonLink,
	['style' => 'padding: 8px 24px; border-radius: 30px; background: #344D89; color: #FFF; text-decoration: none; font-weight: bold;']
) ?>
<?php endif; ?>
