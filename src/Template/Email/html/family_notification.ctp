<?php
$fontFamily = "font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;";
$defaultFont = $fontFamily . " font-size: 14px; line-height: 20px; color: #676a6c;";
?>

<p style="<?= $defaultFont; ?>">
	Dear <?= $userFamily->related_user->name; ?>,
</p>

<p style="<?= $defaultFont; ?>">
	A new family relation has been added: <strong><?= $userFamily->user->name; ?></strong> is your <strong><?= $userFamily->relation; ?></strong>.
</p>

<?= $this->Html->link(
	'View Your Profile',
	['controller' => 'Users', 'action' => 'edit_profile', 'prefix' =>  $userFamily->related_user->prefix, '_full' => true, $userFamily->related_user->id ],
	['style' => $defaultFont . 'padding: 8px 24px; border-radius: 30px; background:  #344D89; color: #FFF; text-decoration: none; font-weight: bold;']
); ?>
