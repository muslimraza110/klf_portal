<?php
$fontFamily = "font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;";
$defaultFont = $fontFamily . " font-size: 14px; line-height: 20px; color: #676a6c;";
?>

<p style="<?= $defaultFont; ?>">
	Dear <?= $user->name; ?>,
</p>

<p style="<?= $defaultFont; ?>">
	<?= $this->Html->link(
		$entity->topic,
		['controller' => 'ForumPosts', 'action' => 'view', $entity->id, '_full' => true],
		['style' => $fontFamily . 'font-size: 18px; line-height: 24px; color: #334fac; text-decoration:none;']
	); ?>
</p>

<p style="<?= $defaultFont; ?>">
	<?= $entity->content; ?>
</p>

<?= $this->Html->link(
	__('View Full Post on Khoja Portal'),
	['controller' => 'ForumPosts', 'action' => 'view', $entity->id, '_full' => true],
	['style' => $defaultFont . 'padding: 8px 24px; border-radius: 30px; background: #344D89; color: #FFF; text-decoration: none; font-weight: bold;']
); ?>

<p style="<?= $fontFamily; ?>color: #9a9d9f; font-size: 11px; line-height: 16px;">* You will need to login first to view this link.</p>