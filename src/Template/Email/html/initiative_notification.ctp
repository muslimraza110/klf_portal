<?php

$fontFamily = "font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;";
$defaultFont = $fontFamily . " font-size: 14px; line-height: 20px; color: #676a6c;";
?>

<p style="<?= $defaultFont; ?>">
	<strong>Dear &lt;Name&gt;</strong>,
</p>

<?= $initiative->email_body; ?>


<?= $this->Html->link(
	'Link to the Initiative',
	['controller' => 'Initiatives', 'action' => 'view', $initiative->id],
	['style' => $defaultFont . 'padding: 8px 24px; border-radius: 30px; background: #344D89; color: #FFF; text-decoration: none; font-weight: bold;']
); ?>
