<?php
$this->Html->scriptBlock(
<<<JS

var reset = function (self) {

	$.post(
		'{$this->Url->build(['action' => 'ajax_reset_image'])}.json',
		{
			entityId: $(self).data('id'),
		},
		function (response) {
 			if (response.status == 'ok') {
				window.location.reload(true);
			}
		}
	);

}

var deleteImage = function (self) {

	$.post(
		'{$this->Url->build(['action' => 'ajax_delete_image'])}.json',
		{
			entityId: $(self).data('id'),
		},
		function (response) {
 			if (response.status == 'ok') {
				window.location.reload(true);
			}
		}
	);

}





function loadImageList(ref, id) {
	
	var _ref = $(ref);
	
	if (_ref.hasClass('busy'))
		return;
	
	if (_ref.hasClass('open')) {
		$('#folder-image-list-' + id).html('');
		_ref.removeClass('open');
		return;
	}
	
	_ref.addClass('busy');
	
	$.get('{$this->Url->build(['controller' => 'Files', 'action' => 'ajax_image_list'])}/' + id, function(data) {
		
		$('#folder-image-list-' + id).html(data);
		
		_ref.removeClass('busy');
		_ref.addClass('open');
	});
	
}

$('.folder-images').on('click', '.image-list-item', function() {
	$('#chosen-asset').val($(this).data('asset-id'));
	$('#choose-asset-form').submit();
});

JS
, ['block' => true]);

echo $this->Html->script($this->Asset->scripts('jcrop'), ['block' => true]);
echo $this->Html->css('AssetManager.jcrop');
?>

<div class="row">
	<div class="col-md-8 col-lg-12">
		
		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['upload'] ?></span>
				<h3>Upload an image (JPG or PNG)</h3>
			</div>
			<div class="ibox-content">
				<?php
					echo $this->Form->create($entity, ['type' => 'file']);
					echo $this->Form->input("$imageType.upload", ['label' => false, 'type' => 'file']);
					echo $this->Form->submit('Upload', ['bootstrap-type' => 'primary', 'class' => 'pull-right']);
					echo $this->Form->end();
				?>
			</div>
		</div>
		
		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['image'] ?></span>
				<h3>Choose an existing image</h3>
			</div>
			<div class="ibox-content">
				<?= $this->Form->create(null, ['id' => 'choose-asset-form']); ?>
				<?= $this->Form->input('chosen_asset', ['type' => 'hidden', 'id' => 'chosen-asset']); ?>
				<ol class="folder-image-list">
					<?= $this->element('recursive_upload_folders', ['folders' => $folders, 'user' => $user]); ?>
				</ol>
				<?= $this->Form->end(); ?>
			</div>
		</div>

	</div>

	<?php if (! empty($entity->$imageType)): ?>
		<div class="col-md-16 col-lg-12">
	
			<div class="ibox">
				<div class="ibox-title">
					<span><?= $icons['crop'] ?></span>
					<h3>Crop Image</h3>
				</div>
				<div class="ibox-content">
					
      		<?= $this->Form->create($entity->$imageType); ?>

					<div style="overflow: hidden">
						<div style="float: left; width: 400px; margin-right: 15px;">
							<?= $this->Asset->crop($entity->$imageType, [
								'width' => 400,
								'crop_x' => $entity->crop_x,
								'crop_y' => $entity->crop_y,
								'crop_width' => $entity->crop_width,
								'crop_height' => $entity->crop_height,
							]); ?>
						</div>
						<div style="float: left; width: 120px;">
							<?= $this->Asset->cropPreview($entity->$imageType, ['width' => 120]); ?>
						</div>
					</div>
          <?= $this->Form->submit('Save Crop', ['bootstrap-type' => 'primary', 'id' => 'crop', 'class' => 'pull-right']); ?>
      		<?= $this->Form->end(); ?>
					<div class="col-md-10">
						<?= $this->Html->link(
							'Reset Image',
							'javascript:;',
							[
								'class' => 'btn btn-md btn-info',
								'onclick' => 'reset(this)',
								'data-id' =>$entity->$imageType->id,
							]
						); ?>

						<?= $this->Html->link(
							'Delete',
							'javascript:;',
							[
								'class' => 'btn btn-md btn-danger pull-right',
								'onclick' => 'deleteImage(this)',
								'data-id' =>$entity->$imageType->id,
							]
						); ?>
					</div>
				</div>
			</div>
			
		</div>
	<?php endif; ?>
 
</div>