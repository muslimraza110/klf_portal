<?= $this->element('ckeditor'); ?>

<?= $this->Form->create($user); ?>

	<div class="row">

		<div class="col-md-12 col-md-offset-6">

			<div class="ibox">
				
				<div class="ibox-title">
					<h5>About / Biography</h5>
				</div>

				<div class="ibox-content">
					<?= $this->Form->input('user_detail.bio', ['class' => 'ckeditor', 'label' => 'Bio - Copywriter Version']); ?>
				</div>
				
				<?php if ($authUser->role == 'W'): ?>
					<div class="ibox-content">
						<?= $this->Form->input('user_detail.private_bio', ['class' => 'ckeditor', 'disabled' => true]); ?>
					</div>
				<?php endif; ?>
				
			</div>

			<div class="submit-big">
				<?= $this->Form->submit('Save', ['bootstrap-type' => 'primary']); ?>
			</div>

		</div>

	</div>



<div class="col-md-12 col-md-offset-6">
	<div class="ibox">
		<div class="ibox-title">
			<h3>Comments</h3>
		</div>
		<div class="ibox-content">

			<?php foreach ($comments as $comment):?>
				<div class="ibox">
					<div class="ibox-title">
						<h3>
							<?php

							if(!$comment->copywriter_user_id) {
								echo $comment->user->name;
							} else {
								echo 'Copywriter';
							}

							?>

							<span class="badge"> wrote at <?= $comment->created; ?></span>

						</h3>
					</div>

					<div class="ibox-content">
						<?=$comment->comment ?>
					</div>

				</div>
			<?php endforeach; ?>

		</div>

		<?= $this->Form->input('user_comments.' . count($comments) . '.comment', ['type' => 'textarea', 'value' =>'']); ?>

		<div class="submit-big">
			<?= $this->Form->submit('Save', ['bootstrap-type' => 'primary']); ?>
		</div>


	</div>
</div>


<?= $this->Form->end(); ?>