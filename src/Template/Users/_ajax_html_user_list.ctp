<ul class="list-group user-list-group">

	<?php if (!$users->isEmpty()): ?>

		<?php foreach ($users as $user): ?>

			<li class="list-group-item">
				<?= $this->Html->link(
					sprintf(
						'%s %s',
						$this->Html->image($user->profile_thumb.'?'.microtime(true), ['class' => 'img-circle']),
						$user->name
					),
					['controller' => 'Users', 'action' => 'view', $user->id],
					['escape' => false]
				); ?>

				<?php if ($model == 'ForumPostWhitelists'): ?>

					<?= $this->Html->link(
						'Add',
						'javascript:;',
						[
							'class' => 'btn btn-sm btn-primary pull-right',
							'onclick' => 'addWhitelist(this)',
							'data-id' => $user->id,
							'data-model' => 'Users'
						]
					) ?>

				<?php else: ?>

					<?= $this->Html->link(
						'Add',
						'javascript:;',
						[
							'class' => 'btn btn-sm btn-primary pull-right',
							'onclick' => 'addUser(this)',
							'data-id' => $user->id
						]
					) ?>

				<?php endif; ?>
			</li>

		<?php endforeach; ?>

	<?php else: ?>

		<li class="list-group-item disabled">
			No user found.
		</li>

	<?php endif; ?>

</ul>