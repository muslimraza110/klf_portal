<?php

$this->Html->script(['plugins/datapicker/bootstrap-datepicker'], ['block' => true]);

$this->Html->css('plugins/datapicker/datepicker3', ['block' => true]);

$scriptBlock = <<<JS

var groupList = function (term) {

	$.post(
		'{$this->Url->build(['action' => 'ajax_html_group_list'])}',
		{
			term: term
		},
		function (response) {

			$('#group-list').html(response);

		}
	);

};

var requestGroupAccess = function (self) {

	$.post(
		'{$this->Url->build(['action' => 'ajax_request_group_access'])}.json',
		{
			group_id: $(self).data('id')
		},
		function () {

			$(self).hide();

		}
	);

};

var cancelGroupAccessRequest = function (self) {
	
	$.post(
		'{$this->Url->build(['action' => 'ajax_cancel_group_access_request'])}.json',
		{
			group_id: $(self).data('id')
		},
		function () {
			
			$(self).hide();
			
		}
	);
	
};

$('.clear-comments').click(function () {

	$.post(
		'{$this->Url->build([ 'action' => 'ajax_clear_comments'])}.json',
		function (response) {

			if (response.status == 'ok') {
					$('.comments, .clear-comments').hide();
			}

		}
	);

});

$('.forum-subscribe').on('click', function (e) {
	
	e.preventDefault();
	
	var self = this;
	
	$.post(
		'{$this->Url->build(['controller' => 'ForumPosts', 'action' => 'ajax_toggle_subscription'])}.json',
		{
			id: $(self).data('id')
		},
		function (response) {
			
			$(self).hide();
			
		}
	);
	
});

$(function () {

	$('.datepicker').datepicker({
		autoclose: true,
		format: 'DD MM dd, yyyy',
		todayBtn: 'linked',
		todayHighlight: true
	});

	var filterGroupsTimeout;

	$('#filter-groups').keypress(function () {

		window.clearTimeout(filterGroupsTimeout);

		var self = this;

		filterGroupsTimeout = window.setTimeout(function () {

			groupList($(self).val());

		}, 500);

	});

	groupList();

});

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>

<?= $this->element('ckeditor') ?>

<?= $this->Form->create($user); ?>

<?php if ($this->request->action == 'edit_profile' || $authUser->role == 'A'): ?>

	<div class="profile-banner" style="background-image: url(<?= $user->banner_img_url ?>">

		<div class="profile-img-wrapper">
			<?= $this->Html->image($user->profile_img_url.'?'.microtime(true), ['width' => '200px', 'class' => 'profile-img']); ?>
			<?= $this->Html->link(
				'Change Profile Image',
				['action' => 'upload_image', $user->id, 'profile_img'],
				['escape' => false, 'class' => 'btn btn-info']
			); ?>
		</div>

		<?= $this->Html->link(
			'Change Banner Image',
			['action' => 'upload_image', $user->id, 'banner_img'],
			['escape' => false, 'class' => 'btn btn-info pull-right']
		); ?>

		<h1><?= $user->name ?></h1>
		<?= (!empty($user->user_detail->title)) ? '<p><span class="label label-default">' . $user->user_detail->title . '</span></p>' : '' ?>
		<?= (!empty($user->user_detail->description)) ? '<p>' . $user->user_detail->description . '</p>' : '' ?>

	</div>

<?php endif; ?>

<div class="row">
	<div class="col-sm-10">

	<?php if ($this->request->action == 'edit_profile' && !$user->isNew()): ?>

		<?php foreach ($categories as $category): ?>

				<div class="ibox">
					<div class="ibox-title">
						<h3><?= $category->name.' and Projects'; ?></h3>
						<div class="ibox-tools">
							<?= $this->Html->link(
								$icons['plus'],
								['controller' => 'Groups', 'action' => 'add', '?' => ['type' => $category->name{0}]],
								['escape' => false, 'class' => 'btn btn-info btn-icon']
							); ?>
						</div>
					</div>
					<?php foreach ($categorizedGroups as $name => $categorizedGroup): ?>
					<?php if ($category->name == $name): ?>
						<div class="ibox-content" style="height: 250px; overflow-y: auto">

							<?php foreach ($categorizedGroup as $group): ?>
    								<?= $this->Html->link(
    									$this->Html->image($group->profile_thumb.'?'.microtime(true), ['height' => '120px']),
    									['controller' => 'Groups', 'action' => 'view', $group->id],
    									['escape' => false, 'title' => $group->name, 'data-toggle' => 'tooltip', 'class' => 'profile-thumb']
    								); ?>
                <?php foreach ($projects as $project): ?>
                  <?php if (in_array($group->id, $project->projectGroupsIds())): ?>
										<li class="list-group-item">
											<?= $this->Html->link(
												$project->name,
	    									['controller' => 'Projects', 'action' => 'view', $project->id],
	    									['escape' => false, 'title' => $project->name]
	    								); ?>
										</li>
									<?php endif; ?>
                <?php endforeach; ?>
							<?php endforeach; ?>

							<table class="table table-condensed table-striped m-t-md">
								<thead>
								<?= $this->Html->tableHeaders([
									'Name',
									'Group Title'
								]) ?>
								</thead>
								<tbody>
								<?php

								$rows = [];

								foreach ($categorizedGroup as $group) {

									$groupTitle = $this->Form->input('user_alias.'.$group->_joinData->id, [
										'class' => 'input-sm',
										'label' => false,
										'templates' => [
											'inputContainer' => '<div class="form-group {{type}}{{required}}" style="display: inline;">{{content}}</div>'
										],
										'placeholder' => 'Title',
										'default' => $group->_joinData->user_alias
									]);

									$rows[] = [
										[$group->name, ['style' => 'width: 50%;']],
										in_array($authUser->role, ['A', 'O']) || $group->_joinData->user_id == $authUser->id ? $groupTitle : $group->_joinData->user_alias,
									];

								}

								echo $this->Html->tableCells($rows);

								?>
								</tbody>
							</table>

						</div>

					<?php endif; ?>
					<?php endforeach;?>
				</div>


		<?php endforeach; ?>
		<div class="ibox">
			<div class="ibox-title">
				<h3>Request Group Access</h3>
			</div>

			<div class="ibox-content" style="max-height: 300px; overflow-y: scroll;">

				<?= $this->Form->input(
					'filter_groups',
					[
						'placeholder' => 'Search for Group Name'
					]
				); ?>

				<div id="group-list"></div>

			</div>
		</div>

		<?php if (!empty($groupRequests)): ?>

			<div class="ibox">
				<div class="ibox-title">
					<h3>Pending Group Access Requests</h3>
				</div>

				<div class="ibox-content" style="max-height: 300px; overflow-y: scroll;">

					<ul class="list-group user-list-group">

						<?php foreach ($groupRequests as $groupRequest): ?>

							<li class="list-group-item">
								<?= $this->Html->link(
									sprintf(
										'%s %s',
										$this->Html->image($groupRequest->group->profile_thumb.'?'.microtime(true)),
										$groupRequest->group->name
									),
									['controller' => 'Groups', 'action' => 'view', $groupRequest->group->id],
									['escape' => false]
								); ?>

								<?= $this->Html->link(
									'Cancel Access Request',
									'javascript:;',
									[
										'class' => 'btn btn-sm btn-danger pull-right',
										'onclick' => 'cancelGroupAccessRequest(this)',
										'data-id' => $groupRequest->group->id
									]
								); ?>
							</li>

						<?php endforeach; ?>

					</ul>

				</div>

			</div>

		<?php endif; ?>

	<?php endif; ?>

		<?php if (in_array($authUser->role, ['A'])): ?>

			<div class="ibox">
				<div class="ibox-title">
					<h3>Groups</h3>
				</div>

				<div class="ibox-content" style="max-height: 300px; overflow-y: scroll;">

					<!-- Manual hidden field -->
					<input type="hidden" name="groups[_ids]" value="">

					<?php foreach ($groups as $name => $group): ?>

						<h5><?= $name; ?></h5>

						<?= $this->Form->input('groups._ids', [
							'label' => false,
							'multiple' => 'checkbox',
							'hiddenField' => false,
							'options' => $group
						]); ?>

					<?php endforeach; ?>

				</div>
			</div>

		<?php endif; ?>

		<?php if (!$user->isNew()): ?>

			<div class="ibox">
				<div class="ibox-title">
					<h3>Family</h3>

					<div class="ibox-tools">
						<?= $this->Html->link(
							$icons['plus'],
							['action' => 'family', $user->id],
							['escape' => false, 'class' => 'btn btn-info btn-icon']
						); ?>
					</div>
				</div>

				<div class="ibox-content">

					<table class="table table-condensed table-family">

						<tbody>
						<?php

						if (!empty($user->user_families))
							foreach ($user->user_families as $family) {

								$actions = [];

								$actions[] = $this->Html->link(
									$icons['edit'],
									['action' => 'family', $user->id, $family->id],
									['escape' => false, 'class' => 'btn btn-sm btn-primary btn-icon']
								);

								$actions[] = $this->Html->link(
									$icons['delete'],
									['action' => 'delete_family', $user->id, $family->id],
									[
										'escape' => false,
										'class' => 'btn btn-sm btn-danger btn-icon',
										'confirm' => 'Are you sure you want to delete this relation?'
									]
								);

								$row = [
									sprintf('<strong>%s</strong> of', $family->relation),
									$this->Html->link(
										sprintf(
											'%s %s',
											$this->Html->image($family->related_user->profile_thumb.'?'.microtime(true), ['class' => 'img-circle']),
											$family->related_user->name
										),
										['action' => 'view', $family->related_user->id],
										['escape' => false]
									),
									[implode(' ', $actions), ['class' => 'actions text-right']]
								];

								echo $this->Html->tableCells($row);

							}
						else
							echo $this->Html->tableCells([[
								['No relations found.', ['colspan' => 3]]
							]]);

						?>
						</tbody>

					</table>

				</div>
			</div>

		<?php endif; ?>

		<?php
			if ($this->request->action == 'edit_profile')
				echo $this->element('user_notification_table');
		?>

		<?php if (!empty($user->forum_post_subscriptions)): ?>

			<div class="ibox">
				<div class="ibox-title">
					<h3>Forum Subscriptions</h3>
				</div>

				<div class="ibox-content">

					<table class="table table-condensed">
						<tbody>
						<?php

						foreach ($user->forum_post_subscriptions as $subscription) {

							$row = [
								$this->Html->link(
									$subscription->forum_post->topic,
									['controller' => 'ForumPosts', 'action' => 'view', $subscription->forum_post_id]
								),
								[
									$this->Html->link(
										'Unsubscribe',
										'#',
										[
											'class' => 'btn btn-sm btn-primary forum-subscribe',
											'data-id' => $subscription->forum_post_id
										]
									),
									['class' => 'actions text-right']
								]
							];

							echo $this->Html->tableCells($row);

						}

						?>
						</tbody>

					</table>

				</div>
			</div>

		<?php endif ?>

	</div>
	<div class="col-sm-14">

		<div class="ibox">
			<div class="ibox-title">
				<h3>Main Information</h3>
			</div>
			<div class="ibox-content">

				<div class="row">
					<div class="col-md-3">
						<?= $this->Form->input('title', [
							'options' => $userTitles,
							'empty' => '-'
						]); ?>
					</div>
					<div class="col-md-6">
						<?= $this->Form->input('first_name'); ?>
					</div>
					<div class="col-md-6">
						<?= $this->Form->input('middle_name'); ?>
					</div>
					<div class="col-md-9">
						<?= $this->Form->input('last_name'); ?>
					</div>
				</div>

				<?= $this->Form->input('gender', [
					'options' => $genders,
					'empty' => '-'
				]); ?>

				<div class="row">

					<div class="col-md-12">
						<?= $this->Form->input('email', ['label' => 'Primary Email']); ?>
					</div>

					<div class="col-md-12">
						<?= $this->Form->input('user_detail.email2', ['label' => 'Secondary Email']); ?>
					</div>

				</div>
				<div class="row">

					<div class="col-md-8">
						<?= $this->Form->input('user_detail.website', ['prepend' => 'http://']); ?>
					</div>

					<div class="col-md-8">
						<?= $this->Form->input('user_detail.facebook'); ?>
					</div>

					<div class="col-md-8">
						<?= $this->Form->input('user_detail.linkedin'); ?>
					</div>

				</div>

				<hr>

				<?= $this->Form->input('user_detail.title', ['label' => 'Business Title']); ?>

				<?= $this->Form->input('user_detail.description', [
					'type' => 'textarea',
					'rows' => 3,
					'help' => 'Maximum 250 characters'
				]); ?>

				<?php if (in_array($authUser->role, ['A']) && $user->id != $authUser->id): ?>

					<hr>

					<div class="row">

						<div class="col-md-12">
							<?= $this->Form->input('role', [
								'options' => $roles,
								'default' => 'C',
								'type' => 'radio'
							]); ?>
						</div>

						<div class="col-md-12">
							<?= $this->Form->label('pending', 'Status'); ?>
							<?= $this->Form->input('pending'); ?>
						</div>

					</div>

				<?php endif; ?>

				<?php if ($this->request->action == 'add'): ?>
					<div class="form-group switch-container m-t-lg">
						<?= $this->Form->input('send_email', ['type' => 'hidden', 'value' => '{{contact.send_email}}']); ?>
						<switch id="send-email" name="send_email" ng-model="contact.send_email"></switch>
						<label for="send-email">Send "Welcome Email" to user</label>
					</div>
				<?php endif; ?>

			</div>
		</div>

		<?php if ($this->request->action == 'add'): ?>

			<div class="ibox">
				<div class="ibox-title">
					<h3>Password</h3>
				</div>

				<div class="ibox-content">

					<div class="row">
						<div class="col-md-12">
							<?=
							$this->Form->input('password', [
								'label' => 'New Password',
								'autocomplete' => 'off'
							]);
							?>
						</div>
						<div class="col-md-12">
							<?=
							$this->Form->input('confirm_password', [
								'type' => 'password',
								'autocomplete' => 'off'
							]);
							?>
						</div>
					</div>

					<p>
						<small class="text-danger">
							* The password must contain at least 6 characters, including one capital letter and one number.
						</small>
					</p>

				</div>
			</div>

		<?php endif; ?>

		<?php if ($user->role == 'O' && in_array($authUser->role, ['A'])): ?>

			<div class="ibox">
				<div class="ibox-title">
					<h3>Governing Board of Trustees</h3>
				</div>

				<div class="ibox-content">

					<?= $this->Form->input('expiration_date', [
						'type' => 'text',
						'class' => 'datepicker',
						'value' => !empty($user->expiration_date) ? $user->expiration_date->format('l F j, Y') : ''
					]); ?>

				</div>
			</div>

		<?php endif; ?>

		<div class="ibox">
			<div class="ibox-title">
				<h3>Publicly Viewable Contact Address</h3>
			</div>
			<div class="ibox-content">

				<?= $this->Form->input('user_detail.address'); ?>
				<?= $this->Form->input('user_detail.address2', ['label' => false]); ?>

				<div class="row">
					<div class="col-sm-6">
						<?= $this->Form->input('user_detail.city'); ?>
					</div>
					<div class="col-sm-6">
						<?= $this->Form->input('user_detail.postal_code'); ?>
					</div>
					<div class="col-sm-6">
						<?= $this->Form->input('user_detail.region', ['label' => 'Province/State']); ?>
					</div>
					<div class="col-sm-6">
						<?= $this->Form->input('user_detail.country'); ?>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<?= $this->Form->input('user_detail.phone', ['label' => 'Primary Phone']); ?>
					</div>
					<div class="col-sm-12">
						<?= $this->Form->input('user_detail.phone2', ['label' => 'Secondary Phone']); ?>
					</div>
				</div>

			</div>
		</div>
		<?php if ($user->id == $authUser->id ||  $authUser->role == 'A' ): ?>
				<div class="ibox">
	        <p><i>&nbsp;A copywriter will have access to edit your bio.</i></p>
					<div class="ibox-title">
						<h3>Private Bio</h3>
					</div>
					<div class="ibox-content">
						<?= $this->Form->input('user_detail.private_bio', ['class' => 'ckeditor']); ?>
					</div>
				</div>
				<div class="ibox">
					<div class="ibox-title">
						<h3>About You</h3><br><br>
						<p>Copywriter version</p>
					</div>
					<div class="ibox-content">
						<?= $this->Form->input('user_detail.bio', ['class' => 'ckeditor', 'disabled' => true]); ?>
					</div>
				</div>
				<div class="ibox">
					<div class="ibox-title">
						<h3>Comments to change your public profile to copywriter</h3>
						<?php if (! empty($comments)): ?>
							<div class="ibox-tools">
								<?= $this->Html->link(
									$icons['delete'].'Clear Comments',
									'javascript:;',
									['escape' => false, 'class' => 'btn btn-danger clear-comments', 'data-id' => $user->id]
								); ?>
							</div>
						<?php endif; ?>
							<div class="ibox-content">
								<div class="comments">
									<?php foreach ($comments as $comment): ?>
										<div class="ibox">
										 	<div class="ibox-title">
												<h3><?= $user->name; ?><span class="badge"> wrote at <?= $comment->created; ?></span></h3>
											</div>
											<div class="ibox-content">
												<?= $comment->comment; ?>
											</div>
										</div>
								  <?php endforeach; ?>
								</div>
								<?= $this->Form->input('user_comments.0.comment', ['type' => 'textarea', 'value' =>'']); ?>
							</div>
					</div>
				</div>
			<?php endif; ?>


</div>


<div class="submit-big">
	<?= $this->Form->submit('Save', ['bootstrap-type' => 'primary']); ?>
</div>

<?= $this->Form->end() ?>
