<?= $this->Form->create($user); ?>

<div class="middle-box">
  <div class="ibox">
    <div class="ibox-content">
      
      <?= $this->Html->image('logo.png', [
        'alt' => 'Khoja',
        'id' => 'login-logo'
      ]); ?>
			
			<?= $this->Flash->render(); ?>
      
      <?= $this->Form->input('email', [
        'disabled' => 'disabled',
      ]); ?>
      
      <?= $this->Form->input('password', [
        'label' => 'New Password',
        'required' => 'required',
        'autocomplete' => 'off'
      ]); ?>
      
      <?= $this->Form->input('confirm_password', [
        'type' => 'password',
        'label' => 'Confirm your new password',
        'required' => 'required',
      ]); ?>

      <p><span class="text-danger">* The password must be between 8 and 24 characters and must include at least 1 capital and lowercase letter and 1 number. </span></p>
      <br />

      <?= $this->Form->submit('Save Password', ['bootstrap-type' => 'primary', 'class' => 'btn-block']); ?>
			
		</div>
	</div>
</div>

<?= $this->Form->end(); ?>


