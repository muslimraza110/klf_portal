<?= $this->Form->create($user); ?>

	<div class="row">

		<div class="col-md-12 col-md-offset-6">

			<div class="ibox">
				<div class="ibox-title">
					<h5>Password</h5>
				</div>

				<div class="ibox-content">

					<div class="row">
						<div class="col-md-12">
							<?= $this->Form->input('password', [
								'autocomplete' => 'off'
							]); ?>
						</div>

						<div class="col-md-12">
							<?= $this->Form->input('confirm_password', [
								'type' => 'password',
								'autocomplete' => 'off'
							]); ?>
						</div>
					</div>

					<p>
						<small class="text-danger">
							* The password must contain at least 6 characters, including one capital letter and one number.
						</small>
					</p>

				</div>
			</div>

			<div class="submit-big">
				<?= $this->Form->submit('Save', ['bootstrap-type' => 'primary']); ?>
			</div>

		</div>

	</div>

<?= $this->Form->end(); ?>