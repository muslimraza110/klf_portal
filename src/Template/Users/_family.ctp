<?php

$scriptBlock = <<<JS

$(function () {
	
	$('#relation').change(function () {
		
		var inputCustom = $('.input-custom');
		
		if ($(this).val() == 'Custom')
			inputCustom.show();
		else
			inputCustom.hide();
		
	});
	
	$('#name').autocomplete({
		appendTo: '#name-container',
		source: '{$this->Url->build(['action' => 'ajax_autocomplete_user_list'])}.json',
		select: function (event, ui) {
			
			$('#name').val(ui.item.label);
			$('#related-user-id').val(ui.item.value);
			
			return false;
			
		}
	})
	.data('ui-autocomplete')._renderItem = function (ul, item) {
		
		return $('<li class="user-item">')
			.data('ui-autocomplete-item', item)
			.append('<a><img src="' + item.profile_thumb + '" class="img-circle pull-left">' + item.label + '</a>')
			.appendTo(ul);
		
	};
	
});

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>

<?= $this->Form->create($userFamily); ?>

	<div class="row">

		<div class="col-md-12 col-md-offset-6">

			<div class="ibox">
				<div class="ibox-title">
					<h5>Family</h5>
				</div>

				<div class="ibox-content" style ="min-height:400px;">

					<div class="row">

						<div class="col-md-12">

							<?= $this->Form->input('relation'); ?>

						</div>

						<div class="col-md-12 input-custom" style="<?= $this->request->data('relation') != 'Custom' ? 'display: none;' : ''; ?>">

							<?= $this->Form->input('custom'); ?>

						</div>

					</div>

					<h3 class="text-center">of</h3>

					<?= $this->Form->input('name', [
						'placeholder' => 'Search for Contact Name',
						'required' => true,
						'disabled' => !$userFamily->isNew(),
						'templates' => [
							'input' => '<div id="name-container" style="position: relative;"><input type="{{type}}" name="{{name}}" class="form-control{{attrs.class}}" {{attrs}} /></div>'
						]
					]); ?>

					<?= $this->Form->input('related_user_id', ['type' => 'hidden']); ?>

				</div>
			</div>

			<div class="submit-big">
				<?= $this->Form->submit('Save', ['bootstrap-type' => 'primary']); ?>
			</div>

		</div>

	</div>

<?= $this->Form->end(); ?>