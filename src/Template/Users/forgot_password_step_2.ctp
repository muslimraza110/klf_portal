<?= $this->Form->create($user); ?>

<div class="middle-box">
  <div class="ibox">
    <div class="ibox-content">
      
      <?= $this->Html->image('logo.png', [
        'alt' => 'Khoja',
        'id' => 'login-logo'
      ]); ?>
			
			<?= $this->Flash->render(); ?>
      
      <?= $this->Form->input('email', [
        'disabled' => 'disabled',
      ]); ?>
      
      <?= $this->Form->input('password', [
        'label' => 'New Password',
        'required' => 'required'
      ]); ?>
      
      <?= $this->Form->input('confirm_password', [
        'type' => 'password',
        'label' => 'Confirm your new password',
        'required' => 'required',
      ]); ?>
      
      <?= $this->Form->submit('Reset Password', ['bootstrap-type' => 'primary', 'class' => 'btn-block']); ?>
			
		</div>
	</div>
</div>

<?= $this->Form->end(); ?>