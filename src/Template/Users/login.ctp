<?= $this->Form->create() ?>

<div class="middle-box">
	<div class="ibox">
		<div class="ibox-content">
			
			<?= $this->Html->image('logo.png', [
        'alt' => 'Khoja',
        'id' => 'login-logo'
      ]); ?>
			
			<?= $this->Flash->render() ?>
      
      <?= $this->Form->input('email', [
        'label' => false,
        'prepend' => '@',
        'autofocus' => 'autofocus'
      ]); ?>
			
      <?= $this->Form->input('password', [
				'label' => false,
				'prepend' => $icons['lock'],
				'help' => $this->Html->link('Forgot your password?', ['action' => 'forgot_password_step_1'], ['class' => 'pull-right text-muted', 'style' => 'font-size: 0.8em;']),
        'autocomplete' => 'off'
			]); ?>
      
      <br />
			
			<?= $this->Form->submit('Login', ['bootstrap-type' => 'primary', 'class' => 'btn-block']); ?>
      
		</div>
	</div>
</div>


<?= $this->Form->end() ?>