<?php

$scriptBlock = <<<JS

$(function () {

	$('input[name=group_category_id]').iCheck('disable');
	$('#group').prop('disabled', true);

	$('#group-name').on('input', function() {
		$('#group').prop('disabled', false);
		$('input[name=group_category_id]').iCheck('enable');
	});

	$('#group-name').autocomplete({
		appendTo: '#group-name-container',
		source: '{$this->Url->build(['action' => 'ajax_autocomplete_group_list'])}.json',
		select: function (event, ui) {

			$('#group-name').val(ui.item.label);
			$('#group-id').val(ui.item.value);

			return false;
			
		}
	})
	.data('ui-autocomplete')._renderItem = function (ul, item) {
		
		return $('<li class="user-item">')
			.data('ui-autocomplete-item', item)
			.append('<a><img src="' + item.profile_thumb + '" class="img-circle pull-left">' + item.label + '</a>')
			.appendTo(ul);
		
	};
	
});

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>

<?= $this->Form->create($user); ?>

<div class="registerscreen middle-box">
	<div class="ibox">
		<div class="ibox-content">

			<?= $this->Html->image('logo.png', [
				'alt' => 'Khoja',
				'id' => 'login-logo'
			]); ?>

			<?= $this->Flash->render(); ?>

			<?php if (!$hashError): ?>

				<div class="row">
					<div class="col-md-12">
						<?= $this->Form->input('title'); ?>
					</div>
					<div class="col-md-12">
						<?= $this->Form->input('gender', [
							'options' => $genders,
							'empty' => '-'
						]); ?>
					</div>
				</div>

				<div class="row">
					<div class="col-md-8">
						<?= $this->Form->input('first_name'); ?>
					</div>
					<div class="col-md-8">
						<?= $this->Form->input('middle_name'); ?>
					</div>
					<div class="col-md-8">
						<?= $this->Form->input('last_name'); ?>
					</div>
				</div>

				<?= $this->Form->input('email', ['autofocus' => 'autofocus']); ?>

				<div class="row">

					<div class="col-md-12">

						<?= $this->Form->input('password', [
							'label' => 'Password',
							'autocomplete' => 'off'
						]); ?>

					</div>

					<div class="col-md-12">

						<?= $this->Form->input('confirm_password', [
							'type' => 'password',
							'label' => 'Confirm Password',
							'autocomplete' => 'off'
						]); ?>

					</div>

				</div>

				<hr>

				<h3 class="m-b-md text-center">Join or Create a Business or Charity</h3>

				<p>Please search for an existing business first and, if not found, create one.</p>

				<div class="row">

					<div class="col-md-11">

						<?= $this->Form->input('group_name', [
							'label' => 'Find Group',
							'placeholder' => 'Search for Group Name',
							'templates' => [
								'input' => '<div id="group-name-container" style="position: relative;"><input type="{{type}}" name="{{name}}" class="form-control{{attrs.class}}" {{attrs}} /></div>'
							]
						]); ?>

						<?= $this->Form->input('group_id', ['type' => 'hidden']); ?>

					</div>

					<div class="col-md-2 text-center">

						<h4 style="margin-top: 30px;">or</h4>

					</div>

					<div class="col-md-11">

						<?= $this->Form->input('group', ['label' => 'Create Group', 'placeholder' => 'Group Name']); ?>

						<?= $this->Form->input('group_category_id', [
							'type' => 'radio',
							'options' => [
								2 => 'Business',
								3 => 'Charity'
							],
							'label' => 'Category'
						]); ?>

					</div>

				</div>

				<br />

				<?= $this->Form->submit('Register', ['bootstrap-type' => 'primary', 'class' => 'btn-block']); ?>

			<?php endif; ?>

		</div>
	</div>
</div>

<?= $this->Form->end(); ?>
