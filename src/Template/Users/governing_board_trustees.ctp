<?= $this->Html->scriptBlock(
	<<<JS

	$(function () {

		$('input.chairperson').on('ifClicked', function (e) {

			$.post(
				'{$this->Url->build(['action' => 'ajax_select_chairperson'])}.json',
				{
					user_id: $(this).data('id'),
					chairperson: +!e.target.checked
				}
			);

			$('input.chairperson').iCheck('uncheck');

		});

	});

JS
	, ['block' => true]); ?>

<div class="ibox">
	<div class="ibox-title">
		<h3>Governing Board Trustees</h3>
	</div>
	<div class="ibox-content">
		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
				<?php
				$headers = [
					'Name',
					'Email',
					'Chairperson'
				];
				echo $this->Html->tableHeaders($headers);
				?>
					</thead>
					<tbody>
				<?php
				if (!$gbts->isEmpty()) {
					foreach ($gbts as $gbt) {

						$row = [
							$gbt->name,
							$gbt->email,
							$this->Form->input('chairperson_'.$gbt->id, [
								'type' => 'checkbox',
								'label' => 'Chairperson',
								'class' => 'chairperson',
								'default' => $gbt->chair_person,
								'data-id' => $gbt->id
							])
						];

						echo $this->Html->tableCells($row);

					}
				} else {
					echo $this->Html->tableCells([[
						['No Governing of Board Trustees found.', ['colspan' => count($headers)]]
					]]);
				}
				?>
				</tbody>
			</table>
		</div>
	</div>
</div>