<?php
$js = <<<JS
	$(function() {

		$('.contact-box').equalizeHeights();

		$(window).resize(function() {
			$('.contact-box').equalizeHeights();
		});

		$('#pending').on('ifToggled', function () {

			$('#show-pending').submit();

		});
		
		$('#role').on('change', function () {
			
			$('#filter').submit();
			
		});

		$('.label').css('white-space', 'unset');

	});
JS;

$js .= "
	$('.follow').on('click', function(e){
		e.preventDefault();

		var userId = $(this).attr('user-id');
		var activateValue = $(this).attr('activate');
		var that = $(this);

		$.ajax({
			type: 'POST',
			url: '" . $this->Url->build(['controller' => 'NotificationSettings', 'action' => 'ajax_add']) . "',
			data: {
				user_id: userId,
				activate: activateValue
			}
		}).success(function(data){

			if (data == true) {
				if (activateValue == 'false') {
					that.text('Follow Me');
					that.removeClass('btn-default');
					that.addClass('btn-info');
					that.attr('activate', true);
				}
				else {
					that.text('Followed');
					that.removeClass('btn-info');
					that.addClass('btn-default');
					that.attr('activate', 'false');
				}
			}

		});
	});
";

echo $this->Html->scriptBlock($js, ['block' => true]);
?>

<?php if (in_array($authUser->role, ['C', 'F'])): ?>

	<div class="well">

		<div class="row">

			<div class="col-md-12 col-md-offset-6 text-center">

				<h3 class="no-margins">Recommend Contact</h3>
				<br>
				<p>Recommend a new contact to the portal.</p>

				<?= $this->Html->link(
					'Recommend',
					['controller' => 'UserRecommendations', 'action' => 'recommend'],
					['class' => 'btn btn-default']
				); ?>

			</div>

		</div>

	</div>

<?php endif; ?>

<div class="row hidden-print">

	<div class="col-md-16">
		<?= $this->Form->create(null, ['type' => 'get', 'id' => 'filter']) ?>
		<?=
		$this->Form->input('q', [
			'label' => false,
			'placeholder' => 'Search for an email or name',
			'append' => $this->Form->button(
				$icons['search'],
				['bootstrap-type' => 'info', 'class' => 'btn-icon', 'style' => 'box-shadow: none;']
			),
			'escape' => false
		])
		?>
	</div>
	<div class="col-md-4">

		<?= $this->Form->input('role', [
			'label' => false,
			'options' => $roles,
			'type' => 'select',
			'empty' => 'Filter By Role'
		]); ?>

		<?= $this->Form->end() ?>
	</div>

	<div class="col-md-4">

		<?php if (in_array($authUser->role, ['A', 'O'])): ?>

			<?= $this->Form->create(null, ['type' => 'get', 'id' => 'show-pending']); ?>

			<?= $this->Form->input('pending', [
				'type' => 'checkbox',
				'label' => 'Show Pending Only',
				'hiddenField' => false
			]); ?>

			<?= $this->Form->end(); ?>

		<?php endif; ?>

	</div>

</div>

<div class="row hidden-print">
	<div class="col-xs-24 m-b-lg">
		<div id="alphabetic-search">
			<?php
				foreach (range('A', 'Z') as $letter) {
					if (isset($this->request->data['letter']) && $this->request->data['letter'] == $letter)
						echo $this->Html->link($letter, ['action' => 'index'], ['class' => 'active']);
					else
						echo $this->Html->link($letter, ['action' => 'index', 'letter' => $letter]);
				}
			?>
		</div>
	</div>
</div>

<div class="blue-bg padding-lg" style="margin-left: -30px; margin-right: -30px;">

	<div class="row">

		<?php if (!$users->isEmpty()): foreach ($users as $user): ?>

			<div class="col-sm-12 col-lg-8">
				<div class="contact-box equal-height">

					<?php
						if ($authUser->id == $user->id) {
							echo '<div class="actions">';
							echo $this->Html->link($icons['edit'], ['action' => 'edit_profile'], [
								'escape' => false,
								'title' => 'Edit Your Profile',
								'data-toggle' => 'tooltip',
								'class' => 'btn btn-info btn-xs btn-transparent btn-icon'
							]);
							echo '</div>';
						}
						elseif (in_array($authUser->role, ['A'])) {

							$actions = $this->Html->link($icons['edit'], ['action' => 'edit', $user->id], [
								'escape' => false,
								'title' => 'Edit',
								'data-toggle' => 'tooltip',
								'class' => 'btn btn-info btn-xs btn-transparent btn-icon'
							]);

							if (in_array($authUser->role, ['A'])) {

								$actions .= $this->Form->postLink($icons['unlock'], ['action' => 'loginAs', $user->id], [
									'escape' => false,
									'title' => 'Login as ' . $user->name,
									'class' => 'btn btn-info btn-xs btn-transparent btn-icon'
								]);

								$actions .= $this->Form->postLink($icons['delete'], ['action' => 'delete', $user->id], [
									'escape' => false,
									'title' => 'Delete',
									'confirm' => 'Are you sure to delete the contact "' . $user->name . '"?',
									'class' => 'btn btn-danger btn-xs btn-transparent btn-icon'
								]);

							}

							echo '<div class="actions">' . $actions . '</div>';
						} elseif ($authUser->role == 'W')
							printf(
								'<div class="actions">%s</div>',
								$this->Html->link(
									$icons['edit'],
									['action' => 'edit_bio', $user->id],
									[
										'escape' => false,
										'title' => 'Edit Biography',
										'class' => 'btn btn-info btn-xs btn-transparent btn-icon'
									]
								)
							);

					?>

					<div class="row">
						<div class="col-md-8 text-center">
							<?php
							echo $this->Html->link(
								$this->Html->image($user->profile_img_url.'?'.microtime(true), ['height' => '120px']),
								['action' => 'view', $user->id],
								['escape' => false, 'class' => 'profile-thumb', 'title' => $user->name, 'data-toggle' => 'tooltip']
							);

							if (!empty($ribbonUrl = $user->ribbonImageUrl())) {
								printf(
									'<div class="col-xs-24" style="top: -25px; padding: 0; height:35px">%s</div>',
									$this->Html->image($ribbonUrl.'?'.microtime(true), ['width' => '120px'])
								);
								$title = $user->role == 'F' ? 'Founder' : 'Governing Board Of Trustees';
								echo '<p><span class="label label-default">'.$title.'</span></p>';
							}

							if (!empty($user->chair_person)) {
								echo '<p><span class="label label-default">Chairperson</span></p>';
							}
							
							//if ($authUser->role != 'W') {
							if (false) {

								if ($authUser->checkNotification($user->id, 'ForumPosts', true)) {
									echo $this->Html->link('Followed', '#', [
										'user-id' => $user->id,
										'class' => 'btn btn-danger btn-xs follow m-t-sm',
										'activate' => "false"
									]);
								}
								else {
									echo $this->Html->link('Follow me', '#', [
										'user-id' => $user->id,
										'class' => 'btn btn-info btn-xs follow m-t-sm',
										'activate' => 1
									]);
								}
							}
							?>
						</div>
						<div class="col-md-16">

							<h3 class="contact-name">
								<?= $this->Html->link($user->name, ['controller' => 'Users', 'action' => 'view', $user->id]); ?>
								<?php /*<span style="font-size: 22px" compile="chatCtrl.getUserStatus(<?= $user->id ?>)"></span>*/ ?>
							</h3>

							<?php if (!empty($user->pending)): ?>

								<p>
									<span class="label label-warning">Pending</span>
								</p>

							<?php endif; ?>

							<?php if (!empty($user->vote_decision) && empty($user->vote_admin_decision)): ?>

								<p>
									<strong><?= $icons['warning']; ?> Final decision pending.</strong>
								</p>

							<?php endif; ?>

							<?php if (!empty($user->user_detail->title)): ?>
								<p><span class="label label-default"><?= $user->user_detail->title ?></span></p>
							<?php endif; ?>

							<p>
								<?php if (!empty($user->user_detail->city)): ?>
									City: <?= $user->user_detail->city; ?><br>
								<?php endif; ?>

								<?php if (!empty($user->user_detail->phone)): ?>
									Phone: <?= $user->user_detail->phone; ?><br>
								<?php endif; ?>

								<?php if (!empty($user->email)): ?>
									Email: <?= $this->Html->link($user->email, 'mailto:' . $user->email, ['target' => '_blank', 'class' => 'text-primary']); ?>
								<?php endif; ?>
							</p>

						</div>
					</div>

					<?= $this->Html->link('View Profile ' . $icons['caret-right'],
						['action' => 'view', $user->id],
						['escape' => false, 'class' => 'view-profile m-t-sm']
					); ?>

				</div>
			</div>

		<?php endforeach; endif; ?>
	</div>
</div>

<div class="m-t-lg">
	<?= $this->element('pagination', ['noPadding' => true]); ?>
</div>
