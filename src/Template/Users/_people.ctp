<?php

$scriptBlock = <<<JS

$(function () {
	
	$('#show').change(function () {
		
		$(this).parents('form').first().submit();
		
	});
	
	$('#v').change(function () {
		
		$('#view-form').submit();
		
	});
	
});

JS;

$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>

<?php if ($authUser->role == 'A'): ?>

	<div class="well">

		<div class="row">

			<div class="col-md-12 col-md-offset-6 text-center">

				<h3 class="no-margins">Invite Contact</h3>
				<br>
				<p>Invite a new contact to the portal.</p>

				<?= $this->Form->create(null, ['url' => ['action' => 'invite'], 'class' => 'form-inline']); ?>

				<?= $this->Form->input('email', [
					'type' => 'text',
					'label' => false,
					'placeholder' => 'Email Address',
					'required' => true
				]); ?>

				<?= $this->Form->submit('Invite'); ?>

				<?= $this->Form->end(); ?>

			</div>

		</div>

	</div>

<?php endif; ?>

<div class="ibox">

	<div class="ibox-title">
		<h3>Pending Invitations</h3>

		<div class="ibox-tools">
			<a href="javascript:;" class="btn btn-primary collapse-link">
				<?= $icons['chevron-down'] ?>Toggle
			</a>
		</div>

	</div>

	<div class="ibox-content" style="display: none;">

		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>

				<?php

				$headers = [
					'Email',
					'Created',
					''
				];

				echo $this->Html->tableHeaders($headers);

				?>
				</thead>
				<tbody>
				<?php
				
				if (!$userInvitations->isEmpty()) {
					foreach ($userInvitations as $invitation) {
						
						$actions = [];
						$actions[] = $this->Form->postLink(
							'Delete',
							['action' => 'deleteInvitation', $invitation->id],
							[
								'class' => 'btn btn-sm btn-danger',
								'confirm' => 'Are you sure to delete the invitation "'.$invitation->email.'"?'
							]
						);
						
						$actions[] = $this->Form->postLink(
							'Resend Invitation',
							['action' => 'resendInvitationEmail', $invitation->id],
							[
								'class' => 'btn btn-sm btn-danger'
							]
						);
						
						$row = [
							$invitation->email,
							$invitation->created,
							[implode('', $actions), ['class' => 'compact']]
						];

						echo $this->Html->tableCells($row);

					} 
				} else {
					echo $this->Html->tableCells([[
						['No pending invitations found.', ['colspan' => count($headers)]]
					]]);
				}

				?>
				</tbody>
			</table>
		</div>

	</div>

</div>

<div class="ibox">

	<div class="ibox-title">
		<h3>People</h3>
		<div class="ibox-tools">
			<?= $this->Form->create(null, ['type' => 'get', 'id' => 'view-form']); ?>
				<?= $this->Form->input('v', [
					'options' => [
						'P' => 'Paginated',
						'A' => 'View All'
					],
					'default' => 'P',
					'label' => false
				]); ?>
			<?= $this->Form->end(); ?>
		</div>
	</div>

	<div class="ibox-content">

		<div class="text-right">

			<?= $this->Form->create(null, ['type' => 'get', 'class' => 'form-inline']); ?>

			<?= $this->Form->end(); ?>

		</div>

		<?= $this->element('pagination', ['noPadding' => true]); ?>

		<br>
		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>

				<?php

				$cDirection = $this->request->query('c_direction') == 'desc' ? 'asc' : 'desc';

				$headers = [
					$this->Paginator->sort('role'),
					$this->Paginator->sort('title'),
					$this->Paginator->sort('first_name'),
					$this->Paginator->sort('last_name'),
					$this->Paginator->sort('email', 'Primary Email'),
					$this->Paginator->sort('UserDetails.title', 'Title'),
					$this->Paginator->sort('created'),
					$this->Paginator->sort('last_login'),
					$this->Html->link('Business', [
						'?' => ['c_sort' => 'business_count', 'c_direction' => $cDirection]
					]),
					$this->Html->link('Charities', [
						'?' => ['c_sort' => 'charity_count', 'c_direction' => $cDirection]
					]),
					$this->Html->link('Total', [
						'?' => ['c_sort' => 'group_count', 'c_direction' => $cDirection]
					]),
					''
				];

				echo $this->Html->tableHeaders($headers);

				?>
				</thead>
				<tbody>
				<?php

				if (!$users->isEmpty())
					foreach ($users as $user) {

						$actions = [];

						$actions[] = $this->Html->link(
							'Edit',
							['action' => 'edit', $user->id],
							['class' => 'btn btn-sm btn-primary']
						);

						if (in_array($authUser->role, ['A'])) {

							$actions[] = $this->Form->postLink(
								'Log in',
								['action' => 'loginAs', $user->id],
								[
									'class' => 'btn btn-sm btn-info',
									'disabled' => $user->id == $authUser->id
								]
							);

							$actions[] = $this->Form->postLink(
								'Delete',
								['action' => 'delete', $user->id],
								[
									'class' => 'btn btn-sm btn-danger',
									'confirm' => 'Are you sure to delete the contact "'.$user->name.'"?'
								]
							);

						}

						$row = [
							$roles[$user->role],
							!in_array($user->role, ['A', 'W']) ? $user->title : '',
							$this->Html->link(
								$user->first_name,
								['action' => 'view', $user->id]
							),
							$this->Html->link(
								$user->last_name,
								['action' => 'view', $user->id]
							),
							$user->email,
							!empty($user->user_detail) ? $user->user_detail->title : '',
							$user->created,
							!empty($user->last_login) ? $user->last_login : '',
							[$user->business_count, ['class' => 'text-center']],
							[$user->charity_count, ['class' => 'text-center']],
							[$user->group_count, ['class' => 'text-center']],
							[implode('', $actions), ['class' => 'compact']]
						];

						echo $this->Html->tableCells($row);

					}
				else
					echo $this->Html->tableCells([[
						['No users found.', ['colspan' => count($headers)]]
					]]);

				?>
				</tbody>
			</table>
		</div>

		<?= $this->element('pagination', ['noPadding' => true]); ?>

	</div>

</div>