<?php
	if ($this->request->session()->check('initial_login.message')) {
		echo '<div class="alert alert-warning">' . $this->request->session()->read('initial_login.message') . '</div>';
	}
?>

<?php foreach ($users as $groupName => $userGroup): ?>
	<h2><?= $groupName ?></h2>
	<table class="table table-bordered table-with-status">
		<thead>
			<?= $this->Html->tableHeaders([
				'First Name',
				'Last Name',
				'Email',
				'Can Email'
			]); ?>
		</thead>
		<tbody>
			<?php 
				foreach ($userGroup as $user) {
					$row = [
						$user->first_name,
						$user->last_name,
						$user->email,
						($groupName == 'Emails that can be sent') ? '<span class="text-success">Right Now</span>' : '<span class="text-muted">Already done</span>'
					];
					
					echo $this->Html->tableCells($row);
				}
				
				if($groupName == 'Emails that can be sent' && !empty($usersForNextBatch)) {
					foreach ($usersForNextBatch as $user) {
						$row = [
							$user->first_name,
							$user->last_name,
							$user->email,
							'<span class="text-danger">Later</span>'
						];
						
						echo $this->Html->tableCells($row, ['class' => 'disabled'], ['class' => 'disabled']);
					}
				}
			?>
		</tbody>
	</table>
<?php endforeach; ?>

<div class="submit-big">
  <?= $this->Form->postButton('Send Emails', null, ['bootstrap-type' => 'primary']); ?>
</div>