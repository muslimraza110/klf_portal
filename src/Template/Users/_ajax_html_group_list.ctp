<ul class="list-group user-list-group">

	<?php if (!$groups->isEmpty()): ?>

		<?php foreach ($groups as $group): ?>

			<li class="list-group-item">
				<?= $this->Html->link(
					sprintf(
						'%s %s',
						$this->Html->image($group->profile_thumb.'?'.microtime(true), ['class' => 'img-circle', 'style' => 'border-radius:0px']),
						$group->name
					),
					['controller' => 'Groups', 'action' => 'view', $group->id],
					['escape' => false]
				); ?>

				<?php if ($model == 'ForumPostWhitelists'): ?>

					<?= $this->Html->link(
						'Add',
						'javascript:;',
						[
							'class' => 'btn btn-sm btn-primary pull-right',
							'onclick' => 'addWhitelist(this)',
							'data-id' => $group->id,
							'data-model' => 'Groups'
						]
					) ?>

				<?php else: ?>

					<?= $this->Html->link(
						'Request Access',
						'javascript:;',
						[
							'class' => 'btn btn-sm btn-primary pull-right',
							'onclick' => 'requestGroupAccess(this)',
							'data-id' => $group->id
						]
					) ?>

				<?php endif; ?>
			</li>

		<?php endforeach; ?>

	<?php else: ?>

		<li class="list-group-item disabled">
			No groups found.
		</li>

	<?php endif; ?>

</ul>