<?php

	echo $this->Html->css([
		'royalslider/royalslider',
		'royalslider/skins/default/rs-default',
	], ['block' => true]);

	echo $this->Html->script([
		'plugins/fullcalendar/fullcalendar.min',
		'ng-assets/controllers/calendar-controller.js?v=2',
		'ng-assets/directives/calendar/calendar-directives',
		'ng-assets/resources/calendar',
	  'royalslider/jquery.royalslider.min',
	], ['block' => true]);

	$commentForm = $this->element('forums/forum-comments', ['postId' => 'postId']);
	$commentForm = str_replace('"', '\"',$commentForm);
	$commentForm = preg_replace('/\s+/', ' ',$commentForm);
	$commentForm = trim($commentForm);

	$normalSlider = "{
		arrowsNav: false,
		fadeinLoadedSlide: true,
		controlNavigationSpacing: 0,
		controlNavigation: 'thumbnails',

		autoPlay: {
			enabled: true,
			pauseOnHover: true,
			delay: 6000
		},

		thumbs: {
			autoCenter: false,
			fitInViewport: true,
			orientation: 'vertical',
			spacing: 5,
			paddingBottom: 0,
			firstMargin: false
		},

		keyboardNavEnabled: true,
		imageScaleMode: 'fill',
		imageAlignCenter: true,
		slidesSpacing: 0,
		loop: false,
		loopRewind: true,
		numImagesToPreload: 3,

		autoScaleSlider: false,
		autoScaleSliderWidth: 1060,
		autoScaleSliderHeight: 300,
		imgHeight: 300
	}";

	$fixedSlider = "{
		arrowsNav: false,
		fadeinLoadedSlide: true,
		controlNavigationSpacing: 0,
		controlNavigation: false,
		autoPlay: {
			enabled: false
		},

		keyboardNavEnabled: false,
		imageScaleMode: 'fill',
		imageAlignCenter: true,

		autoScaleSlider: true,
		autoScaleSliderWidth: 1900,
		autoScaleSliderHeight: 600,
	}";
	
	$sliderOptions = $setting->value ? $fixedSlider : $normalSlider;

	echo $this->Html->scriptBlock(
<<<JS

		$(document).ready(function () {
			
			if ($('#home-slider').length > 0) {
				
				$('#home-slider').royalSlider({$sliderOptions});

				$('.rsTmb').on('click', function() {
					
					var currLink = $(this).attr('data-link');
					
					window.open(currLink, '_blank');
					
				});
				
			}
			
		});

		function clickLike(selector) {
			
			$(selector).find('.like-btn')
				.on('click', function() {
					
					var button = $(this);
					var totalLikesSelector = $('.total-likes', button.parent().parent()).first();
					var totalLikes = parseInt(totalLikesSelector.text());
					var url = '{$this->Url->build(['controller' => 'ForumLikes', 'action' => 'ajax_like'])}';
					
					console.log(parseInt(totalLikesSelector.text()));
					
					forumLikes(button, totalLikesSelector, totalLikes, url);
					
				});
			
		}
		
		clickLike('.btn-group');

		$('.btn-group').find('.comment-btn').on('click', function() {
			
			var commentForm = '{$commentForm}';
			var socialFeedBoxId = $(this).parent().parent();
			var postId = $(this).attr('forum-post-id');

			commentForm = commentForm.replace(/postId/gi, postId);
			
		  $('.forum-comments', socialFeedBoxId).html(commentForm);

			$('form input').keypress(function(e) {
				
				if (e.which == 13) {
					
					var commentValue = $(this).val();

					if (commentValue.length <= 0) {
						return false;
					}
					
				}
				
			});

			$('form').unbind().on('submit', function(e) {
				
				e.preventDefault();
				
				var data = $(this).serialize();
				var formId = '#' + $(this).attr('id');

				$.ajax({
			    type: 'POST',
			    url: '{$this->Url->build(['controller' => 'ForumPosts', 'action' => 'ajax_add_comment'])}',
			    data: data
			  })
			    .success(function (data) {
						
						if ((data['error'] == undefined || data['error'] == null || data['error'].length == 0) && data.length != 0) {
							
							$('input', formId).val('');
	
							var profileUrl = '#';
	
							if (! data.user) {
								
								profileUrl = '{$this->Url->build(['controller' => 'Contacts', 'action' => 'profile'])}'
								profileUrl += '/' + data['contact']['id'];
								
							}
	
							var profileImage = data['profileImage'];
	
							if (profileImage == 'profile.png') {
								profileImage = PATH + '/img/' + profileImage;
							}
	
							var socialCommentDiv = '<li class="social-comment new-comment">' +
								'<a class="pull-left" href="' + profileUrl + '">' +
									'<img class="avatar img-circle" src="' + profileImage + '">' +
								'</a>' +
								'<div class=\"media-body\">' +
									'<a href=' + profileUrl + '>' + data['contact']['first_name'] + ' ' + data['contact']['last_name'] + '</a>' +
									'<div>' + data['comment']['content'] + '</div>' +
									'<div class=\"btn-group\">' +
										'<button class=\"btn btn-xs like-btn\" user-id='+ data['comment']['user_id'] + ' forum-post-id=' + data['comment']['id'] + '>Like</button>' +
										'<i class=\"fa fa-arrow-up\"></i> <span class=\"total-likes\">0</span>' +
									'</div>' +
								'</div>' +
							'</li>';
	
							if ($('.well', socialFeedBoxId).length > 0) {
								
								$('.well ul', socialFeedBoxId).append(socialCommentDiv);
								
							} else {
								
								$('.social-footer', socialFeedBoxId).append('<div class=\"well\"><ul class=\"list-unstyled\">'
									+ socialCommentDiv +
								'</ul></div>');
								
							}
	
							var totalComments = parseInt($('.post-summary', socialFeedBoxId).find('.total-comments').text()) + 1;
							$('.post-summary', socialFeedBoxId).find('.total-comments').html(totalComments);
	
							$('.comment-btn', socialFeedBoxId).click();
	
							clickLike('.new-comment .btn-group');
	
						}
						
			    });
				
			});
		});
		
		$('.social-footer').each(function() {
			
			$('li.social-comment', $(this)).hide();
			$('li.social-comment:lt(2)', $(this)).show();
			
		});
		
		$('.view-less-comments').hide();
		
		var x = 3;

		$('.view-more-comments').on('click', function (e) {
			
			e.preventDefault();

			var parentContainer = $(this).parent().parent(),
					maxSize = parentContainer.find('.social-comment').length;

			x = (x + 3 <= maxSize) ? x + 3 : maxSize;

      $('li.social-comment:lt(\"' + x + '\")', parentContainer).fadeIn();

      if (x != 3) {
      	$('.view-less-comments', parentContainer).fadeIn();
      }

      if (x == maxSize) {
        $(this).hide();
      }
      
		});

		$('.view-less-comments').click(function (e) {
			
			e.preventDefault();

			var parentContainer = $(this).parent().parent(),
					maxSize = parentContainer.find('.social-comment').length;

      x = (x - 3 < 0) ? 3 : (x % 3 != 0 ? x - (x % 3) : x - 3);
      
      $('li.social-comment', parentContainer).not(':lt(\"' + x + '\")').fadeOut();

      if (x == 3) {
        $('.view-less-comments', parentContainer).hide();
      }

      $('.view-more-comments', parentContainer).show();

    });

		$('#myTabs a').click(function (e) {
			
		  e.preventDefault();
		  e.stopImmediatePropagation();
		  
		  $(this).tab('show');
		  
		});

		var groupAccess = function (self) {
			
			var postUrl = '{$this->Url->build(['controller' => 'Groups', 'action' => 'ajax_group_access'])}' + '.json';
			
			$.post(
				postUrl,
				{
					group_request_id: $(self).data('id'),
					type: $(self).data('type')
				},
				function (response) {

					if (response.status == 'ok')
						$('#group-access-buttons-' + $(self).data('id')).hide();

				}
			);

		};
		
		var cancelAccessRequest = function (self) {
			
			if ($(self).data('type') == 'projects') {
				
				var url = '{$this->Url->build(['action' => 'ajax_cancel_project_access_request'])}' + '.json';
				var data = {project_id: $(self).data('id')};
				
			} else {
				
				var url = '{$this->Url->build(['action' => 'ajax_cancel_group_access_request'])}' + '.json';
				var data = {group_id: $(self).data('id')};
				
			}
			
			$.post(url, data, function () {
				
				$(self)
					.append('&nbsp;{$icons['check']}')
					.addClass('disabled');
					
			});
			
		};

JS
	, ['block' => true]);
	
?>

<?php

if (!$authUser->terms) {
	echo $this->element('terms_modal');
}

?>

<?php if (! $slides->isEmpty()): ?>

	<div id="slider-wrap" class="m-b-md">
		<div id="home-slider" class="royalSlider rsDefault">

			<?php foreach ($slides as $slide): ?>

				<div class="rsContent">
					
					<?= $this->Html->image($slide->getFeaturedImage(1200).'?'.microtime(true), ['class' => 'rsImg']); ?>

					<div class="rsTmb" style="display:none;" data-link=<?= $this->Url->build(['controller' => 'Posts', 'action' => 'view', $slide->id]); ?>>
						
						<?= $this->Html->image($slide->getFeaturedImage(200, true).'?'.microtime(true), ['class' => 'thumb-image']); ?>
						
						<h4><?= $slide->name; ?></h4>
						
						<p>
							<?= $this->Text->truncate(strip_tags($slide->content), 200, [
								'ellipsis' => '...',
								'exact' => false
							]); ?>
						</p>
						
					</div>

					<?= $this->element('slide', ['slide' => $slide]); ?>
					
				</div>
			
			<?php endforeach; ?>

		</div>
	</div>

<?php endif; ?>

<?php

	$groupRequest = false;
	
	foreach ($groups as $group) {
		
		if (!empty($group->group_requests)) {
			$groupRequest = true;
		}
		
	}
	
?>

<!-- pending requests -->

<?php if (!empty($pendingUsers['count']) || $groupRequest || !empty($taggedRecommendations)) :?>

	<div class="row">
		
		<div class="ibox">
			<div class="ibox-title">
				<h1>Pending Requests</h1>
			</div>
		</div>
		
		<?php if (!empty($pendingUsers['count'])): ?>

			<div class="col-md-8">

				<div class="ibox">
					<div class="ibox-title">
						<span><?= $icons['user']; ?></span>
						<h3>Khoja Contacts (<?= $pendingUsers['count']; ?> pending)</h3>
					</div>

					<div class="ibox-content">

						<ul class="list-group user-list-group">

							<?php foreach ($pendingUsers['users'] as $model => $users): ?>

								<li class="m-t-sm" style="list-style: none;">
									<h4><?= $model == 'UserRecommendations' ? 'Recommendations' : 'Contacts'; ?></h4>
								</li>

								<?php foreach ($users as $user): ?>

									<li class="list-group-item">

											<strong style="line-height: 2;"><?= $user->name; ?></strong>

											<?= $this->Html->link(
												'View',
												['controller' => $model, 'action' => 'view', $user->id],
												['class' => 'btn btn-sm btn-primary pull-right']
											); ?>

										<div style="clear: both;"></div>

									</li>

								<?php endforeach; ?>

							<?php endforeach; ?>

						</ul>

					</div>
				</div>

			</div>

		<?php endif; ?>

		<?php if (!empty($taggedRecommendations)): ?>

			<div class="col-md-8">

				<div class="ibox">
					<div class="ibox-title">
						<span><?= $icons['user']; ?></span>
						<h3>New Contacts</h3>
					</div>

					<div class="ibox-content">

						<ul class="list-group user-list-group">

							<li class="m-b-sm" style="list-style: none;">
								A New Contact has been recommended to the portal and you have been subscribed as a reference
							</li>

							<?php foreach ($taggedRecommendations as $recommendation): ?>

								<li class="list-group-item">

									<strong style="line-height: 2;"><?= $recommendation->name; ?></strong>

									<?= $this->Html->link(
										'Review',
										['controller' => 'UserRecommendations', 'action' => 'view', $recommendation->id],
										['class' => 'btn btn-sm btn-primary pull-right']
									); ?>

									<div style="clear: both;"></div>

								</li>

							<?php endforeach; ?>

						</ul>

					</div>
				</div>

			</div>

		<?php endif; ?>

		<div class="col-sm-8">

			<div class="ibox">
				<div class="ibox-title">
					<span><?= $icons['link'] ?></span>
					<h3>Quick Links</h3>
				</div>
				<div class="ibox-content">

					<p><?= $this->Html->link('Forums', ['controller' => 'ForumPosts', 'action' => 'index']); ?></p>
					<p><?= $this->Html->link('Forms', ['controller' => 'Forms', 'action' => 'index', 'F']); ?></p>
					<p><?= $this->Html->link('News', ['controller' => 'Posts', 'action' => 'index', 1]); ?></p>

				</div>
			</div>

			<?php /*

			<div class="ibox">
				<div class="ibox-title">
					<span><?= $icons['calendar']; ?></span>
					<h3>Events</h3>
				</div>

				<div class="ibox-content blue-bg">

					<calendar widget="true"></calendar>

					<div class="m-t-lg text-center">
						<?=
							$this->Html->link($icons['arrows'] . ' View Full',
								['controller' => 'Events', 'action' => 'index'],
								['escape' => false, 'class' => 'btn btn-primary']
							);
						?>
					</div>

				</div>
			</div>

	 		*/ ?>

		</div>
		
	</div>

<?php endif; ?>

<div class="row">

<?php if (! empty($groups)): ?>

	<?php foreach ($groups as $group): ?>
	
		<?php if (!empty($group->group_requests)): ?>
		
			<div class="col-md-8">
				
				<div class="ibox">
					
					<div class="ibox-title">
						<span><?= $icons['user-plus']; ?></span>
						<h3><?=  $group->name;   ?></h3>
					</div>
					
					<div class="ibox-content" style="max-height: 300px; overflow-y: scroll;">
						
						<ul class="list-group user-list-group">
							
							<?php foreach ($group->group_requests as $request): ?>
							
								<li class="list-group-item">
									
									<?= $this->Html->link(
										sprintf(
											'%s %s',
											$this->Html->image(
												$request->user->profile_thumb . '?' . microtime(true),
												['class' => 'img-circle']
											),
											$request->user->name
										),
										['controller' => 'Users', 'action' => 'view', $request->user->id],
										['escape' => false]
									); ?>
									
									<div id="group-access-buttons-<?= $request->id; ?>" class="pull-right">

										<?= $this->Html->link('Accept', 'javascript:;', [
											'class' => 'btn btn-sm btn-primary',
											'onclick' => 'groupAccess(this)',
											'data-id' => $request->id,
											'group-id' => $group->id,
											'data-type' => 'A'
										]); ?>

										<?= $this->Html->link('Reject', 'javascript:;', [
											'class' => 'btn btn-sm btn-danger',
											'onclick' => 'groupAccess(this)',
											'data-id' => $request->id,
											'group-id' => $group->id,
											'data-type' => 'R'
										]); ?>

									</div>
									
								</li>
							
							<?php endforeach; ?>
							
						</ul>
						
					</div>
					
				</div>
				
			</div>
		
		<?php endif; ?>
	
	<?php endforeach; ?>
	
<?php endif; ?>

<!-- pending comments -->
<?php if (!empty($userComments)): ?>
	<div class="col-md-8">
		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['user']; ?></span>
				<h3 >Pending Comments</h3>
			</div>
			<div class="ibox-content">
				<ul class="list-group user-list-group">
					<?php foreach ($userComments as $userComment): ?>
							<li class="list-group-item">
								<div class="ibox-title">
									<?= $this->Html->link(
										$userComment->name,
										['action' => 'edit_bio', $userComment->id],
										['class' => 'btn btn-sm btn-primary pull-left bold']
									); ?>
								</div>
								<div class="ibox-content">
									<ul class="list-group user-list-group">
										<?php foreach ($userComment->user_comments as $comment): ?>
											<?php if ($comment->status == 'A'): ?>
												<li>
													<span class="badge"> wrote at <?= $comment->created; ?></span>
													<p><?= $comment->comment; ?></p>
												</li>
											<?php endif; ?>
										<?php endforeach; ?>
									</ul>
								</div>
							</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
	</div>
<?php endif; ?>

<!--profile statuses for Copywriter -->
<?php if (!empty($profileStatuses)): ?>
<div class="col-md-8">
	<div class="ibox">
		<div class="ibox-title">
			<span><?= $icons['leanpub']; ?></span>
			<h3 >Profile Statuses</h3>
			<div class="ibox-tools">
				<?= $this->Form->create(false, ['type' => 'get', 'inputDefaults' => ['required' => false]]); ?>
					<?= $this->Form->input('state', [
						'empty' => 'All',
						'label' => false,
						'onchange' => 'this.form.submit()',
						'options' => ['U' => 'Public', 'R' => 'Private'],
						'value' =>$state
					]);
					?>
				<?= $this->Form->end(); ?>
			</div>
		</div>
		<div class="ibox-content">
			<ul class="list-group user-list-group">
				<?php foreach ($profileStatuses as $user): ?>
					<?php $status = !empty($user->user_detail->bio) ? '<span class="label link-block label-primary">Public</span>' : '<span class="label link-block label-info">Private</span>'; ?>
						<?= '<li class="list-group-item">'?>
							<div class="ibox-title">
								<?= $this->Html->link(
									$user->name,
									['action' => 'edit_bio', $user->id]
								); ?>
								<div class="ibox-tools">
										<?= $status ?>
								</div>
							</div>
						</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<div class="m-t-lg">
		<?= $this->element('pagination', ['noPadding' => true]); ?>
	</div>
</div>

<?php endif; ?>

<!--pending businesses/charities -->
<?php if (!empty($pendingGroups)): ?>

	<div class="col-md-8">
		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['users']; ?></span>
				<h3>Finish Adding Your Business or Charity</h3>
			</div>
			<div class="ibox-content">
				<ul class="list-group">

					<?php foreach ($pendingGroups as $pendingGroup): ?>

						<li class="list-group-item">
							<h4 class="inline">
								<?= $pendingGroup->name; ?>
							</h4>
							<?= $this->Html->link(
								'Edit',
								['controller' => 'Groups', 'action' => 'edit', $pendingGroup->id],
								['class' => 'btn btn-sm btn-primary pull-right']
							); ?>
							<div style="clear: both;"></div>
						</li>

					<?php endforeach; ?>

				</ul>
			</div>
		</div>
	</div>

<?php endif; ?>

<!-- Pending access requests -->
<?php if (!empty($pendingAccessRequests['groups']) || !empty($pendingAccessRequests['projects'])): ?>

	<div class="col-md-8">
		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['users']; ?></span>
				<h3>Pending Group or Project Access Requests</h3>
			</div>
			<div class="ibox-content">
				<ul class="list-group">

					<?php foreach (['groups' => 'group', 'projects' => 'project'] as $key => $model): ?>

						<?php foreach ($pendingAccessRequests[$key] as $pendingAccessRequest): ?>

							<li class="list-group-item">
								<h4 class="inline">
									<?= $icons[$key]; ?>
									<?= $this->Html->link(
										$pendingAccessRequest->$model->name,
										['controller' => ucfirst($key), 'action' => 'view', $pendingAccessRequest->$model->id]
									); ?>
								</h4>
								<?= $this->Html->link(
									'Cancel',
									'javascript:;',
									[
										'class' => 'btn btn-sm btn-danger pull-right',
										'onclick' => 'cancelAccessRequest(this)',
										'data-type' => $key,
										'data-id' => $pendingAccessRequest->$model->id
									]
								); ?>
								<div style="clear: both;"></div>
							</li>

						<?php endforeach; ?>

					<?php endforeach; ?>

				</ul>
			</div>
		</div>
	</div>

<?php endif; ?>

<!-- Initiatives -->
<?php if (!empty($initiatives)): ?>

	<div class="col-md-8">
		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['dollar'] ?></span>
				<h3>Initiatives</h3>
			</div>
			<div class="ibox-content">

				<div class="table-responsive">

					<table class="table table-condensed">

						<thead>

						<?php

						$headers = [
							'Name',
							'Amount',
							''
						];

						echo $this->Html->tableHeaders($headers);

						?>

						</thead>

						<tbody>

						<?php

						$rows = [];

						foreach ($initiatives as $initiative) {

							$rows[] = [
								'<h4>' . $this->Html->link(
									$initiative->name,
									['controller' => 'Initiatives', 'action' => 'view', $initiative->id]
								) . '</h4>',
								$this->Number->format($initiative->currentPledgeAmount, ['places' => 0, 'before' => 'USD ']) .
								' / ' . $this->Number->format($initiative->goalPledgeAmount, ['places' => 0, 'before' => 'USD ']),
								$this->Html->link(
									'View',
									['controller' => 'Initiatives', 'action' => 'view', $initiative->id],
									['class' => 'btn btn-sm btn-primary pull-right']
								)
							];

						}

						echo $this->Html->tableCells($rows);

						?>

						</tbody>

					</table>

				</div>

			</div>
		</div>
	</div>

<?php endif; ?>

<!-- User fundraising pledges -->
<?php if (!empty($fundraisingPledges)): ?>
	<div class="col-md-8">
		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['money'] ?></span>
				<h3>My Pledges</h3>
				<div class="ibox-tools">
					<?= $this->Html->link(
						'View All',
						['controller' => 'Fundraisings', 'action' => 'my_pledges'],
						['class' => 'btn btn-sm btn-primary pull-right']
					); ?>
				</div>
			</div>
			<div class="ibox-content">
				<div class="table-responsive">
					<table class="table table-condensed table-striped">
						<thead>
						<?php
							$headers = [
								'Project/s Name',
								'Amount',
								'Status',
								['' => ['class' => 'compact']]
							];
							echo $this->Html->tableHeaders($headers);
						?>
						</thead>
						<tbody>
						<?php
							$rows = [];
							$actions = [];
							foreach ($fundraisingPledges as $fundraisingPledge) {

									$fundraising = $fundraisingPledge->fundraising;

									if ($fundraising) {

										$disabled = !empty($fundraisingPledge->amount_pending) || $fundraisingPledge->amount_pending < 0 ? true : false;

										if ($fundraisingPledge->admin_response == 'D') {
											$actions = [
												'Request Denied.'
											];
											$actionsClass = 'text-center badge-danger actions';
										} elseif ($fundraisingPledge->admin_response == 'A') {
											$actions = [
												'Request Approved.'
											];
											$actionsClass = 'text-center badge-primary actions';
										} else {
											$actions = [
												$this->Html->link(
												'Request Change',
												['controller' => 'Fundraisings', 'action' => 'edit_pledge', $fundraisingPledge->id],
												[
													'escape' => false,
													'target' => '_blank',
													'class' => 'btn btn-sm btn-info',
													'disabled' => $disabled
												])
											];
											$actionsClass = 'actions';
										}

										$rows = [];
										$rows[] = [
											$fundraising->name,
											$this->Number->currency($fundraisingPledge->amount),
											$fundraising->fundraisingStatus(),
											[implode(' ', $actions),['class' => $actionsClass]]
										];
										echo $this->Html->tableCells($rows);

									}

								}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>

<?php endif; ?>

<!-- forumposts -->

<?php if (!empty($allSodForumPosts)): ?>

	<div class="col-md-8">
		
		<div class="ibox">
			
			<div class="ibox-title">
				<span><?= $icons['coffee'] ?></span>
				<h3> Forums: <span>Join the Discussion</span></h3>
			</div>
			
			<div class="ibox-content">
				
				<div class="table-responsive">
					<table class="table table-condensed table-striped">
						
						<thead>
						<?php
							$headers = [
								'Post Title',
								['' => ['class' => 'compact']]
							];
							echo $this->Html->tableHeaders($headers);
						?>
						</thead>
						
						<tbody>
						<?php
							$rows = [];

							foreach ($allSodForumPosts as $allSodForumPost) {

								$actions = [
									$this->Html->link('view',
										['controller' => 'ForumPosts', 'action' => 'view', $allSodForumPost->id ],
										['escape' => false, 'class' => 'pull-right btn btn-info btn-icon']
									)
								];

								$rows[] = [
									$this->Html->link($allSodForumPost->topic,
										['controller' => 'ForumPosts', 'action' => 'view', $allSodForumPost->id ]
									),
									[implode(' ', $actions),['class' => 'actions']]
								];


							}
							echo $this->Html->tableCells($rows);
						?>
						</tbody>
						
					</table>
				</div>
				
			</div>
			
		</div>
		
	</div>

<?php endif; ?>

<?php if (!empty($forumPostSubscriptions)): ?>
	<div class="col-md-8">
		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['rss'] ?></span>
				<h3>Forum Subscriptions</h3>
			</div>
			<div class="ibox-content">
				<div class="table-responsive">
					<table class="table table-condensed table-striped">
						<tbody>
						<?php

						$rows = [];

						foreach ($forumPostSubscriptions as $subscription) {

							$postLink = $this->Html->link($subscription->forum_post->topic,
								['controller' => 'ForumPosts', 'action' => 'view', $subscription->forum_post_id]
							);

							if (empty($lastCommentTime = $subscription->forum_post->latest_comment_time)) {
								$lastCommentTime = '-';
							}

							$title = <<<HTML
$postLink ({$subscription->forum_post->created})<br>
<small>
	Replies: <b>{$subscription->forum_post->count_comments}</b> | Last comment time: <b>$lastCommentTime</b>
</small>
HTML;

							$rows[] = [
								$title
							];

						}

						echo $this->Html->tableCells($rows);

						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>

</div>
