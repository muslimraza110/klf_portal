<?php

$this->Html->css('plugins/jsTree/style.min', ['block' => true]);

$this->Html->script('plugins/jsTree/jstree.min', ['block' => true]);

$this->Html->scriptBlock(
	<<<JS

	$(function() {

		$('.equalize1').equalizeHeights();

		$(window).resize(function() {
			$('.equalize1').equalizeHeights();
		});

		$('#projects-tree').bind('select_node.jstree', function (e, data) {

			var href = data.node.a_attr.href;

			if (href == '#') {
				return '';
			}

			document.location.href = href;

		})

		$('#projects-tree').jstree({
			'core' : {
				'check_callback' : true
				},
				'plugins' : [ 'types', 'dnd' ],
				'types' : {
					'default' : {
						'icon' : 'fa fa-folder'
				},
				'html' : {
					'icon' : 'fa fa-file-code-o'
				},
				'svg' : {
					'icon' : 'fa fa-file-picture-o'
				},
				'css' : {
					'icon' : 'fa fa-file-code-o'
				},
				'img' : {
					'icon' : 'fa fa-file-image-o'
				},
				'js' : {
					'icon' : 'fa fa-file-text-o'
				}
			}
		});

	});

	var userAlias = function (self) {

		$.post(
			'{$this->Url->build(['controller' => 'Groups', 'action' => 'ajax_user_alias'])}.json',
			{
				groups_users_id: $(self).data('id'),
				alias: $(self).val()
			}
		);

	};

JS
, ['block' => true]);

?>

<?php if (false): /* if (empty($user->vote_admin_decision)): */ ?>
	<?php if ($authUser->role == 'O' ): ?>

		<?= $this->element('vote', ['entity' => $user, 'vote' => $vote, 'model' => 'Users']); ?>

	<?php endif; ?>

	<?php if (!empty($votes) && !$votes->isEmpty()): ?>

		<?= $this->element('vote_results', ['entity' => $user, 'votes' => $votes, 'model' => 'Users']); ?>

	<?php endif; ?>
<?php endif; ?>
<div class="profile-banner" style="background-image: url(<?= $user->banner_img_url ?>">

	<div class="profile-img-wrapper">
		<?= $this->Html->image($user->profile_img_url.'?'.microtime(true), ['width' => '200px', 'class' => 'profile-img']); ?>
	</div>

	<?php if ($authUser->id == $user->id): ?>
		<div class="pull-right">
			<?= $this->Html->link($icons['edit'].' Edit Profile',
				['action' => 'edit_profile'],
				['escape' => false, 'title' => 'Edit Your Profile', 'data-toggle' => 'tooltip', 'class' => 'btn btn-info btn-icon']
			); ?>
		</div>
	<?php elseif ($authUser->role == 'A'): ?>
		<div class="pull-right">
			<?= $this->Html->link($icons['edit'].' Edit Contact',
				['action' => 'edit', $user->id],
				['escape' => false, 'title' => 'Edit Contact', 'data-toggle' => 'tooltip', 'class' => 'btn btn-info btn-icon']
			); ?>
			<br><br>
			<?= $this->Form->postLink($icons['unlock'].' Login As',
				['action' => 'loginAs', 'controller' => 'Users', $user->id],
				['escape' => false, 'title' => 'Login As', 'data-toggle' => 'tooltip', 'class' => 'pull-right btn btn-info btn-icon']
			); ?>
		</div>
	<?php endif; ?>

	<h1><?= $user->name ?></h1>
	<?= (!empty($user->user_detail->title)) ? '<p><span class="label label-default">' . $user->user_detail->title . '</span></p>' : '' ?>
	<?= (!empty($user->user_detail->description)) ? '<p>' . $user->user_detail->description . '</p>' : '' ?>

</div>

<?php if (!empty($categorizedGroups)): ?>

	<div class="row">

		<?php $columnWidth = 'col-md-'.(24 / count($categorizedGroups)); ?>

		<?php foreach ($categorizedGroups as $name => $categorizedGroup): ?>

			<div class="<?= $columnWidth; ?>">

				<div class="ibox">
					<div class="ibox-title">
						<span><?= $icons['groups']; ?></span>
						<h3><?= $name; ?></h3>
					</div>
					<div class="ibox-content equalize1" style="height: 120px; overflow-x: auto">

						<?php foreach ($categorizedGroup as $group): ?>

							<?= $this->Html->link(
								$this->Html->image($group->profile_thumb.'?'.microtime(true), ['height' => '120px']),
								['controller' => 'Groups', 'action' => 'view', $group->id],
								['escape' => false, 'title' => $group->name, 'data-toggle' => 'tooltip']
							); ?>

						<?php endforeach; ?>
						<hr>

						<table class="table table-condensed table-striped">
							<thead>
							<?php

							$headers = [
								'Name',
								'Title'
							];

							echo $this->Html->tableHeaders($headers);

							?>
							</thead>
							<tbody>
							<?php

							$rows = [];

							foreach ($categorizedGroup as $group) {

								$rows[] = [
									$group->name,
									$group->_joinData->user_alias
								];

							}

							echo $this->Html->tableCells($rows);

							?>
							</tbody>
						</table>

					</div>
				</div>

			</div>

		<?php endforeach; ?>

	</div>

<?php endif; ?>

<div class="row">

	<div class="col-sm-10 col-md-8">
		<?php if (!empty($user->groups)): ?>
		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['folder']; ?></span>
				<h3>Group Projects</h3>
				<div class="ibox-tools">
					<?=  $this->Html->link(
						$icons['plus'],
						['controller' => 'Projects', 'action' => 'add', $group->id],
						['escape' => false, 'class' => 'btn btn-info btn-icon']
					); ?>
				</div>
			</div>
			<?php if (!empty($projectTree)): ?>
			<div class="ibox-content">
				<div id="projects-tree">

					<?php

					$list = [];

					foreach ($projectTree as $key => $node) {

						$listGroup = [];
						foreach ($node as $group) {

							$projects = [];

							foreach ($group->projects as $project) {
								$projects[] = $this->Html->link($project->name, ['controller' => 'Projects', 'action' => 'view', $project->id]);
							}

							$groupLink = $this->Html->link($group->name, ['controller' => 'Groups', 'action' => 'view', $group->id]);
							$listGroup[$groupLink] = $projects;

						}

						$list[$key] = $listGroup;

					}

					echo $this->Html->nestedList($list);

					?>

				</div>

			</div>
			<?php endif; ?>
		</div>
		<?php endif; ?>

		<div class="ibox">
			<div class="ibox-title">
				<span><?= $icons['info'] ?></span>
				<h3>Information</h3>
			</div>

			<div class="ibox-content">

				<?php if(!empty($user->email)): ?>
					<h4>Email</h4>
					<p><?= $this->Html->link($user->email, 'mailto:' . $user->email, ['target' => '_blank']); ?></p>
				<?php endif; ?>

				<?php if(!empty($user->user_detail->email2)): ?>
					<h4>Secondary Email</h4>
					<p><?= $this->Html->link($user->user_detail->email2, 'mailto:' . $user->user_detail->email2, ['target' => '_blank']); ?></p>
				<?php endif; ?>

				<?php if(!empty($user->user_detail->phone)): ?>
					<h4>Phone</h4>
					<p><?= $user->user_detail->phone; ?></p>
				<?php endif; ?>

				<?php if (! empty($user->user_detail->phone2)): ?>
					<h4>Secondary Phone</h4>
					<p><?= $user->user_detail->phone2; ?></p>
				<?php endif; ?>

				<?php if (!empty($user->user_detail->multi_line_address)): ?>
					<h4>Address</h4>
					<address><?= $user->user_detail->multi_line_address; ?></address>
				<?php endif; ?>

				<?php if (!empty($user->user_detail->website)): ?>
					<h4>Website</h4>
					<p><?= $this->Html->link($user->user_detail->website, 'http://'.$user->user_detail->website, ['target' => '_blank']); ?></p>
				<?php endif; ?>

				<?php if (!empty($user->user_detail->facebook)): ?>
					<h4>Facebook</h4>
					<p><?= $this->Html->link($user->user_detail->facebook, $user->user_detail->facebook, ['target' => '_blank']); ?></p>
				<?php endif; ?>

				<?php if (!empty($user->user_detail->linkedin)): ?>
					<h4>LinkedIn</h4>
					<p><?= $this->Html->link($user->user_detail->linkedin, $user->user_detail->linkedin, ['target' => '_blank']); ?></p>
				<?php endif; ?>

			</div>
		</div>


		<?php if (!empty($user->fundraising_pledges)): // Fundraising Pledges START ?>

			<div class="ibox">
				<div class="ibox-title">
					<span><?= $icons['dollar'] ?></span>
					<h3>Pledges</h3>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-condensed">
							<thead>
							<?php

							$headers = [
								'Initiative',
								'Amount'
							];

							echo $this->Html->tableHeaders($headers);

							?>
							</thead>
							<tbody>
							<?php

							$rows = [];

							foreach ($user->fundraising_pledges as $pledge) {

								$amount = $this->Number->format($pledge->amount, ['places' => 0, 'before' => 'USD ']);

								if ($pledge->anonymous && ($pledge->user_id != $authUser->id || $authUser->role != 'A')) {
									continue;
								}

								if ($pledge->hide_amount) {

									if ($pledge->user_id == $authUser->id || $authUser->role == 'A') {
										$amount = $this->Number->format($pledge->amount, ['places' => 0, 'before' => 'USD ']) . '<span style="color:red">*</span>';
									} else {
										$amount = 'Hidden';
									}

								}

								$rows[] = [
									$this->Html->link(
										$pledge->fundraising->initiative->name,
										['controller' => 'Initiatives', 'action' => 'view', $pledge->fundraising->initiative->id]
									),
									$amount
								];

							}

							echo $this->Html->tableCells($rows);

							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		<?php endif; // Fundraising Pledges END ?>

		<?php if (!empty($user->user_families)): ?>

			<div class="ibox">
				<div class="ibox-title">
					<span><?= $icons['user']; ?></span>
					<h3>Family</h3>
				</div>

				<div class="ibox-content no-padding">
					<table class="table table-condensed table-family">

						<tbody>
						<?php

						if (!empty($user->user_families))
							foreach ($user->user_families as $family) {
								$row = [
									$family->relation == 'Custom Relationship' ?  sprintf('<strong>%s</strong>', $family->relation) : sprintf('<strong>%s</strong> of', $family->relation),
									$this->Html->link(
										sprintf(
											'%s %s',
											$this->Html->image($family->related_user->profile_thumb.'?'.microtime(true), ['class' => 'img-circle']),
											$family->related_user->name
										),
										['action' => 'view', $family->related_user->id],
										['escape' => false]
									)
								];

								echo $this->Html->tableCells($row);

							}
						else
							echo $this->Html->tableCells([[
								['No relations found.', ['colspan' => 3]]
							]]);

						?>
						</tbody>

					</table>

				</div>
			</div>

		<?php endif; ?>

	</div>

	<div class="col-sm-14 col-md-16">
		
		<?php if (!empty($user->user_detail)): ?>
			
			<?php if (!empty($user->user_detail->bio) && trim($user->user_detail->bio) != ''): ?>
				<div class="ibox">
					<div class="ibox-title">
						<span><?= $icons['align-left']; ?></span>
						<h3>About Me</h3>
					</div>
					<div class="ibox-content">
						<?= nl2br($user->user_detail->bio); ?>
					</div>
				</div>
			<?php endif; ?>
			
			<?php if (($authUser->role == 'W') || (empty($user->user_detail->bio))): ?>
				<?php if ((!empty(trim($user->user_detail->private_bio)))): ?>
					<div class="ibox">
						<div class="ibox-title">
							<h3>Private Bio</h3>
						</div>
						<div class="ibox-content">
							<?= nl2br($user->user_detail->private_bio); ?>
						</div>
					</div>
				<?php endif; ?>
			<?php endif; ?>
			
		<?php endif; ?>

	</div>
</div>
