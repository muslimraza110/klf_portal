<?= $this->Form->create($passwordReset); ?>

<div class="middle-box">
  <div class="ibox">
    <div class="ibox-content">
      
      <?= $this->Html->image('logo.png', [
        'alt' => 'Khoja',
        'id' => 'login-logo'
      ]); ?>
      
      <?= $this->Flash->render(); ?>
      
      <p class="text-left" style="margin-bottom: 25px;">Enter your email address to receive an email containing a link to reset your password. It can take a couple minutes for the email to arrive. If you don't see it in your inbox, verify your junk mail folder.</p>
      
      <?= $this->Form->input('email', ['autofocus' => 'autofocus']); ?>
      
      <?= $this->Form->submit('Submit', ['bootstrap-type' => 'primary', 'class' => 'btn-block']); ?>
			
			<p class="text-center" style="margin-bottom: 0;">
				<?= $this->Html->link('Go back to login', ['action' => 'login'], ['class' => 'text-muted', 'style' => 'font-size: 0.8em;']); ?>
			</p>
			
    </div>
  </div>
</div>

<?= $this->Form->end(); ?>