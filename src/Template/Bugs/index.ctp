<?php
	$scriptBlock = <<<JS
		$(function() {

	    $('#bug-category-id, #user-id').change(function() {
	      $('#submitForm').submit();
	    });

	    $('#complete').click(function() {
          return confirm('You sure you want to complete all bugs?');
    	});
	    
	    $('#sandbox').click(function() {
          return confirm('Would you like to add a commit to the SANDBOX?');
    	});
	    
	    $('#portal').click(function() {
          return confirm('Would you like to add a commit to the PORTAL?');
    	});
			
		});
JS;

	$this->Html->scriptBlock($scriptBlock, ['block' => true]);
?>
<?= $this->Form->create(null, ['id' => 'commit', 'name' => 'commit']); ?>
<div class="row">
	<div class="col-xs-24">
		<div class="ibox">
			<div class="ibox-title">
				<h3>Commits</h3>
				<div class="ibox-tools">
					<?php if ($authUser->role == 'A') : ?>
						<?= $this->Form->button('Sandbox', ['type' => 'submit', 'class' => 'btn-default', 'id' => 'sandbox', 'name' => 'sandbox']) ?>
						<?= $this->Form->button('Portal', ['type' => 'submit', 'class' => 'btn-default', 'id' => 'portal', 'name' => 'portal']) ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="ibox-content">
				<div class="table-responsive">
					<table class="table table-condensed table-striped">
						<thead>
							<?php

								$headers = [
									'Committed To',
									'Bug Numbers',
									'Date Committed'
								];

								echo $this->Html->tableHeaders($headers);

							?>
						</thead>
						<tbody>
							<?php

								$rows = [];

								if(!empty($commits))
									foreach ($commits as $commit) {

										if ($commit->type == 'S')
											$commitLabel = '<span class="label label-success">'.$commitTypes[$commit->type].'</span>';
										else
											$commitLabel = '<span class="label label-primary">'.$commitTypes[$commit->type].'</span>';

										$rows[] = [
											$commitLabel,
											implode(', ', json_decode($commit->bug_nums)),
											$commit->created->format('M d, Y - h:i A')
										];

									}

								echo $this->Html->tableCells($rows);

							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<?= $this->Form->end(); ?>
	<div class="col-xs-24 col-md-12">
		
		<div class="ibox" id="table-checkbox">
			<div class="ibox-title">
				<div class="row">
					<div class="col-xs-24">
						<h3>Pending Bugs: <?= !empty($bugCategory) ? $bugCategories[$bugCategory] : 'All' ?></h3>
						<div class="pull-right">
							<?= $this->Form->create(null, ['type' => 'get', 'id' => 'submitForm']); ?>

								<?= $this->Html->link('Bug Categories',
									['action' => 'index', 'controller' => 'BugCategories'],
									['class' => 'btn btn-xs btn-default m-r-sm']
								); ?>

				        <?= $this->Form->input('bug_category_id', [
				        		'templates' => [
				        			'inputContainer' => '<div class="select" style="display:inline-block">{{content}}</div>'
				        		],
				        		'empty' => '--Category--',
				        		'label' => false,
				        		'class' => 'dropdown-small'
				        	]); 
				        ?>
								<?= $this->Form->input('user_id', [
									'templates' => [
										'inputContainer' => '<div class="select" style="display:inline-block">{{content}}</div>'
									],
									'empty' => '--User--',
									'label' => false,
									'class' => 'dropdown-small'
								]);
								?>
				      <?= $this->Form->end(); ?>	
			      </div>
					</div>
				</div>
			</div>
			<div class="ibox-content">

				<?php foreach($labels as $priority => $label): ?>	
					<div class="panel panel-default">
						<div class="panel-heading">
							<span class="label label-<?php echo $label ?>"><?php echo $priorities[$priority] ?></span>				
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table no-margins">
									<tbody>
									<?php
									$rows = [];

									if (!isset($openBugs[$priority]))
										$openBugs[$priority] = [];

									foreach($openBugs[$priority] as $bug) {

										$notes = '';

										if (! empty($bug->description)) {
											$notes = $this->Html->link('Notes', 'javascript://', [
												'class' => 'label label-default notes-link',
												'data-toggle' => "popover",
												'data-trigger' => "focus",
												'data-placement' => "bottom",
												'data-content' => $bug->description
											]);
										}

										$rows[] = [
											'#'.$bug->id,
											$bug->created->format('M d, Y'),
											!empty($bug->user->name) ? $bug->user->name : '',
											!empty($bug->bug_category) ? $bug->bug_category->name : '--',
											$bug->name,
											$notes,
											$this->Html->link(
												$icons['edit'],
												['action' => 'edit', $bug->id],
												['escape' => false]
											),
											$this->Html->link(
												$icons['delete'],
												['action' => 'delete', $bug->id],
												['escape' => false, 'confirm' => 'Confirm delete?']
											),
											$this->Html->link(
												$icons['check-square-o'],
												['action' => 'updateStatus', $bug->id, '?' => $this->request->query],
												['escape' => false, 'title' => 'Commit', 'confirm' => 'Are you sure you want to Commit this bug?']
											)
										];
									}

									echo $this->Html->tableCells($rows);
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>

	<div class="col-xs-24 col-md-12">
<?= $this->Form->create(null, ['id' => 'submitStatus', 'name' => 'submitStatus']); ?>
		<div class="ibox" id="table-checkbox">
			<div class="ibox-title">
				<h5>Bugs</h5>
			</div>
			<div class="ibox-content">
				<?php foreach($statuses as $label => $status): ?>	
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-12">
									<span class="label label-<?php echo $labels[$label] ?>"><?php echo $status ?></span>	
								</div>

								<?php if ($status == 'Committed') : ?>
									<div class="col-xs-12">
										<?= $this->Form->button('Complete All', ['class' => 'btn btn-xs pull-right btn-primary', 'type' => 'submit', 'name' => 'complete', 'id' => 'complete']); ?>
									</div>
								<?php endif; ?>
							</div>
										
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-hover table-condensed no-margins">
									<tbody>
									<?php
									$rows = [];
									foreach ($seenBugs as $seenBug) {
										if (strtolower($status) == $seenBug->status) {
											$statusField = $seenBug->status;

											$note = '';

											if (! empty($seenBug->description)) {
												$note = $this->Html->link('Notes', 'javascript://', [
													'class' => 'label label-default notes-link',
													'data-toggle' => "popover",
													'data-trigger' => "focus",
													'data-placement' => "bottom",
													'data-content' => $seenBug->description
												]);
											}

											$actions = [
												'edit' => $this->Html->link(
													$icons['edit'],
													['action' => 'edit', $seenBug->id],
													['escape' => false]
												),
												'pending' => $this->Html->link(
													$icons['mail-reply'],
													['action' => 'returnPending', $seenBug->id],
													['escape' => false, 'title' => 'Set as Pending', 'confirm' => 'Are you sure you want to Set this bug as Pending?']
												),
												'complete' =>	$this->Html->link(
													$icons['check-square-o'],
													['action' => 'complete', $seenBug->id],
													['escape' => false, 'title' => 'Complete', 'confirm' => 'Are you sure you want to Set this bug as Completed?']
												),
												'verify' => $this->Html->link(
													$icons['check-square-o'],
													['action' => 'updateStatus', $seenBug->id],
													['escape' => false, 'title' => 'Verify', 'confirm' => 'Are you sure you want to Verify this bug?']
												)
											];

											if ($status == 'Completed') {
												unset($actions['complete']);
												unset($actions['pending']);
											}

											else if ($status == 'Committed') {
												unset($actions['verify']);
											}

											else {
												unset($actions['complete']);
												unset($actions['pending']);
												unset($actions['verify']);
											}

											$rows[] = [
												'#'.$seenBug->id,
												$seenBug->$statusField->format('M d, Y'),
												!empty($seenBug->user->name) ? $seenBug->user->name : '',
												!empty($seenBug->bug_category) ? $seenBug->bug_category->name : '--',
												$seenBug->name,
												$note,
												[implode(' ', $actions), ['class' => 'actions']]
											];
										}
									}

									echo $this->Html->tableCells($rows);
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				<?php endforeach ?>
			</div>	
		</div>
		<?= $this->Form->end();?>
	</div>
</div>

