<?php echo $this->Form->create($bug, ['type' => 'file']); ?>
<?= $this->element('asset_carousel', ['assets' => $assets]) ?>
<div class="row">
	<div class="col-xs-24 col-md-12">
		<div class="ibox">
			<div class="ibox-title">
				<h5><?= $title ?> <?= !empty($bug->id) ? ' : #'.$bug->id : '' ?></h5>

			</div>
			<div class="ibox-content">


				<div class="row">
					<div class="col-xs-12">
						<?php echo $this->Form->input('name'); ?>
					</div>
				</div>

				<?php echo $this->Form->input('description'); ?>

				<div class="row">
					<div class="col-xs-12">
						<?= $this->Form->input('bug_category_id', [
							'empty' => '--'
						]); ?>
					</div>
					<div class="col-xs-12">
						<?php echo $this->Form->input('priority'); ?>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="col-xs-24 col-md-12">
		<?= $this->element('upload_files') ?>
	</div>
</div>
<div class="ibox">
	<div class="ibox-content">
		<div class="submit-big">
			<?php echo $this->Form->button('Save', ['class' => 'btn-primary', 'name' => 'submit-form']); ?>
		</div>
	</div>
</div>
<?php echo $this->Form->end(); ?>
