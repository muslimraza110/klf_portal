<?= $this->Form->create($bug); ?>

<div class="row">

	<div class="col-md-12 col-md-offset-6">

		<div class="ibox">
			<div class="ibox-title">
				<h5>Feedback</h5>
			</div>

			<div class="ibox-content">

				<?= $this->Form->input('name', ['label' => 'Title', 'required' => true]); ?>

				<?= $this->Form->input('description', ['label' => 'Feedback', 'required' => true]); ?>

			</div>
		</div>

		<div class="submit-big">
			<?= $this->Form->submit('Submit', ['bootstrap-type' => 'primary']); ?>
		</div>

	</div>

</div>

<?= $this->Form->end(); ?>