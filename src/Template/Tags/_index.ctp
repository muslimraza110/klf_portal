<table class="table table-bordered">
	<thead>
	<?= $this->Html->tableHeaders([
		'Name',
		['Actions' => ['class' => 'compact text-center']]
	]); ?>
	</thead>
	<tbody>
	<?php

	if (!$tags->isEmpty()) {

		$rows = [];

		foreach ($tags as $tag) {

			$actions = $this->Html->link(
				$icons['edit'].'Edit',
				['action' => 'edit', $tag->id],
				['escape' => false, 'class' => 'btn btn-primary']
			);

			$actions .= $this->Form->postLink(
				$icons['delete'].'Delete',
				['action' => 'delete', $tag->id],
				['escape' => false, 'class' => 'btn btn-danger', 'confirm' => 'Are you sure you want to delete the tag "'.$tag->name .'"?']
			);

			$rows[] = [
				$tag->name,
				[$actions, ['class' => 'icon compact']]
			];

		}

		echo $this->Html->tableCells($rows);

	}

	?>
	</tbody>
</table>

<?= $this->element('pagination', ['noPadding' => true]); ?>