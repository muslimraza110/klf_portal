<?= $this->Form->create($tag); ?>

<div class="row">

	<div class="col-md-12 col-md-offset-6">

		<div class="ibox">
			<div class="ibox-content">

				<?= $this->Form->input('name'); ?>

			</div>
		</div>

		<div class="submit-big">
			<?= $this->Form->submit('Save', ['class' => 'btn btn-primary']); ?>
		</div>

	</div>

</div>

<?= $this->Form->end(); ?>