<?php
$scriptBlock = <<<JS

	$(function() {
		
		$(".dd").nestable();
		
		$('.saveList').on('click', function(e) {
			e.preventDefault();
			
			$.post('{$this->Url->build(['controller' => 'Tags', 'action' => 'ajaxSaveSorting'])}.json', {
				serialized_tags : $('.dd').nestable('serialize')
			}).done(function (order) {
				 location.reload();
			}); 

		});
	});
 
JS;
$this->Html->scriptBlock($scriptBlock, ['block' => true]);

?>

<div class="text-right">
	<?= $this->Html->link('Save Sorting', '#', ['class' => 'btn btn-primary saveList']) ?>
</div>

<hr>

<div class="dd">

	<?= $this->element('recursive_multi_level_tags', [
		'tags' => $tags,
		'level' => 0
	]); ?>

</div>

