<?php
$this->Html->css('plugins/chosen/chosen.min', ['block' => true]);

$this->Html->script('plugins/chosen/chosen.jquery.min', ['block' => true]);

$this->Html->scriptBlock(
	<<<JS

	$(function() {

		$('#tags-ids option').prop('selected', true);

		$('#tags-ids').chosen({
			placeholder_text_multiple: 'Enter Tags',
			create_option: true,
			single_backstroke_delete: true
		});

	});

JS
	, ['block' => true]);
?>

<?= $this->Form->create($tag); ?>

<div class="row">

	<div class="col-md-12 col-md-offset-6">

		<div class="ibox">

			<div class="ibox-content" style="height: 300px;">

				<div class="row">

					<div class="col-xs-16">
						<?= $this->Form->input('name', ['label' => 'Top Level']); ?>
					</div>

					<div class="col-xs-8">

						<label>&nbsp;</label>

						<?php

						$disabled = true;

						if ($tag->isNew() || $tag->root_tag->id == $tag->id) {
							$disabled = false;
						}

						echo $this->Form->input('mandatory', [
							'disabled' => $disabled,
							'checked' => $tag->root_tag->mandatory
						]);

						?>

					</div>

				</div>

				<?= $this->Form->input('tags._ids'); ?>

			</div>

		</div>

		<div class="submit-big">
			<?= $this->Form->submit('Save', ['class' => 'btn btn-primary']); ?>
		</div>

	</div>

</div>

<?= $this->Form->end(); ?>