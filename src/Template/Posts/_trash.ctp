<table class="table table-bordered table-with-status">
  <thead>
    <tr class="status-heading">
      
      <th colspan="5"></th>
      <th colspan="2" class="status-heading-cell">Status</th>
      <th colspan="1"></th>
    </tr>
    <?php
      $headers = [
        $this->Paginator->sort('name'),
        $this->Paginator->sort('published', 'Date of Publication'),
        $this->Paginator->sort('Categories.name', 'Category'),
        
        $this->Paginator->sort('Users.first_name', 'Created by'),
        
        ['Pending' => ['class' => 'compact']],
        ['Current' => ['class' => 'compact']],
        
        ['Actions' => ['class' => 'compact']]
      ];
      
      echo $this->Html->tableHeaders($headers);
    ?>
  </thead>

  <tbody>
    <?php
      if (! $posts->isEmpty()) {
        foreach ($posts as $post) {
          
          $row = [
            $this->Html->link($post->name, ['controller' => 'Posts', 'action' => 'view', $post->id]),
            (!empty($post->published)) ? $post->published : 'Unpublished',
            (!empty($post->category)) ? $post->category->name : 'Uncategorized',
            (!empty($post->user)) ? $post->user->name : 'Unknown',
            
            [
              ($post->isPending()) ? '<i class="dot text-warning"></i>' : '',
              ['class' => 'status']
            ],
            
            [
              ($post->isCurrent()) ? '<i class="dot text-success"></i>' : '',
              ['class' => 'status']
            ]
          ];

          
          $actions = $this->Form->postLink(
            $icons['reply'] . 'Restore',
            ['controller' => 'Posts', 'action' => 'delete', $post->id, true],
            ['escape' => false, 'class' => 'btn btn-info']
          );
          
          $actions .= $this->Form->postLink(
            $icons['delete'] . 'Delete',
            ['controller' => 'Posts', 'action' => 'trash', $post->id],
            [
              'escape' => false,
              'class' => 'btn btn-danger',
              'confirm' => 'Do you want to permanently delete "' . $post->name . '" ?'
            ]
          );
          
          $row[] = [$actions, ['class' => 'compact']];
          
          echo $this->Html->tableCells([$row]);
        }
        
        
      }
    ?>
  </tbody>
</table>

<?= $this->element('pagination', ['noPadding' => true]); ?>