<?php
	
	$data = $this->Url->build(["controller" => "ForumPosts", "action" => "ng_forum_posts"]);
	/*
  $this->Html->scriptBlock('
    angular.module("KhojaApp").controller("PostsIndexController", function($scope, $resource) {
      $scope.posts = $resource("' . $this->Url->build(["controller" => "ForumPosts", "action" => "ng_forum_posts"]) . '").query();
      $scope.posts = $resource("' . $this->Url->build(["controller" => "ForumPosts", "action" => "ng_forum_posts"]) . '", {}, {
		  });
    });
		
  ', ['block' => true]);*/
?>

<div class="row">
	<div class="col-sm-16">
		<div class="ibox">
			<div class="ibox-title ibox-icons">
				<?= $icons['commenting'] ?>
				<h3> Forums: <span>Join the Discussion</span></h3>
			</div>

    	<div class="ibox-content" ng-controller="PostsIndexController">
				<div class="tabs-container">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-1" data-toggle="tab">Latest</a></li>
            <li class=""><a href="#tab-2" data-toggle="tab">Yesterday</a></li>
          </ul>

          <div class="tab-content">
            <div class="tab-pane active" id="tab-1">
              <div class="panel-body" ng-repeat="post in posts">
  
              	<div class="social-feed-box">

              		<div class="social-avatar">
								    <?= $this->Html->link($this->Html->image('profile.jpg', ['class' => 'avatar']),
								    	['controller' => 'Contacts', 'action' => 'profile', '{{ post.user.last_name }}' ],
								    	['escape' => false, 'class' => 'pull-left']
								    ); ?>

								    <div class="media-body">
								      <a ng-href="<?= $this->Url->build(['controller' => 'Contacts', 'action' => 'profile'])?>/{{ post.user.id }}"}>
								      	{{ post.user.first_name }} {{ post.user.last_name }}
								      </a>
								 
								      <small class="text-muted">{{ post.created }}</small>
								    </div>
									</div>

									<div class="social-body" >
								    {{ post.content }}
								  </div>

              	</div>

              </div>
            </div>

          </div>
      	</div>
			</div>

		</div>
	</div>
</div>