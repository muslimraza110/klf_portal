<?php if (!$posts->isEmpty()): ?>
	<!-- <div class="row"> -->
		<?php foreach ($groupedPosts as $topic => $posts): ?>
			<h2 class="m-b-n"><?= $topic;?></h2>
			<table class="table table-bordered table-with-status">
	<thead>
		<?php if (in_array($authUser->role, ['A', 'O'])): ?>
			<tr class="status-heading">
				<th colspan="<?= (is_null($category->parent_id)) ? '7' : '6'?>"></th>
				<th colspan="2" class="status-heading-cell">Status</th>
			</tr>
		<?php endif; ?>

		<?php
			$width = ['style' => 'width:17%;'];
			$headers = [
				['Name' => $width],
				['Date of Publication' => $width],
				['Type' => $width],
				['Category' => $width]
			];

			if (in_array($authUser->role, ['A', 'O', 'R'])) {

				$headers[] = $this->Paginator->sort('Users.first_name', 'Created by');

				$headers[] = ['Pending' => ['class' => 'compact']];
				$headers[] = ['Current' => ['class' => 'compact']];

				$headers[] = ['Actions' => ['class' => 'compact text-center']];
			}

			echo $this->Html->tableHeaders($headers);
		?>
	</thead>

	<tbody>
		<?php
		// debug($post);exit;
			if (!empty($posts)) {
				foreach ($posts as $post) {

					$publish = (!empty($post->published)) ? $post->published : 'Unpublished';

					if(in_array($authUser->role, ['A', 'O']) && $post->isPending() && empty($post->published)) {
						$publish = $this->Form->postLink('Publish Now', ['action' => 'publish', $post->id], ['class' => 'btn btn-info btn-xs']);
					}

					$row = [
						$this->Html->link(html_entity_decode($post->name),
							$post->post_link,
							['target' => '_blank']
						),
						$publish,
						(($post->isPdf()) ? 'PDF' : 'Online Post'),
						(!empty($post->post_category)) ? $post->post_category->name : 'Uncategorized'
					];

					if (in_array($authUser->role, ['A', 'O', 'R'])) {

						$row[] = (!empty($post->user)) ? $post->user->name : 'Unknown';

						$row[] = [
							($post->isPending()) ? '<i class="dot text-warning"></i>' : '',
							['class' => 'status']
						];

						$row[] = [
							($post->isCurrent()) ? '<i class="dot text-success"></i>' : '',
							['class' => 'status']
						];

						$actions = $this->Html->link(
							$icons['edit'] . 'Edit',
							['controller' => 'Posts', 'action' => 'edit', $post->id],
							['escape' => false, 'class' => 'btn btn-primary']
						);

						$actions .= $this->Form->postLink(
							$icons['delete'] . 'Delete',
							['controller' => 'Posts', 'action' => 'delete', $post->id],
							[
								'escape' => false,
								'class' => 'btn btn-danger',
								'confirm' => 'Do you really want to move the post "' . $post->name . '" to the trash?'
							]
						);

						$row[] = [$actions, ['class' => 'compact']];
					}

					echo $this->Html->tableCells([$row]);
				}


			}
		?>
	</tbody>
</table>
		<?php endforeach; ?>
	<!-- </div> -->
<?php endif; ?>
<?php if (in_array($authUser->role, ['A', 'O'])): ?>
	<div class="row">
		<div class="col-xs-24 col-md-18">
		</div>
		<div class="col-xs-24 col-md-6">
			<div class="pull-right">

				<?= $this->Html->link($icons['edit'] . 'Edit Categories', ['controller' => 'Categories', 'action' => 'index'], [
					'escape' => false,
					'class' => 'btn btn-primary btn-sm'
				]); ?>

				<?= $this->Html->link($icons['trash'] . 'View Trash', ['controller' => 'Posts', 'action' => 'trash'], [
					'escape' => false,
					'class' => 'btn btn-danger btn-sm'
				]); ?>

			</div>
		</div>
	</div>
<?php else: ?>
<?php endif; ?>
