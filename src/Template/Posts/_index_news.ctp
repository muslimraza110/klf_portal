<?php
echo $this->Html->css([
	'royalslider/royalslider',
	'royalslider/skins/default/rs-default',
], ['block' => true]);

echo $this->Html->script([
	'royalslider/jquery.royalslider.min',
], ['block' => true]);

echo $this->Html->scriptBlock("

    $(document).ready(function(){

      $('#archived').change(function() {
        $('#archiveForm').submit();
      });

      $('#news-slider').royalSlider({
        arrowsNav: true,
        fadeinLoadedSlide: true,
        controlNavigationSpacing: 0,
        controlNavigation: false,
        autoPlay: {
          enabled: true,
          pauseOnHover: true,
          delay: 6000
        },

        keyboardNavEnabled: true,
        imageScaleMode: 'fill',
        imageAlignCenter: true,
        slidesSpacing: 0,
        loop: false,
        loopRewind: true,
        numImagesToPreload: 3,

        autoScaleSlider: true,
        autoScaleSliderWidth: 1900,
        autoScaleSliderHeight: 600,
      });

    });

  ", ['block' => true]);
?>

<div id="slider-wrap" class="m-b-lg">
	<div id="news-slider" class="royalSlider rsDefault">
		<?php foreach ($slides as $id => $slide): ?>

			<?php $view = $this->Url->build(['action' => 'view', $slide->id]); ?>

			<div class="rsContent">
				<?= $this->Html->image($slide->getFeaturedImage(1900).'?'.microtime(true), ['class' => 'rsImg']); ?>
				<?= $this->element('slide', ['slide' => $slide]); ?>
			</div>

		<?php endforeach; ?>
	</div>
</div>
<?= $this->Form->create(null, ['type' => 'get', 'id' => 'archiveForm']) ?>
<div class="row">
	<div class="col-xs-3 pull-right">
		<?= $this->Form->input('archived', [
			'label' => false,
			'options' => $statusSelect
		]);
		?>
	</div>
</div>
<?= $this->Form->end(); ?>
<?php if (!$posts->isEmpty()): ?>
	<?php foreach ($posts as $post): ?>
		<?php if (!$post->isArchived() || !empty($this->request->data['archived'])): ?>
			<div class="vote-item">
				<div class="row">
					<div class="<?= (in_array($authUser->role, ['A', 'O', 'R', 'W'])) ? 'col-xs-18' : 'col-xs-24'; ?>">
						<div class="vote-icon pull-left">
							<?= $this->Html->image($post->getFeaturedImage(null, true).'?'.microtime(true), ['width' => 200]); ?>
						</div>

						<?=
						$this->Html->link($post->name,
							['controller' => 'Posts', 'action' => 'view', $post->id],
							['class' => 'vote-title', 'target' => '_blank']
						);
						?>

						<div class="vote-info text-muted">
							<?php
							$publish = (!empty($post->published)) ? $post->published : 'Unpublished';
							echo $publish;
							?>
						</div>
					</div>

					<?php if (in_array($authUser->role, ['A', 'O', 'R', 'W'])): ?>
						<div class="col-xs-6">
							<div class="m-t-lg"></div>
							<?php
							if (in_array($authUser->role, ['A', 'O', 'R', 'W']) && empty($this->request->data['archived'])) {
								echo $this->Form->postLink('Archive', ['action' => 'archive', $post->id], [
									'class' => 'btn btn-info btn-xs m-r-xs'
								]);
							} else {
								echo $this->Form->postLink('Unarchive', ['action' => 'archive', $post->id], [
									'class' => 'btn btn-info btn-xs m-r-xs'
								]);
							}

							if (in_array($authUser->role, ['A', 'O']) && $post->isPending() && empty($post->published)) {
								echo $this->Form->postLink('Publish Now', ['action' => 'publish', $post->id], [
									'class' => 'btn btn-info btn-xs m-r-xs'
								]);
							}

							if (in_array($authUser->role, ['A', 'O']) && !$post->isPending() && !empty($post->published))
								echo $this->Form->postLink('Unpublish Now', ['action' => 'unpublish', $post->id], [
									'class' => 'btn btn-info btn-xs m-r-xs'
								]);

							echo $this->Html->link(
								$icons['image'] . ' Upload',
								['controller' => 'Posts', 'action' => 'upload_image', $post->id, 'featured_img'],
								['escape' => false, 'class' => 'btn btn-xs btn-info m-r-xs']
							);

							echo $this->Html->link($icons['edit'] . 'Edit', ['action' => 'edit', $post->id], [
								'escape' => false,
								'class' => 'btn btn-xs btn-primary m-r-xs'
							]);

							echo $this->Form->postLink($icons['delete'] . 'Delete', ['action' => 'delete', $post->id], [
								'escape' => false,
								'class' => 'btn btn-xs btn-danger',
								'confirm' => 'Are you sure to delete this new?'
							]);
							?>
						</div>
					<?php endif; ?>

				</div>
			</div>
		<?php endif; ?>
	<?php endforeach; ?>
<?php endif; ?>

<?= $this->element('pagination'); ?>
