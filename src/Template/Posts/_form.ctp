<?php

	echo $this->Html->scriptBlock("
		var elem = document.querySelector('.js-switch');
		var init = new Switchery(elem);

		$('.form-group.file').hide();
		
		function togglePinned() {
			
			if ($('#category-id').val() == 1) {
				$('label[for=pinned]').show();
			} else {
				$('label[for=pinned]').hide();
				$('label[for=pinned]').children().removeClass('checked');
				$('#pinned').prop('checked', false);
			}
			
		}
		
		elem.onchange = function() {
			if (this.checked) {
				$('.form-group.file').fadeIn();
				$('.form-group.textarea').hide();
			}
			else {
				$('.form-group.file').hide();
				$('.form-group.textarea').fadeIn();
			}
		};

		$(window).load(function(){
			var elem = document.querySelector('.js-switch');
			
			if (elem.checked) {
				$('.form-group.file').fadeIn();
				$('.form-group.textarea').hide();
			}
		});
		
		$(function() {
			$('fieldset').equalizeHeights();
			
			$(window).resize(function() {
				$('fieldset').equalizeHeights();
			});
			togglePinned();
			$('#category-id').change(function () {
				togglePinned()
			});
		});

		$('#topic').autocomplete({
			appendTo: '#topic-container',
			source: " . safe_json_encode($topics) . "
		});

	", ['block' => true]);


	echo $this->Html->script([
		'plugins/datapicker/bootstrap-datepicker'
	], ['block' => true]);

	echo $this->Html->css('plugins/datapicker/datepicker3', ['block' => true]);

	echo $this->Html->scriptBlock("

		var now = new Date();

		$('#publish-date').datepicker({
			//startDate: now,
			autoclose: true,
			format: 'DD MM dd, yyyy',
			todayBtn: 'linked',
			todayHighlight: true,
			//forceParse: true
		});

	", ['block' => true]);

	if(!isset($post->related_posts) || empty($post->related_posts))
		$post->related_posts = '[]';
	
	echo $this->Html->scriptBlock('
		KhojaApp.controller("RelatedPostsController", ["$scope", "$resource", function($scope, $resource) {
			
			$scope.available = {};
			$scope.selected = ' . $post->related_posts . ';
			
			$resource("' . $this->Url->build(['controller' => 'App', 'action' => 'related_posts', 'prefix' => 'admin']) . '").query(function(data) {
				
				for (var i = 0; i < data.length; i++) {
					var folder = data[i].folder;
					
					if($scope.available[folder] == undefined)
						$scope.available[folder] = [];
					
					$scope.available[folder].push(data[i]);
				}
				
			}, function(rejection) {
				
				if (rejection.status !== -1) //not timeout or cancel
					alert("Could not load posts...");
			});
			
			$scope.$watch("selected", function(selected) {
				$scope.selectedAsJson = angular.toJson(selected, true);
			}, true);
			
			$scope.isSelected = function(folder, index) {
				for (var i = 0; i < $scope.selected.length; i++) {
					if($scope.available[folder][index].model == $scope.selected[i].model && $scope.available[folder][index].id == $scope.selected[i].id) {
						return true;
					}
				}
				
				return false;
			};
			
		}]);
	', ['block' => true]);
?>


<?= $this->element('ckeditor'); ?>

<?= $this->Form->create($post, ['type' => 'file']); ?>

<div class="ibox">
	<div class="ibox-content">
		
		<?= $this->Form->input('name'); ?>
		
		<?= $this->Form->input('topic', [
			'type' => 'text',
			'help' => 'e.g. Event, News, etc.',
			'templates' => [
				'input' => '<div id="topic-container" style="position: relative;"><input type="{{type}}" name="{{name}}" class="form-control{{attrs.class}}" {{attrs}} /></div>',
			]
		]); ?>

		<?php if (!$post->isNew() && $setting->value) {
			echo $this->Form->input('pinned', ['class' => 'test']);
		} ?>
		
		<br />

		<div class="row">
			<div class="col-xs-24 col-sm-12">
				
				<fieldset>
					<legend>Publication Dates</legend>
					
					<?php
							echo $this->Form->input('published', ['label' => 'Date of publication', 'id' => 'publish-date', 'type' => 'text']);
					?>
				</fieldset>
				
			</div>
			<div class="col-xs-24 col-sm-12">
				
				<fieldset>
					<legend>Publication Details</legend>
					
					<?= $this->Form->input('category_id', ['label' => 'Category']); ?>
					<?= $this->Form->input('post_theme_id', ['label' => 'Theme', 'default' => 1]); ?>
					
				</fieldset>
				
			</div>
		</div>
		
		<br />

		<?=
			$this->Form->input('upload_form', [
				'type' => 'checkbox',
				'class' => 'js-switch',
				'label' => 'Do you want to upload a pdf form?',
				'default' => $post->isPdf()
			]);
		?>
		<br />
		<?php
			$uploadPlaceholder = 'Upload a file...';

			if (isset($post->post_upload) && !empty($post->post_upload->name)) {
				$uploadPlaceholder = $post->post_upload->name;
			}

			echo $this->Form->input("post_upload.upload", ['label' => false, 'type' => 'file', 'placeholder' => $uploadPlaceholder]);
		?>

		<?= $this->Form->input('content', ['class' => 'ckeditor']); ?>

		<?= $this->Form->label("Combine with other posts:"); ?>
		
		<div class="row" ng-controller="RelatedPostsController" id="RelatedPostsSelector">
			<div class="col-xs-24" id="RelatedPostsSelected">
				
				<div>
					<div class="list-title">Drag posts from the list on the right</div>
					
					<ul dnd-list="selected">
						<li ng-repeat="item in selected"
							dnd-draggable="item"
							dnd-moved="selected.splice($index, 1)"
							dnd-effect-allowed="move"
						>
							<div class="name"><i class="fa fa-times" ng-click="selected.splice($index, 1)"></i>{{item.name}}</div>
							<div class="info">
								<p class="date">{{item.date}}</p>
								<p class="category">{{item.folder}} - {{item.category}}</p>
							</div>
							
						</li>
					</ul>
				</div>
				
				<?= $this->Form->input('related_posts', [
					'type' => 'hidden',
					'value' => '{{selectedAsJson}}'
				]); ?>
				
			</div>
			<div class="col-xs-24" id="RelatedPostsAvailable">
				
				<uib-tabset active="active" type="pills">
					<uib-tab index="listName" ng-repeat="(listName, list) in available track by listName" heading="{{listName}}">
						
						<ul dnd-list="list">
							<li ng-repeat="item in list track by $index"
								dnd-draggable="item"
								dnd-effect-allowed="copy"
								dnd-disable-if="isSelected(listName, $index)"
								ng-class="{'disabled' : isSelected(listName, $index)}"
							>
								<div class="name">{{item.name}}</div>
								<div class="info">
									<p class="date">{{item.date}}</p>
									<p class="category">{{item.category}}</p>
								</div>
							</li>
						</ul>
						
					</uib-tab>
				</uib-tabset>
				
			</div>
		</div>
		
	</div>
</div>

<div class="submit-big">
	<?= $this->Form->submit('Save', ['class' => 'btn btn-primary']); ?>
</div>

<?= $this->Form->end(); ?>
