<?php

	if ($post->isPending()) {
		echo '<div class="alert alert-warning" style="overflow: hidden;">
			<i class="fa fa-exclamation-circle fa-3x fa-pull-left" aria-hidden="true"></i>
			This is only a preview.<br />This page will only be available after ';
		echo (!empty($post->published)) ? $post->published : 'the start date is set';
		echo '.</div>';
	}
	
	echo $this->element('views/post', ['post' => $post, 'isMain' => true]);

	if(isset($relatedPosts) && !empty($relatedPosts)) {
		foreach($relatedPosts as $relatedPost) {
			
			switch($relatedPost['model']) {
				
				case 'Posts':
					echo $this->element('views/post', ['post' => $relatedPost['data']]);
					break;
				
				case 'Forms':
					echo $this->element('views/form', ['form' => $relatedPost['data']]);
					break;
				
			}
			
		}
	}
	
?>