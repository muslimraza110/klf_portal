<div class="ibox">

	<div class="ibox-title">
		<h3><?= $role ?></h3>
	</div>

	<div class="ibox-content">

		<div class="table-responsive">

			<table class="table table-condensed">

				<thead>
				<?php

				$headers = [
					'User',
					''
				];

				echo $this->Html->tableHeaders($headers);

				?>
				</thead>

				<tbody>
				<?php

				$rows = [];

				foreach ($users as $userId => $user) {

					$actions = [
						'edit' => $this->Html->link(
							'Edit',
							['action' => 'edit', 'controller' => 'Users', $userId],
							['class' => 'btn btn-sm btn-primary']
						)
					];

					$rows[] = [
						$user,
						[implode('', $actions), ['class' => 'compact text-right']]
					];

				}

				echo $this->Html->tableCells($rows);

				?>
				</tbody>

			</table>

		</div>

	</div>

</div>