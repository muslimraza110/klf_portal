<?= $this->Form->create($emailList); ?>
<div class="row">

	<div class="col-xs-24 col-sm-6">

		<div class="ibox">

			<div class="ibox-title">
				<?= $this->Form->input('EmailLists.title', [
					'placeholder' => 'Name',
					'label' => false,
					'class' => 'm-t-sm'
				]) ?>
			</div>

			<div class="ibox-content">

				<h4>Users</h4>

				<?php if (empty($emailList->email_receivers)): ?>
					None Selected
				<?php else: ?>

					<ul style="list-style: none;">

						<?php foreach ($emailList->email_receivers as $receiver): ?>

							<li> <?= $receiver->name ?> - <?= $receiver->email ?></li>

						<?php endforeach; ?>

					</ul>

				<?php endif; ?>

			</div> <!-- ibox-content end -->

		</div> <!-- ibox end -->

	</div> <!-- col end -->

	<div class="col-xs-24 col-sm-18">

		<div class="ibox">

			<div class="ibox-title">
				<h3>Contacts</h3>
			</div>

			<div class="ibox-content">

				<div class="row">

					<?php foreach ($roleGroupedUserLists as $role => $users): ?>

					<div class="col-xs-24 col-sm-6">
						<h4><?= $role ?></h4>
						<?= $this->Form->input('EmailLists.email_receivers._ids', [
							'label' => false,
							'multiple' => 'checkbox',
							'hiddenField' => false,
							'options' => $users
						]); ?>
					</div>

					<?php endforeach; ?>

				</div>

			</div> <!-- ibox-content end -->

		</div> <!-- ibox end -->

	</div> <!-- col end -->

	<div class="col-xs-24">
		<div class="submit-big">
			<?= $this->Form->submit('Submit'); ?>
		</div>
	</div>

</div>
<?= $this->Form->end(); ?>