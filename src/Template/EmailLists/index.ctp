<div class="row">
	<div class="col-xs-24">

		<div class="ibox">

			<div class="ibox-title">
				<h3>Contact Lists</h3>
			</div>

			<div class="ibox-content">

				<div class="table-responsive">

					<table class="table table-condensed">

						<thead>
						<?php

						$headers = [
							'Name',
							'User Count',
							''
						];

						echo $this->Html->tableHeaders($headers);

						?>
						</thead>

						<tbody>
						<?php

						$rows = [];

						foreach ($roleGroupedUserLists as $role => $users) {

							$actions = [
								'view' => $this->Html->link(
									'View',
									['action' => 'view', $role],
									['class' => 'btn btn-sm btn-primary']
								)
							];

							$rows[] = [
								$role,
								count($users),
								[implode('', $actions), ['class' => 'compact text-right']]
							];

						}

						foreach ($emailLists as $emailList) {

							$actions = [
								'edit' => $this->Html->link(
									'Edit',
									['action' => 'edit', $emailList->id],
									['class' => 'btn btn-sm btn-primary']
								),
								'delete' => $this->Form->postLink(
									'Delete',
									['action' => 'delete', $emailList->id],
									[
										'class' => 'btn btn-sm btn-danger',
										'confirm' => 'Are you sure you want to delete this list?'
									]
								)
							];

							$rows[] = [
								$emailList->title,
								count($emailList->email_receivers),
								[implode('', $actions), ['class' => 'compact']]
							];

						}

						echo $this->Html->tableCells($rows);

						?>
						</tbody>

					</table>

				</div>

			</div>

		</div>

	</div>
</div>