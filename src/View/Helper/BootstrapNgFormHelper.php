<?php
namespace App\View\Helper;

//use Cake\View\Helper\FormHelper;

//extends BootstrapFormHelper instead
use Bootstrap\View\Helper\BootstrapFormHelper;

class BootstrapNgFormHelper extends BootstrapFormHelper {
  
  protected function _split_on_ng_tags($string) {
    preg_match_all('/[^{]+|{{[^}]+}}/', $string, $matches);
    return $matches[0];
  }
  
  protected function _explode_but_ignore_angular_vars($string) {
    $a = [];
    $pieces = $this->_split_on_ng_tags($string);
    
    foreach($pieces as $piecesKey => $piece) {
      
      //if does not start with {{
      if (!preg_match('/^{{/', $piece)) {
        
        //remove ending dot if any
        $piece = preg_replace('/^\\.|\\.$/', '', $piece);
        
        //explode on dots if any left
        $pieceExploded = explode('.', $piece);
        
        //place in array to return
        foreach($pieceExploded as $pieceExplodedKey => $piece) {
          $a[] = $piece;
        }
        
      }
      else {
        $a[] = $piece;
      }
    }
    
    return $a;
  }
  
  protected function _initInputField($field, $options = []) {
    
    //if any angular tags found only
    if(preg_match('/{{[^}]+}}/', $field)) {
      $fieldNameArray = $this->_explode_but_ignore_angular_vars($field);
      
      //I am really not sure this will always work as expected
      //I don't understand enough Cake IdGeneratorTrait
      if(!isset($options['id']) || $options['id'] !== false) {
        $idArray = [];
        foreach($fieldNameArray as $piece) {
          
          //if does not start with {{
          if (!preg_match('/^{{/', $piece)) {
            $idArray[] = $this->_domId($piece);
          }
          else {
            $idArray[] = $piece;
          }
        }
        
        $options['id'] = implode('-', $idArray);
      }
      
      if (!isset($options['name'])) {
        $endsWithBrackets = '';
        if (substr($field, -2) === '[]') {
          $field = substr($field, 0, -2);
          $endsWithBrackets = '[]';
        }
        
        $parts = $fieldNameArray; //This line is the only difference with the original cake code. It was: explode('.', $field);
        $first = array_shift($parts);
        $options['name'] = $first . (!empty($parts) ? '[' . implode('][', $parts) . ']' : '') . $endsWithBrackets;
      }
    }
    
    return parent::_initInputField($field, $options);
  }
  
}