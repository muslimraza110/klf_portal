<?php

namespace App\View\Helper;

use Cake\View\Helper;

class StepProgressHelper extends Helper {

	public $helpers = ['Html'];

	public function render(array $steps) {

		$columnWidth = floor(24 / count($steps));
		$columnOffset = floor((24 % count($steps))/2);

		$return = <<<HTML
<div class="col-xs-24 col-xs-offset-$columnOffset">
HTML;


		$return .= '<div class="row">'.PHP_EOL;

		foreach ($steps as $step) {

			static $class = 'complete';

			if (!empty($step['active'])) {
				$class = 'active';
			}


			if (!empty($step['route'])) {
				$link = $this->Html->link($step['name'], $step['route'], ['class' => 'text-center']).PHP_EOL;
			} else {
				$link = '<span class="text-center">'.$step['name'].'</span>'.PHP_EOL;
			}

			if (!empty($step['active'])) {
				$link = '<strong>' . $link . '</strong>';
			}

			$return .= <<<HTML
<div class="col-xs-$columnWidth $class text-center">
	$link
</div>

HTML;

			if (!empty($step['active']))
				$class = 'disabled';

		}

		$return .= '</div></div>'.PHP_EOL;

		return $return;

	}

}