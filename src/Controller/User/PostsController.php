<?php
namespace App\Controller\User;

use Cake\Network\Exception\NotFoundException;
use Cake\I18n\Time;

class PostsController extends \App\Controller\PostsController {

  public function index($categoryId) {
    $this->_index($categoryId);
  }

  public function view($id) {
  	$this->_view($id);
  }

  public function download($id) {
  	$this->_download($id);
  }

}
