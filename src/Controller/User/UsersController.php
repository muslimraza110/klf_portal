<?php

namespace App\Controller\User;

class UsersController extends \App\Controller\UsersController {

	public function profile() {

		$this->_profile();

	}

	public function edit_profile() {

		$this->_edit_profile();

	}

	public function index() {

		$this->_index();

	}

	public function view($id) {

		$this->_view($id);

	}

	public function edit($id) {

		$this->_edit($id);

	}

	public function family($id, $userFamilyId = null) {

		$this->_family($id, $userFamilyId);

	}

	public function delete_family($id, $userFamilyId) {

		$this->_delete_family($id, $userFamilyId);

	}

	public function change_password() {

		$this->_change_password();

	}

	public function ajax_autocomplete_user_list() {

		$this->_ajax_autocomplete_user_list();

	}

	public function upload_image($id, $imageType) {

		$this->_upload_image($id, $imageType);

	}

	public function ajax_reset_image() {

		$this->_ajax_reset_image();

	}

	public function ajax_delete_image() {

		$this->_ajax_delete_image();

	}

	public function ajax_html_user_list() {

		$this->_ajax_html_user_list();

	}

	public function ajax_html_group_list() {

		$this->_ajax_html_group_list();

	}

	public function ajax_request_group_access() {

		$this->_ajax_request_group_access();

	}

	public function ajax_cancel_group_access_request() {

		$this->_ajax_cancel_group_access_request();

	}

	public function ajax_clear_comments() {

		$this->_ajax_clear_comments();

	}

	public function dashboard() {

		$this->_dashboard();

	}

	public function ajax_request_project_access() {

		$this->_ajax_request_project_access();

	}

	public function ajax_cancel_project_access_request() {

		$this->_ajax_cancel_project_access_request();

	}

}
