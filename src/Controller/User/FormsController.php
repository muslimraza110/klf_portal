<?php
namespace App\Controller\User;

class FormsController extends \App\Controller\FormsController {

	public function index() {
		$this->_index();
	}

	public function view($id) {
		$this->_view($id);
	}

	public function save_answers($id) {
		$this->_save_answers($id);
	}

	public function download($id) {
		$this->_download($id);
	}

}
