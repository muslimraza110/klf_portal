<?php

namespace App\Controller\User;

class AssetsController extends \App\Controller\AssetsController {

	public function view($fileId) {

		$this->_view($fileId);

	}

	public function download($fileId) {

		$this->_download($fileId);

	}

}