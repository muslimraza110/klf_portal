<?php

namespace App\Controller\User;

class GalleriesController extends \App\Controller\GalleriesController {

	public function index() {
		$this->_index();
	}

	public function view($folderId) {
		$this->_view($folderId);
	}

}