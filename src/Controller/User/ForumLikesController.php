<?php
namespace App\Controller\User;

class ForumLikesController extends \App\Controller\ForumLikesController 
{
	public function ajax_like($dislike = false)
	{
		$this->_ajax_like($dislike);
	}

	public function view($forumPostId) {
		$this->_view($forumPostId);
	}

}