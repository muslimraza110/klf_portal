<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Utility\Inflector;

class PagesController extends \App\Controller\AppController {

  public function initialize() {
    parent::initialize();
  }

  public function display() {
    $path = func_get_args();
    
    $count = count($path);
    
    if (!$count) {
      return $this->redirect('/');
    }
    
    $page = $subpage = null;
    
    if (!empty($path[0])) {
      $page = $path[0];
      $title = Inflector::humanize($page);
    }
    
    if (!empty($path[1])) {
      $subpage = $path[1];
      $title .= ' / ' . Inflector::humanize($subpage);
    }
    
    $this->set(compact('title', 'page', 'subpage'));
    
    try {
      $this->render(implode('/', $path));
    }
    catch (MissingTemplateException $e) {
      if (Configure::read('debug')) {
        throw $e;
      }
      throw new NotFoundException();
    }
  }

  public function terms_and_conditions() {
    $this->viewBuilder()->layout('empty');

    if ($this->request->is(['post', 'put'])) {
      $this->redirect($this->_authUser->home_route);
    }
  }

	protected function _faq() {

		$faq = $this->Pages->findByTitle('faq')->first();

		$this->set(compact('faq'));

		$this->render('/Pages/faq');

	}

	protected function _edit_faq() {

		$faq = $this->Pages->findByTitle('faq')->first();

		if (empty($faq)) {

			$faq = $this->Pages->newEntity([
				'title' => 'faq'
			]);

		}

		if ($this->request->is(['post', 'put'])) {

			$faq = $this->Pages->patchEntity($faq, $this->request->data);

			if ($this->Pages->save($faq)) {

				$this->Flash->success('FAQ Page has been saved successfully.');

				$this->redirect(['action' => 'faq']);

			} else {
				$this->Flash->error('An error occurred. FAQ Page could not be saved.');
			}

		}

		$this->set(compact('faq'));

		$this->render('/Pages/edit_faq');

	}

	protected function _mission() {

		$mission = $this->Pages->findByTitle('mission')->first();

		$this->set(compact('mission'));

		$this->render('/Pages/mission');

	}

	protected function _edit_mission() {

		$mission = $this->Pages->findByTitle('mission')->first();

		if (empty($mission)) {

			$mission = $this->Pages->newEntity([
				'title' => 'mission'
			]);

		}

		if ($this->request->is(['post', 'put'])) {

			$mission = $this->Pages->patchEntity($mission, $this->request->data);

			if ($this->Pages->save($mission)) {

				$this->Flash->success('Mission Page has been saved successfully.');

				$this->redirect(['action' => 'mission']);

			} else {

				$this->Flash->error('An error occurred. Mission Page could not be saved.');

			}

		}

		$this->set(compact('mission'));

		$this->render('/Pages/edit_mission');

	}

}