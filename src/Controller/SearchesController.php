<?php
namespace App\Controller;

class SearchesController extends \App\Controller\AppController {
	
	protected function _index() {		
		$terms = '';

		if (!empty($this->request->query)) {
			$results = $this->Search->scope($this->request->query);
		}
		
		if (!empty($this->request->query['q'])) {
      $this->request->data['q'] = $this->request->query['q'];
      $terms = explode(' ', trim($this->request->query['q']));
    }

    if (!empty($this->request->query['scope'])) {
      $this->request->data['scope'] = $this->request->query['scope'];
    }

		$title = 'Search Results';
		$titleIcon = $this->_icons['search'];

		$this->set(compact('title', 'titleIcon', 'results', 'terms'));
		$this->render(DS . 'Searches' . DS . 'index');
	}
	
}