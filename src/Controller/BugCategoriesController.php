<?php

namespace App\Controller;

class BugCategoriesController extends AppController {

	protected function _index() {

		$bugCategories = $this->BugCategories->find();

		$title = 'Bug Categories';
		$breadcrumbs = [
			['name' => 'Bugs', 'route' => ['controller' => 'Bugs', 'action' => 'index']],
			['name' => 'Bug Categories', 'active' => true]
		];

		$this->set(compact('bugCategories', 'title', 'breadcrumbs'));

		$this->render('/BugCategories/index');
	}

	public function _add() {

		$bugCategories = $this->BugCategories->newEntity();

		$title = 'Add Category';
		$breadcrumbs = [
			['name' => 'Bugs', 'route' => ['controller' => 'Bugs', 'action' => 'index']],
			['name' => 'Bug Categories', 'route' => ['controller' => 'BugCategories', 'action' => 'index']],
			['name' => 'Add Category', 'active' => true]
		];

		$this->_form($bugCategories, $title, $breadcrumbs);

	}

	public function _edit($id) {

		$bugCategories = $this->BugCategories->find()
			->where(['id' => $id])
			->first();

		$title = $bugCategories->name;
		$breadcrumbs = [
			['name' => 'Bugs', 'route' => ['controller' => 'Bugs', 'action' => 'index']],
			['name' => 'Bug Categories', 'route' => ['controller' => 'BugCategories', 'action' => 'index']],
			['name' => $title, 'active' => true]
		];

		$this->_form($bugCategories, $title, $breadcrumbs);
	}

	protected function _form($bugCategories, $title, $breadcrumbs) {

		if ($this->request->is(['post', 'put'])) {

			$bugCategories = $this->BugCategories->patchEntity($bugCategories, $this->request->data);

			if ($this->BugCategories->save($bugCategories)) {
				$this->Flash->success('The Category has been saved.');
				return $this->redirect(['action' => 'index']);
			}
			else
				$this->Flash->error('An error occurred while trying to save the Category.');

		}

		$this->set(compact('bugCategories', 'breadcrumbs', 'title', 'types'));
		$this->render('/BugCategories/_form');

	}

	public function _delete($id) {
		$this->autoRender = false;

		if (! $this->BugCategories->exists($id))
			throw new NotFoundException();

		$bugCategory = $this->BugCategories->find()
			->where(['id' => $id])
			->first();

		$this->BugCategories->delete($bugCategory);

		$this->Flash->success('Bug Category has been deleted');
		$this->redirect(['action' => 'index']);
	}

}