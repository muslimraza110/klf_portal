<?php
namespace App\Controller;

use Cake\Collection\Collection;
use Cake\I18n\Number;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;

class FundraisingsController extends AppController {

	protected function _index() {

		if (!empty($status = $this->request->query('status'))) {
			$this->request->data['status'] = $status;
		} else {
			$status = 'A';
		}

		$this->paginate = [
			'contain' => [
				'Users',
				'FundraisingPledges',
				'Initiatives'
			],
			'limit' => 30,
			'sortWhitelist' => [
				'name', 'Users.first_name', 'amount', 'type', 'Initiatives.name', 'created'
			],
			'conditions' => [
				'Fundraisings.status' => $status
			]
		];

		$fundraisings = (new Collection($this->paginate()))
			->each(function ($value, $key) {
				$value->amount_pledged = (float)(new Collection($value->fundraising_pledges))->sumOf('amount');
			});

		$title = 'Fundraising';
		$titleIcon = $this->_icons['dollar'];

		if (!in_array($this->_authUser->role, ['A', 'O'])) {
			$this->set('bannerActions', []);
		}

		$this->set(compact('title', 'titleIcon', 'fundraisings'));

		$this->render('/Fundraisings/_index');

	}

	protected function _add() {

		if (!in_array($this->_authUser->role, ['A', 'O'])) {
			throw new NotFoundException;
		}

		$fundraising = $this->Fundraisings->newEntity([
			'user_id' => $this->_authUser->id,
			'type' => 'B',
			'fundraising_users'
		]);

		$breadcrumbs = [
			['name' => 'Fundraising', 'route' => ['action' => 'index']],
			['name' => 'Add Fundraising', 'active' => true]
		];

		$this->set(compact('breadcrumbs'));

		$this->_form($fundraising);

	}

	protected function _edit($id) {

		if (!in_array($this->_authUser->role, ['A', 'O'])) {
			throw new NotFoundException;
		}

		$fundraising = $this->Fundraisings->get($id, [
			'contain' => [
				'FundraisingUsers' => [
					'Users'
				]
			]
		]);

		if ($fundraising->status == 'R') {
			throw new ForbiddenException;
		}

		$breadcrumbs = [
			['name' => 'Fundraising', 'route' => ['action' => 'index']],
			['name' => 'Edit Fundraising', 'active' => true]
		];

		$this->set(compact('breadcrumbs'));

		$this->_form($fundraising);

	}

	protected function _delete($id) {

		$this->request->allowMethod('post');

		if (!in_array($this->_authUser->role, ['A', 'O'])) {
			throw new NotFoundException;
		}

		$fundraising = $this->Fundraisings->get($id);

		$fundraising->status = 'D';

		if ($this->Fundraisings->save($fundraising)) {
			$this->Flash->success('Fundraising has been deleted.');
		} else {
			$this->Flash->error('Fundraising could not be deleted.');
		}

		return $this->redirect(['action' => 'index']);

	}

	protected function _archive($id) {

		$this->request->allowMethod('post');

		if (!in_array($this->_authUser->role, ['A', 'O'])) {
			throw new NotFoundException;
		}

		$fundraising = $this->Fundraisings->get($id, [
			'contain' => [
				'Initiatives'
			]
		]);


			$fundraising->initiative->status = 'R';
			$this->Fundraisings->Initiatives->save($fundraising->initiative);


		$fundraising->status = 'R';

		if ($this->Fundraisings->save($fundraising)) {
			$this->Flash->success('Fundraising has been archived.');
		} else {
			$this->Flash->error('Fundraising could not be archived.');
		}

		return $this->redirect(['action' => 'index', '?' => ['status' => 'R']]);

	}

	protected function _view($id) {

		$fundraising = $this->Fundraisings->get($id, [
			'contain' => [
				'FundraisingPledges' => [
					'Users',
					'sort' => [
						'FundraisingPledges.created' => 'DESC'
					]
				],
				'FundraisingUsers' => [
					'Users'
				]
			]
		]);

		$fundraisingAmountPledged = (float)(new Collection($fundraising->fundraising_pledges))->sumOf('amount');

		$authUserAmountPledged = (float)$this->Fundraisings->FundraisingPledges
			->findByFundraisingIdAndUserId($id, $this->_authUser->id)
			->where([
				'FundraisingPledges.amount !=' => 0
			])
			->select([
				'amount_total' => 'SUM(FundraisingPledges.amount)'
			])
			->first()
			->amount_total;

		$highestPledge = $this->Fundraisings->FundraisingPledges->findByFundraisingId($id)
			->order([
				'FundraisingPledges.amount' => 'DESC'
			])
			->first();

		$title = 'Fundraising';
		$titleIcon = $this->_icons['dollar'];
		$breadcrumbs = [
			['name' => 'Fundraisings', 'route' => ['action' => 'index']],
			['name' => $title, 'active' => true]
		];

		$bannerActions = [
			$this->_icons['back'] => ['action' => 'index', '?' => ['status' => $fundraising->status]]
		];

		$this->set(compact('title', 'titleIcon', 'breadcrumbs', 'bannerActions', 'fundraising', 'fundraisingAmountPledged', 'authUserAmountPledged', 'highestPledge'));

		$this->render('/Fundraisings/_view');

	}

	protected function _pledge($id) {

		$this->request->allowMethod('post');

		$fundraising = $this->Fundraisings->get($id, [
			'contain' => [
				'FundraisingPledges',
				'Initiatives' => [
					'Projects' => [
						'Creators',
						'Groups'
					]
				]
			]
		]);

		if ($fundraising->status == 'R') {
			throw new ForbiddenException;
		}

		$fundraisingAmountPledged = (float)(new Collection($fundraising->fundraising_pledges))->sumOf('amount');

		$pledge = 0;

		$anonymous = $hideAmount = false;

		if ($fundraising->type == 'B') {

			$authUserPledge = $this->Fundraisings->FundraisingPledges
				->findByFundraisingIdAndUserId($id, $this->_authUser->id)
				->where([
					'FundraisingPledges.amount !=' => 0
				])
				->first();


			if (
				$fundraisingAmountPledged >= $fundraising->amount
				|| !empty($authUserPledge)
			) {
				throw new ForbiddenException;
			}

			$pledge = round($fundraising->amount / $fundraising->blocks, 2) * $this->request->data('pledge_blocks');
			$anonymous = $this->request->data('anonymous') ? $this->request->data('anonymous') : false;
			$hideAmount = $this->request->data('hide_amount') ? $this->request->data('hide_amount') : false;

		}

		if (in_array($fundraising->type, ['P', 'M'])) {

			if (empty($this->request->data['amount'])) {
				throw new ForbiddenException;
			}

			$pledge = $this->request->data('amount');

			$anonymous = $this->request->data('anonymous') ? $this->request->data('anonymous') : false;
			$hideAmount = $this->request->data('hide_amount') ? $this->request->data('hide_amount') : false;

		}

		if ($pledge < 0.01) {
			throw new ForbiddenException;
		}

		if (isset($this->request->data['group_id'])) {
			$charity = $this->request->data['group_id'];
		}

		$fundraisingPledge = $this->Fundraisings->FundraisingPledges->newEntity([
			'fundraising_id' => $id,
			'user_id' => $this->_authUser->id,
			'group_id' => $charity,
			'amount' => $pledge,
			'anonymous' => $anonymous,
			'hide_amount' => $hideAmount
		]);

		$this->_authUser->sendPledgeNotification($fundraising, $fundraisingPledge);

		if ($this->Fundraisings->FundraisingPledges->save($fundraisingPledge)) {
			$this->Flash->success('Your pledge of '.Number::format($pledge, ['places' => 0, 'before' => 'USD ']).' has been recorded.');
		} else {
			$this->Flash->error('An error occurred. Your pledge could not be recorded.');
		}

		if (isset($fundraising->initiative_id)) {
			return $this->redirect(['controller' => 'Initiatives', 'action' => 'view', $fundraising->initiative_id]);
		} else {
			return $this->redirect(['action' => 'view', $fundraising->id]);
		}

	}

	protected function _form($fundraising) {

		$this->loadModel('EmailLists');

		if ($this->request->is(['post', 'put'])) {

			$this->request->data['amount'] = str_replace(',', '', $this->request->data('amount'));

			$error = false;

			if ($this->request->data['type'] == 'M') {

				$this->request->data['amount'] = 0;

				foreach ($this->request->data['fundraising_users'] as $key => $user) {

					$this->request->data['fundraising_users'][$key]['amount'] = $user['amount'] = str_replace(',', '', $user['amount']);
					$this->request->data['amount'] += $user['amount'];

				}

			}

			if ($this->request->data['type'] == 'B') {

				if (empty($this->request->data['blocks']) || empty($this->request->data['max_blocks'])) {

					$error = true;
					$this->Flash->error('If fundraising type "block" is selected, specify the amount of blocks.');

				} elseif (!is_int($this->request->data['amount'] / $this->request->data['blocks'])) {

					$error = true;
					$this->Flash->error('Target and amount of blocks need the be divisible without remainder.');

				}

			} else {
				$this->request->data['blocks'] = null;
				$this->request->data['max_blocks'] = 0;
			}

			$associated = ['FundraisingUsers'];

			$fundraising = $this->Fundraisings->patchEntity(
				$fundraising,
				$this->request->data,
				['associated' => $associated]
			);

			foreach ($fundraising->fundraising_users as $user) {
				if ($user->user_id == null) {
					$error = true;
					$this->Flash->error('Please Input a Name for the User.');
				}
			}

			if (!$error) {

				if ($this->Fundraisings->save($fundraising)) {

					$this->Flash->success('Fundraising saved successfully.');

					$this->redirect(['action' => 'view', $fundraising->id]);

				} else {
					$this->Flash->error('An error occurred. Fundraising could not be saved.');
				}

			}

		}

		$contactList = $this->EmailLists->getRoleGroupedUserList();
		$emailLists = $this->EmailLists->getGroupedEmailLists();

		$types = $this->Fundraisings->types();

		$titleIcon = $this->_icons['dollar'];

		$this->set(compact('titleIcon', 'fundraising', 'projects', 'projectInitiatives', 'types', 'contactList', 'emailLists'));

		$this->render('/Fundraisings/_form');

	}

	protected function _edit_pledge($id) {

		$fundraisingPledge = $this->Fundraisings->FundraisingPledges->get($id, [
			'contain' => [
				'Fundraisings'
			]
		]);

		if ($this->request->is(['post', 'put'])) {

			if ($fundraisingPledge->fundraising->type == 'B') {

				if ($this->request->data['amount_pending'] != -1) {

					$this->request->data['amount_pending'] = round($fundraisingPledge->fundraising->amount / $fundraisingPledge->fundraising->blocks, 2) * $this->request->data['amount_pending'];

				}

			}

			$fundraisingPledge = $this->Fundraisings->FundraisingPledges->patchEntity($fundraisingPledge, $this->request->data);
			if ($this->Fundraisings->FundraisingPledges->save($fundraisingPledge)) {
				$this->Flash->success(__('The fundraising  pledge has been saved.'));
				return $this->redirect(['controller' => 'Users', 'action' => 'dashboard']);
			} else {
				$this->Flash->error(__('The fundraising  pledge could not be saved. Please, try again.'));
			}

		}

		$this->set(compact('fundraisingPledge'));
		$this->render(DS. 'Fundraisings' . DS . '_edit_pledge');

	}

	protected function _review_changes() {

		// View null = Pending, A = Approved, D = Denied
		$view = null;

		if (isset($this->request->query['v']) && !empty($this->request->query['v'])) {
			$view = $this->request->data['v'] = $this->request->query['v'];
		}

		$this->paginate = [
			'contain' => [
				'Fundraisings' => [
					'Initiatives' => [
						'Projects'
					],
					'Projects'
				],
				'Users'
			],
			'sortWhitelist' => [
				'amount',
				'amount_pending',
				'Users.first_name',
				'Fundraisings.type',
				'Fundraisings.fundraising_status'
			]
		];

		if (empty($view)) {
			$conditions = [
				'OR' => [
					'FundraisingPledges.amount_pending <' => 0,
					'FundraisingPledges.amount_pending IS NOT' => null
				],
				'FundraisingPledges.amount_pending !=' => 0,
			];
		} else {
			$conditions = [
				'FundraisingPledges.admin_response IS' => $view
			];
		}

		$query = $this->Fundraisings->FundraisingPledges->find()
			->where($conditions)
			->order('FundraisingPledges.created DESC');

		$fundraisingPledges = $this->paginate($query);

		$this->set(compact('fundraisingPledges'));
		$this->render(DS. 'Fundraisings' . DS . '_review_changes');

	}

	protected function _my_pledges() {

		$this->paginate = [
			'contain' => [
				'Fundraisings',
			],
			'sortWhitelist' => [
				'amount',
				'amount_pending',
				'Fundraisings.type',
				'Fundraisings.fundraising_status'
			],
			'conditions' => [
				'FundraisingPledges.user_id' => $this->_authUser->id
			],
		];

		$query = $this->Fundraisings->FundraisingPledges->find()
			->orderDesc('FundraisingPledges.modified');

		$fundraisingPledges = $this->paginate($query);

		$this->set(compact('fundraisingPledges'));
		$this->render(DS. 'Fundraisings' . DS . '_my_pledges');

	}


	protected function _ajax_admin_response() {

		$id = $this->request->data['id'];
		$adminResponse = $this->request->data['response'];

		$response = ['status' => 'error'];

		$fundraising = $this->Fundraisings->FundraisingPledges->get($id, [
			'contain' => [
				'Fundraisings',
				'Users'
			]
		]);

		if ($adminResponse == 'A') {

			$fundraising = $this->Fundraisings->FundraisingPledges->patchEntity($fundraising, [
				'amount' => $fundraising->amount_pending,
				'amount_pending' => 0,
				'admin_response' => 'A',
				'from_amount' => $fundraising->amount
			], ['validate' => false]);

		}


		if ($adminResponse == 'D') {

			$fundraising = $this->Fundraisings->FundraisingPledges->patchEntity($fundraising, [
				'admin_response' => 'D'
			]);

		}

		if ($this->Fundraisings->FundraisingPledges->save($fundraising)) {

			$response = ['status' => 'ok'];

		}

		if ($response['status'] == 'ok') {

			$fundraising->user->sendPledgeRequestNotification($fundraising);

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

}
