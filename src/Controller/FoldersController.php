<?php

namespace App\Controller;

use Cake\Network\Exception\InternalErrorException;

class FoldersController extends AppController {

	public function initialize() {

		parent::initialize();

		$this->loadComponent('RequestHandler');

	}

	protected function _index() {

		$this->Folders->recover();

		$folders = $this->Folders->find('threaded');

		$title = 'Assets';
		$titleIcon = $this->_icons['file-image-o'];
		$breadcrumbs = [
			['name' => $title, 'active' => true]
		];

		$user = $this->_authUser;

		$this->set(compact('folders', 'title', 'titleIcon', 'breadcrumbs', 'user'));

		$this->render('/Folders/_index');

	}

	protected function _add() {

		$folder = $this->Folders->newEntity();

		$title = 'New Folder (Assets)';
		$titleIcon = $this->_icons['file-image-o'];
		$breadcrumbs = [
			['name' => 'Assets', 'route' => ['action' => 'index']],
			['name' => $title, 'active' => true]
		];

		$this->set(compact('title', 'titleIcon', 'breadcrumbs'));

		$this->_form($folder);
	}

	protected function _edit($id) {

		$folder = $this->Folders->get($id);

		$title = 'Edit Folder (Assets)';
		$titleIcon = $this->_icons['file-image-o'];
		$breadcrumbs = [
			['name' => 'Assets', 'route' => ['action' => 'index']],
			['name' => $title, 'active' => true]
		];

		$this->set(compact('title', 'titleIcon', 'breadcrumbs'));

		$this->_form($folder);
	}

	protected function _form($folder) {

		if ($this->request->is(['post', 'put'])) {

			$folder = $this->Folders->patchEntity($folder, $this->request->data);

			if ($this->Folders->save($folder)) {
				$this->Flash->success('Folder saved successfully.');
				return $this->redirect(['action' => 'index']);
			}
			else {
				$this->Flash->error('Error occurred while trying to save.');
			}
		}

		$parents = $this->Folders->find('treeList');
		$this->set(compact('folder', 'parents'));

		$this->render('/Folders/_form');
	}

	protected function _delete($id) {
		$this->request->allowMethod(['post', 'delete']);

		$folder = $this->Folders->get($id);

		if(!$folder)
			throw new NotFoundException();

		if ($this->Folders->delete($folder)) {
			$this->Flash->success('The folder "' . $folder->name . '" has been deleted.');
			return $this->redirect(['action' => 'index']);
		}
		else {
			$this->Flash->error('The folder "' . $folder->name . '" could\'t be deleted.');
		}
	}

	protected function _view($id) {

		if (! $this->Folders->exists($id))
			throw new NotFoundException();

		$folder = $this->Folders->get($id);

		if (!empty($this->request->data['resource']))
			$this->_upload($id, $this->request->data);

		$title = '' . $folder->name . ' (Assets)';
		$titleIcon = $this->_icons['file-image-o'];
		$breadcrumbs = [
			['name' => 'Assets', 'route' => ['action' => 'index']],
			['name' => $title, 'active' => true]
		];

		$this->set(compact('title', 'titleIcon', 'breadcrumbs', 'folder'));

		$this->render('/Folders/_view');

	}

	protected function _ajax_save() {

		if(!isset($this->request->data['list']))
			throw new InternalErrorException();

		$list = $this->request->data['list'];

		$this->Folders->saveRecursiveList($list);

		$response = ['status' => 'ok'];

		//$this->Flash->success('Folders saved.');

		$this->RequestHandler->renderAs($this, 'json');

		$this->set(compact('response'));
		$this->set('_serialize', 'response');
	}

	protected function _upload($id, array $data) {

		if (! $this->Folders->exists($id))
			throw new InternalErrorException();

		$file = $this->Folders->Files->newEntity($data, ['associated' => ['resource']]);

		$file->folder_id = $id;

		$this->Folders->Files->save($file);

		$response = ['status' => 'ok'];

		$this->RequestHandler->renderAs($this, 'json');

		$this->set(compact('response'));
		$this->set('_serialize', 'response');
	}

}