<?php
namespace App\Controller\Admin;

class BugCategoriesController extends \App\Controller\BugCategoriesController
{
	public function index() {

		$this->_index();

	}

	public function edit($id) {

		$this->_edit($id);

	}

	public function add() {

		$this->_add();

	}

	public function delete($id) {

		$this->_delete($id);

	}

}