<?php
namespace App\Controller\Admin;


class KhojaCareCategoriesController extends \App\Controller\KhojaCareCategoriesController {

	public function index() {

		$this->_index();

	}


	public function view($id) {

		$this->_view($id);

	}

	public function add() {

		$this->_add();

	}

	public function edit($id) {

		$this->_edit($id);

	}


	public function delete($id) {

		$this->_delete($id);

	}

	public function ajax_reorder_khojaCareCategory_list() {

		$this->_ajax_reorder_khojaCareCategory_list();

	}

}
