<?php

namespace App\Controller\Admin;

class TagsController extends \App\Controller\TagsController {

	public function index() {

		$this->_index();

	}

	public function add() {

		$this->_add();

	}

	public function edit($id) {

		$this->_edit($id);

	}

	public function delete($id) {

		$this->_delete($id);

	}
	public function multi_level_tag() {

		$this->_multi_level_tag();

	}

	public function multi_level_tag_add($parentId = null) {

		$this->_multi_level_tag_add($parentId);

	}

	public function multi_level_tag_edit($id) {

		$this->_multi_level_tag_edit($id);

	}

	public function multi_level_tag_delete($id) {

		$this->_multi_level_tag_delete($id);

	}

	public function ajaxSaveSorting() {

		$this->_ajaxSaveSorting();

	}


}