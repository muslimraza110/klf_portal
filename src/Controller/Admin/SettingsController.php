<?php

namespace App\Controller\Admin;

class SettingsController extends \App\Controller\SettingsController {

	public function index() {

		$this->_index();

	}

	public function edit($id) {

		$this->_edit($id);

	}

}