<?php
namespace App\Controller\Admin;

class FaqsController extends \App\Controller\FaqsController {

	public function index($list = null) {
		$this->_index($list);
	}

	// public function view($id) {
	// 	$this->_view($id);
	// }

	public function add($id = null) {
		$this->_add($id);
	}

	public function edit($id) {
		$this->_edit($id);
	}

	public function delete($id) {
		$this->_delete($id);
	}

	public function ajax_reorder_faq_list() {
		$this->_ajax_reorder_faq_list();
	}

}