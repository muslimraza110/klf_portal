<?php

namespace App\Controller\Admin;

class UserVotesController extends \App\Controller\UserVotesController {

	public function ajax_vote() {

		$this->_ajax_vote();

	}

	public function ajax_final_decision() {

		$this->_ajax_final_decision();

	}

}