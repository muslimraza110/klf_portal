<?php

namespace App\Controller\Admin;

class FilesController extends \App\Controller\FilesController {

	public function index() {

		$this->_index();

	}

	public function delete($id) {

		$this->_delete($id);

	}

	public function download($id) {

		$this->_download($id);

	}

	public function ajax_view($folderId) {

		$this->_ajax_view($folderId);

	}

	public function ajax_image_list($folderId) {
		$this->_ajax_image_list($folderId);
	}

}