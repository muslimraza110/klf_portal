<?php
namespace App\Controller\Admin;

class EmailListsController extends \App\Controller\EmailListsController {

	public function index() {

		$this->_index();

	}

	public function add() {

		$this->_add();

	}

	public function edit($id) {

		$this->_edit($id);

	}

	public function view($role) {

		$this->_view($role);

	}

	public function delete($id) {

		$this->_delete($id);

	}

}