<?php
namespace App\Controller\Admin;

class InitiativesController extends \App\Controller\InitiativesController {

	public function index() {

		$this->_index();

	}

	public function add() {

		$this->_add();

	}

	public function edit($id) {

		$this->_edit($id);

	}

	public function delete($id) {

		$this->_delete($id);

	}

	public function stepInitiative() {

		$this->_stepInitiative();

	}

	public function stepProjects() {

		$this->_stepProjects();

	}

	public function stepDonations() {

		$this->_stepDonations();

	}

	public function stepFundraisings() {

		$this->_stepFundraisings();

	}

	public function stepSummary() {

		$this->_stepSummary();

	}

	public function emailPreview() {

		$this->_emailPreview();

	}

	public function view($id) {

		$this->_view($id);

	}

	public function view_eblast($id) {

		$this->_viewEblast($id);

	}

	public function archive($id) {

		return $this->_archive($id);

	}

	public function reactivate($id) {

		return $this->_reactivate($id);

	}

	public function pledges($initiativeId) {

		return $this->_pledges($initiativeId);

	}

	public function editPledge($id) {

		return $this->_editPledge($id);

	}

	public function deletePledge($id) {

		return $this->_deletePledge($id);

	}

	public function communication($id) {

		return $this->_communication($id);

	}

	public function pinEmail($id) {

		return $this->_pinEmail($id);

	}

}