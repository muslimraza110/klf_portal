<?php

namespace App\Controller\Admin;

class RoxyFilemanController extends \App\Controller\RoxyFilemanController {

	public function dirtree() {

		$this->_dirtree();

	}

	public function fileslist() {

		$this->_fileslist();

	}

	public function createdir() {

		$this->_createdir();

	}

	public function deletedir() {

		$this->_deletedir();

	}

	public function upload() {

		$this->_upload();

	}

	public function deletefile() {

		$this->_deletefile();

	}

}