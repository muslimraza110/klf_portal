<?php
namespace App\Controller\Admin;

class BugsController extends \App\Controller\BugsController {

	public function index() {
		$this->_index();
	}
	
	public function edit($id) {
		$this->_edit($id);
	}

	public function add() {
		$this->_add();
	}

	public function updateStatus($id) {
		$this->_updateStatus($id);
	}

	public function returnPending($id) {
		$this->_returnPending($id);
	}

	public function complete($id) {
		$this->_complete($id);
	}

	public function feedback() {

		$this->_feedback();

	}
	
}
