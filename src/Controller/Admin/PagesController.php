<?php

namespace App\Controller\Admin;

class PagesController extends \App\Controller\PagesController {

	public function faq() {

		$this->_faq();

	}

	public function edit_faq() {

		$this->_edit_faq();

	}

	public function mission() {

		$this->_mission();

	}

	public function edit_mission() {

		$this->_edit_mission();

	}

}