<?php
namespace App\Controller\Admin;

class ForumPostsController extends \App\Controller\ForumPostsController
{
	public function index($scope = 'public') {
		$this->_index($scope);
	}

	public function view($id) {
		$this->_view($id);
	}

	public function ajax_add_comment() {
		$this->_ajax_add_comment();
	}

	public function eblast($id) {
		$this->_eblast($id);
	}
	public function ajax_html_email_user_list() {
		$this->_ajax_html_email_user_list();
	}

	public function add($scope = null, $scopeId = null) {
		$this->_add($scope, $scopeId);
	}

	public function edit($id) {
		$this->_edit($id);
	}

	public function delete($id) {
		$this->_delete($id);
	}

	public function approve($id) {

		$this->_approve($id);

	}

	public function add_subtopic($model) {
		$this->_add_subtopic($model);
	}

	public function delete_subtopic($id, $scope = 'admin') {
		$this->_delete_subtopic($id, $scope);
	}

	public function view_subtopic($id) {
		$this->_view_subtopic($id);
	}

	public function add_subtopic_post($id) {
		$this->_add_subtopic_post($id);
	}


	public function ajax_html_moderator_list() {

		$this->_ajax_html_moderator_list();

	}

	public function ajax_html_whitelist_list() {

		$this->_ajax_html_whitelist_list();

	}

	public function ajax_add_moderator() {

		$this->_ajax_add_moderator();

	}

	public function ajax_add_whitelist() {

		$this->_ajax_add_whitelist();

	}

	public function ajax_remove_moderator() {

		$this->_ajax_remove_moderator();

	}

	public function ajax_remove_whitelist() {

		$this->_ajax_remove_whitelist();

	}

	public function ajax_email_confirmation() {

		$this->_ajax_email_confirmation();

	}

	public function ajax_toggle_subscription() {
		$this->_ajax_toggle_subscription();
	}

}