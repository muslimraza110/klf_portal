<?php
namespace App\Controller\Admin;

class EblastsController extends \App\Controller\EblastsController {

	public function index() {
		$this->_index();
	}

	public function form() {
		$this->_form();
	}

	public function eblast() {
		$this->_eblast();
	}

	public function view($id) {
		$this->_view($id);
	}

}