<?php
namespace App\Controller\Admin;

class TmpUploadFormsController extends \App\Controller\TmpUploadFormsController {

	public function batch_upload_forms() {

		$this->_batch_upload_forms();

	}

	public function batch_upload_posts() {

		$this->_batch_upload_posts();

	}

}
