<?php
namespace App\Controller;

use Cake\Network\Exception\NotFoundException;
use Cake\Network\Exception\InternalErrorException;

class FormFieldsController extends AppController {

	protected function _delete($id) {
		$this->request->allowMethod('DELETE');
		$this->autoRender = false;

		$formField = $this->FormFields->get($id);

		if (! $formField)
			throw new NotFoundException();

		if($this->FormFields->delete($formField)) {
			$this->response->type('json');
			$this->response->body(true);
		}
		else {
			throw new InternalErrorException();
		}
	}

}