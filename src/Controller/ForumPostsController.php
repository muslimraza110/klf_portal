<?php
namespace App\Controller;
use Cake\Collection\Collection;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\I18n\Time;
use Cake\Routing\Router;
use Cake\Utility\Inflector;
use Cake\View\View;

class ForumPostsController extends \App\Controller\AppController {

	protected function _index($scope = 'public') {


		$query = $this->ForumPosts->find()
			->where([
				'ForumPosts.parent_id IS' => null,
				'ForumPosts.subforum_id IS' => null,
			]);

		$this->paginate = [
			'limit' => 25,
			'contain' => [
				'Users.ProfileImg',

			]
		];

		$where = [];

		if ($scope != 'public') {
			$privateIds = [];

			switch ($scope) {
				case 'business':
					$where = [
						'Groups.category_id' => 2
					];
					$model = 'Groups';
					$privateIds = $this->ForumPosts->getGroupsPrivateIds($this->_authUser);
					break;
				case 'charity':
					$where = [
						'Groups.category_id' => 3
					];
					$model = 'Groups';
					$privateIds = $this->ForumPosts->getGroupsPrivateIds($this->_authUser);
					break;
				case 'project':
					$privateIds = $this->ForumPosts->getProjectsPrivateIds($this->_authUser);
					$model = 'Projects';
					$where = [];
					break;
				case 'admin':
					$query->where(['ForumPosts.visibility IS NOT' => null]);
					$model = 'admin';
					break;
				default:
					throw new NotFoundException();
					break;
			}

			if (in_array($model, ['Projects', 'Groups'])) {
				$this->paginate = [
					'limit' => 25,
					'contain' => [
						'Users.ProfileImg',
						$model => [
							'conditions' => [
								$where
							]
						]
					]
				];

				$query->where([
					'ForumPosts.id IN' => !empty($privateIds) ? $privateIds : [0],
					'ForumPosts.subforum_id IS NOT' => null,
				]);
			}

		} else {

			$publicIds = $this->ForumPosts->getPublicIds();


			$query->where([
				'ForumPosts.id IN' => !empty($publicIds) ? $publicIds : [0],
				'ForumPosts.visibility IS' => null,
			]);

		}

		$groupedForumPosts = [];

		$subtopics = $this->ForumPosts->Subforums->findByModel($scope)->toArray();
		$title = ucwords($scope.' forum');

		foreach ($this->paginate($query) as $post) {
			if ($scope == 'public' ) {
				$groupedForumPosts[ucwords($scope)][$post->id] = $post;
			} elseif(!empty($post->groups)) {
				foreach ($post->groups as $group)
					$groupedForumPosts[$group->name][$post->id] = $post;
			} elseif(!empty($post->projects)) {
				foreach ($post->projects as $project)
					$groupedForumPosts[$project->name][$post->id] = $post;
			} elseif($scope == 'admin' ) {
				$visibility = $this->ForumPosts->adminVisibility[$post->visibility];
				$groupedForumPosts[$visibility][$post->id] = $post;
			}
		}

		$this->set(compact('title'), 'Forum');
		$this->set(compact('groupedForumPosts', 'subtopics', 'scope'));
		$this->render(DS . 'ForumPosts' . DS . '_index');
	}

	protected function _view($id) {

		if (! $this->ForumPosts->exists($id))
			throw new NotFoundException();

		$model = $this->ForumPosts->getModelName($id);

		$contain = [
			'contain' => [
				'Moderators'
			]
		];
		$scope = $model['scope'];

		if (!empty($model) && $model['scope'] != 'admin') {
			$contain = [
				'contain' => [
					'Moderators',
					$model['model']
				]
			];
			$scope = $model['scope'];
		}

		$parentForumPost = $this->ForumPosts->get($id, array_merge($contain, ['contain' => ['Subforums']]));

		if (!$parentForumPost->isWhitelisted($this->_authUser)) {
			$this->Flash->error('You are not permitted to view this post.');
			return $this->redirect(['action' => 'index']);
		}

		$moderatorIds = [];

		if (!empty($parentForumPost->moderators))
			$moderatorIds = (new Collection($parentForumPost->moderators))
				->extract('id')
				->toArray();

		$title = $parentForumPost->topic; //make sure title shows on paginate pages
		
		$query = $this->ForumPosts->find('threaded')
			->contain(['Users.ProfileImg'])
			->where(['ForumPosts.id' => $id])
			->orWhere(['ForumPosts.parent_id ' => $id])
			->order(['ForumPosts.created' => 'ASC']);


		if (!in_array($this->_authUser->role, ['A', 'O']) && !in_array($this->_authUser->id, $moderatorIds)) {
			$query->where(['ForumPosts.pending' => false]);
		}

		$this->paginate = [
			'limit' => 25
		];

		$paginated = false;

		if (isset($this->request->query['page']) && $this->request->query['page'] >= 2) {
			$forumPost = $parentForumPost;
			$paginated = true;
			$forumPosts = $this->paginate($query);
		} else {
			$forumPost = $this->paginate($query)->first();
		}

		if ($this->request->is('post', 'put')) {
			$forumComment = $this->ForumPosts->newEntity();
			$forumComment = $this->ForumPosts->patchEntity($forumComment, $this->request->data);

			if ($parentForumPost->moderated)
				$forumComment->pending = true;

			if ($this->ForumPosts->save($forumComment)) {
				$this->Flash->success(!empty($forumComment->pending) ? 'The comment will be published after a moderator has reviewed it.' : 'The comment has been published');
				$this->redirect(['action' => 'view', $forumComment->parent_id, '#' => 'forum-post-' . $forumComment->id]);
			}
			else {
				$this->Flash->error('The comment cannot be published, please try again');
				$this->redirect(['action' => 'view', $forumComment->parent_id]);
			}
		}

		$subscriptionFrequencies = $this->ForumPosts->ForumPostSubscriptions->frequencies;

		$bannerActions = [
			$this->_icons['back'] => ['action' => 'view_subtopic', $parentForumPost->subforum_id]
		];

		$breadcrumbs = [
			['name' => ucwords($parentForumPost->subforum->model).' Forum', 'route' => ['action' => 'index', $parentForumPost->subforum->model]],
			['name' => $parentForumPost->subforum->title, 'route' => ['action' => 'view_subtopic', $parentForumPost->subforum_id]],
			['name' => $title, 'active' => true]
		];

		$this->set(compact('title', 'bannerActions', 'breadcrumbs', 'forumPost', 'moderatorIds', 'subscriptionFrequencies', 'parentForumPost', 'paginated', 'forumPosts'));

		$this->render(DS . 'ForumPosts' . DS . '_view');
	}

	protected function _add($scope, $id = null) {

		$authorized = false;

		switch ($scope) {
			case 'charity':
			case 'business':
				if(!empty($id)) {
					$authorized = $this->ForumPosts->checkAuthorization('Groups', $id, $this->_authUser);
				} else {
					$authorized = true;
				}
				break;
			case 'project':
				if(!empty($id)) {
					$authorized = $this->ForumPosts->checkAuthorization('Projects', $id, $this->_authUser);
				} else {
					$authorized = true;
				}
				break;
			case 'public':
					$authorized = true;
				break;
			case 'admin':
					if ($this->_authUser->role == 'A') {
						$authorized = true;
					}
					break;
			default:
				throw new NotFoundException();
				break;
		}

		if (!$authorized) {
			throw new ForbiddenException();
		}

		$forumPost = $this->ForumPosts->newEntity([
			'user_id' => $this->_authUser->id
		]);

		$title = 'New '.ucwords($scope).' Post';

		$titleIcon = $this->_icons['commenting'];

		$breadcrumbs = [
			['name' => ucwords($scope).' Forum', 'route' => ['action' => 'index/'.$scope]],
			['name' => $title, 'active' => true]
		];

		$this->set(compact('title', 'titleIcon', 'breadcrumbs'));

		$this->_form($forumPost, $scope, $id);
	}

	protected function _edit($id) {
		
		if (! $this->ForumPosts->exists($id))
			throw new NotFoundException();

		$model = $this->ForumPosts->getModelName($id);

		$contain = [
			'contain' => [
				'Moderators'
			]
		];

		$scope = $model['scope'];

		if (!empty($model) && $model['scope'] != 'admin' ) {
			$contain = [
				'contain' => [
					'Moderators',
					$model['model']
				]
			];
			$scope = $model['scope'];
		}

		$forumPost = $this->ForumPosts->get($id, $contain);

		if (!empty($forumPost->parent_id))
			$parentForumPost = $this->ForumPosts->get($forumPost->parent_id, [
				'contain' => [
					'Moderators'
				]
			]);
		else
			$parentForumPost = $forumPost;

		$moderatorIds = (new Collection($parentForumPost->moderators))->extract('id')->toArray();

		if ($this->_authUser->id != $forumPost->user_id
			&& !in_array($this->_authUser->role, ['A', 'O'])
			&& !in_array($this->_authUser->id, $moderatorIds)
		) {
			throw new NotFoundException;
		}

		$title = 'Edit '.ucwords($scope).' Post';
		$titleIcon = $this->_icons['commenting'];
		$this->set(compact('title', 'titleIcon'));
		$this->_form($forumPost, $scope);
	}

	protected function _form($forumPost, $scope = null, $id = null) {

		if (!$forumPost->isWhitelisted($this->_authUser)) {
			$this->Flash->error('You are not permitted to edit this post.');
			return $this->redirect(['action' => 'index']);
		}

		$subforum = [];
		if (!empty($forumPost->subforum_id)) {
			$subforum = $this->ForumPosts->Subforums->get($forumPost->subforum_id);
		}

		if (!empty($this->request->query['eblast'])) {
			$this->request->data['eblast'] = $this->request->query['eblast'];
		}

		if ($this->request->is(['post', 'put'])) {

			if ($isNew = $forumPost->isNew())
				$this->request->data['moderators']['_ids'][] = $this->_authUser->id;

				$forumPost = $this->ForumPosts->patchEntity($forumPost, $this->request->data, ['associated' => ['Groups', 'Projects', 'Moderators']]);

			if ($save = $this->ForumPosts->save($forumPost)) {


				if (!empty($this->request->data['eblast'])) {

					if ($isNew && $forumPost->visibility == 'W') {
						$this->Flash->success('The post has been published. You many now add restricted users.');
						return $this->redirect(['action' => 'edit', $save->id, '?' => ['eblast' => true]]);
					} else {
						$this->Flash->success('The post has been published');
						$extraRecipients = [];

						if (!$isNew && $forumPost->visibility == 'W') {
							$extraRecipients['Restricted'] = $forumPost->whitelistedUserList();
						}

						$content = (new View)
							->element('Eblasts/post_notification', [
								'authUser' => $this->_authUser,
								'forumPost' => $forumPost
							]);

						$session = [
							'subject' => 'Post Notification',
							'content' => $content,
							'button_label' => 'Link to the Post',
							'button_link' => Router::url(['action' => 'view', $forumPost->id, 'prefix' => '_', '_full' => true]),
							'return_url' => Router::url(['action' => 'view', !empty($forumPost->parent_id) ? $forumPost->parent_id : $forumPost->id, '_full' => true]),
							'extra_recipients' => $extraRecipients
						];

						$this->Session->write('Eblast', $session);

						return $this->redirect(['controller' => 'Eblasts', 'action' => 'form']);

					}

				}

				if (!empty($save->subforum_id)) {
					$subforum = $this->ForumPosts->Subforums->get($forumPost->subforum_id);
				}

				if ($isNew) {
					return $this->redirect(['action' => 'view', !empty($forumPost->parent_id) ? $forumPost->parent_id : $forumPost->id]);
				}

			} else {
				$this->Flash->error('The post cannot be published, please try again');
			}

			if (!empty($subforumId = $forumPost->subforum_id)) {
				$subforum = $this->ForumPosts->Subforums->get($subforumId);
				$this->redirect(['action' => 'view', !empty($forumPost->parent_id) ? $forumPost->parent_id : $forumPost->id]);
			} else {
				$this->redirect(['action' => 'view', !empty($forumPost->parent_id) ? $forumPost->parent_id : $forumPost->id]);
			}

		}

		$groups = $this->ForumPosts->publicVisibility;

		if (in_array($scope, ['charity', 'business'])) {

			$groups = $this->ForumPosts->getUserGroups($this->_authUser);

		} elseif ($scope == 'project') {

			$groups = $this->ForumPosts->getUserProjects($this->_authUser);

		} elseif ($scope == 'public') {

			$groups =  [];

		} elseif ($scope == 'admin') {

			$groups = $this->ForumPosts->adminVisibility;

		}


		$postVisibility = $this->ForumPosts->postVisibility;

		$roles = $this->ForumPosts->Users->roles;
		$this->set(compact('forumPost', 'groups', 'scope', 'id', 'roles', 'subforum', 'postVisibility'));
		$this->render(DS . 'ForumPosts' . DS . '/_form');
	}

	protected function _add_subtopic($model) {


		$subforum = $this->ForumPosts->Subforums->newEntity([
			'user_id' => $this->_authUser->id,
			'model' => $model
		]);

		if ($this->request->is(['post', 'put'])) {

				$subforum = $this->ForumPosts->Subforums->patchEntity($subforum, $this->request->data);

			if ($this->ForumPosts->Subforums->save($subforum)) {
				$this->Flash->success('The Subforum has been created');
			} else {
				$this->Flash->error('The Subforum cannot be published, please try again');
			}

			$this->redirect(['controller' => 'ForumPosts', 'action' => 'index/'.$model]);
		}

		$breadcrumbs = [
			['name' => ucwords($subforum->model).' Forum', 'route' => ['action' => 'index/'.$subforum->model]],
			['name' => $subforum->model, 'active' => true]
		];

		$this->set(compact('breadcrumbs'));

		$this->set(compact('subforum', 'model'));
		$this->render(DS . 'ForumPosts' . DS . '/_add_subtopic');
	}

	protected function _view_subtopic($id) {

		$subforum = $this->ForumPosts->Subforums->get($id);

		$where['ForumPosts.subforum_id'] = $subforum->id;

		$query = $this->ForumPosts->find()
			->where($where);

		$this->paginate = [
			'limit' => 25,
			'contain' => [
				'Users.ProfileImg',
			]
		];

		$groupedForumPosts = [];
		$visibility = [];

		foreach ($this->paginate($query) as $post) {
			if ($post->isWhitelisted($this->_authUser)) {
				$groupedForumPosts[$subforum->model][$post->id] = $post;
			}
		}

		$title = $subforum->title;

		$breadcrumbs = [
			['name' => ucwords($subforum->model) . ' Forum', 'route' => ['action' => 'index/' . $subforum->model]],
			['name' => $title, 'active' => true]
		];

		$this->set(compact('groupedForumPosts', 'subforum', 'title', 'hideTitle', 'breadcrumbs'));
		$this->render(DS . 'ForumPosts' . DS . '/_view_subtopic');

	}

	protected function _add_subtopic_post($id) {

		$forumPost = $this->ForumPosts->newEntity([
			'user_id' => $this->_authUser->id,
			'subforum_id' => $id
		]);

		$subforum = $this->ForumPosts->Subforums->get($id);
		$scope = $subforum->model;

		$breadcrumbs = [
			['name' => ucwords($subforum->model).' Forum', 'route' => ['action' => 'index', $subforum->model]],
			['name' => ucwords($subforum->title), 'route' => ['action' => 'view_subtopic', $subforum->id]]
		];

		$this->set(compact('breadcrumbs'));

		$this->_form($forumPost, $scope);

	}

	protected function _delete_subtopic($id, $scope) {

		$this->request->allowMethod('post');

		$subForum = $this->ForumPosts->Subforums->get($id);

		if (!$subForum) {
			throw new NotFoundException();
		}

		$subForum->status = 'D';

		if ($this->ForumPosts->Subforums->save($subForum)) {

			$this->Flash->success('The subtopic has been deleted.');

		} else {
			$this->Flash->error('The subtopic couldn\'t be deleted.');
		}

		return $this->redirect(['action' => 'index', $scope]);

	}

	protected function _delete($id) {
		
		$this->request->allowMethod('post');

    $forumPost = $this->ForumPosts->get($id);
    
    if (!$forumPost)
      throw new NotFoundException();

		if (!empty($forumPost->parent_id))
			$parentForumPost = $this->ForumPosts->get($forumPost->parent_id, [
				'contain' => [
					'Moderators'
				]
			]);
		else
			$parentForumPost = $this->ForumPosts->get($forumPost->id, [
				'contain' => [
					'Moderators'
				]
			]);;

		// debug($parentForumPost);exit;

		$moderatorIds = (new Collection($parentForumPost->moderators))->extract('id')->toArray();

		if ($this->_authUser->id != $forumPost->user_id
			&& !in_array($this->_authUser->role, ['A', 'O'])
			&& !in_array($this->_authUser->id, $moderatorIds)
		)
			throw new NotFoundException;

    $forumPost->status = 'D';

    if ($this->ForumPosts->save($forumPost)) {
    	$errors = [];

    	if ($forumPost->parent_id == null) {

    		$comments = $this->ForumPosts->find()
    		->where([
    			'ForumPosts.parent_id' => $forumPost->id
    		]);

    		foreach ($comments as $comment) {
    			$comment->status = 'D';
    			$this->ForumPosts->save($comment);
    		}
    	}

      $this->Flash->success('The forum post has been deleted.');
    }
    else
      $this->Flash->error('The forum post couldn\'t be deleted.');
    
    return $this->redirect(['action' => 'index']);
  }

	protected function _approve($id) {

		$forumPost = $this->ForumPosts->get($id, [
			'contain' => [
				'ParentForumPosts.Moderators'
			]
		]);

		$moderatorIds = (new Collection($forumPost->parent_forum_post->moderators))->extract('id')->toArray();

		if (!in_array($this->_authUser->role, ['A', 'O']) && !in_array($this->_authUser->id, $moderatorIds))
			throw new ForbiddenException;

		$forumPost->pending = false;

		$this->ForumPosts->save($forumPost);

		return $this->redirect(['action' => 'view', $forumPost->parent_id, '#' => 'forum-post-'.$forumPost->id]);

	}

  protected function _ajax_add_comment() {
		
		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			$response = [];

			if (! empty($this->request->data['parent_id']) && ! empty($this->request->data['content'])) {

				$forumPost = $this->ForumPosts->newEntity([
					'parent_id' => $this->request->data['parent_id'],
					'content' => $this->request->data['content'],
					'user_id' => $this->_authUser->id,
					'created' => Time::now(),
					'modified' => Time::now()
				]);

				$parentForumPost = $this->ForumPosts->get($forumPost->parent_id);

				if (!$parentForumPost->isWhitelisted($this->_authUser)) {
					exit;
				}

				if ($parentForumPost->moderated)
					$forumPost->pending = true;
				
				$profileImage = 'profile.png';
				$response['user'] = false;

				if ($this->ForumPosts->save($forumPost)) {

					$contact = $this->ForumPosts->Users->Contacts->findByUserId($this->_authUser->id)
						->contain(['Profile'])
						->first();

					if (! empty($contact->profile)) {
						$profileImage = $contact->profile->url(['width' => 200]);
					}

					if (empty($contact)) {
						$contact = $this->ForumPosts->Users->get($this->_authUser->id);
					}

					$response['comment'] = $forumPost;
					$response['contact'] = $contact;
					$response['user'] = true;
 					$response['profileImage'] = $profileImage;
				}
				else {
					$response['error'] = 'The comment cannot be saved.';
				}
			}
			else {
				$response['error'] = 'The content cannot be empty.';
			}

			$this->response->type('json');
	  	$this->response->body(json_encode($response));
		}
	}

	protected function _eblast($id) {

		$forumPost = $this->ForumPosts->get($id, [
			'contain' => [
				'ParentForumPosts.ForumComments.Users',
				'ForumComments.Users'
			]
		]);

		$content = (new View)
			->element('Eblasts/post_notification', [
				'authUser' => $this->_authUser,
				'forumPost' => $forumPost
			]);

		$linkId = !empty($forumPost->parent_id) ? $forumPost->parent_id : $forumPost->id;

		$extraRecipients = [];

		if ($forumPost->visibility == 'W' || (!empty($forumPost->parent_forum_post) && $forumPost->parent_forum_post->visibility == 'W')) {
			$extraRecipients['Restricted'] = !empty($forumPost->parent_forum_post) ? $forumPost->parent_forum_post->whitelistedUserList() : $forumPost->whitelistedUserList();
		}

		$session = [
			'subject' => 'Post Notification',
			'content' => $content,
			'button_label' => 'Link to the Post',
			'button_link' => Router::url(['action' => 'view', $linkId, '#' => 'forum-post-'.$forumPost->id, 'prefix' => '_', '_full' => true]),
			'return_url' => Router::url(['action' => 'view', $linkId, '#' => 'forum-post-'.$forumPost->id, '_full' => true]),
			'extra_recipients' => $extraRecipients
		];

		$comments = !empty($forumPost->parent_forum_post) ? $forumPost->parent_forum_post->forum_comments : $forumPost->forum_comments;

		foreach ($comments as $comment) {
			$session['extra_recipients']['Replies'][$comment->user_id] = $comment->user->name;
		}

		$this->Session->write('Eblast', $session);

		return $this->redirect(['controller' => 'Eblasts', 'action' => 'form']);

	}
	public function ng_forum_posts() {
		$this->autoRender = false;
		$this->response->disableCache();

		$forumPosts = $this->ForumPosts->find('threaded')
			->contain(['Users']);

		$this->response->type('json');
		$this->response->body(json_encode($forumPosts));

		return $this->response;
	}

	protected function _ajax_html_moderator_list() {

		$id = $this->request->data('id');

		if (empty($id)) {

			$this->autoRender = false;

			return '';

		}

		$forumPost = $this->ForumPosts->get($id, [
			'contain' => [
				'Moderators.ProfileImg'
			]
		]);

		$this->set(compact('forumPost'));

		$this->render('/ForumPosts/_ajax_html_moderator_list');

	}

	protected function _ajax_html_whitelist_list() {

		$id = $this->request->data('id');

		if (empty($id)) {

			$this->autoRender = false;

			return '';

		}

		$forumPost = $this->ForumPosts->get($id, [
			'contain' => [
				'ForumPostWhitelists'
			]
		]);

		foreach ($forumPost->forum_post_whitelists as &$whitelist) {

			if ($whitelist->model == 'GroupsTable') {
				$relation = 'Groups';
			} else {
				$relation = 'Users';
			}

			$whitelist->association = $this->ForumPosts->$relation->get($whitelist->model_id, [
				'contain' => [
					'ProfileImg'
				]
			]);

		}

		$this->set(compact('forumPost'));

		$this->render('/ForumPosts/_ajax_html_whitelist_list');

	}

	protected function _ajax_add_moderator() {

		$id = $this->request->data('id');
		$userId = $this->request->data('user_id');

		$response = ['status' => 'error'];

		if (!empty($id) && !empty($userId)) {

			$this->loadModel('ForumPostsModerators');

			$forumPostsModerators = $this->ForumPostsModerators->newEntity([
				'forum_post_id' => $id,
				'moderator_id' => $userId
			]);

			if ($this->ForumPostsModerators->save($forumPostsModerators))
				$response = ['status' => 'ok'];

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_add_whitelist() {

		$id = $this->request->data('id');
		$modelId = $this->request->data('model_id');
		$model = $this->request->data('model');

		$response = ['status' => 'error'];

		if (!empty($id) && !empty($modelId) && !empty($model)) {

			$forumPostWhitelists = $this->ForumPosts->ForumPostWhitelists->newEntity([
				'forum_post_id' => $id,
				'model_id' => $modelId,
				'model' => $model.'Table'
			]);

			if ($this->ForumPosts->ForumPostWhitelists->save($forumPostWhitelists)) {
				$response = ['status' => 'ok'];
			}

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_remove_moderator() {

		$id = $this->request->data('id');
		$userId = $this->request->data('user_id');

		$response = ['status' => 'error'];

		if (!empty($id) && !empty($userId)) {

			$this->loadModel('ForumPostsModerators');

			$forumPostsModerators = $this->ForumPostsModerators
				->findByForumPostIdAndModeratorId($id, $userId)
				->first();

			if (!empty($forumPostsModerators) && $this->ForumPostsModerators->delete($forumPostsModerators))
				$response = ['status' => 'ok'];

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_remove_whitelist() {

		$id = $this->request->data('id');

		$response = ['status' => 'error'];

		if (!empty($id)) {

			$forumPostWhitelists = $this->ForumPosts->ForumPostWhitelists->get($id);

			if ($this->ForumPosts->ForumPostWhitelists->delete($forumPostWhitelists)) {
				$response = ['status' => 'ok'];
			}

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_email_confirmation() {

		$this->loadModel('EmailLists');

		$contactList = !empty($this->request->data['contactList']) ? $this->request->data['contactList'] : [0];
		$emailReceivers = !empty($this->request->data['emailReceivers']) ? $this->request->data['emailReceivers'] : [0];
		$model = $this->request->data['model'];

		$emailReceivers = $this->EmailLists->getEmailReceivers($emailReceivers, $contactList);

		$receiversNames = [];
		foreach ($emailReceivers as $emailReceiver) {
			$receiversNames[] = $emailReceiver->name;
		}


		$this->set(compact('emailReceivers', 'receiversNames', 'model'));

		$this->render('/ForumPosts/_ajax_email_confirmation');

	}

	protected function _ajax_toggle_subscription() {

		$id = $this->request->data('id');
		$frequency = $this->request->data('frequency');
		$userId = $this->_authUser->id;

		$response = ['status' => 'error'];

		if (!empty($id)) {

			$forumPostSubscription = $this->ForumPosts->ForumPostSubscriptions->findByForumPostIdAndUserId($id, $userId)
				->first();

			if (empty($forumPostSubscription)) {

				$forumPostSubscription = $this->ForumPosts->ForumPostSubscriptions->newEntity([
					'forum_post_id' => $id,
					'user_id' => $userId,
					'frequency' => $frequency
				]);

				$this->ForumPosts->ForumPostSubscriptions->save($forumPostSubscription);

				$response = ['status' => 'subscribed', 'frequency' => $this->ForumPosts->ForumPostSubscriptions->frequencies[$frequency]];

			} else if (!empty($frequency) && $forumPostSubscription->frequency != $frequency) {

				$forumPostSubscription->frequency = $frequency;

				$this->ForumPosts->ForumPostSubscriptions->save($forumPostSubscription);

				$response = ['status' => 'subscribed', 'frequency' => $this->ForumPosts->ForumPostSubscriptions->frequencies[$frequency]];

			} else {

				$this->ForumPosts->ForumPostSubscriptions->delete($forumPostSubscription);

				$response = ['status' => 'unsubscribed'];

			}

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

}