<?php
namespace App\Controller;

class NotificationSettingsController extends \App\Controller\AppController {

	protected function _ajax_add() {
		$this->autoRender = false;
		
		$response = '';

		if (! empty($this->request->data['user_id']) && ! empty($this->request->data['activate'])) {
			
			if ($this->request->data['activate'] == "false") {
				$this->request->data['activate'] = false;
			}

			$response = $this->_authUser->saveNotifications($this->request->data['user_id'], $this->request->data['activate'], false, 'ForumPosts');
		}

		$this->response->type('json');
	  $this->response->body(json_encode($response));
	}

}