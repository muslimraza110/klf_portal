<?php
namespace App\Controller;

class ErrorController extends \App\Controller\AppController {

	public function beforeRender(\Cake\Event\Event $event){
		parent::beforeRender($event);
		
		if (!isset($this->_authUser) || !$this->_authUser)
			$this->viewBuilder()->layout('empty_error');
		
		$this->viewBuilder()->templatePath('Error');
	}

}