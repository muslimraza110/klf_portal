<?php

namespace App\Controller;

use Cake\Collection\Collection;
use Cake\I18n\Time;
use Cake\Utility\Inflector;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Behavior\TreeBehavior;

class ProjectsController extends AppController {

	protected function _index() {

		$this->paginate = [
			'contain' => [
				'Groups',
				'Users',
				'Creators',
				'Tags'
			],
			'limit' => 30,
			'sortWhitelist' => [
				'name', 'Creators.first_name', 'Groups.name', 'start_date', 'city', 'created'
			]
		];

		$projects = $this->paginate();

		$titleIcon = $this->_icons['tasks'];

		$this->set(compact('titleIcon', 'projects'));

		$this->render('/Projects/_index');

	}

	protected function _add($groupId = null) {

		$project = $this->Projects->newEntity();

		$breadcrumbs = [
			['name' => 'Projects', 'route' => ['action' => 'index']],
			['name' => 'Add Project', 'active' => true]
		];

		if ($this->request->is(['get']) && !empty($groupId)) {

				$this->request->data['group_id'] = $groupId;

		}

		$this->set(compact('breadcrumbs'));

		$this->_form($project);

	}

	protected function _edit($id) {

		$project = $this->Projects->get($id, [
			'contain' => [
				'Groups',
				'Tags'
			]
		]);

		$project->tags = (new Collection($project->tags))
			->indexBy(function ($q) {
				return $q->root_tag->id;
			})
			->toArray();

		if (
			$this->_authUser->role != 'A'
			&& $project->creator_id != $this->_authUser->id
			&& !$project->isAdminAnyGroups($this->_authUser)
		) {
			throw new NotFoundException;
		}

		$breadcrumbs = [
			['name' => 'Projects', 'route' => ['action' => 'index']],
			['name' => 'Edit Project', 'active' => true]
		];

		$this->set(compact('breadcrumbs'));

		$this->_form($project);

	}

	protected function _delete($id) {

		$this->request->allowMethod('post');

		$project = $this->Projects->get($id, ['contain' => ['Groups']]);

		if (!in_array($this->_authUser->role, ['A']) && !$project->isAdminAnyGroups($this->_authUser)) {
			throw new NotFoundException;
		}

		$project->status = 'D';

		if ($this->Projects->save($project))
			$this->Flash->success('Project has been deleted.');
		else
			$this->Flash->error('Project could not be deleted.');

		return $this->redirect(['action' => 'index']);

	}

	protected function _view($id) {

		if ($this->request->is(['post', 'put'])) {

			if (!empty($note = $this->request->data('note'))) {

				$projectNote = $this->Projects->ProjectNotes->newEntity([
					'project_id' => $id,
					'user_id' => $this->_authUser->id,
					'note' => $note
				]);

				if ($this->Projects->ProjectNotes->save($projectNote)) {

					$this->Flash->success('Note has been saved successfully.');

					unset($this->request->data['note']);

				} else
					$this->Flash->error('An error occurred. Note could not be saved.');

			}

		}

		$project = $this->Projects->get($id, [
			'contain' => [
				'Groups.ProfileImg',
				'ProjectNotes.Users.ProfileImg',
				'Events' => [
					'Users',
					// 'conditions' => [
					// 	'Events.start_date >=' => Time::now()
					// ]
				],
				'Users',
				'Tags',
				'ForumPosts' => function ($q) {
					return $q->contain('Users')
						->where(['ForumPosts.parent_id IS' => null]);
				},
				'ProjectRequests' => [
					'Users.ProfileImg',
					'conditions' => [
						'ProjectRequests.status' => 'P'
					]
				]
			]
		]);

		$memberIds = (new Collection($project->users))->extract('id')->toArray();

		$projectRequestCount = $this->Projects->ProjectRequests->findByProjectIdAndUserId($id, $this->_authUser->id)
			->count();

		$requestProjectAccess = (!in_array($this->_authUser->id, $memberIds) && $projectRequestCount < 1);

		$title = 'Project';
		$breadcrumbs = [
			['name' => 'Projects', 'route' => ['action' => 'index']],
			['name' => $title, 'active' => true]
		];

		$this->set(compact('title', 'breadcrumbs', 'project', 'requestProjectAccess', 'memberIds'));

		$this->render('/Projects/_view');

	}

	protected function _khoja_cares() {

		$this->loadModel('KhojaCareCategories');

		$khojaCareCategories = $this->KhojaCareCategories->find()
			->contain([
				'Projects'=> function ($q) {
					return $q->contain([
						'Groups',
						'Creators'
					])->where([
						'Projects.approval_stamp' => 1
					]);
				}
			]);

		$titleIcon = $this->_icons['get-pocket'];

		$this->set(compact('titleIcon', 'khojaCareCategories', 'category'));


		$this->render('/Projects/_khoja_cares');
		
	}

	protected function _form($project) {

		if ($this->request->is(['post', 'put'])) {

			if (!empty($startDate = $this->request->data('start_date')))
				$this->request->data['start_date'] = Time::createFromFormat('m/d/y', $startDate);

			if (!empty($estCompletionDate = $this->request->data('est_completion_date')))
				$this->request->data['est_completion_date'] = Time::createFromFormat('m/d/y', $estCompletionDate);

			if (!empty($this->request->data['read_only'])) {
				$this->request->data['status'] = 'R';
			} else {
				$this->request->data['status'] = 'A';
			}

			if ($isNew = $project->isNew()) {
				$this->request->data['users'] = [
					'_ids' => [$this->_authUser->id]
				];
				$this->request->data['creator_id'] =  $this->_authUser->id;
			}

			if (isset($this->request->data['tags'])) {

				foreach ($this->request->data['tags'] as $rootTagId => $tag) {

					if (empty($tag['id'])) {
						unset($this->request->data['tags'][$rootTagId]);
					}

				}

			}

			if (count($project->groups) == 1) {
				$this->request->data['groups'][0]['id'] = $project->groups[0]->id;
				$this->request->data['groups'][0]['_joinData']['percent'] = 100;
			}

			$project = $this->Projects->patchEntity($project, $this->request->data, ['associated' => ['Users', 'Tags', 'Groups']]);

			if ($this->Projects->save($project)) {

				$this->Flash->success('Project saved successfully.');

				if ($isNew) {
					$this->redirect(['action' => 'edit', $project->id]);
				} else {
					$this->redirect(['action' => 'view', $project->id]);
				}


			} else
				$this->Flash->error('An error occurred. Project could not be saved.');

		}

		$groups = $this->Projects->Users->Groups->find('list');

		if ($this->_authUser->role != 'A') {

			$groups = $groups->matching('Users', function ($query) {

				return $query->where([
					'Users.id' => $this->_authUser->id
				]);

			});

			$groups = $groups->toArray();

			$projects = $this->Projects->find()
				->contain([
					'Groups'
				])
				->matching('Users', function ($query) {

					return $query->where([
						'Users.id' => $this->_authUser->id
					]);

				})
				->toArray();

			if (!empty($project->groups)) {

				$projects[] = $project;

				foreach ($projects as $project) {
					foreach ($project->groups as $group) {
						if (!array_key_exists($group->id, $groups)) {
							$groups[$group->id] = $group->name;
						}
					}
				}

				asort($groups);

			}

		}

		$khojaCareCategories = $this->Projects->KhojaCareCategories->find('list', [
			'order' => [
				'KhojaCareCategories.position' => 'ASC'
			],
			'conditions' => [
				'OR' => [
					'KhojaCareCategories.status' => 'A',
					'KhojaCareCategories.id' => $project->khoja_care_category_id
				]
			]
		]);

		$titleIcon = $this->_icons['tasks'];
		$tagsList = $this->Projects->Tags->multiLevelTagsList();

		$this->set(compact('titleIcon', 'project', 'groups', 'tagsList', 'khojaCareCategories'));
		$this->render('/Projects/_form');

	}

	protected function _percentages($id) {

		$project = $this->Projects->find()
			->contain([
				'Groups'
			])
			->where([
				'Projects.id' => $id
			])
			->first();

		if ($this->request->is(['post', 'put'])) {

			$project = $this->Projects->patchEntity($project, $this->request->data, ['associated' => ['Groups']]);

			if ($this->Projects->save($project)) {
				$this->Flash->success('Charities updated successfully.');
			} else {
				$this->Flash->success('There was an error in saving, please try again.');
			}

		}

		$this->set(compact('project'));
		$this->render('/Projects/percentages');

	}

	protected function _ajax_html_group_list() {

		$term = $this->request->data('term');
		$id = $this->request->data('id');
		$remove = true;


		$groups = $this->Projects->Users->Groups->find();


		if ($this->_authUser->role != 'A') {
			$groups = $groups->matching('Users', function ($query) {
				return $query->where([
					'Users.id' => $this->_authUser->id
				]);
			});
		}

		if (!empty($term)) {

			$groupProjects = $this->Projects->projectGroups($id)->extract('id')->toArray();
			$groups = $groups
				->where([
						'name LIKE' => '%' . $term . '%',
						'Groups.id NOT IN' => !empty($groupProjects) ? $groupProjects : [0]
				]);
			$remove = false;

		} else {

			$groups = $this->Projects->projectGroups($id);

		}

		$this->set(compact('groups', 'remove'));

		$this->render('/Projects/_ajax_html_group_list');

	}

	protected function _ajax_html_user_list() {

		$id = $this->request->data('id');

		if (empty($id)) {

			$this->autoRender = false;

			return '';

		}

		$project = $this->Projects->get($id, [
			'contain' => [
				'Groups',
				'Users.ProfileImg'
			]
		]);

		$this->set(compact('project'));

		$this->render('/Projects/_ajax_html_user_list');

	}

	protected function _ajax_remove_note() {

		$response = ['status' => 'error'];

		$noteId = $this->request->data('id');
		$note = $this->Projects->ProjectNotes->get($noteId);

		$note = $this->Projects->ProjectNotes->patchEntity($note, ['status' => 'D']);

		if ($this->Projects->ProjectNotes->save($note)) {
			$response = ['status' => 'ok'];
		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_add_user() {

		$id = $this->request->data('id');
		$userId = $this->request->data('user_id');

		$response = ['status' => 'error'];

		if (!empty($id) && !empty($userId)) {

			$this->loadModel('ProjectsUsers');

			$projectsUsers = $this->ProjectsUsers->newEntity([
				'project_id' => $id,
				'user_id' => $userId
			]);

			if ($this->ProjectsUsers->save($projectsUsers))
				$response = ['status' => 'ok'];

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_add() {

		$model = $this->request->data('model');

		if (empty($model)) {

			$this->autoRender = false;

			return '';

		}

		$var = strtolower(Inflector::singularize($model));

		$id = $this->request->data('id');
		$modelId = $this->request->data($var . '_id');

		$response = ['status' => 'error'];

		if (!empty($id) && !empty($modelId)) {

			$Model = 'Projects' . $model;

			$this->loadModel($Model);

			$entity = $this->$Model->newEntity([
				'project_id' => $id,
				$var . '_id' => $modelId
			]);

			if ($this->$Model->save($entity)) {
				$response = ['status' => 'ok'];
			}


		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_remove() {

		$model = $this->request->data('model');

		if (empty($model)) {

			$this->autoRender = false;

			return '';

		}

		$var = strtolower(Inflector::singularize($model));

		$id = $this->request->data('id');
		$modelId = $this->request->data($var . '_id');

		$response = ['status' => 'error'];

		if (!empty($id) && !empty($modelId)) {

			$Model = 'Projects' . $model;

			$this->loadModel($Model);
			$query = 'findByProjectIdAnd' . Inflector::singularize($model) . 'Id';
			$entity = $this->$Model
				->$query($id, $modelId)
				->first();

			if (!empty($entity) && $this->$Model->delete($entity)) {
				$response = ['status' => 'ok'];
			}


		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_remove_user() {

		$id = $this->request->data('id');
		$userId = $this->request->data('user_id');

		$response = ['status' => 'error'];

		if (!empty($id) && !empty($userId)) {

			$this->loadModel('ProjectsUsers');

			$projectsUsers = $this->ProjectsUsers
				->findByProjectIdAndUserId($id, $userId)
				->first();

			if (!empty($projectsUsers) && $this->ProjectsUsers->delete($projectsUsers))
				$response = ['status' => 'ok'];

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_project_access() {

		$projectRequestId = $this->request->data('project_request_id');
		$type = $this->request->data('type');

		$response = ['status' => 'error'];

		if (!empty($projectRequestId) && !empty($type)) {

			$projectRequest = $this->Projects->ProjectRequests->get($projectRequestId, [
				'contain' => [
					'Projects.Groups'
				]
			]);

			if ($this->_authUser->role == 'A' || $projectRequest->project->isAdminAnyGroups($this->_authUser)) {

				$projectRequest->status = $type == 'A' ? $type : 'R';

				if ($this->Projects->ProjectRequests->save($projectRequest)) {

					if ($projectRequest->status == 'A') {

						$this->loadModel('ProjectsUsers');

						$projectsUsers = $this->ProjectsUsers->newEntity([
							'project_id' => $projectRequest->project_id,
							'user_id' => $projectRequest->user_id
						]);

						$this->ProjectsUsers->save($projectsUsers);

					}

					$response = ['status' => 'ok'];

				}

			}

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

}