<?php
namespace App\Controller;

class FaqsController extends \App\Controller\AppController {

	protected function _index($editList = null) {

		$this->Faqs->recover();

		$faqs = $this->Faqs->find()
			->where([
				'Faqs.parent_id is' => NULL,
				'Faqs.status' => 'A'
			])
			->order('Faqs.position');

		foreach ($faqs as $key => $faq) {

			$faq['children'] = $faq->sortFaqTree();

		}

		if (!empty($editList)) {

			$this->set(compact('faqs'));
			$this->render('/Faqs/_faq_list');

		}	else {

			$this->set(compact('faqs'));
			$this->render('/Faqs/index');

		}

	}

	protected function _add($parentId = null) {

		$data = [];

		if (!empty($parentId)) {
			$data['parent_id'] = $parentId;
		}

		$lastPosition = $this->Faqs->find()
			->select([
				'last_position' => 'MAX(Faqs.position)'
			])
			->first();
		$data['status'] = 'A';
		$faq = $this->Faqs->newEntity($data);

		$title = 'New FAQ';
		$titleIcon = $this->_icons['question-circle'];
		$this->set(compact('title', 'titleIcon'));

		$this->_form($faq);

	}

	protected function _edit($id) {

		$faq = $this->Faqs->get($id);

		$title = 'Edit FAQ';
		$titleIcon = $this->_icons['edit'];
		$this->set(compact('title', 'titleIcon'));

		$this->_form($faq);

	}

	// protected function _view($id) {
	//
	// 	$this->Faqs->recover();
	// 	$conditions = [
	// 		'Faqs.status' => 'A',
	// 		'Faqs.id' => $id
	// 	];
	//
	// 	$faqs = $this->Faqs->find()
	// 		->where($conditions)
	// 		->order('Faqs.position');
	//
	// 	foreach ($faqs as $key => $faq) {
	//
	// 		$faq['children'] = $faq->sortFaqTree();
	//
	// 	}
	//
	// 	$this->set(compact('faqs'));
	//
	//
	// 	$this->render('/Faqs/_view');
	//
	// }

	protected function _form($faq) {

		if ($this->request->is(['post', 'put'])) {

			$faq = $this->Faqs->patchEntity($faq, $this->request->data);

			if (! $this->Faqs->save($faq)) {
				$this->Flash->error('This Faq title already exist please try again.');
				return $this->redirect(['action' => $this->request->action, $faq->id]);
			}

			$childFaqs = [];

			if (!empty($this->request->data['faqs']['_ids'])) {

				$childFaqTitles = $this->request->data['faqs']['_ids'];

				$siblingLastOrderNum = $this->Faqs->find()
					->select([
						'last_position' => 'MAX(position)'
					])
					->where([
						'Faqs.parent_id' => $faq->id,
						'Faqs.status' => 'A'
					])
					->first();

				$siblingLastOrderNum = $siblingLastOrderNum->last_position;

				foreach ($childFaqTitles as $childFaqTitle) {

					$existingFaq = $this->Faqs->find()
						->where([
							'Faqs.parent_id' => $faq->id,
							'Faqs.status' => 'A',
							'Faqs.title' => $childFaqTitle
						])
						->first();

					if (empty($siblingLastOrderNum)) {
						$siblingLastOrderNum = 0;
					}

					if (!$existingFaq) {
						$childFaqs[] = [
							'title' => $childFaqTitle,
							'status' => 'A',
							'parent_id' => $faq->id,
							'position' => $siblingLastOrderNum++
						];
					}

				}

				$this->Faqs->deleteAll([
					'parent_id' => $faq->id,
					'title not in' => $childFaqTitles
				]);

				$childFaqs = $this->Faqs->newEntities($childFaqs);

			} else {

				$this->Faqs->deleteAll([
					'parent_id' => $faq->id
				]);

			}

			if ($this->Faqs->saveMany($childFaqs) || empty($childFaqs)) {
				$this->Flash->success('Faq Category saved successfully.');
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error('An error occurred. Faq Category could not be saved.');
			}

		}



		$titleIcon = $this->_icons['question-circle'];
		$faqs = $this->Faqs->find('list', [
				'keyField' => 'title'
			])
			->where([
				'Faqs.parent_id' => $faq->id,
				'Faqs.status' => 'A'
			]);
		$this->set(compact('titleIcon', 'faq', 'faqs'));

		if(!empty($faq->parent_id) || $faq->description != '') {
			$this->render(DS . 'Faqs' . DS . '_faq_form');
		} else {
				$this->render(DS . 'Faqs' . DS . '_form');
		}


	}


	protected function _delete($id) {


		$faq = $this->Faqs->get($id);

		$faq->status = 'D';

		if ($this->Faqs->save($faq)) {
			$this->Flash->success('The FAQ has been deleted.');
		} else {
			$this->Flash->error('The FAQ  couldn\'t be deleted.');
		}

		return $this->redirect(['action' => 'index']);

	}


	protected function _ajax_reorder_faq_list() {

		$faqs = $this->request->data['serialized_faqs'];

		foreach ($faqs as $position => $rootFaq) {
			$this->Faqs->rebuildTree($rootFaq, $position, $rootFaq['id'], true);
		}

		$response = [
			'success' => true
		];

		$this->set([
			'response' => $response,
			'_serialize' => ['response']
		]);

	}


}
