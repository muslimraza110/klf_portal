<?php
namespace App\Controller;

use Cake\Network\Exception\NotFoundException;

class CategoriesController extends \App\Controller\AppController {

	protected function _index() {
		$categories = $this->Categories->find();
		$this->set(compact('categories'));
	}

	protected function _add($parentId) {

		if (! $this->Categories->exists($parentId))
			throw new NotFoundException();

		$postCategory = $this->Categories->newEntity([
			'parent_id' => $parentId
		]);

		$this->set('title', 'New Category');
		$this->_form($postCategory);
	}

	protected function _edit($id) {

		if (! $this->Categories->exists($id))
			throw new NotFoundException();

		$postCategory = $this->Categories->get($id);

		$this->set('title', 'Edit Category');
		$this->_form($postCategory);
	}

	protected function _form($postCategory) {

		if ($this->request->is(['post', 'put'])) {
			$postCategory = $this->Categories->patchEntity($postCategory, $this->request->data);

			if ($this->Categories->save($postCategory)) {
				$this->Flash->success('The category "' . $postCategory->name . '" has been saved.');
				return $this->redirect(['controller' => 'Categories', 'action' => 'index']);
			}
			else {
				$this->Flash->success('The category could not be saved. Please review the form for errors.');
			}
		}

		$this->set(compact('postCategory'));
		$this->render('_form');
	}

	protected function _delete($id) {
		$this->request->allowMethod('post');

		if (! $this->Categories->exists($id))
			throw new NotFoundException();

		$postCategory = $this->Categories->get($id);

		if ($this->Categories->delete($postCategory))
			$this->Flash->success('The category "' . $postCategory->name . '" has been deleted.');
		else {
			if(!$this->Session->check('Message.flash')) {
				$this->Flash->error('The category "' . $postCategory->name . '" couldn\'t be deleted.');
			}
		}

		return $this->redirect(['controller' => 'Categories', 'action' => 'index']);
	}

}