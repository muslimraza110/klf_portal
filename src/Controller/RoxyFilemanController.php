<?php

namespace App\Controller;

class RoxyFilemanController extends AppController {

	public function initialize() {

		parent::initialize();

		$this->loadComponent('RequestHandler');

		$this->loadModel('Folders');
		$this->loadModel('Files');
		$this->loadModel('AssetManager.Assets');

	}

	protected function _dirtree() {

		$folders = $this->Folders->find()
				->contain([
					'Files' => ['resource']
				]);

		$response = [];

		$requestType = $this->request->query('type');
		$webImages = $this->Assets->mimeTypes('webImage');

		foreach ($folders as $folder) {

			if ($requestType == 'image') {

				foreach ($folder->files as &$file) {

					if (!in_array($file->resource->type, $webImages))
						$file = null;

				}

				$folder->files = array_filter($folder->files);

			}

			$folderObj = new \stdClass;

			$folderObj->p = '/'.$folder->name;
			$folderObj->f = count($folder->files);
			$folderObj->d = 0;

			$response[] = $folderObj;

		}

		$this->RequestHandler->renderAs($this, 'json');

		$this->set(compact('response'));
		$this->set('_serialize', 'response');

	}

	protected function _fileslist() {

		$path = $this->request->query('d');

		$folder = $this->Folders->find()
				->contain([
					'Files' => ['resource']
				])
				->where([
					'Folders.name' => trim($path, '/')
				])
				->first();

		$response = [];

		$requestType = $this->request->query('type');
		$webImages = $this->Assets->mimeTypes('webImage');

		foreach ($folder->files as $file) {

			if ($requestType == 'image' && !in_array($file->resource->type, $webImages))
				continue;

			$fileObj = new \stdClass;

			$fileObj->p = $file->resource->relativeUrl();
			$fileObj->s = $file->resource->size;
			$fileObj->t = $file->resource->modified->toUnixString();
			$fileObj->w = (int)$file->resource->width;
			$fileObj->h = (int)$file->resource->height;

			$response[] = $fileObj;

		}

		$this->RequestHandler->renderAs($this, 'json');

		$this->set(compact('response'));
		$this->set('_serialize', 'response');

	}

	protected function _createdir() {

		$name = $this->request->query('n');

		$response = new \stdClass;

		if (!empty($name)) {

			$folder = $this->Folders->newEntity([
				'name' => $name
			]);

			if ($this->Folders->save($folder)) {

				$response->res = 'ok';
				$response->msg = '';

			} else {

				$response->res = 'error';
				$response->msg = 'Folder could not be created.';

			}

		}

		$this->RequestHandler->renderAs($this, 'json');

		$this->set(compact('response'));
		$this->set('_serialize', 'response');

	}

	protected function _deletedir() {

		$name = trim($this->request->query('d'), '/');

		$response = new \stdClass;

		if (!empty($name)) {

			$folder = $this->Folders->find()
					->where([
						'name' => $name
					])
					->first();

			if ($this->Folders->delete($folder)) {

				$response->res = 'ok';
				$response->msg = '';

			} else {

				$response->res = 'error';
				$response->msg = 'Folder could not be deleted.';

			}

		}

		$this->RequestHandler->renderAs($this, 'json');

		$this->set(compact('response'));
		$this->set('_serialize', 'response');

	}

	protected function _upload() {

		$folderName = trim($this->request->data('d'), '/');

		$response = new \stdClass;

		if (!empty($folderName)) {

			$folder = $this->Folders->find()
					->where([
							'name' => $folderName
					])
					->first();

			if (!empty($folder)) {

				$error = false;

				foreach ($this->request->data['files'] as $upload) {

					$file = $this->Files->newEntity([
						'folder_id' => $folder->id,
						'resource' => [
							'upload' => $upload
						]
					]);

					if (!$this->Files->save($file, ['associated' => 'resource']))
						$error = true;

				}

				if (!$error) {

					$response->res = 'ok';
					$response->msg = '';

				} else {

					$response->res = 'error';
					$response->msg = 'One of your files could not be uploaded.';

				}

			}

		}

		$this->RequestHandler->renderAs($this, 'json');

		$this->set(compact('response'));
		$this->set('_serialize', 'response');

	}

	protected function _deletefile() {

		$name = $this->request->query('f');

		$response = new \stdClass;

		preg_match('~.*-(\d+)\..*~', $name, $matches);

		$assetId = (int)$matches[1];

		if ($assetId > 0) {

			$asset = $this->Assets->get($assetId);

			if (!empty($asset) && $asset->model == 'FilesTable') {

				$file = $this->Files->get($asset->model_id);

				if ($this->Files->delete($file)) {

					$response->res = 'ok';
					$response->msg = '';

				} else {

					$response->res = 'error';
					$response->msg = 'File could not be deleted.';

				}

			}

		}

		$this->RequestHandler->renderAs($this, 'json');

		$this->set(compact('response'));
		$this->set('_serialize', 'response');

	}

}