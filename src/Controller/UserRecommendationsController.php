<?php

namespace App\Controller;

use Cake\Collection\Collection;
use Cake\Network\Exception\NotFoundException;

class UserRecommendationsController extends AppController {

	protected function _index() {
		$contain = [
			'Recommenders',
			'Users',
			'Votes'
		];

		$userRecommendations = $this->UserRecommendations->find()
			->contain($contain);

		$this->paginate = [
			'limit' => 30,
			'sortWhitelist' => [
				'first_name', 'email', 'Recommenders.first_name', 'created'
			]
		];

    $noAdminDecisionRecommendations = $this->UserRecommendations->find()
			->contain($contain)
			->where(['UserRecommendations.vote_admin_decision' => false]);


		$this->paginate($userRecommendations);

		$title = 'Recommendations';
		$bannerActions = [];

		$this->set(compact('title', 'bannerActions', 'userRecommendations', 'noAdminDecisionRecommendations'));

		$this->render('/UserRecommendations/_index');

	}

	protected function _view($id) {

		if (!in_array($this->_authUser->role, ['A', 'O'])) {

			$userRecommendation = $this->UserRecommendations->get($id, [
				'contain' => [
					'TaggedUsers'
				]
			]);

			$taggedUserIds = (new Collection($userRecommendation->tagged_users))->extract('id')->toArray();

			if (!in_array($this->_authUser->id, $taggedUserIds))
				throw new NotFoundException;

			$this->set('bannerActions', []);

		}

		if ($this->request->is(['post', 'put'])) {

			if (!empty($note = $this->request->data('note'))) {

				$userRecommendationNote = $this->UserRecommendations->UserRecommendationNotes->newEntity([
					'user_recommendation_id' => $id,
					'user_id' => $this->_authUser->id,
					'note' => $note,
					'private' => $this->request->data('private')
				]);

				if ($this->UserRecommendations->UserRecommendationNotes->save($userRecommendationNote)) {

					$this->Flash->success('Note has been saved successfully.');

					unset($this->request->data['note']);

				} else
					$this->Flash->error('An error occurred. Note could not be saved.');

			}

		}

		$conditions = [];

		if (!in_array($this->_authUser->role, ['A', 'O']))
			$conditions = [
				'UserRecommendationNotes.private' => false
			];

		$userRecommendation = $this->UserRecommendations->get($id, [
			'contain' => [
				'Recommenders.ProfileImg',
				'UserRecommendationNotes' => [
					'Users.ProfileImg',
					'conditions' => $conditions
				]
			]
		]);

		$vote = $votes = null;

		if (in_array($this->_authUser->role, ['A', 'O']))
			$vote = $this->UserRecommendations->Users->UserVotes
				->find('votes', [
					'entity_id' => $id,
					'voter_id' => $this->_authUser->id,
					'model' => 'UserRecommendations'
				])
				->first();

		if (in_array($this->_authUser->role, ['A']))
			$votes = $this->UserRecommendations->Users->UserVotes
				->find('votes', [
					'entity_id' => $id,
					'model' => 'UserRecommendations'
				]);

		$title = 'Recommendation';
		$breadcrumbs = [
			['name' => 'Recommendations', 'route' => ['action' => 'index']],
			['name' => $title, 'active' => true]
		];

		$this->set(compact('title', 'breadcrumbs', 'userRecommendation', 'vote', 'votes'));

		$this->render('/UserRecommendations/_view');

	}

	protected function _ajax_html_user_list() {

		$id = $this->request->data('id');

		if (empty($id)) {

			$this->autoRender = false;

			return '';

		}

		$userRecommendation = $this->UserRecommendations->get($id, [
			'contain' => [
				'TaggedUsers.ProfileImg'
			]
		]);

		$this->set(compact('userRecommendation'));

		$this->render('/UserRecommendations/_ajax_html_user_list');

	}

	protected function _ajax_add_user() {

		$id = $this->request->data('id');
		$userId = $this->request->data('user_id');

		$response = ['status' => 'error'];

		if (!empty($id) && !empty($userId)) {

			$this->loadModel('UserRecommendationsUsers');

			$userRecommendationsUsers = $this->UserRecommendationsUsers->newEntity([
				'user_recommendation_id' => $id,
				'user_id' => $userId
			]);

			if ($this->UserRecommendationsUsers->save($userRecommendationsUsers)) {

				$userRecommendation = $this->UserRecommendations->get($id);

				$userRecommendation->sendTaggedUserInvitation(
					$this->UserRecommendations->Users->get($userId)
				);

				$response = ['status' => 'ok'];

			}

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_remove_user() {

		$id = $this->request->data('id');
		$userId = $this->request->data('user_id');

		$response = ['status' => 'error'];

		if (!empty($id) && !empty($userId)) {

			$this->loadModel('UserRecommendationsUsers');

			$userRecommendationsUsers = $this->UserRecommendationsUsers
				->findByUserRecommendationIdAndUserId($id, $userId)
				->first();

			if (!empty($userRecommendationsUsers) && $this->UserRecommendationsUsers->delete($userRecommendationsUsers))
				$response = ['status' => 'ok'];

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _recommend() {

		$userRecommendation = $this->UserRecommendations->newEntity([
			'recommender_id' => $this->_authUser->id,
			'pending' => true
		]);

		if ($this->request->is(['post', 'put'])) {

			$email = $this->request->data('email');

			$existingRecommendations = $this->UserRecommendations->findByEmail($email)
				->count();

			$existingUsers = $this->UserRecommendations->Users->findByEmail($email)
				->count();

			$existingInvitations = $this->UserRecommendations->Users->UserInvitations->findByEmail($email)
				->count();

			if ($existingRecommendations > 0 || $existingUsers > 0 || $existingInvitations > 0)
				$this->Flash->error('Contact is a member or has already been recommended.');
			else {

				$this->request->data['user_recommendation_notes'][0]['user_id'] = $this->_authUser->id;

				if (!empty($this->request->data['tagged_users']))
					$this->request->data['tagged_users'] = [
						'_ids' => explode(',', $this->request->data['tagged_users'])
					];

				$this->UserRecommendations->patchEntity($userRecommendation, $this->request->data, ['associated' => ['UserRecommendationNotes', 'TaggedUsers']]);

				if ($this->UserRecommendations->save($userRecommendation)) {

					if (!empty($userRecommendation->tagged_users)) {
						foreach ($userRecommendation->tagged_users as $user) {

							if ($user->id == $this->_authUser->id) {
								continue;
							}

							$userRecommendation->sendTaggedUserInvitation($user);

						}
					}

					$this->Flash->success('Contact has been recommended.');

					return $this->redirect(['controller' => 'Users', 'action' => 'index']);

				} else
					$this->Flash->error('An error occurred. Contact could not be recommended.');

			}

		}

		$this->set(compact('userRecommendation'));

		$this->render('/UserRecommendations/_recommend');

	}

}
