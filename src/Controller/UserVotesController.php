<?php

namespace App\Controller;

use Cake\I18n\Time;

class UserVotesController extends AppController {

	protected function _ajax_vote() {

		$response = ['status' => 'error'];

		$id = $this->request->data('id');
		$voteId = $this->request->data('vote_id');
		$model = $this->request->data('model');
		$vote = $this->request->data('vote');

		if (!empty($id) && !empty($model)) {

			if (!empty($voteId)) {

				$userVote = $this->UserVotes->findByIdAndVoterId($voteId, $this->_authUser->id)
					->first();

				$userVote->vote = $vote;
				$userVote->created = Time::now();

			} else
				$userVote = $this->UserVotes->newEntity([
					'entity_id' => $id,
					'voter_id' => $this->_authUser->id,
					'model' => $model,
					'vote' => $vote
				]);

			if ($this->UserVotes->save($userVote)) {

				$this->UserVotes->makeDecision($id, $model);

				$response = ['status' => 'ok'];

			}

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_final_decision() {

		$response = ['status' => 'error'];

		$id = $this->request->data('id');
		$model = $this->request->data('model');
		$vote = $this->request->data('vote');

		if (!empty($id) && !empty($model) && in_array($model, ['Users', 'UserRecommendations'])) {

			$this->loadModel($model);

			$entity = $this->$model->get($id);

			if ($vote == 'Y')
				$entity->pending = false;
			else
				$entity->pending = true;

			$entity->vote_decision = $vote;
			$entity->vote_decision_date = Time::now();
			$entity->vote_admin_decision = true;

			if ($this->$model->save($entity)) {

				$this->UserVotes->processAdminDecision($id, $model);

				$response = ['status' => 'ok'];

			}

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

}