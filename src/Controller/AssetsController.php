<?php
namespace App\Controller;

use Cake\Filesystem\File;
use Cake\Routing\Router;

class AssetsController extends AppController {

	protected function _view($fileId) {

		$this->autoRender = false;

		$asset = $this->Assets->get($fileId);

		$result = $this->s3GetObject($asset);

		$this->response->type($result['ContentType']);
		$this->response->body($result['Body']);

	}

	protected function _download($fileId) {

		$this->autoRender = false;

		$asset = $this->Assets->get($fileId);

		$result = $this->s3GetObject($asset);

		$this->response->type($result['ContentType']);
		$this->response->download($asset->name);
		$this->response->body($result['Body']);

	}

	protected function _delete($id) {
    $this->autoRender = false;

   	$asset = $this->Assets->get($id);

    $file = new File(ROOT . DS . 'assets' . DS . $asset->id);

    $file->delete();

    $this->Assets->delete($asset);

		//---aws --- //

		$s3 = $this->Assets->s3Client();

		$result = $s3->deleteObject(array(
			'Bucket' => $this->Assets->s3Bucket(),
			'Key'    => $id
		));

		//---aws---//

    $this->Flash->success('Image has been deleted.');

		if (!empty($returnUrl = $this->request->query('return_url')))
			return $this->redirect(Router::fullBaseUrl().$returnUrl);

  }

}
