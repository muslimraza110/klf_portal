<?php

namespace App\Controller\Copywriter;

class UsersController extends \App\Controller\UsersController {

	public function profile() {

		$this->_profile();

	}

	public function edit_profile() {

		$this->_edit_profile();

	}

	public function index() {

		$this->_index();

	}

	public function view($id) {

		$this->_view($id);

	}

	public function change_password() {

		$this->_change_password();

	}

	public function edit_bio($id) {

		$this->_edit_bio($id);

	}

	public function upload_image($id, $imageType) {

		$this->_upload_image($id, $imageType);

	}

	public function ajax_reset_image() {

		$this->_ajax_reset_image();

	}

	public function ajax_delete_image() {

		$this->_ajax_delete_image();

	}

	public function dashboard() {

		$this->_dashboard();

	}

}
