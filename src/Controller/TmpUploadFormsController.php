<?php
namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class TmpUploadFormsController extends AppController {

	protected function _batch_upload_forms() {
		$this->autoRender = false;
		$Forms = TableRegistry::get('Forms');
		$tmpForms = $this->TmpUploadForms->find();

		foreach ($tmpForms as $tmpForm) {
			$data = [
				'id' => $tmpForm->id,
				'user_id' => 5,
				'type' => 'F',
				'name' => $tmpForm->name,
				'topic' => 'Resources',
				'audience' => '["F","T"]',
				'start_date' => Time::now(),
				'form_upload' => [
					'upload' => [
						'name' => trim($tmpForm->file),
						'type' => 'application/pdf',
						'size' => 0,
						'tmp_name' => ''
					]
				]
			];

			$form = $Forms->find()
				->contain(['FormUpload'])
				->where([
					'Forms.id' => $tmpForm->id
				])
				->first();

			if (empty($form)) {
				$form = $Forms->newEntity();
			}

			$form = $Forms->patchEntity($form, $data, [
				'associated' => [
					'FormUpload'
				]
			]);

			//pr($form);

			if ($Forms->save($form)) {
				$fileLocation = ROOT . DS . '_DOCS' . DS . 'upload' . DS . $form->form_upload->name;
				$filePieces = explode('.', $form->form_upload->name);
				$extension = array_pop($filePieces);
				$filename = implode('.', $filePieces);
				$newFilename = $filename . '-' . $form->form_upload->id . '.' . $extension;
				$newFileLocation = ROOT . DS . 'assets' . DS . $newFilename;

				if (!copy($fileLocation, $newFileLocation)) {
					pr($fileLocation . 'cannot be found.');
				}
				else {
					pr($filename . ' has been uploaded.');
				}
			}

		}
	}

	protected function _batch_upload_posts() {
		$this->autoRender = false;
		ini_set('max_execution_time', 300);

		$Posts = TableRegistry::get('Posts');
		$PostForms = TableRegistry::get('TmpUploadMemos');
		$tmpForms = $PostForms->find();

		foreach ($tmpForms as $key => $tmpForm) {

			switch($tmpForm->type) {
				case 'Weapons':
					$category_id = 9;
					$theme_id = 5;
				break;

				case 'Rag':
					$category_id = 10;
					$theme_id = 1;
				break;

				case 'Memo':
					$category_id = 8;
					$theme_id = 4;
				break;

				case 'Sweep':
					$category_id = 7;
					$theme_id = 6;
				break;
			}

			$data = [
				'user_id' => 5,
				'post_type_id' => 2,
				'post_category_id' => $category_id,
				'post_theme_id' => $theme_id,
				'name' => $tmpForm->name,
				'published' => $tmpForm->published,
				'post_upload' => [
					'upload' => [
						'name' => trim($tmpForm->file),
						'type' => 'application/pdf',
						'size' => 0,
						'tmp_name' => ''
					]
				]
			];

			$post = $Posts->newEntity();
			$post = $Posts->patchEntity($post, $data, [
				'associated' => [
					'PostUpload'
				]
			]);

			if ($Posts->save($post)) {

				$fileLocation = ROOT . DS . '_DOCS' . DS . 'upload' . DS . $post->post_upload->name;
				$filePieces = explode('.', $post->post_upload->name);

				$extension = array_pop($filePieces);
				$filename = implode('.', $filePieces);
				$newFilename = $filename . '-' . $post->post_upload->id . '.' . $extension;
				$newFileLocation = ROOT . DS . 'assets' . DS . $newFilename;

				if (!copy($fileLocation, $newFileLocation)) {
					pr($fileLocation . 'cannot be found.');
				}
				else {
					pr($filename . ' has been uploaded.');
				}
			}
		}

	}

}
