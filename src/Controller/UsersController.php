<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Log\Log;
use Cake\I18n\Time;
use Cake\Utility\Security;
use Cake\Utility\Text;
use Cake\Network\Exception\NotFoundException;
use App\Form\PasswordResetForm;
use Cake\Validation\Validator;
use Cake\Collection\Collection;

class UsersController extends \App\Controller\AppController {
	
	public function initialize() {
		parent::initialize();
		$this->Auth->allow(['register', 'forgot_password_step_1', 'forgot_password_step_2', 'set_initial_password', 'ajax_autocomplete_group_list']);
		
		if (in_array($this->request->action, ['login', 'register', 'forgot_password_step_1', 'forgot_password_step_2', 'set_initial_password']))
			$this->viewBuilder()->layout('empty');
	}
	
	protected function _index() {
		$this->set('title', 'Contacts');
		
		$conditions = [];

		if (!empty($this->request->query['q'])) {

			$this->request->data['q'] = $this->request->query['q'];
			$conditions['Users.id IN'] = $this->Search->usersQuery($this->request->query['q'])->select(['Users.id']);

		}
		
		if (!empty($this->request->query['letter'])) {

			$this->request->data['letter'] = $this->request->query['letter'];
			$conditions['Users.last_name LIKE'] = $this->request->query['letter'] . '%';

		}

		if (!empty($this->request->query['role'])) {

			$this->request->data['role'] = $this->request->query['role'];
			$conditions['Users.role'] = $this->request->query['role'];

		}

		if (!in_array($this->_authUser->role, ['A', 'O']))
			$conditions['Users.pending'] = false;
		elseif (!empty($pending = $this->request->query('pending'))) {

			$this->request->data['pending'] = $pending;

			$conditions['Users.pending'] = true;

		}

		$roles = $this->Users->roles;

		if (!in_array($this->_authUser->role, ['A', 'O'])) {

			unset($roles['A']);

			$conditions['Users.role !='] = 'A';

		}
		
		$this->paginate = [
			'conditions' => $conditions,
			'contain' => [
				'UserDetails',
				'ProfileImg',
			],
			'sortWhitelist' => ['first_name', 'last_name', 'email', 'role', 'last_login'],
			'limit' => 30
		];
		
		$users = $this->paginate($this->Users);

		if (!in_array($this->_authUser->role, ['A', 'O']))
			$this->set('bannerActions', []);

		$this->set(compact('users', 'roles'));
		$this->render('/Users/_index');
	}
	
	protected function _view($id) {

		$conditions = [];

		if (!in_array($this->_authUser->role, ['A', 'O']))
			$conditions['Users.pending'] = false;

		$user = $this->Users->findById($id)
			->contain([
				'UserDetails',
				'ProfileImg',
				'BannerImg',
				'Groups' => function ($query) {

					$query->contain([
							'ProfileImg',
							'Projects'
						]);

					if (!in_array($this->_authUser->role, ['A', 'O'])) {

						$authUserGroupIds = $this->_authUser->getGroupIds();

						$query->where([
							'Groups.pending' => false,
							'OR' => [
								'Groups.hidden' => false,
								'Groups.id IN' => !empty($authUserGroupIds) ? $authUserGroupIds : [0]
							]
						]);

					}

					return $query;

				},
				'UserFamilies.RelatedUsers.ProfileImg',
				'FundraisingPledges' => [
					'Fundraisings.Initiatives',
					'sort' => 'Fundraisings.initiative_id',
				]
			])
			->where($conditions)
			->first();

		$categorizedGroups = $this->Users->Groups->categorize($user->groups);

		$projectTree = $user->projectTree();

		$vote = $votes = null;

		if ($this->_authUser->role == 'O')
			$vote = $this->Users->UserVotes
				->find('votes', [
					'entity_id' => $id,
					'voter_id' => $this->_authUser->id,
					'model' => 'Users'
				])
				->first();

		if (in_array($this->_authUser->role, ['A']))
			$votes = $this->Users->UserVotes
				->find('votes', [
					'entity_id' => $id,
					'model' => 'Users'
				]);
		
		if (!$user)
			throw new NotFoundException('The user could not be found.');
		
		if ($this->request->action == 'view') {
			$title = $user->name;
			$breadcrumbs[] = ['name' => 'Contacts', 'route' => ['action' => 'index']];
			$this->set(compact('title', 'breadcrumbs'));
		}
		$relations = $this->Users->UserFamilies->getRelations();
		$this->set(compact('user', 'categorizedGroups', 'vote', 'votes', 'projectTree', 'relations'));
		$this->render('/Users/_view');
	}
	
	protected function _profile() {
		$this->set('title', 'Your Profile');
		$this->_view($this->_authUser->id);
	}
	
	protected function _edit_profile() {
		$this->set('title', 'Edit Your Profile');
		$user = $this->Users->getUser($this->_authUser->id);

		$categorizedGroups = $this->Users->Groups->categorize($user->groups);

		$groupRequests = $this->Users->GroupRequests->find()
			->contain([
				'Groups.ProfileImg'
			])
			->where([
				'GroupRequests.user_id' => $user->id,
				'GroupRequests.status' => 'P'
			])
			->toArray();

		$this->set(compact('categorizedGroups', 'groupRequests'));

		$this->_form($user);
	}
	
	protected function _edit($id) {
		
		$user = $this->Users->getUser($id);

		if (!$user || !in_array($this->_authUser->role, ['A', 'O']))
			throw new NotFoundException("The user could not be found.");
		
		$this->_form($user);
	}

	protected function _edit_bio($id) {

		$user = $this->Users->getUser($id);

		if (empty($user))
			throw new NotFoundException;

		if ($this->request->is(['post', 'put'])) {

			$this->request->data['user_comments'][key($this->request->data['user_comments'])]['copywriter_user_id'] = $this->_authUser->id;
			$this->Users->patchEntity($user, $this->request->data, ['associated' => ['UserDetails', 'UserComments']]);

			if ($this->Users->save($user)) {

				$this->Flash->success('Biography has been saved successfully.');

				$this->redirect(['action' => 'index']);

			} else
				$this->Flash->error('An error occurred. Biography could not be saved.');

		}

		$title = 'Biography';

		$breadcrumbs = [
			['name' => 'Contacts', 'route' => ['action' => 'index']],
			['name' => $title, 'active' => true]
		];

		$this->set(compact('title', 'breadcrumbs', 'user'));
		$comments = $this->Users->UserComments->find()
			->contain('Users')
			->where(
				[
				 'user_id' => $id
				])
			->toArray();

		$this->set(compact('title', 'breadcrumbs', 'user', 'comments'));
		$this->render('/Users/_edit_bio');

	}

	protected function _delete($id) {
		$this->request->allowMethod('post');

		if (! $this->Users->exists($id))
			throw new NotFoundException('The contact could not be found.');

		$user = $this->Users->get($id);
		$user->status = 'D';

		if ($this->Users->save($user))
			$this->Flash->success(sprintf('The contact "%s" has been deleted.', $user->name));
		else
			$this->Flash->error(sprintf('The contact "%s" couldn\'t be deleted.', $user->name));

		return $this->redirect(['action' => 'index']);
	}
	
	protected function _form($user) {
		
		if ($this->request->is(['post', 'put'])) {
			
			if (!$user->isNew() && !empty($this->request->data['notification_settings'])) {
				foreach ($this->request->data['notification_settings'] as $categoryId => $value) {
					$user->saveNotifications($categoryId, $value['activate'], $value['send_email'], 'Posts');
				}
			}
			

			unset($this->request->data['notification_settings']);
			$comment =$this->request->data('user_comments.0.comment');
      if( empty($comment) || empty(trim($comment))){
			 unset($this->request->data['user_comments']);
			}
			if (!empty($expirationDate = $this->request->data('expiration_date')))
				$this->request->data['expiration_date'] = Time::createFromFormat('l F j, Y', $expirationDate);

			$user = $this->Users->patchEntity($user, $this->request->data, ['associated' => ['Groups', 'UserDetails', 'UserComments']]);

			if ($user->dirty('pending')) {

				$user->vote_decision = $user->pending ? 'N' : 'Y';
				$user->vote_decision_date = Time::now();

				if (in_array($this->_authUser->role, ['A']))
					$user->vote_admin_decision = true;

			}

			$notifyCopywriter = $user->dirty('user_comments');
			if ($this->Users->save($user)) {

				if ($notifyCopywriter) {

					$copywriter = $this->_authUser->getCopywriter();
					$this->_authUser->sendCommentToCopywriter($copywriter, $comment);

				}

				if (!empty($this->request->data['user_alias'])) {

					$userAliases = $this->request->data('user_alias');

					$groupsUsers = $this->Users->GroupsUsers->find()
						->where([
							'GroupsUsers.id IN' => array_keys($userAliases)
						]);

					foreach ($groupsUsers as $groupUser) {
						$groupUser->user_alias = !empty($userAliases[$groupUser->id]) ? $userAliases[$groupUser->id] : null;
					}

					$this->Users->GroupsUsers->saveMany($groupsUsers);

				}

				$this->Flash->success('The user has been saved.');
				return $this->redirect(['action' => 'view', $user->id]);

			}
			else {
				$this->Flash->error('An error occurred while trying to save the user.');
			}
			
		}
		
		if ($this->request->action == 'add' || $this->request->action == 'edit') {
			$title = ($this->request->action == 'add') ? 'New Contact' : 'Edit Contact';
			$breadcrumbs[] = ['name' => 'Contacts', 'route' => ['action' => 'index']];
			$this->set(compact('title', 'breadcrumbs'));
		}
		
		$comments = $this->Users->UserComments->find()
			->where(
				['status' => 'A',
				 'user_id' => $this->_authUser->id
				])
			->toArray();
		$roles = $this->Users->roles;
		$genders = $this->Users->genders;
		$userTitles = $this->Users->titles;
		$postCategories = $this->Users->Posts->Categories->findByModel('Posts');

		$categories = $this->Users->Groups->Categories->find();

		$projects = $this->Users->Groups->Projects->find()
			->contain([
				'Groups',
				'Users'
				])
			->matching('Users', function($q) {
				return $q->where([
					'Users.id' => $this->_authUser->id
				]);
			}
		)
		->toArray();

		$groups = $this->Users->Groups->find()
			->contain([
				'Categories'
			])
			->where(
				$this->Users->Groups->buildUserRoleConditions($this->_authUser)
			)
			->order([
				'Categories.name',
				'Groups.name'
			])
			->combine('id', 'name', 'category.name');

		$user->unsetProperty('password');
		$user->unsetProperty('confirm_password');

		$this->set(compact('user', 'roles', 'postCategories', 'groups', 'genders', 'userTitles', 'comments', 'categories', 'projects'));
		$this->render('/Users/_form');
	}

	protected function _family($id, $userFamilyId = null) {

		$user = $this->Users->get($id);

		$relations = $this->Users->UserFamilies->getRelations();
		$relations['Custom'] = 'Custom';

		if (empty($userFamilyId))
			$userFamily = $this->Users->UserFamilies->newEntity([
				'user_id' => $id
			]);
		else {

			$userFamily = $this->Users->UserFamilies->get($userFamilyId, [
				'contain' => [
					'RelatedUsers'
				]
			]);

			$this->request->data['name'] = $userFamily->related_user->name;
			$this->request->data['related_user_id'] = $userFamily->related_user->id;

			if (
				!in_array($userFamily->relation, $relations)
				&& !$this->request->is(['post', 'put'])
			) {

				$this->request->data['relation'] = 'Custom';
				$this->request->data['custom'] = $userFamily->relation;

			}


		}

		if (!empty($user->gender)) {

			if ($user->gender == 'F')
				unset($relations['Uncle']);
			else
				unset($relations['Aunt']);

		}

		if ($this->request->is(['post', 'put'])) {

			if ($this->request->data('relation') == 'Custom')
				$this->request->data['relation'] = $this->request->data('custom');

			if (empty($this->request->data['relation']) || empty($this->request->data['related_user_id']))
				$this->Flash->error('Please fill in all fields.');
			else {

				$userFamily = $this->Users->UserFamilies->patchEntity($userFamily, $this->request->data);

				if ($this->Users->UserFamilies->save($userFamily)) {

					if (empty($userFamilyId)) {

						$inverseUserFamily = $this->Users->UserFamilies->newEntity([
							'user_id' => $userFamily->related_user_id,
							'related_user_id' => $userFamily->user_id,
							'relation' => $userFamily->getRelationInverse()
						]);

						$this->Users->UserFamilies->save($inverseUserFamily);

						$userFamily = $this->Users->UserFamilies->get($userFamily->id, [
							'contain' => [
								'Users',
								'RelatedUsers'
							]
						]);

						$userFamily->sendNotification();

					}

					$this->Flash->success('Family relation added successfully.');

					if ($this->_authUser->id == $id)
						return $this->redirect(['action' => 'edit_profile']);

					return $this->redirect(['action' => 'edit', $id]);

				} else
					$this->Flash->error('An error occurred. Family relation could not be added.');

			}

		}

		if ($this->_authUser->id == $id)
			$breadcrumbs = [
				['name' => 'Edit My Profile', 'route' => ['action' => 'edit_profile']]
			];
		else
			$breadcrumbs = [
				['name' => 'Edit Contact', 'route' => ['action' => 'edit', $id]]
			];

		$breadcrumbs[] = ['name' => 'Family', 'active' => true];

		$this->set(compact('breadcrumbs', 'userFamily', 'relations'));

		$this->render('/Users/_family');

	}

	protected function _delete_family($id, $userFamilyId) {

		$userFamily = $this->Users->UserFamilies->get($userFamilyId);

		if ($this->Users->UserFamilies->delete($userFamily))
			$this->Flash->success('Family relation has been deleted successfully.');
		else
			$this->Flash->error('An error occurred. Family relation could not be deleted.');

		if ($this->_authUser->id == $id)
			return $this->redirect(['action' => 'edit_profile']);

		return $this->redirect(['action' => 'edit', $id]);

	}

	protected function _change_password() {

		$user = $this->Users->get($this->_authUser->id);

		if ($this->request->is(['post', 'put'])) {

			$this->Users->patchEntity($user, $this->request->data);

			if ($this->Users->save($user)) {

				$this->Flash->success('Password has been changed successfully.');

				return $this->redirect($user->home_route);

			} else
				$this->Flash->error('An error occurred. Password could not be changed.');

		}

		$user->unsetProperty('password');
		$user->unsetProperty('confirm_password');

		$this->set(compact('user'));

		$this->render('/Users/_change_password');

	}

	protected function _ajax_autocomplete_user_list() {

		$response = [];

		$term = $this->request->query('term');

		if (!empty($term)) {

			$users = $this->Users->find();

			$users = $users
				->contain([
					'ProfileImg'
				])
				->where([
					'Users.pending' => false,
					'OR' => [
						'Users.first_name LIKE' => '%'.$term.'%',
						'Users.last_name LIKE' => '%'.$term.'%',
						function ($exp, $query) use ($term, $users) {

							return $exp->like(
								$users->func()->concat([
									'Users.first_name' => 'identifier',
									' ',
									'Users.last_name' => 'identifier'
								]),
								'%'.$term.'%'
							);

						}
					]
				]);

			foreach ($users as $user)
				$response[] = [
					'value' => $user->id,
					'label' => $user->name,
					'profile_thumb' => $user->profile_thumb
				];

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	public function ajax_autocomplete_group_list() {

		$response = [];

		$term = $this->request->query('term');

		if (!empty($term)) {

			$groups = $this->Users->Groups->find()
				->contain([
					'ProfileImg'
				])
				->where([
					'Groups.pending' => false,
					'Groups.hidden' => false,
					'Groups.name LIKE' => '%'.$term.'%'
				]);

			foreach ($groups as $group)
				$response[] = [
					'value' => $group->id,
					'label' => $group->name,
					'profile_thumb' => $group->profile_thumb
				];

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_clear_comments(){

		$response = ['status' => 'error'];

		$update = $this->Users->UserComments->updateAll(
			['status' => null] ,
			[
				'status' => 'A',
				'user_id' => $this->_authUser->id
			]
		);

		if($update){
			$response = ['status' => 'ok'];
		}
		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}
	protected function _add() {
		$user = $this->Users->newEntity([
			'vote_decision' => 'Y',
			'vote_decision_date' => Time::now(),
			'vote_admin_decision' => true
		]);
		$user->user_details = $this->Users->UserDetails->newEntity();
		$this->_form($user);
	}

	protected function _loginAs($id) {
		$this->request->allowMethod(['post']);

		//Set current login user ID into the session
		$previousLogin = $this->Auth->user('id');
		$this->request->session()->write('previousLogin', $previousLogin);

		//Login as the selected user
		$user = $this->Users->findById($id)->first();
		$this->request->session()->write('temporaryLogin', $user->toArray());
		$this->Auth->setUser($user->toArray());

		$this->Flash->info(sprintf('You are now logged in as "<strong>%s</strong>".<br /> The chat functionality has been disabled.', $user['name']));

		return $this->redirect($user->home_route);
	}

	protected function _initial_login() {

		$users = $this->Users->find()
			->where([
				'OR' => [
					'Users.password' => '',
					'Users.password IS' => null,
				]
			])
			->groupBy(function ($user) {
				return ($user->reset_key == '') ? 'Emails that can be sent' : 'Emails that have been sent';
			})
			->toArray();

		$usersForNextBatch = [];

		if(!empty($users['Emails that can be sent']) && count($users['Emails that can be sent']) > 100) {
			$count = 0;
			foreach($users['Emails that can be sent'] as $key => $user) {
				$count++;

				if($count > 100) {
					$usersForNextBatch[] = $user;
					unset($users['Emails that can be sent'][$key]);
				}
			}
		}

		ksort($users);

		if ($this->request->is(['post', 'put'])) {
			$errors = [];

			foreach ($users['Emails that can be sent'] as $user) {

				$userInvitation = $this->Users->UserInvitations->newEntity([
					'user_id' => $user->id,
					'email' => $user->email,
					'hash' => sha1(Security::randomBytes(32))
				]);

				if ($this->Users->UserInvitations->save($userInvitation)) {

					$userInvitation->sendInvitation();

					$user->reset_key = Text::uuid();

					$this->Users->save($user);

				}

				/*
				// Original Molly Maid functionality
				$passwordReset = new PasswordResetForm();
				$user->template = 'setPassword';

				if (! $passwordReset->execute($user->toArray()))
					$errors[] = $passwordReset->error;
				*/
			}

			if (empty($errors)) {

				$anHourLater = Time::now()->second(0)->modify('+10 minutes');
				$anHourLaterMessage = 'Please wait until <strong class="text-danger" style="text-transform: uppercase;">' . $anHourLater->format('h:i a') . '</strong> to send the next batch of emails or else the emails will be flagged as spam.';

				setcookie("initial_login_time", $anHourLater->toUnixString(), $anHourLater->toUnixString());

				$this->request->session()->write([
					'initial_login.message' => $anHourLaterMessage,
					'initial_login.time' => $anHourLater
				]);

				$this->Flash->success(__('The emails have been sent.'));
			}
			else {
				$errorFlash = '<ul>';
				foreach ($errors as $error) {
					$errorFlash .= '<li>' . $error . '</li>';
				}
				$errorFlash .= '</ul>';

				$this->Flash->error($error);
			}

			$this->redirect([]);
		}

		if ($this->request->session()->check('initial_login')) {
			if($this->request->session()->read('initial_login.time') <= Time::now())
				$this->request->session()->delete('initial_login');
		}
		elseif (isset($_COOKIE["initial_login_time"])) {
			$anHourLater = Time::createFromTimestamp($_COOKIE["initial_login_time"]);
			$anHourLaterMessage = 'Please wait until <strong class="text-danger" style="text-transform: uppercase;">' . $anHourLater->format('h:i a') . '</strong> to send the next batch of emails or else the emails will be flagged as spam.';

			if($anHourLater <= Time::now()) {
				unset($_COOKIE["initial_login_time"]);
			}
			else {
				$this->request->session()->write([
					'initial_login.message' => $anHourLaterMessage,
					'initial_login.time' => $anHourLater
				]);
			}
		}

		$title = 'Initial Login';
		$breadcrumbs[] = ['name' => 'Contacts / People', 'route' => ['action' => 'index']];

		$this->set(compact('users', 'usersForNextBatch', 'title', 'breadcrumbs'));

		$this->render('/Users/_initial_login');

	}

	//-----------------------------------------------------------------------------------------
	
	public function register($hash) {

		if (empty($hash))
			throw new NotFoundException;

		$hashError = false;

		$userInvitation = $this->Users->UserInvitations->find()
			->contain([
				'Users'
			])
			->where([
				'UserInvitations.hash' => $hash
			])
			->first();

		if (
			empty($userInvitation)
			|| (!empty($userInvitation->user) && empty($userInvitation->user->reset_key))
		) {

			$user = $this->Users->newEntity();

			$hashError = true;

			$this->Flash->error('Your invitation was not found or has expired.');

		} else {

			if (!empty($userInvitation->recommended)) {

				$userRecommendation = $this->Users->UserRecommendations->find()
					->where([
						'UserRecommendations.user_id IS' => null,
						'UserRecommendations.email' => $userInvitation->email
					])
					->first();

				$user = $this->Users->newEntity([
					'email' => $userRecommendation->email,
					'first_name' => $userRecommendation->first_name,
					'last_name' => $userRecommendation->last_name
				]);

			} elseif (!empty($userInvitation->user)) {
				$user = $userInvitation->user;
			} else {
				$user = $this->Users->newEntity([
					'email' => $userInvitation->email
				]);
			}

		}

		if (!$hashError) {

			$this->Users->validator()->notEmpty('password', null, false);
			$this->Users->validator()->notEmpty('confirm_password', null, false);

			if ($this->request->is(['post', 'put'])) {

				if (empty($this->request->data['group_id']) && empty($this->request->data['group']))
					$this->Flash->error('Please join an existing group or create your own.');
				else {

					$groupError = false;

					if (!empty($this->request->data['group'])) {

						$group = $this->Users->Groups->newEntity([
							'category_id' => $this->request->data('group_category_id') == 3 ? 3 : 2,
							'name' => $this->request->data('group'),
							'email' => $this->request->data('email'),
							'pending' => false
						]);

						$this->Users->Groups->save($group);

					} else {

						$group = $this->Users->Groups->findById($this->request->data('group_id'))
							->where([
								'Groups.pending' => false,
								'Groups.hidden' => false
							])
							->first();

						if (empty($group)) {

							$groupError = true;

							$this->Flash->error('Selected group does not exist. Please choose another group.');

						}

					}

					if (!$groupError) {

						$user = $this->Users->patchEntity($user, $this->request->data);

						$user->vote_decision = 'Y';
						$user->vote_decision_date = Time::now();
						$user->vote_admin_decision = true;
						$user->reset_key = '';

						if ($this->Users->save($user)) {

							$this->Flash->success('Your account has been activated and you have been logged in automatically. Welcome!');

							// -

							if (!empty($this->request->data['group'])) {

								$this->loadModel('GroupsUsers');

								$groupsUsers = $this->GroupsUsers->newEntity([
									'group_id' => $group->id,
									'user_id' => $user->id,
									'admin' => true
								]);

								$this->GroupsUsers->save($groupsUsers);

							} else {

								$groupRequest = $this->Users->Groups->GroupRequests->newEntity([
									'group_id' => $group->id,
									'user_id' => $user->id
								]);

								$this->Users->Groups->GroupRequests->save($groupRequest);

							}

							// -

							$userInvitation->user_id = $user->id;

							$this->Users->UserInvitations->save($userInvitation);

							// -

							if (!empty($userRecommendation)) {

								$userRecommendation->user_id = $user->id;

								$this->Users->UserRecommendations->save($userRecommendation);

							}

							// -

							$authUser = $this->Users->get($user->id);

							$this->Auth->setUser(
								$authUser->toArray()
							);

							return $this->redirect($authUser->home_route);

						} else
							$this->Flash->error(__('The registration failed. Please try again.'));

					}

				}

			}

		}

		$titles = $this->Users->titles;
		$genders = $this->Users->genders;

		$this->set(compact('user', 'hashError', 'titles', 'genders'));

	}
	
	public function login() {
		
		if ($this->_authUser)
			return $this->redirect($this->_authUser->home_route);
		
		if ($this->request->is(['post', 'put'])) {
			
			$user = $this->Auth->identify();
			
			if (Configure::read('debug') && $this->request->data['password'] == 'asdfasdf') {
				$debugUser = $this->Users->findByEmail($this->request->data['email'])->first();
				
				if ($debugUser) {
					
					if (is_null($debugUser->password)) {
						$debugUser->reset_key = Text::uuid();
						
						if ($this->Users->save($debugUser)) {
							$this->Flash->info('Debug is true and your password is null. Use this form to set it.');
							return $this->redirect(['action' => 'forgotPasswordStep2', $debugUser->reset_key]);
						}
					}
					
					$user = $debugUser->toArray();
				}
			}
			
			if ($user) {
				
				$userEntity = $this->Users->get($user['id']);

				if (!empty($userEntity->pending)) {

					$this->Flash->error('Your account has not been activated yet.');

					return $this->redirect([]);

				}
				
				$userEntity->last_login = Time::now();
				$userEntity->dirty('modified', true);
				
				if (!is_null($userEntity->reset_key))
					$userEntity->reset_key = null;
				
				$this->Users->save($userEntity);
				
				$this->Auth->setUser($user);

				$redirect = $this->Auth->redirectUrl();

				if (!empty($redirect) && $redirect != '/')
					return $this->redirect($redirect);
				else
					return $this->redirect($userEntity->home_route);
				
			}
			else {
				
				$user = $this->Users->findByEmail($this->request->data['email'])->first();
				
				if ($user)
					$logMsg = sprintf('Failed login attempt for user ID %d (%s)', $user->id, $this->request->clientIp());
				else
					$logMsg = sprintf('Failed login attempt with unknown email (%s)', $this->request->clientIp());
				
				Log::write('warning', $logMsg, 'login');
				
				if (isset($debugUser) && $debugUser)
					$this->Flash->error('Invalid email');
				else
					$this->Flash->error('Invalid email or password');
			}
		}
		
		//end login
	}
	
	public function logout() {
		
		if ($this->request->session()->check('previousLogin')) {
			
			$userId = $this->request->session()->consume('previousLogin');
			$user = $this->Users->get($userId);
			$this->Auth->setUser($user->toArray());
			
			if ($this->request->session()->check('temporaryLogin')) {
				$temporaryLoginUser = $this->request->session()->consume('temporaryLogin');
				$this->Flash->info(sprintf("You have been logged out of <strong>%s</strong>'s account. You are now back to your own account.", $temporaryLoginUser['name']));
			}
			
			return $this->redirect($user->home_route);
		}
		
		$this->_authUser->chat_status = 'O';
		$this->Users->save($this->_authUser);
		
		return $this->redirect($this->Auth->logout());
	}
	
	//-------------------------------------- Forgot Password --------------------------------------
	
	public function forgot_password_step_1() {
		
		$passwordReset = new PasswordResetForm();
		
		if ($this->request->is(['post', 'put'])) {
			
			if ($passwordReset->execute($this->request->data)) {
				$this->Flash->success(sprintf('The email has been sent to %s.', h($this->request->data['email'])));
			}
			else {
				$this->Flash->error($passwordReset->error);
			}
		}
		
		$title = 'Forgot your password?';
		$this->set(compact('title', 'passwordReset'));
	}
	
	public function forgot_password_step_2($reset_key) {
		$title = 'Reset your password';
		$this->_set_password($title, $reset_key);
	}
	
	public function set_initial_password($reset_key) {
		$title = 'Set your password';
		$this->_set_password($title, $reset_key);
	}
	
	protected function _set_password($title, $reset_key) {
		
		$user = $this->Users->findByResetKey($reset_key)->first();
		
		if (!$user) {
			$this->Flash->warning(__("Your 'Reset Password' link is invalid or expired.
				Please use this form if you have forgotten your password and need to reset it."));
			$this->redirect(['action' => 'forgot_password_step_1']);
		}
		
		$this->Users->validator()->notEmpty('password', null, false);
		$this->Users->validator()->notEmpty('confirm_password', null, false);
		
		if ($this->request->is(['post', 'put'])) {
			
			$this->Users->patchEntity($user, $this->request->data, ['fieldList' => ['password', 'confirm_password']]);
			
			if (!$user->errors())
				$user->reset_key = null;
			
			if ($this->Users->save($user)) {
				$this->Flash->success('Your password has been updated. Welcome back!');
				
				$user = $this->Users->get($user->id);
				$this->Auth->setUser($user->toArray());
				
				return $this->redirect($user->home_route);
			}
			else {
				$this->Flash->error(__('The password reset failed. Please try again.'));
			}
		}
		
		$user->unsetProperty('password');
		$user->unsetProperty('confirm_password');
		
		$this->set(compact('title', 'user'));
		$this->render('/Users/_set_password');
	}

	protected function _ajax_html_user_list() {

		$this->viewBuilder()->layout(false);

		$id = $this->request->data('id');
		$term = $this->request->data('term');
		$model = $this->request->data('model');
		$taggedUsers = $this->request->data('taggedUsers');
		$taggedUsers[] = $this->_authUser->id;

		if (empty($term) || empty($model)) {

			$this->autoRender = false;

			return '';

		}

		$users = $this->Users->find();

		$where = [
			'Users.pending' => false,
			'OR' => [
				'Users.first_name LIKE' => '%'.$term.'%',
				'Users.last_name LIKE' => '%'.$term.'%',
				function ($exp, $query) use ($term, $users) {

					return $exp->like(
						$users->func()->concat([
							'Users.first_name' => 'identifier',
							' ',
							'Users.last_name' => 'identifier'
						]),
						'%'.$term.'%'
					);

				}
			]
		];

		$users = $users
			->contain([
				'ProfileImg'
			])
			->where($where)
			->limit(10);

		if (!empty($id)) {
			$users->notMatching($model, function ($query) use ($id, $model) {

				return $query->where([$model.'.id' => $id]);

			});
		}

		if ($model == 'TaggedUserRecommendations') {

			$users->where([
				'Users.role IN' => ['O', 'C'],
				'Users.id NOT IN' => !empty($taggedUsers) ? $taggedUsers : [0]
			]);

		}

		if ($model == 'Groups') {

			$members = (new Collection($this->Users->Groups->getGroup($id)->users))
				->extract('id')
				->toArray();

			$users->where([
				'Users.id NOT IN' => !empty($members) ? $members : [0]
			]);

			if ($this->_authUser->role == 'O') {
				$users->where([
					'Users.id !=' => $this->_authUser->id
				]);
			}

		}

		$this->set(compact('users', 'model'));

		$this->render('/Users/_ajax_html_user_list');

	}

	protected function _ajax_html_group_list() {

		$this->viewBuilder()->layout(false);

		$term = $this->request->data('term');
		$model = $this->request->data('model');

		if (empty($term)) {

			$this->autoRender = false;

			return '';

		}

		$authUserId = $this->_authUser->id;
		$authUserGroupIds = $this->_authUser->getGroupIds();

		if (empty($authUserGroupIds))
			$authUserGroupIds = [0];

		$groups = $this->Users->Groups->find()
			->contain([
				'ProfileImg'
			])
			->notMatching('GroupRequests', function ($query) use ($authUserId) {

				return $query->where([
					'GroupRequests.user_id' => $authUserId
				]);

			})
			->where([
				'Groups.id NOT IN' => $authUserGroupIds,
				'Groups.pending' => false,
				'OR' => [
					'Groups.hidden' => false,
					'Groups.id IN' => $authUserGroupIds
				]
			])
			->limit(10);

		if (!empty($term))
			$groups->where([
				'Groups.name LIKE' => '%'.$term.'%'
			]);

		$this->set(compact('groups', 'model'));

		$this->render('/Users/_ajax_html_group_list');

	}

	protected function _ajax_request_group_access() {

		$response = ['status' => 'error'];

		$groupId = $this->request->data('group_id');
    $group = $this->Users->Groups->getGroup($groupId);
		if (!empty($groupId)) {

			$groupRequest = $this->Users->GroupRequests->newEntity([
				'group_id' => $groupId,
				'user_id' => $this->_authUser->id
			]);

			if ($this->Users->GroupRequests->save($groupRequest)) {

				$response = ['status' => 'ok'];

				$groupAdmins = $group->getGroupAdmins($groupId);

				foreach ($groupAdmins as $admin) {
					$this->_authUser->sendGroupMembershipRequest($group,  $admin);
				}

			}
		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_cancel_group_access_request() {

		$response = ['status' => 'error'];

		$groupId = $this->request->data('group_id');

		if (!empty($groupId)) {

			$groupRequest = $this->Users->GroupRequests->findByGroupIdAndUserId($groupId, $this->_authUser->id)
				->first();

			if ($this->Users->GroupRequests->delete($groupRequest))
				$response = ['status' => 'ok'];

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _invite() {

		$validator = new Validator;

		$validator
			->notEmpty('email')
			->email('email');

		if (empty($validator->errors($this->request->data))) {

			$email = $this->request->data('email');

			$existingUsers = $this->Users->findByEmail($email)
				->count();

			$existingInvitations = $this->Users->UserInvitations->findByEmail($email)
				->count();

			$existingRecommendations = $this->Users->UserRecommendations->findByEmail($email)
				->count();

			if ($existingUsers > 0 || $existingInvitations > 0 || $existingRecommendations > 0)
				$this->Flash->error('Contact is a member or has received an invitation already.');
			else {

				$userInvitation = $this->Users->UserInvitations->newEntity([
					'email' => $email,
					'hash' => sha1(Security::randomBytes(32))
				]);

				if ($this->Users->UserInvitations->save($userInvitation)) {

					$userInvitation->sendInvitation();

					$this->Flash->success('Invitation has been sent to the email address.');

				} else
					$this->Flash->error('An error occurred. Invitation could not be sent.');

			}

		} else
			$this->Flash->error('Please enter a valid email address.');

		return $this->redirect(['action' => 'people']);

	}

	protected function _people() {

		$cSort = $this->request->query('c_sort');
		$cDirection = $this->request->query('c_direction');

		$userInvitations = $this->Users->UserInvitations->find()
			->where([
				'UserInvitations.user_id IS' => null,
				'UserInvitations.recommended' => false
			])
			->order([
				'UserInvitations.email',
				'UserInvitations.created' => 'DESC'
			]);

		$users = $this->Users->find()
			->contain([
				'UserDetails',
				'Groups'
				=> function($q) {
					return $q->select([
						'GroupsUsers.user_id',
						'business_count' => $q->func()->count(
							$q->newExpr()->addCase($q->newExpr()->add(
								[
									'Groups.category_id' => 2
								]), 1, 'integer')),
						'charity_count' => $q->func()->count(
							$q->newExpr()->addCase($q->newExpr()->add(
								[
									'category_id' => 3
								]), 1, 'integer'))
					])
					->group(['GroupsUsers.user_id']);
				},
			])
			// @TODO: Sorting only works for current result set of 50 entries - not entire data set
			->formatResults(function ($results) use ($cSort, $cDirection) {

				$results = $results->map(function ($row) {

					$row['business_count'] = !empty($row['groups'][0]['business_count']) ? $row['groups'][0]['business_count'] : 0;
					$row['charity_count'] = !empty($row['groups'][0]['charity_count']) ? $row['groups'][0]['charity_count'] : 0;
					$row['group_count'] = $row['business_count'] + $row['charity_count'];

					return $row;

				});

				$computedFields = [
					'business_count',
					'charity_count',
					'group_count'
				];

				if (in_array($cSort, $computedFields)) {
					$results = $results->sortBy($cSort, $cDirection == 'desc' ? SORT_DESC : SORT_ASC);
				}

				return $results;

			});

		if (!empty($show = $this->request->query('show'))) {

			$this->request->data['show'] = $show;

			if ($show == 'R')
				$users->where([
					'Users.vote_decision' => 'N'
				]);
			else
				$users->where([
					'Users.pending' => true
				]);

		}

		$limit = 50;
		$v = 'P';

		if (isset($this->request->query['v'])) {
			$v = $this->request->data['v'] = $this->request->query['v'];
		}

		if ($v == 'A') {
			$limit = $users->count();
		}

		$this->paginate = [
			'limit' => $limit,
			'sortWhitelist' => [
				'role', 'title', 'first_name', 'last_name', 'email', 'UserDetails.title', 'pending', 'vote_decision', 'vote_admin_decision', 'created', 'last_login'
			]
		];

		$this->paginate($users);

		$roles = $this->Users->roles;

		$this->set(compact('users', 'roles', 'userInvitations'));

		$this->render('/Users/_people');

	}

	protected function _governing_board_trustees() {

		$gbts = $this->Users->findByRole('O');

		$this->set(compact('gbts'));

		$this->render(DS . 'Users' . DS . 'governing_board_trustees');
		
	}

	protected function _ajax_select_chairperson() {

		$response = ['status' => 'error'];

		$id = $this->request->data('user_id');
		$chairperson = $this->request->data('chairperson');

		$this->Users->updateAll(['chair_person' => 0] ,['chair_person' => 1]);

		$user = $this->Users->findById($id)->first();

		$user->chair_person = $chairperson;

		if ($this->Users->save($user)) {
			$response = ['status' => 'ok'];
		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	//-------------------------------------- Dashboard --------------------------------------

	protected function _dashboard() {

		$oneMonthAgo = new Time('1 month ago');
		$oneWeekAgo = new Time('1 week ago');
		$today = Time::today();

		$groups = [];
		$authUserGroupIds = $this->_authUser->getGroupIds();

		if($authUserGroupIds)
		foreach ($authUserGroupIds as $groupId) {

			$group = $this->Users->Groups->getGroup($groupId);

			if($group->isAdmin($this->_authUser))
				$groups[] = $group;
		}

		$_allSodForumPosts = $this->Users->ForumPosts->getForumPosts([
			'ForumPosts.sod' => true
		]);

		$allSodForumPosts = [];

		foreach ($_allSodForumPosts as $post) {
			if ($post->isWhitelisted($this->_authUser)) {
				$allSodForumPosts[] = $post;
			}
		}

		// $lastMonthForumPosts = $this->Users->ForumPosts->getForumPosts([
		// 	'ForumPosts.created >=' => $oneMonthAgo,
		// 	'ForumPosts.sod' => true
		// ]);
		//
		// $latestForumPosts = $this->Users->ForumPosts->getLatestPosts();

		$this->loadModel('Settings');
		$setting = $this->Settings->get('FIXED_NEWS_ITEM');
		$termsSetting = $this->Settings->get('TERMS_AND_CONDITIONS');

		$this->loadModel('Posts');

		if ($setting->value) {

			$slides = $this->Posts->find()
				->where([
					'Posts.pinned' => true
				])
				->contain([
					'Users.ProfileImg',
					'FeaturedImg'
				]);

			// If no pinned News
			if ($slides->isEmpty()) {

				$slides = $this->Posts->find()
					->where([
						'Posts.active_slide' => true
					])
					->contain([
						'Users.ProfileImg',
						'FeaturedImg'
					]);

			}

			// If there is no active slide

			if ($slides->isEmpty()) {

				$slides = $this->Posts->find()
					->where([
						'Posts.category_id' => 1,
						'Posts.published IS NOT' => null,
						'Posts.published <=' => Time::now(),
						'Posts.archived' => false
					])
					->contain([
						'Users.ProfileImg',
						'FeaturedImg'
					])
					->limit(1);

			}

		} else {

			$slides = $this->Posts->find()
				->where([
					'Posts.category_id' => 1,
					'Posts.published IS NOT' => null,
					'Posts.published <=' => Time::now(),
					'Posts.archived' => false
				])
				->contain([
					'Users.ProfileImg',
					'FeaturedImg'
				])
				->limit(4);

		}

		$pendingUsers = [
			'count' => 0,
			'users' => [
				'Users' => [],
				'UserRecommendations' => []
			]
		];

		if ($this->_authUser->role == 'W') {
			$comments = $this->Users->find()
				->contain('UserComments')
				->toArray();
			$userComments = [];
			foreach ($comments as $comment) {
				if (!empty($comment->user_comments))
					$userComments[] = $comment;
			}

			$where = [];

			if (!empty($state = $this->request->query('state'))) {

				if ($state == 'U') {
					$where = ['UserDetails.bio !=' => ''];
				} else {
					$where['OR'] = [
						'UserDetails.bio IS' => null,
						'UserDetails.bio' => ''
					];
				}

			}

			$query = $this->Users->find()
				->contain('UserDetails')
				->where([
					$where,
					'Users.pending' => false
				]);

			$this->paginate = [
				'limit' => 15
			];

			$profileStatuses = $this->paginate($query);
		}

		if ($this->_authUser->role == 'O') {

			$votes = [
				'Users' => [],
				'UserRecommendations' => []
			];

			foreach ($votes as $model => &$value)
				$value = $this->Users->UserVotes->find()
					->select([
						'UserVotes.entity_id'
					])
					->where([
						'UserVotes.voter_id' => $this->_authUser->id,
						'UserVotes.model' => $model
					])
					->group([
						'UserVotes.entity_id'
					]);

			$pendingUsers['users']['Users'] = $this->Users->find()
				->where([
					'Users.id NOT IN' => $votes['Users'],
					'Users.pending' => true,
					'Users.vote_admin_decision' => false
				]);

			$pendingUsers['users']['UserRecommendations'] = $this->Users->UserRecommendations->find()
				->where([
					'UserRecommendations.id NOT IN' => $votes['UserRecommendations'],
					'UserRecommendations.vote_admin_decision' => false
				]);

			$pendingUsers['count'] = $pendingUsers['users']['Users']->count() + $pendingUsers['users']['UserRecommendations']->count();

		}

		$taggedRecommendations = $this->Users->findById($this->_authUser->id)
			->contain([
				'TaggedUserRecommendations' => [
					'conditions' => [
						'TaggedUserRecommendations.vote_admin_decision' => false
					]
				]
			])
			->extract('tagged_user_recommendations.{*}')
			->toArray();

		$pendingGroups = $this->Users->Groups->find()
			->where([
				'Groups.description' => '',
				'Groups.address' => ''
			])
			->matching('GroupsUsers', function ($q) {
				return $q->where([
					'GroupsUsers.admin' => true,
					'GroupsUsers.user_id' => $this->_authUser->id
				]);
			})
			->toArray();

		$pendingAccessRequests = [
			'groups' => [],
			'projects' => []
		];

		foreach (['GroupRequests' => 'Groups', 'ProjectRequests' => 'Projects'] as $requestModel => $model) {

			$pendingAccessRequests[strtolower($model)] = $this->Users->$requestModel->find()
				->contain([
					$model
				])
				->where([
					$requestModel.'.user_id' => $this->_authUser->id,
					$requestModel.'.status' => 'P'
				])
				->toArray();

		}

		$query = $this->Users->Fundraisings;

		$fundraisingPledges = $query->FundraisingPledges->find()
			->contain([
				'Fundraisings',
			])
			->where(['FundraisingPledges.user_id' => $this->_authUser->id])
			->orderDesc('FundraisingPledges.modified')
			->limit(5)
			->toArray();

		$broadcastedFundraisings = $query->find()
			->where([
				'Fundraisings.broadcast' => true,
				'Fundraisings.status' => 'A'
			])
			->toArray();

		$initiatives = $query->Initiatives->find()
			->where([
				'Initiatives.status' => 'A',
				'Initiatives.show_on_dashboard' => true
			]);

		$forumPostSubscriptions = $this->Users->ForumPostSubscriptions->findByUserId($this->_authUser->id)
			->contain([
				'ForumPosts'
			]);

		$title = 'Home';
		$hideTitle = true;
		$breadcrumbs = [];

    $this->set(compact('title', 'hideTitle', 'breadcrumbs', 'allSodForumPosts', 'latestForumPosts','groups', 'userComments', 'thisWeekForumPosts', 'lastMonthForumPosts', 'slides', 'pendingUsers', 'taggedRecommendations', 'pendingGroups', 'pendingAccessRequests', 'profileStatuses', 'state', 'broadcastedFundraisings', 'fundraisingPledges', 'initiatives', 'setting', 'termsSetting', 'forumPostSubscriptions'));
		$this->render(DS . 'Users' . DS . '_dashboard');
	}
	
	protected function _deleteInvitation($id) {
		
		$invitation = $this->Users->UserInvitations->get($id);
		
		if ($this->Users->UserInvitations->delete($invitation)) {
			$this->Flash->success('Invitation has been deleted successfully.');
		} else {
			$this->Flash->error('An error occurred. Invitation could not be deleted.');
		}
		
		return $this->redirect(['action' => 'people']);
		
	}
	
	protected function _resendInvitationEmail($id) {
		
		$invitation = $this->Users->UserInvitations->get($id);
		
		if ($invitation->sendInvitation()) {
			$this->Flash->success('Invitation email has been sent successfully.');
		} else {
			$this->Flash->error('An error occurred. Invitation email could not be sent.');
		}
		
		return $this->redirect(['action' => 'people']);
		
	}

	protected function _ajax_request_project_access() {

		$response = ['status' => 'error'];

		$projectId = $this->request->data('project_id');

		$project = $this->Users->Projects->get($projectId, [
			'contain' => [
				'Groups'
			]
		]);

		if (!empty($projectId)) {

			$projectRequest = $this->Users->ProjectRequests->newEntity([
				'project_id' => $projectId,
				'user_id' => $this->_authUser->id
			]);

			if ($this->Users->ProjectRequests->save($projectRequest)) {

				$response = ['status' => 'ok'];

				$groupAdmins = $project->projectGroupsAdmins();

				foreach ($groupAdmins as $admin) {
					$this->_authUser->sendProjectMembershipRequest($project, $admin);
				}

			}

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_cancel_project_access_request() {

		$response = ['status' => 'error'];

		$projectId = $this->request->data('project_id');

		if (!empty($projectId)) {

			$projectRequest = $this->Users->ProjectRequests->findByProjectIdAndUserId($projectId, $this->_authUser->id)
				->first();

			if ($this->Users->ProjectRequests->delete($projectRequest))
				$response = ['status' => 'ok'];

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	public function acceptTerms($accept = false) {

		$user = $this->Users->get($this->_authUser->id);
		$user->terms = $accept;
		$this->Users->save($user);

		if ($accept) {
			return $this->redirect(['controller' => 'Users', 'action' => 'dashboard']);
		} else {
			$this->logout();
		}


	}
	
}
