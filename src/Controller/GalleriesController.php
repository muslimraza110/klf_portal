<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;

class GalleriesController extends AppController {

	protected function _index() {

		$bannerActions = [];

		$this->loadModel('Folders');

		$folders = $this->Folders->findByParentId(Configure::read('App.assetGalleryId'))
				->contain([
					'Files' => function (\Cake\ORM\Query $query) {

						return $query
								->contain('resource')
								->where([
									'resource.name' => 'cover.jpg',
									'resource.type' => 'image/jpeg'
								]);

					}
				]);

		$title = 'Galleries';
		$titleIcon = '<i class="fa fa-picture-o"></i>';
		$breadcrumbs = [];

		$this->set(compact('bannerActions', 'title', 'titleIcon', 'breadcrumbs', 'folders'));

		$this->render('/Galleries/index');

	}

	protected function _view($folderId) {

		$bannerActions = [];

		$this->loadModel('Folders');

		$folder = $this->Folders->get($folderId, [
			'contain' => [
					'Files' => function (\Cake\ORM\Query $query) {

						return $query
								->contain('resource')
								->where([
									'resource.type IN' => [
										'image/gif',
										'image/jpeg',
										'image/png',
										'video/mp4'
									]
								]);

					}
			]
		]);

		if ($folder->parent_id != Configure::read('App.assetGalleryId'))
			throw new ForbiddenException;

		$title = $folder->name;
		$titleIcon = '<i class="fa fa-picture-o"></i>';
		$breadcrumbs = [
			['name' => 'Galleries', 'route' => ['action' => 'index']],
			['name' => $title, 'active' => true]
		];

		$this->set(compact('bannerActions', 'title', 'titleIcon', 'breadcrumbs', 'folder'));

		$this->render('/Galleries/view');
	}

}