<?php
namespace App\Controller;

use App\Model\Entity\Initiative;
use Cake\Collection\Collection;
use Cake\I18n\Time;
use Cake\Network\Exception\ForbiddenException;
use Cake\I18n\Number;
use Cake\Routing\Router;
use Cake\View\View;

class InitiativesController extends AppController {

	protected function _index() {

		if (!empty($status = $this->request->query('status'))) {
			$this->request->data['status'] = $status;
		} else {
			$status = 'A';
		}

		$this->paginate = [
			'contain' => [
				'Users',
				'Projects',
				'Fundraisings' => [
					'FundraisingPledges'
				]
			],
			'conditions' => [
				'Initiatives.status' => $status
			],
			'limit' => 30,
			'sortWhitelist' => [
				'name', 'Users.first_name', 'created'
			]
		];

		$initiatives = (new Collection($this->paginate()));

		$this->set(compact('initiatives'));
		$this->render('/Initiatives/_index');

	}

	protected function _add() {

		$initiative = $this->Initiatives->newEntity();

		$this->request->session()->write('Initiative', $initiative);

		$breadcrumbs = [
			['name' => 'Initiatives', 'route' => ['action' => 'index']],
			['name' => 'Initiatives', 'active' => true]
		];

		$this->set(compact('breadcrumbs'));
		$this->_stepInitiative();

	}

	protected function _edit($id) {

		$initiative = $this->Initiatives->get($id, [
			'contain' => [
				'Users',
				'Projects',
				'Fundraisings' => [
					'FundraisingUsers',
					'FundraisingPledges',
					'Projects'
				],
				'Groups'
			]
		]);

		$breadcrumbs = [
			['name' => 'Initiatives', 'route' => ['action' => 'index']],
			['name' => $initiative->name, 'route' => ['action' => 'view', $initiative->id]],
			['name' => 'Initiatives', 'active' => true]
		];

		$this->request->session()->write('Initiative', $initiative);

		$this->set(compact('breadcrumbs'));
		$this->_stepInitiative();

	}

	protected function _delete($id) {

		$initiative = $this->Initiatives->get($id);

		if (!in_array($this->_authUser->role, ['A', 'O'])) {
			throw new NotFoundException;
		}

		$initiative->status = 'D';

		if ($this->Initiatives->save($initiative)) {
			$this->Flash->success('Initiative has been deleted');
		} else {
			$this->Flash->error('An error occured while deleting the initiative');
		}

		$this->redirect(['action' => 'index']);

	}

	protected function _stepInitiative() {

		$session = $this->request->session();
		$sessionInitiative = $session->read('Initiative');

		if (!$sessionInitiative) {
			throw new ForbiddenException('The Initiative information cannot be loaded.');
		}

		if ($this->request->is(['post', 'put'])) {

			$step = $this->request->data('submit');

			if ($step == 'next') {

				// Create temporary template entity to run validation
				$initiative = $this->Initiatives->patchEntity($sessionInitiative, $this->request->data);

				if (!$initiative->errors()) {

					$session->write('Initiative', $initiative);
					return $this->redirect(['action' => 'stepProjects']);

				}

			}

		}

		$this->set(compact('sessionInitiative'));
		$this->render('/Initiatives/step_initiative');

	}

	protected function _stepProjects() {

		$session = $this->request->session();
		$sessionInitiative = $session->read('Initiative');

		if (!$sessionInitiative) {
			throw new ForbiddenException('The Initiative information cannot be loaded.');
		}

		if ($this->request->is(['post', 'put'])) {

			$step = $this->request->data('submit');

			if ($step == 'previous') {

				return $this->redirect(['action' => 'stepInitiative']);

			} elseif ($step == 'next') {

				if (!isset($this->request->data['projects'])) {
					$this->Flash->error('At least 1 Project must be added.');
					return $this->redirect(['action' => 'stepProjects']);
				}

				// Create temporary template entity to run validation
				$initiative = $this->Initiatives->patchEntity($sessionInitiative, ['projects' => $this->request->data['projects']]);

				$percentage = 0;
				foreach($initiative->projects as $project) {
					$percentage += $project->_joinData->percentage;
				}

				if ($percentage != 100) {

					$this->Flash->error('All Projects must equal 100%');
					return $this->redirect(['action' => 'stepProjects']);

				}

				if (!$initiative->errors()) {

					$session->write('Initiative', $initiative);
					return $this->redirect(['action' => 'stepDonations']);

				}

			}

		}

		$projects = $this->Initiatives->Projects->find('list')
			->where([
				'approval_stamp' => 1,
				'status' => 'A',
			]);

		$projectsArray = $this->Initiatives->Projects->find()
			->contain([
				'Groups'
			])
			->where([
				'Projects.id IN' => array_keys($projects->toArray())
			])
			->indexBy('id')
			->toArray();

//		$groups = $this->Initiatives->Projects->Groups->find()
//			->contain([
//				'Projects' => [
//					'conditions' => [
//						'Projects.id IN' => array_keys($projects->toArray())
//					]
//				]
//			])
//			->matching('Projects', function ($q) use ($projects) {
//				return $q
//					->where([
//						'Projects.id IN' => array_keys($projects->toArray())
//					]);
//			})
//			->group('Groups.id');
		//	->indexBy('projects.0.id');

		//$sessionInitiative = $this->Initiatives->patchEntity($sessionInitiative, ['Projects' => $this->Initiatives->Projects->find()->toArray()] ,['Associated' => ['Projects']]);

		$this->set(compact('sessionInitiative', 'projects', 'projectsArray'));
		$this->render('/Initiatives/step_projects');

	}

	protected function _stepDonations() {

		$session = $this->request->session();
		$sessionInitiative = $session->read('Initiative');

		if (!$sessionInitiative) {
			throw new ForbiddenException('The Initiative information cannot be loaded.');
		}

		if ($this->request->is(['post', 'put'])) {

			$step = $this->request->data('submit');

			if ($step == 'previous') {

				return $this->redirect(['action' => 'stepProjects']);

			} elseif ($step == 'next') {

				if (!isset($this->request->data['groups'])) {
					$this->Flash->error('At least 1 Charity must be added.');
					return $this->redirect(['action' => 'stepDonations']);
				}

				// Create temporary template entity to run validation
				$initiative = $this->Initiatives->patchEntity($sessionInitiative, ['groups' => $this->request->data['groups']]);

				if (!$initiative->errors()) {

					$session->write('Initiative', $initiative);
					return $this->redirect(['action' => 'stepFundraisings']);

				}

			}

		}

		$groups = $this->Initiatives->Groups->find('list')
			->where([
				'Groups.donations' => true,
				'Groups.status' => 'A',
				'Groups.category_id' => 3 // 3 = Charities
			]);

		$groupsArray = $this->Initiatives->Groups->find()
			->where([
				'Groups.id IN' => array_keys($groups->toArray())
			])
			->indexBy('id')
			->toArray();

		$this->set(compact('sessionInitiative', 'groups', 'groupsArray'));
		$this->render('/Initiatives/step_donations');

	}

	protected function _stepFundraisings() {

		$session = $this->request->session();
		$sessionInitiative = $session->read('Initiative');

		if (!$sessionInitiative) {
			throw new ForbiddenException('The Initiative information cannot be loaded.');
		}

		if ($this->request->is(['post', 'put'])) {

			$step = $this->request->data('submit');

			if ($step == 'previous') {

				return $this->redirect(['action' => 'stepDonations']);

			} elseif ($step == 'next') {

				if (!isset($this->request->data['fundraisings'])) {
					$this->Flash->error('At least 1 Fundraising must be added.');
					return $this->redirect(['action' => 'stepFundraisings']);
				}

				$funds = [];
				foreach ($this->request->data['fundraisings'] as $key => $fundraising) {

					$currentFund = $this->Initiatives->Fundraisings->get($fundraising['id'], [
						'contain' => [
							'FundraisingUsers'
						]
					]);

					$projects = [];

					foreach ($fundraising['projects'] as $key => $project) {

						if ($project[key($project)] == 1) {
							$projects[] = $this->Initiatives->Projects->get(key($project));
						}

					}

					$currentFund->projects = $projects;
					$funds[] = $currentFund;

				}

				$sessionInitiative->fundraisings = $funds;

				$session->write('Initiative', $sessionInitiative);
				return $this->redirect(['action' => 'stepSummary']);

			}
		}

		$fundraisingList = $this->Initiatives->Fundraisings->find('list', [
			'valueField' => function ($q) {
				return sprintf('%s - %s - $%s', $q->name, $q->typeLabel(), number_format($q->amount));
			}
		])
			->where([
				'OR' => [
					'initiative_id IS' => null,
					'initiative_id' => $sessionInitiative->id
				]
			]);

		$this->set(compact('sessionInitiative', 'fundraisingList'));
		$this->render('/Initiatives/step_fundraisings');

	}

	protected function _stepSummary() {

		$session = $this->request->session();
		$sessionInitiative = $session->read('Initiative');

		if (!$sessionInitiative) {
			throw new ForbiddenException('The Initiative information cannot be loaded.');
		}

		if ($this->request->is(['post', 'put'])) {

			$step = $this->request->data('submit');

			if ($step == 'previous') {

				return $this->redirect(['action' => 'stepFundraisings']);

			} elseif ($step == 'submit') {

				$sessionInitiative->user_id = $this->Auth->user('id');
				$sessionInitiative->status = 'A';
				$initiative = $this->Initiatives->save($sessionInitiative);

				$selectedFundraisingIds = [];

				foreach ($sessionInitiative->fundraisings as $fundraising) {

					$fundraising->initiative_id = $initiative->id;
					$this->Initiatives->Fundraisings->save($fundraising);
					$selectedFundraisingIds[] = $fundraising->id;

				}

				$removeFundraisings = $this->Initiatives->Fundraisings->find()
					->where([
						'Fundraisings.initiative_id' => $initiative->id,
						'Fundraisings.id NOT IN' => $selectedFundraisingIds
					]);

				if (!$removeFundraisings->isEmpty()) {
					foreach ($removeFundraisings as $removeFundraising) {
						$removeFundraising->initiative_id = null;
						$this->Initiatives->Fundraisings->save($removeFundraising);
					}
				}

				if (!empty($this->request->data['eblast'])) {

					$initiative = $this->Initiatives->get($initiative->id, [
						'contain' => [
							'Fundraisings.FundraisingPledges.Users',
							'Projects' => [
								'Groups.Users',
								'Users'
							]
						]
					]);

					$content = (new View)
						->element('Eblasts/initiative_notification', [
							'initiative' => $initiative
						]);

					$session = [
						'subject' => 'Initiative Notification',
						'content' => $content,
						'button_label' => 'Link to the Initiative',
						'button_link' => Router::url(['action' => 'view', $initiative->id, 'prefix' => '_', '_full' => true]),
						'return_url' => Router::url(['action' => 'view', $initiative->id, '_full' => true]),
						'extra_recipients' => $this->_extractRecipientGroups($initiative)
					];

					$this->Session->write('Eblast', $session);

					return $this->redirect(['controller' => 'Eblasts', 'action' => 'form']);

				}

				return $this->redirect(['action' => 'view', $initiative->id]);

			}

		}

/*		$totalAmount = 0;

		foreach ($sessionInitiative->fundraisings as $fundraising) {
			$totalAmount += $fundraising->amount;
		}

		$totalAmount = Number::format($totalAmount, [
			'before' => 'USD $'
		]);*/

		$this->set(compact('sessionInitiative'));
		$this->render('/Initiatives/step_summary');

	}

	protected function _emailPreview() {

		$this->viewBuilder()->layout(false);

		$session = $this->request->session();
		$initiative = $session->read('Initiative');

		$this->set(compact('initiative', 'content'));
		$this->render('/Email/html/initiative_notification');

	}

	protected function _view($id) {

		$initiative = $this->Initiatives->get($id, [
			'contain' => [
				'Projects' => [
					'Groups'
				],
				'Fundraisings' => [
					'Projects',
					'FundraisingPledges' => [
						'Users',
						'Groups'
					],
					'FundraisingUsers' => [
						'Users'
					]
				],
				'Groups'
			]
		]);

		$groups = (new Collection($initiative->groups))
			->indexBy('id')
			->extract('name')
			->toArray();

		$breadcrumbs = [
			['name' => 'Initiatives', 'route' => ['action' => 'index']],
			['name' => 'Initiatives', 'active' => true]
		];

		$this->set(compact('initiative', 'breadcrumbs', 'groups'));
		$this->render('/Initiatives/_view');

	}

	private function _extractRecipientGroups(Initiative $initiative) {

		$return = [];

		if (!empty($initiative->fundraisings)) {

			foreach ($initiative->fundraisings as $fundraising) {

				if (!empty($fundraising->fundraising_pledges)) {

					foreach ($fundraising->fundraising_pledges as $pledge) {
						$return['Pledgers'][$pledge->user_id] = $pledge->user->name;
					}

				}

			}

		}

		if (!empty($initiative->projects)) {

			foreach ($initiative->projects as $project) {

				if (!empty($project->groups)) {

					foreach ($project->groups as $group) {

						foreach ($group->users as $user) {
							$return['Charity Contacts'][$user->id] = $user->name;
						}

					}

				}

				foreach ($project->users as $user) {
					$return['Project Contacts'][$user->id] = $user->name;
				}

			}

		}

		if (!empty($return['Pledgers'])) {
			asort($return['Pledgers']);
		}

		if (!empty($return['Charity Contacts'])) {
			asort($return['Charity Contacts']);
		}

		if (!empty($return['Project Contacts'])) {
			asort($return['Project Contacts']);
		}

		return $return;

	}

	protected function _viewEblast($id) {

		$initiative = $this->Initiatives->get($id, [
			'contain' => [
				'Fundraisings.FundraisingPledges.Users',
				'Projects' => [
					'Groups.Users',
					'Users'
				]
			]
		]);

		$content = (new View)
			->element('Eblasts/initiative_notification', [
				'initiative' => $initiative
			]);

		$attachments = [];

		if (isset($this->request->query['update']) && $this->request->query['update']) {
			$attachments = [
				$initiative->id . '-initiative-pledgers.xlsx' => $initiative->generateExcelPledgesReport()
			];
		}

		$session = [
			'origin' => [
				'name' => 'initiative',
				'id' => $initiative->id
			],
			'subject' => 'Initiative Notification',
			'content' => $content,
			'button_label' => 'Link to the Initiative',
			'button_link' => Router::url(['action' => 'view', $initiative->id, 'prefix' => '_', '_full' => true]),
			'return_url' => Router::url(['action' => 'view', $initiative->id, '_full' => true]),
			'extra_recipients' => $this->_extractRecipientGroups($initiative),
			'attachments' => $attachments
		];

		$this->Session->write('Eblast', $session);

		return $this->redirect(['controller' => 'Eblasts', 'action' => 'form']);

	}

	protected function _archive($id) {

		$this->request->allowMethod('post');

		if (!in_array($this->_authUser->role, ['A', 'O'])) {
			throw new NotFoundException;
		}

		$initiative = $this->Initiatives->get($id);

		$initiative->status = 'R';

		if ($this->Initiatives->save($initiative)) {
			$this->Flash->success('Initiative has been archived.');
		} else {
			$this->Flash->error('Initiative could not be archived.');
		}

		return $this->redirect(['action' => 'index', '?' => ['status' => 'R']]);

	}

	protected function _reactivate($id) {

		$this->request->allowMethod('post');

		if (!in_array($this->_authUser->role, ['A', 'O'])) {
			throw new NotFoundException;
		}

		$initiative = $this->Initiatives->get($id);

		$initiative->status = 'A';

		if ($this->Initiatives->save($initiative)) {
			$this->Flash->success('Initiative has been marked as active.');
		} else {
			$this->Flash->error('Initiative could not be marked as active please try again.');
		}

		return $this->redirect(['action' => 'index', '?' => ['status' => 'A']]);

	}

	protected function _pledges($fundraisingId) {

		$fundraisingPledges = $this->Initiatives->Fundraisings->FundraisingPledges->find()
			->where([
				'FundraisingPledges.fundraising_id' => $fundraisingId,
				'FundraisingPledges.amount !=' => 0
			])
			->contain([
				'Fundraisings',
				'Users',
				'Groups'
			]);

		$fundraising = $this->Initiatives->Fundraisings->get($fundraisingId, [
			'contain' => 'Initiatives'
		]);

		$title = 'View Pledges For: ' . $fundraising->initiative->name;
		$breadcrumbs = [
			['name' => 'Initiatives', 'route' => ['action' => 'index', 'controller' => 'Initiatives']],
			['name' => $fundraising->initiative->name, 'route' => ['action' => 'view', 'controller' => 'Initiatives', $fundraising->initiative_id]],
			['name' => $title, 'active' => true]
		];

		$this->set(compact('fundraisingPledges', 'title', 'breadcrumbs'));
		$this->render('/Initiatives/pledges');

	}

	protected function _editPledge($fundraisingPledgeId) {

		$fundraisingPledge = $this->Initiatives->Fundraisings->FundraisingPledges->find()
			->where([
				'FundraisingPledges.id' => $fundraisingPledgeId,
			])
			->contain([
				'Fundraisings' => [
					'Initiatives' => [
						'Groups'
					]
				],
				'Users',
				'Groups'
			])
			->first();

		$groups = (new Collection($fundraisingPledge->fundraising->initiative->groups))
			->indexBy('id')
			->extract('name')
			->toArray();

		if ($this->request->is(['post', 'put'])) {

			if ($fundraisingPledge->fundraising->type == 'B') {

				$this->request->data['amount'] = round($fundraisingPledge->fundraising->amount / $fundraisingPledge->fundraising->blocks, 2) * $this->request->data['block_amount'];

			}

			$fundraisingPledge = $this->Initiatives->Fundraisings->FundraisingPledges->patchEntity($fundraisingPledge, $this->request->data);

			if ($this->Initiatives->Fundraisings->FundraisingPledges->save($fundraisingPledge)) {

				$this->Flash->success('Pledge Saved Successfully.');
				return $this->redirect(['action' => 'pledges', 'controller' => 'Initiatives', $fundraisingPledge->fundraising_id]);

			} else {

				$this->Flash->error('An error has occurred please try again.');

			}

		}

		$title = 'Edit Pledge From: ' . $fundraisingPledge->user->name;
		$breadcrumbs = [
			['name' => 'Initiatives', 'route' => ['action' => 'index', 'controller' => 'Initiatives']],
			['name' => $fundraisingPledge->fundraising->initiative->name, 'route' => ['action' => 'view', 'controller' => 'Initiatives', $fundraisingPledge->fundraising->initiative_id]],
			['name' => 'View Pledges For: ' . $fundraisingPledge->fundraising->initiative->name, 'route' => ['action' => 'pledges', 'controller' => 'Initiatives', $fundraisingPledge->fundraising_id]],
			['name' => $title, 'active' => true]
		];

		$this->set(compact('fundraisingPledge', 'title', 'breadcrumbs', 'groups'));
		$this->render('/Initiatives/edit_pledge');

	}

	protected function _deletePledge($fundraisingPledgeId) {

		$this->autoRender = false;

		$fundraisingPledge = $this->Initiatives->Fundraisings->FundraisingPledges->find()
			->contain([
				'Fundraisings'
			])
			->where([
				'FundraisingPledges.id' => $fundraisingPledgeId,
			])
			->first();

		if ($this->_authUser->role != 'A') {
			return $this->redirect(['action' => 'index', 'controller' => 'Initiatives']);
		}

		$this->Initiatives->Fundraisings->FundraisingPledges->delete($fundraisingPledge);

		$this->Flash->success('Pledge has been successfully deleted');
		return $this->redirect(['action' => 'pledges', 'controller' => 'Initiatives', $fundraisingPledge->fundraising->initiative_id]);

	}

	protected function _communication($id) {

		$initiativeEmails = $this->Initiatives->InitiativeEblasts->find();

		$initiativeEmails
			->select([
				'created_date' => $initiativeEmails->func()->date_format([
					'InitiativeEblasts.created' => 'identifier',
					"'%m%d%y%h%i'" => 'literal'
				])
			])
			->contain([
				'Initiatives' => [
					'InitiativePins'
				]
			])
			->autoFields(true)
			->where([
				'InitiativeEblasts.initiative_id' => $id
			])
			->matching('Initiatives.InitiativePins', function ($q) {
				return $q
					->where([
						'DATE_FORMAT(InitiativePins.date_group, "%m%d%y%h%i") = DATE_FORMAT(InitiativeEblasts.created, "%m%d%y%h%i")'
					]);
			})
			->group('created_date')
			->order('InitiativeEblasts.created asc');

		$initiative = $this->Initiatives->get($id);

		$title = 'View Eblasts for: ' . $initiative->name;

		$breadcrumbs = [
			['name' => $initiative->name, 'route' => ['action' => 'view', $id]],
			['name' => $title, 'active' => true]
		];

		$this->set(compact('initiativeEmails', 'title', 'breadcrumbs'));
		$this->render('/Initiatives/communication');

	}

	protected function _pinEmail($id) {

		$this->autoRender = false;

		$pin = $this->request->query['pin'];
		$date = $this->request->query['date_group'];

		if ($this->Initiatives->updatePinnedEmail($id, $pin, $date)) {
			$this->Flash->success('Pinned Email Updated successfully.');
			return $this->redirect(['action' => 'communication', $id]);
		};

	}

}