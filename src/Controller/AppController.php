<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Database\Type;
use Cake\I18n\Time;
use Cake\Network\Exception\BadRequestException;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Utility\Inflector;

class AppController extends \Cake\Controller\Controller {

	public $Session = null;
	protected $_appConfig = [];
	protected $_authUser;
	protected $_nav;
	protected $_icons = [];

	public $helpers = [
		'Html' => [
			'className' => 'Bootstrap.BootstrapHtml'
		],
		'Form' => [
			'className' => 'BootstrapNgForm', //Extend Bootstrap Form Helper but handles Angular Tags
			'useCustomFileInput' => true
		],
		'Paginator' => [
			'className' => 'Bootstrap.BootstrapPaginator'
		],
		'Modal' => [
			'className' => 'Bootstrap.BootstrapModal'
		],
		'AssetManager.Asset'
	];

	public function initialize() {

		$this->loadComponent('Flash');
		$this->loadComponent('Paginator');
		$this->loadComponent('RequestHandler');

		$this->loadComponent('Auth', [
			'authorize' => 'Controller',
			'authenticate' => [
				'Form' => [
					'fields' => ['username' => 'email']
				]
			],
			'loginAction' => ['controller' => 'Users', 'action' => 'login', 'prefix' => false],
		]);

		$this->loadComponent('Utilities', [
			'iconSets' => ['basic']
		]);

		$this->loadComponent('Search');

		$this->loadComponent('Security', [
			'blackHoleCallback' => 'forceSSL',
			'validatePost' => false
		]);

	}

	public function isAuthorized($user = null) {

		// Any registered user can access public functions
		if (empty($this->request->params['prefix']))
			return true;

		if (empty($user))
			return false;

		$prefix = $this->request->params['prefix'];

		if (array_key_exists($prefix, PREFIXES))
			return $user['role'] === PREFIXES[$prefix]['role'];

		// Default deny
		return false;
	}

	public function beforeFilter(\Cake\Event\Event $event) {
		parent::beforeFilter($event);

		Time::$defaultLocale = 'en-CA';

		\Cake\I18n\Time::setToStringFormat('M/d/yy, h:mm a'); // For any mutable DateTime
		\Cake\I18n\FrozenTime::setToStringFormat('M/d/yy, h:mm a'); // For any immutable DateTime
		\Cake\I18n\Date::setToStringFormat('M/d/yy'); // For any mutable Date
		\Cake\I18n\FrozenDate::setToStringFormat('M/d/yy'); // For any immutable Date

		Type::build('time')->useImmutable()->useLocaleParser()->setLocaleFormat('h:mm a');
		Type::build('date')->useImmutable()->useLocaleParser()->setLocaleFormat('M/d/yy');
		Type::build('datetime')->useImmutable()->useLocaleParser()->setLocaleFormat('M/d/yy, h:mm a');
		Type::build('timestamp')->useImmutable()->useLocaleParser()->setLocaleFormat('M/d/yy, h:mm a');

		if (in_array(Configure::read('App._environment'), ['sandbox', 'production'])) {
			$this->Security->requireSecure();
		}

		$this->Session = $this->request->session();
		$this->set('Session', $this->Session);

		$this->_icons = $this->Utilities->setIcons();
		$this->set('icons', $this->_icons);

		$title = $this->Utilities->setDefaultTitle();
		$breadcrumbs = [];
		$this->set(compact('title', 'breadcrumbs'));

		// Set the user logged in
		$Users = TableRegistry::get('Users');

		if (!is_null($this->Auth->user('id'))) {
			$this->_authUser = $Users->findById($this->Auth->user('id'))
				->contain([
					'UserDetails',
					'ProfileImg',
					'Groups'
				])
				->first();

			$this->_authUser->unsetProperty('password');
			$notifications = $this->_authUser->getNotifications();

			//Redirects urls from CKEditors to the right prefix for the user
			if (isset($this->request->params['prefix']) && $this->request->params['prefix'] == '_') {
				$redirect = $this->request->params;
				$redirect['prefix'] = $this->_authUser->prefix;
				unset($redirect['pass']);

				if(!empty($this->request->params['pass'])) {
					foreach($this->request->params['pass'] as $args) {
						$redirect[] = $args;
					}
				}

				$this->redirect($redirect);
			}

			$this->set('notifications', $notifications);
			$this->set('authUser', $this->_authUser);
		}
		else {
			$this->_authUser = false;
			$this->set('authUser', false);
			$this->viewBuilder()->layout('empty');
		}

		// To avoid race conditions, we clear the cache at the start of the application if it's requested
		// (Mostly requested by $this->_upload_image())
		if (!empty($image = $this->Session->consume('Image.clearCache')))
			$image->clearCache();

		//Save user_visits if logged in but not if admin logged in as someone else
		if ($this->_authUser && !$this->request->session()->check('previousLogin')) {
			$this->__saveVisit();
		}

		$this->_appConfig = [
			'debug' => Configure::read('debug'),
			'path' => Configure::read('path')
		];
		$this->set('appConfig', $this->_appConfig);

		$this->_setNav();

		$searchScopes = $this->_searchScopes();
		$this->set(compact('searchScopes'));
	}

	public function beforeRender(\Cake\Event\Event $event) {
		if (!empty($this->request->params['_ext']) && $this->request->params['_ext'] == 'json') {
			if (!array_key_exists('_serialize', $this->viewVars))
				throw new BadRequestException();
		}

		$Users = TableRegistry::get('Users');
		$user = $this->Auth->user('id');

		if (!empty($user)) {
			$updatedUser = $Users->get($user);
			if (!$updatedUser->terms && $this->request->param('controller') != 'Users' && $this->request->param('action') != 'dashboard') {
				return $this->redirect(['controller' => 'Users', 'action' => 'dashboard']);
			}
		}


	}

	public function forceSSL() {

		return $this->redirect('https://'.$this->request->env('HTTP_HOST').$this->request->here());

	}

	protected function _setNav() {

		if (empty($this->_authUser))
			return [];

		$this->_nav = [

			'edit-profile' => [
				'icon' => $this->_icons['user'],
				'name' => 'Edit My Profile',
				'route' => ['controller' => 'Users', 'action' => 'edit_profile']
			],

			'user' => [

				'view-profile' => [
					'icon' => $this->_icons['eye'],
					'name' => 'View My Profile',
					'route' => ['controller' => 'Users', 'action' => 'view', $this->_authUser->id]
				],

				'edit-profile' => [
					'icon' => $this->_icons['user'],
					'name' => 'Edit My Profile',
					'route' => ['controller' => 'Users', 'action' => 'edit_profile']
				],

				'password' => [
					'icon' => $this->_icons['lock'],
					'name' => 'Change Password',
					'route' => ['controller' => 'Users', 'action' => 'change_password']
				],

				/*
				'mail' => [
					'icon' => $this->_icons['email'],
					'name' => 'Inbox',
					'route' => ['controller' => 'Messages', 'action' => 'index']
				],
				*/

				//is an exception handled by angular JS, do not change the key
				/*
				@TODO
				'chat-status' => [
					'route' => '#'
				],
				*/

				'divider' => [
					'divider' => true
				],

				'logout' => [
					'icon' => $this->_icons['sign-out'],
					'name' => 'Logout',
					'route' => ['controller' => 'Users', 'action' => 'logout', 'prefix' => false]
				]
			],

			'left' => [

				'home' => [
					'icon' => $this->_icons['home'],
					'name' => 'Home',
					'route' => $this->_authUser->home_route
				],

				'forums' => [
					'icon' => $this->_icons['commenting-o'],
					'name' => 'Forums',
					'route' => '#',
					'children' => [
						'admin-forums' => [
							'name' => 'Admin',
							'route' => ['controller' => 'ForumPosts', 'action' => 'index', 'admin']
						],
						'charity-forums' => [
							'name' => 'Charities',
							'route' => ['controller' => 'ForumPosts', 'action' => 'index', 'charity']
						],
						// 'projects-forums' => [
						// 	'name' => 'Projects',
						// 	'route' => ['controller' => 'ForumPosts', 'action' => 'index', 'project']
						// ],
						'business-forums' => [
							'name' => 'Businesses',
							'route' => ['controller' => 'ForumPosts', 'action' => 'index', 'business']
						],
						'other-forums' => [
							'name' => 'Other',
							'route' => ['controller' => 'ForumPosts', 'action' => 'index', 'public']
						]
					]
				],

				'resources' => [
					'icon' => $this->_icons['list'],
					'name' => 'Resources',
					'route' => '#',
					'children' => [

						'news' => [
							'name' => 'News',
							'route' => ['controller' => 'Posts', 'action' => 'index', 1],
						],

						'khoja-summits' => [
							'name' => 'Khoja Summits',
							'route' => ['controller' => 'Posts', 'action' => 'index', 4]
						]

						/*
						'forms' => [
							'name' => 'Forms',
							'route' => ['controller' => 'Forms', 'action' => 'index'],
						],
						*/

					]
				],
				'mission' => [
					'icon' => $this->_icons['bullseye'],
					'name' => 'Mission',
					'route' => ['controller' => 'Pages', 'action' => 'mission']
				],
				/*
				'forms' => [
					'icon' => $this->_icons['file-pdf-o'],
					'name' => 'Documents',
					'route' => ['controller' => 'Forms', 'action' => 'index', '?' => ['type' => 'F']]
				],

				'surveys' => [
					'icon' => $this->_icons['bar-chart'],
					'name' => 'Surveys',
					'route' => ['controller' => 'Forms', 'action' => 'index', '?' => ['type' => 'S']]
				],
				*/

				'groups' => [
					'icon' => $this->_icons['group'],
					'name' => 'Groups',
					'route' => '#',
					'children' => [

						'business-groups' => [
							'name' => 'Businesses',
							'route' => ['controller' => 'Groups', 'action' => 'index', '?' => ['type' => 'B']],
						],

						'charity-groups' => [
							'name' => 'Charities',
							'route' => ['controller' => 'Groups', 'action' => 'index', '?' => ['type' => 'C']],
						]

					]
				],

				'contacts' => [
					'icon' => $this->_icons['user'],
					'name' => 'Contacts',
					'route' => ['controller' => 'Users', 'action' => 'index']
				],

				'projects' => [
					'icon' => $this->_icons['tasks'],
					'name' => 'Projects',
					'route' => ['controller' => 'Projects', 'action' => 'index']
				],

				'initiative' => [
					'icon' => $this->_icons['dollar'],
					'name' => 'Initiatives',
					'route' => ['controller' => 'Initiatives', 'action' => 'index'],
				],

				'khoja-cares' => [
					'icon' => $this->_icons['get-pocket'],
					'name' => 'Khoja Cares',
					'route' => ['controller' => 'Projects', 'action' => 'khoja_cares']
				],

				'events' => [
					'icon' => $this->_icons['calendar'],
					'name' => 'Events',
					'route' => ['controller' => 'Events', 'action' => 'index']
				],

				'feedback' => [
					'icon' => $this->_icons['comments'],
					'name' => 'Feedback',
					'route' => ['controller' => 'Bugs', 'action' => 'feedback']
				],

        'faq' => [
					'icon' => $this->_icons['question-circle'],
					'name' => 'FAQ',
					'route' => ['controller' => 'Faqs', 'action' => 'index']
				],
				/*
				'galleries' => [
					'icon' => $this->_icons['picture-o'],
					'name' => 'Galleries',
					'route' => ['controller' => 'Galleries', 'action' => 'index']
				],
				*/


			],
			'right' => []
		];

		if (!empty($this->request->params['prefix'])) {
			switch ($this->request->params['prefix']) {
				case 'admin':
				case 'operating_board':

					$this->_nav['left'] = array_merge($this->_nav['left'], [

						'admin-stuff' => [
							'icon' => $this->_icons['cogs'],
							'name' => 'Administration',
							'children' => [

								'assets' => [
									'icon' => $this->_icons['file-image-o'],
									'name' => 'Assets',
									'route' => ['controller' => 'Folders', 'action' => 'index']
								],

								'tags' => [
									'icon' => $this->_icons['tags'],
									'name' => 'Tags',
									'route' => ['controller' => 'Tags', 'action' => 'index']
								],

								'multi-level-tags' => [
									'icon' => $this->_icons['tags'],
									'name' => 'Multi-Level Tags',
									'route' => ['controller' => 'Tags', 'action' => 'multi_level_tag']
								],

								'khoja-care-categories' => [
									'icon' => $this->_icons['bars'],
									'name' => 'Khoja Care Categories',
									'route' => ['controller' => 'KhojaCareCategories', 'action' => 'index']
								],

								'e-blast' => [
									'icon' => $this->_icons['envelope'],
									'name' => 'E-Blast',
									'route' => ['controller' => 'Eblasts', 'action' => 'index']
								]

							]
						]

					]);

					$this->_nav['left']['contacts'] = [
						'icon' => $this->_icons['user'],
						'name' => 'Contacts',
						'route' => '#',
						'children' => [

							'people' => [
								'name' => 'People',
								'route' => ['controller' => 'Users', 'action' => 'index'],
							],

							'recommendations' => [
								'name' => 'Recommendations',
								'route' => ['controller' => 'UserRecommendations', 'action' => 'index']
							],

						]
					];

					if (in_array($this->request->params['prefix'], ['admin'])) {

						$this->_nav['left']['admin-stuff']['children']['people'] = [
							'icon' => $this->_icons['users'],
							'name' => 'People',
							'route' => ['controller' => 'Users', 'action' => 'people']
						];

						$this->_nav['left']['admin-stuff']['children']['contact_lists'] = [
							'icon' => $this->_icons['gear'],
							'name' => 'Contact Lists',
							'route' => ['controller' => 'EmailLists', 'action' => 'index']
						];

						$this->_nav['left']['admin-stuff']['children']['forum_subscribers'] = [
							'icon' => $this->_icons['rss'],
							'name' => 'Forum Subscribers',
							'route' => ['controller' => 'ForumPostSubscriptions', 'action' => 'index']
						];

						$this->_nav['left']['admin-stuff']['children']['settings'] = [
							'icon' => $this->_icons['gear'],
							'name' => 'Settings',
							'route' => ['controller' => 'Settings', 'action' => 'index']
						];

					}

					if ($this->request->params['prefix'] == 'admin') {

						$this->_nav['right'] = [

							'bug_tracker' => [
								'icon' => $this->_icons['bug'],
								'name' => 'Bugs',
								'route' => ['controller' => 'Bugs', 'action' => 'index']
							]

						];

						$this->_nav['left']['contacts']['children']['gbt'] = [
							'name' => 'Governing Board of Trustees',
							'route' => ['controller' => 'Users', 'action' => 'governing_board_trustees']
						];

						$this->_nav['left']['initiative']['children']['Initiatives'] = [
							'name' => 'Initiatives',
							'route' => ['controller' => 'Initiatives', 'action' => 'index']
						];

						$this->_nav['left']['initiative']['children']['fundraisings'] = [
							'name' => 'Fundraisings',
							'route' => ['controller' => 'Fundraisings', 'action' => 'index'],
						];

						$this->_nav['left']['initiative']['children']['review-changes'] = [
							'name' => 'Review Pledge Requests',
							'route' => ['controller' => 'Fundraisings', 'action' => 'review_changes']
						];


					}

					break;
			}

			if ($this->request->params['prefix'] == 'copywriter') {

				$this->_nav['left'] = [

					'contacts' => [
						'icon' => $this->_icons['users'],
						'name' => 'Contacts',
						'route' => ['controller' => 'Users', 'action' => 'index']
					],

					'resources' => [
						'icon' => $this->_icons['list'],
						'name' => 'Resources',
						'route' => '#',
						'children' => [
							'news' => [
								'name' => 'News',
								'route' => ['controller' => 'Posts', 'action' => 'index', 1],
							]
						]
					]

				];

			}

		}

		foreach ($this->_nav as $navKey => $navItems) {
			$this->_nav[$navKey] = $this->_editNavItems($navItems);
			unset($this->_nav[$navKey]['hasActiveItem']);
		}

		$this->set('nav', $this->_nav);
		return $this->_nav;
	}

	//Set Active Nav Items
	protected function _editNavItems($navArray, $markParentAsActive = true) {
		$activeRoute = Router::url($this->here);

		foreach ($navArray as $itemKey => $item) {
			if (isset($item['children']) && !empty($item['children'])) {

				$navArray[$itemKey]['children'] = $this->_editNavItems($item['children'], $markParentAsActive);

				if (isset($navArray[$itemKey]['children']['hasActiveItem']) && $markParentAsActive) {
					$navArray[$itemKey]['active'] = false;
					unset($navArray[$itemKey]['children']['hasActiveItem']);
					$navArray[$itemKey]['hasActiveItem'] = true;
				}
			}
			elseif (isset($item['route']) && !is_null($activeRoute)) {

				//Automatically fix routes
				if (is_array($navArray[$itemKey]['route'])) {

					if (!isset($navArray[ $itemKey ]['route']['plugin']))
						$navArray[ $itemKey ]['route']['plugin'] = false;

					if (!isset($navArray[ $itemKey ]['route']['prefix']))
						$navArray[ $itemKey ]['route']['prefix'] = $this->_authUser->prefix;

				}

				$link = Router::url($navArray[$itemKey]['route']);

				if ($link == $activeRoute) {
					$navArray[$itemKey]['active'] = true;
					$navArray['hasActiveItem'] = true;
				}
			}
		}

		return $navArray;
	}

	protected function _searchScopes() {
		return [
			'A' => 'All',
			'U' => 'Contacts',
			'E' => 'Events',
			'F' => 'Forms',
			'K' => 'Forums',
			'G' => 'Groups',
			'N' => 'News',
			'P' => 'Projects'
		];
	}

	private function __saveVisit() {

		//Don't save ajax or .json requests (only used by angular)
		if ($this->request->is('ajax') || (!empty($this->request->params['_ext']) && $this->request->params['_ext'] == 'json' ))
			return false;

		$exceptions = [
			['controller' => 'Chat', 'action' => 'chat_refresh'],
			['controller' => 'Events', 'action' => 'saved_events'],
		];

		foreach ($exceptions as $exception) {
			if ($exception['action'] == $this->request->params['action'] && $exception['controller'] == $this->request->params['controller']) {
				return false;
			}
		}

		$request = [
			'params' => $this->request->params,
			'data' => $this->request->data,
			'query' => $this->request->query,
			'environment' => [
				'http_referer' => $this->request->env('HTTP_REFERER'),
				'request_method' => $this->request->env('REQUEST_METHOD')
			]
		];

		$UserVisits = TableRegistry::get('UserVisits');

		$visit = $UserVisits->newEntity([
			'user_id' => $this->_authUser->id,
			'controller' => $this->request->params['controller'],
			'action' => $this->request->params['action'],
			'request' => safe_json_encode($request),
			'path' => $this->request->here,
			'user_ip' => $this->request->clientIp(),
			'user_browser' => $this->request->env('HTTP_USER_AGENT')
		]);

		$UserVisits->save($visit);
		return true;
	}

	public function s3GetObject($asset) {
		//gets single asset
		$this->loadModel('Assets');

		$s3 = $this->Assets->s3Client();

		$result = $s3->getObject([
			'Bucket'=> $this->Assets->s3Bucket(),
			'Key'=> $asset->id,
			'ResponseContentType' => $asset->type
		]);

		return $result;
	}

	protected function _upload_image($id, $imageType) {
		$model = $this->modelClass;

		if (! $this->$model->exists($id))
			throw new NotFoundException();

		$entity = $this->$model->findById($id)
			->contain([
				Inflector::classify($imageType)
			])
			->first();

		switch($model) {

			case 'Groups':
				$redirectLink = ['controller' => 'Groups', 'action' => 'edit', $entity->id];
				break;

			case 'Users':
				if ($this->_authUser->id == $entity->id)
					$redirectLink = ['controller' => 'Users', 'action' => 'edit_profile'];
				else
					$redirectLink = ['controller' => 'Users', 'action' => 'edit', $entity->id];
				break;

			case 'Posts':
				if (! empty($entity->category_id))
					$redirectLink = ['action' => 'index', $entity->category_id];
				else
					$redirectLink = ['action' => 'index'];
				break;

			default:
				$redirectLink = ['action' => 'edit', $entity->id];
				break;
		}

		if ($this->request->is(['post', 'put'])) {


			if (!empty($this->request->data['chosen_asset'])) {
				$this->loadModel('AssetManager.Assets');

				$assetToCopy = $this->Assets->findById($this->request->data['chosen_asset'])->first();

				if ($assetToCopy) {

					if (empty($entity->$imageType)) {
						$entity->$imageType = $this->Assets->newEntity([
							'model' => $model . 'Table',
							'model_id' => $id,
							'association' => Inflector::classify($imageType)
						]);
					}

					// Copy fields
					$copyFields = ['name', 'type', 'size', 'width', 'height'];
					$nullifyFields = ['crop_x', 'crop_y', 'crop_width', 'crop_height'];

					foreach ($copyFields as $field)
						$entity->$imageType->$field = $assetToCopy->$field;

					foreach ($nullifyFields as $field)
						$entity->$imageType->$field = null;

					// Note Bene: Re-merged previous changes to respect the 'ratio' parameter

					$behaviorConfig = $this->$model->behaviors()->get('Asset')->config();

					if (!empty($behaviorConfig['hasOne'][Inflector::classify($imageType)]['ratio'])) {

						$ratio = $behaviorConfig['hasOne'][Inflector::classify($imageType)]['ratio'];

						$entity->$imageType->crop_x = 0;
						$entity->$imageType->crop_y = 0;

						$entity->$imageType->crop_width = $entity->$imageType->width;
						$entity->$imageType->crop_height = round($entity->$imageType->width / $ratio);

						if ($entity->$imageType->crop_height > $entity->$imageType->height) {

							$entity->$imageType->crop_height = $entity->$imageType->height;
							$entity->$imageType->crop_width = $entity->$imageType->height * $ratio;

						}

					}

					// Save
					/*
					 * Note Bene: We need to save the asset entity directly, instead of saving it through $this->$model->save()
					 * Saving through $this->$model->save() could(!) possibly work with _joinData
					 * See: http://book.cakephp.org/3.0/en/orm/saving-data.html#saving-data-to-the-join-table
					 * Tried to figure out a way to do it, but had no luck :(
					 */
					if ($this->Assets->save($entity->$imageType)) {

						// Trick Asset Manager to download the image for us (if it's not already there)
						$assetToCopy->url();

						// Create temporary object to simulate file upload
						$tmpObj = new \stdClass;

						$tmpObj->id = $entity->$imageType->id;
						$tmpObj->upload = [
							'tmp_name' => $assetToCopy->path(),
							'type' => $assetToCopy->type
						];

						// Upload file to S3
						TableRegistry::get('AssetManager.Assets')->awsUploadFile($tmpObj);

						$this->Session->write('Image.clearCache', $entity->$imageType);
						$this->redirect([]);

					}
					else {
						$this->Flash->error('Error...');
					}

				}

				unset($this->request->data['chosen_asset']);
			}
			else {
				if (isset($this->request->data[$imageType]['upload'])) {

					$entity = $this->$model->patchEntity($entity, $this->request->data, [
						'associated' => [Inflector::classify($imageType)]
					]);

					$this->$model->save($entity);

					$this->Session->write('Image.clearCache', $entity->$imageType);
					$this->redirect([]);

				}
				else {
					$entity->$imageType->crop($this->request->data);
					$this->Session->write('Image.clearCache', $entity->$imageType);
					$this->redirect($redirectLink);
				}
			}
		}

		$title = 'Upload Image';
		$titleIcon = $this->_icons['image'];

		switch($model) {

			case 'Groups':

				$title = ($imageType == 'profile_img') ? 'Profile Image' : 'Banner Image';
				$title .= ' of ' . $entity->name;
				$breadcrumbs = [
					['name' => 'Groups', 'route' => ['action' => 'index']],
					['name' => 'Edit Group', 'route' => ['action' => 'edit', $entity->id]],
				];
				break;

			case 'Users':

				$title = ($imageType == 'profile_img') ? 'Profile Image' : 'Banner Image';

				if ($entity->id == $this->_authUser->id) {
					$title = 'Your ' . $title;
					$breadcrumbs[] = ['name' => 'Edit Your Profile', 'route' => ['action' => 'edit_profile']];
				}
				else {
					$title .= ' of ' . $entity->name;
					$breadcrumbs = [
						['name' => 'Contacts', 'route' => ['action' => 'index']],
						['name' => 'Edit Contact', 'route' => ['action' => 'edit', $entity->id]],
					];
				}

				break;

			case 'Posts':
				$title = 'Post Image';
				$category = $this->$model->Categories->findById($entity->category_id)->first();
				$breadcrumbs[] = ['name' => $category->name, 'route' => ['action' => 'index', $entity->category_id]];
		}

		$this->loadModel('Folders');

		$folders = $this->Folders->find('threaded');

		$bannerActions = [$this->_icons['back'] => $redirectLink];
		$user = $this->_authUser;

		$this->set(compact('entity', 'imageType', 'title', 'titleIcon', 'breadcrumbs', 'bannerActions', 'folders', 'redirectLink', 'user'));
		$this->render('/Shared/_upload_image');
	}

	protected function _ajax_delete_image() {

		$id = $this->request->data['entityId'];

		$response = ['status' => 'error'];

		$this->loadModel('Assets');

		$image = $this->Assets->get($id);

		if ($this->Assets->delete($image)) {
			$response = ['status' => 'ok'];
		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_reset_image() {

		$this->loadModel('Assets');

		$id = $this->request->data['entityId'];

		$asset = $this->Assets->get($id);

		$response = ['status' => 'error'];
		
		$ratio = 1;
		if ($asset->association == 'FeaturedImg') {
			$ratio = 24.5 / 9;
		} elseif ($asset->association == 'BannerImg') {
			$ratio = 1.6;
		}

		$asset = $this->Assets->patchEntity($asset, [
			'crop_x' => 0,
			'crop_y' => 0,
			'crop_width' => $asset->width,
			'crop_height' => $asset->height / $ratio,
		]);

		if ($this->Assets->save($asset)) {
			$response = ['status' => 'ok'];
		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _related_posts($returnJson = true) {
		$this->autoRender = false;
		$relatedPosts = [];

		// To do for each models that have pages to link to

		$this->loadModel('Posts');

		$posts = $this->Posts->find()
			->contain([
				'Categories'
			]);

		foreach($posts as $post) {
			$array = [];

			//Filter out PDF in posts/_form but not in ckeditor
			if (! $post->isPdf() || !$returnJson)
				$array = $this->Posts->getPostArray('Posts', $post);

			if (!empty($array))
				$relatedPosts[] = $array;
		}

		//---

		$this->loadModel('Forms');

		$forms = $this->Forms->find();

		foreach($forms as $form) {
			$array = [];

			if (! $form->isPdf() || !$returnJson)
				$array = $this->Forms->getPostArray('Forms', $form);

			if (!empty($array))
				$relatedPosts[] = $array;
		}

		if($returnJson) {
			$this->response->disableCache();
			$this->response->type('json');
			$this->response->body(json_encode($relatedPosts));
		}
		else {
			return $relatedPosts;
		}
	}

	protected function _pinned_post() {

		$this->loadModel('Posts');

		$pinnedPost = $this->Posts->find()
			->where([
				'Posts.pinned' => true
			])
			->first();

		return $pinnedPost;

	}


	//-------------------------------------------------------------CKEDTITOR-JSONS------------

	protected function _links() {
		$this->autoRender = false;
		$links = [];

		$relatedPost = $this->related_posts(false);

		if(!empty($relatedPost))
			$links += $relatedPost;

		// To allow link to file uploaded

		$dir = WWW_ROOT . 'assets';
		$file_ext = ['pdf', 'doc', 'docx', 'xls', 'xlsx', 'txt'];
		$files = scandir($dir);

		foreach($files as $file) {
			if($file == '.' || $file == '..')
				continue;

			$fileArray = explode('.', $file);
			$ext = array_pop($fileArray);

			$fileDate = date("l F j, Y", filectime(WWW_ROOT . 'assets' . DS . $file));

			if(in_array($ext, $file_ext))
				$links[] = ['folder' => 'Files', 'category' => '', 'date' => $fileDate, 'name' => $file, 'url' => '/assets/' . rawurlencode($file)];
		}

		//end

		$this->response->disableCache();
		$this->response->type('json');
		$this->response->body(json_encode($links));
	}

	/*
	public function images() {
		$this->autoRender = false;
		$images = [];

		$dir = WWW_ROOT . 'assets';
		$file_ext = ['bmp','gif','jpeg','jpg','png'];
		$files = scandir($dir);

		foreach($files as $file) {
			if($file == '.' || $file == '..')
				continue;

			$fileArray = explode('.', $file);
			$ext = array_pop($fileArray);
			if(in_array($ext, $file_ext))
				$images[] = ['image' => '/assets/' . rawurlencode($file)];
		}

		$this->response->disableCache();
		$this->response->type('json');
		$this->response->body(json_encode($images));
	}
	*/

}
