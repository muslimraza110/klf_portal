<?php
namespace App\Controller\Component;

use App\Model\Table\Table;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use Cake\I18n\Time;

class SearchComponent extends \Cake\Controller\Component {

	public $components = ['Auth'];
	
	protected function _prepare($q, $type) {
		$terms = explode(' ', $q);
		$where = [];

		foreach ($terms as $term) {

			switch ($type) {

				case 'K':
					$where = [
						'OR' => [
							'ForumPosts.topic LIKE' => '%' . $term . '%',
							'ForumPosts.content LIKE' => '%' . $term . '%'
						]
					];
					break;

				case 'U':
					$where = [
						'OR' => [
							'Users.first_name LIKE' => '%' . $term. '%',
							'Users.last_name LIKE' => '%' . $term . '%',
							'Users.email LIKE' => '%' . $term . '%',
							'UserDetails.city LIKE' => '%'.$term.'%',
							'UserDetails.region LIKE' => '%'.$term.'%',
							'UserDetails.country LIKE' => '%'.$term.'%',
							'UserDetails.private_bio LIKE' => '%'.$term.'%',
						]
					];
					break;

				case 'F':
					$where = [

						'CAST(Forms.start_date AS date) <=' => Time::now(),
						
						'OR' => [
							'Forms.name LIKE' => '%' . $term . '%',
							'Forms.topic LIKE' => '%' . $term . '%',
						]
					];
				break;
				
				case 'G':

					$Users = TableRegistry::get('Users');

					$user = $Users->get($this->Auth->user('id'), [
						'contain' => [
							'Groups'
						]
					]);

					$where['OR'] = [
						'Groups.name LIKE' => '%' . $term . '%',
						'Groups.description LIKE' => '%' . $term . '%',
						'Groups.email LIKE' => '%' . $term . '%',
						'Groups.phone LIKE' => '%' . $term . '%',
						'Groups.address LIKE' => '%' . $term . '%',
						'Groups.address2 LIKE' => '%' . $term . '%',
						'Groups.city LIKE' => '%' . $term . '%',
						'Groups.region LIKE' => '%' . $term . '%',
						'Groups.country LIKE' => '%' . $term . '%',
						'Groups.postal_code LIKE' => '%' . $term . '%',
						'Groups.bio LIKE' => '%' . $term . '%'
					];

					$permissionConditions = [];

					if (!in_array($user->role, ['A'])) {

						$authUserGroupIds = $user->getGroupIds();

						$permissionConditions['Groups.pending'] = false;

						$permissionConditions[]['OR'] = [
							'Groups.hidden' => false,
							'Groups.id IN' => !empty($authUserGroupIds) ? $authUserGroupIds : [0]
						];

					}

					$Groups = TableRegistry::get('Groups');

					$groupIds = $Groups->find()
						->select([
							'Groups.id'
						])
						->matching('Tags', function ($query) use ($term) {

							return $query
								->where([
									'Tags.name LIKE' => '%'.$term.'%'
								]);

						})
						->where([
							$permissionConditions
						])
						->extract('id')
						->toArray();

					$where = array_merge(
						$where,
						$permissionConditions
					);

					if (!empty($groupIds))
						$where = [
							'OR' => [
								$where,
								'Groups.id IN' => $groupIds
							]
						];

					break;

				case 'N':
					$where = [
						'Posts.published IS NOT' => NULL,
						'OR' => [
							'Posts.name LIKE' => '%' . $term . '%',
							'Posts.content LIKE' => '%' . $term . '%',
						]
					];
					break;

				case 'P':
					$where = [
						'OR' => [
							'Projects.name LIKE' => '%'.$term.'%'
						]
					];
					break;

				case 'E':
					$where = [
						'OR' => [
							'Events.name LIKE' => '%'.$term.'%'
						]
					];
					break;

			}
		}
	
		return $where;
	}

	public function scope($q) {
		
		$scope = 'A';
		$query = $q['q'];
		$results = [];

		if (isset($q['scope']) && !empty($q['scope'])) {
			$scope = $q['scope'];
		}

		switch ($scope) {
			case 'U':
				$results = $this->users($query);
				break;
			case 'G':
				$results = $this->groups($query);
				break;
			case 'K':
				$results = $this->forumPosts($query);
				break;
			case 'N':
				$results = $this->posts($query);
				break;
			case 'F':
				$results = $this->forms($query);
				break;
			case 'P':
				$results = $this->projects($query);
				break;
			case 'E':
				$results = $this->events($query);
				break;
			case 'A':
				$results = $this->all($query);
				break;
			
		}
		
		return $results;
	}
	

	public function users($q) {
		$where = [];
		$Users = TableRegistry::get('Users');
		$authUser = $Users->get($this->Auth->user('id'));
		if ($authUser->role == 'C') {
			$where = [
				'Users.role !=' => 'A'
			];
		}
		$users = $this->usersQuery($q)
			->contain([
				'ProfileImg',
				'UserDetails'
			])
			->where($where)
			->each(function ($value, $key) {
				$value->dataType = 'Users';
			})
			->toList();
		
		return $users;
	}
	
	public function usersQuery($q) {
		$Users = TableRegistry::get('Users');
		
		$where = $this->_prepare($q, 'U');
		
		$usersQuery = $Users->find()
			->where($where);
		
		return $usersQuery;
	}

	public function groups($q) {
		
		$groups = $this->groupsQuery($q)
			->each(function ($value, $key) {
				$value->dataType = 'Groups';
			})
			->toList();
		
		return $groups;
	}
	
	public function groupsQuery($q) {
		$Groups = TableRegistry::get('Groups');
		
		$where = $this->_prepare($q, 'G');
		
		$groupsQuery = $Groups->find()
			->where($where);
		
		return $groupsQuery;
	}

	public function forumPostsQuery($q) {
		$ForumPosts = TableRegistry::get('ForumPosts');
		$Users = TableRegistry::get('Users');

		$authUser = $Users->get($this->Auth->user('id'), [
			'contain' => [
				'Groups'
			]
		]);

		$where = $this->_prepare($q, 'K');

		$forumPostsQuery = $ForumPosts->find()
			->where($where);

		if (($forumPostIds = $ForumPosts->getPermittedIds($authUser)) !== null) {

			if (empty($forumPostIds))
				$forumPostIds = [0];

			$forumPostsQuery->where([
				'OR' => [
					'ForumPosts.id IN' => $forumPostIds,
					'ForumPosts.parent_id IN' => $forumPostIds
				]
			]);

		}

		return $forumPostsQuery;
	}

	public function forumPosts($q) {
		
		$forumPosts = $this->forumPostsQuery($q)
			->contain([
				'ParentForumPosts'
			])
			->each(function ($value, $key) {
				$value->dataType = 'ForumPosts';
			})
			->toList();

		return $forumPosts;
	}

	public function postsQuery($q, $categoryId = null) {
		$Posts = TableRegistry::get('Posts');

		$where = $this->_prepare($q, 'N');

		if (!is_null($categoryId))
			$where['Posts.category_id'] = $categoryId;

		$postsQuery = $Posts->find()
			->where($where);
	 
		return $postsQuery;
	}

	public function posts($q, $postTypeId = null) {
		$posts = $this->postsQuery($q, $postTypeId)
			->each(function ($value, $key) {
				$value->dataType = 'Posts';
			})
			->toList();

		return $posts;
	}

	public function formsQuery($q) {
		$Forms = TableRegistry::get('Forms');

		$where = $this->_prepare($q, 'F');

		$formsQuery = $Forms->find()
			->where($where)
			->order(['Forms.start_date', 'Forms.name DESC']);
	 
		return $formsQuery;
	}

	public function forms($q) {
		$forms = $this->formsQuery($q)
			->each(function ($value, $key) {
				$value->dataType = 'Forms';
			})
			->toList();
	 
		return $forms;
	}

	public function projectsQuery($q) {

		$Projects = TableRegistry::get('Projects');

		$where = $this->_prepare($q, 'P');

		$projectsQuery = $Projects->find()
			->where($where);

		return $projectsQuery;

	}

	public function projects($q) {

		$projects = $this->projectsQuery($q)
			->each(function ($value, $key) {
				$value->dataType = 'Projects';
			})
			->toList();

		return $projects;

	}

	public function eventsQuery($q) {

		$Events = TableRegistry::get('Events');

		$where = $this->_prepare($q, 'E');

		$eventsQuery = $Events->find()
			->where($where);

		return $eventsQuery;

	}

	public function events($q) {

		$events = $this->eventsQuery($q)
			->each(function ($value, $key) {
				$value->dataType = 'Events';
			})
			->toList();

		return $events;

	}

	public function all($q) {
		$results = [];

		$users = $this->users($q);
		$groups = $this->groups($q);
		$forumPosts= $this->forumPosts($q);
		$posts = $this->posts($q);
		$forms = $this->forms($q);
		$projects = $this->projects($q);
		$events = $this->events($q);

		$results = (new Collection([]))
			->append($users)
			->append($groups)
			->append($forumPosts)
			->append($posts)
			->append($forms)
			->append($projects)
			->append($events)
			->toList();
		
		return $results;
	}
	
}