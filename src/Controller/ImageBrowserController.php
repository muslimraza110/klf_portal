<?php
namespace App\Controller;

use Cake\Routing\Router;

class ImageBrowserController extends AppController {

	protected $_acceptedMIMETypes = [
		'image/jpeg',
		'image/jpg',
		'image/png',
		'image/gif'
	];

	public function initialize() {

		parent::initialize();

		$this->loadComponent('RequestHandler');

	}

	protected function _browse() {

		$this->loadModel('Files');

		$where = [];
		$this->_authUser->role != 'A' ? $where['Folders.is_protected !='] = 1 : null;
		$where['resource.type IN'] = $this->Files->resource->mimeTypes('webImage');

		$files = $this->Files->find()
			->contain([
				'Folders',
				'resource'
			])
			->where($where)
			->order(['Folders.name']);

		$response = [];

		foreach ($files as $file) {
			$stdObj = new \stdClass;

			$stdObj->image = Router::url(['controller' => 'Assets', 'action' => 'view', $file->resource->id, 'prefix' => '_'], ['_full' => true]);
			$stdObj->thumb = $file->resource->url(['width' => '250']);

			$stdObj->folder = (! empty($file->folder)) ? $file->folder->name : 'Unknown Folder';

			$response[] = $stdObj;
		}

		$this->RequestHandler->renderAs($this, 'json');

		$this->set(compact('response'));
		$this->set('_serialize', 'response');
	}

}