<?php

namespace App\Controller\OrganizingCommittee;

class GalleriesController extends \App\Controller\GalleriesController {

	public function index() {
		$this->_index();
	}

	public function view($folderId) {
		$this->_view($folderId);
	}

}