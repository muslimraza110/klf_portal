<?php
namespace App\Controller\OrganizingCommittee;

class FundraisingsController extends \App\Controller\FundraisingsController {

	public function index() {

		$this->_index();

	}

	public function view($id) {

		$this->_view($id);

	}

	public function pledge($id) {

		$this->_pledge($id);

	}

	public function my_pledges() {

		$this->_my_pledges();

	}


	public function edit_pledge($id) {

		$this->_edit_pledge($id);

	}

}
