<?php

namespace App\Controller\OrganizingCommittee;

class FilesController extends \App\Controller\FilesController {

	public function index() {

		$this->_index();

	}

	public function download($id) {

		$this->_download($id);

	}

	public function ajax_view($folderId) {

		$this->_ajax_view($folderId);

	}

	public function ajax_image_list($folderId) {
		$this->_ajax_image_list($folderId);
	}

}