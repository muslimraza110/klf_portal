<?php

namespace App\Controller\OrganizingCommittee;

class UserRecommendationsController extends \App\Controller\UserRecommendationsController {

	public function view($id) {

		$this->_view($id);

	}

	public function recommend() {

		$this->_recommend();

	}

	public function ajax_html_user_list() {

		$this->_ajax_html_user_list();

	}

	public function ajax_add_user() {

		$this->_ajax_add_user();

	}

}