<?php

namespace App\Controller;

class FilesController extends AppController {

	protected function _index() {
		$this->render(DS . 'Files' . DS . '_index');
	}

	protected function _delete($id) {
		$this->request->allowMethod(['post', 'delete']);
		
		$file = $this->Files->get($id);
		
		if ($this->Files->delete($file)) {
			$this->Flash->success('File deleted successfully.');
			return $this->redirect(['controller' => 'Folders', 'action' => 'view', $file->folder_id]);
		}
		else
			$this->Flash->error('The file could not be deleted.');
	}

	protected function _download($id) {

		$this->autoRender = false;
		
		$file = $this->Files->find()
			->contain([
				'resource'
			])
			->where([
				'Files.id' => $id
			])
			->first();

		$result = $this->s3GetObject($file->resource);

		$this->response->type($result['ContentType']);
		$this->response->download($file->resource->name);
		$this->response->body($result['Body']);
		
//		$this->response->file(
//			$file->resource->path(),
//			[
//				'download' => true,
//				'name' => $file->resource->name
//			]
//		);
//
//		$this->render(DS . 'Files' . DS . '_download');
	}

	protected function _ajax_view($folderId) {
		$this->loadModel('Assets');
		$this->viewBuilder()->layout(false);

		$bucket = $this->Assets->s3Bucket();
		$files = $this->Files->find()
			->contain([
				'resource',
				'Folders'
			])
			->where([
				'Folders.id' => $folderId
			]);
		
		$this->set(compact('files', 'bucket'));
		$this->render(DS . 'Files' . DS . '_ajax_view');
	}

	protected function _ajax_image_list($folderId) {
		$this->viewBuilder()->layout(false);
		
		$this->loadModel('AssetManager.Assets');
		
		$webImages = $this->Assets->mimeTypes('webImage');
		$bucket = $this->Assets->s3Bucket();
		
		$files = $this->Files->find()
			->contain([
				'resource',
				'Folders'
			])
			->where([
				'Folders.id' => $folderId,
				'resource.type IN' => $webImages
			]);
		
		$this->set(compact('files', 'bucket'));
		$this->render(DS . 'Files' . DS . '_ajax_image_list');
	}

}