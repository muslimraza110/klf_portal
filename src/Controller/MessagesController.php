<?php
namespace App\Controller;
use Cake\Network\Exception\InternalErrorException;
use Cake\Network\Exception\NotFoundException;
use Cake\I18n\Time;

class MessagesController extends \App\Controller\AppController
{
	protected function _index($type = 'inbox')
	{
		$titleIcon = $this->_icons['envelope'];
		$query = $this->Messages->MessagesUsers->find($type);

		$this->paginate = [
      'limit' => 25
    ];

		$messages = $this->paginate($query);

		if ($this->request->is(['post', 'put'])) {
			if (isset($this->request->data['messages'])) {
				$ids = [];

				foreach ($this->request->data['messages'] as $data) {
					if($data['check'])
						$ids[] = $data['id'];
				}

				if (!empty($ids)) {
					if ($this->request->data['Form']['action'] == 'delete')
						$this->_delete($ids);
					else
						$this->_toggle_options($this->request->data['Form']['action'], $ids, false);
				}
			}

			$this->redirect(['action' => 'index', $type]);
		}

		$this->set(compact('titleIcon', 'type', 'messages'));
		$this->render(DS . 'Messages' . DS . 'index');
	}

	protected function _view($id)
	{
		$conditions = [
			'Messages.id' => $id,
			'NOT' => [
				'Messages.sent IS' => null
			]
		];
		
		if ($this->request->action == 'view_sent') {
			$conditions['Senders.user_id'] = $this->_authUser->id;
		}

		$message = $this->Messages->find()
			->contain([
				'Senders.Users',
				'Recipients.Users',
				'CcRecipients.Users'
			])
			->where($conditions)
			->first();
		
		if (!$message || ($this->request->action == 'view' && !$message->isRecipientOf())) {
			throw new NotFoundException('The message doesn\'t seem to exist...');
		}
		
		if($this->request->action != 'view_sent')
			$message->toggleOption('read');

		$this->set(compact('message'));

		$this->render(DS . 'Messages' . DS . 'view');
	}

	protected function _view_sent($id)
	{
		$this->_view($id);
	}
	
	protected function _view_archive($id)
	{
		$this->_view($id);
	}

	protected function _add()
	{
	
		$message = $this->Messages->newEntity();

		if ($this->request->is(['post', 'put'])) {
			$this->request->data['Senders']['user_id'] = $this->_authUser->id;
		}
		
		$this->_form($message);
	}

	protected function _edit($id)
	{
		$title = 'Edit Draft';
    $this->set(compact('title'));
    /*
    $message = $this->Messages->find()
    	->contain([
    		'MessagesUsers.Users'
    	])
    	->where([
    		'Messages.id' => $id
    	])
    	->first();
		*/

    $message = $this->Messages->get($id, [
    	'contain' => [
    		'MessagesUsers.Users'
    	]
    ]);

    if (!$message || !is_null($message->sent)) {
			throw new NotFoundException('Could not find the message to edit.');
		}

		$message = $this->Messages->getTagitStrings($message);
		//unset($message->messages_users);

		$this->_form($message);
	}

	protected function _form($message)
	{
		if ($this->request->is(['post', 'put'])) {
			
			$this->request->data = $this->Messages->setTagitData($this->request->data);

			if (empty($message->messages_users)) {
				$this->request->data['messages_users'][] = [
					'user_id' => $this->Auth->user('id'),
					'action' => 'F'
				];
			}

			if(!$this->Messages->recipientErrors) {

				if(isset($this->request->data['send-btn'])) {
					$this->request->data['sent'] = Time::now();
				}
				// why delete?
				/*
				if(!empty($this->request->data['id']) && $this->Messages->validates($this->request->data)) {
					$this->Messages->MessagesUsers->deleteAll(['MessagesUsers.message_id' => $this->request->data['id']]);
				}*/
			
				$message = $this->Messages->patchEntity($message, $this->request->data, [
					'associated' => ['MessagesUsers']
				]);
				//pr($message);
				unset($message->recipients); //cant save these data to the message table
				unset($message->cc_recipients);
				
				if($this->Messages->save($message)) {
					if(isset($this->request->data['send-btn'])) {
						$this->Flash->success('The message has been sent');
					} else {
						$this->Flash->success('The message has been saved as draft');
					}

					if ($this->request->action == 'edit')
						$this->redirect(['action' => 'index', 'draft']);
					else
						$this->redirect(['action' => 'index']);

				} else {
					$this->Flash->error('An error occured');
				}

			} elseif($this->Messages->recipientErrors) {

				$errors = $this->Messages->recipientErrors;
				
				if(isset($errors['NO-RECIPIENT'])) {
					$this->Flash->error('The message cannot be sent because no recipients have been selected.');
				}
				else {
					$flashMsg = 'Some recipients are invalid. Please always use the autocomplete to add recipients.
					<br / >These recipients have been removed: <ul>';
					
					foreach($errors as $recipientName) {
						$flashMsg .= '<li>' . $recipientName . '</li>';
					}
					
					$flashMsg .= '</ul>';
					$this->Flash->error($flashMsg);
				}
			}
		}
		
		$this->set(compact('message'));
		$this->render(DS . 'Messages' . DS . '_form');
	}

	protected function _reply($id, $all = false)
	{
		if (!$this->request->is(['post', 'put'])) {
			$message = $this->Messages->find()
				->contain([
					'Senders.Users',
					'MessagesUsers.Users'
				])
				->where([
					'Messages.id' => $id
				])
				->first();
			
			if (!$message->isRecipientOf())
				throw new NotFoundException('Could not find the message to reply to.');

			if ($message) {
				$message->sent = null;
				$message->id = '';
				$message->created = '';
				$message->modified = '';
				$message->subject = 'RE: ' . $message->subject;

				$reply = ($all) ? 'all' : 'sender';
				$message = $this->Messages->getTagitStrings($message, $reply);
				unset($message->sender);
				unset($message->messages_users);
			}

			if (!$message || !is_null($message->sent)) {
				throw new NotFoundException('Could not find the message to reply to.');
			}

			$this->request->data = $message;
		}

		$this->add();
	}

	protected function _delete($id)
	{
		$this->autoRender = false;

	}

	protected function _move_to_archive($id)
	{
		$this->_toggle_options('archive', $id);
		$this->redirect(['action' => 'index', 'archive']);
	}

	protected function _remove_from_archive($id)
	{
		$this->_toggle_options('not archive', $id);
		$this->redirect(['action' => 'index', 'archive']);
	}

	protected function _mark_as_read($id)
	{
		$this->_toggle_options('read', $id, true);
	}

	protected function _mark_as_not_read($id)
	{
		$this->_toggle_options('not read', $id, true);
	}

	protected function _toggle_options($markAs, $id = [], $redirect = false)
	{
		$this->autoRender = false;
		$ids = [];

		if(is_array($id)) {
			$ids = $id;
		}
		else {
			$ids[] = $id;
		}

		$messages = $this->Messages->find()
			->contain([
				'MessagesUsers'
			])
			->where([
				'Messages.id IN' => $ids,
				'NOT' => [
					'Messages.sent IS' => null
				]
			]);

		if (!$messages->isEmpty()) {
			foreach($messages as $message) {
				if($message->toggleOption($markAs)) {

					switch($markAs) {
						case 'archive':
							$flashMsgSingular = 'The message has been moved to your archive.';
							$flashMsgPlural = 'The messages have been moved to your archive.';
							break;
						case 'not archive':
							$flashMsgSingular = 'The message has been moved back to your inbox.';
							$flashMsgPlural = 'The messages have been moved back to your inbox.';
							break;
						case 'read':
							$flashMsgSingular = 'The message has been marked as read.';
							$flashMsgPlural = 'The messages have been marked as read.';
							break;
						case 'not read':
							$flashMsgSingular = 'The message has been marked as not read.';
							$flashMsgPlural = 'The messages have been marked as not read.';
							break;
						case 'trash':
							$flashMsgSingular = 'The message has been moved to trash.';
							$flashMsgPlural = 'The messages have been moved to trash.';
							break;
						case 'not trash':
							$flashMsgSingular = 'The message has been restored.';
							$flashMsgPlural = 'The message has been restored.';
							break;
					}
				}
			}
		}

		if (count($ids) == 1) {
			if (isset($flashMsgSingular))
				$this->Flash->success($flashMsgSingular);
			else
				$this->Flash->error('The action did not work...');
		} else {
			if (isset($flashMsgPlural))
				$this->Flash->success($flashMsgPlural);
			else
				$this->Flash->error('The action did not work...');
		}
		
		if($redirect)
			$this->redirect(['action' => 'index']);
	}

	protected function _ajax_autocomplete_recipients() {
		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			$this->response->disableCache();
			
			if(empty($this->request->query['term']))
				throw new InternalErrorException('Must use a search term.');
			
			$term = strtolower($this->request->query['term']);

			$recipients = $this->Messages->MessagesUsers->Users->find()
				->select(['id', 'first_name', 'last_name', 'email'])
				->where([
					'OR' => [
						'LOWER(first_name) LIKE' => $term . '%',
						'LOWER(last_name) LIKE' => $term . '%',
						'CONCAT(LOWER(first_name), " ", LOWER(last_name)) LIKE' => $term . '%',
						'LOWER(email) LIKE' => $term . '%',
					]
				])
				->toArray();

			$this->response->type('json');
			$this->response->body(json_encode($recipients));
		}
	}

	protected function _ajax_important_tag()
	{
		if ($this->request->is('ajax')) {
			$this->autoRender = false;

			if (isset($this->request->data['id'])) {
				$messageUser = $this->Messages->MessagesUsers->get($this->request->data['id']);

				if ($messageUser->important == 0)
					$messageUser->important = 1;
				else
					$messageUser->important = 0;

				if ($this->Messages->MessagesUsers->save($messageUser)) {
					if ($messageUser->important == 1)
						$response['success'] = 'active';
					else
						$response['success'] = 'inactive';
				} else {
					$response['error'] = 'The request cannot be saved';
				}
			}

			$this->response->type('json');
	  	$this->response->body(json_encode($response));
		}
	}

}

