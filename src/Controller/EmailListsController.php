<?php
namespace App\Controller;

class EmailListsController extends \App\Controller\AppController {

	protected function _index() {

		$emailLists = $this->EmailLists->find()
			->contain([
				'EmailReceivers'
			]);

		$roleGroupedUserLists = $this->EmailLists->getRoleGroupedUserList();

		$title = 'Contact Lists';
		$breadcrumbs = [
			['active' => true, 'name' => $title]
		];

		$this->set(compact('emailLists', 'breadcrumbs', 'title', 'roleGroupedUserLists'));
		$this->render('/EmailLists/index');

	}

	protected function _add() {

		$emailList = $this->EmailLists->newEntity([
			'user_id' => $this->Auth->user('id')
		]);

		$title = 'Add Contact List';

		$this->_form($emailList, $title);

	}

	protected function _edit($id) {

		$emailList = $this->EmailLists->get($id, [
			'contain' => 'EmailReceivers'
		]);

		$title = 'Edit ' . $emailList->title;

		$this->_form($emailList, $title);

	}

	protected function _form($emailList, $title) {

		if ($this->request->is(['post', 'put'])) {

			$emailList = $this->EmailLists->patchEntity($emailList, $this->request->data, ['associated' => ['EmailReceivers']]);

			if ($this->EmailLists->save($emailList)) {
				$this->Flash->success('Contact List saved successfully.');
				return $this->redirect(['action' => 'index']);
			}

		}

		$roleGroupedUserLists = $this->EmailLists->getRoleGroupedUserList();

		$breadcrumbs = [
			['name' => 'Contact Lists', 'route' => ['action' => 'index']],
			['active' => true, 'name' => $title]
		];

		$this->set(compact('emailList','roleGroupedUserLists', 'breadcrumbs', 'title'));
		$this->render('/EmailLists/_form');

	}

	protected function _view($role) {

		$roleKey = array_search($role, $this->EmailLists->Users->roles);
		$roleGroupedUserLists = $this->EmailLists->getRoleGroupedUserList();

		if (($roleKey !== false && isset($roleGroupedUserLists[$role])) || $role == 'Unassociated Contacts') {
			$users = $roleGroupedUserLists[$role];
		} else {
			$this->Flash->error('Invalid Role. (' . $role . ')');
			return $this->redirect(['action' => 'index']);
		}

		$title = 'View ' . $role;
		$breadcrumbs = [
			['name' => 'Contact Lists', 'route' => ['action' => 'index']],
			['active' => true, 'name' => $title]
		];

		$this->set(compact('users', 'role', 'breadcrumbs', 'title'));
		$this->render('/EmailLists/view');

	}

	protected function _delete($id) {

		$this->autoRender = false;

		$emailList = $this->EmailLists->get($id);
		$this->EmailLists->delete($emailList);

		$this->Flash->success('Contact List Delete Successfully.');
		return $this->redirect(['action' => 'index']);

	}

}
