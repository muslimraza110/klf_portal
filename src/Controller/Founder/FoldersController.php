<?php
namespace App\Controller\Founder;

class FoldersController extends \App\Controller\FoldersController {

	public function index() {

		$this->_index();

	}

	public function add() {

		$this->_add();

	}

	public function edit($id) {

		$this->_edit($id);

	}

	public function delete($id) {

		$this->_delete($id);

	}

	public function view($id) {

		$this->_view($id);

	}

	public function ajax_save() {

		$this->_ajax_save();

	}

}