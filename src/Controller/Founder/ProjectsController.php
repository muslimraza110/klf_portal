<?php

namespace App\Controller\Founder;

class ProjectsController extends \App\Controller\ProjectsController {

	public function index() {

		$this->_index();

	}

	public function add($id = null) {

		$this->_add($id);

	}

	public function edit($id) {

		$this->_edit($id);

	}

	public function delete($id) {

		$this->_delete($id);

	}

	public function view($id) {

		$this->_view($id);

	}

	public function khoja_cares() {

		$this->_khoja_cares();

	}

	public function ajax_html_user_list() {

		$this->_ajax_html_user_list();

	}

	public function ajax_html_group_list() {

		$this->_ajax_html_group_list();

	}

	public function ajax_add() {

		$this->_ajax_add();

	}

	public function ajax_remove() {

		$this->_ajax_remove();

	}

	public function ajax_remove_note() {

		$this->_ajax_remove_note();

	}

	public function ajax_project_access() {

		$this->_ajax_project_access();

	}

}