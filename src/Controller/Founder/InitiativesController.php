<?php
namespace App\Controller\Founder;

class InitiativesController extends \App\Controller\InitiativesController {

	public function index() {

		$this->_index();

	}

	public function stepInitiative() {

		$this->_stepInitiative();

	}

	public function stepProjects() {

		$this->_stepProjects();

	}

	public function stepDonations() {

		$this->_stepDonations();

	}

	public function stepFundraisings() {

		$this->_stepFundraisings();

	}

	public function stepSummary() {

		$this->_stepSummary();

	}

	public function emailPreview() {

		$this->_emailPreview();

	}

	public function view($id) {

		$this->_view($id);

	}


}