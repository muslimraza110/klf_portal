<?php
namespace App\Controller\Founder;

class GroupsController extends \App\Controller\GroupsController {
	
	public function index() {
		$this->_index();
	}
	
	public function view($id) {
		$this->_view($id);
	}
	
	public function add() {
		$this->_add();
	}
	
	public function edit($id) {
		$this->_edit($id);
	}
	
	public function upload_image($id, $imageType) {
		$this->_upload_image($id, $imageType);
	}
	
	public function ajax_reset_image() {

		$this->_ajax_reset_image();

	}

	public function ajax_delete_image() {

		$this->_ajax_delete_image();

	}

	public function delete($id) {
		$this->_delete($id);
	}

	public function ajax_html_member_list() {

		$this->_ajax_html_member_list();

	}

	public function ajax_add_member() {

		$this->_ajax_add_member();

	}

	public function ajax_group_access() {

		$this->_ajax_group_access();

	}

	public function ajax_group_admin() {

		$this->_ajax_group_admin();

	}


  public function ajax_leave_group() {

		$this->_ajax_leave_group();

	}

}
