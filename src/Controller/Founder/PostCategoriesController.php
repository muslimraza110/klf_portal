<?php
namespace App\Controller\Founder;

class CategoriesController extends \App\Controller\CategoriesController {

	public function index() {

		$this->_index();

	}

	public function add($parentId) {

		$this->_add($parentId);

	}

	public function edit($id) {

		$this->_edit($id);

	}

	public function delete($id) {

		$this->_delete($id);

	}

}