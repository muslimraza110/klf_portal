<?php

namespace App\Controller\Founder;

class UserRecommendationsController extends \App\Controller\UserRecommendationsController {

	public function index() {

		$this->_index();

	}

	public function view($id) {

		$this->_view($id);

	}

	public function ajax_html_user_list() {

		$this->_ajax_html_user_list();

	}

	public function ajax_add_user() {

		$this->_ajax_add_user();

	}

	public function ajax_remove_user() {

		$this->_ajax_remove_user();

	}

	public function recommend() {

		$this->_recommend();

	}

}