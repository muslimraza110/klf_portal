<?php

namespace App\Controller\Founder;

class TagsController extends \App\Controller\TagsController {

	public function index() {

		$this->_index();

	}

	public function add() {

		$this->_add();

	}

	public function edit($id) {

		$this->_edit($id);

	}

	public function delete($id) {

		$this->_delete($id);

	}

}