<?php
namespace App\Controller;

class NotificationExcludesController extends \App\Controller\AppController {

	protected function _ajax_add() {
		$this->autoRender = false;
		$response = [];

		if (! empty($this->request->data['model_id']) && ! empty($this->request->data['model'])) {

			$notificationExclude = $this->NotificationExcludes->find()
				->where([
					'NotificationExcludes.user_id' => $this->_authUser->id,
					'NotificationExcludes.model_id' => $this->request->data['model_id'],
					'NotificationExcludes.model' => $this->request->data['model']
				])
				->first();

			if (empty($notificationExclude)) {
				$notificationExclude = $this->NotificationExcludes->newEntity([
					'user_id' => $this->_authUser->id,
					'model_id' => $this->request->data['model_id'],
					'model' => $this->request->data['model']
				]);

				if ($this->NotificationExcludes->save($notificationExclude)) {
					$response['success'] = 'success';
				}
				else {
					$response['error'] = 'error';
				}
			}
			else {
				$response['success'] = 'success';
			}
		}

		$this->response->type('json');
	  $this->response->body(json_encode($response));
	}

}