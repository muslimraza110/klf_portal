<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class KhojaCareCategoriesController extends AppController {

	protected function _index() {

		$khojaCareCategories = $this->KhojaCareCategories->find()
			->orderAsc('KhojaCareCategories.position')
			->where(['KhojaCareCategories.status' => 'A']);

		$title = 'Khoja Care Categories';

		$this->set(compact('khojaCareCategories', 'title'));

		$this->render(DS . 'KhojaCareCategories' . DS . 'index');

	}


	protected function _view($id) {

		$khojaCareCategory = $this->KhojaCareCategories->get($id);

		$this->set(compact('khojaCareCategories'));

	}

	protected function _add() {

		$khojaCareCategory = $this->KhojaCareCategories->newEntity();

		$title = 'New Khoja Care Category';

		$this->set(compact('title'));

		$this->_form($khojaCareCategory);

	}

	protected function _edit($id) {

		$khojaCareCategory = $this->KhojaCareCategories->get($id);

		$title = 'Edit Khoja Care Category';

		$this->set(compact('title'));

		$this->_form($khojaCareCategory);

	}

	protected function _form($khojaCareCategory) {

		if ($this->request->is(['post', 'put'])) {

			$khojaCareCategory = $this->KhojaCareCategories->patchEntity($khojaCareCategory, $this->request->data);
			if ($this->KhojaCareCategories->save($khojaCareCategory)) {
				$this->Flash->success(__('The khoja care category has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The khoja care category could not be saved. Please, try again.'));
			}

		}


		$breadcrumbs = [
			['name' => 'Khoja Cares Categories', 'route' => ['action' => 'index']],
		];

		$this->set(compact('khojaCareCategory', 'breadcrumbs'));
		$this->render(DS . 'KhojaCareCategories' . DS . '_form');


	}

	protected function _delete($id) {

		$this->request->allowMethod(['post']);

		if ($this->_authUser->role != 'A') {
			throw new NotFoundException;
		}

		$khojaCareCategory = $this->KhojaCareCategories->get($id);

		$khojaCareCategory->status = 'D';

		if ($this->KhojaCareCategories->save($khojaCareCategory)) {
			$this->Flash->success(__('The khoja care category has been deleted.'));
		} else {
			$this->Flash->error(__('The khoja care category could not be deleted. Please, try again.'));
		}

		return $this->redirect(['action' => 'index']);

	}

	protected function _ajax_reorder_khojaCareCategory_list() {

		$this->autoRender = false;

		if (!empty($newPositions = $this->request->data['positions'])) {

			$counter = 0;
			foreach ($newPositions as $newPosition) {
				$counter++;
				$khojaCareCategory = $this->KhojaCareCategories->get($newPosition);
				$khojaCareCategory = $this->KhojaCareCategories->patchEntity($khojaCareCategory, ['position' => $counter]);
				$this->KhojaCareCategories->save($khojaCareCategory);
			}

		}

	}
}
