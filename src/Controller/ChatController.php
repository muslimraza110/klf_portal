<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\InternalErrorException;
use Cake\Network\Exception\ServiceUnavailableException;

class ChatController extends \App\Controller\AppController {
  
  public function chat_refresh() {
    $this->request->allowMethod(['post']);
    $this->response->disableCache();
    
    if(Configure::read('debug'))
      throw new ServiceUnavailableException('The chat functionnality is deactivated when DEBUG = TRUE');
    else if ($this->request->session()->check('previousLogin'))
      throw new ForbiddenException('The chat functionnality is deactivated when an administrator login as someone else.');
    else if (!$this->_authUser || !isset($this->_authUser->id)) {
      throw new ForbiddenException('You are not logged in anymore...');
    }
    
    if (!isset($this->request->data['action']))
      throw new InternalErrorException('The action is missing.');
    
    $this->loadModel('Users');
    Time::setJsonEncodeFormat('YYYY-MM-dd HH:mm:ssZ');
    
    switch ($this->request->data['action']) {
      case 'change_status':
        $this->_change_status();
        break;
      case 'get_session':
        $this->_get_session();
        break;
			case 'close_session':
				$this->_close_session('CLOSE');
				break;
			case 'reopen_session':
				$this->_close_session('OPEN');
				break;
			case 'minimize_session':
				$this->_close_session('MINIMIZE');
				break;
			case 'maximize_session':
				$this->_close_session('MAXIMIZE');
				break;
			case 'mark_as_read':
				$this->_mark_messages_as_read();
				break;
      case 'send_message':
        $this->_send_message();
        break;
      case 'refresh':
      default:
        $this->_refresh();
        break;
    }
  }
  
  protected function _refresh() {
    $lastRefreshTime = $this->_authUser->last_online;
    $this->_authUser = $this->_update_last_online();
    
    $online_max_limit = new Time('30 seconds ago');
    
		//get users online
    $usersOnline = $this->Users->find()
      ->where([
        'Users.chat_status' => 'A',
        'Users.last_online >=' => $online_max_limit,
        'Users.id !=' => $this->_authUser->id
      ])
      ->contain([
        'Contacts.Profile'
      ])
			->toArray();
		
		//check for new messages since last refresh
    $sessions = $this->Users->ChatSessions->find()
      ->matching('Users', function ($q) {
        return $q->where([
          'Users.id' => $this->_authUser->id
        ]);
      })
      ->matching('ChatInteractions', function ($q) use ($lastRefreshTime) {
        return $q->where([
          'ChatInteractions.created >=' => $lastRefreshTime
        ]);
      })
      ->contain([
        'ChatInteractions' => function ($q) {
          return $q
						->order(['ChatInteractions.created' => 'DESC'])
						->limit(15);
        },
        'Users' => function ($q) {
          return $q->contain(['Contacts.Profile']);
        },
      ])
			->all();
		
		//if new messages in session, mark session as open
		if($sessions) {
			$sessionIds = [];
			
			foreach($sessions as $session) {
				
				$data = [];
				foreach($session->users as $key => $user) {
					$data['users'][$key]['id'] = $user->id;
					
					if($user->id == $this->_authUser->id)
						$data['users'][$key]['_joinData']['open'] = Time::now();
				}
				$this->Users->ChatSessions->patchEntity($session, $data, ['associated' => ['Users._joinData']]);
				$this->Users->ChatSessions->save($session);
				
				$sessionIds[] = $session->id;
			}
		}
		
		//check if chat sessions should be reopened
		$openedSessions = [];
		if(isset($this->request->data['firstRequest']) && $this->request->data['firstRequest']) {
			
			$openedSessions = $this->Users->ChatSessions->find()
				->matching('Users', function ($q) {
					return $q->where([
						'Users.id' => $this->_authUser->id
					]);
				})
				->where([
					'ChatSessionsUsers.open IS NOT' => NULL
				])
				->contain([
					'ChatInteractions' => function ($q) {
						return $q->limit(50);
					},
					'Users' => function ($q) {
						return $q->contain(['Contacts.Profile']);
					},
				]);
			
			if(!empty($sessionIds)) {
				$openedSessions->andWhere(['ChatSessions.id NOT IN' => $sessionIds]);
			}
			
			$openedSessions->all();
		}
    
    $this->set('userEntity', $this->_authUser);
    $this->set(compact('usersOnline', 'sessions', 'openedSessions'));
    $this->set('_serialize', ['userEntity', 'usersOnline', 'sessions', 'openedSessions']);
  }
  
  protected function _update_last_online() {
    $this->_authUser->last_online = Time::now();
    $this->_authUser->dirty('modified', true);
    
    if ($this->Users->save($this->_authUser))
      return $this->_authUser;
    else
      throw new InternalErrorException('The changes to the user could not be saved.');
  }
  
  protected function _change_status() {
    
    if (!isset($this->request->data['status']))
      throw new InternalErrorException('The status code is missing.');
    
    $this->_authUser->chat_status = $this->request->data['status'];
    $this->_authUser->dirty('modified', true);
    
    if (!$this->Users->save($this->_authUser)) {
      throw new InternalErrorException('The changes to the user could not be saved.');
    }
    
    $this->set('userEntity', $this->_authUser);
    $this->set('_serialize', ['userEntity']);
  }
  
  protected function _get_session() {
    
    if (!isset($this->request->data['user_ids']))
      throw new InternalErrorException('The session user ids are missing.');
    
    $sessionUserIds = $this->request->data['user_ids'];
    $sessionUserIds[] = $this->_authUser->id;
		
    $chatSession = $this->Users->ChatSessions->getChatSession($sessionUserIds);
    
    if (empty($chatSession)) {
      $chatSession = $this->Users->ChatSessions->newEntity([
        'total_authUsers' => count($sessionUserIds),
        'chat_interactions' => [],
        'users' => [
          '_ids' => $sessionUserIds
        ],
      ], ['associated' => ['Users', 'ChatInteractions']]);
    }
		
		$data = [];
		foreach($chatSession->users as $key => $user) {
			$data['users'][$key]['id'] = $user->id;
			
			if($user->id == $this->_authUser->id) {
				$data['users'][$key]['_joinData']['open'] = Time::now();
				$data['users'][$key]['_joinData']['minimized'] = 0;
			}
		}
		$this->Users->ChatSessions->patchEntity($chatSession, $data, ['associated' => ['Users._joinData']]);
		$this->Users->ChatSessions->save($chatSession);
    
    $interactions = $chatSession->chat_interactions;
    $interactions = array_reverse($interactions);
    unset($chatSession->chat_interactions);
    
    $this->set('session', $chatSession);
    $this->set(compact('interactions'));
    $this->set('_serialize', ['session', 'interactions']);
  }
	
	protected function _close_session($action = 'CLOSE') {
		
		if (!isset($this->request->data['chat_session_id']))
      throw new InternalErrorException('The session data is missing.');
		
		$chatSession = $this->Users->ChatSessions->findById($this->request->data['chat_session_id'])
			->contain(['Users'])
			->first();
		
		$data = [];
		foreach($chatSession->users as $key => $user) {
			$data['users'][$key]['id'] = $user->id;
			
			if($user->id == $this->_authUser->id) {
				switch($action) {
					case 'MINIMIZE':
					case 'MAXIMIZE':
						$data['users'][$key]['_joinData']['minimized'] = ($action == 'MAXIMIZE') ? 0 : 1;
						break;
					
					default:
					case 'CLOSE':
					case 'OPEN':
						if($action == 'OPEN') {
							$data['users'][$key]['_joinData']['minimized'] = 0;
							$data['users'][$key]['_joinData']['open'] = Time::now();
						}
						else
							$data['users'][$key]['_joinData']['open'] = NULL;
						
						break;
				}
			}
		}
		$this->Users->ChatSessions->patchEntity($chatSession, $data, ['associated' => ['Users._joinData']]);
		
		if($this->Users->ChatSessions->save($chatSession)) {
			$this->set('auth_user_join_data', $chatSession->auth_user_join_data);
			$this->set('_serialize', ['auth_user_join_data']);
		}
		else {
			throw new InternalErrorException('Could not save session changes...');
		}
	}
  
  protected function _send_message() {
    
    if (!isset($this->request->data['data']))
      throw new InternalErrorException('The needed data is missing.');
    
    if (isset($this->request->data['data']['chat_session']['created'])) //date fields cause problems
      unset($this->request->data['data']['chat_session']['created']);
    
    //only keep relationship to user, not data in case of overwrite
    if (isset($this->request->data['data']['chat_session']['users'])) {
      foreach ($this->request->data['data']['chat_session']['users'] as $key => $user) {
        $this->request->data['data']['chat_session']['users'][$key] = ['id' => $user['id']];
      }
    }
    
    $chatInteraction = $this->Users->ChatInteractions->newEntity(['type' => 'M']);
    $chatInteraction = $this->Users->ChatInteractions->patchEntity($chatInteraction, $this->request->data['data'], [
      'associated' => ['ChatSessions.Users']
    ]);
    
    if ($this->Users->ChatInteractions->save($chatInteraction)) {
      $this->_refresh();
    }
    else {
      throw new InternalErrorException('Cannot save chat interaction');
    }
  }
	
	protected function _mark_messages_as_read() {
		
		if (!isset($this->request->data['chat_session_id']))
      throw new InternalErrorException('The session data is missing.');
		
		if (!$this->Users->ChatSessions->exists($this->request->data['chat_session_id']))
      throw new InternalErrorException('The chat session does not exist.');
		
		/*
		//NOT GOOD, MUST TRACK IF READ PER USER (WONT WORK WITH MULTIPLE USER)
		$bulkUpdate = $this->Users->ChatInteractions->updateAll(
			['been_read' => true], // update to record
			['chat_session_id' => $this->request->data['chat_session_id']] // conditions
		);
		
		if($bulkUpdate) {
			$this->set('_serialize', ['nothing']);
		}
		else {
			throw new InternalErrorException('Could not save session changes...');
		}
		*/
		
		$this->set('_serialize', ['nothing']);
	}
 
}