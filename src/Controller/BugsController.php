<?php
namespace App\Controller;

use Cake\Network\Exception\NotFoundException;
use Cake\I18n\Time;

class BugsController extends AppController {

	protected function _index() {
		$this->loadModel('Commits');

		$commits = $this->Commits->find()
			->order('created DESC')
			->limit(5);

		$bugCategories = $this->Bugs->BugCategories->find('list')->toArray();
		$condition = [];

		if ($this->request->is(['get']) && !empty($this->request->query['bug_category_id'])) {
			$condition['bug_category_id'] = $this->request->data['bug_category_id'] = $this->request->query['bug_category_id'];
		}
		else {
			$condition['bug_category_id IS NOT'] = $this->request->data['bug_category_id'] = NULL;
		}

		if ($this->request->is(['get']) && !empty($this->request->query['user_id'])) {
			$condition['user_id'] = $this->request->data['user_id'] = $this->request->query['user_id'];
		}


		$openBugs = $this->Bugs->find()
			->contain([
				'Users',
				'BugCategories'
			])
			->where([
				'completed IS' => null,
				'committed IS' => null,
				'verified IS' => null,
				$condition
			]);

		$seenBugs = $this->Bugs->find()
			->contain([
				'Users',
				'BugCategories'
			])
			->where([
				'committed is not' => null,
				$condition
			]);

		if ($this->request->is(['post', 'put'])) {
			
			if (isset($this->request->data['complete'])) {
				foreach ($seenBugs as $seenBug) {

					if (!empty($seenBug->committed) && empty($seenBug->completed)) {

						$seenBug->completed = Time::now();
						$this->Bugs->save($seenBug);
					}
				}
			}
			else if (isset($this->request->data['sandbox'])) {

				$committedBugs = $this->Bugs->find()
					->where([
						'committed is not' => null,
						'completed IS' => null,
						'verified IS' => null
					])
					->extract('id')
					->toArray();

				if (empty($committedBugs))
					$committedBugs = ['Changes not listed'];

				$commit = $this->Commits->newEntity([
					'type' => 'S',
					'bug_nums' => json_encode($committedBugs)
				]);

				$this->Commits->save($commit);

			}

			else if (isset($this->request->data['portal'])) {

				$committedBugs = $this->Bugs->find()
					->where([
						'committed is not' => null,
						'completed IS' => null,
						'verified IS' => null
					])
					->extract('id')
					->toArray();

				if (empty($committedBugs))
					$committedBugs = ['Changes not listed'];

				$commit = $this->Commits->newEntity([
					'type' => 'P',
					'bug_nums' => json_encode($committedBugs)
				]);

				$this->Commits->save($commit);

			}
		}

		$openBugs = $openBugs->groupBy('priority')->toArray();
		$commitTypes = $this->Commits->types();
		$users = $this->Bugs->Users->find('list')
			->contain('Bugs')
			->innerJoinWith('Bugs');

		$priorities = $this->Bugs->priorities();
		$labels = $this->Bugs->labels();
		$statuses = $this->Bugs->statuses();

		$this->set(compact('openBugs', 'seenBugs', 'priorities', 'labels', 'bugCategories', 'bugCategory', 'statuses', 'commitTypes', 'commits', 'users'));
		$this->render('/Bugs/index');
	}

	protected function _add() {

		$bug = $this->Bugs->newEntity([
			'user_id' => $this->Auth->user('id')
		]);


		$header['title'] = 'Add Bug';

		$header['breadcrumbs'] = [
			['name' => 'Bugs', 'route' => ['controller' => 'Bugs', 'action' => 'index']],
		];

		$this->_form($bug, $header);
	}

	protected function _edit($id) {

		if (! $this->Bugs->exists($id))
			throw new NotFoundException();

		$bug = $this->Bugs->findById($id)->first();

		$header['title'] = 'Edit Bug';

		$header['breadcrumbs'] = [
			['name' => 'Bugs', 'route' => ['controller' => 'Bugs', 'action' => 'index']],
		];
		$this->_form($bug, $header);
	}

	public function _form($bug, $header) {

		$priorities = $this->Bugs->priorities();

		if ($this->request->is(['post', 'put'])) {

			$bug = $this->Bugs->patchEntity($bug, $this->request->data);

			if (isset($this->request->data['commit'])) {
				$bug->committed = Time::now();
			}

			if (empty($this->request->data['complete'])) {
				$bug->completed = null;
			}
			elseif (empty($bug->completed)) {
				$bug->completed = Time::now();
			}

			if ($this->Bugs->save($bug)) {

				if (isset($this->request->data['upload-file'])) {
					if (!empty($this->request->data['upload']) && $this->request->data['upload']['error'] == 0) {

						$this->Bugs->Assets->awsUploadFile('Bugs', $bug->id, $this->request->data['upload']);

					}
					else if ($this->request->data['upload']['error'] == 1)
						$this->Flash->error('Max File size exceeded.');
					else if ($this->request->data['upload']['error'] == 7)
						$this->Flash->error('Failed to write file to disk.');

					return $this->redirect(['action' => 'edit', $bug->id]);

				}
				else {
					$this->Flash->success('Bug has been saved successfully.');
					return $this->redirect(['action' => 'index']);
				}

			}
			else {
				$this->Flash->error('An error has occurred.');
			}
		}

		$bugCategories = $this->Bugs->BugCategories->find('list');

		$assets = $this->Bugs->Assets->find()->where([
			'model_id' => $bug->id,
			'association' => 'Bugs'
		]);

		$bucket = $this->Bugs->Assets->s3Bucket();
		
		$this->set(compact('priorities', 'bug', 'header', 'bugCategories', 'assets', 'bucket'));
		$this->render('/Bugs/_form');
	}



	public function delete($id) {
		$this->autoRender = false;

		if (! $this->Bugs->exists($id))
			throw new NotFoundException();

		$bug = $this->Bugs->find()
			->where(['Bugs.id' => $id])
			->first();
		$this->Bugs->delete($bug);

		$this->Flash->success('Bug has been deleted.');
		$this->redirect(['action' => 'index']);
	}

	protected function _updateStatus($id) {
		$this->autoRender = false;

		$bug = $this->Bugs->find()
			->where([
					'id' => $id
				])
			->first();
		

		if (empty($bug->committed))
			$bug->committed = Time::now();
		else if (!empty($bug->completed) && empty($bug->verified))
			$bug->verified = Time::now();

		$this->Bugs->save($bug);

		$this->Flash->success('Bug status has been updated.');
		$this->redirect(['action' => 'index', '?' => $this->request->query]);

	}

	protected function _returnPending($id) {
		$this->autoRender = false;

		$bug = $this->Bugs->find()
			->where([
				'id' => $id
			])
			->first();

		$bug->committed = NULL;
		$bug->priority = 6;

		$this->Bugs->save($bug);

		$this->Flash->success('Bug status has returned to pending.');
		$this->redirect(['action' => 'index']);
	}

	protected function _complete($id) {
		$this->autoRender = false;

		$bug = $this->Bugs->find()
			->where([
				'id' => $id
			])
			->first();

		$bug->completed = Time::now();

		$this->Bugs->save($bug);

		$this->Flash->success('Bug has been completed.');
		$this->redirect(['action' => 'index']);
	}

	protected function _feedback() {

		$newEntity = $this->Bugs->newEntity([
			'user_id' => $this->_authUser->id,
			'priority' => 5
		]);

		$bug = clone $newEntity;

		if ($this->request->is(['post', 'put'])) {

			$this->Bugs->patchEntity($bug, $this->request->data);

			if ($this->Bugs->save($bug)) {

				$this->Flash->success('Feedback has been submitted.');

				$this->request->data = [];

				$bug = clone $newEntity;

			} else
				$this->Flash->error('An error occurred. Feedback could not be submitted.');

		}

		$titleIcon = $this->_icons['comments'];

		$this->set(compact('titleIcon', 'bug'));

		$this->render('/Bugs/_feedback');

	}

}
