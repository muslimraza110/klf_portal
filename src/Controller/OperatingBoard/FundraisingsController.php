<?php
namespace App\Controller\OperatingBoard;

class FundraisingsController extends \App\Controller\FundraisingsController {

	public function index() {

		$this->_index();

	}

	public function add() {

		$this->_add();

	}

	public function edit($id) {

		$this->_edit($id);

	}

	public function delete($id) {

		$this->_delete($id);

	}

	public function archive($id) {

		$this->_archive($id);

	}

	public function view($id) {

		$this->_view($id);

	}

	public function pledge($id) {

		$this->_pledge($id);

	}

	public function my_pledges() {

		$this->_my_pledges();

	}


	public function edit_pledge($id) {

		$this->_edit_pledge($id);

	}


}
