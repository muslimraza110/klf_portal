<?php
namespace App\Controller\OperatingBoard;

class FormsController extends \App\Controller\FormsController {

	public function index() {
		$this->_index();
	}

	public function view($id) {
		$this->_view($id);
	}

	public function print_form($id) {
		$this->_print_form($id);
	}

	public function save_answers($id) {
		$this->_save_answers($id);
	}

	public function upload_form($id) {
		$this->_upload_form($id);
	}

	public function download($id) {
		$this->_download($id);
	}

	public function add_post($id) {
		$this->_add_post($id);
	}

	//---

	public function stats($id) {

		$this->_stats($id);

	}

	public function remind_unanswered($id) {

		$this->_remind_unanswered($id);

	}

	public function user_answers($user_form_id) {

		$this->_user_answers($user_form_id);

	}

	public function add() {

		$this->_add();

	}

	public function edit($id) {

		$this->_edit($id);

	}

	public function duplicate($id) {

		$this->_duplicate($id);

	}

	public function fields($id) {

		$this->_fields($id);

	}

	public function publish($id) {

		$this->_publish($id);

	}

	public function delete($id, $restore = false) {

		$this->_delete($id, $restore);

	}

	public function trash($id = null) {

		$this->_trash($id);

	}

}
