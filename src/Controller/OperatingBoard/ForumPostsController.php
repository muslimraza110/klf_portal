<?php

namespace App\Controller\OperatingBoard;

class ForumPostsController extends \App\Controller\ForumPostsController {

	public function index($scope = 'public') {

		$this->_index($scope);

	}

	public function view($id) {

		$this->_view($id);

	}

	public function ajax_add_comment() {

		$this->_ajax_add_comment();

	}

	public function add($scope = null, $scopeId = null) {
		$this->_add($scope, $scopeId);
	}

	public function edit($id) {

		$this->_edit($id);

	}

	public function delete($id) {

		$this->_delete($id);

	}

	public function approve($id) {

		$this->_approve($id);

	}

	public function add_subtopic($model) {
		$this->_add_subtopic($model);
	}

	public function view_subtopic($id) {
		$this->_view_subtopic($id);
	}

	public function add_subtopic_post($id) {
		$this->_add_subtopic_post($id);
	}

	public function ajax_html_moderator_list() {

		$this->_ajax_html_moderator_list();

	}

	public function ajax_add_moderator() {

		$this->_ajax_add_moderator();

	}

	public function ajax_remove_moderator() {

		$this->_ajax_remove_moderator();

	}

	public function ajax_toggle_subscription() {
		$this->_ajax_toggle_subscription();
	}

}