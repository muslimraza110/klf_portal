<?php
namespace App\Controller\OperatingBoard;

class MessagesController extends \App\Controller\MessagesController 
{
	public function index($type = "inbox") 
	{
		$this->_index($type);	
	}

	public function add()
	{
		$this->_add();
	}

	public function view($id)
	{
		$this->_view($id);
	}

	public function view_sent($id)
	{
		$this->_view_sent($id);
	}

	public function view_archive($id) 
	{
		$this->_view_archive($id);
	}

	public function move_to_archive($id)
	{
		$this->_move_to_archive($id);
	}

	public function remove_from_archive($id)
	{
		$this->_remove_from_archive($id);
	}

	public function mark_as_read($id)
	{
		$this->_mark_as_read($id);
	}

	public function mark_as_not_read($id)
	{
		$this->_mark_as_not_read($id);
	}

	public function delete($id)
	{
		$this->_delete($id);
	}

	public function reply($id, $all = false) 
	{
		$this->_reply($id);
	}

	public function edit($id)
	{
		$this->_edit($id);
	}

	public function ajax_autocomplete_recipients() 
	{
		$this->_ajax_autocomplete_recipients();
	}

	public function ajax_important_tag()
	{
		$this->_ajax_important_tag();
	}
}