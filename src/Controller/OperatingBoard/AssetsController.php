<?php

namespace App\Controller\OperatingBoard;

class AssetsController extends \App\Controller\AssetsController {

	public function view($fileId) {

		$this->_view($fileId);

	}

	public function download($fileId) {

		$this->_download($fileId);

	}

	public function delete($fileId) {

		$this->_delete($fileId);

	}

}