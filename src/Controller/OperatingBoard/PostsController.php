<?php
namespace App\Controller\OperatingBoard;

use Cake\Network\Exception\NotFoundException;
use Cake\I18n\Time;

class PostsController extends \App\Controller\PostsController {

  public function index($categoryId) {
    $this->_index($categoryId);
  }

  public function view($id) {
  	$this->_view($id);
  }

  public function download($id) {
  	$this->_download($id);
  }

	public function add($categoryId) {
		$this->_add($categoryId);
	}

	public function edit($id) {
		$this->_edit($id);
	}

	public function publish($id) {

		$this->_publish($id);

	}

	public function unpublish($id) {

		$this->_unpublish($id);

	}

  public function archive($id) {

    $this->_archive($id);

  }

	public function delete($id, $restore = false) {

		$this->_delete($id, $restore);

	}

	public function trash($id = null) {

		$this->_trash($id);

	}

	public function upload_image($id, $imageType) {
		$this->_upload_image($id, $imageType);
	}

	public function ajax_reset_image() {

		$this->_ajax_reset_image();

	}

	public function ajax_delete_image() {

		$this->_ajax_delete_image();

	}

}
