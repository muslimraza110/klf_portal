<?php
namespace App\Controller;

use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\Collection\Collection;

class PostsController extends \App\Controller\AppController {

  protected function _index($categoryId) {

		$category = $this->Posts->Categories->get($categoryId);

		if (!$category)
 			throw new NotFoundException();

		$this->set('title', $category->name);

		if ($categoryId == 1) {

			$conditions = [
				'Posts.category_id' => 1
			];

		} else {

			$conditions = [
				'Posts.category_id' => $categoryId
			];

		}

		if (!empty($archived = $this->request->query('archived'))) {
			$this->request->data['archived'] = $archived;
			$conditions['archived'] = $archived;
		}

		if (!in_array($this->_authUser->role, ['A', 'O', 'R', 'W'])) {
			$conditions['CAST(Posts.published AS date) <='] = Time::now();
		}

    if (!in_array($this->_authUser->role, ['A', 'O', 'R', 'W'])) {
			$noAdd = true;
			$this->set(compact('noAdd'));
		}

		$this->paginate = [
			'limit' => 25,
			'sortWhitelist' => [
				'Posts.name', 'Posts.published', 'Posts.topic'
			]
		];


		$query = $this->Posts->find()
			->where($conditions)
			->contain([
				'Users',
				'FeaturedImg'
			]);
			// ->orderDesc('Posts.published');


		$posts = $this->paginate($query);

		$statusSelect = $this->Posts->getViewStatus();

		$this->set(compact('posts', 'category', 'categoryId', 'statusSelect'));

		// if ($this->_authUser->role == 'F') {
		// 	$hideTitle = true;
		// 	$this->set(compact('hideTitle'));
		// }
		if ($categoryId == 1) {

			$conditions = [
				'Posts.category_id IN ' => $this->Posts->Categories->newsCategories(),
				'Posts.published IS NOT' => null,
				'Posts.published <=' => Time::now(),
				'Posts.archived' => false
			];

			$slides = $this->Posts->find()
				->where($conditions)
				->contain(['FeaturedImg'])
				->limit(25);

			$this->set(compact('slides'));

			$this->render('/Posts/_index_news');
		}
		else if($categoryId == 4) {

			$groupedPosts = (new Collection($this->Posts->find()
				->contain([
					'Users'
				])
				->where([
					'Posts.category_id' => 4
				])))
				->sortBy('topic', SORT_ASC, SORT_STRING)
				->groupBy('topic')
				->each(function ($posts, $topic) {
					(new Collection($posts))->sortBy('name');
				});

			$this->set(compact('groupedPosts'));
			$this->render('/Posts/_index_default');
		}
	}

	protected function _download($id) {

		$this->autoRender = false;

		$post = $this->Posts->findById($id)
			->contain([
				'PostUpload'
			])
			->first();

		if (!$post)
 			throw new NotFoundException('The file could not be found.');

		$this->response->file(
			$post->post_upload->path(),
			[
				'download' => true,
				'name' => $post->post_upload->name
			]
		);

	}

	protected function _setTitleIcon($category) {

		$iconCategoryId = $category->id;

		if (!is_null($category->parent_id))
			$iconCategoryId = $category->parent_id;

		switch($iconCategoryId) {
			case 1:
				$titleIcon = $this->_icons['list'];
				break;
			case 2:
				$titleIcon = $this->_icons['list'];
				break;
		}

		$this->set(compact('titleIcon'));
	}

  protected function _view($id) {

  	if (! $this->Posts->exists($id))
 			throw new NotFoundException();

  	$post = $this->Posts->findById($id)
  		->contain(['FeaturedImg'])
  		->first();

  	$categoryId = $post->category_id;
  	$categoryName = $this->Posts->Categories->findById($categoryId)->first()->name;

  	$title = $post->name;
		$hideTitle = true;

		$relatedPosts = $post->getRelatedPosts();

		foreach($relatedPosts as $relatedPost) {

			if ($relatedPost['model'] != 'Forms')
				continue;

			$this->request->data['forms'][$relatedPost['data']->id] = $relatedPost['data']->getUserAnswers($this->_authUser->id);

			if ($relatedPost['data']->isCompleted())
				$relatedPost['data']->disabled = true;
		}

		$breadcrumbs = [
			['name' => $categoryName, 'route' => ['action' => 'index', $categoryId]],
			['name' => $title, 'active' => true]
		];


		$previous = $this->Posts->find()
			->where([
				'Posts.published >' => $post->published,
				'category_id' => 1
			])
			->orderAsc('Posts.published')
			->first();

		$next = $this->Posts->find()
			->where([
				'Posts.published <' => $post->published,
				'category_id' => 1
			])
			->orderDesc('Posts.published')
			->first();

  	$this->set(compact('title', 'hideTitle', 'breadcrumbs', 'categoryId', 'id', 'post', 'relatedPosts', 'previous', 'next'));

  	$this->render('/Posts/_view');
  }

  protected function _add($categoryId) {

		$category = $this->Posts->Categories->findById($categoryId)->first();

		if (!$category)
			throw new NotFoundException('The category could not be found.');

		$title = 'New Post';
		$breadcrumbs[] = ['name' => $category->name, 'route' => ['action' => 'index', $category->id]];
		$this->set(compact('title', 'breadcrumbs'));

		$post = $this->Posts->newEntity([
			'user_id' => $this->_authUser->id,
			'category_id' => $categoryId
		]);

		$this->_form($post);
	}

	protected function _edit($id) {

		$post = $this->Posts->findById($id)
			->contain([
				'Categories',
				'PostUpload'
			])
			->first();

		if (!$post)
			throw new NotFoundException('The post could not be found.');

		$post->updateRelatedPostJson();

		$title = 'Edit Post';
		$breadcrumbs[] = ['name' => $post->category->name, 'route' => ['action' => 'index', $post->category_id]];
		$this->set(compact('title', 'breadcrumbs'));

		$this->_form($post);
	}

	protected function _form($post) {

  	$this->loadModel('Settings');

		if ($this->request->is(['post', 'put'])) {

			if (isset($this->request->data['topic'])) {
				$this->request->data['topic'] = ucwords($this->request->data['topic']);
			}

			if (empty($this->request->data['post_upload']['upload']['name'])) {
				unset($this->request->data['post_upload']);
			}

			$previousPinnedPost = $this->Posts->find()
				->where([
					'Posts.pinned' => true
				])
				->first();

			if (!empty($previousPinnedPost) && $this->request->data['pinned']) {
				$previousPinnedPost->pinned = false;
				$this->Posts->save($previousPinnedPost);
			}

			$post = $this->Posts->patchEntity($post, $this->request->data, ['associated' => ['PostUpload']]);

			if ($this->Posts->save($post)) {
				$this->Flash->success(sprintf('The post "%s" has been saved.', $post->name));

					return $this->redirect(['action' => 'index', $post->category_id]);
			}
			else {
				$this->Flash->error('The post could not be saved. Please review the form for errors.');
			}
 		}

 		$categories = $this->Posts->Categories->find('list');
 		$postThemes = $this->Posts->PostThemes->find('list');
 		$setting = $this->Settings->get('FIXED_NEWS_ITEM');

		$topics = $this->Posts->find()
			->select(['topic'])
			->where([
				'Posts.topic IS NOT' => null,
				'Posts.topic !=' => ''
			])
			->distinct(['topic'])
			->extract('topic')
			->toArray();

 		$this->set(compact('post', 'categories', 'postThemes', 'topics', 'setting'));
 		$this->render('/Posts/_form');
	}

	protected function _publish($id) {
		$this->request->allowMethod('post');

		if(!$this->Posts->exists($id))
			throw new NotFoundException();

		$post = $this->Posts->get($id);
		$post->published = Time::now();

		if ($this->Posts->save($post))
			$this->Flash->success('The post "' . $post->name . '" has been published.');
		else
			$this->Flash->error('The post "' . $post->name . '" couldn\'t be published.');

		$this->redirect(['action' => 'view', $post->id]);
	}

	protected function _unpublish($id) {

		$this->request->allowMethod('post');

		$post = $this->Posts->get($id);

		$post->published = null;

		if ($this->Posts->save($post))
			$this->Flash->success('The post "'.$post->name.'" has been unpublished.');
		else
			$this->Flash->error('The post "'.$post->name.'" couldn\'t be unpublished.');

		$this->redirect(['action' => 'index', $post->category_id]);

	}

  public function _archive($id) {

		$this->request->allowMethod('post');

		$post = $this->Posts->get($id);

    if($post->archived) {
      $post->archived = false;
      $message = 'un';
    }else {
      $post->archived = true;
      $message = '';
    }

		if ($this->Posts->save($post))
			$this->Flash->success('The post "'.$post->name.'" has been '.$message.'archived.');
		else
			$this->Flash->error('The post "'.$post->name.'" couldn\'t be '.$message.'archived.');

		$this->redirect(['action' => 'index', $post->category_id]);

  }

	protected function _delete($id, $restore = false) {
		$this->request->allowMethod('post');

		$post = $this->Posts->get($id, ['statusCheck' => false]);


		if(!$post)
			throw new NotFoundException();

		if(!$restore) {
			$post->status = 'D';

			if ($this->Posts->save($post))
				$this->Flash->success('The post "' . $post->name . '" has been moved to the trash.');
			else
				$this->Flash->error('The post "' . $post->name . '" couldn\'t be moved to the trash.');
		}
		else {
			$post->status = 'A';

			if ($this->Posts->save($post))
				$this->Flash->success('The post "' . $post->name . '" has been restored.');
			else
				$this->Flash->error('The post "' . $post->name . '" couldn\'t be restored.');
		}

		$this->redirect(['action' => 'index', $post->category_id]);
	}

	protected function _trash($id = null) {

		if ($this->request->is('post')) {
			if(is_null($id)) {

				if ($this->Posts->deleteAll(['status' => 'D']))
					$this->Flash->success('The trash has been emptied.');
				else {
					$this->Flash->error('The posts could not be deleted.');
				}
			}
			else {

				$post = $this->Posts->get($id);

				if ($post && $post->status == 'D' && $this->Posts->delete($post))
					$this->Flash->success('The post "' . $post->name . '" has been deleted.');
				else
					$this->Flash->error('The post "' . $post->name . '" couldn\'t be deleted.');
			}

			return $this->redirect(['action' => 'trash']);
		}

		$query = $this->Posts->find('all', ['options' => ['checkStatus' => false]])
			->where(['Posts.status' => 'D'])
			->contain(['PostTypes', 'PostCategories'])
			->applyOptions(['statusCheck' => false]);

		$this->paginate = [
			'limit' => 25,
			'sortWhitelist' => [
				'name', 'published', 'PostCategories.name'
			]
		];

		$posts = $this->paginate($query);

		$titleIcon = $this->_icons['trash'];

			$this->set(compact('posts', 'titleIcon'));

		$this->render('/Posts/_trash');

	}


}
