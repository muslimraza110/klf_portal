<?php
namespace App\Controller;

use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\Collection\Collection;
use Cake\I18n\Number;

class GroupsController extends \App\Controller\AppController {

	protected function _index() {
		$this->set('title', 'Groups');

		$conditions = [];

		if (isset($this->request->query['q'])) {
			$this->request->data['q'] = $this->request->query['q'];
			$conditions['Groups.id IN'] = $this->Search->groupsQuery($this->request->query['q'])->select(['Groups.id']);
		}

		if (isset($this->request->query['letter'])) {
			$this->request->data['letter'] = $this->request->query['letter'];
			$conditions['Groups.name LIKE'] = $this->request->query['letter'] . '%';
		}

		$conditions = array_merge(
			$conditions,
			$this->Groups->buildUserRoleConditions($this->_authUser)
		);

		if (in_array($this->_authUser->role, ['A', 'O']) && !empty($pending = $this->request->query('pending'))) {

			$this->request->data['pending'] = $pending;

			$conditions['Groups.pending'] = true;

		}

		if (!empty($type = $this->request->query('type'))) {

			$this->request->data['type'] = $type;

			if ($type == 'C')
				$conditions['Groups.category_id'] = 3;
			else
				$conditions['Groups.category_id'] = 2;

		}

		if ($this->_authUser->role != 'A') {
			$conditions['Groups.passive'] = false;
		}

		$this->paginate = [
			'conditions' => $conditions,
			'contain' => [
				'ProfileImg',
				'Categories',
				'GroupRequests'
			],
			'limit' => 30
		];

		$groups = $this->paginate();

		$groupRequestStatuses = $this->Groups->GroupRequests->getStatuses();

		$this->set(compact('groups', 'type', 'groupRequestStatuses'));
		$this->render('/Groups/_index');
	}

	protected function _view($id) {

		$group = $this->Groups->getGroup($id);
		$projects = $group->projects;
		$relatedGroupsProjects = $group->getRelatedGroupsProjects();


		$projects = array_merge(is_array($projects) ? $projects : [] , is_array($relatedGroupsProjects) ? $relatedGroupsProjects : []);

		$projectEvents = [];
		$events = [];

		if (!empty($projects)) {
			$projectEvents = (new Collection($projects))
			->extract('events')->toArray()[0];
		}

		if (!empty($projectEvents) || !empty($group->events)) {

			$events = array_merge(is_array($projectEvents) ? $projectEvents : [], is_array($group->events) ? $group->events : []);

		}

		$events = !empty($events) ? (new Collection($events))->sortBy('start_date', SORT_DESC) : $events;


		$forumPosts = $group->forum_posts;


		if (!$group || !$group->hasPermission($this->_authUser))
			throw new NotFoundException('The group could not be found.');

		$memberIds = (new Collection($group->users))->extract('id')->toArray();

		$groupRequestCount = $this->Groups->GroupRequests->findByGroupIdAndUserId($id, $this->_authUser->id)
			->count();

		$requestGroupAccess = (!in_array($this->_authUser->id, $memberIds) && $groupRequestCount < 1);

		/*
		$vote = $votes = null;

		if ($this->_authUser->role == 'O')
			$vote = $this->Groups->Users->UserVotes
				->find('votes', [
					'entity_id' => $id,
					'voter_id' => $this->_authUser->id,
					'model' => 'Groups'
				])
				->first();

		if (in_array($this->_authUser->role, ['A', 'F']))
			$votes = $this->Groups->Users->UserVotes
				->find('votes', [
					'entity_id' => $id,
					'model' => 'Groups'
				]);
		*/

		$title = $group->name;
		$breadcrumbs[] = ['name' => 'Groups', 'route' => ['action' => 'index']];
		$this->set(compact('title', 'breadcrumbs'));

		$this->set(compact('group', 'requestGroupAccess', 'projects', 'memberIds', 'forumPosts', 'events'));
		$this->render('/Groups/_view');
	}

	protected function _add() {

		$group = $this->Groups->newEntity([
			'category_id' => $this->request->query('type') == 'C' ? 3 : 2
		]);

		$this->_form($group);
	}

	protected function _edit($id) {

		$group = $this->Groups->getGroup($id);

		if (!$group || !$group->hasPermission($this->_authUser))
			throw new NotFoundException('The group could not be found.');

		$this->_form($group);
	}

	protected function _form($group) {

		if ($this->request->is(['post', 'put'])) {

			if(! empty($this->request->data['tags']['_ids']))
			foreach ($this->request->data['tags']['_ids'] as $tag ) {

				if(!is_numeric ( $tag)) {

				 $lastId = $this->Groups->Tags->saveTag($tag);

				 $this->request->data['tags']['_ids'][] = $lastId->id;
			 	}

			}
			if ($this->request->data('category_id') == 3)
				$this->request->data['hidden'] = 0;

			if ($group->isNew())
				$this->request->data['users'][] = [
					'id' => $this->_authUser->id,
					'_joinData' => [
						'admin' => true
					]
				];

			$group = $this->Groups->patchEntity($group, $this->request->data, ['associated' => ['Categories', 'Users._joinData', 'Tags']]);

			if ($group->dirty('pending')) {

				$group->vote_decision = $group->pending ? 'N' : 'Y';
				$group->vote_decision_date = Time::now();

			}

			if ($this->Groups->save($group)) {
				$this->Flash->success(sprintf('The group "%s" has been saved.', $group->name));
				return $this->redirect(['action' => 'view', $group->id]);
			}
			else {
				$this->Flash->error('An error occurred while trying to save the group.');
			}
		}

		$title = ($this->request->action == 'add') ? 'New Group' : 'Edit Group';
		$breadcrumbs[] = ['name' => 'Groups', 'route' => ['action' => 'index']];
		$this->set(compact('title', 'breadcrumbs'));

		$tags = $this->Groups->Tags->find('list')
			->where(['Tags.parent_id IS' => null]);

		$categories = $this->Groups->Categories->findByModel('Groups')->find('list');

		$members = [];

		if ($group->users)
			$members = (new Collection($group->users))->combine('id', 'name')->toArray();

		$this->set(compact('group', 'categories', 'members', 'tags'));
		$this->render('/Groups/_form');
	}

	protected function _delete($id) {
		$this->request->allowMethod('post');

		$group = $this->Groups->get($id);

		if (!$group || !$group->hasPermission($this->_authUser))
			throw new NotFoundException('The group could not be found.');

		if ($group->canDelete()) {

			$group->status = 'D';

			if ($this->Groups->save($group))
				$this->Flash->success(sprintf('The group "%s" has been deleted.', $group->name));
			else
				$this->Flash->error(sprintf('An error occurred while trying to delete the group "%s".', $group->name));
		}
		else {
			$this->Flash->error(sprintf('The group "%s" couldn\'t be deleted because it has one or more active users.', $group->name));
		}

		return $this->redirect(['action' => 'index']);
	}

	protected function _ajax_html_member_list() {

		$id = $this->request->data('id');

		if (empty($id)) {

			$this->autoRender = false;

			return '';

		}

		$group = $this->Groups->get($id, [
			'contain' => [
				'Users.ProfileImg'
			]
		]);

		$this->set(compact('group'));

		$this->render('/Groups/_ajax_html_member_list');

	}

	protected function _ajax_add_member() {

		$id = $this->request->data('id');
		$userId = $this->request->data('user_id');

		$response = ['status' => 'error'];

		if (!empty($id) && !empty($userId)) {

			$groupsUsers = $this->Groups->GroupsUsers->newEntity([
				'group_id' => $id,
				'user_id' => $userId
			]);

			if ($this->Groups->GroupsUsers->save($groupsUsers))
				$response = ['status' => 'ok'];

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_group_access() {

		$groupRequestId = $this->request->data('group_request_id');
		$type = $this->request->data('type');

		$response = ['status' => 'error'];

		if (!empty($groupRequestId) && !empty($type)) {

			$groupRequest = $this->Groups->GroupRequests->get($groupRequestId, [
				'contain' => [
					'Groups'
				]
			]);

			if (in_array($this->_authUser->role, ['A']) || $groupRequest->group->isAdmin($this->_authUser)) {

				$groupRequest->status = $type == 'A' ? $type : 'R';

				if ($this->Groups->GroupRequests->save($groupRequest)) {

					if ($groupRequest->status == 'A') {

						$this->loadModel('GroupsUsers');

						$groupsUsers = $this->GroupsUsers->newEntity([
							'group_id' => $groupRequest->group_id,
							'user_id' => $groupRequest->user_id
						]);

						$this->GroupsUsers->save($groupsUsers);

					}

					$response = ['status' => 'ok'];

				}

			}

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_group_admin() {

		$groupsUsersId = $this->request->data('groups_users_id');
		$admin = $this->request->data('admin');

		$response = ['status' => 'error'];

		if (!empty($groupsUsersId)) {

			$groupsUsers = $this->Groups->GroupsUsers->get($groupsUsersId, [
				'contain' => [
					'Groups'
				]
			]);

			if (in_array($this->_authUser->role, ['A', 'O']) || $groupsUsers->group->isAdmin($this->_authUser)) {

				$groupsUsers->admin = $admin;

				if ($this->Groups->GroupsUsers->save($groupsUsers))
					$response = ['status' => 'ok'];

			}

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	protected function _ajax_leave_group() {

		$groupId = $this->request->data('group_id');

		if (empty($userId = $this->request->data('id'))) {
			$userId = $this->_authUser->id;
		}

		$response = ['status' => 'error'];
		$conditions = [
			'group_id' => $groupId,
			'user_id' => $userId
		];

		$user = $this->Groups->GroupsUsers->find()
			->where($conditions)
			->first();

		if ($this->Groups->GroupsUsers->delete($user)) {

			$this->Groups->GroupRequests->deleteAll(
				['group_id' => $groupId, 'user_id' => $userId]
			);

			$response = ['status' => 'ok'];

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);
	}

}
