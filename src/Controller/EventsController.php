<?php
namespace App\Controller;

use Cake\Network\Exception\NotFoundException;
use Cake\Network\Exception\InternalErrorException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Collection\Collection;

class EventsController extends \App\Controller\AppController {

	public function beforeFilter(\Cake\Event\Event $event) {

		parent::beforeFilter($event);

		if (empty($this->_authUser))
			die('Not authorized.');

	}

	protected function _index($id = null) {

		$title = 'Events';
		$hideTitle = true;

		$startDate =  Time::now();

	
		
		$lastDate = $this->Events->find()
			->where(['start_date >=' => $startDate->i18nFormat('YYYY-MM-dd')])
			->order('start_date DESC')
			->first();

		$where = [];
		$link = [];

		$dateDiff = 0;
		if (!empty($id)) {
			$where = [
				'Events.id' => $id
			];
			$link = [
				'link' => true
			];
		} else {
				$where = [
					'Events.start_date >=' => $startDate->i18nFormat('YYYY-MM-dd')
				];
		}

		$events = $this->Events->find()
		->contain([
			'Users',
			'Groups'
		])
		->where($where)
		->select([
			's_date' => 'DATE_FORMAT(Events.start_date, "%b %d, %Y")',
		])
		->select($link)
		->select($this->Events)
		->select($this->Events->Users)
		->select($this->Events->Groups)
		->orderDesc('Events.start_date');

		
		$futureEvents =( new Collection($events))
		->groupBy('s_date');

		// debug($futureEvents->toArray());

		if (!empty($lastDate)) {
			$dateDiff = date_diff($lastDate->start_date, $startDate)->days + 2;
			$lastDate = $lastDate->start_date->i18nFormat('YYYY-MM-dd');
		}

		$bannerActions = [];

		 $breadcrumbs = [
			 ['name' => $title, 'active' => true]
		 ];

		$this->set(compact('title', 'hideTitle', 'bannerActions', 'breadcrumbs', 'lastDate', 'dateDiff', 'futureEvents'));
		$this->render(DS . 'Events' . DS . '_index');
	}

	public function saved_events() {
		$this->autoRender = false;
    $this->response->disableCache();

		$start = Time::now()->i18nFormat('YYYY-MM-dd');
    $end = Time::now()->i18nFormat('YYYY-MM-dd');

    if(isset($this->request->query['start'])) {
      $start = Time::parse($this->request->query['start'])->i18nFormat('YYYY-MM-dd');
    }

    if(isset($this->request->query['end'])) {
      $end = Time::parse($this->request->query['end'])->i18nFormat('YYYY-MM-dd');
    }

		$where = [
			'OR' => [
				[
					'Events.start_date >=' => $start,
					'Events.start_date <=' => $end,
				],
				[
					'Events.end_date IS NOT' => null,
					'Events.end_date >=' => $start,
					'Events.end_date <=' => $end,
				]
			]
		];


		$eventsQuery = $this->Events->find()
			->contain([
				'Groups',
				'Users',
				'Projects'
			])
      ->where($where);

		$events = [];
		foreach($eventsQuery as $event) {

      /*
        End date is exclusive
        All day events displayed on calendar show the span of the event and not
        when it actually ends (it'll be short 1 day) so here add 1 day
      */
      if ($event->all_day && !empty($event->end_date)) {

        if ($event->end_date > $event->start_date) {
          $event->end_date->addDays(1);
        }

      }

			$title = [];

			if (!empty($event->group))
				$title[] = $event->group->name;

			if (!empty($event->user))
				$title[] = $event->user->name;

			$title[] = $event->name;

			$userEvents = $this->Events->find()
				->select('Events.id')
				->where([
					'Events.user_id' => $this->_authUser->id
				])->extract('id');



			$events[] = [
				'id' => 'event-' . $event->id,
				'eId' => $event->id,
				'group_id' => $event->group_id,
				'project_id' => $event->project_id,
				'groupName' => !empty($event->group) && empty($groupId) ? $event->group->name : '',
				'projectName' => !empty($event->project) ? $event->project->name : '',
				'title' => implode(', ', $title),
				'description' => (!empty(trim($event->description))) ? $event->description : null,
				'start' => $event->start_date->i18nFormat('YYYY-MM-dd HH:mm:ssZ'),
				'end' => (!is_null($event->end_date)) ? $event->end_date->i18nFormat('YYYY-MM-dd HH:mm:ssZ') : null,
				'allDay' => $event->all_day,
				'city' => $event->city,
				'country' => $event->country,
				'className' => 'default',
				'entity' => $event,
				'userEvents' => $userEvents
			];
		}

		$this->response->type('json');
    $this->response->body(safe_json_encode($events));
	}

	/*
  public function contact_events() {
		$this->autoRender = false;
    $this->response->disableCache();
		
		$EventSettings = TableRegistry::get('EventSettings');
		$eventSettings = $EventSettings->find()
			->indexBy('id')
			->toArray();
		
		if(!$eventSettings[1]['value'] && !$eventSettings[2]['value']) { // id:1 = birthdays, id:2 = anniversaries
			$this->response->type('json');
			$this->response->body(safe_json_encode([]));
		}
		else {
			
			$conditions = [];
			$events = [];
	
			$start = Time::now()->i18nFormat('YYYY-MM-dd');
			$end = Time::now()->i18nFormat('YYYY-MM-dd');
	
			if(isset($this->request->query['start'])) {
				$start = Time::parse($this->request->query['start'])->i18nFormat('YYYY-MM-dd');
			}
	
			if(isset($this->request->query['end'])) {
				$end = Time::parse($this->request->query['end'])->i18nFormat('YYYY-MM-dd');
			}
	
			$start_year = (new Time($start))->i18nFormat('YYYY');
			$end_year = (new Time($end))->i18nFormat('YYYY');
	
			$date_formats = ["$start_year-MM-dd" => "$start_year-%m-%Y"];
			$toJsonFormat = ["$start_year-MM-dd HH:mm"];
	
			if($start_year != $end_year) {
				$date_formats["$end_year-MM-dd"] = "$end_year-%m-%Y";
				$toJsonFormat[] = "$end_year-MM-dd HH:mm";
			}
			
			foreach($date_formats as $php_format => $sql_format) {
				
				if($eventSettings[1]['value']) {
					$conditions['OR'][] = [
						'DATE_FORMAT(Contacts.birthday, "' . $sql_format . '") >=' => $start,
						'DATE_FORMAT(Contacts.birthday, "' . $sql_format . '") <=' => $end,
					];
				}
				
				if($eventSettings[2]['value']) {
					$conditions['OR'][] = [
						'DATE_FORMAT(Contacts.start_date, "' . $sql_format . '") >=' => $start,
						'DATE_FORMAT(Contacts.start_date, "' . $sql_format . '") <=' => $end,
					];
				}
			}
	
			$this->loadModel('Contacts');
	
			$contacts = $this->Contacts->find()
				->select([
					'id',
					'first_name', 'last_name',
					'birthday',
					'start_date'
				])
				->where($conditions);
	
			foreach ($contacts as $contact) {
	
				foreach($toJsonFormat as $dtFormat) {
	
					if(!empty($contact->birthday) && $eventSettings[1]['value']) {
						$birthday = $contact->birthday->i18nFormat($dtFormat);
	
						if ($birthday >= $start && $birthday <= $end) {
							$events[] = [
								'id' => 'birthday-' . $contact->id,
								'title' => $contact->name . "'s birthday",
								'start' => $birthday,
								'allDay' => true,
								'className' => 'birthday',
	
								'repeat' => 'Y',
								'originalDate' => $contact->birthday->i18nFormat('LLLL d')
							];
						}
					}
	
					if(!empty($contact->start_date) && $eventSettings[2]['value']) {
						$start_day = $contact->start_date->i18nFormat($dtFormat);
	
						if ($start_day >= $start && $start_day <= $end) {
							$events[] = [
								'id' => 'start-' . $contact->id,
								'title' => $contact->name . " joined MOLLY MAID",
								'start' => $start_day,
								'allDay' => true,
								'className' => 'start',
	
								'repeat' => 'Y',
								'originalDate' => $contact->start_date->i18nFormat('LLLL d, yyyy')
							];
						}
					}
	
				}
			}
	
			$this->response->type('json');
			$this->response->body(safe_json_encode($events));
		}
  }
	*/

	public function settings() {
		$this->autoRender = false;
		$this->response->disableCache();

		$EventSettings = TableRegistry::get('EventSettings');

		if($this->request->is('post')) {

			if(!isset($this->request->data['records']))
				throw new InternalErrorException('Missing Data');

			foreach($this->request->data['records'] as $record) {

				$eventSetting = $EventSettings->get($record['id']);
				$eventSetting->value = (bool)($record['value']);

				if(!$EventSettings->save($eventSetting)) {
					throw new InternalErrorException('Could not save change to calendar setting for: ' . ucwords($record['name']));
				}
			}

			$this->response->type('json');
			$this->response->body(safe_json_encode(true));
		}
		else {

			$eventSettings = $EventSettings->find();

			$this->response->type('json');
			$this->response->body(safe_json_encode($eventSettings));
		}
	}

	public function save($id = null) {
		$this->autoRender = false;
		$this->response->disableCache();


		if (empty($id))
			$event = $this->Events->newEntity([
				'user_id' => $this->_authUser->id
			]);
		else {
			$event = $this->Events->get($id);
			$this->request->data = array_intersect_key($this->request->data, $event->toArray());
		}



		$this->request->data['start_date'] = new Time($this->request->data['start_date']);

		if (! empty($this->request->data['end_date'])) {
			$this->request->data['end_date'] = new Time($this->request->data['end_date']);
		}


		$event = $this->Events->patchEntity($event, $this->request->data);

		$this->Events->save($event);

		$response = [];

		if (! empty($event)) {

			$event = $this->Events->get($event->id, [
				'contain' => [
					'Groups',
					'Users',
					'Projects'
				]
			]);

			/*
				End date is exclusive
				All day events displayed on calendar show the span of the event and not
				when it actually ends (it'll be short 1 day) so here add 1 day
			*/
			if ($event->all_day && !empty($event->end_date)) {

				if ($event->end_date > $event->start_date) {
					$event->end_date->addDays(1);
				}

			}

			$title = [];

			if (!empty($event->group))
				$title[] = $event->group->name;

			if (!empty($event->user))
				$title[] = $event->user->name;

			$title[] = $event->name;

			$response = [
				'id' => 'event-' . $event->id,
				'group_id' => $event->group_id,
				'project_id' => $event->project_id,
				'groupName' => !empty($event->group) ? $event->group->name : '',
				'projectName' => !empty($event->project) ? $event->project->name : '',
				'title' => implode(', ', $title),
				'description' => (!empty(trim($event->description))) ? $event->description : null,
				'start' => $event->start_date->i18nFormat('YYYY-MM-dd HH:mm:ssZ'),
				'end' => (!empty($event->end_date)) ? $event->end_date->i18nFormat('YYYY-MM-dd HH:mm:ssZ') : null,
				'allDay' => $event->all_day,
				'className' => 'default',
				'entity' => $event
			];
		}

		$this->response->type('json');
		$this->response->body(safe_json_encode($response));
	}

	public function delete($id) {
		$this->autoRender = false;
		$this->response->disableCache();
		$this->request->allowMethod(['post', 'delete']);

		$event = $this->Events->get($id);

		if ($this->Events->delete($event)) {

			$this->response->type('json');
			$this->response->body(safe_json_encode(1));
		}
		elseif ($this->request->is('delete')) {
			$this->request->header("HTTP/1.0 404 Not Found");
		}
		else {
			$this->Flash->error(__('Error while trying to delete event.'));
		}
	}

	public function group_options() {

		$groups = $this->Events->Groups->find()
			->where([
				'Groups.pending' => false
			]);

		if (!in_array($this->_authUser->role, ['A', 'O'])) {

			$authUserGroupIds = $this->_authUser->getGroupIds();

			$groups->where([
				'Groups.id IN' => !empty($authUserGroupIds) ? $authUserGroupIds : [0]
			]);

		}

		$response = [
			[
				'name' => '--',
				'value' => 0
			]
		];

		foreach ($groups as $group)
			$response[] = [
				'name' => $group->name,
				'value' => $group->id
			];

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

	public function project_options() {

		$projects = $this->Events->Projects->find()
			->contain('Groups')
			->where([
				'Projects.status !=' => 'R'
			]);

		if (!in_array($this->_authUser->role, ['A', 'O'])) {

			$authUser = $this->_authUser;

			$projects->matching('Groups', function ($query) use ($authUser) {
				return $query->matching('GroupsUsers' ,function($q) use ($authUser) {
					return $q->where(['GroupsUsers.user_id' => $authUser->id]);
				});
			});

		}

		$response = [
			[
				'name' => '--',
				'value' => 0,
				'groupIds' => [0]
			]
		];

		foreach ($projects as $project)	{

			$groupIds = [];

			foreach ($project->groups as $group) {
				$groupIds[] = $group->id;
			}
			if (empty($groupIds)) {
				$groupIds[] = 0;
			}

			$response[] = [
				'name' => $project->name,
				'value' => $project->id,
				'groupId' => $project->group_id,
				'groupIds' => $groupIds
			];

			if (!empty($groupIds)) {
				$response[0]['groupIds'] = array_merge($response[0]['groupIds'], $groupIds);
			}

		}

		$this->set([
			'response' => $response,
			'_serialize' => 'response'
		]);

	}

}