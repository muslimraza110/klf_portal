<?php

namespace App\Controller;

class SettingsController extends AppController {

	protected function _index() {

		$settings = $this->Settings->find();

		$pinnedPost = $this->_pinned_post();

		$title = 'Settings';
		$breadcrumbs = [
			['name' => 'Settings', 'active' => true]
		];

		$this->set(compact('settings', 'title', 'breadcrumbs', 'pinnedPost'));

		$this->render('/Settings/index');

	}

	protected function _edit($id) {

		$pinnedPost = $this->_pinned_post();

		if (!empty($pinnedPost) && $id == 'FIXED_NEWS_ITEM') {
			$this->Flash->warning('Please remove the pinned post before editing the Fixed News Item Setting.');
			return $this->redirect(['action' => 'index']);
		}

		$setting = $this->Settings->get($id);

		if ($this->request->is(['post', 'put'])) {

			$setting = $this->Settings->patchEntity($setting, $this->request->data);

			if ($this->Settings->save($setting)) {
				$this->Flash->success('Setting saved successfully!');
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error('There was an error in saving. Please try again.');
			}

		}

		$title = 'Edit ' . $setting->name;
		$breadcrumbs = [
			['name' => 'Settings', 'route' => ['action' => 'index']],
			['name' => $title, 'active' => true]
		];

		$this->set(compact('setting', 'title', 'breadcrumbs'));
		$this->render('/Settings/edit');

	}

}