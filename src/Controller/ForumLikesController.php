<?php
namespace App\Controller;
use Cake\I18n\Time;

class ForumLikesController extends \App\Controller\AppController 
{
	protected function _ajax_like($dislike = false)
	{
		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			$response = [];

			if (isset($this->request->data['forum_post_id']) && isset($this->request->data['user_id'])) {

				$data = [
					'forum_post_id' => $this->request->data['forum_post_id'],
					'user_id' => $this->request->data['user_id']
				];

				$forumLike = $this->ForumLikes->find()
					->where($data)
					->first();
				
				if (empty($forumLike)) {
					$forumLike = $this->ForumLikes->newEntity($data);
				}

				if ($dislike) {

					if ($forumLike->dislikes > 0) {
						$forumLike->dislikes = 0;
					} else {
						$forumLike->dislikes = 1;
						$forumLike->likes = 0;
					}

				} else {

					if ($forumLike->likes > 0) {
						$forumLike->likes = 0;
					} else {
						$forumLike->likes = 1;
						$forumLike->dislikes = 0;
					}

				}
				
				if ($this->ForumLikes->save($forumLike)) {
					$response['success'] = 'success';
					$response['like'] = (int)$forumLike->likes;
					$response['dislike'] = (int)$forumLike->dislikes;
				}

			} else {
				$response['error'] = 'The content cannot be empty.';
			}

			$forumPost = $this->ForumLikes->ForumPosts->get($this->request->data('forum_post_id'));

			$this->set('data', $forumPost);

			$this->render('/Element/forums/luv_it_button_group');

		}
	}

	protected function _view($forumPostId) {

		$forumPost = $this->ForumLikes->ForumPosts->get($forumPostId, [
			'contain' => [
				'ParentForumPosts',
				'ForumLikes' => [
					'Users.ProfileImg',
					'sort' => 'Users.first_name'
				],
				'ForumDislikes' => [
					'Users.ProfileImg',
					'sort' => 'Users.first_name'
				]
			]
		]);

		$title = 'Likes and Dislikes';
		$bannerActions = false;

		$breadcrumbs = [
			['name' => 'Forum', 'route' => ['action' => 'index']],
			[
				'name' => !empty($forumPost->parent_forum_post) ? $forumPost->parent_forum_post->topic : $forumPost->topic,
				'route' => [
					'controller' => 'ForumPosts', 'action' => 'view', !empty($forumPost->parent_forum_post) ? $forumPost->parent_forum_post->id : $forumPost->id
				]
			]
		];

		$this->set(compact('title', 'bannerActions', 'breadcrumbs', 'forumPost'));

		$this->render('/ForumLikes/_view');

	}

}