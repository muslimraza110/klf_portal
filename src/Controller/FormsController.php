<?php
namespace App\Controller;

use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Exception\ForbiddenException;
use Cake\Collection\Collection;

class FormsController extends \App\Controller\AppController {

	protected function _index() {

		$conditions = [];

		if ($this->_authUser->role != 'A') {
			$conditions['CAST(Forms.start_date AS date) <='] = Time::now();
		}

		$query = $this->Forms->find()
			->where($conditions)
			->contain(['Users', 'FormUpload']);

		if (!empty($type = $this->request->query('type'))) {

			if ($type == 'P')
				$query->matching('FormPosts');
			elseif ($type == 'F')
				$query->matching('FormUpload');
			else // type == 'S'
				$query
					->notMatching('FormPosts')
					->notMatching('FormUpload');

		}
		
		$this->paginate = [
			'limit' => 25,
			'sortWhitelist' => [
				'Forms.name', 'Forms.topic', 'Forms.start_date', 'Forms.end_date', 'Users.first_name'
			]
		];
		
		$forms = $this->paginate($query);

		$this->set(compact('forms'));
		$this->render('/Forms/_index');
	}

  protected function _view($id) {

    if (! $this->Forms->exists($id))
			throw new NotFoundException();

		$form = $this->Forms->findById($id)
			->contain(['FormFields', 'FormPosts'])
			->first();

		if ($this->request->session()->check('formErrors.' . $id)) {
			$form->customErrors = $this->request->session()->consume('formErrors.' . $id);
		}

		$categoryId = $form->type;

		if (!$form->isCurrent() && !$form->isCompleted() && $this->_authUser->role != 'A')
			throw new NotFoundException();
		
		$this->request->data['forms'][$id] = $form->getUserAnswers($this->_authUser->id);
		
		if ($this->request->session()->check('requestData')) {
			$this->request->data = $this->request->session()->consume('requestData');
		}

    $title = $form->name;
    $hideTitle = true;
    $breadcrumbs[] = ['name' => 'Forms', 'route' => ['action' => 'index']];
		$this->set(compact('title', 'hideTitle', 'breadcrumbs'));
	
		$this->set(compact('form'));
		$this->render('/Forms/_view');
  }

  protected function _print_form($id) {
  	$this->viewBuilder()->layout('print');

  	$form = $this->Forms->findById($id)
			->contain(['FormFields'])
			->first();

		$this->set(compact('form'));
  	$this->render('/Forms/_print_form');
  }
 
  protected function _upload_form($id) {
		if (! $this->Forms->exists($id))
			throw new NotFoundException();

		$form = $this->Forms->findById($id)
			->contain(['FormUpload'])
			->first();
		
		if ($this->request->is(['post', 'put'])) {
			if (isset($this->request->data['form_upload']['upload']) && !empty($this->request->data['form_upload']['upload']['name'])) {
				$form = $this->Forms->patchEntity($form, $this->request->data, [
					'associated' => [
			      'FormUpload'
			    ]
				]);
				
				if ($this->Forms->save($form)) {
					$this->Flash->success('Form has been uploaded');
					$this->redirect(['action' => 'index', $form->type]);
				}
				else {
					$this->Flash->error('Form cannot be uploaded');
				}
			}
			else {
				$this->redirect(['action' => 'index', $form->type]);
			}
		}

		$this->set(compact('form'));

		$this->render(DS . 'Forms' . DS . '_upload_form');
	}

	protected function _download($id) {
		
		$this->autoRender = false;

		$form = $this->Forms->find()
			->contain([
				'FormUpload'
			])
			->where([
				'Forms.id' => $id
			])
			->first();
		
		if ($this->_authUser->role != 'A' && $form->auth_user_not_in_audience)
 			throw new ForbiddenException();
		
		$this->response->file(
			$form->form_upload->path(),
			[
				'download' => true,
				'name' => $form->form_upload->name
			]
		);
		
	}

	protected function _add_post($id) {
		if (! $this->Forms->exists($id))
			throw new NotFoundException();

		$form = $this->Forms->findById($id)
			->contain(['FormPosts'])
			->first();

		if ($this->request->is(['post', 'put'])) {
			$form = $this->Forms->patchEntity($form, $this->request->data, [
				'associated' => [
		      'FormPosts'
		    ]
			]);
			
			if ($this->Forms->save($form)) {
				$this->Flash->success('Post has been saved.');
				$this->redirect(['action' => 'index', $form->type]);
			}
			else {
				$this->Flash->error('Post cannot be saved.');
			}
		}

		$this->set(compact('form'));

		$this->render(DS . 'Forms' . DS . '_add_post');
	}
 

  protected function _save_answers($id) {
    $this->request->allowMethod('post');
		
		$theForm = $this->Forms->get($id);
		
		if ($this->_authUser->role != 'A' && $theForm->auth_user_not_in_audience)
 			throw new ForbiddenException();

		$userForm = $this->Forms->UserForms->find()
			->where([
				'UserForms.form_id' => $id,
				'UserForms.user_id' => $this->_authUser->id
			])
			->first();

		if($theForm->type == 'F' || !$userForm) {

			$formFields = $this->Forms->FormFields->find()
				->where(['form_id' => $id])
				->indexBy('id')
				->toArray();

			$form = $this->Forms->UserForms->newEntity([
				'user_id' => $this->_authUser->id,
				'form_id' => $id,
			]);

			//save multi answers as json_data
			foreach($this->request->data['forms'][$id]['user_form_values'] as $key => $data) {
				if(isset($data['_ids']))
					$this->request->data['forms'][$id]['user_form_values'][$key]['value'] = json_encode($data['_ids']);
			}

			$form = $this->Forms->UserForms->patchEntity($form, $this->request->data['forms'][$id], ['associated' => ['UserFormValues']]);

			foreach($form->user_form_values as $key => $userForm) {
				if ($formFields[$userForm->form_field_id]->required) {
					switch($formFields[$userForm->form_field_id]->type) {
						case 'checkbox':
							if($userForm->value == 'No')
								$form->user_form_values[$key]->errors("value", "Must be checked");
								break;
						case 'checkboxes':
							if($userForm->value == '""')
								$form->user_form_values[$key]->errors("value", "Minimum of one option checked");
								break;
						default:
							if(empty($userForm->value))
								$form->user_form_values[$key]->errors("value", "Cannot be empty");
					}
				}
			}
			//pr($form->errors());
			if (!empty($form->errors())) {
				$this->request->session()->write('formErrors.' . $id, $form->errors());
				$this->Flash->error('An error occured. Please review your answers.', ['key' => 'form-' . $id]);
			}
			elseif ($this->Forms->UserForms->save($form)) {
				//Call function to send email to administator of the the form if any (email_user_id)
				$form->send_answers_to_admin();
			}
		}
		else {
			$this->Flash->error('You have already filled out this form in the past.', ['key' => 'form-' . $id]);
		}
		
		$this->request->session()->write('requestData', $this->request->data);
		
		if (isset($this->request->query['return'])) {
			$return = urldecode($this->request->query['return']);
			$this->redirect($return . '#form-' . $id);
		}
		else {
			$this->redirect(['controller' => 'Forms', 'action' => 'view', $id, '#' => 'form-' . $id]);
		}
		
  }

	protected function _get_form_and_users($id) {

		if (! $this->Forms->exists($id))
			throw new NotFoundException();

		$form = $this->Forms->findById($id)
			->contain([
				'FormFields',
				'UserForms' => function ($q) {
					return $q
						->contain([
							'Users' => function ($q) {
								return $q->applyOptions(['statusCheck' => false]); //keep deleted user in results
							}
						])
						->order(['UserForms.created' => 'DESC']);
				}
			])
			->first();

		$userForms = [];

		foreach($form->user_forms as $user_form) {
			$userForms['Expected'][] = $user_form;
		}

		$expectedUsers = $this->Forms->Users->find()
			->notMatching('UserForms', function ($q) use ($id){
				return $q->where(['UserForms.form_id' => $id]);
			})
			->order(['first_name', 'last_name']);

		$this->set(compact('form', 'userForms', 'expectedUsers'));
		return $form;
	}

	protected function _stats($id) {

		$form = $this->_get_form_and_users($id);
		$stats = $form->getStats();

		$title = 'Statistics';
		$breadcrumbs[] = ['name' => 'Forms', 'route' => ['action' => 'index']];
		$this->set(compact('title', 'breadcrumbs'));

		$this->set(compact('stats'));

		$this->render('/Forms/_stats');

	}

	protected function _remind_unanswered($id) {

		$form = $this->Forms->get($id);

		$form->remindUnanswered();

		$this->Flash->success('Reminders have been sent successfully.');

		return $this->redirect(['action' => 'stats', $id]);

	}

	protected function _user_answers($user_form_id) {

		$userForm = $this->Forms->UserForms->get($user_form_id);
		$id = $userForm->form_id;

		$form = $this->_get_form_and_users($id);

		$user = $this->Forms->Users->findById($userForm->user_id)->applyOptions(['statusCheck' => false])->first();
		$this->request->data['forms'][$id] = $form->getUserAnswers($userForm->user_id, $user_form_id);

		$title = 'User Answers';
		$breadcrumbs = [
			['name' => 'Forms', 'route' => ['action' => 'index']],
			['name' => 'Form Statistics', 'route' => ['action' => 'stats', $form->id]],
		];
		$this->set(compact('title', 'breadcrumbs'));

		$this->set(compact('user'));

		$this->render('/Forms/_user_answers');

	}

	protected function _add() {

		$form = $this->Forms->newEntity([
			'user_id' => $this->_authUser->id
		]);

		$this->_form($form);
	}

	protected function _edit($id) {

		if (! $this->Forms->exists($id))
			throw new NotFoundException();

		$form = $this->Forms->get($id);

		$this->_form($form);
	}

	protected function _duplicate($id) {

		if (! $this->Forms->exists($id))
			throw new NotFoundException();

		$formToDuplicate = $this->Forms->findById($id)
			->contain(['FormFields'])
			->first();

		$form = $this->Forms->newEntity([
			'user_id' => $this->_authUser->id,
			'type' => $formToDuplicate->type,
			'name' => $formToDuplicate->name,
			'topic' => $formToDuplicate->topic,
			'audience' => $formToDuplicate->audience,
			'form_fields' => []
		]);

		if(!empty($formToDuplicate->form_fields)) {
			foreach($formToDuplicate->form_fields as $field) {
				$form->form_fields[] = $this->Forms->FormFields->newEntity([
					'order_num' => $field->order_num,
					'name' => $field->name,
					'type' => $field->type,
					'options' => $field->options
				]);
			}
		}

		$this->Flash->info('The information from form <strong># F' . $formToDuplicate->id . '</strong> has been copied.');

		$this->_form($form);
	}

	protected function _form($form) {

		if ($this->request->is(['post', 'put'])) {

			$this->request->data['topic'] = ucwords($this->request->data['topic']);

			if ($this->request->action == 'duplicate')
				$form = $this->Forms->patchEntity($form, $this->request->data, ['associated' => 'FormFields']);
			else
				$form = $this->Forms->patchEntity($form, $this->request->data);

			if ($this->Forms->save($form)) {
				if ($this->request->data['upload_form'] != 'false')
					return $this->redirect(['action' => 'upload_form', $form->id]);
				elseif ($this->request->data['add_post'] != 'false')
					return $this->redirect(['action' => 'add_post', $form->id]);
				else
					return $this->redirect(['action' => 'fields', $form->id]);
			}
			else {
				$this->Flash->error('The form could not be saved. Please review the form for errors.');
			}
		}

		if ($this->request->action == 'add')
			$title = 'New Form';
		elseif ($this->request->action == 'duplicate')
			$title = 'Duplicate Form';
		else
			$title = 'Edit Form';

		$breadcrumbs[] = ['name' => 'Forms', 'route' => ['action' => 'index']];
		$this->set(compact('title', 'breadcrumbs'));

		$userRoles = $this->Forms->Users->roles;

		$topics = $this->Forms->find()
			->select(['topic'])
			->distinct(['topic'])
			->extract('topic')
			->toArray();

		$users = $this->Forms->Users->find('list', [
			'keyField' => 'id',
			'valueField' => 'name'
		])
			->select(['id', 'first_name', 'last_name'])
			->where([
				'Users.role' => 'A'
			]);

		$this->set(compact('form', 'userRoles', 'topics', 'users'));
		$this->render('/Forms/_form');
	}

	protected function _fields($id) {

		if (! $this->Forms->exists($id))
			throw new NotFoundException();

		$form = $this->Forms->findById($id)
			->contain(['FormFields'])
			->first();

		if ($this->request->is(['post', 'put'])) {

			if (!empty($this->request->data['form_fields'])) {
				foreach($this->request->data['form_fields'] as $fieldKey => $field) {

					if(isset($field['option_array']) && is_array($field['option_array']) && !empty($field['option_array'])) {
						$this->request->data['form_fields'][$fieldKey]['options'] = implode(PHP_EOL, $field['option_array']);
					}
					else {
						$this->request->data['form_fields'][$fieldKey]['options'] = "";
					}
				}
			}

			$form = $this->Forms->patchEntity($form, $this->request->data, ['fieldList' => ['form_fields'], 'associated' => 'FormFields']);

			//validation of empty option fields
			if (!empty($form->form_fields)) {
				foreach($form->form_fields as $fieldKey => $field) {
					if (!in_array($field->type, ['dropdown', 'radio', 'checkboxes']))
						continue;
					else {

						if (empty($field->option_array))
							$field->errors('name', 'Must have options');
						else {
							foreach($field->option_array as $optionKey => $option) {
								if(empty(trim($option))) {
									$field->errors('option_array.' . $optionKey, 'Cannot be empty');
								}
							}
						}

					}
				}
			}

			//reorder form_fields
			$form->form_fields = array_values((new Collection($form->form_fields))->sortBy('order_num', SORT_NUMERIC)->toArray());

			if ($this->Forms->save($form)) {
				$this->Flash->success('The form "' . $form->name . '" has been saved.');
				return $this->redirect(['action' => 'index', $form->type]);
			}
			else {
				$this->Flash->error('The form could not be saved. Please review the form for errors.');
			}
		}

		//allow to show php error with angular

		$validationErrors = $form->errors();

		if(!empty($validationErrors) && !empty($validationErrors['form_fields'])) {
			foreach($validationErrors['form_fields'] as $fieldKey => $field) {
				foreach($field as $errorKey => $error) {
					$validationErrors['form_fields'][$fieldKey][$errorKey] = array_values($error)[0];
				}
			}
		}

		//end

		$fieldTypes = $this->Forms->FormFields->types;

		$title = $form->type;

		$this->set(compact('title', 'form', 'fieldTypes', 'validationErrors'));
		$this->render('/Forms/_fields_form');
	}

	protected function _publish($id) {
		$this->request->allowMethod('post');

		if(!$this->Forms->exists($id))
			throw new NotFoundException();

		$form = $this->Forms->get($id);
		$form->start_date = Time::now();

		if ($this->Forms->save($form))
			$this->Flash->success('The form "' . $form->name . '" has been published.');
		else
			$this->Flash->error('The form "' . $form->name . '" couldn\'t be published.');

		$this->redirect(['action' => 'view', $form->id]);
	}

	protected function _delete($id, $restore = false) {
		$this->request->allowMethod('post');

		$form = $this->Forms->get($id, ['statusCheck' => false]);

		if(!$form)
			throw new NotFoundException();

		if(!$restore) {
			$form->status = 'D';

			if ($this->Forms->save($form))
				$this->Flash->success('The form "' . $form->name . '" has been moved to the trash.');
			else
				$this->Flash->error('The form "' . $form->name . '" couldn\'t be moved to the trash.');
		}
		else {
			$form->status = 'A';

			if ($this->Forms->save($form))
				$this->Flash->success('The form "' . $form->name . '" has been restored.');
			else
				$this->Flash->error('The form "' . $form->name . '" couldn\'t be restored.');
		}

		$this->redirect(['action' => 'index', $form->type]);
	}

	protected function _trash($id = null) {

		$query = $this->Forms->find()
			->where(['Forms.status' => 'D'])
			->contain(['Users'])
			->applyOptions(['statusCheck' => false]);

		if ($this->request->is('post')) {
			if(is_null($id)) {
				$errors = [];

				foreach($query as $form) {
					if (!$this->Forms->delete($form)) {
						$errors[] = '<li>' . $form->name . '</li>';
					}
				}

				if(empty($errors)) {
					$this->Flash->success('The trash has been emptied.');
				}
				else {
					$errorList = implode('\n', $errors);
					$this->Flash->error('Some forms could not be deleted when emptying the trash:<ul>' . $errorList . '</ul>');
				}

			}
			else {

				$form = $this->Forms->get($id);

				if ($form && $form->status == 'D' && $this->Forms->delete($form))
					$this->Flash->success('The form "' . $form->name . '" has been deleted.');
				else
					$this->Flash->error('The form "' . $form->name . '" couldn\'t be deleted.');
			}

			return $this->redirect(['action' => 'trash']);
		}

		$this->paginate = [
			'limit' => 25,
			'sortWhitelist' => [
				'name', 'topic', 'start_date', 'end_date', 'Users.first_name'
			]
		];

		$forms = $this->paginate($query);

		$this->set(compact('forms'));

		$this->render('/Forms/_trash');

	}

}
