<?php
namespace App\Controller;

class ForumPostSubscriptionsController extends AppController {

	protected function _index() {

		$this->paginate = [
			'limit' => 50,
			'contain' => [
				'ForumPosts',
				'Users'
			],
			'sortWhitelist' => [
				'Users.first_name', 'ForumPosts.topic', 'created'
			]
		];

		$forumPostSubscriptions = $this->paginate();

		$title = 'Forum Subscribers';
		$breadcrumbs = [
			['active' => true, 'name' => $title]
		];

		$this->set(compact('breadcrumbs', 'title', 'forumPostSubscriptions'));

		$this->render('/ForumPostSubscriptions/_index');

	}

}