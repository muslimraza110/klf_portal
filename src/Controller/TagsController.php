<?php

namespace App\Controller;

use Cake\Collection\Collection;

class TagsController extends AppController {

	protected function _index() {

		$this->paginate = [
			'conditions' => [
				'Tags.parent_id IS' => null
			],
			'limit' => 30
		];

		$tags = $this->paginate();

		$titleIcon = $this->_icons['tags'];

		$this->set(compact('titleIcon', 'tags'));

		$this->render('/Tags/_index');

	}

	protected function _add() {

		$tag = $this->Tags->newEntity([
			'type' => 'S'
		]);

		$breadcrumbs = [
			['name' => 'Tags', 'route' => ['action' => 'index']],
			['name' => 'Add Tag', 'active' => true]
		];

		$this->set(compact('breadcrumbs'));

		$this->_form($tag);

	}

	protected function _edit($id) {

		$tag = $this->Tags->get($id);

		$breadcrumbs = [
			['name' => 'Tags', 'route' => ['action' => 'index']],
			['name' => 'Edit Tag', 'active' => true]
		];

		$this->set(compact('breadcrumbs'));

		$this->_form($tag);

	}

	protected function _delete($id) {

		$this->request->allowMethod('post');

		$tag = $this->Tags->get($id);

		if ($this->Tags->delete($tag)) {

			$this->Tags->GroupsTags->deleteAll([
				'tag_id' => $tag->id
			]);

			$this->Tags->ProjectsTags->deleteAll([
				'tag_id' => $tag->id
			]);

			$this->Flash->success('Tag has been deleted.');

		} else
			$this->Flash->error('Tag could not be deleted.');

		return $this->redirect(['action' => 'index']);

	}

	protected function _form($tag) {

		if ($this->request->is(['post', 'put'])) {

			if (!empty($this->request->data['tags']['_ids'])) {

				foreach ($this->request->data['tags']['_ids'] as $tag) {

					if (!is_numeric($tag)) {

						$lastId = $this->Tags->saveTag($tag);

						$this->request->data['tags']['_ids'][] = $lastId->id;

					}

				}

			}

			$tag = $this->Tags->patchEntity($tag, $this->request->data);

			if ($this->Tags->save($tag)) {

				$this->Flash->success('Tag saved successfully.');

				$this->redirect(['action' => 'index']);

			} else {
				$this->Flash->error('An error occurred. Tag could not be saved.');
			}

		}

		$titleIcon = $this->_icons['tags'];

		$this->set(compact('titleIcon', 'tag'));

		$this->render('/Tags/_form');

	}

	protected function _multi_level_tag() {

		$this->Tags->recover();

		$tags = $this->Tags->find()
			->where([
				'Tags.parent_id is' => NULL,
				'Tags.type' => 'M',
				'Tags.status' => 'A'
			])
			->order('order_num');

		foreach ($tags as $key => $tag) {

			$tag['children'] = $tag->sortTagTree();

		}

		$titleIcon = $this->_icons['tags'];

		$this->set(compact('titleIcon', 'tags'));
		$this->render('/Tags/_multi_level_tag');

	}

	protected function _multi_level_tag_add() {

		$lastOrderNum = $this->Tags->find()
			->select([
				'last_order_num' => 'MAX(order_num)'
			])
			->first();

		$tag = $this->Tags->newEntity([
			'type' => 'M',
			'status' => 'A',
			'order_num' => $lastOrderNum->last_order_num + 1
		]);

		$breadcrumbs = [
			['name' => 'Tags', 'route' => ['action' => 'multi_level_tag']],
			['name' => 'Add Multi-Level Tag', 'active' => true]
		];

		$this->set(compact('breadcrumbs'));

		$this->_multi_level_tag_form($tag);

	}

	protected function _multi_level_tag_edit($id) {

		$tag = $this->Tags->get($id);

		$breadcrumbs = [
			['name' => 'Tags', 'route' => ['action' => 'multi_level_tag']],
			['name' => 'Edit Multi-Level Tag', 'active' => true]
		];

		$this->set(compact('breadcrumbs'));

		$this->_multi_level_tag_form($tag);

	}

	protected function _multi_level_tag_delete($id) {

		$this->request->allowMethod('post');

		$tag = $this->Tags->get($id);
		$children = $this->Tags->find('children', ['for' => $id]);

		foreach ($children as $child) {
			$child->status = 'D';
			$this->Tags->save($child);
		}

		$tag = $this->Tags->patchEntity($tag, [
			'status' => 'D'
		]);

		if ($this->Tags->save($tag)) {
			$this->Flash->success('Tag has been deleted.');

		} else
			$this->Flash->error('Tag could not be deleted.');

		return $this->redirect(['action' => 'multi_level_tag']);

	}

	protected function _multi_level_tag_form($tag) {

		if ($this->request->is(['post', 'put'])) {

			$tag = $this->Tags->patchEntity($tag, $this->request->data);

			if (! $this->Tags->save($tag)) {
				$this->Flash->error('This Tag name already exist please try again.');
				return $this->redirect(['action' => $this->request->action, $tag->id]);
			}

			$childTags = [];

			if (!empty($this->request->data['tags']['_ids'])) {

				$childTagNames = $this->request->data['tags']['_ids'];

				$siblingLastOrderNum = $this->Tags->find()
					->select([
						'last_order_num' => 'MAX(order_num)'
					])
					->where([
						'Tags.parent_id' => $tag->id,
						'Tags.type' => 'M',
						'Tags.status' => 'A'
					])
					->first();

				$siblingLastOrderNum = $siblingLastOrderNum->last_order_num;

				foreach ($childTagNames as $childTagName) {

					$existingTag = $this->Tags->find()
						->where([
							'Tags.parent_id' => $tag->id,
							'Tags.status' => 'A',
							'Tags.name' => $childTagName
						])
						->first();

					if (empty($siblingLastOrderNum)) {
						$siblingLastOrderNum = 0;
					}

					if (!$existingTag) {
						$childTags[] = [
							'name' => $childTagName,
							'type' => 'M',
							'status' => 'A',
							'parent_id' => $tag->id,
							'order_num' => $siblingLastOrderNum++
						];
					}

				}

				$this->Tags->deleteAll([
					'parent_id' => $tag->id,
					'name not in' => $childTagNames
				]);

				$childTags = $this->Tags->newEntities($childTags);

			} else {

				$this->Tags->deleteAll([
					'parent_id' => $tag->id
				]);

			}

			if ($this->Tags->saveMany($childTags) || empty($childTags)) {
				$this->Flash->success('Multi-Level Tag saved successfully.');
				return $this->redirect(['action' => 'multi_level_tag']);
			} else {
				$this->Flash->error('An error occurred. Tag could not be saved.');
			}

		}

		//chosen submits values using keys so weird
		$tags = $this->Tags->find('list', [
				'keyField' => 'name'
			])
			->where([
				'Tags.parent_id' => $tag->id,
				'Tags.status' => 'A'
			]);

		$titleIcon = $this->_icons['tags'];

		$this->set(compact('titleIcon', 'tag', 'tags'));
		$this->render('/Tags/_multi_level_tag_form');

	}

	protected function _ajaxSaveSorting() {

		$tags = $this->request->data['serialized_tags'];

		foreach ($tags as $orderNum => $rootTag) {
			$this->Tags->rebuildTree($rootTag, $orderNum, $rootTag['id'], true);
		}

		$response = [
			'success' => true
		];

		$this->set([
			'response' => $response,
			'_serialize' => ['response']
		]);

	}

}