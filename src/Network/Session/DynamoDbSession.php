<?php
namespace App\Network\Session;

use Aws\DynamoDb\DynamoDbClient;
use Cake\Core\Configure;

class DynamoDbSession implements \SessionHandlerInterface
{

	private $handler;

	/**
	 * DynamoDbSession constructor.
	 */
	public function __construct()
	{
		$client = new DynamoDbClient([
			'region' => 'us-east-1',
			'version' => 'latest',
			'http' => [
				'verify' => false
			]
		]);

		$this->handler = $client->registerSessionHandler(array(
			'table_name' => 'sessions'
		));
	}

	public function close()
	{
		return $this->handler->close();
	}

	public function destroy($session_id)
	{
		return $this->handler->destroy($session_id);
	}

	public function gc($maxlifetime)
	{
		return $this->handler->gc($maxlifetime);
	}

	public function open($save_path, $session_id)
	{
		return $this->handler->open($save_path, $session_id);
	}

	public function read($session_id)
	{
		return $this->handler->read($session_id);
	}

	public function write($session_id, $session_data)
	{
		return $this->handler->write($session_id, $session_data);
	}

}
