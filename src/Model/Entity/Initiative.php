<?php
namespace App\Model\Entity;

use App\Model\Table\Table;
use Cake\ORM\TableRegistry;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Initiative extends Entity {

	protected function _getCurrentPledgeAmount() {

		$Initiatives = TableRegistry::get('Initiatives');
		$initiative = $Initiatives->get($this->id, [
			'contain' => [
				'Fundraisings' => [
					'FundraisingPledges'
				]
			]
		]);

		$currentPledgeAmount = 0;

		if ($initiative->fundraisings) {

			foreach ($initiative->fundraisings as $fundraising) {

				if ($initiative->status == 'R')	{
					$currentPledgeAmount += $fundraising->total_fundraising_pledges;
					$currentPledgeAmount += $fundraising->matched_amount;
				} else {
					$currentPledgeAmount += $fundraising->pledged_amount;
				}

			}

		}

		return $currentPledgeAmount;

	}

	protected function _getGoalPledgeAmount() {

		$Initiatives = TableRegistry::get('Initiatives');

		if ($this->id && !isset($this->fundraisings)) {
			$initiative = $Initiatives->get($this->id, [
				'contain' => [
					'Fundraisings'
				]
			]);
		} else {
			$initiative = $this;
		}

		$goalPledgeAmount = 0;

		if ($initiative->fundraisings) {
			foreach ($initiative->fundraisings as $fundraising) {
				$goalPledgeAmount += $fundraising->totalPledgedAmount;
			}
		}

		return $goalPledgeAmount;

	}

	public function hasMatchFundraising() {

		$Initiatives = TableRegistry::get('Initiatives');

		$initiative = $Initiatives->find()
			->contain([
				'Fundraisings' => [
					'conditions' => [
						'type' => 'M'
					]
				]
			])
			->where([
				'Initiatives.id' => $this->id
			])
			->first();

		if (empty($initiative->fundraisings)) {
			return false;
		}

		return true;

	}

	protected function _getPledgers() {

		$Initiatives = TableRegistry::get('Initiatives');
		$initiative = $Initiatives->get($this->id, [
			'contain' => [
				'Fundraisings' => [
					'FundraisingPledges' => [
						'sort' => [
							'FundraisingPledges.created DESC'
						],
						'Users' => [
							'UserDetails'
						],
						'Fundraisings' => [
							'FundraisingUsers'
						],
						'Groups'
					]
				],
				'Projects'
			]
		]);

		$pledgers = [];

		if ($initiative->fundraisings) {

			foreach ($initiative->fundraisings as $fundraising) {

				if ($fundraising->fundraising_pledges) {

					foreach ($fundraising->fundraising_pledges as $pledge) {
						if ($pledge->amount != 0) {
							$pledgers[$pledge->created->toUnixString()] = $pledge;
						}
					}

				}

			}

		}

		krsort($pledgers);

		return $pledgers;

	}

	protected function _getProjectNames() {

		$Initiatives = TableRegistry::get('Initiatives');

		$initiative = $Initiatives->find()
			->contain([
				'Projects'
			])
			->where([
				'Initiatives.id' => $this->id
			])
			->first();

		$projects = [];

		foreach ($initiative->projects as $project) {
			$projects[] = $project->name;
		}

		return $projects;

	}

	public function generateExcelPledgesReport() {

		$Initiatives = TableRegistry::get('Initiatives');

		$initiative = $Initiatives->find()
			->where([
				'Initiatives.id' => $this->id
			])
			->first();

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		/*
		 * Setting the A1 Title
		 */

		$sheet->setCellValue('A1', 'Pledgers');
		$sheet->getRowDimension('1')->setRowHeight(25);
		$sheet->getStyle('A1')->applyFromArray([
			'font' => [
				'bold' => true,
				'size' => 24
			]
		]);

		/*
		 * Setting Cell formatting
		 */

		$sheet->getStyle('B2:F2')->applyFromArray([
			'font' => [
				'bold' => true,
			]
		]);

		$sheet->getDefaultColumnDimension()->setWidth(24);

		/*
		 * Setting the Headers
		 */

		$headers = [
			'First Name',
			'Last Name',
			'Phone #',
			'Email',
			'Initiative',
			'Fundraising',
			'Charity',
			'Projects',
			'Pledge Amount',
			'match' => 'Match Amount',
			'Date'
		];

		if (!$initiative->hasMatchFundraising()) {
			unset($headers['match']);
		}

		/*
		 * Setting the Rows
		 */

		$rows = [];

		foreach ($initiative->pledgers as $pledge) {

			$row = [
				$pledge->user->first_name,
				$pledge->user->last_name,
				$pledge->user->user_detail->phone,
				$pledge->user->email,
				$initiative->name,
				$pledge->fundraising->name,
				(!empty($pledge->group) ? $pledge->group->name : '--'),
				implode(';', $initiative->project_names),
				$pledge->amount,
				'match' => $pledge->amount,
				$pledge->created
			];

			if (!$initiative->hasMatchFundraising()) {
				unset($row['match']);
			}

			$rows[] = $row;

		}

		array_unshift($rows, $headers);

		$sheet->fromArray(
			$rows,
			null,
			'A2'
		);

		$writer = new Xlsx($spreadsheet);

		$file = ROOT . DS . 'tmp' . DS . $initiative->id . '-initiative-pledgers.xlsx';

		$writer->save($file);

		return $file;

	}

}