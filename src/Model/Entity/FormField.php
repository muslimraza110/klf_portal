<?php
namespace App\Model\Entity;

use Cake\ORM\TableRegistry;

class FormField extends Entity {

	protected $_virtual = ['count_per_answer', 'option_array', 'option_array_with_keys'];

	protected function _getOptionArray() {
		$options = [];

		if (!empty($this->options)) {
			$values = preg_split("/\\r\\n|\\r|\\n/", $this->options);
			
			foreach ($values as $key => $value) {
				$options[] = $value;
			}
		}
		
		return $options;
	}
	
	protected function _getOptionArrayWithKeys() {
		$options = [];
		
		if (!empty($this->option_array)) {
			foreach ($this->option_array as $option) {
				$options[htmlentities($option)] = $option;
			}
		}
		
		return $options;
	}

	protected function _getCountPerAnswer() {
		$options = $this->option_array;
		
		if (empty($options))
			return false;
		
		$userFormValues = [];
		
		if (! empty($this->user_form_values)) {
			$userFormValues = $this->user_form_values;
		}
		else {
			$UserFormValues = TableRegistry::get('UserFormValues');
			$userFormValues = $UserFormValues->findByFormFieldId($this->id);
		}
		
		$answers = [];
		
		foreach($options as $option) {
			$answers[htmlentities($option)] = 0;
		}
		
		foreach ($userFormValues as $formValue) {
			
			if($this->type == 'checkboxes') {
				$value = json_decode($formValue->value);
				
				if (is_array($value) && ! empty($value)) {
					foreach ($value as $val) {
						$val = html_entity_decode($val); //way around bug for old data saved in DB
						$val = htmlentities($val);
						
						if (isset($answers[$val])) {
							$answers[$val]++;
						}
						else {
							if(!isset($answers['Invalid'])) {
								$answers['Invalid'] = 0;
							}
							$answers['Invalid']++;
						}
					}
				}
			}
			else {
				$value = html_entity_decode($formValue->value); //way around bug for old data saved in DB
				$value = htmlentities($value);
				
				if (isset($answers[$value]))
					$answers[$value]++;
				else
					$answers[$value] = 1;
			}
		}
		
		return json_encode($answers);
	}

}