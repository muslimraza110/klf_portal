<?php

namespace App\Model\Entity;

use Cake\Mailer\MailerAwareTrait;

class UserFamily extends Entity {

	use MailerAwareTrait;

	const GENDER_MALE = 0x1;
	const GENDER_FEMALE = 0x2;

	public function getRelationInverse($gender = null) {

		$simpleRelations = [
			'Parent' => 'Child',
			'Child' => 'Parent',
			'Sibling' => 'Sibling',
			'Cousin' => 'Cousin',
			'Grandparent' => 'Grandchild',
			'Grandchild' => 'Grandparent'
		];

		if (array_key_exists($this->relation, $simpleRelations))
			return $simpleRelations[$this->relation];

		if (in_array($this->relation, ['Uncle', 'Aunt']) && !is_null($gender)) {

			if ($gender == self::GENDER_FEMALE)
				return 'Niece';

			return 'Nephew';

		}




		return 'Custom Relationship';

	}

	public function sendNotification() {

		return $this->getMailer('UserFamily')
			->send('sendNotification', [$this]);

	}

}