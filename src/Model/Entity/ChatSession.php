<?php
namespace App\Model\Entity;

class ChatSession extends Entity {
	
	public $_virtual = ['auth_user_join_data'];
	
	protected function _getAuthUserJoinData() {
		
		if (!empty($this->users) && isset($_SESSION['Auth']['User']['id'])) {
			foreach($this->users as $user) {
				if ($user->id == $_SESSION['Auth']['User']['id'])
					return $user->_joinData;
			}
		}
		
		return false;
	}
 
}