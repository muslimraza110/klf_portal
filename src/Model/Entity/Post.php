<?php
namespace App\Model\Entity;

use Cake\Core\Configure;
use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Mailer\MailerAwareTrait;
use Cake\Routing\Router;

class Post extends Entity {

	use MailerAwareTrait;

	public function shootNotificationEmails() {
		//IMPORTANT: Keep in sync with NotificationShell.php function _shoot_notification_emails()

		if (empty($this->published) || $this->published > FrozenTime::now() || $this->notification_emails_sent)
			return false;

		$Posts = TableRegistry::get('Posts');

		$notifications = $Posts->Users->NotificationSettings->find()
			->where([
				'NotificationSettings.model' => 'Posts',
				'NotificationSettings.send_email' => 1,
				'NotificationSettings.created <=' => $this->created,
				'NotificationSettings.condition_column' => 'category_id',
				'NotificationSettings.condition_value' => $this->category_id
			])
			->matching('Users');

		if (!$notifications->isEmpty()) {
			foreach($notifications as $notification) {
				$user = $notification->_matchingData['Users'];
				$this->getMailer('User')->send('sendNotification', [$this, $user]);
			}
		}

		$this->notification_emails_sent = 1;
		$Posts->save($this);
		return true;
	}

  public function getFeaturedImage($width = null, $thumb = false) {
    if (empty($this->id))
      return;
    if (! empty($this->featured_img)) {

      if(empty($width)) {
          return $this->featured_img->relativeUrl();
      }
      return $this->featured_img->url(['width' => $width]);
    }
    else {
      if ($thumb)
        return Router::url('/img/logo.png', true);
      else
        return Router::url('/img/untitled.jpg', true);
    }
  }

  public function isCurrent() {
    if (!empty($this->status) && $this->status != 'D' && !empty($this->published)) {

      $publishDate = Time::parse($this->published->i18nFormat('YYYY-MM-dd'));
      $now = Time::parse(Time::now()->i18nFormat('YYYY-MM-dd'));

      if($publishDate <= $now)
        return true;
    }

    return false;
  }

  public function isPending() {
    if (!empty($this->status) && $this->status != 'D') {

      if(empty($this->published))
        return true;
      else {

        $publishDate = Time::parse($this->published->i18nFormat('YYYY-MM-dd'));
        $now = Time::parse(Time::now()->i18nFormat('YYYY-MM-dd'));

        if($publishDate > $now)
          return true;
      }
    }

    return false;
  }

  public function isArchived() {

    return $this->archived;

  }

  public function getRelatedPosts() {
    $relatedPosts = [];

    if(empty($this->related_posts))
      return $relatedPosts;

    $Posts = TableRegistry::get('Posts');
    $relatedPosts = $Posts->getRelatedPosts($this->related_posts);

    return $relatedPosts;
  }

  public function updateRelatedPostJson() {
    $relatedPosts = $this->getRelatedPosts();

    if(empty($relatedPosts) || !is_array($relatedPosts))
      return;

		$relatedPostsArray = [];
    $Posts = TableRegistry::get('Posts');

    foreach($relatedPosts as $relatedPost) {
      $array = $Posts->getPostArray($relatedPost['model'], $relatedPost['data']);
			if(!empty($array))
				$relatedPostsArray[] = $array;
    }

    $this->related_posts = safe_json_encode($relatedPostsArray);
    $this->dirty('modified', true);
    $Posts->save($this);
    $this->dirty('modified', false);
    return;
  }

  public function isPdf() {
    if (!empty($this->id) && !isset($post->post_upload)) {
      $Posts = TableRegistry::get('Posts');

      $post = $Posts->findById($this->id)
        ->contain(['PostUpload'])
        ->first();

    }
    else {
      $form = $this;
    }

    if (isset($post->post_upload) && !empty($post->post_upload))
      return true;
    else
      return false;
  }

  protected function _getPostLink() {

    if (empty($this->id))
      return;

    if ($this->isPdf())
      return ['controller' => 'Posts', 'action' => 'download', $this->id, 'prefix' => '_', '_full' => true];
    else
      return ['controller' => 'Posts', 'action' => 'view', $this->id, 'prefix' => '_', '_full' => true];
  }

}
