<?php
namespace App\Model\Entity;

use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use Cake\I18n\Time;

class Form extends Entity {

	use MailerAwareTrait;
	
	public function hasFields() {
		if ($this->_getTotalQuestions())
			return true;
		else
			return false;
	}

	protected function _getPublished() {
		
		if (!empty($this->start_date && !empty($this->end_date))) {
			return 'From ' . $this->start_date . ' to ' . $this->end_date;
		}
		elseif (!empty($this->start_date)) {
			return $this->start_date;
		}
		
		return;
	}
	
	protected function _getTotalQuestions() {
		if (!isset($this->form_fields) || empty($this->form_fields)) {
			
			$FormFields = TableRegistry::get('FormFields');
			
			return $FormFields->find()
				->where(['form_id' => $this->id])
				->count();
			
		}
		else {
			return count($this->form_fields);
		}
	}
	
	protected function _getResponse() {
		
		if (empty($this->id) || $this->isPending())
			return '—';
		
		$stats = $this->getStats();
		
		return $stats['Response'];
	}

	public function getStats() {
		$formId = $this->id;
		$audienceChars = (is_array($this->audience)) ? $this->audience : [];
		
		$Users = TableRegistry::get('Users');
		
		$users = $Users->find()
			->applyOptions(['statusCheck' => false]) //keep deleted user in results
			->contain([
				'UserForms' => function ($q) use ($formId) {
					return $q
						->where(['UserForms.form_id' => $formId]);
				}
			]);
		
		$stats = [
			'Expected Answers' => 0,
			'Did Not Answer' => 0,
			'Users Deleted' => 0,
			'Response' => '0/0',
			'Max Repeat Per User' => 0,
			'In Audience' => 0,
			'Not In Audience' => 0
		];
		
		foreach ($users as $user) {
			
			if (isset($user->user_forms) && !empty($user->user_forms)) {
				$stats['Expected Answers']++;
				
				if ($user->status == 'D')
					$stats['Users Deleted']++;
				
				if (count($user->user_forms) > $stats['Max Repeat Per User'])
					$stats['Max Repeat Per User'] = count($user->user_forms);
			}
			elseif ($user->status != 'D') {
				$stats['Expected Answers']++;
				$stats['Did Not Answer']++;
			}
		}
		
		if ($stats['Expected Answers'] != 0) {
			$responsePercentage = ($stats['Expected Answers'] - $stats['Did Not Answer']) / $stats['Expected Answers'] * 100;
		} else {
			$responsePercentage = 0;
		}
		
		$stats['Response'] = floor($responsePercentage) . '%';
		
		return $stats;
	}

  public function isCurrent() {
    if (!empty($this->start_date)) {

      $startDate = Time::parse($this->start_date->i18nFormat('YYYY-MM-dd'));
      $now = Time::parse(Time::now()->i18nFormat('YYYY-MM-dd'));

      if ($startDate <= $now && empty($this->end_date))
        return true;
			elseif (!empty($this->end_date)) {

				$endDate = Time::parse($this->end_date->i18nFormat('YYYY-MM-dd'));

				if ($startDate <= $now && $endDate >= $now)
					return true;
			}
    }

    return false;
  }

  public function isPending() {

		if (empty($this->start_date))
			return true;
		else {

			$startDate = Time::parse($this->start_date->i18nFormat('YYYY-MM-dd'));
			$now = Time::parse(Time::now()->i18nFormat('YYYY-MM-dd'));

			if ($startDate > $now)
				return true;
		}

    return false;
  }

	public function isCompleted() {
    if (!empty($this->start_date) && !empty($this->end_date)) {

			$startDate = Time::parse($this->start_date->i18nFormat('YYYY-MM-dd'));
			$endDate = Time::parse($this->end_date->i18nFormat('YYYY-MM-dd'));
			$now = Time::parse(Time::now()->i18nFormat('YYYY-MM-dd'));

			if ($startDate < $now && $endDate < $now)
				return true;
		}

    return false;
  }

	public function getRelatedPosts() {
    $relatedPosts = [];

    if(empty($this->related_posts))
      return $relatedPosts;

    $Forms = TableRegistry::get('Forms');
    $relatedPosts = $Forms->getRelatedPosts($this->related_posts);

    return $relatedPosts;
  }

  public function updateRelatedPostJson() {
    $relatedPosts = $this->getRelatedPosts();

    if(empty($relatedPosts) || !is_array($relatedPosts))
      return;

		$relatedPostsArray = [];
    $Forms = TableRegistry::get('Forms');

    foreach($relatedPosts as $relatedPost) {
      $array = $Forms->getPostArray($relatedPost['model'], $relatedPost['data']);
			if(!empty($array))
				$relatedPostsArray[] = $array;
    }

    $this->related_posts = safe_json_encode($relatedPostsArray);
		$this->dirty('modified', true);
    $Forms->save($this);
    $this->dirty('modified', false);
    return;
  }

	public function getUserAnswers($authUserId, $userFormId = null) {
		
		if(empty($this->id) || empty($authUserId) || empty($this->form_fields) || !isset($this->form_fields)) {
			$this->disabled = false;
			return [];
		}

		$UserForms = TableRegistry::get('UserForms');
		
		if (!is_null($userFormId)) {
			$userForm = $UserForms->findById($userFormId)
				->contain(['UserFormValues'])
				->first();
		}
		else {
			$userForm = $UserForms->find()
				->where([
					'UserForms.form_id' => $this->id,
					'UserForms.user_id' => $authUserId
				])
				->contain(['UserFormValues'])
				->order(['UserForms.created' => 'DESC'])
				->first();
		}

		if(empty($userForm) || !isset($userForm->user_form_values)) {
			$this->disabled = false;
			return [];
		}
		
		if($this->type != 'F' || $this->isCompleted())
			$this->disabled = true;
		
		$this->auth_user_data = $userForm;

		$requestData = [];
		$formValues = (new Collection($userForm->user_form_values))->indexBy('form_field_id')->toArray();

		foreach($this->form_fields as $formFieldkey => $formField) {
			
			if($formField->type == 'checkboxes' && isset($formValues[$formField->id])) {
				$options = [];
				
				$selectedOptions = (!empty($formValues[$formField->id]['value'])) ? json_decode($formValues[$formField->id]['value']) : [];

				if(!empty($selectedOptions)) {
					foreach($selectedOptions as $selectedOption) {
						
						$selectedOption = html_entity_decode($selectedOption); //way around bug for old data saved in DB
						
						if (!in_array($selectedOption, $formField->option_array))
							$requestData['user_form_values'][$formFieldkey]['invalid_answer'][] = $selectedOption;
						
						$selectedOption = htmlentities($selectedOption);
						
						$requestData['user_form_values'][$formFieldkey]['_ids'][] = $selectedOption;
					}
					
					if(isset($requestData['user_form_values'][$formFieldkey]['invalid_answer'])) {
						
						foreach($requestData['user_form_values'][$formFieldkey]['invalid_answer'] as $key => $answer) {
							$requestData['user_form_values'][$formFieldkey]['invalid_answer'][$key] = '— ' . $answer;
						}
						
						$requestData['user_form_values'][$formFieldkey]['invalid_answer'] = implode(PHP_EOL, $requestData['user_form_values'][$formFieldkey]['invalid_answer']);
					}
				}
			}
			else if(($formField->type == 'dropdown' || $formField->type == 'radio') && isset($formValues[$formField->id])) {
				
				$selectedOption = html_entity_decode($formValues[$formField->id]['value']); //way around bug for old data saved in DB
				
				if (!in_array($selectedOption, $formField->option_array))
					$requestData['user_form_values'][$formFieldkey]['invalid_answer'] = $selectedOption;
				
				$selectedOption = htmlentities($formValues[$formField->id]['value']);
				
				$requestData['user_form_values'][$formFieldkey]['value'] = $selectedOption;
			}
			else if(isset($formValues[$formField->id])) {
				$requestData['user_form_values'][$formFieldkey]['value'] = $formValues[$formField->id]['value'];
			}
		}

		return $requestData;
	}

	public function _getCategory() {
		if (empty($this->id))
			return;

		if ($this->isPdf())
			return 'PDF';
		elseif ($this->isPost())
			return 'Post';
		else
			return 'Online Form';
	}
	
	public function isPdf() {
		
		if (!empty($this->id) && !isset($form->form_upload)) {
			$Forms = TableRegistry::get('Forms');
			
			$form = $Forms->findById($this->id)
				->contain(['FormUpload'])
				->first();
		}
		else {
			$form = $this;
		}
		
		if (isset($form->form_upload) && !empty($form->form_upload))
			return true;
		else
			return false;
	}

	public function isPost() {
		if (!empty($this->id) && !isset($form->form_post)) {
			$Forms = TableRegistry::get('Forms');
			
			$form = $Forms->findById($this->id)
				->contain(['FormPosts'])
				->first();
		}
		else {
			$form = $this;
		}

		if (isset($form->form_post) && !empty($form->form_post))
			return true;
		else
			return false;
	}

	protected function _getPostLink() {
		
		if (empty($this->id))
			return;
		
    if ($this->isPdf())
      return ['controller' => 'Forms', 'action' => 'download', $this->id, 'prefix' => '_', '_full' => true];
    else
      return ['controller' => 'Forms', 'action' => 'view', $this->id, 'prefix' => '_', '_full' => true];
  }

	public function remindUnanswered() {

		$Users = TableRegistry::get('Users');

		$id = $this->id;

		$users = $Users->find()
			->notMatching('UserForms', function ($query) use ($id) {

				return $query->where(['UserForms.form_id' => $id]);

			});

		foreach ($users as $user)
			$this->getMailer('Form')->send('sendReminder', [$this, $user]);

	}

	
}