<?php
namespace App\Model\Entity;

use Cake\ORM\TableRegistry;

class PostTheme extends Entity {
	
	public function getPostCount() {
		
		if (empty($this->id))
			return 0;
		
		$Posts = TableRegistry::get('Posts');
		
		return $Posts->findByPostThemeId($this->id)->count();
	}

	public function canDelete() {
		return ($this->getPostCount() === 0);
	}
	
}