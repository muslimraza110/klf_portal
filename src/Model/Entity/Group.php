<?php
namespace App\Model\Entity;

use Cake\Collection\Collection;
use Cake\Core\Configure;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

class Group extends Entity {

	use VoteDecisionTrait;
	
	private function _findProfileImg($width, $image) {
		$profileImg = Router::url('/img/'.$image.'.jpg', true);
		
		if (!empty($this->profile_img))
			$profileImg = $this->profile_img->url(['width' => $width]);
		
		return $profileImg;
	}
	
	protected function _getProfileTiny() {
		return $this->_findProfileImg('50', 'group-profile-tiny');
	}
	
	protected function _getProfileThumb() {
		return $this->_findProfileImg('120', 'group-profile-thumb');
	}
	
	protected function _getProfileImgUrl() {
		return $this->_findProfileImg('200', 'group-profile-img');
	}
	
	protected function _getBannerImgUrl() {
		$bannerImg = Router::url('/img/banner-img.jpg', true);
		
		if (!empty($this->banner_img))
			$bannerImg = $this->banner_img->url(['height' => '200']);
		
		return $bannerImg;
	}
	
	
	protected function _getSingleLineAddress() {
		
		$a = [];
		
		if (!empty($this->address))
			$a[] = h($this->address);
		
		if (!empty($this->address2))
			$a[] = h($this->address2);
		
		if (!empty($this->city))
			$a[] = h($this->city);
		
		$b = [];
		
		if (!empty($this->region))
			$b[] = $this->region;
		
		if (!empty($this->postal_code))
			$b[] = h($this->postal_code);
		
		if (!empty($this->country))
			$b[] = $this->country;
		
		$a[] = implode(' ', $b);
		
		return implode(', ', $a);
	}
	
	protected function _getMultiLineAddress() {
		$a = [];
		
		if (!empty($this->address))
			$a[] = h($this->address);
		
		if (!empty($this->address2))
			$a[] = h($this->address2);
		
		if (!empty($this->city) || !empty($this->region) || !empty($this->postal_code)) {
			$city = [];
			$region = [];
			
			if (!empty($this->city))
				$city[] = h($this->city);
			
			if (!empty($this->region))
				$region[] = $this->region;
			
			if (!empty($this->postal_code))
				$region[] = h($this->postal_code);
			
			if (!empty($region))
				$city[] = implode(' ', $region);
			
			if (!empty($city))
				$a[] = implode(', ', $city);
		}
		
		if (!empty($this->country))
			$a[] = $this->country;
		
		
		return implode('<br />', $a);
	}
	
	public function getRelatedGroups(User $authUser = null) {

		if (empty($id = $this->id))
			return false;

		if (!empty($authUser))
			$authUserGroupIds = $authUser->getGroupIds();

		if (empty($authUserGroupIds))
			$authUserGroupIds = [0];

		$Groups = TableRegistry::get('Groups');

		$groupIds = $Groups->findById($id)
			->contain([
				'Users.Groups' => function ($query) use ($id, $authUserGroupIds) {

					return $query
						->where([
							'OR' => [
								[
									'Groups.id !=' => $id,
									'Groups.hidden' => false
								],
								[
									'Groups.id !=' => $id,
									'Groups.id IN' => $authUserGroupIds
								]
							]
						]);

				}
			])
			->extract('users.{*}.groups.{*}.id')
			->toArray();

		if (empty($groupIds))
			return false;

		return $Groups->find()
			->contain([
				'ProfileImg'
			])
			->where([
				'Groups.id IN' => array_unique($groupIds)
			]);

	}
	
	public function getMemberCount() {
		
		if (empty($this->id))
			return false;
		
		$Users = TableRegistry::get('Users');
		
		return $Users->find()
			->matching('Groups', function(Query $q) {
				return $q->where(['Groups.id' => $this->id]);
			})
		->count();
	}

	public function getRelatedGroupsProjects() {

		if (empty($this->getRelatedGroups())) {
			return;
		}

		return (new Collection($this->getRelatedGroups()
		->contain([
			// 'Events.Users',
			'Projects' => [
				'Users',
				'Groups'
				// 'Events.Projects.Users'
			],
		])))
		->extract('projects')
		->unfold()
		->toArray();

	}



	public function canDelete() {
		return ($this->getMemberCount() == 0);
	}

	public function hasPermission(User $authUser) {

		if (!in_array($authUser->role, ['A']) && $this->hidden && !in_array($this->id, $this->getGroupIds()))
			return false;

		return true;

	}

	public function isAdmin(User $user) {

		$GroupsUsers = TableRegistry::get('GroupsUsers');

		$groupsUsers = $GroupsUsers->findByGroupIdAndUserId($this->id, $user->id)
			->first();

		if (empty($groupsUsers))
			return false;

		return $groupsUsers->admin;

	}

  public function getGroupAdmins($groupId) {

    $GroupsUsers = TableRegistry::get('GroupsUsers');

    $Users = TableRegistry::get('Users');

    $adminGroups = $GroupsUsers->findByGroupId($groupId)
      ->where(['admin' => 1]);

		if(! empty($adminGroups) )	{
			$admins = [];
			foreach ($adminGroups as $adminGroup) {
				if($admin = $Users->findById($adminGroup->user_id)->first())
					$admins[] = $admin;
			}

			return $admins;
		}

		return false;
  }

	public function getRequestStatus(User $user) {

		$Groups = TableRegistry::get('Groups');

		$group = $Groups->get($this->id, [
			'contain' => [
				'Users' => [
					'conditions' => [
						'Users.id' => $user->id
					]
				]
			]
		]);

		if (!empty($group->users))
			return '';

		if (empty($this->group_requests)) {

			$group = $Groups->get($this->id, [
				'contain' => [
					'GroupRequests'
				]
			]);

			$groupRequests = $group->group_requests;

		} else
			$groupRequests = $this->group_requests;

		$indexedGroupRequests = (new Collection($groupRequests))->indexBy('user_id')->toArray();

		if (array_key_exists($user->id, $indexedGroupRequests))
			return $indexedGroupRequests[$user->id]->status;

		return '';

	}

}