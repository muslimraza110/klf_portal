<?php
namespace App\Model\Entity;

class UserDetail extends Entity {
	
	protected function _getSingleLineAddress() {
		
		$a = [];
		
		if (!empty($this->address))
			$a[] = h($this->address);
		
		if (!empty($this->address2))
			$a[] = h($this->address2);
		
		if (!empty($this->city))
			$a[] = h($this->city);
		
		$b = [];
		
		if (!empty($this->region))
			$b[] = $this->region;
		
		if (!empty($this->postal_code))
			$b[] = h($this->postal_code);
		
		if (!empty($this->country))
			$b[] = $this->country;
		
		$a[] = implode(' ', $b);
		
		return implode(', ', $a);
	}
	
	protected function _getMultiLineAddress() {
		$a = [];
		
		if (!empty($this->address))
			$a[] = h($this->address);
		
		if (!empty($this->address2))
			$a[] = h($this->address2);
		
		if (!empty($this->city) || !empty($this->region) || !empty($this->postal_code)) {
			$city = [];
			$region = [];
			
			if (!empty($this->city))
				$city[] = h($this->city);
			
			if (!empty($this->region))
				$region[] = $this->region;
			
			if (!empty($this->postal_code))
				$region[] = h($this->postal_code);
			
			if (!empty($region))
				$city[] = implode(' ', $region);
			
			if (!empty($city))
				$a[] = implode(', ', $city);
		}
		
		if (!empty($this->country))
			$a[] = $this->country;
		
		
		return implode('<br />', $a);
	}
	
}