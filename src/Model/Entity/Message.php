<?php
namespace App\Model\Entity;

use Cake\Collection\Collection;
use Cake\ORM\TableRegistry;

class Message extends Entity
{
	public function recipientList($recipientType)
	{
		$getRecipients = [];

		if ($recipientType == 'T')
			$getRecipients = $this->recipients;
		else
			$getRecipients = $this->cc_recipients;

		$collection = new Collection($getRecipients);

		$recipientList = $collection->extract(function ($recipient) {
	  	return $recipient->user->name;
		})
		->toArray();

		$recipientList = implode(', ', $recipientList);
		
		return $recipientList;
	}

	public function isRecipientOf($getId = false)
	{
		$authUserReceivedMessage = [];

		if (isset($this->recipients)) {
			foreach($this->recipients as $recipient) {
				if($recipient->user_id == $_SESSION['Auth']['User']['id']) {
					$authUserReceivedMessage[] = $recipient['id'];
				}
			}
		}
		
		if (isset($this->cc_recipients)) {
			foreach($this->cc_recipients as $recipient) {
				if($recipient->user_id == $_SESSION['Auth']['User']['id']) {
					$authUserReceivedMessage[] = $recipient['id'];
				}
			}
		}

		if (isset($this->messages_users)) {
			foreach($this->messages_users as $messageUser) {
				if(in_array($messageUser->action, ['T', 'C']) &&
					$messageUser->user_id == $_SESSION['Auth']['User']['id']
				) {
					$authUserReceivedMessage[] = $messageUser->id;
				}
			}
		}

		if ($getId == true) {
			return $authUserReceivedMessage;
		}
		elseif (!empty($authUserReceivedMessage)) {
			return true;
		}
		
		return false;
	}

	public function isSenderOf($getId = false)
	{
		$authUserSentMessage = false;
		
		if(isset($this->sender)) {
			if($this->sender->user_id == $_SESSION['Auth']['User']['id']) {
				$authUserSentMessage = $this->sender->id;
			}
		}
		
		if($getId == true) {
			return $authUserSentMessage;
		}
		elseif($authUserSentMessage) {
			return true;
		}
		
		return false;
	}

	public function deleteDraft()
	{
		$messagesUserIds = $this->isSenderOf();
		
		if($messagesUserIds) {
			if($this->delete($this->id))
				return true;
		}
		
		return false;
	}

	public function toggleOption($markAs = 'read')
	{
		$messagesUserIds = $this->isRecipientOf(true);
		$MessagesUsers = TableRegistry::get('MessagesUsers');
		$errors = [];

		if(!empty($messagesUserIds)) {
			$ifSaved = false;
			
			foreach($messagesUserIds as $messagesUserId) {
				$messageUser = $MessagesUsers->get($messagesUserId);
		
				switch($markAs) {
					case 'archive':
						$messageUser->archived = 1;
						break;
					case 'not archive':
						$messageUser->archived = 0;
						break;
					case 'read':
						$messageUser->message_read = 1;
						break;
					case 'not read':
						$messageUser->message_read = 0;
						break;
					case 'trash':
						$messageUser->trash = 1;
						break;
					case 'not trash':
						$messageUser->trash = 0;
						break;
				}
				
				if($MessagesUsers->save($messageUser))
					$ifSaved = true;
			}
			
			if($ifSaved)
				return true;
		}
		
		return false;
	}
	
}


