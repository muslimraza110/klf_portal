<?php
namespace App\Model\Entity;

use Cake\ORM\TableRegistry;

class FundraisingPledge extends Entity {

	protected function _getAmountTo() {

		$Fundraisings = TableRegistry::get('Fundraisings');

		$fundraisingPledge = $Fundraisings->FundraisingPledges->get($this->id, [
			'contain' => ['Fundraisings']
		]);

		if ($fundraisingPledge->fundraising->type == 'B' && $fundraisingPledge->amount_pending == -1) {
				return 0;
		} else {
			return $this->amount_pending;
		}

	}

	public function matchers() {

		$FundraisingPledges = TableRegistry::get('FundraisingPledges');

		$fundraisingPledge = $FundraisingPledges->find()
			->contain([
				'Fundraisings' => [
					'FundraisingUsers' => [
						'Users'
					]
				]
			])
			->where([
				'FundraisingPledges.id' => $this->id
			])
			->first();

		return $fundraisingPledge->fundraising->fundraising_users;

	}

	public function _getMatcherNames() {

		$matchers = $this->matchers();

		$users = [];

		foreach ($matchers as $matcher) {

			$users[] = $matcher->user->fullName;

		}

		if (count($users) > 2) {
			$users[] = 'and ' . array_pop($users);
		}

		return implode(', ', $users);

	}

	protected function _getMatchedAmount() {

		$FundraisingPledges = TableRegistry::get('FundraisingPledges');

		$fundraisingPledge = $FundraisingPledges->find()
			->contain([
				'Fundraisings'
			])
			->where([
				'FundraisingPledges.id' => $this->id
			])
			->first();

		if (!empty($fundraisingPledge)) {
			return $fundraisingPledge->fundraising->matched_amount;
		}

		return 0;

	}

}
