<?php

namespace App\Model\Entity;

trait VoteDecisionTrait {

	protected function _getVoteDecisionLabel() {

		if ($this->vote_decision == 'Y')
			return '<div class="label label-success">Accepted</div>';
		elseif ($this->vote_decision == 'N')
			return '<div class="label label-danger">Rejected</div>';

		return '<div class="label label-default">Not Enough Votes</div>';

	}

}