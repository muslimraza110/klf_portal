<?php
namespace App\Model\Entity;

use Cake\ORM\TableRegistry;

class Email extends Entity {

	protected function _getEblastRecipients() {

		$Emails = TableRegistry::get('Emails');
		$Users = TableRegistry::get('Users');

		$emails = $Emails->find()
			->contain([
				'Users'
			])
			->where([
				'Emails.created LIKE' => $this->created->format('Y-m-d H:i') . '%'
			]);

		$recipients = [];

		foreach ($emails as $email) {

			$emailObject = '';

			if (@unserialize($email->email_object) !== false) {
				$emailObject = unserialize($email->email_object);
			}

			if (!empty($emailObject)) {

				if (!empty($emailObject['_to'])) {

					$user = $Users->find()
						->where([
							'Users.email' => key($emailObject['_to'])
						])
						->first();

					if (!empty($user)) {

						$name = [];

						if (!empty(trim($user->last_name))) {
							$name[] = $user->last_name;
						}

						if (!empty(trim($user->first_name))) {
							$name[] = $user->first_name;
						}

						$recipients[$user->id] = implode(', ', $name);
					}

				}

			}

		}

		asort($recipients);

		return $recipients;

	}

	public function isInitiative() {

		$emailObject = '';

		if (@unserialize($this->email_object) !== false) {
			$emailObject = unserialize($this->email_object);
		}

		if (isset($emailObject['viewVars']['buttonLink'])) {

			$url = explode('/', $emailObject['viewVars']['buttonLink']);

			if (in_array('initiatives', $url)) {
				return true;
			}

		}

		return false;

	}

	public function isPinned($initiativeId) {

		$InitiativePins = TableRegistry::get('InitiativePins');

		$initiativePin = $InitiativePins->find()
			->where([
				'DATE_FORMAT(InitiativePins.date_group, "%m%d%y%h%i") = ' . $this->created->format('mdyhi'),
				'InitiativePins.initiative_id' => $initiativeId
			])
			->first();

		if (!empty($initiativePin)) {
			return true;
		}

		return false;

	}

}