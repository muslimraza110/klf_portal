<?php
namespace App\Model\Entity;

use Cake\I18n\Time;

class Event extends Entity {

	public function getDate($date) {

		if (!empty($date)) {
			return !empty($this->all_day) ? $date->i18nFormat('MM/dd/yy') : $date;
		}

	}

	protected function _getDateRange() {

		return $this->getDate($this->start_date) . (!empty($this->end_date) ? ' - ' . $this->getDate($this->end_date) : null);

	}


}
