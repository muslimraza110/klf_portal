<?php

namespace App\Model\Entity;

use Cake\Collection\Collection;
use Cake\ORM\TableRegistry;

class Tag extends Entity {

	protected function _getRootTag() {

		$Tags = TableRegistry::get('Tags');

		if (empty($this->parent_id)) {
			return $this;
		} else {

			$tag = $Tags->findById($this->parent_id)
				->first();

			while (!empty($tag->parent_id)) {
				$tag = $tag->_getRootTag();
			}

		}

		return $tag;

	}

	public function parentTag() {

		$Tags = TableRegistry::get('Tags');

		$tag = $Tags->findById($this->parent_id)
			->first();

		if (!empty($tag)) {
			return $tag;
		}

	}

	public function isParent() {

		$Tags = TableRegistry::get('Tags');

		$tags = $Tags->find()
			->where([
				'parent_id' => $this->id,
				'type' => 'M'
			]);

		if ($tags->isEmpty()) {
			return false;
		}

		return true;

	}

	public function treeLabel() {

		$Tags = TableRegistry::get('Tags');

		$label = [];

		$parentTag = $Tags->find()
			->where([
				'id' => $this->parent_id
			])
			->first();

		$label[] = !empty($parentTag->name) ? $parentTag->name : null;

		while(!empty($parentTag->parent_id)) {

			$parentTag = $Tags->find()
				->where([
					'id' => $parentTag->parent_id
				])
				->first();

			array_unshift($label, $parentTag->name);

		}

		array_push($label, $this->name);

		return implode(' :: ', $label);

	}

	public function sortTagTree() {

		$Tags = TableRegistry::get('Tags');

		$tags = $Tags->find('children', ['for' => $this->id])
			->find('threaded')
			->where([
				'Tags.status' => 'A',
				'Tags.type' => 'M'
			])
			->toArray();

		$tags = (new Collection($tags))->sortBy('order_num', SORT_NUMERIC)->toArray();

		$sortedTags = [];

		foreach ($tags as $tag) {

			$sortedTags[] = $tag;

			if (!empty($tag->children))	{

				$tag->children = (new Collection($tag->children))->sortBy('order_num', SORT_NUMERIC)->toArray();

				foreach ($tag->children as $child) {

					$sortedTags += $child->sortTagTree();

				}

			}

		}

		return $sortedTags;

	}

}