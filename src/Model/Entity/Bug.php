<?php
namespace App\Model\Entity;

use Cake\ORM\TableRegistry;
use Cake\Network\Email\Email;

class Bug extends Entity {
  protected $_accessible = ['*' => true];

	public function _getStatus() {

    if (!empty($this->verified))
      return 'verified';
    else if (!empty($this->completed))
      return 'completed';
    else if (!empty($this->committed))
      return 'committed';
	}

  public function sendEmail() {
    $Users = TableRegistry::get('Users');

    $user = $Users->find()
      ->where(['id' => $this->user_id])
      ->first();

		$email = new Email('default');
		// @TODO: NEED NEW VALUES FOR KHOJA
    //$email->from('no-reply@itcgrain.com', 'ITC');

    $email->to($user->email, $user->name);
    $email->template('bug_tracker')
      ->emailFormat('html')
      ->viewVars(['bug' => $this])
      ->subject('Bug tracker - ' . $this->name);

		// @TODO: TEST CODE -- NEED NEW VALUES FOR KHOJA
		/*
    if (Configure::read('debug')) {
      $email
        ->from('no-reply@itcgrain.com', 'ITC')
        ->to('junji@incodeapps.com', 'Admin');
    }*/

    return true; // @TODO: $email->send();
  }


}
