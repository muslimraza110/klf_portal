<?php

namespace App\Model\Entity;

use Cake\Collection\Collection;
use Cake\ORM\TableRegistry;

class Project extends Entity {

	public function isMember(User $user) {

		if (empty($this->users)) {

			$Projects = TableRegistry::get('Projects');

			$project = $Projects->get($this->id, [
				'contain' => [
					'Users'
				]
			]);

			$users = $project->users;

		} else
			$users = $this->users;

		$indexedUsers = (new Collection($users))->indexBy('id')->toArray();

		return array_key_exists($user->id, $indexedUsers);

	}

	public function isAdmin(User $user) {

		$GroupsUsers = TableRegistry::get('GroupsUsers');

		$groupsUsers = $GroupsUsers->findByGroupIdAndUserId($this->group_id, $user->id)
			->first();

		if (empty($groupsUsers))
			return false;

		return $groupsUsers->admin;

	}

	public function isAdminAnyGroups(User $user) {

		$Projects = TableRegistry::get('Projects');

		$projectGroups = (new Collection($Projects->ProjectsGroups->find()
			->contain([
				'Groups.GroupsUsers' => function($q) use($user){
					return $q->where([
						'GroupsUsers.admin' => true,
						'GroupsUsers.user_id' => $user->id
					]);
				}
			])
			->where([
				'ProjectsGroups.project_id' => $this->id
			])))
			->extract('groups_users')->toArray();


		return array_filter($projectGroups);


	}


	public function projectGroupsAdmins() {

		$Projects = TableRegistry::get('Projects');

		return  $Projects->ProjectsGroups->find()
			->contain([
				'Groups.GroupsUsers' => function($q) {
					return $q
					->contain([
						'Users'
					])
					->where([
						'GroupsUsers.admin' => true,
					]);
				}
			])
			->where([
				'ProjectsGroups.project_id' => $this->id
			])
			->extract('group.groups_users.{*}.user');

	}

	public function getRequestStatus(User $user) {

		$Projects = TableRegistry::get('Projects');

		$project = $Projects->get($this->id, [
			'contain' => [
				'Users' => [
					'conditions' => [
						'Users.id' => $user->id
					]
				]
			]
		]);

		if (!empty($project->users))
			return '';

		if (empty($this->project_requests)) {

			$project = $Projects->get($this->id, [
				'contain' => [
					'ProjectRequests'
				]
			]);

			$projectRequests = $project->project_requests;

		} else
			$projectRequests = $this->project_requests;

		$indexedProjectRequests = (new Collection($projectRequests))->indexBy('user_id')->toArray();

		if (array_key_exists($user->id, $indexedProjectRequests))
			return $indexedProjectRequests[$user->id]->status;

		return '';

	}

	public function tagsList() {

		$Tags = TableRegistry::get('Tags');

		$tags = $Tags->find('list')
			->contain([
				'Projects'
			])
			->matching('Projects', function($q) {
				return $q
					->where([
						'Projects.id' => $this->id
					]);
			})
			->where([
				'Tags.status' => 'A',
				'Tags.type' => 'M'
			])
			->toArray();

		return $tags;

	}

	public function tagRequired($tagId) {

		$Tags = TableRegistry::get('Tags');

		$tag = $Tags->findById($tagId)
			->first();

		return $tag->mandatory;

	}

	protected function _projectGroupsData($field) {

		$Projects = TableRegistry::get('Projects');

		return (new Collection($Projects->get($this->id, [
			'contain' => [
				'Groups'
			]
		])->groups))
		->extract($field);

	}

	public function projectCharities() {

		$charities = $this->_projectGroupsData('name');

		return implode('; ', $charities->toList());;

	}

	public function projectGroupsIds() {

		return $this->_projectGroupsData('id')->toList();

	}

}
