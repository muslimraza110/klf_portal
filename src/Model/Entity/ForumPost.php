<?php
namespace App\Model\Entity;

use App\Model\Table\Table;
use Cake\Collection\Collection;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class ForumPost extends Entity {
	
	protected function _getCountComments() {
		$ForumPosts = TableRegistry::get('ForumPosts');

    $count = $ForumPosts->findByParentId($this->id)
			->where([
				'ForumPosts.pending' => false
			])
      ->count();

    return $count;
	}

	protected function _getLatestCommentTime() {

		$ForumPosts = TableRegistry::get('ForumPosts');
		$timeAgo = '';

    if (! empty($this->id)) {

  		$latestComment = $ForumPosts->findByParentId($this->id)
				->where([
					'ForumPosts.pending' => false
				])
        ->order([
        	'created' => 'DESC'
        ])
        ->first();

      if (!empty($latestComment))
      	$timeAgo = $latestComment->created->timeAgoInWords(['format' => 'MMM d, YYY', 'end' => '+1 day']);
    }

    return $timeAgo;
	}

  protected function _getCountLikes() {
    $ForumLikes = TableRegistry::get('ForumLikes');
    $count = 0;

    if (! empty($this->id)) {
      $count = $ForumLikes->find()
        ->where([
          'forum_post_id' => $this->id,
          'likes' => 1
        ])
        ->count();
    }
    
    return $count;
  }

	protected function _getCountDislikes() {

		$count = 0;

		if (!empty($this->id)) {

			$count = TableRegistry::get('ForumLikes')->find()
				->where([
					'ForumLikes.forum_post_id' => $this->id,
					'ForumLikes.dislikes' => 1
				])
				->count();

		}

		return $count;

	}

  public function likedBefore($userId, $dislike = false) {
    $ForumLikes = TableRegistry::get('ForumLikes');

    if (! empty($this->id)) {

      $like = $ForumLikes->find()
        ->where([
          'forum_post_id' => $this->id,
          'user_id' => $userId
        ]);

			if ($dislike) {
				$like->where([
					'ForumLikes.dislikes' => true
				]);
			} else {
				$like->where([
					'ForumLikes.likes' => true
				]);
			}

      if (!empty ($like->first()))
        return 1;
    }

    return false;
  }

  public function userTotalPosts($userId) {
    $ForumPosts = TableRegistry::get('ForumPosts');

    $userPosts = $ForumPosts->find()
      ->where([
        'user_id' => $userId
      ])
      ->count();

    return $userPosts;
  }

  protected function _getPostsChildren() {
    $ForumPosts = TableRegistry::get('ForumPosts');
    $children = [];

    if (! empty($this->id)) {
      $children = $ForumPosts->findByParentId($this->id)
				->where([
					'ForumPosts.pending' => false
				])
        ->contain([
          'Users.ProfileImg'
        ]);
    }
    
    return $children;
  }

	protected function _getPendingPosts() {

		if (!empty($this->parent_id))
			$id = $this->parent_id;
		else
			$id = $this->id;

		$ForumPosts = TableRegistry::get('ForumPosts');

		return $ForumPosts->findByParentId($id)
			->where([
				'ForumPosts.pending' => true
			]);

	}

	public function hasPermission(User $authUser) {

		if (in_array($authUser->role, ['A']))
			return true;

		if (!isset($this->moderators) || !isset($this->groups)) {

			if (!empty($this->parent_id))
				$id = $this->parent_id;
			else
				$id = $this->id;

			$ForumPosts = TableRegistry::get('ForumPosts');

			$forumPost = $ForumPosts->get($id, [
				'contain' => [
					'Moderators',
					'Groups'
				]
			]);

			$moderators = $forumPost->moderators;
			$groups = $forumPost->groups;

		} else {

			$moderators = $this->moderators;
			$groups = $this->groups;

		}

		$moderatorIds = (new Collection($moderators))->extract('id')->toArray();
		$groupIds = (new Collection($groups))->extract('id')->toArray();

		if (empty($groups) || in_array($authUser->id, $moderatorIds) || in_array($authUser->id, $groupIds))
			return true;

		return false;

	}

	public function isModerator(User $authUser) {

		if (!isset($this->moderators)) {

			if (!empty($this->parent_id))
				$id = $this->parent_id;
			else
				$id = $this->id;

			$ForumPosts = TableRegistry::get('ForumPosts');

			$forumPost = $ForumPosts->get($id, [
				'contain' => [
					'Moderators'
				]
			]);

			$moderators = $forumPost->moderators;

		} else
			$moderators = $this->moderators;

		$moderatorIds = (new Collection($moderators))->extract('id')->toArray();

		return in_array($authUser->id, $moderatorIds);

	}

	public function isWhitelisted(User $authUser) {

		if (
			$this->visibility != 'W'
			|| $authUser->role == 'A'
			|| $this->user_id == $authUser->id
		) {
			return true;
		}

		if (!isset($this->forum_post_whitelists)) {
			$forumPostWhitelists = TableRegistry::get('ForumPostWhitelists')->findByForumPostId($this->id);
		} else {
			$forumPostWhitelists = $this->forum_post_whitelists;
		}

		$users = $groups = [];

		foreach ($forumPostWhitelists as $whitelist) {

			if ($whitelist->model == 'GroupsTable') {
				$groups[] = $whitelist->model_id;
			} else {
				$users[] = $whitelist->model_id;
			}

		}

		if (
			in_array($authUser->id, $users)
			|| count(array_intersect($groups, $authUser->getGroupIds())) > 0
		) {
			return true;
		}

		return false;

	}

	protected function _getCountUsers() {

		$ForumPosts = TableRegistry::get('ForumPosts');

		$forumPost = $ForumPosts->find()
			->contain([
				'ForumComments' => function ($q) {
					return $q
						->where([
							'user_id !=' => $this->user_id
						])
						->distinct('user_id');
				}
			])
			->where([
				'ForumPosts.id' => $this->id
			])
			->first();

		return count($forumPost->forum_comments) + 1;

	}

	public function isSubscribed(User $authUser) {

		return !empty(
			TableRegistry::get('ForumPostSubscriptions')->findByForumPostIdAndUserId($this->id, $authUser->id)->first()
		);

	}

	public function subscriptionType(User $authUser) {

		$ForumPostSubscriptions = TableRegistry::get('ForumPostSubscriptions');

		if ($this->isSubscribed($authUser)) {
			return $ForumPostSubscriptions->frequencies[$ForumPostSubscriptions->findByForumPostIdAndUserId($this->id, $authUser->id)->first()->frequency];
		}

		return 'None';

	}

	public function whitelistedUserList() {

		$ForumPosts = TableRegistry::get('ForumPosts');

		$forumPost = $ForumPosts->find()
			->contain([
				'ForumPostWhitelists'
			])
			->where([
				'ForumPosts.id' => $this->id
			])
			->first();

		$users = [];

		foreach ($forumPost->forum_post_whitelists as $whitelist) {

			if ($whitelist->model == 'GroupsTable') {

				$group = $ForumPosts->Groups->get($whitelist->model_id, [
					'contain' => [
						'Users'
					]
				]);

				foreach ($group->users as $user) {
					$users[$user->id] = $user->name;
				}

			} elseif ($whitelist->model == 'UsersTable') {

				$user = $ForumPosts->Users->get($whitelist->model_id);
				$users[$user->id] = $user->name;

			}

		}

		return $users;

	}
	
}