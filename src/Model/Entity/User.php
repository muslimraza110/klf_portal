<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\Collection\Collection;
use Cake\Core\Configure;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use App\Form\PasswordResetForm;
use Cake\Routing\Router;

class User extends Entity {

	use VoteDecisionTrait,
		MailerAwareTrait;

	public $_virtual = ['name', 'role_label', 'profile_thumb', 'profile_tiny'];
	protected $_hidden = ['password', 'confirm_password', 'reset_key'];

	public function send_welcome_email() {

		if (empty($this->id))
			return false;

		$passwordReset = new PasswordResetForm();
		$this->template = 'setPassword';

		$passwordReset->execute($this->toArray());
	}

	private function _findProfileImg($width, $image) {

		if (! empty($this->profile_img))
			$profileImage = $this->profile_img->url(['width' => $width]);

		if (empty($this->profile_img) || empty($profileImage))
			$profileImage = Router::url('/img/'.$image.'.jpg', true);

		return $profileImage;
	}

	protected function _getProfileTiny() {
		return $this->_findProfileImg('50', 'user-profile-tiny');
	}

	protected function _getProfileThumb() {
		return $this->_findProfileImg('120', 'user-profile-thumb');
	}

	protected function _getProfileImgUrl() {
		return $this->_findProfileImg('200', 'user-profile-img');
	}

	protected function _getBannerImgUrl() {
		$bannerImg = Router::url('/img/banner-img.jpg', true);

		if (!empty($this->banner_img))
			$bannerImg = $this->banner_img->url(['height' => '200']);

		return $bannerImg;
	}

	protected function _setPassword($password) {
		return (new DefaultPasswordHasher)->hash($password);
	}

	protected function _getName() {

		$titles = TableRegistry::get($this->source())->titles;
		$noTitles =  TableRegistry::get('Users')->noTitle;

		return implode(
			' ',
			array_filter([
				(!empty($this->title) && array_key_exists($this->title, $titles) && !in_array($this->role, $noTitles)) ? $titles[$this->title] : '',
				$this->first_name,
				$this->middle_name,
				$this->last_name
			])
		);

	}

	protected function _getRoleLabel() {
		return $this->_label('role');
	}

	protected function _getPrefix() {
		if (empty($this->role))
			return false;

		foreach (PREFIXES as $prefix => $options) {
			if ($options['role'] === $this->role) {
				return $prefix;
			}
		}

		return false;
	}

	protected function _getHomeRoute() {

		if (empty($this->role))
			return false;

		foreach (PREFIXES as $prefix => $options) {
			if ($options['role'] === $this->role) {

				if (!empty($prefix['home_route'])) {
					return $prefix['home_route'];
				}
				else {

					$defaultRoute = DEFAULT_HOMEROUTE;

					if (is_array($defaultRoute) && (!empty($defaultRoute['prefix']) && $defaultRoute['prefix'] === true))
						$defaultRoute['prefix'] = $prefix;

					return $defaultRoute;
				}

			}
		}

		return false;
	}

	protected function _getNotificationCondition($categoryId, $model) {

		$defaultCondition = [
			'model' => $model,
			'user_id' => $this->id
		];

		if ($model == 'ForumPosts') {
			$conditions = ['condition_column' => 'user_id', 'condition_value' => $categoryId];
		}
		else {
			$conditions = ['condition_column' => 'category_id', 'condition_value' => $categoryId];
		}

		$conditions = array_merge($defaultCondition, $conditions);

		return $conditions;
	}

	public function notification($categoryId, $model) {
		$NotificationSettings = TableRegistry::get('NotificationSettings');
		$conditions = $this->_getNotificationCondition($categoryId, $model);

		$data = $NotificationSettings->find()
			->where($conditions)
			->first();

		return $data;
	}

	public function checkNotification($categoryId, $model, $activation = true) {
		$data = $this->notification($categoryId, $model);

		if (empty($data)) {
			return 0;
		}
		else {
			if ($activation) {
				return 1;
			}
			else {
				return $data['send_email'];
			}
		}
	}

	public function saveNotifications($categoryId, $activate = 1, $sendEmail = 0, $model = 'Posts') {

		$NotificationSettings = TableRegistry::get('NotificationSettings');
		$data = $this->notification($categoryId, $model);
		$conditions = $this->_getNotificationCondition($categoryId, $model);
		$sendEmailArray = ['send_email' => $sendEmail];

		if ($activate) {

			if (empty($data)) {
				$conditions = array_merge($conditions, $sendEmailArray);
				$data = $NotificationSettings->newEntity($conditions);
			}
			else {
				$data->send_email = $sendEmail;
			}

			if ($NotificationSettings->save($data)) {
				return true;
			}
			else {
				return false;
			}
		}

		if (! $activate && ! empty($data)) {
			if ($NotificationSettings->delete($data))
				return true;
			else
				return false;
		}

		return true;
	}

	public function ribbonImageUrl() {

		if (in_array($this->role, ['F', 'O'])) {

			$color = $this->role == 'F' ? 'agolden' : 'ablue';

			return Router::url('/img/'.$color.'-ribbon.png', true);

		}

		return null;

	}

	public function getNotifications() {

		$NotificationSettings = TableRegistry::get('NotificationSettings');
		$NotificationExcludes = TableRegistry::get('NotificationExcludes');

		$notificationSettings = $NotificationSettings->findByUserId($this->id)
			->where([
				'NotificationSettings.send_email' => 0
			]);

		$notifications = [];

		if (! $notificationSettings->isEmpty()) {

			foreach ($notificationSettings as $notificationSetting) {

				$idsToExclude = $NotificationExcludes->find('list', [
						'keyField' => 'id',
						'valueField' => 'model_id',
					])
					->where([
						'NotificationExcludes.user_id' => $this->id,
						'NotificationExcludes.model' => $notificationSetting->model,
					])
					->toArray();

				$modelToQuery = TableRegistry::get($notificationSetting->model);
				$model = $notificationSetting->model;

				$conditions = [
					$notificationSetting->model . '.' . $notificationSetting->condition_column
						=> $notificationSetting->condition_value
				];

				$postsInNotifications = $modelToQuery->find()
					->where($conditions);

				if (! empty($idsToExclude)) {
					$postsInNotifications->andWhere([
						$model . '.id NOT IN' => $idsToExclude
					]);
				}

				switch($model) {

					case 'Posts':
						$postsInNotifications->andWhere([
							'Posts.published >' => $notificationSetting->created,
							'CAST(Posts.published AS date) <=' => Time::now()
						])
						->each(function ($value, $key) {
							$value->dataType = 'Posts';
						});
						break;

					case 'ForumPosts':
						$postsInNotifications->andWhere([
								'ForumPosts.created >' => $notificationSetting->created,
								'ForumPosts.parent_id IS' => NULL
							])
							->each(function ($value, $key) {
								$value->dataType = 'ForumPosts';
							});
						break;
				}

				if (! $postsInNotifications->isEmpty())
					$notifications[] = $postsInNotifications->toArray();
			}

		}

		return $notifications;
	}

	public function getCopywriter () {
		$Users = TableRegistry::get('Users');
		return $Users->findByRole('W')->first();
	}

	public function getGroupIds() {

		if (empty($this->groups)) {

			$Users = TableRegistry::get('Users');

			$user = $Users->get($this->id, [
				'contain' => [
					'Groups'
				]
			]);

			$groups = $user->groups;

		} else
			$groups = $this->groups;

		return (new Collection($groups))->extract('id')->toArray();

	}

	public function getProjectIds() {

		if (empty($this->projects)) {

			$Users = TableRegistry::get('Users');

			$user = $Users->get($this->id, [
				'contain' => [
					'Projects'
				]
			]);

			$projects = $user->projects;

		} else {
			$projects = $this->projects;
		}

		return (new Collection($projects))->extract('id')->toArray();

	}

	public function sendDecision() {

		return $this->getMailer('User')
			->send('sendDecision', [$this]);

	}

	public function sendGroupMembershipRequest($group, $groupAdmin) {

		return $this->getMailer('User')
			->send('sendGroupMembershipRequest', [$this, $group, $groupAdmin]);

	}

	public function sendCommentToCopywriter($copywriter, $comment) {

		return $this->getMailer('User')
			->send('sendCommentToCopywriter', [$this, $copywriter, $comment]);

	}

	public function sendProjectMembershipRequest($project, $groupAdmin) {

		return $this->getMailer('User')
			->send('sendProjectMembershipRequest', [$this, $project, $groupAdmin]);

	}

	public function sendFundraisingNotification( $fundrasing, $receiver) {

		return $this->getMailer('User')
			->send('sendFundraisingNotification', [$this, $fundrasing, $receiver]);

	}

	public function sendInitiativeNotification( $fundrasing, $receiver) {

		return $this->getMailer('User')
			->send('sendInitiativeNotification', [$this, $fundrasing, $receiver]);

	}

	public function sendPledgeRequestNotification($pledgeRequest) {

		return $this->getMailer('User')
			->send('sendPledgeRequestNotification', [$pledgeRequest, $this]);

	}

	public function projectTree() {

		$treeList = [];
		$Users = TableRegistry::get('Users');

		$userGroups = $Users->get($this->id, [
			'contain' => [
				'Groups.Projects',
				'Groups.Categories'
			]
		]);

		// $projectsNoGroups = (new Collection($usersProjects->groups))
		// 	->filter(function ($group, $key) {
		// 		return $group->projects == null;
		// 	});

		$userGroups = (new Collection($userGroups->groups))
		->filter(function ($group, $key) {
			return $group->projects != null;
		});

		foreach ($userGroups as $group) {

			$treeList[$group->category->name][] = $group;

		}

		return $treeList;

	}

	public function sendPledgeNotification($fundraising, $pledge) {

		$projects = $cc = [];
		$Groups = TableRegistry::get('Groups');
		$Fundraisings = TableRegistry::get('Fundraisings');

		foreach ($fundraising->initiative->projects as $project) {
			$projects[] = $project->name;
			$cc[] = $project->creator->email;
			foreach ($project->groups as $group) {
				if (!in_array($group->email, $cc)) {
					$cc[] = $group->email;
				}
			}
		}

		$group = $Groups->get($pledge->group_id);

		if (Configure::read('debug')) {
			$cc = ['junji@incodeapps.com'];
		}

		$projectsString = implode('', $projects);

		if (count($fundraising->initiative->projects) == 2) {
			$projectsString = implode(' & ', $projects);
		} elseif (count($fundraising->initiative->projects) > 2) {
			$projectsString = preg_replace('/,([^,]+)$/', ' &$1', implode(', ', $projects));
		}

		$fundraising = $Fundraisings->find()
			->contain([
				'Initiatives'
			])
			->where([
				'Fundraisings.id' => $fundraising->id
			])
			->first();

		return $this->getMailer('User')
			->cc($cc)
			->send('sendPledgeNotification', [
				$this,
				['fundraising' => $fundraising, 'pledge' => $pledge, 'projects' => $projectsString, 'group' => $group],
				['email' => $this->email, 'name' => $this->_getName()]
			]);

	}

}
