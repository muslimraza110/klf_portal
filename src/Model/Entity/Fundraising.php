<?php
namespace App\Model\Entity;

use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use Cake\I18n\Number;

class Fundraising extends Entity {

	protected $_fundraisingStatuses = [
		'O' => 'Open',
		'C' => 'Closed'
	];

	protected $_types = [
		'B' => 'Block',
		'P' => 'Pledge',
		'M' => 'Match',
		'D' => 'Donation'
	];

	public function fundraisingProjects($separator) {

		$Fundraisings = TableRegistry::get('Fundraisings');

		$fundraisings = $Fundraisings->get($this->id, [
			'contain' => ['Projects']
		]);

		$projects = (new Collection($fundraisings))->extract('project.name')->toArray();

		return implode($separator, $projects);

	}

	public function fundraisingStatus() {

		if (!empty($this->fundraising_status)) {
			return $this->_fundraisingStatuses[$this->fundraising_status];
		}
	}

	public function typeLabel() {

		if (!empty($this->type)) {
			return $this->_types[$this->type];
		}

		return '';

	}

	protected function _getTotalFundraisingPledges() {

		$Fundraisings = TableRegistry::get('Fundraisings');
		$fundraising = $Fundraisings->get($this->id, [
			'contain' => 'FundraisingPledges'
		]);

		$amount = 0;

		foreach ($fundraising->fundraising_pledges as $pledges) {
			$amount += $pledges->amount;
		}

		return $amount;

	}

	protected function _getPledgedAmount() {

		$amount = $this->_getTotalFundraisingPledges();

		$Fundraisings = TableRegistry::get('Fundraisings');

		$fundraising = $Fundraisings->find()
			->contain([
				'Initiatives'
			])
			->where([
				'Fundraisings.id' => $this->id
			])
			->first();

		if ($fundraising->type == 'M' && isset($fundraising->initiative) && $fundraising->initiative->status == 'R') {
			$amount += $this->_getMatchedAmount();
		} elseif ($fundraising->type != 'B') {
			$amount *= 2;
		}

		return $amount;

	}

	protected function _getMatchedAmount() {

		$Fundraisings = TableRegistry::get('Fundraisings');
		$fundraising = $Fundraisings->find()
			->contain([
				'FundraisingUsers',
				'FundraisingPledges'
			])
			->where([
				'Fundraisings.id' => $this->id
			])
			->first();


		$matchedAmount = 0;

		foreach ($fundraising->fundraising_users as $fundraisingUser) {

			if ($fundraisingUser->match_full_amount) {

				$matchedAmount += $fundraisingUser->amount;

			} else {

				$matchedAmount += ($fundraisingUser->amount * ($this->_getTotalFundraisingPledges() / $fundraising->amount));

			}

		}

		return $matchedAmount;

	}

	protected function _getTotalPledgedAmount() {

		if ($this->type == 'M') {
			return $this->amount * 2;
		} else {
			return $this->amount;
		}

	}

	public function getUserAmountPledged($userId) {

		$FundraisingPledges = TableRegistry::get('FundraisingPledges');

		$fundraisingPledges = $FundraisingPledges->find()
			->where([
				'FundraisingPledges.fundraising_id' => $this->id,
				'FundraisingPledges.user_id' => $userId
			]);

		$amount = 0;

		foreach ($fundraisingPledges as $pledges) {
			$amount += $pledges->amount;
		}

		return $amount;

	}

}
