<?php

namespace App\Model\Entity;

use Cake\Collection\Collection;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;

class UserRecommendation extends Entity {

	use VoteDecisionTrait,
		MailerAwareTrait;

	protected function _getName() {

		$name = [
			$this->first_name,
			$this->last_name
		];

		return implode(' ', array_filter($name));

	}

	public function sendTaggedUserInvitation($user) {

		return $this->getMailer('UserRecommendation')
			->send('sendInvitation', [$this, $user]);

	}

	public function sendUserInvitation() {

		$UserInvitations = TableRegistry::get('UserInvitations');

		$userInvitation = $UserInvitations->newEntity([
			'email' => $this->email,
			'hash' => sha1(Security::randomBytes(32)),
			'recommended' => true
		]);

		if ($UserInvitations->save($userInvitation)) {

			// Overload with name field (used in email)
			$userInvitation->name = $this->name;

			return $userInvitation->sendInvitation();

		}

		return false;

	}

	public function sendTaggedUserNotifications() {

		$UserRecommendations = TableRegistry::get('UserRecommendations');

		$userRecommendation = $UserRecommendations->get($this->id, [
			'contain' => [
				'TaggedUsers'
			]
		]);

		$userRecommendationMailer = $this->getMailer('UserRecommendation');

		foreach ($userRecommendation->tagged_users as $user)
			$userRecommendationMailer->send('sendTaggedUserNotification', [$this, $user]);

		return true;

	}

	public function hasVoted(User $user) {

		if (!isset($this->votes)) {

			$UserRecommendations = TableRegistry::get('UserRecommendations');

			$userRecommendation = $UserRecommendations->get($this->id, [
				'contain' => [
					'Votes'
				]
			]);

			$votes = $userRecommendation->votes;

		} else
			$votes = $this->votes;

		if (empty($this->votes))
			return false;

		$indexedVotes = (new Collection($votes))->extract('voter_id')->toArray();

		return in_array($user->id, $indexedVotes);

	}

}