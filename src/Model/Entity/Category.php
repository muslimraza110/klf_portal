<?php
namespace App\Model\Entity;

use Cake\ORM\TableRegistry;

class Category extends Entity {
	
	public function getCount() {
		
		if (empty($this->id))
			return 0;
		
		$Model = TableRegistry::get($this->model);
		
		return $Model->findByCategoryId($this->id)->count();
	}
	
	public function canDelete() {
		return ($this->getCount() === 0);
	}
	
}