<?php

namespace App\Model\Entity;

use Cake\Collection\Collection;
use Cake\ORM\TableRegistry;

class Faq extends Entity {

	protected function _getRootFaq() {

		$Faqs = TableRegistry::get('Faqs');

		if (empty($this->parent_id)) {
			return $this;
		} else {

			$faq = $Faqs->findById($this->parent_id)
				->first();

			while (!empty($faq->parent_id)) {
				$faq = $faq->_getRootFaq();
			}

		}

		return $faq;

	}

	public function parentFaq() {

		$Faqs = TableRegistry::get('Faqs');

		$faq = $Faqs->findById($this->parent_id)
			->first();

		if (!empty($faq)) {
			return $faq;
		}

	}

	public function isParent() {

		$Faqs = TableRegistry::get('Faqs');

		$faqs = $Faqs->find()
			->where([
				'parent_id' => $this->id,
			]);

		if ($faqs->isEmpty()) {
			return false;
		}

		return true;

	}

	public function treeLabel() {

		$Faqs = TableRegistry::get('Faqs');

		$label = [];

		$parentFaq = $Faqs->find()
			->where([
				'id' => $this->parent_id
			])
			->first();

		$label[] = $parentFaq->title;

		while(!empty($parentFaq->parent_id)) {

			$parentFaq = $Faqs->find()
				->where([
					'id' => $parentFaq->parent_id
				])
				->first();

			array_unshift($label, $parentFaq->title);

		}

		array_push($label, $this->title);

		return implode(' :: ', $label);

	}

	public function sortFaqTree() {

		$Faqs = TableRegistry::get('Faqs');

		$faqs = $Faqs->find('children', ['for' => $this->id])
			->find('threaded')
			->where([
				'Faqs.status' => 'A',
			])
			->toArray();

		$faqs = (new Collection($faqs))->sortBy('position', SORT_NUMERIC)->toArray();

		$sortedFaqs = [];

		foreach ($faqs as $faq) {

			$sortedFaqs[] = $faq;

			if (!empty($faq->children))	{

				$faq->children = (new Collection($faq->children))->sortBy('position', SORT_NUMERIC)->toArray();

				foreach ($faq->children as $child) {

					$sortedFaqs += $child->sortFaqTree();

				}

			}

		}

		return $sortedFaqs;

	}

}
