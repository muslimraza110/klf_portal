<?php
namespace App\Model\Entity;

use Cake\ORM\TableRegistry;
use Cake\Mailer\MailerAwareTrait;

class UserForm extends Entity {

	use MailerAwareTrait;
  
  public function send_answers_to_admin() {

  	$UserForms = TableRegistry::get('UserForms');

  	if (empty($this->id))
  		return;

  	$userForm = $UserForms->findById($this->id)
  		->contain([
  			'Forms', 
  			'Users'
  		])
  		->first();

    if (! is_null($userForm->form->email_user_id)) {
    	$adminUser = $UserForms->Forms->Users->get($userForm->form->email_user_id);

	    /*
	    $data['post'] = [
	      'form_id' => $form->id,
	      'user_id' => $user->id
	    ];
	    $template = 'user_answers_notification';
	    $subject = $form->name . ' has been submitted by ' . $user->name;
			*/
	    
	    if ($this->getMailer('User')->send('sendNotification', [$userForm, $adminUser]))
				return true;
			else {
				$this->error = __('An error occured while sending the email. Please try again later.');
				return false;
			} 
    }
  }

}