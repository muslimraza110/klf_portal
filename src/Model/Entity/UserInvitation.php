<?php

namespace App\Model\Entity;

use Cake\Mailer\MailerAwareTrait;

class UserInvitation extends Entity {

	use MailerAwareTrait;

	public function sendInvitation(array $user = []) {

		if (empty($user))
			$user = (new \Cake\Network\Session)->read('Auth.User');

		return $this->getMailer('User')
			->send('sendInvitation', [$this, $user]);

	}

}