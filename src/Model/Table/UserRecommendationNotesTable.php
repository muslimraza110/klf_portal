<?php

namespace App\Model\Table;

use Cake\Validation\Validator;

class UserRecommendationNotesTable extends Table {

	protected $_order = ['created' => 'DESC'];

	public function initialize(array $config) {

		parent::initialize($config);

		$this->addAssociations([
			'belongsTo' => [
				'UserRecommendations',
				'Users'
			]
		]);

	}

	public function validationDefault(Validator $validator) {

		return $validator
			->notEmpty('note');

	}

}