<?php
namespace App\Model\Table;

class EmailsTable extends Table {

	protected $_order = ['created' => 'DESC'];

	public function initialize(array $config) {
		parent::initialize($config);
		
		$this->table('emails');
		$this->entityClass('App\Model\Entity\Email');
		
		$this->addAssociations([
			'belongsTo' => [
				'Users',
      ],
    ]);
	}

}