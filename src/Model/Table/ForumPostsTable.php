<?php
namespace App\Model\Table;
use App\Model\Entity\User;
use Cake\I18n\Time;
use Cake\Network\Session;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Cake\Collection\Collection;
//use App\Form\NotificationForm;
use Cake\Mailer\MailerAwareTrait;

class ForumPostsTable extends Table {

	use MailerAwareTrait;

	protected $_activeStatuses = ['A'];

	public $adminVisibility = [
		'P' => 'Public',
		'A' => 'Admin',
		'S' => 'Semi-Private',
		'W' => 'Restricted'
	];

	public $publicVisibility = [
		'P' => 'Public',
		'W' => 'Restricted'
	];

	public $postVisibility = [
		'P' => 'Public',
		'R' => 'Private'
	];

	protected $_order = ['pinned' => 'DESC', 'created' => 'DESC'];

	public function initialize(array $config) {
		parent::initialize($config);
		
		$this->addBehavior('Tree');
		
    $this->addAssociations([
			'belongsTo' => [
				'Users',
				'ParentForumPosts' => [
          'className' => 'ForumPosts',
          'foreignKey' => 'parent_id'
        ],
				'Subforums'
			],
			'hasMany' => [
				'ForumLikes' => [
					'conditions' => [
						'ForumLikes.likes' => true
					]
				],
				'ForumDislikes' => [
					'className' => 'ForumLikes',
					'conditions' => [
						'ForumDislikes.dislikes' => true
					]
				],
				'ForumComments' => [
					'className' => 'ForumPosts',
					'foreignKey' => 'parent_id'
				],
				'ForumPostWhitelists',
				'ForumPostSubscriptions'
			],
			'belongsToMany' => [
				'Moderators' => [
					'joinTable' => 'forum_posts_moderators',
					'className' => 'Users'
				],
				'Groups' => [
					'joinTable' => 'forum_posts_groups'
				],
				'Projects' => [
					'joinTable' => 'forum_posts_projects'
				]
			]
    ]);
	}

  public function validationDefault(\Cake\Validation\Validator $validator) {
		return $validator
			->notEmpty('content', 'Required');
	}

	public function getForumPosts($condition = []) {
		$conditions = [
			'ForumPosts.parent_id IS' => null
		];

		$Users = TableRegistry::get('Users');

		$authUser = $Users->get((new Session)->read('Auth.User.id'), [
			'contain' => [
				'Groups'
			]
		]);

		if (($permittedIds = $this->getPermittedIds($authUser)) !== null)
			$conditions['ForumPosts.id IN'] = !empty($permittedIds) ? $permittedIds : [0];

		if (!empty($condition)) {
			$conditions = array_merge($condition, $conditions);
		}

		$forumPosts = $this->find()
			->contain([
				'Users.ProfileImg'
			])
	    ->where($conditions);
	    //->limit(5);

	  return $forumPosts;
	}

	//posts orderby latest comments
	public function getLatestPosts() {

		$Users = TableRegistry::get('Users');

		$authUser = $Users->get((new Session)->read('Auth.User.id'), [
			'contain' => [
				'Groups'
			]
		]);

		$permittedIds = $this->getPermittedIds($authUser);

		$latestParentPosts = $this->find()
			->contain([
				'ParentForumPosts.Users.ProfileImg'
			])
			->where([
				'ForumPosts.parent_id IS NOT' => null,
				'ForumPosts.pending' => false
			])
			->group([
				'ForumPosts.parent_id'
			])
			->limit(3);

		if ($permittedIds !== null)
			$latestParentPosts->where([
				'ForumPosts.parent_id IN' => !empty($permittedIds) ? $permittedIds : [0]
			]);

		$latestParentPosts = $latestParentPosts->combine(
				function ($forumPost) {

					return $forumPost->created->format('c');

				},
				'parent_forum_post'
			)
			->toArray();

		$excludeIds = [];

		foreach ($latestParentPosts as $post)
			$excludeIds[] = $post->id;

		$latestPosts = $this->find()
			->contain([
				'Users.ProfileImg'
			])
			->where([
				'ForumPosts.parent_id IS' => null,
				'ForumPosts.sod' => true,
				'ForumPosts.id NOT IN' => !empty($excludeIds) ? $excludeIds : [0]
			]);

		if ($permittedIds !== null)
			$latestPosts->where([
				'ForumPosts.id IN' => !empty($permittedIds) ? $permittedIds : [0]
			]);

		$latestPosts = $latestPosts->limit(3)
			->combine(
				function ($forumPost) {

					return $forumPost->created->format('c');

				},
				function ($forumPost) {

					return $forumPost;

				}
			)
			->toArray();

		$posts = array_merge($latestParentPosts, $latestPosts);

		krsort($posts);

		return array_slice(array_values($posts), 0, 3);

	}
	
	public function afterSave($event, $entity, $options) {
		/*
		
		//Email/notification_forum_post.ctp should be modify to include info about user who posted this
		//Also template must be different if forumPost is a comment or a brand new post
		
    if ($entity->isNew()) {
			
      $conditions = [
      	'NotificationSettings.model' => 'ForumPosts',
				'NotificationSettings.send_email' => 1,
				'NotificationSettings.condition_column' => 'user_id',
				'NotificationSettings.condition_value' => $entity->user_id
      ];

      $notifications = $this->Users->NotificationSettings->find()
        ->where($conditions)
				->contain(['Users']);
      
      if (!$notifications->isEmpty()) {
				foreach($notifications as $notification) {
					$this->getMailer('User')->send('sendNotification', [$entity, $notification->user]);
				}
      }
    }
    */
  }

	// @TODO: Queries will be very expensive in a large database. Probably needs caching

	public function getPublicIds() {

		$ForumPostsGroups = TableRegistry::get('ForumPostsGroups');
		$ForumPostsProjects = TableRegistry::get('ForumPostsProjects');

		$groups = $ForumPostsGroups->find()
			->select([
				'ForumPostsGroups.forum_post_id',
			])
			->group([
				'ForumPostsGroups.forum_post_id',
			]);

		$projects = $ForumPostsProjects->find()
			->select([
				'ForumPostsProjects.forum_post_id',
			])
			->group([
				'ForumPostsProjects.forum_post_id',
			]);

		$where = [
			'ForumPosts.parent_id IS' => null,
			'ForumPosts.id NOT IN' => $groups,
			'OR' => [
				[
					'ForumPosts.id NOT IN' => $projects
				]
			]
		];

		return $this->find()
			->select([
				'ForumPosts.id'
			])
			->where($where)
			->extract('id')
			->toArray();

	}


	public function getProjectsPrivateIds(User $authUser) {

		$publicIds = $this->getPublicIds();

		if (!in_array($authUser->role, ['A'])) {

			$userId = $authUser->id;
			$userProjectIds = $authUser->getProjectIds();

			$projectIds = $this->find()
				->select([
					'ForumPosts.id'
				])
				->innerJoinWith('Projects', function ($query) use ($userProjectIds) {

					return $query->where([
						'Projects.id IN' => !empty($userProjectIds) ? $userProjectIds : [0]
					]);

				})
				->where([
					'ForumPosts.parent_id IS' => null,
					'ForumPosts.id NOT IN' => !empty($publicIds) ? $publicIds : [0]
				])
				->extract('id')
				->toArray();

			$moderatedIds = $this->find()
				->select([
					'ForumPosts.id'
				])
				->innerJoinWith('Projects')
				->innerJoinWith('Moderators', function ($query) use ($userId) {

					return $query->where([
						'Moderators.id' => $userId,
					]);

				})
				->where([
					'ForumPosts.parent_id IS' => null,
					'ForumPosts.id NOT IN' => !empty($publicIds) ? $publicIds : [0]
				])
				->extract('id')
				->toArray();

			return array_unique(array_merge($projectIds, $moderatedIds));

		} else {
			return $this->find()
				->select([
					'ForumPosts.id'
				])
				->innerJoinWith('Projects')
				->where([
					'ForumPosts.parent_id IS' => null,
					'ForumPosts.id NOT IN' => !empty($publicIds) ? $publicIds : [0]
				])
				->extract('id')
				->toArray();
		}
	}


	public function getGroupsPrivateIds(User $authUser) {

		$publicIds = $this->getPublicIds();

		if (!in_array($authUser->role, ['A'])) {

			$userId = $authUser->id;
			$userGroupIds = $authUser->getGroupIds();

			$groupIds = $this->find()
				->select([
					'ForumPosts.id'
				])
				->innerJoinWith('Groups', function ($query) use ($userGroupIds ) {

					return $query->where([
						'Groups.id IN' => !empty($userGroupIds) ? $userGroupIds : [0],

					]);

				})
				->where([
					'ForumPosts.parent_id IS' => null,
					'ForumPosts.id NOT IN' => !empty($publicIds) ? $publicIds : [0],
				])
				->extract('id')
				->toArray();

			$moderatedIds = $this->find()
				->select([
					'ForumPosts.id'
				])
				->innerJoinWith('Groups')
				->innerJoinWith('Moderators', function ($query) use ($userId) {

					return $query->where([
						'Moderators.id' => $userId
					]);

				})
				->where([
					'ForumPosts.parent_id IS' => null,
					'ForumPosts.id NOT IN' => !empty($publicIds) ? $publicIds : [0]
				])
				->extract('id')
				->toArray();

			return array_unique(array_merge($groupIds, $moderatedIds));

		} else
			return $this->find()
				->select([
					'ForumPosts.id'
				])
				->innerJoinWith('Groups')
				->where([
					'ForumPosts.parent_id IS' => null,
					'ForumPosts.id NOT IN' => !empty($publicIds) ? $publicIds : [0]
				])
				->extract('id')
				->toArray();

	}

	public function getPermittedIds(User $authUser) {

		if (!in_array($authUser->role, ['A']))
			return array_unique(array_merge(
				$this->getPublicIds(),
				$this->getGroupsPrivateIds($authUser),
				$this->getProjectsPrivateIds($authUser)
			));

		return null;

	}

	public function getModelName($id) {


		if (!empty($group = $this->get($id, ['contain' => 'Groups'])->groups)) {
			return [
				'model' =>'Groups',
				'scope' => 'charity'
			];
		} elseif (!empty($this->get($id, ['contain' => 'Projects'])->projects)) {
			return [
				'model' =>'Projects',
				'scope' => 'project'
			];
		} elseif (!empty($visibility = $this->get($id)->visibility) && $visibility != 'W') {
			return ['scope' => 'admin'];
		}

		return false;

	}

	public function checkAuthorization($model, $id, User $authUser) {

		${$model} = TableRegistry::get($model);
		$model = ${$model}->get($id, ['contain' => 'Users']);
		$memberIds = (new Collection($model->users))->extract('id')->toArray();
		if ($model->isAdmin($authUser)
			|| $authUser->role == 'A'
			|| in_array($authUser->id, $memberIds)) {

			return true;

		}

		return false;
	}


	public function getUserGroups(User $authUser) {

		$groupIds = $authUser->getGroupIds();
		if (!empty($groupIds)) {
			return $this->Groups->find()
				->contain([
					'Categories'
				])
				->where([
					'Groups.id IN' => $groupIds
					// $this->Groups->buildUserRoleConditions($authUser)
				])
				->order([
					'Categories.name',
					'Groups.name'
				])
				->combine('id', 'name', 'category.name');
		} else return false;
	}

	public function getUserProjects(User $authUser) {
		$projectIds = $authUser->getProjectIds();
		$groupIds = $authUser->getGroupIds();
		
		if (!empty($projectIds)) {
			return $this->Projects->find()
				->contain([
					'Groups'
				])
				->where([
						'Projects.id IN' => $projectIds,
						// 'Groups.id IN' => $groupIds
					// $this->Projects->buildUserRoleConditions($authUser)
				])
				->order([
					'Groups.name',
					'Projects.name'
				])
				->combine('id', 'name', 'group.name');
		} else return false;

	}

	public function getUserGroupsAndProjects(User $authUser) {

		$groups = $this->getUserGroups($authUser)->toArray();
		$projects = $this->getUserProjects($authUser)->toArray();

		return array_merge($groups, $projects);
	}
}