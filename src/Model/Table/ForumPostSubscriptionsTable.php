<?php
namespace App\Model\Table;

class ForumPostSubscriptionsTable extends Table {

	public $frequencies = [
		'I' => 'Immediately',
		'D' => 'Once a day (9 PM EST)',
		'W' => 'Once a week (Friday 9 PM EST)'
	];

	protected $_order = ['created' => 'DESC'];

	public function initialize(array $config) {

		parent::initialize($config);

		$this->addAssociations([
			'hasMany' => [
				'ForumPostComments' => [
					'className' => 'ForumPosts',
					'foreignKey' => 'parent_id',
					'bindingKey' => 'forum_post_id'
				]
			],
			'belongsTo' => [
				'ForumPosts',
				'Users'
			]
		]);

	}

}