<?php

namespace App\Model\Table;

use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class UserVotesTable extends Table {

	protected $_order = [
		'vote' => 'DESC',
		'created' => 'DESC'
	];

	public function initialize(array $config) {

		parent::initialize($config);

		$this->addAssociations([
			'belongsTo' => [
				'Voters' => [
					'className' => 'Users'
				]
			]
		]);

	}

	public function findVotes(\Cake\ORM\Query $query, array $options) {

		$conditions = [
			'UserVotes.entity_id' => $options['entity_id'],
			'UserVotes.model' => $options['model']
		];

		if (isset($options['voter_id']))
			$conditions['UserVotes.voter_id'] = $options['voter_id'];

		return $query
			->contain([
				'Voters'
			])
			->where($conditions);

	}

	public function makeDecision($entityId, $model) {

		$EntityModel = TableRegistry::get($model);

		$entity = $EntityModel->get($entityId);

		if (!empty($entity->vote_decision))
			return false;

		$Users = TableRegistry::get('Users');

		if ($model == 'UserRecommendations')
			$where = ['Users.role IN' => ['A', 'O']];
		else
			$where = ['Users.role' => 'O'];

		$totalVoters = $Users->find()
			->where($where)
			->where([
				'Users.pending' => false
			])
			->count();

		$votes = $this->find()
			->select([
				'vote',
				'count' => 'COUNT(*)'
			])
			->where([
				'UserVotes.entity_id' => $entityId,
				'UserVotes.model' => $model
			])
			->group([
				'UserVotes.vote'
			])
			->combine('vote', 'count')
			->toArray();

		if (empty($votes['Y']) && empty($votes['N']))
			return false;

		$autoDecide = false;

		if (in_array($model, ['Groups']))
			$autoDecide = true;

		// Remove people who abstained
		$totalVoters -= isset($votes['A']) ? $votes['A'] : 0;

		$totalDecisions = (isset($votes['Y']) ? $votes['Y'] : 0) + (isset($votes['N']) ? $votes['N'] : 0);

		if (!empty($votes['Y']) && ($votes['Y'] / $totalVoters) > 0.509) {

			if ($autoDecide)
				$entity->pending = false;

			$entity->vote_decision = 'Y';

		} elseif (
			(!empty($votes['N']) && ($votes['N'] / $totalVoters) > 0.509)
			|| $totalVoters == $totalDecisions
		) {

			if ($autoDecide)
				$entity->pending = true;

			$entity->vote_decision = 'N';

		}

		if ($entity->dirty('vote_decision') && empty($entity->vote_admin_decision)) {

			$entity->vote_decision_date = Time::now();

			$EntityModel->save($entity);

			return $entity;

		}

		return false;

	}

	public function processAdminDecision($entityId, $model) {

		$EntityModel = TableRegistry::get($model);

		$entity = $EntityModel->get($entityId);

		if (empty($entity->vote_admin_decision))
			return false;

		switch ($model) {

			case 'Users':

				$entity->sendDecision();

				break;

			case 'UserRecommendations':

				if ($entity->vote_decision == 'Y')
					$entity->sendUserInvitation();

				$entity->sendTaggedUserNotifications();

				break;

			default:
				return false;

		}

		return true;

	}

}