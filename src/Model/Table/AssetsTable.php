<?php
namespace App\Model\Table;

use Aws\S3\S3Client;

class AssetsTable extends Table {

	public function initialize(array $config) {
		parent::initialize($config);
	}

	public function s3Client() {
		$credentials = \Cake\Core\Configure::read('AWS.S3.credentials');

		$s3Options = [
			'version' => '2006-03-01',
			'region' => 'us-east-1',
			'http'    => [
				'verify' => false
			]
		];

		if (! empty($credentials))
			$s3Options['credentials'] = $credentials;

		$s3Client = new S3Client($s3Options);

		return $s3Client;

	}

	public function s3Bucket() {

		return \Cake\Core\Configure::read('AWS.S3.bucket');

	}

	public function awsUploadFile($model, $modelId, $file) {
		$file['name'] = str_replace(' ', '_', $file['name']);

		$asset = $this->newEntity([
			'model_id' => $modelId,
			'model' => $model.'Table',
			'association' => $model,
			'name' => $file['name'],
			'size' => $file['size'],
			'type' => $file['type']
		]);

		$asset = $this->save($asset);

		$keyname = $asset->id;
		$filepath = $file['tmp_name'];

		$s3 = $this->s3Client();

		$result = $s3->putObject(array(
			'Bucket' => $this->s3Bucket(),
			'Key'    => $keyname,
			'SourceFile' => $filepath,
			'ContentType' => $file['type'],
			'ACL'    => 'public-read'
		));

	}

}
