<?php
namespace App\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

class Table extends \Cake\ORM\Table {

  protected $_activeStatuses = [];
	protected $_order = [];

  public function initialize(array $config) {
    parent::initialize($config);

    if ($this->hasField('name'))
      $this->displayField('name');

    $this->addBehavior('Timestamp');
  }

  public function beforeFind($event, $query, $options, $primary) {

    // TO SET DEFAULT ORDER
		$query = $this->_setDefaultOrder($query);

    // TO AUTOMATICALLY FILTER OUT DELETED RECORDS
		// ->applyOptions(['statusCheck' => false]) to turn this off
    if ( (!isset($options['statusCheck']) || $options['statusCheck'] !== false) && !empty($this->_activeStatuses) ) {
			$query->where([$this->alias() . '.status IN' => $this->_activeStatuses]);
    }
	}

	private function _setDefaultOrder($query) {

		$order = $query->clause('order');

		if (empty($order) && !empty($this->_order)) {

			if (is_array($this->_order)) {
				$order = [];

				foreach ($this->_order as $orderClause => $orderDirection) {
					if (is_numeric($orderClause))
						$order[] = $this->alias() . '.' . $orderDirection;
					else {
						if(empty($orderDirection))
							$order[] = $orderClause;
						else
							$order[$this->alias() . '.' . $orderClause] = $orderDirection;
					}
				}
			}
			else {
				$order = $this->alias() . '.' . $this->_order;
			}

			$query->order($order);
		}

		return $query;
	}

  public function exists($conditions, $checkStatus = false) {

		if (! is_array($conditions) && is_numeric($conditions)) {
			$conditions = [
				'id' => $conditions
			];
		}

		if ($checkStatus && !empty($this->_activeStatuses)) {
			$conditions['status IN '] = $this->_activeStatuses;
		}

		return parent::exists($conditions);
	}

	//--------------------------------------------------------------------------------------

	public function getRelatedPosts($relatedPostsJson) {

		$relatedPosts = [];

		if(!in_array($this->_entityClass, ['App\Model\Entity\Post', 'App\Model\Entity\Form']))
			return;

		$relatedPostsArray = json_decode($relatedPostsJson);
		$relatedPosts = [];

    if (is_array($relatedPostsArray) && !empty($relatedPostsArray)) {
      foreach($relatedPostsArray as $relatedPostObj) {

        if(!isset($relatedPostObj->model) || !isset($relatedPostObj->id))
          continue;

        $model = $relatedPostObj->model;
        $id = $relatedPostObj->id;

        switch ($model) {

          case 'Posts':

						if($this->_entityClass == 'App\Model\Entity\Post')
							$modelTable = $this;
						else
							$modelTable = TableRegistry::get($model);

            $relatedPost = $modelTable->findById($id)
							->contain(['Categories'])
							->first();

            break;

          case 'Forms':
						if($this->_entityClass == 'App\Model\Entity\Form')
							$modelTable = $this;
						else
							$modelTable = TableRegistry::get($model);

            $relatedPost = $modelTable->findById($id)
              ->contain(['FormFields.UserFormValues'])
              ->first();

              if(isset($_SESSION['formErrors'][$id])) {
                $relatedPost->customErrors = $_SESSION['formErrors'][$id];
              }

            break;

        }

        if($relatedPost) {
          $relatedPosts[] = [
            'model' => $model,
            'data' => $relatedPost
          ];
        }
      }
    }

		return $relatedPosts;
	}

	public function getPostArray($model, $relatedPost) {

		switch ($model) {
			case 'Posts':
				return [
					'folder' => 'Resources',
					'category' => (!empty($relatedPost->category)) ? $relatedPost->category->name : 'Uncategorized',
					'name' => $relatedPost->name,
					'date' => (!is_null($relatedPost->published)) ? $relatedPost->published : 'Unpublished',
					'url' => Router::url($relatedPost->post_link),
					'model' => 'Posts',
					'id' => $relatedPost->id,
				];
				break;

			case 'Forms':
				if($relatedPost->type == 'S') {
					return [
						'folder' => 'Polls & Surveys',
						'category' => (!empty($relatedPost->topic)) ? $relatedPost->topic : 'Topic Unknown',
						'name' => $relatedPost->name,
						'date' => (!empty($relatedPost->published)) ? $relatedPost->published : 'Unpublished',
						'sort_date' => (!empty($relatedPost->start_date)) ? $relatedPost->start_date : 'Unpublished',
						'url' => Router::url($relatedPost->post_link),
						'model' => 'Forms',
						'id' => $relatedPost->id,
					];
				}
				else {
					return [
						'folder' => 'Resources',
						'category' => 'Forms',
						'name' => $relatedPost->name,
						'date' => (!is_null($relatedPost->published)) ? $relatedPost->published : 'Unpublished',
						'sort_date' => (!empty($relatedPost->start_date)) ? $relatedPost->start_date : 'Unpublished',
						'url' => Router::url($relatedPost->post_link),
						'model' => 'Forms',
						'id' => $relatedPost->id,
					];
				}
				break;
		}

		return;
	}

  public function getViewStatus() {
    return $this->_viewStatus;
  }

}
