<?php
namespace App\Model\Table;

class NotificationExcludesTable extends Table {

	public function initialize(array $config) {
		parent::initialize($config);
		
    $this->addAssociations([
      'belongsTo' => [
        'Users'
      ],
    ]);
  }

}