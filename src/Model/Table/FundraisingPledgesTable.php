<?php
namespace App\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class FundraisingPledgesTable extends Table {

	protected $_order = ['amount' => 'DESC'];

	public function initialize(array $config) {

		parent::initialize($config);

		$this->addAssociations([
			'belongsTo' => [
				'Fundraisings',
				'Users',
				'Groups'
			]
		]);

	}

	public function validationDefault(Validator $validator) {

		return $validator
			->add('amount_pending', [
				'validateAmountPending' => [
					'rule' => 'validateAmountPending',
					'provider' => 'table',
					'message' => 'New Amount cannot be the same.'
				]
			])
			->notEmpty('name')
			->notEmpty('percentage');

	}

	public function validateAmountPending($value, $context) {

		$oldPledge = $this->get($context['data']['id']);

		if (!empty($oldPledge)) {

			if ($oldPledge->amount_pending != $value) {
				return true;
			}

		}

		return false;

	}

}