<?php
namespace App\Model\Table;

use Cake\ORM\TableRegistry;

class MessagesTable extends Table
{
	public function initialize(array $config)
	{
		parent::initialize($config);
		
    $this->addAssociations([
			'hasMany' => [
				'MessagesUsers' => [
					'dependent' => true
				],
				'Recipients' => [
					'className' => 'MessagesUsers',
					'foreignKey' => 'message_id',
					'conditions' => ['action' => 'T'],
					'dependent' => true
				],
				'CcRecipients' => [
					'className' => 'MessagesUsers',
					'foreignKey' => 'message_id',
					'conditions' => ['action' => 'C'],
					'dependent' => true
				]
			],
			
			'hasOne' => [
				'Senders' => [
					'className' => 'MessagesUsers',
					'foreignKey' => 'message_id',
					'conditions' => [
						'Senders.action' => 'F'
					],
					'dependent' => true
				]
			]
    ]);
  }

  public function validationDefault(\Cake\Validation\Validator $validator)
  {
    return $validator
    	->notEmpty('subject', 'A subject is required')
    	->notEmpty('content', 'Message is required');
  }

  public $recipientErrors = [];

  public function setTagitData($requestData)
  {
		$recipientsArray = [];
		$ccRecipientsArray = [];
		
		if(isset($requestData['recipients']) && !empty($requestData['recipients'])) {
			$conversionResults = $this->_convertTagitString($requestData['recipients']);
			$recipientsArray = $conversionResults['array'];
			$requestData['recipients'] = $conversionResults['string'];
		}
		
		if(isset($requestData['cc_recipients']) && !empty($requestData['cc_recipients'])) {
			$conversionResults = $this->_convertTagitString($requestData['cc_recipients'], 'C');
			$ccRecipientsArray = $conversionResults['array'];
			$requestData['cc_recipients'] = $conversionResults['string'];
		}
		
		if (isset($requestData['messages_users']))
			$requestData['messages_users'][] = array_merge($recipientsArray, $ccRecipientsArray);
		else
			$requestData['messages_users'] = array_merge($recipientsArray, $ccRecipientsArray);
		
		if(isset($requestData['send-btn']) && empty($requestData['messages_users'])) {
			$this->recipientErrors['NO-RECIPIENT'] = true;
		}

		return $requestData;
	}

	private function _convertTagitString($string, $userActionType = 'T', $model = 'User')
	{
		$newDataArray = [];
		$data = explode(',', $string);

		foreach($data as $key => $singleItemString) {
			$itemPieces = explode('::', $singleItemString);

			$itemId = $itemPieces[0];
			$itemName = $itemPieces[1];
			$messageUserId = '';
			$messageUserArray = [];

			if (isset($itemPieces[2]) && !empty($itemPieces[2]))
				$messageUserId = $itemPieces[2];

			if (!empty($messageUserId))
				$messageUserArray['id'] = $messageUserId;

			$Users = TableRegistry::get('Users');
			$user = $Users->find()
				->where([
					'Users.id' => $itemId
				])
				->first();

			if($user->name == $itemName) {
				$newDataArray[] = [
					'user_id' => $itemId,
					'action' => $userActionType,
					'message_read' => 0,
					'archived' => 0,
					'id' => $messageUserId
				];

			} else {
				$this->recipientErrors[$singleItemString] = $itemName;
				unset($data[$key]);
			}
		}
		
		$updatedString = implode(',', $data);
		
		return [
			'array' => $newDataArray,
			'string' => $updatedString
		];
	}

	public function getTagitStrings($message, $reply = false)
	{
		$recipients = [];
		$ccRecipients = [];

		if ($reply == 'sender') {
			if (isset($message->sender) && !empty($message->sender)) {
				$recipients[] = $message->sender->user_id . '::' . $message->sender->user->name;
			}
		} else {
			if (isset($message->messages_users) && !empty($message->messages_users)) {
				foreach ($message->messages_users as $messageUser) {
					switch ($messageUser->action) {
						case 'T':
							if (! $reply || ($reply == 'all' && $messageUser->user_id != $_SESSION['Auth']['User']['id'])) {
								$recipients[] = $messageUser->user_id . '::' . $messageUser->user->name . '::' . $messageUser->id;
							}
							break;
						case 'C':
							if (! $reply) {
								$ccRecipients[] = $messageUser->user_id . '::' . $messageUser->user->name . '::' . $messageUser->id;
							}
							break;
					}
				}
			}
		}

		$message->recipients = implode(',', $recipients);
		$message->cc_recipients = implode(',', $ccRecipients);

		return $message;
	}
	

}