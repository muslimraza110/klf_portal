<?php
namespace App\Model\Table;


class KhojaCareCategoriesTable extends Table {


	public function initialize(array $config) {

		parent::initialize($config);

		$this->addAssociations([
			'hasMany' => [
				'Projects' => [
					'foreignKey' => 'khoja_care_category_id'
				]
			]
		]);

	}

	public function afterSave(\Cake\Event\Event $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options) {

		if ($entity->isNew()) {

			$entity = $this->patchEntity($entity, ['position' => $entity->id]);
			$this->save($entity);

		}

	}

}
