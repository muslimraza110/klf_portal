<?php
namespace App\Model\Table;
use Cake\ORM\TableRegistry;

class NotificationSettingsTable extends Table {

	public function initialize(array $config) {
		parent::initialize($config);
		
    $this->addAssociations([
      'belongsTo' => [
        'Users'
      ],
    ]);
  }

  //tmp function to Lock all Franchise Owners to receive "MollyMaid Rag" and "Molly Maid Weekly Sweep" by email
  public function get_conditions($conditionValue, $userId, $model = '') {

    $data = [
      'user_id' => $userId,
      'model' => 'Posts',
      'condition_column' => 'category_id',
      'condition_value' => $conditionValue,
    ];

    if (! empty($model)) {
      foreach ($data as $key => $value) {
        $data[$model . '.' . $key] = $data[$key];
        unset($data[$key]);
      }
    }

    return $data;
  }

  public function save_notification($conditions, $data, $userId, $label) {

  	$NotificationSettings = TableRegistry::get('NotificationSettings');

    $notification = $NotificationSettings->find()
      ->where($conditions)
      ->first();

    if (empty($notification)) {
      $notification = $NotificationSettings->newEntity($data);
    }

    if ($notification->send_email == 0 || empty($notification->send_email)) {
      $notification->send_email = 1;
    }
  
    if ($NotificationSettings->save($notification)) {
      echo '<br />' . 'The ' . $label . ' has been created for user: ' . $userId . '<br />';
    }
    else {
      echo '<br />' . 'The ' . $label . ' notification cannot be created for user: ' . $userId . '<br />';
    }

  }
	
}