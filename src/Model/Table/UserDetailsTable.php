<?php
namespace App\Model\Table;

use Cake\Validation\Validator;

class UserDetailsTable extends Table {

	public function initialize(array $config) {

		parent::initialize($config);

		$this->addAssociations([
			'belongsTo' => [
				'Users'
			]
		]);

	}

	public function validationDefault(Validator $validator) {

		return $validator
			->notEmpty('phone', 'Your phone number is required')
			->allowEmpty('description')
			->add('description', [
				'max250' => [
					'rule' => ['maxLength', 250],
					'message' => 'The character limit is 250'
				]
			]);

	}

}