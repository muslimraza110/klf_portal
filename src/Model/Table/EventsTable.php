<?php
namespace App\Model\Table;

class EventsTable extends Table {
	
	protected $_order = ['start_date' => 'DESC'];
  
  public function initialize(array $config) {
		parent::initialize($config);

		$this->addAssociations([
			'belongsTo' => [
				'Groups',
				'Users',
				'Projects'
			]
		]);
    
  }
  
  public function validationDefault(\Cake\Validation\Validator $validator) {
		return $validator
			->notEmpty('name', 'Required')
			->notEmpty('start_date', 'Required');
	}
  
}