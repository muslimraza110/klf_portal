<?php
namespace App\Model\Table;

use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\Datasource\EntityInterface;

class PostsTable extends Table {

	protected $_activeStatuses = ['A'];

	protected $_order = [
		'CASE WHEN Posts.published IS NULL THEN 0 ELSE 1 END' => '',
		'published' => 'DESC',
		'name' => 'DESC'
	];

  protected $_viewStatus = [
    'Active',
    'Archived'
  ];

	public function initialize(array $config) {
		parent::initialize($config);

		$this->addAssociations([
			'belongsTo' => [
				'Categories' => [
					'conditions' => [
						'Categories.model' => 'Posts'
					]
				],
				'PostThemes',
				'Users'
			]
		]);

		$this->addBehavior('AssetManager.Asset', [
			'hasOne' => [
				'FeaturedImg' => [
					'type' => 'image'
				],
				'PostUpload' => [
					'type' => 'file'
				]
			]
		]);

	}

	public function afterSave(Event $event, EntityInterface $entity, \ArrayObject $options) {
		$entity->shootNotificationEmails();
	}

	public function validationDefault(Validator $validator) {
		return $validator
			->notEmpty('name', 'Required');
	}

}
