<?php
namespace App\Model\Table;

class ChatInteractionsTable extends Table {
	
	protected $_order = ['created' => 'DESC'];
	
	public $types = [
		'M' => 'Message',
	];

  public function initialize(array $config) {
		parent::initialize($config);
		
    $this->addAssociations([
			'belongsTo' => [
        'ChatSessions',
				'Users'
			]
    ]);
  }

}