<?php
namespace App\Model\Table;

class FundraisingUsersTable extends Table {

	public function initialize(array $config) {

		parent::initialize($config);

		$this->addAssociations([
			'belongsTo' => [
				'Fundraisings',
				'Users'
			]
		]);

	}

}