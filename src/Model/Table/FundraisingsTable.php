<?php
namespace App\Model\Table;

use Cake\Event\Event;
use Cake\Validation\Validator;

class FundraisingsTable extends Table {

	protected $_activeStatuses = ['A', 'R'];

	protected $_order = ['name', 'created' => 'DESC'];

	protected $_types = [
		'B' => 'Block',
		'P' => 'Pledge',
		'M' => 'Match',
		//'D' => 'Donation'
	];

	public function initialize(array $config) {

		parent::initialize($config);

		$this->addAssociations([
			'hasMany' => [
				'FundraisingPledges',
				'FundraisingUsers'
			],
			'belongsTo' => [
				'Users',
				'Initiatives'
			],
			'belongsToMany' => [
				'Projects' => [
					'joinTable' => 'fundraisings_projects'
				]
			]
		]);

	}

	public function validationDefault(Validator $validator) {

		return $validator
			->notEmpty('name')
			->notEmpty('description')
			->notEmpty('amount')
			->notEmpty('type');

	}

	public function beforeMarshal(Event $event, \ArrayObject $data, \ArrayObject $options) {

		if (isset($data['amount'])) {
			$data['amount'] = str_replace(',', '', $data['amount']);
		}

	}

	public function types() {

		return $this->_types;

	}

}