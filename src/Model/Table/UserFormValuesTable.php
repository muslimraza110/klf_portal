<?php
namespace App\Model\Table;

class UserFormValuesTable extends Table {
	
	public function initialize(array $config) {
		parent::initialize($config);
		
    $this->addAssociations([
			'belongsTo' => [
				'UserForms',
				'FormFields'
			]
    ]);
  }

}