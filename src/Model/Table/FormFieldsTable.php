<?php
namespace App\Model\Table;

class FormFieldsTable extends Table {
	
	protected $_order = 'order_num';
	
	public $types = [
		'text_block' => 'Text Block', //key must be in $scope.typesNotFormFields in Admin/Forms/_field_form.ctp
		
		'text' => 'Single-Line Text',
		'textarea' => 'Multi-Line Text',
		'dropdown' => 'Dropdown Menu', //key must be in $scope.typesWithOptions in Admin/Forms/_field_form.ctp
		'checkbox' => 'Single Checkbox',
		'checkboxes' => 'Multiple Checkboxes', //key must be in $scope.typesWithOptions in Admin/Forms/_field_form.ctp
		'radio' => 'Radio Options', //key must be in $scope.typesWithOptions in Admin/Forms/_field_form.ctp
		'email' => 'Email',
		'telephone' => 'Telephone' 
	];
	
	public function initialize(array $config) {
		parent::initialize($config);
		
    $this->addAssociations([
			'belongsTo' => [
				'Forms'
			],
			'hasMany' => [
				'UserFormValues' => [
					'dependent' => true //@TODO: does not work with trash
				]
			]
    ]);
  }

  public function validationDefault(\Cake\Validation\Validator $validator) {
		return $validator
			->notEmpty('name', 'Required')
			->notEmpty('type', 'Required');
	}
	
}