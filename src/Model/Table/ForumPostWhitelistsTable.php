<?php
namespace App\Model\Table;

class ForumPostWhitelistsTable extends Table {

	protected $_order = ['model', 'modified' => 'DESC'];

	public function initialize(array $config) {

		parent::initialize($config);

		$this->addAssociations([
			'belongsTo' => [
				'ForumPosts'
			]
		]);

	}

}