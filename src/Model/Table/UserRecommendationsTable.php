<?php

namespace App\Model\Table;

use Cake\Validation\Validator;

class UserRecommendationsTable extends Table {

	protected $_order = ['first_name', 'last_name'];

	public function initialize(array $config) {

		parent::initialize($config);

		$this->addAssociations([
			'belongsTo' => [
				'Users',
				'Recommenders' => [
					'className' => 'Users'
				]
			],
			'hasMany' => [
				'UserRecommendationNotes',
				'Votes' => [
					'className' => 'UserVotes',
					'foreignKey' => 'entity_id',
					'conditions' => [
						'Votes.model' => 'UserRecommendations'
					]
				]
			],
			'belongsToMany' => [
				'TaggedUsers' => [
					'joinTable' => 'user_recommendations_users',
					'className' => 'Users',
					'targetForeignKey' => 'user_id'
				]
			]
		]);

	}

	public function validationDefault(Validator $validator) {

		return $validator
			->notEmpty('email')
			->email('email')
			->notEmpty('first_name')
			->notEmpty('last_name');

	}

}