<?php

namespace App\Model\Table;

use Cake\ORM\TableRegistry;

class EmailListsTable extends Table {

	public function initialize(array $config) {

		parent::initialize($config);

		$this->addAssociations([
			'belongsTo' => [
				'Users',
			],
			'belongsToMany' => [
				'EmailReceivers' => [
					'joinTable' => 'email_lists_users',
					'className' => 'Users',
				],
			]
		]);

	}

	public function validationDefault(\Cake\Validation\Validator $validator) {
		return $validator
			->notEmpty('title', 'Required');
	}


	public function getRoleGroupedUserList() {

		$Users = TableRegistry::get('Users');

		$users = $Users->find()
			->contain([
				'Groups'
			])
			->order(['role', 'last_name']);

		$roleGroupedUserListAll = $roleGroupedUserList = $roleGroupedUserListGroups = [];

		foreach ($users as $user) {

			$roleGroupedUserListAll['All'][$user->id] = $user->name;

			if ($user->role != 'W') {
				if(!empty($user->groups)) {
					$roleGroupedUserList[$Users->roles[$user->role]][$user->id] = $user->name;

					foreach ($user->groups as $group) {
						$roleGroupedUserListGroups[$group->name][$user->id] = $user->name;
					}

				} else {
					$roleGroupedUserList['Unassociated Contacts'][$user->id] = $user->name;
				}
			}
		}

		ksort($roleGroupedUserList);
		ksort($roleGroupedUserListGroups);

		return array_merge($roleGroupedUserListAll, $roleGroupedUserList, $roleGroupedUserListGroups);

	}

	public function getCustomUserList() {

		$Users = TableRegistry::get('Users');

		$users = $Users->find()
			->contain([
				'Groups'
			])
			->order([
				'role',
				'last_name'
			]);

		$userList = [];

		foreach ($users as  $user) {

			$userList['Contact List']['All'][$user->id] = $user->name;

			if ($user->role != 'W') {

				if(!empty($user->groups)) {

					$userList['Contact List'][$Users->roles[$user->role]][$user->id] = $user->name;

					foreach ($user->groups as $group) {

						switch ($group->category_id) {
							case 2:
								$groupType = 'Business Contacts';
							break;
							case 3:
								$groupType = 'Charity Contacts';
							break;
							default:
								$groupType = '';
						}

						if (!empty($groupType)) {
							$userList[$groupType][$group->name][$user->id] = $user->name;
						}

					}

				} else {
					$userList['Contact List']['Unassociated Contacts'][$user->id] = $user->name;
				}

			}

		}

		ksort($userList['Business Contacts']);
		ksort($userList['Charity Contacts']);

		return $userList;

	}

	public function getAddedContactLists() {

		$emailLists = $this->find()
			->contain([
				'EmailReceivers'
			])
			->toArray();

		$contactList = [];

		foreach ($emailLists as $emailList) {

			foreach ($emailList->email_receivers as $receiver) {
				$contactList[$emailList->title][$receiver->id] = $receiver->name;
			}

		}

		return $contactList;

	}

	public function getAllEmailLists() {

		$contactList = $this->getRoleGroupedUserList();
		$addedContactList = $this->getAddedContactLists();

		if (!empty($addedContactList)) {
			$contactList = array_merge($contactList, $addedContactList);
		}

		return $contactList;

	}

	public function getGroupedEmailLists() {

		$Users = TableRegistry::get('Users');

		$emailLists = $this->find()
			->contain(['EmailReceivers']);

		$groupedEmailLists = [];

		foreach ($emailLists as $emailList) {
			$list = [];
			foreach ($emailList->email_receivers as $receiver) {
				$list[$Users->roles[$receiver->role]][]['name'] = $receiver->name;
			}
			$groupedEmailLists[] = [
				'id' => $emailList->id,
				'title' => $emailList->title,
				'receivers' => $list
			];
		}

		return $groupedEmailLists;

	}

	protected function _getReceiversFromLists($contactList) {

		return $this->find()
			->contain('EmailReceivers')
			->where(['EmailLists.id IN' => $contactList])
			->extract('email_receivers.{*}.id')
			->toArray();

	}

	public function getEmailReceivers($emailReceivers , $contactList) {

		$emailList = [];

		if (!empty($contactList)) {
			$emailList = $this->_getReceiversFromLists($contactList);
		}

		if (!empty($emailReceivers)) {
			$emailList = array_merge($emailReceivers, $emailList);
		}

		$Users = TableRegistry::get('Users');

		return $Users->find()
			->where(['Users.id IN' => array_unique($emailList)]);

	}

}