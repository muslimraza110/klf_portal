<?php
namespace App\Model\Table;

use Cake\I18n\Time;
use Cake\Validation\Validator;
use Cake\Core\Configure;
use Cake\Utility\Hash;

class UsersTable extends Table {

	protected $_activeStatuses = ['A'];

	protected $_order = ['first_name', 'last_name'];
	
	public $roles = [
		'A' => 'Administrator',
		'C' => 'Contact',
		'F' => 'Founder',
		'O' => 'Governing Board of Trustees',
		'W' => 'Copywriter',
		'R' => 'Organizing Committee'
	];

	public $noTitle = [
		'A',
		'W',
	];

	public $titles = [
		'Dr' => 'Dr.',
		'Mr' => 'Mr.',
		'Mrs' => 'Mrs.',
		'Ms' => 'Ms.'
	];

	public $genders = [
		'M' => 'Male',
		'F' => 'Female'
	];

	public function initialize(array $config) {
		parent::initialize($config);
		
		$this->displayField('name');

		$this->addAssociations([
			'hasMany' => [
				'Posts',
				'ForumPosts',
				'ForumLikes',
				'UserForms',
				'ChatInteractions',
				'NotificationSettings',
				'UserVisits',
				'Emails',
        'UserComments',
				'UserHistories',
				'UserVotes' => [
					'foreignKey' => 'voter_id'
				],
				'UserInvitations',
				'UserRecommendations' => [
					'foreignKey' => 'recommender_id'
				],
				'UserRecommendationNotes',
				'UserFamilies',
				'RelatedUserFamilies' => [
					'className' => 'UserFamilies',
					'foreignKey' => 'related_user_id'
				],
				'GroupRequests',
				'Events',
				'ProjectNotes',
				'Bugs',
				'CreatedProjects' => [
					'className' => 'Projects',
					'foreignKey' => 'creator_id'
				],
				'ProjectRequests',
				'EmailLists',
				'Fundraisings',
				'FundraisingPledges',
				'Initiatives',
				'InitiativeEblasts',
				'Initiatives',
				'ForumPostWhitelists' => [
					'foreignKey' => 'model_id',
					'conditions' => [
						'ForumPostWhitelists.model' => 'UsersTable'
					]
				],
				'ForumPostSubscriptions'
			],
			'hasOne' => [
				'UserDetails'
			],
			'belongsToMany' => [
				'Groups' => [
					'targetForeignKey' => 'group_id'
				],
				'ChatSessions',
				'ModeratedForumPosts' => [
					'joinTable' => 'forum_posts_moderators',
					'className' => 'ForumPosts',
					'foreignKey' => 'moderator_id',
					'targetForeignKey' => 'forum_post_id'
				],
				'TaggedUserRecommendations' => [
					'joinTable' => 'user_recommendations_users',
					'className' => 'UserRecommendations',
					'targetForeignKey' => 'user_recommendation_id'
				],
				'Projects' => [
					'joinTable' => 'projects_users'
				],
				'ContactList' => [
					'joinTable' => 'email_lists_users'
				]
			]
		]);
		
		
		$this->addBehavior('AssetManager.Asset', [
			'hasOne' => [
				'ProfileImg' => [
					'type' => 'image',
					'ratio' => 1
				],
				'BannerImg' => [
					'type' => 'image'
				]
			]
		]);
		
	}
	
	public function getUser($id) {
		
		return $this->findById($id)
			->contain([
				'UserDetails',
        'UserComments',
				'ProfileImg',
				'BannerImg',
				'Groups.ProfileImg',
				'UserFamilies.RelatedUsers.ProfileImg',
				'ForumPostSubscriptions.ForumPosts'
			])
			->first();
	}
	
	public function validationDefault(Validator $validator) {
		return $validator
			
			->notEmpty('first_name', 'Your first name is required')
			->notEmpty('last_name', 'Your last name is required')
			
			->notEmpty('gender', 'Gender is required')
			->notEmpty('title', 'Your title is required')

			->add('role', 'validRole', [
				'rule' => ['inList', array_keys($this->roles)],
				'message' => 'Select a role from the list'
			])
			
			->requirePresence('email', 'create')
			->notEmpty('email', 'An email is required')
			->add('email', [
				'email' => [
					'rule' => 'email',
					'message' => 'Invalid email address',
				],
				'unique' => [
					'rule' => 'validateUnique',
					'provider' => 'table',
					'message' => 'This email is already registered.'
				]
			])
			
			->notEmpty('password', 'A password is required', 'create')
			->add('password', 'securePassword', [
				'rule' => ['custom', '/^(?=.*?[A-Z])(?=(.*[\d]){1,})(?!.*\s).{6,}$/'],
				'message' => 'Your password must contain at least 6 characters including one capital letter and one number.',
				/*
				'rule' => ['custom', "/(?=.*?[a-zA-Z])(?=.*\\d)(?!.*\\s).{8,24}$/"],
				'message' => 'The password must be between 8 and 24 characters and must include at least 1 capital and lowercase letter and 1 number.'
				*/
				'on' => function ($context) {
					return (Configure::read('debug')) ? false : true;
				}
			])
			
			->notEmpty('confirm_password', 'Please re-enter your password', function ($context) {
				
				if ($context['newRecord'])
					return true;
				
				if (Hash::check($context, 'data.password'))
					$password = $context['data']['password'];
				
				return !empty($password);
				
			})
			->add('confirm_password', 'compareWithPassword', [
				'rule' => ['compareWith', 'password'],
				'message' => 'Passwords do not match.',
			]);
	}

	public function beforeSave(\Cake\Event\Event $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options) {

		if ($entity->dirty('role')) {

			if ($entity->role == 'O' && empty($entity->expiration_date))
				$entity->expiration_date = Time::now()->addYear();
			elseif ($entity->role != 'O')
				$entity->expiration_date = null;

		}

	}

	public function afterSave(\Cake\Event\Event $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options) {

		if ($entity->dirty('role')) {

			$editorId = (new \Cake\Network\Session)->read('Auth.User.id');

			if (empty($editorId))
				$editorId = null;

			$userHistory = $this->UserHistories->newEntity([
				'user_id' => $entity->id,
				'editor_id' => $editorId,
				'old_role' => $entity->getOriginal('role'),
				'new_role' => $entity->role
			]);

			$this->UserHistories->save($userHistory);

		}

	}
	
}