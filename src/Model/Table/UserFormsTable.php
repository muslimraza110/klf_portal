<?php
namespace App\Model\Table;

class UserFormsTable extends Table {
	
	public function initialize(array $config) {
		parent::initialize($config);
		
    $this->addAssociations([
			'belongsTo' => [
				'Forms',
				'Users'
			],
			'hasMany' => [
				'UserFormValues' => [
					'dependent' => true,
				]
			]
    ]);
  }

}