<?php
namespace App\Model\Table;

use Cake\Validation\Validator;


class UserCommentsTable extends Table {

  protected $_activeStatuses = ['A'];

	public function initialize(array $config) {
		parent::initialize($config);

    $this->addAssociations([
			'belongsTo' => [
				'Users',
			],
    ]);
  }



}
