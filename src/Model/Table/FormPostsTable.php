<?php

namespace App\Model\Table;

class FormPostsTable extends Table {

	public function initialize(array $config) {

		parent::initialize($config);

		$this->belongsTo('Forms', [
			'foreignKey' => 'form_id',
		]);

	}

	public function validationDefault(\Cake\Validation\Validator $validator) {
		return $validator
			->notEmpty('content', 'Required');
	}

}