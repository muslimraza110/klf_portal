<?php

namespace App\Model\Table;

use Cake\Validation\Validator;

class ProjectNotesTable extends Table {

	protected $_order = ['created' => 'DESC'];

	public function initialize(array $config) {

		parent::initialize($config);

		$this->addAssociations([
			'belongsTo' => [
				'Projects',
				'Users'
			]
		]);

	}

	public function validationDefault(Validator $validator) {

		return $validator
			->notEmpty('note');

	}

}