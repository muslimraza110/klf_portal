<?php
namespace App\Model\Table;

use Cake\Validation\Validator;

class GroupsTable extends Table {

	protected $_activeStatuses = ['A'];

	protected $_order = ['name'];

	public $statuses = [
		'A' => 'Active',
		'D' => 'Deleted'
	];

	public function initialize(array $config) {
		parent::initialize($config);

		$this->addAssociations([
			'hasMany' => [
				'GroupRequests',
				'Events' => [
					'conditions' =>[
						'project_id' => 0
					],
					'sort' => [
						'Events.start_date' => 'DESC'
					]
				],
				'ForumPostWhitelists' => [
					'foreignKey' => 'model_id',
					'conditions' => [
						'ForumPostWhitelists.model' => 'GroupsTable'
					]
				]
			],
			'belongsTo' => [
				'Categories' => [
					'conditions' => [
						'Categories.model' => 'Groups'
					]
				]
			],
			'belongsToMany' => [
				'Users' => [
					'joinTable' => 'groups_users'
				],
				'ForumPosts' => [
					'joinTable' => 'forum_posts_groups',
				],
				'Tags' => [
					'joinTable' => 'groups_tags'
				],
				'Projects' => [
					'joinTable' => 'projects_groups'
				],
				'Initiatives' => [
					'joinTable' => 'initiatives_groups'
				],
			]
		]);

		$this->addBehavior('AssetManager.Asset', [
			'hasOne' => [
				'ProfileImg' => [
					'type' => 'image',
					'ratio' => 1
				],
				'BannerImg' => [
					'type' => 'image'
				]
			]
		]);

	}

	public function getGroup($id) {

		return $this->findById($id)
			->contain([
				'Events.Users',
				'Projects' => [
					'Users',
					'Events.Projects.Users'
				],
				'ForumPosts' => function ($q) {

					return $q->contain('Users')
						->where(['ForumPosts.parent_id IS' => null]);

				},
				'ProfileImg',
				'BannerImg',
				'Categories',
				'Users' => [
					'ProfileImg',
					'conditions' => [
						'Users.pending' => false
					]
				],
				'Tags',
				'GroupRequests' => [
					'Users.ProfileImg',
					'conditions' => [
						'GroupRequests.status' => 'P'
					]
				]
			])
			->first();
	}

	public function getProjects($id) {

		return $this->Projects->findByGroupId($id)
			->toArray();
	}

	public function validationDefault(Validator $validator) {
		return $validator
			->notEmpty('address', 'Address is required')
			->allowEmpty('description')
			->add('description', [
				'max250' => [
					'rule' => ['maxLength', 250],
					'message' => 'The character limit is 250',
				]
			]);
	}

	public function buildUserRoleConditions(\App\Model\Entity\User $authUser, $modelName = 'Groups') {

		$groupIds = $authUser->getGroupIds();

		if (empty($groupIds))
			$groupIds = [0];

		$conditions = [];

		if (!in_array($authUser->role, ['A'])) {

			$conditions['OR'][$modelName.'.hidden'] = false;
			$conditions['OR'][$modelName.'.id IN'] = $groupIds;

		}

		if (!in_array($authUser->role, ['A', 'O'])) {

			if (!empty($conditions['OR'])) {

				$conditions[]['OR'] = $conditions['OR'];

				unset($conditions['OR']);

				$conditions[]['OR'] = [
					$modelName.'.pending' => false,
					$modelName.'.id IN' => $groupIds
				];

			} else {

				$conditions['OR'][$modelName.'.pending'] = false;
				$conditions['OR'][$modelName.'.id IN'] = $groupIds;

			}

		}

		return $conditions;

	}

	public function categorize(array $groups) {

		$categories = $this->Categories->findByModel('Groups')
			->combine('id', 'name')
			->toArray();

		$return = [];

		foreach ($groups as $group) {

			$categoryName = $categories[$group->category_id];

			if (!isset($return[$categoryName]))
				$return[$categoryName] = [];

			$return[$categoryName][] = $group;

		}

		return $return;

	}

}
