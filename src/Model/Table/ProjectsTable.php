<?php

namespace App\Model\Table;

use Cake\Validation\Validator;

class ProjectsTable extends Table {

	protected $_activeStatuses = ['A', 'R'];

	protected $_order = ['approval_stamp', 'name', 'created' => 'DESC'];

	public function initialize(array $config) {

		parent::initialize($config);

		$this->addAssociations([
			'belongsTo' => [
				'Groups',
				'Creators' => [
					'className' => 'Users'
				],
				'KhojaCareCategories'
			],
			'hasMany' => [
				'ProjectNotes',
				'Events' => [
					'conditions' => [
						'Events.project_id !=' => 0 
					],
					'sort' => [
						'Events.start_date' => 'DESC'
					]
				]	,
				'ProjectRequests'
			],
			'belongsToMany' => [
				'Tags' => [
					'joinTable' => 'projects_tags'
				],
				'Users' => [
					'joinTable' => 'projects_users'
				],
				'ForumPosts' => [
					'joinTable' => 'forum_posts_projects'
				],
				'Groups' => [
					'joinTable' => 'projects_groups'
				],
				'Initiatives' => [
					'joinTable' => 'initiatives_projects'
				],
				'Fundraisings' => [
					'joinTable' => 'fundraisings_projects'
				]
			],

		]);

	}

	public function validationDefault(Validator $validator) {

		return $validator
			->notEmpty('group_id')
			->notEmpty('name')
			->notEmpty('description')
			->notEmpty('start_date')
			->notEmpty('city')
			// ->notEmpty('region')
			->notEmpty('country');

	}

	public function buildUserRoleConditions(\App\Model\Entity\User $authUser, $modelName = 'Projects') {

		$projectIds = $authUser->getProjectIds();

		if (empty($projectIds))
			$projectIds = [0];

		$conditions = [];

		if (!in_array($authUser->role, ['A'])) {

			$conditions['OR'][$modelName.'.id IN'] = $projectIds;

		}

		if (!in_array($authUser->role, ['A', 'O'])) {

			if (!empty($conditions['OR'])) {

				$conditions[]['OR'] = $conditions['OR'];

				unset($conditions['OR']);

				$conditions[]['OR'] = [
					$modelName.'.id IN' => $projectIds
				];

			} else {

				$conditions['OR'][$modelName.'.pending'] = false;
				$conditions['OR'][$modelName.'.id IN'] = $projectIds;

			}

		}

		return $conditions;

	}

	public function khojaCareCategories() {

		$projectsKhojaCareCategoryIds = $this->find()
			->where([
				'Projects.khoja_care_category_id !=' => 0,
				'Projects.approval_stamp' => 1
			])
			->distinct('khoja_care_category_id')
			->extract('khoja_care_category_id')
			->toArray();

		return $this->KhojaCareCategories->find('list', [
			'contain' => [
				'Projects'
			],
			'order' => [
				'KhojaCareCategories.position' => 'ASC'
			],
			'conditions' => [
					'KhojaCareCategories.id IN' => $projectsKhojaCareCategoryIds,
			]
		]);

	}

	public function projectGroups($id) {

		return $this->Groups->find()
		 ->matching('ProjectsGroups', function ($query) use($id) {
			 return $query->where([
				 'ProjectsGroups.project_id' => $id
			 ]);
		 });

	}


}
