<?php
namespace App\Model\Table;

use App\Model\Entity\Event;
use Cake\Datasource\EntityInterface;
use Cake\Validation\Validator;

class CategoriesTable extends Table {
	
	protected $_order = ['name'];

	
	public function initialize(array $config) {
		parent::initialize($config);
		
		$this->addAssociations([
			'hasMany' => [
				'Groups',
				'Posts',
				'Bugs'
			]
		]);
	}
	
	public function beforeDelete(Event $event, EntityInterface $entity, \ArrayObject $options) {
		
		if (! $entity->canDelete()) {
			return false;
		}
	}
	
	public function validationDefault(Validator $validator) {
		return $validator
			->notEmpty('name', 'Required')
			->notEmpty('model', 'Required');
	}


	public function newsCategories() {

		return [1, 4];

	}
	
}