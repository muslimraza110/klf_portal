<?php
namespace App\Model\Table;

class InitiativePinsTable extends Table {

	public function initialize(array $config) {
		parent::initialize($config);

		$this->addAssociations([
			'belongsTo' => [
				'Initiatives'
			]
		]);
	}

}