<?php

namespace App\Model\Table;

use Cake\Validation\Validator;

class UserFamiliesTable extends Table {

	protected $_order = ['relation'];

	protected $relations = [
		'Parent' => 'Parent',
		'Child' => 'Child',
		'Sibling' => 'Sibling',
		'Cousin' => 'Cousin',
		'Grandparent' => 'Grandparent',
		'Grandchild' => 'Grandchild',
		'Uncle' => 'Uncle',
		'Aunt' => 'Aunt'
	];

	public function initialize(array $config) {

		parent::initialize($config);

		$this->addAssociations([
			'belongsTo' => [
				'Users',
				'RelatedUsers' => [
					'className' => 'Users'
				]
			]
		]);

	}

	public function validationDefault(Validator $validator) {

		return $validator
			->notEmpty('relation');

	}

	public function getRelations() {

		return $this->relations;

	}

}