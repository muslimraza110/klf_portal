<?php
namespace App\Model\Table;

class PostThemesTable extends Table {
	
	public function initialize(array $config) {
		parent::initialize($config);
		
    $this->addAssociations([
			'hasMany' => [
				'Posts',
			],
			'belongsTo' => [
				'PostCategories',
			],
    ]);
  }

  public function validationDefault(\Cake\Validation\Validator $validator) {
		return $validator
			->notEmpty('name', 'Required');
	}

	public function beforeDelete($event, $entity, $options) {
		if (! $entity->canDelete()) {
			SessionComponent::setFlash('This theme cannot be deleted because one or more posts are using it.');
			return false;
		}
	}
	
}