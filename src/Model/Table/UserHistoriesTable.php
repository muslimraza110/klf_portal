<?php

namespace App\Model\Table;

class UserHistoriesTable extends Table {

	protected $_order = [
		'created' => 'DESC'
	];

	public function initialize(array $config) {

		parent::initialize($config);

		$this->addAssociations([
			'belongsTo' => [
				'Users',
				'Editors' => [
					'className' => 'Users'
				]
			]
		]);

	}

}