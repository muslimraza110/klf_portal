<?php

namespace App\Model\Table;

class FilesTable extends Table {

	public function initialize(array $config) {

		parent::initialize($config);

		$this->belongsTo('Folders');

		$this->addBehavior('AssetManager.Asset', [
			'hasOne' => [
				'resource' => [
					'type' => 'file'
				]
			]
		]);

	}

}