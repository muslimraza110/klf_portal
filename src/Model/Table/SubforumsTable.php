<?php
namespace App\Model\Table;


class SubforumsTable extends Table {


	protected $_activeStatuses = ['A'];

	public function initialize(array $config) {
		parent::initialize($config);


		$this->addAssociations([
			'belongsTo' => [
				'Users',
			],
			'hasMany' => [
				'ForumPosts' => [
					'joinTable' => 'subforum_groups'
				],
			],
		]);
	}

	public function validationDefault(\Cake\Validation\Validator $validator) {
		return $validator
			->notEmpty('title', 'Required');
	}


}