<?php
namespace App\Model\Table;

use Cake\Log\Log;

class ChatSessionsTable extends Table {
	
	protected $_order = ['created' => 'DESC'];

  public function initialize(array $config) {
		parent::initialize($config);
		
    $this->addAssociations([
			'hasMany' => [
				'ChatInteractions'
			],
			'belongsToMany' => [
				'Users',
			]
    ]);
  }
	
	public function getChatSession($userIds) {
		
		if (count($userIds) < 2)
			return null;
		
		$matchingIds = [];
		
		foreach($userIds as $userId) {
			$sessionIds = $this->find()
				->where(['ChatSessions.total_users' => count($userIds)])
				->matching('Users', function ($q) use ($userId) {
					return $q->where([
						'Users.id' => $userId
					]);
				})
				->extract('id')
				->toArray();
			
			// USER HAS NO SESSIONS
			if (empty($sessionIds)) {
				return null;
			}
			
			// FIRST USER
			if (empty($matchingIds)) {
				$matchingIds = $sessionIds;
			}
			// ELSE FIND COMMON IDS
			else {
				$matchingIds = array_intersect($matchingIds, $sessionIds);
			}
			
			// STOP LOOPING IF NO MATCHING IDS
			if (empty($matchingIds)) {
				return null;
			}
			
		}
		
		// ERROR IN DB - THERE SHOULD NOT BE MORE THAN ONE
		if (count($matchingIds) > 1) {
			$matchingIdsCopy = $matchingIds;
			$goodId = array_pop($matchingIdsCopy);
			
			asort($userIds);
			$userIdsForFilename = implode('_', $userIds);
			
			Log::config('chat_errors', [
				'className' => 'Cake\Log\Engine\FileLog',
				'path' => LOGS,
				'levels' => [],
				'scopes' => ['chat'],
				'file' => 'chat_errors_' . $userIdsForFilename,
			]);
			
			foreach($matchingIdsCopy as $chatSessionId) {
				Log::warning('Chat session ID ' . $chatSessionId . ' to be merged with ID ' . $goodId, ['scope' => ['chat']]);
			}
		}
		
		$id = array_pop($matchingIds);

		return $this->find()
			->where([
				'ChatSessions.id' => $id
			])
			->contain([
				'ChatInteractions' => function ($q) {
					return $q->limit(25);
				},
				'Users' => function ($q) {
					return $q->select(['id']);
				}
			])
			->first();
		;
	}

}