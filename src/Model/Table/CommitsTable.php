<?php
namespace App\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;

class CommitsTable extends Table {

	public function initialize(array $config) {
		parent::initialize($config);

	}

	public function types() {
		return [
			'S' => 'Sandbox',
			'P' => 'Portal'
		];
	}

}