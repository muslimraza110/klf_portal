<?php

namespace App\Model\Table;

class FoldersTable extends Table {

	protected $_order = 'position';

	public function initialize(array $config) {

		parent::initialize($config);

		$this->belongsTo('Parent', [
			'className' => 'Folders',
			'foreignKey' => 'parent_id'
		]);

		$this->hasMany('Files');

		$this->addBehavior('Tree', [
			'level' => 'level'
		]);

		$this->addBehavior('Timestamp');

	}

	public function saveRecursiveList($list, $parentId = null) {

		if (empty($parentId))
			$this->recover();

		foreach($list as $key => $currentList) {

			$category = $this->get($currentList['id']);

			$category->parent_id = $parentId;
			$category->position = $key;

			$this->save($category);

			if (!empty($currentList['children']))
				$this->saveRecursiveList($currentList['children'], $category->id);

		}

	}

}