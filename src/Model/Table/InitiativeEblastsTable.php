<?php
namespace App\Model\Table;

use Cake\ORM\RulesChecker;

class InitiativeEblastsTable extends Table {

	public function initialize(array $config) {
		parent::initialize($config);
		$this->addAssociations([
			'belongsTo' => [
				'Users',
				'Initiatives'
			]
		]);
	}

	public function buildRules(RulesChecker $rules) {

		$rules->add($rules->existsIn(['initiative_id'], 'Initiatives'));
		$rules->add($rules->existsIn(['user_id'], 'Users'));
		return $rules;

	}

}
