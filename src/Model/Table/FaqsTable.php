<?php
namespace App\Model\Table;

class FaqsTable extends Table {

	protected $_activeStatuses = ['A'];

	public function initialize(array $config) {

		parent::initialize($config);

		$this->addBehavior('Tree', [
			'level' => 'level'
		]);

	}


	public function validationDefault(\Cake\Validation\Validator $validator) {

		return $validator
			->add('title', [
				'unique' => [
					'rule' => 'validateName',
					'provider' => 'table',
					'message' => 'This FAQ is already registered.'
				]
			])
			->notEmpty('title','Title is required');

	}

	public function validateName($value, $context) {

		$conditions['Faqs.title'] = $value;

		if (isset($context['data']['id'])) {
			$conditions['Faqs.id !='] = $context['data']['id'];
		}

		if (isset($context['data']['parent_id'])) {

			$faq = $this->find()
				->where([
					'Faqs.parent_id' => $context['data']['parent_id'],
					'Faqs.status' => 'A',
					$conditions
				])
				->first();

		} else {

			$faq = $this->find()
				->where([
					'Faqs.parent_id is' => null,
					'Faqs.status' => 'A',
					$conditions
				])
				->first();

		}

		if (empty($faq)) {
			return true;
		}

		return false;

	}

	public function multiLevelFaqsList() {

		$faqs = $this->find()
			->where([
				'Faqs.parent_id is' => null,
				'Faqs.status' => 'A',
			])
			->order('Faqs.position');

		$faqsList = [];

		foreach ($faqs as $index => $faq) {



			$children = $this->find('children', ['for' => $faq->id])
				->find('threaded')
				->where(['Faqs.status' => 'A'])
				->toArray();

			$children = (new Collection($children))->sortBy('position', SORT_NUMERIC)->toArray();

			$faqsList[$faq->id] = [$faq->id => $faq->title];

			if (!empty($children)) {

				$faqsList[$faq->id] = $this->recursiveBuildList($faq, $children);

			}

		}

		return $faqsList;

	}

	public function recursiveBuildList($parentFaq, $children) {

		$faqs = [];

		if ($parentFaq->root_faq->id == $parentFaq->id) {
			$faqs[$parentFaq->id] = [
				'text' => $parentFaq->title,
				'value' => $parentFaq->id,
				'style' => 'font-weight:bold'
			];
		} else {
			$faqs[$parentFaq->id] = [
				'text' => str_repeat(
						html_entity_decode('&#160;&#160;&#160;&#160;&#160;&#160;'),
						$parentFaq->level
					) . html_entity_decode('&#9658;') . $parentFaq->title,
				'value' => $parentFaq->id,
				'style' => 'font-weight:bold'
			];
		}

		foreach ($children as $child) {

			if ($child->isParent()) {

				$child->children = (new Collection($child->children))->sortBy('position', SORT_NUMERIC)->toArray();

				$faqs += $this->recursiveBuildList($child, $child->children);

			} else {

				$faqs[$child->id] = [
					'text' => str_repeat(
							html_entity_decode('&#160;&#160;&#160;&#160;&#160;&#160;'),
							$child->level
						) . html_entity_decode('&#9658;') . $child->title,
					'value' => $child->id
				];

			}

		}

		return $faqs;

	}

	public function rebuildTree($currentFaq, $position, $parentId, $root) {

		if (!empty($currentFaq['children'])) {
			foreach ($currentFaq['children'] as $childPosition => $child) {
				if ($root) {
					$this->rebuildTree($child, $childPosition, $parentId, false);
				} else {
					$this->rebuildTree($child, $childPosition, $currentFaq['id'], false);
				}
			}
		}

		$faq = $this->find()
			->where([
				'Faqs.id' => $currentFaq['id']
			])
			->first();

		$faq->position = $position;

		if (!$root) {
			$faq->parent_id = $parentId;
		} else {
			$faq->parent_id = null;
		}

		$this->save($faq);

	}

	public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options) {

		$data['user_id'] = (new \Cake\Network\Session)->read('Auth.User.id');

	}


}
