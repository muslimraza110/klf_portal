<?php
namespace App\Model\Table;

class FormsTable extends Table {

	protected $_activeStatuses = ['A'];

	protected $_order = [
		//'CASE WHEN Forms.start_date IS NULL THEN 0 ELSE 1 END' => '',
		//'start_date' => 'DESC',
		//'created' => 'DESC',
		//'name' => 'DESC'
		'id' => 'DESC'
	];

	public $types = [
		'F' => 'Form',
		'S' => 'Polls & Surveys'
	];

	public function initialize(array $config) {
		parent::initialize($config);

    $this->addAssociations([
			'belongsTo' => [
				'Users'
			],
			'hasMany' => [
				'FormFields' => [
					'dependent' => true
				],
				'UserForms' => [
					'dependent' => true
				],
			],
			'hasOne' => [
				'FormPosts' => [
					'dependent' => true
				]
			]
    ]);

    $this->addBehavior('AssetManager.Asset', [
			'hasOne' => [
				'FormUpload' => [
					'type' => 'file'
				]
			]
		]);
  }

  public function validationDefault(\Cake\Validation\Validator $validator) {
		return $validator
			->notEmpty('name', 'Required')
			->notEmpty('topic', 'Required');
	}

}