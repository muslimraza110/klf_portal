<?php
namespace App\Model\Table;

use Cake\ORM\Query;

class MessagesUsersTable extends Table 
{	
	public function initialize(array $config) 
	{
		parent::initialize($config);

		$this->addAssociations([
			'belongsTo' => [
				'Messages',
				'Users'
			]
		]);
	}

	public function validationDefault(\Cake\Validation\Validator $validator) 
  {
    return $validator
    	->notEmpty('message_id', 'Message is required')
    	->notEmpty('user_id', 'User is required')
    	->notEmpty('action', 'Action is required');
  }

  public function findInbox(Query $query, array $options)
  {    
    return $query
    	->contain($this->_containData())
	    ->where([
	    	'MessagesUsers.user_id' => $_SESSION['Auth']['User']['id'],
	    	'MessagesUsers.action IN' => ['T', 'C'],
	    	'MessagesUsers.archived' => 0,
        'MessagesUsers.trash' => 0,
	    	'NOT' => [
					'Messages.sent IS' => null
				]
	    ])
	    ->order([
  			'Messages.sent' => 'DESC'
  		]);;
  }

  public function findSent(Query $query, array $options)
  {
  	return $query
    	->contain($this->_containData())
	    ->where([
	    	'MessagesUsers.user_id' => $_SESSION['Auth']['User']['id'],
	    	'MessagesUsers.action' => 'F',
	    	'MessagesUsers.archived' => 0,
        'MessagesUsers.trash' => 0,
	    	'NOT' => [
					'Messages.sent IS' => null
				]
	    ])
	    ->order([
  			'Messages.sent'
  		]);;
  }

  public function findDraft(Query $query, array $options)
  {
  	return $query
  		->contain($this->_containData())
  		->where([
  			'MessagesUsers.user_id' => $_SESSION['Auth']['User']['id'],
  			'MessagesUsers.action' => 'F',
        'MessagesUsers.trash' => 0,
  			'Messages.sent IS' => null,
  		])
  		->order([
  			'Messages.created'
  		]);;
  }

  public function findArchive(Query $query, array $options)
  {
  	return $query
  		->contain($this->_containData())
  		->where([
  			'MessagesUsers.user_id' => $_SESSION['Auth']['User']['id'],
  			'MessagesUsers.action IN' => ['T', 'C'],
  			'MessagesUsers.archived' => 1,
        'MessagesUsers.trash' => 0,
  		])
  		->order([
  			'Messages.sent'
  		]);
  }

  public function findImportant(Query $query, array $options)
  {
    return $query
      ->contain($this->_containData())
      ->where([
        'MessagesUsers.user_id' => $_SESSION['Auth']['User']['id'],
        'MessagesUsers.action IN' => ['T', 'C'],
        'MessagesUsers.important' => 1,
        'MessagesUsers.trash' => 0,
      ])
      ->order([
        'Messages.sent'
      ]);
  }

  public function findTrash(Query $query, array $options)
  {
    return $query
      ->contain($this->_containData())
      ->where([
        'MessagesUsers.user_id' => $_SESSION['Auth']['User']['id'],
        'MessagesUsers.trash' => 1
      ]);
  }

  protected function _containData()
  {
  	return [
  		'Users',
  		'Messages',
  		'Messages.Senders.Users',
  		'Messages.Recipients.Users',
  		'Messages.CcRecipients.Users'
  	];
  }

  
}