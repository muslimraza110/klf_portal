<?php
namespace App\Model\Table;

class UserVisitsTable extends Table {

  public function initialize(array $config) {
		parent::initialize($config);

    $this->addAssociations([
      'belongsTo' => [
        'Users'
      ]
    ]);
  }
  
}
