<?php

namespace App\Model\Table;

class ProjectRequestsTable extends Table {

	protected $_order = ['created' => 'DESC'];

	protected $statuses = [
		'A' => 'Accepted',
		'P' => 'Pending',
		'R' => 'Rejected'
	];

	public function initialize(array $config) {

		parent::initialize($config);

		$this->addAssociations([
			'belongsTo' => [
				'Projects',
				'Users'
			]
		]);

	}

	public function getStatuses() {

		return $this->statuses;

	}

}