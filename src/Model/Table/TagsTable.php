<?php
namespace App\Model\Table;

use Cake\Collection\Collection;
use Cake\Validation\Validator;

class TagsTable extends Table {

	protected $_order = ['name'];

	public function initialize(array $config) {

		parent::initialize($config);

		$this->addBehavior('Tree', [
			'level' => 'level'
		]);

		$this->addAssociations([
			'belongsToMany' => [
				'Groups' => [
					'joinTable' => 'groups_tags'
				],
				'Projects'
			]
		]);

	}

	public function validationDefault(Validator $validator) {

		return $validator
			->add('name', [
				'unique' => [
					'rule' => 'validateName',
					'provider' => 'table',
					'message' => 'This Tag is already registered.'
				]
			])
			->notEmpty('name','Multi-level Tag is required');

	}

	public function validateName($value, $context) {

		$conditions['Tags.name'] = $value;

		if (isset($context['data']['id'])) {
			$conditions['Tags.id !='] = $context['data']['id'];
		}

		if (isset($context['data']['parent_id'])) {

			$tag = $this->find()
				->where([
					'Tags.parent_id' => $context['data']['parent_id'],
					'Tags.status' => 'A',
					$conditions
				])
				->first();

		} else {

			$tag = $this->find()
				->where([
					'Tags.parent_id is' => null,
					'Tags.status' => 'A',
					$conditions
				])
				->first();

		}

		if (empty($tag)) {
			return true;
		}

		return false;

	}

	public function saveTag($tagName) {

		$tag = $this->newEntity(['name' => $tagName]);
		$this->save($tag);

		return $tag;

	}

	public function multiLevelTagsList() {

		$tags = $this->find()
			->where([
				'Tags.type' => 'M',
				'Tags.parent_id is' => null,
				'Tags.status' => 'A',
			])
			->order('Tags.order_num');

		$tagsList = [];

		foreach ($tags as $index => $tag) {



			$children = $this->find('children', ['for' => $tag->id])
				->find('threaded')
				->where(['Tags.status' => 'A'])
				->toArray();

			$children = (new Collection($children))->sortBy('order_num', SORT_NUMERIC)->toArray();

			$tagsList[$tag->id] = [$tag->id => $tag->name];

			if (!empty($children)) {

				$tagsList[$tag->id] = $this->recursiveBuildList($tag, $children);

			}

		}

		return $tagsList;

	}

	public function recursiveBuildList($parentTag, $children) {

		$tags = [];

		if ($parentTag->root_tag->id == $parentTag->id) {
			$tags[$parentTag->id] = [
				'text' => $parentTag->name,
				'value' => $parentTag->id,
				'style' => 'font-weight:bold'
			];
		} else {
			$tags[$parentTag->id] = [
				'text' => str_repeat(
						html_entity_decode('&#160;&#160;&#160;&#160;&#160;&#160;'),
						$parentTag->level
					) . html_entity_decode('&#9658;') . $parentTag->name,
				'value' => $parentTag->id,
				'style' => 'font-weight:bold'
			];
		}

		foreach ($children as $child) {

			if ($child->isParent()) {

				$child->children = (new Collection($child->children))->sortBy('order_num', SORT_NUMERIC)->toArray();

				$tags += $this->recursiveBuildList($child, $child->children);

			} else {

				$tags[$child->id] = [
					'text' => str_repeat(
							html_entity_decode('&#160;&#160;&#160;&#160;&#160;&#160;'),
							$child->level
						) . html_entity_decode('&#9658;') . $child->name,
					'value' => $child->id
				];

			}

		}

		return $tags;

	}

	public function rebuildTree($currentTag, $orderNum, $parentId, $root) {

		if (!empty($currentTag['children'])) {
			foreach ($currentTag['children'] as $childOrderNum => $child) {
				if ($root) {
					$this->rebuildTree($child, $childOrderNum, $parentId, false);
				} else {
					$this->rebuildTree($child, $childOrderNum, $currentTag['id'], false);
				}
			}
		}

		$tag = $this->find()
			->where([
				'Tags.id' => $currentTag['id']
			])
			->first();

		$tag->order_num = $orderNum;

		if (!$root) {
			$tag->parent_id = $parentId;
		} else {
			$tag->parent_id = null;
		}

		$this->save($tag);

	}

}
