<?php
namespace App\Model\Table;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;

class BugsTable extends Table {
	
	protected $_order = [
		'created' => 'DESC'
	];

  public function initialize(array $config) {
    parent::initialize($config);

    $this->addAssociations([
			'belongsTo' => [
				'Users',
        'Categories' => [
        	'conditions' => [
        		'model' => 'Bugs'
					]
				],
				'BugCategories'
			],// From ITC, doesn't seem applicable to khoja, but keeping for now just in case
			'hasMany' => [
				'Assets' => [
					'foreignKey' => 'model_id',
					'conditions' => [
						'Assets.association' => 'Bugs'
					]
				]
			]
    ]);
  }

  public function afterSave(Event $event, EntityInterface $entity, \ArrayObject $options) {

    if ($entity->complete) {
      $entity->sendEmail();
    }

  }

  public function priorities() {
    return [
      1 => 'Critical',
      2 => 'High',
      3 => 'Medium',
      4 => 'Low',
			5 => 'Feedback',
			6 => 'Redo',
    ];
  }
  public function labels() {
    return [
			6 => 'danger',
      1 => 'danger',
      2 => 'warning',
      3 => 'success',
      4 => 'default',
			5 => 'info',
    ];
  }

  public function statuses() {
    return [
      4 => 'Committed',
      2 => 'Completed',
      3 => 'Verified'
    ];
  }

}