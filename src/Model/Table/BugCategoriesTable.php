<?php
namespace App\Model\Table;

class BugCategoriesTable extends Table {

	public function initialize(array $config) {
		parent::initialize($config);

		$this->addAssociations([
			'hasMany' => [
				'Bugs'
			]
		]);
	}

}
