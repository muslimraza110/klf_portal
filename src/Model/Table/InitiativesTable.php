<?php
namespace App\Model\Table;

use Cake\I18n\Time;
use Cake\Validation\Validator;

class InitiativesTable extends Table {


	public function initialize(array $config) {

		parent::initialize($config);

		$this->addAssociations([
			'hasMany' => [
				'Fundraisings',
				'InitiativeEblasts' => [
					'sort' => [
						'InitiativeEblasts.created' => 'DESC'
					]
				],
				'InitiativePins'
			],
			'belongsTo' => [
				'Users'
			],
			'belongsToMany' => [
				'Projects' => [
					'joinTable' => 'initiatives_projects'
				],
				'Groups' => [
					'joinTable' => 'initiatives_groups'
				]
			]
		]);

	}

	public function validationDefault(Validator $validator) {

		return $validator
			->notEmpty('name')
			->notEmpty('description');

	}

	public function updatePinnedEmail($id, $pin, $date) {

		$date = Time::createFromTimestamp($date);

		if ($pin) {

			$initiativePin = $this->InitiativePins->newEntity([
				'initiative_id' => $id,
				'date_group' => $date
			]);

			$this->InitiativePins->save($initiativePin);


		} else {

			$initiativePin = $this->InitiativePins->find()
				->where([
					'DATE_FORMAT(InitiativePins.date_group, "%m%d%y%h%i") = ' . $date->format('mdyhi'),
					'InitiativePins.initiative_id' => $id
				])
				->first();

			$this->InitiativePins->delete($initiativePin);

		}

		return true;

	}

}