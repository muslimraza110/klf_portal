<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Mailer\Email;

// */2 * * * * /var/www/khoja/bin/cake EmailQueue >/dev/null 2>&1

class EmailQueueShell extends Shell {

	public function initialize() {

		parent::initialize();

		$this->loadModel('Emails');

	}

	public function main() {

		$queuedEmails = $this->Emails->find()
			->where([
				'Emails.error IS' => null,
				'Emails.sent IS' => null,
				'Emails.queued' => true
			])
			->order([
				'Emails.created'
			])
			->limit(50);

		if (Configure::read('debug')) {

			$debugEmailAddress = Configure::read('debuggingEmail');

			if (empty($debugEmailAddress)) {
				$debugEmailAddress = 'junji@incodeapps.com';
			}

		}

		$attachments = [];

		foreach ($queuedEmails as $queuedEmail) {

			$email = (new Email)->unserialize($queuedEmail->email_object);

			$email->set('_to', $email->to());

			if (isset($debugEmailAddress)) {
				$email->to($debugEmailAddress);
			}

			try {
				$email->send();
			} catch (\Exception $e) {
				$queuedEmail->error = $e->getMessage();
			}

			if (empty($queuedEmail->error)) {
				$queuedEmail->sent = Time::now();
				$queuedEmail->queued = false;
				$queuedEmail->html_content = $email->message('html');
			}

			$this->Emails->save($queuedEmail);

			if (!empty($email->attachments())) {

				$attachments = $email->attachments();

				if ($this->_canDeleteAttachment($attachments)) {

					foreach (array_keys($attachments) as $name) {

						unlink(ROOT . DS . 'tmp' . DS . $name);

					}

				}

			}

		}

	}

	private function _canDeleteAttachment($attachments) {

		$queuedEmail = $this->Emails->find()
			->where([
				'Emails.error IS' => null,
				'Emails.sent IS' => null,
				'Emails.queued' => true,
				'Emails.attachments' => json_encode(array_keys($attachments))
			])
			->order([
				'Emails.created'
			])
			->first();

		if (empty($queuedEmail)) {
			return true;
		}

		return false;

	}

}