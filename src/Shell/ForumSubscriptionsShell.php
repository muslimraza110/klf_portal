<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Mailer\Email;
use Cake\Routing\Router;

// Assuming UTC!
// */1 * * * * /var/www/khoja/bin/cake ForumSubscriptions immediately >/dev/null 2>&1
// 5 2 * * * /var/www/khoja/bin/cake ForumSubscriptions daily >/dev/null 2>&1
// 5 2 * * 5 /var/www/khoja/bin/cake ForumSubscriptions weekly >/dev/null 2>&1

class ForumSubscriptionsShell extends Shell {

	public function initialize() {

		parent::initialize();

		$this->loadModel('Emails');
		$this->loadModel('ForumPostSubscriptions');

	}

	public function immediately() {

		$time = Time::now();

		$startTime = Time::createFromTime($time->hour, $time->minute, 0)->subMinutes(2);

		$endTime = clone $startTime;
		$endTime->addSeconds(59);

		$this->_createNotifications('I', $startTime, $endTime);

	}

	public function daily() {

		$time = Time::now();

		$startTime = Time::createFromTime($time->hour, 0, 0)->subDay();

		$endTime = clone $startTime;
		$endTime->addDay()->subSecond();

		$this->_createNotifications('D', $startTime, $endTime);

	}

	public function weekly() {

		$time = Time::now();

		$startTime = Time::createFromTime($time->hour, 0, 0)->subDays(7);

		$endTime = clone $startTime;
		$endTime->addDays(7)->subSecond();

		$this->_createNotifications('W', $startTime, $endTime);

	}

	private function _createNotifications($frequency, Time $startTime, Time $endTime) {

		$subscriptions = $this->ForumPostSubscriptions->find()
			->contain([
				'ForumPosts' => [
					'ForumComments' => [
						'sort' => 'created'
					]
				],
				'ForumPostComments' => [
					'conditions' => [
						'ForumPostComments.created >=' => $startTime,
						'ForumPostComments.created <=' => $endTime
					]
				],
				'Users'
			])
			->where([
				'ForumPostSubscriptions.frequency' => $frequency
			])
			->reject(function ($subscription, $key) {
				return empty($subscription->forum_post_comments);
			});

		// Invoked without arguments to load routes in order to use user prefixes
		Router::url();

		$emailTemplate = 'eblast_notification';

		$oldestForumPostComment = [
			'id' => 0,
			'page' => 0
		];

		foreach ($subscriptions as $subscription) {

			$commentCount = count($subscription->forum_post_comments);

			/*
			 * This block of code just tries to identify what page the oldest forum post is in so that links in the email would directly go to that specific page and scroll down on that specific post id
			 */

			$oldestForumPostComment = [
				'id' => 0,
				'page' => 1
			];

			// Limit set in the pagination _view
			$paginationLimit = 25;

			if (empty($oldestForumPostComment['id'])) {

				// Get oldest post not sent to the subscriber
				$oldestForumPostComment['id'] = $subscription->forum_post_comments[0]->id;

				// Add the parent post to accurately count the pages
				array_unshift($subscription->forum_post->forum_comments, $subscription->forum_post);

				// Find the index of the oldest post
				foreach ($subscription->forum_post->forum_comments as $index => $forumComment) {
					if ($forumComment->id == $oldestForumPostComment['id']) {
						$oldestForumPostCommentIndex = $index;
						break;
					}
				}

				// Chunk all the Posts based on pagination limit
				$chunkedPosts = array_chunk($subscription->forum_post->forum_comments, $paginationLimit, true);

				// Each (chunk + 1) == page number, if the Oldest page index isset in the current loop get the page then break
				foreach ($chunkedPosts as $page => $pagePosts) {

					if (isset($pagePosts[$oldestForumPostCommentIndex])) {
						$oldestForumPostComment['page'] = $page + 1;
						break;
					}

				}

			}

			/*************************************************************************************/

			if ($commentCount < 2) {
				$commentCountText = '<strong>One new comment</strong> has';
			} else {
				$commentCountText = '<strong>'.$commentCount.' new comments</strong> have';
			}

			$htmlContent = <<<HTML
<p>
	$commentCountText been posted in a forum you subscribed: <strong>{$subscription->forum_post->topic}</strong>.
</p>
HTML;

			$email = (new Email)
				->domain(Configure::read('App.fullBaseUrl'))
				->emailFormat('html')
				->template($emailTemplate)
				->subject('Forum Subscription: '.$subscription->forum_post->topic)
				->from('notification@khojaleadershipforum.org')
				->to($subscription->user->email)
				->set([
					'htmlContent' => $htmlContent,
					'buttonLabel' => 'Link to the Post',
					'buttonLink' => Router::url([
						'controller' => 'ForumPosts',
						'action' => 'view',
						$subscription->forum_post_id,
						'prefix' => $subscription->user->prefix,
						'_full' => true,
						'#' => 'forum-post-' . $oldestForumPostComment['id'],
						'?' => [
							'page' => $oldestForumPostComment['page']
						]
					])
				]);

			$emailEntity = $this->Emails->newEntity([
				'type' => $emailTemplate,
				'email_object' => $email->serialize(),
				'queued' => true
			]);

			$this->Emails->save($emailEntity);

		}

	}

}