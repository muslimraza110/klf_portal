<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Utility\Security;

class UserImportsShell extends Shell {

	public function initialize() {

		parent::initialize();

		$this->loadModel('Users');
		$this->loadModel('UserImports');

	}

	public function main() {

		foreach ($this->UserImports->find() as $userImport) {

			$userInvitation = $this->Users->UserInvitations->newEntity([
				'email' => $userImport->email,
				'hash' => sha1(Security::randomBytes(32))
			]);

			if ($this->Users->UserInvitations->save($userInvitation)) {

				$userInvitation->sendInvitation(
					$this->Users->findByEmail('ali@ebrahim.com')->first()->toArray()
				);

				$this->out('Invitation for "'.$userImport->email.'" sent.');

			} else {
				$this->out('Invitation for "'.$userImport->email.'" could not be sent.');
			}

		}

	}

}