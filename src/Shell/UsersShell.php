<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\I18n\Time;

class UsersShell extends Shell {
	
	public function initialize() {
		parent::initialize();
		$this->loadModel('Users');
	}
	
	public function main() {
		
		$this->_mark_as_offline();
	}
	
	protected function _mark_as_offline() {
		$online_max_limit = new Time('30 seconds ago');
		
		$usersIdle = $this->Users->find()
      ->where([
        'Users.chat_status !=' => 'O',
        'Users.last_online <' => $online_max_limit,
      ])
			->all();
		
		$count = 0;
		
		if (!empty($usersIdle)) {
			foreach($usersIdle as $userEntity) {
				$userEntity->dirty('modified', true);
				$userEntity->chat_status = 'O';
				
				if ($this->Users->save($userEntity))
					$count++;
			}
		}
		
		$this->out($count . ' users have been marked as offline.');
		$this->out('_mark_as_offline() is done!');
	}
	
}