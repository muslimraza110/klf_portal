<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\Mailer\MailerAwareTrait;

class NotificationsShell extends Shell {
	
	use MailerAwareTrait;
	
	public function initialize() {
		parent::initialize();
		$this->loadModel('Users');
	}
	
	public function main() {
		
		$this->_notify_users_of_posts_published_at_a_later_date();
	}
	
	protected function _notify_users_of_posts_published_at_a_later_date() {
		
		$posts = $this->Users->Posts->find()
			->where([
				'Posts.published IS NOT' => null,
				'Posts.published <=' => Time::now(),
				'Posts.notification_emails_sent' => 0
			])
			/*
			->matching('Users', function ($q) {
				return $q->where(['Users.role IN' => 'Posts.audience']);
			})
			*/
			->all();
		
		if(!empty($posts)) {
			foreach($posts as $post) {
				$this->_shoot_notification_emails($post);
			}
		}
		
		$this->out('_notify_users_of_posts_published_at_a_later_date() is done!');
	}
	
	protected function _shoot_notification_emails($post) {
		
		if (is_null($post->post_category_id) || $post->post_type_id == 1)
			$conditions = ['NotificationSettings.condition_column' => 'post_type_id', 'NotificationSettings.condition_value' => $post->post_type_id];
		else
			$conditions = ['NotificationSettings.condition_column' => 'post_category_id', 'NotificationSettings.condition_value' => $post->post_category_id];
		
		$defaultConditions = [
			'NotificationSettings.model' => 'Posts',
			'NotificationSettings.send_email' => 1,
			'NotificationSettings.created <=' => $post->created,
		];
		
		$conditions = array_merge($conditions, $defaultConditions);
		
		$notifications = $this->Users->NotificationSettings->find()
			->where($conditions)
			->matching('Users', function ($q) use ($post) {
				return $q->where(['Users.role IN' => $post->audience]);
			});
		
		$count = 0;
		
		if (!$notifications->isEmpty()) {
			foreach($notifications as $notification) {
				$user = $notification->_matchingData['Users'];
				if($this->getMailer('User')->send('sendNotification', [$post, $user])) {
					$count++;
				}
			}
		}
		
		$post->notification_emails_sent = 1;
		$post->dirty('modified', true);
		$this->Users->Posts->save($post);
		
		$this->out($count . ' emails have been sent for post #P' . $post->id);
	}
	
}