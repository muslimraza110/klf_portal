<?php


namespace App\Shell;

use Cake\Console\Shell;
use Cake\I18n\Time;

class OperatingBoardExpirationCheckShell extends Shell {


	public function initialize() {

		parent::initialize();

		$this->loadModel('Users');

	}

	public function main() {

		$users = $this->Users->find()
			->where([
				'Users.expiration_date IS NOT' => null,
				'Users.expiration_date <' => Time::now()
			]);

		if (empty($users))
			return;

		foreach ($users as $user) {
			$user->role = 'C';
			$user->expiration_date = null;
			$this->Users->save($user);
		}

	}

}
