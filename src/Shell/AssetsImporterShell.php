<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\I18n\Time;

class AssetsImporterShell extends Shell {

	private $_exportPath = ROOT.'/assets/export/';

	public function initialize() {

		parent::initialize();

		$this->loadModel('AssetManager.Assets');

	}

	public function main() {

		$exportZips = (new Folder($this->_exportPath))->find('export-.*\.zip', true);

		if (empty($exportZips)) {

			$this->out('[WARNING] No export ZIP archives found');
			exit;

		}

		$zip = new \ZipArchive;

		$zipFile = $this->_exportPath.end($exportZips);

		if (!$zip->open($zipFile)) {

			$this->err(sprintf('[ERROR] Reading ZIP archive "%s" failed', $zipFile));
			exit;

		}

		$zipFolder = new Folder(sys_get_temp_dir().'/'.uniqid(), true);

		$zip->extractTo($zipFolder->pwd());

		$zip->close();

		$exportTxtFile = $zipFolder->pwd().'/export.txt';

		if (!file_exists($exportTxtFile)) {

			$this->err('[ERROR] No export.txt found inside ZIP archive');
			exit;

		}

		$exportTxt = file($exportTxtFile, FILE_IGNORE_NEW_LINES);

		$newAssets = [];

		foreach ($exportTxt as $row) {

			$columns = explode('|', $row);

			if (count($columns) != 4) {

				$this->out(sprintf('[WARNING] Column count mismatch for asset ID %d', $columns[0]));
				continue;

			}

			$asset = $this->Assets->findById($columns[0])
					->first();

			if (empty($asset))
				$newAssets[] = $columns;
			else {

				$exportedAsset = unserialize($columns[1]);

				if ($asset->model != $exportedAsset->model
						|| $asset->model_id != $exportedAsset->model_id
						|| $asset->association != $exportedAsset->association)
					$newAssets[] = $columns;
				else {

					// @TODO: Updating assets not implemented yet

				}

			}

		}

		foreach ($newAssets as $newAsset) {

			$exportedAsset = unserialize($newAsset[1]);
			$exportedAssociation = unserialize($newAsset[2]);

			$associationTable = $exportedAssociation->source();

			$this->loadModel($associationTable);

			$association = $this->$associationTable->findById($exportedAssociation->id)
					->first();

			if (!empty($association))
				$exportedAssociation = $association;
			else {

				$exportedAssociation->id = null;
				$exportedAssociation->modified = Time::now();

				$exportedAssociation->isNew(true);

			}

			if ($this->$associationTable->save($exportedAssociation)) {

				$exportedAsset->id = null;
				$exportedAsset->model_id = $exportedAssociation->id;
				$exportedAsset->modified = Time::now();

				$exportedAsset->isNew(true);

				if ($this->Assets->save($exportedAsset)) {

					if (rename($zipFolder->pwd().'/'.$newAsset[0], $exportedAsset->path()))
						$this->out(sprintf('[INFO] New asset ID %d was created from exported asset ID %d', $exportedAsset->id, $newAsset[0]));
					else
						$this->out(sprintf('[WARNING] Could not create file for asset ID %d', $newAsset[0]));

				} else
					$this->out(sprintf('[WARNING] Could not create new asset for asset ID %d', $newAsset[0]));

			} else
				$this->out(sprintf('[WARNING] Could not create association for asset ID %d', $newAsset[0]));

		}

		$zipFolder->delete();

	}

}