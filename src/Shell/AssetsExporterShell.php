<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\I18n\Time;

class AssetsExporterShell extends Shell {

	private $_exportPath = ROOT.'/assets/export/';

	public function initialize() {

		parent::initialize();

		$this->loadModel('AssetManager.Assets');

	}

	public function main() {

		$assets = $this->Assets->find()
				// Limit to shop assets
				->where([
					'Assets.model IN' => ['ProductCategoriesTable', 'ProductImagesTable']
				]);

		$zip = new \ZipArchive;

		$zipFile = sprintf(
				'%sexport-%s.zip',
				$this->_exportPath,
				(new Time)->format('YmdHis')
		);

		$zip->open($zipFile, \ZipArchive::CREATE);

		$txt = '';

		foreach ($assets as $asset) {

			$associationTable = str_replace('Table', '', $asset->model);

			$this->loadModel($associationTable);

			$association = $this->$associationTable->findById($asset->model_id)
					->first();

			if (empty($association)) {

				$this->out(sprintf('[WARNING] Association "%s" not found for asset ID %d', $associationTable, $asset->id));
				continue;

			}

			$assetFile = $asset->path();

			if (!file_exists($assetFile)) {

				$this->out(sprintf('[WARNING] File was not found for asset ID %d', $asset->id));
				continue;

			}

			$zip->addFile($assetFile, $asset->id);

			$row = [
				$asset->id,
				serialize($asset),
				serialize($association),
				sha1_file($assetFile)
			];

			$txt .= implode('|', $row)."\n";

		}

		$zip->addFromString('export.txt', $txt);

		$zip->close();

		$this->out('[INFO] ZIP archive created: '.$zipFile);

	}

}