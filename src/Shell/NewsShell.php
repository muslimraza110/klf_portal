<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\I18n\Time;

// */1 * * * * /var/www/khoja/bin/cake News >/dev/null 2>&1

class NewsShell extends Shell {

	public function initialize() {
		parent::initialize();
		$this->loadModel('Settings');
		$this->loadModel('Posts');
	}

	public function main() {

		$this->_rotate_news();
	}

	protected function _rotate_news() {

		$setting = $this->Settings->get('FIXED_NEWS_ITEM');

		if (empty($setting->cron_last_executed)) {
			$setting->cron_last_executed = Time::now();
			$this->Settings->save($setting);
		} else {

			$nextExecutionTime = (new Time($setting->cron_last_executed))->addMinutes($setting->cron_timer);

			if (Time::now() < $nextExecutionTime) {
				return;
			} else {
				$setting->cron_last_executed = Time::now();
				$this->Settings->save($setting);
			}

		}

		if ($setting->value) {

			$pinnedPost = $this->Posts->find()
				->where([
					'Posts.category_id' => 1,
					'Posts.status' => 'A',
					'Posts.pinned' => true,
					'Posts.archived' => false,
					'Posts.published IS NOT' => null
				])
				->first();

			if (!empty($pinnedPost)) {
				$this->out('A post is marked as pinned. Rotation ignored.');
			} else {

				// Current active post
				$activeSlide = $this->Posts->find()
					->where([
						'Posts.active_slide' => true
					])
					->first();

				$excludeSlide = [];

				if (!empty($activeSlide)) {
					$excludeSlide['Posts.id !='] = $activeSlide->id;
					$activeSlide->active_slide = false;
					$this->Posts->save($activeSlide);
				}

				// Find 4 latest posts
				$posts = $this->Posts->find()
					->where([
						'Posts.category_id' => 1,
						'Posts.status' => 'A',
						'Posts.archived' => false,
						'Posts.published IS NOT' => null,
						'Posts.published <=' => Time::now(),
						$excludeSlide
					])
					->limit(4)
					->extract('id')
					->toArray();

				$activateSlide = $this->Posts->get($posts[array_rand($posts)]);
				$activateSlide->active_slide = true;

				if ($this->Posts->save($activateSlide)) {
					$this->out('Rotation successfully changed.');
				} else {
					$this->out('An error has occurred in the rotation.');
				}

			}

		} else {
			$this->out('Fixed News Item set to false.');
		}

	}

}