<?php

namespace App\Mailer;

class UserRecommendationMailer extends Mailer {

	public function sendInvitation($userRecommendation, $user) {

		return $this->setMailer(
			'recommendation_invitation',
			sprintf('Recommend %s for the Khoja Portal', $userRecommendation->name),
			compact('userRecommendation', 'user'),
			[$user->email, $user->name]
		);

	}

	public function sendTaggedUserNotification($userRecommendation, $user) {

		return $this->setMailer(
			'tagged_user_notification',
			sprintf('Decision on %s', $userRecommendation->name),
			compact('userRecommendation', 'user'),
			[$user->email, $user->name]
		);

	}

  // public function sendGroupMembershipRequest($user, $group) {
  //
	// 	return $this->setMailer(
	// 		'group_membership_request',
	// 		compact('user', 'group'),
	// 		[$user->email, $user->name]
	// 	);
  //
	// }

}
