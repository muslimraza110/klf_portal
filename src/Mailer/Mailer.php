<?php
namespace App\Mailer;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class Mailer extends \Cake\Mailer\Mailer {
	
	protected $_error;
	
	public function last_error() {
		return $this->_error;
	}
	
	public function setMailer($template, $subject, $vars = [], $to = null, $from = null) {
		
		$this->_email
			->emailFormat('html')
			->template($template)
			->subject($subject);
		
		if (!empty($vars) && is_array($vars)) {
			foreach ($vars as $key => $val) {
				$this->_email->set($key, $val);
			}
		}

		$this->_email->set('_to', $to);
		
		try {
			if (is_array($from) && isset($from['email']) && isset($from['name']))
				$this->_email->from($from['email'], $from['name']);
			elseif (is_array($from) && isset($from[0]) && isset($from[1]))
				$this->_email->from($from[0], $from[1]);
			else
				$this->_email->from($from);
		}
		catch (\Exception $e) {
			$this->keep_track_of_email($this->_email, $e);
			return false;
		}
		
		if (Configure::read('debug')) {

			if (Configure::read('debuggingEmail')) {
				$testing_email = Configure::read('debuggingEmail');
			}
			else {
				$testing_email = 'jesse@incodeapps.com';
			}

			try {
				$this->_email
					//->from($testing_email, 'CakePhp Debugging')
					->cc([])
					->bcc([])
					->to($testing_email);
			}
			catch (\Exception $e) {
				$this->keep_track_of_email($this->_email, $e);
				return false;
			}
		}
		else {
			
			try {
				if (is_array($to) && isset($to['email']) && isset($to['name']))
					$this->_email->to($to['email'], $to['name']);
				elseif (is_array($to) && isset($to[0]) && isset($to[1]))
					$this->_email->to($to[0], $to[1]);
				else
					$this->_email->to($to);
			}
			catch (\Exception $e) {
				$this->keep_track_of_email($this->_email, $e);
				return false;
			}
		}
		
		return true;
	}
	
	public function send($action, $args = [], $headers = []) {
		
		try {
			$emailToSend = $this->_email;
			$emailSent = parent::send($action, $args, $headers);
			$this->keep_track_of_email($emailToSend);
			return true;
		}
		catch (\Exception $e) {
			$this->keep_track_of_email($this->_email, $e);
			return false;
		}
		
		return false;
	}
	
	public function keep_track_of_email($mailer, \Exception $exception = null) {
		
		/*
		//Keep in logs
		if (!is_null($exception)) {
			$this->_error = $exception;
			
			$message = sprintf(
				"[%s] %s",
				get_class($exception),
				$exception->getMessage()
			);
			
			$debug = Configure::read('debug');
			
			if ($debug && method_exists($exception, 'getAttributes')) {
				$attributes = $exception->getAttributes();
				if ($attributes) {
					$message .= "\nException Attributes: " . var_export($exception->getAttributes(), true);
				}
			}
			
			$message .= "\nMailer Function: " . $mailer->viewBuilder()->template();
			
			if ($this->config('trace')) {
				$message .= "\nSerialized Email Object:\n" . $mailer->serialize();
				$message .= "\nStack Trace:\n" . $exception->getTraceAsString() . "\n\n";
			}
			
			Log::write('error', $message, 'email');
		}
		*/
		
		
		//Keep in DB
		$EmailsSent = TableRegistry::get('Emails');
		
		$entity = $EmailsSent->newEntity();
		$entity->type = $mailer->viewBuilder()->template();
		$entity->html_content = $mailer->message('html');
		$entity->email_object = $mailer->serialize();
		
		if (!empty($mailer->viewVars)) {
			if (isset($mailer->viewVars['user'])) {
				$entity->user_id = is_array($mailer->viewVars['user']) ? $mailer->viewVars['user']['id'] : $mailer->viewVars['user']->id;
			}
		}
		
		if (!is_null($exception)) {
			$this->_error = $exception;
			$entity->error = $exception->getMessage();
		}
		
		$EmailsSent->save($entity);
	}
	
}