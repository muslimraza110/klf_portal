<?php

namespace App\Mailer;

class FormMailer extends Mailer {

	public function sendReminder($form, $user) {

		return $this->setMailer(
			'form_reminder',
			'Khoja Form: '.$form->name.' is awaiting your input',
			compact('form', 'user'),
			[$user->email, $user->name]
		);

	}

}