<?php

namespace App\Mailer;

class UserFamilyMailer extends Mailer {

	public function sendNotification($userFamily) {

		return $this->setMailer(
			'family_notification',
			'New Family Relation in the Khoja Portal',
			compact('userFamily'),
			[$userFamily->related_user->email, $userFamily->related_user->name]
		);

	}

}