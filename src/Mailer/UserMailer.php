<?php
namespace App\Mailer;

class UserMailer extends Mailer {

	public function resetPassword($user) {
		return $this->setMailer('reset_password', 'Khoja: Password Reset', ['user' => $user], [$user->email, $user->name]);
	}

	public function setPassword($user) {
		return $this->setMailer('set_password', 'Welcome!', ['user' => $user], [$user->email, $user->name]);
	}
	
	public function sendNotification($entity, $user) {
		$subject = 'New Post: ' . $entity->name;
		$template = 'notification';
		$vars = [
			'user' => $user,
			'entity' => $entity
		];
		
		$class = get_class($entity);
		$class = explode('\\', $class);
		$class = array_pop($class);
		
		switch($class) {
			case 'ForumPost':
				$template = 'notification_forum_post';
				break;
			case 'Post':
				$template = 'notification_post';
				break;
			case 'UserForm':
				$subject = 'Form "'. $entity->form->name . '" has been submitted by ' . $entity->user->name;
				$template = 'user_answers_notification';
				break;
		}
		
		return $this->setMailer($template, $subject, $vars, [$user->email, $user->name]);
	}

	public function sendInvitation($userInvitation, $user) {

		return $this->setMailer(
			'invitation',
			'Invitation to the Khoja Portal',
			compact('userInvitation', 'user'),
			[$userInvitation->email, $userInvitation->email]
		);

	}

	public function sendDecision($user) {

		return $this->setMailer(
			'decision',
			'Decision on Your Khoja Portal Membership',
			compact('user'),
			[$user->email, $user->name]
		);

	}

	public function sendGroupMembershipRequest($user, $group, $groupAdmin) {

		return $this->setMailer(
			'group_membership_request',
			'Group Membership Request',
			compact('user', 'group', 'groupAdmin'),
			[$groupAdmin->email, $groupAdmin->name]
		);

	}

	public function sendCommentToCopywriter($user, $copywriter, $comment) {

		return $this->setMailer(
			'comment_about_bio',
			'Comment About Bio',
			compact('user', 'copywriter', 'comment'),
			[$copywriter->email, $copywriter->name]
		);

	}

	public function sendProjectMembershipRequest($user, $project, $groupAdmin) {

		return $this->setMailer(
			'project_membership_request',
			'Project Membership Request',
			compact('user', 'project', 'groupAdmin'),
			[$groupAdmin->email, $groupAdmin->name]
		);

	}

	public function sendFundraisingNotification($user, $fundraising, $receiver) {

		return $this->setMailer(
			'fundraising_notification',
			$fundraising->email_subject,
			compact('user', 'fundraising', 'receiver'),
			[$receiver->email, $receiver->name]
		);

	}

	public function sendInitiativeNotification($user, $initiative, $receiver) {

		return $this->setMailer(
			'initiative_notification',
			$initiative->email_subject,
			compact('user', 'initiative', 'receiver'),
			[$receiver->email, $receiver->name]
		);

	}

	public function sendPledgeNotification($user, $pledgeInfo, $receiver) {

		return $this->setMailer(
			'pledge_notification',
			'A Pledge has been made!',
			compact('user', 'pledgeInfo', 'receiver', 'group'),
			[$receiver['email'], $receiver['name']]
		);

	}

	public function sendPledgeRequestNotification($pledgeRequest, $receiver) {

		return $this->setMailer(
			'pledge_request_notification',
			'Your Pledge Request has been Updated.',
			compact('pledgeRequest', 'receiver'),
			[$receiver['email'], $receiver['name']]
		);

	}

}
